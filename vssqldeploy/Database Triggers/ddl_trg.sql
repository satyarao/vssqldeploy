﻿CREATE   TRIGGER [ddl_trg]
ON DATABASE
FOR DDL_DATABASE_LEVEL_EVENTS
AS   
BEGIN
SET NOCOUNT ON 
SET XACT_ABORT OFF

	DECLARE @data XML  
	BEGIN TRY

		SET @data = EVENTDATA() 

		INSERT dbo.DDL_Log
		(
			PostTime, 
			DB_User, 
			EventType, 
			TSQLCommand,
			ObjectSchema,
			ObjectName,
			AppName,
			CheckInUser,
			CheckInComment,
			ScriptFileName
		)   
		VALUES   
		(
			GETDATE(),   
			CONVERT(nvarchar(100), SUSER_SNAME()),   
			@data.value('(/EVENT_INSTANCE/EventType)[1]', 'NVARCHAR(100)'),   
			@data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'NVARCHAR(2000)'),
			@data.value('(/EVENT_INSTANCE/SchemaName)[1]',  'VARCHAR(255)'),  
			@data.value('(/EVENT_INSTANCE/ObjectName)[1]',  'VARCHAR(255)'), 
			APP_NAME(),
			TRY_CONVERT(VARCHAR(50), SESSION_CONTEXT(N'CheckInUser')),
			TRY_CONVERT(VARCHAR(200), SESSION_CONTEXT(N'CheckInComments')),
			TRY_CONVERT(VARCHAR(200), SESSION_CONTEXT(N'ScriptFileName'))
		);

	END TRY
	BEGIN CATCH
			--select ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY()
			--PRINT FORMATMESSAGE( 'ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY() : (%s, %i, %i, %s, %i)',  ISNULL(ERROR_PROCEDURE(), 'NA'), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY())
	END CATCH
END
