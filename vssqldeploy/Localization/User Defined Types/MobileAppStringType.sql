﻿CREATE TYPE [Localization].[MobileAppStringType] AS TABLE (
    [LanguageID]       NVARCHAR (10)    NOT NULL,
    [AppStringsID]     NVARCHAR (255)   NOT NULL,
    [DefaultAppString] NVARCHAR (MAX)   NOT NULL,
    [TenantID]         UNIQUEIDENTIFIER NULL,
    [AppString]        NVARCHAR (MAX)   NULL);

