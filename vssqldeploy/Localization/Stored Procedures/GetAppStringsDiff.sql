﻿-- =============================================
-- Author:		Davydovich Raman
-- Create date: 8/16/2019
-- Description:	Get app strings diff for languages
-- =============================================
CREATE PROCEDURE [Localization].[GetAppStringsDiff]
    @Language1 NVARCHAR(10),
    @Language2 NVARCHAR(10),
    @Offset INT,
    @Limit INT
AS
BEGIN
    SET NOCOUNT ON;

    WITH
        TempResult
        AS
        (
            SELECT ISNULL(strings1.AppStringsId, strings2.AppStringsId) AS AppStringsId, strings1.AppString AS Value1, strings2.AppString AS Value2
            FROM (SELECT AppStringsId, AppString
                FROM Localization.AppStrings
                WHERE LanguageId = @Language1) AS strings1
                FULL JOIN (SELECT AppStringsId, AppString
                FROM Localization.AppStrings
                WHERE LanguageId = @Language2) AS strings2 ON strings1.AppStringsId = strings2.AppStringsId
            WHERE strings1.AppStringsId IS NULL OR strings2.AppStringsId IS NULL
        ),
        TempCount
        AS
        (
            SELECT COUNT(*) AS Total
            FROM TempResult
        )

    SELECT *
    FROM TempResult, TempCount
    ORDER BY TempResult.AppStringsId 
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY;
END
