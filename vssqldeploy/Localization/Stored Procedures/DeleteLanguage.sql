﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 5/19/2016
-- Update date: 17/01/2017 delete language and tenant strings for certain tenant
-- Description:	Delete language from localization
-- =============================================
CREATE PROCEDURE [Localization].[DeleteLanguage] 
	-- Add the parameters for the stored procedure here
	@LanguageId nvarchar(max),
	@TenantId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN		
		DELETE FROM Localization.TenantStrings WHERE SupportedLanguageId = @LanguageId AND TenantId = @TenantId
		DELETE FROM Localization.SupportedLanguage WHERE SupportedLanguageId = @LanguageId AND TenantId = @TenantId
	END	
END
