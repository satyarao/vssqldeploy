﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 4/25/2016
-- Update date: 5/20/2016
-- Description:	Delete string or tenant string for language and increment version
-- =============================================
CREATE PROCEDURE [Localization].[DeleteString]
	@AppStringsId NVARCHAR(4000),
	@LanguageId NVARCHAR(10),
	@TenantId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRAN
	IF @TenantId IS NOT NULL 
	BEGIN
		DELETE FROM Localization.TenantStrings
		WHERE AppStringsId = @AppStringsId
		AND SupportedLanguageId = @LanguageId
		AND TenantId = @TenantId
	END
	ELSE 
	BEGIN
		DELETE FROM Localization.AppStrings
		WHERE LanguageId = @LanguageId
		AND AppStringsId = @AppStringsId
	END
	
	EXEC [Localization].[IncrementLanguageVersion] @LanguageId = @LanguageId

	COMMIT TRAN
END
