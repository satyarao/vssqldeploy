﻿
-- =============================================
-- Author:		Igor Gaidukov
-- Update date: 03/20/2018 - Igor Gaidukov - use MERGE instead of TRANSACTION
-- Description:	Import a appstrings key/values pair and increment version
-- =============================================
CREATE PROCEDURE [Localization].[ImportMobileAppStrings] 
	@TenantId UNIQUEIDENTIFIER,
	@AppStrings MobileAppStringType READONLY
AS
BEGIN
	SET NOCOUNT ON;
	
	SET XACT_ABORT ON
	
	BEGIN TRAN
		DECLARE 
			@AppStringValue [nvarchar](max),
			@CurrentLanguageID [nvarchar](10),
			@CurrentAppStringsID [nvarchar](255),
			@CurrentDefaultAppString [nvarchar](max),
			@CurrentTenantID [uniqueidentifier],
			@CurrentAppString [nvarchar](max)

		DECLARE cur CURSOR READ_ONLY FOR	
			SELECT [LanguageID],
				[AppStringsID],
				[DefaultAppString],
				[TenantID],
				[AppString]
			FROM @AppStrings

		OPEN cur 
			FETCH NEXT FROM cur INTO @CurrentLanguageID, @CurrentAppStringsID, @CurrentDefaultAppString, @CurrentTenantID, @CurrentAppString
			WHILE @@FETCH_STATUS = 0 
			BEGIN 
				
				SET @AppStringValue = CASE 
							WHEN @CurrentAppString IS NULL OR LTRIM(@CurrentAppString) = '' THEN @CurrentDefaultAppString
							ELSE @CurrentAppString 
						END
				
				IF NOT EXISTS ( SELECT 1 FROM Localization.AppStrings WHERE AppStringsId = @CurrentAppStringsID AND LanguageId = @CurrentLanguageID )
				BEGIN
					--Create new app string as default
					INSERT INTO Localization.AppStrings (
						AppStringsId, 
						LanguageId, 
						AppString) 
					VALUES (
						@CurrentAppStringsID, 
						@CurrentLanguageID, 
						@AppStringValue)
				END
				ELSE IF NOT EXISTS (SELECT 1 FROM Localization.TenantStrings  WHERE AppStringsId = @CurrentAppStringsID AND SupportedLanguageId = @CurrentLanguageID AND TenantId = @TenantID)
					BEGIN
						--Override app string
						IF NOT EXISTS ( SELECT 1 FROM Localization.AppStrings WHERE AppStringsId = @CurrentAppStringsID AND LanguageId = @CurrentLanguageID AND AppString = @AppStringValue)
						BEGIN
							--Do not override string value if it is equal to a default value for a key.
							INSERT INTO Localization.TenantStrings (
								AppStringsId, 
								SupportedLanguageId, 
								AppString,
								TenantId) 
							VALUES (
								@CurrentAppStringsID, 
								@CurrentLanguageID, 
								@AppStringValue,
								@TenantId)
						END
					END
					ELSE
					BEGIN
						--Update overridden app string
						IF NOT EXISTS ( SELECT 1 FROM Localization.AppStrings WHERE AppStringsId = @CurrentAppStringsID AND LanguageId = @CurrentLanguageID AND AppString = @AppStringValue)
						BEGIN
							UPDATE Localization.TenantStrings 
							SET AppString = @AppStringValue
							WHERE AppStringsId = @CurrentAppStringsID AND TenantId = @TenantId AND SupportedLanguageId = @CurrentLanguageID
						END
						ELSE
						BEGIN
							--Delete overridden string value if it is equal to a default value for a key
							DELETE FROM Localization.TenantStrings
							WHERE AppStringsId = @CurrentAppStringsID AND SupportedLanguageId = @CurrentLanguageID AND TenantId = @TenantID
						END 
					END		

				EXEC [Localization].[IncrementLanguageVersion] @LanguageId = @CurrentLanguageID

				FETCH NEXT FROM cur INTO @CurrentLanguageID, @CurrentAppStringsID, @CurrentDefaultAppString, @CurrentTenantID, @CurrentAppString
			END 
		CLOSE cur 
		DEALLOCATE cur

	COMMIT TRAN
END
