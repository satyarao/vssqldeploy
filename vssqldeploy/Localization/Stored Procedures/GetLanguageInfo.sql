﻿-- =============================================
-- Create date: 2016-06-10
-- Description:	Get information about language
-- =============================================
CREATE PROCEDURE [Localization].[GetLanguageInfo] 
	@LanguageId NChar(10) = NULL,
	@TenantId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF(@TenantId IS NOT NULL)
	BEGIN
		SELECT 
			 l.[LanguageId]
			,l.[LanguageName]
			,l.[Version]
		FROM [Localization].[Languages] l
		JOIN Localization.SupportedLanguage s ON l.LanguageId = s.SupportedLanguageId
		Where l.[LanguageId] = ISNULL(@LanguageId, l.[LanguageId])
		AND s.TenantId = ISNULL(@TenantId, s.TenantId)
	END
	ELSE
	BEGIN
		SELECT 
			 l.[LanguageId]
			,l.[LanguageName]
			,l.[Version]
		FROM [Localization].[Languages] l
		Where l.[LanguageId] = ISNULL(@LanguageId, l.[LanguageId])
	END
END
