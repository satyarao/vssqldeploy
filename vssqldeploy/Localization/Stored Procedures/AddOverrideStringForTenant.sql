﻿
-- =============================================
-- Author:		Mason Sciotti
-- Create date: 03/02/2016
-- Update date: 05/20/2016
-- Update date: 03/20/2018 - Igor Gaidukov - use MERGE instead of TRANSACTION
-- Description:	Adds a new tenant override key/value pair for a given language and increment version
-- =============================================
CREATE PROCEDURE [Localization].[AddOverrideStringForTenant] 
	@StringKey nvarchar(50),
	@StringValue nvarchar(max),
	@LanguageId nvarchar(10),
	@TenantId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
	
	MERGE Localization.TenantStrings  AS target
	USING (SELECT   @StringKey AS StringKey
				  , @StringValue AS StringValue
				  , @LanguageId AS LanguageId
				  , @TenantId AS TenantId) AS source
	ON (target.AppStringsId = source.StringKey
		AND target.SupportedLanguageId = source.LanguageId
		AND target.TenantId = source.TenantId)

	WHEN MATCHED THEN
		UPDATE SET AppString = source.StringValue

	WHEN NOT MATCHED THEN
		INSERT ( [AppStringsId]
				,[SupportedLanguageId]
				,[TenantId]
				,[AppString])
		VALUES (source.StringKey
			,source.LanguageId
			,source.TenantId
			,source.StringValue);

	EXEC [Localization].[IncrementLanguageVersion] @LanguageId = @LanguageId
END
