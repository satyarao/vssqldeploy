﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 4/21/2016
-- Description:	Get Tenant/Language combinations for app strings
-- Update date: 2/15/2019 Darya Batalava - added limit, offset
-- Update date: 8/16/2019 Davydovich Raman - added tenantId, searchTerm
-- =============================================
CREATE PROCEDURE [Localization].[GetTenantLanguageCombinations]
    @TenantId uniqueidentifier = NULL,
    @SearchTerm NVARCHAR(MAX) = NULL,
    @Offset INT = NULL,
    @Limit INT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    IF @Offset IS NULL SET @Offset = 0;
    IF @Limit IS NULL SET @Limit  = (SELECT COUNT(*)
    FROM Localization.SupportedLanguage);

    WITH
        TempResult
        AS
        (
            SELECT l.LanguageId, s.TenantId, t.Name AS TenantName, l.[Version], l.LanguageName
            FROM Localization.SupportedLanguage s
                JOIN Localization.Languages l ON l.LanguageId = s.SupportedLanguageId
                JOIN dbo.Tenant t ON t.TenantId=s.TenantId
            WHERE (@TenantId IS NULL OR (t.TenantId = @TenantId))
                AND (@SearchTerm IS NULL
                OR (l.LanguageId like '%' + @SearchTerm + '%')
                OR (l.[Version] like '%' + @SearchTerm + '%')
                OR (l.LanguageName like '%' + @SearchTerm + '%'))
        ),
        TempCount
        AS
        (
            SELECT COUNT(*) AS Total
            FROM TempResult
        )

    SELECT LanguageId,
        TenantId,
        TenantName,
        [Version],
        LanguageName,
        Total
    FROM TempResult, TempCount
    ORDER BY LanguageId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

END
