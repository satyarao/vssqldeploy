﻿
-- =============================================
-- Author:		Mason Sciotti
-- Create date: 3/1/2016
-- Modified:    6/22/2016
-- Description:	Return Strings for Input Language
-- =============================================
CREATE PROCEDURE [Localization].[GetLanguageStrings] 
	@LanguageId NCHAR(10),
	@TenantId UNIQUEIDENTIFIER = null,
	@AppStringsId NVARCHAR(4000) = null,
	@AppString NVARCHAR(MAX) = null
AS
BEGIN	
	SET NOCOUNT ON;

	SELECT DISTINCT 
		  base.AppStringsId
		, COALESCE(ast.AppString, asl.AppString, base.AppString) AS AppString
		, base.AppString AS BaseAppString
		, COALESCE(ast.SupportedLanguageId, asl.LanguageId, base.LanguageId) AS LanguageId
		, COALESCE(ast.TenantId, '00000000-0000-0000-0000-000000000000') AS TenantId
	FROM Localization.AppStrings base
	LEFT JOIN Localization.AppStrings asl on asl.AppStringsId = base.AppStringsId and asl.LanguageId = @LanguageId
	LEFT JOIN Localization.TenantStrings ast ON ast.AppStringsId = base.AppStringsId and ast.SupportedLanguageId = @LanguageId AND ast.TenantId = @TenantId
	WHERE (@AppStringsId IS NULL OR base.AppStringsId LIKE '%' + @AppStringsId + '%')
	AND base.LanguageId = 'en'
	--TODO Work with collate to query by AppString
	AND (@AppString IS NULL 
		OR (base.AppString LIKE '%' + @AppString + '%' and base.LanguageId = @languageId)
		OR asl.AppString LIKE '%' + @AppString + '%'
		OR ast.AppString LIKE '%' + @AppString + '%'
		)

	ORDER BY base.AppStringsId;
END
