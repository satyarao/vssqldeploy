﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 3/2/2016
-- Update date: 17/01/2017 add language for certain tenant
-- Description:	Add language for tenant in Localization tables
-- =============================================
CREATE PROCEDURE [Localization].[AddAppLanguageForTenant]
	-- Add the parameters for the stored procedure here
	@LanguageName nvarchar(max),
	@LanguageId nvarchar(10),
	@TenantId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

END
IF EXISTS (SELECT * from Localization.Languages WHERE LanguageId = @LanguageId)
BEGIN
   IF EXISTS (SELECT * FROM Localization.SupportedLanguage WHERE SupportedLanguageId = @LanguageId AND TenantId = @TenantId)
	BEGIN
		UPDATE Localization.Languages SET LanguageName = @LanguageName WHERE LanguageId = @LanguageId
	END
   ELSE
	BEGIN
		UPDATE Localization.Languages SET LanguageName = @LanguageName WHERE LanguageId = @LanguageId
		INSERT INTO Localization.SupportedLanguage (SupportedLanguageId, TenantId) VALUES (@LanguageId, @TenantId)
	END
END
ELSE
	BEGIN
		INSERT INTO Localization.Languages (LanguageId, LanguageName, [Version]) VALUES (@LanguageId, @LanguageName, 1)
		INSERT INTO Localization.SupportedLanguage (SupportedLanguageId, TenantId) VALUES (@LanguageId, @TenantId)
	END
