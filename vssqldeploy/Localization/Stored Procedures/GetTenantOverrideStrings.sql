﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 3/1/2016
-- Description:	Get tenant-specific strings by language and tenantId
-- =============================================
CREATE PROCEDURE [Localization].[GetTenantOverrideStrings] 
	@Language nchar(10),
	@TenantId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT AppStringsId, AppString
	FROM [Localization].[TenantStrings]
    WHERE SupportedLanguageId = @Language AND TenantId = @TenantId
		   
END
