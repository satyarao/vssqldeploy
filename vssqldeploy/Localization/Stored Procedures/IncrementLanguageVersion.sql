﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 4/21/2016
-- Description:	Increment langauge version for localization
-- =============================================
CREATE PROCEDURE [Localization].[IncrementLanguageVersion]
	@LanguageId NVARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Localization.Languages
	SET [Version] = [Version] + 1
	WHERE LanguageId = @LanguageId
END
