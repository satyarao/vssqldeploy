﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 6/20/2016
-- Update date: 8/25/2016
-- Update date: 6/19/2017 - Andrei Ramanovich - add search in overrides
-- Update date: 3/19/2019 - Vadim Prikich - separate value and default value string search WP-12444
-- Description:	Version 2 of getting localization strings for portals
-- =============================================
CREATE PROCEDURE [Localization].[GetStringsForPortals] 
	@LanguageId nvarchar(max),	
	@TenantId uniqueidentifier = null,
	@AppStringsId nvarchar(max) = null,
	@AppString nvarchar(max) = null,
	@OverrideString nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT ON;

	WITH Base (LanguageId, TenantId, AppStringsId, AppString) as ( 
		SELECT LanguageId, '00000000-0000-0000-0000-000000000000', AppStringsId, AppString		
		FROM Localization.AppStrings
		WHERE LanguageId = @LanguageId
		and (@AppStringsId is null or AppStringsId like '%' + @AppStringsId + '%')
	),

	Overrides (LanguageId, TenantId, AppStringsId, AppString) as (
		SELECT SupportedLanguageId, TenantId, AppStringsId, AppString
		from Localization.TenantStrings
		WHERE SupportedLanguageId = @LanguageId
		and (@AppStringsId is null or AppStringsId like '%' + @AppStringsId + '%')
		and (@TenantId is null or TenantId = @TenantId)
	)

	SELECT 
		  Base.LanguageId
		, Base.AppStringsId
		, Base.AppString as BaseValue 
		, Overrides.TenantId
		, Overrides.AppString
	FROM Base left outer join Overrides on Base.AppStringsId = Overrides.AppstringsId	
	WHERE (@AppString is null or Base.AppString like '%' + @AppString + '%')
		AND (@OverrideString is null or Overrides.AppString like '%' + @OverrideString + '%')

END
