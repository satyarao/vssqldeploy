﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 5/03/2016
-- Description:	Get all LanguageIds for portals filtering
-- =============================================
CREATE PROCEDURE [Localization].[GetLanguageIds]
	-- Add the parameters for the stored procedure here	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT LanguageId
	FROM Localization.Languages
END
