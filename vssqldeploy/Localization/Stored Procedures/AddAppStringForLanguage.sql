﻿
-- =============================================
-- Author:		Mason Sciotti
-- Create date: 03/02/2016
-- Update date: 05/20/2016
-- Update date: 03/20/2018 - Igor Gaidukov - use MERGE instead of TRANSACTION
-- Description:	Adds a new appstring key/value pair for a given language and increment version
-- =============================================
CREATE PROCEDURE [Localization].[AddAppStringForLanguage] 
	@StringKey nvarchar(250),
	@StringValue nvarchar(max),
	@LanguageId nvarchar(10)
AS
BEGIN
	SET NOCOUNT ON;
	
	MERGE Localization.AppStrings  AS target
	USING (SELECT   @StringKey AS StringKey
				  , @StringValue AS StringValue
				  , @LanguageId AS LanguageId) AS source
	ON (target.AppStringsId = source.StringKey
		AND target.LanguageId = source.LanguageId)

	WHEN MATCHED THEN
		UPDATE SET AppString = source.StringValue

	WHEN NOT MATCHED THEN
		INSERT ( [AppStringsId]
				,[LanguageId]
				,[AppString])
		VALUES (source.StringKey
			,source.LanguageId
			,source.StringValue);

	EXEC [Localization].[IncrementLanguageVersion] @LanguageId = @LanguageId
END
