﻿CREATE TABLE [Localization].[TenantStrings] (
    [AppStringsId]        NVARCHAR (255)   NOT NULL,
    [SupportedLanguageId] NVARCHAR (10)    NOT NULL,
    [TenantId]            UNIQUEIDENTIFIER NOT NULL,
    [AppString]           NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [UK_AppString_Language_Tenant] UNIQUE NONCLUSTERED ([AppStringsId] ASC, [SupportedLanguageId] ASC, [TenantId] ASC) WITH (FILLFACTOR = 80)
);

