﻿CREATE TABLE [Localization].[SupportedLanguage] (
    [SupportedLanguageId] NVARCHAR (10)    NOT NULL,
    [TenantId]            UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_SupportedLanguage] PRIMARY KEY CLUSTERED ([SupportedLanguageId] ASC, [TenantId] ASC) WITH (FILLFACTOR = 80)
);

