﻿CREATE TABLE [Localization].[AppStrings] (
    [AppStringsId] NVARCHAR (255) NOT NULL,
    [LanguageId]   NVARCHAR (10)  NOT NULL,
    [AppString]    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [UK_AppString_Language] UNIQUE NONCLUSTERED ([AppStringsId] ASC, [LanguageId] ASC) WITH (FILLFACTOR = 80)
);

