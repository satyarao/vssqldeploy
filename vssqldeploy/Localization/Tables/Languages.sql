﻿CREATE TABLE [Localization].[Languages] (
    [LanguageId]   NVARCHAR (10)  NOT NULL,
    [LanguageName] NVARCHAR (MAX) NOT NULL,
    [Version]      INT            NOT NULL,
    CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED ([LanguageId] ASC) WITH (FILLFACTOR = 80)
);

