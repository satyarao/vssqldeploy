﻿CREATE TABLE [NuDetect].[UserPaymentUnblocks] (
    [Id]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserId]  UNIQUEIDENTIFIER NOT NULL,
    [DateUtc] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_UserPaymentUnblocks] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserPaymentUnblocks_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserPaymentUnblocks_UserId]
    ON [NuDetect].[UserPaymentUnblocks]([UserId] ASC);

