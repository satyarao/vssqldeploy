﻿CREATE TABLE [NuDetect].[UserCardUnblocks] (
    [Id]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserId]  UNIQUEIDENTIFIER NOT NULL,
    [DateUtc] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_UserCardUnblocks] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserCardUnblocks_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserCardUnblocks_UserId]
    ON [NuDetect].[UserCardUnblocks]([UserId] ASC);

