﻿CREATE TABLE [NuDetect].[UserExemptions] (
    [UserId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_UserExemptions] PRIMARY KEY NONCLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_UserExemptions_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);

