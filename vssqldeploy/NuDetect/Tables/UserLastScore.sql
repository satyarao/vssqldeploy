﻿CREATE TABLE [NuDetect].[UserLastScore] (
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [NuDetectScore]     INT              NOT NULL,
    [NuDetectScoreBand] VARCHAR (20)     NOT NULL,
    [ScoreSignals]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_UserLastScore] PRIMARY KEY NONCLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_UserLastScore_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);

