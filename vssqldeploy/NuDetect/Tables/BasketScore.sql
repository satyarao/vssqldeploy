﻿CREATE TABLE [NuDetect].[BasketScore] (
    [BasketId]          UNIQUEIDENTIFIER NOT NULL,
    [NuDetectScore]     INT              NOT NULL,
    [NuDetectScoreBand] VARCHAR (20)     NOT NULL,
    [ScoreSignals]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_BasketScore] PRIMARY KEY NONCLUSTERED ([BasketId] ASC),
    CONSTRAINT [FK_BasketScore_Basket] FOREIGN KEY ([BasketId]) REFERENCES [dbo].[Basket] ([BasketID]) ON DELETE CASCADE
);

