﻿CREATE TABLE [NuDetect].[DeviceExemptions] (
    [DeviceId] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_DeviceId] PRIMARY KEY NONCLUSTERED ([DeviceId] ASC)
);

