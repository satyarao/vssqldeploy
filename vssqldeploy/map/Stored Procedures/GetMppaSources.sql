﻿-- =============================================
-- Author:		Dzmitry Mikhailau
-- Create date: 6/7/2016
-- Description:	Get MPPA Sources
-- =============================================
CREATE PROCEDURE [map].[GetMppaSources]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT source FROM map.MppaSources
END
