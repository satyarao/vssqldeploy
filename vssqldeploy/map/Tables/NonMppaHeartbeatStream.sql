﻿CREATE TABLE [map].[NonMppaHeartbeatStream] (
    [HeartbeatID]       INT            IDENTITY (1, 1) NOT NULL,
    [StoreID]           NVARCHAR (256) NOT NULL,
    [EventTime]         DATETIME       NOT NULL,
    [System]            NVARCHAR (256) NULL,
    [HeartbeatStatus]   NVARCHAR (256) NOT NULL,
    [Category]          NVARCHAR (256) NULL,
    [Severity]          NVARCHAR (256) NULL,
    [HeartbeatInterval] FLOAT (53)     NULL,
    CONSTRAINT [PK_HeartbeatStream] PRIMARY KEY CLUSTERED ([HeartbeatID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [nci_wi_NonMppaHeartbeatStream_0BA5C74087DF48880516]
    ON [map].[NonMppaHeartbeatStream]([StoreID] ASC, [EventTime] ASC)
    INCLUDE([HeartbeatID]) WITH (FILLFACTOR = 80);


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [map].[TriggerInsteadOfInsertHeartbeatStream] 
   ON  [map].[NonMppaHeartbeatStream] 
   INSTEAD OF INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @CompareTime DATETIME = DATEADD(minute, -30, GETDATE())

	DELETE FROM [map].[NonMppaHeartbeatStream] 
		WHERE HeartbeatID IN (
			SELECT HeartbeatID
			FROM [map].[NonMppaHeartbeatStream] 
			WHERE StoreID IN (
				SELECT StoreID from inserted WHERE inserted.StoreID IS NOT NULL
			)
			AND EventTime < @CompareTime
		)

	INSERT INTO [map].[NonMppaHeartbeatStream]
	SELECT StoreID, EventTime, System, HeartbeatStatus, Category, Severity, HeartbeatInterval
	FROM inserted 
	WHERE inserted.StoreID IS NOT NULL

	INSERT INTO [map].[StoreActivity] (StoreID)
	SELECT i.StoreID
	FROM inserted i
	WHERE i.StoreID IS NOT NULL
	ORDER BY i.EventTime ASC
END





