﻿if not exists (select * from dbo.TestTable t where t.Name = 'message 1')
begin
	insert into dbo.TestTable (Id, Name, Description) values (1, 'message 1', 'message 1')
end

if not exists (select * from dbo.TestTable t where t.Name = 'message 2')
begin
	insert into dbo.TestTable (Id, Name, Description) values (2, 'message 2', 'message 1')
end