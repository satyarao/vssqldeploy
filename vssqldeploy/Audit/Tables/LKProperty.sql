﻿CREATE TABLE [Audit].[LKProperty] (
    [AuditID]         INT            IDENTITY (1, 1) NOT NULL,
    [PropertyName]    NVARCHAR (128) NOT NULL,
    [TenantTypeID]    INT            NULL,
    [Ascend]          BIT            NOT NULL,
    [DefaultValue]    NVARCHAR (MAX) NULL,
    [PropertyComment] NVARCHAR (MAX) NULL,
    [IsActive]        BIT            NOT NULL,
    [Operation]       NVARCHAR (50)  NULL,
    [UpdatedBy]       NVARCHAR (50)  NULL,
    [UpdatedOn]       DATETIME2 (7)  CONSTRAINT [DF_AuditLKProperty_UpdatedOn] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_AuditLKProperty] PRIMARY KEY CLUSTERED ([AuditID] ASC) WITH (FILLFACTOR = 80)
);

