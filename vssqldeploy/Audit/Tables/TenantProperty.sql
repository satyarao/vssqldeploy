﻿CREATE TABLE [Audit].[TenantProperty] (
    [AuditID]       INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]      UNIQUEIDENTIFIER NOT NULL,
    [PropertyName]  NVARCHAR (128)   NOT NULL,
    [PropertyValue] NVARCHAR (250)   NOT NULL,
    [Operation]     NVARCHAR (50)    NULL,
    [UpdatedBy]     NVARCHAR (50)    NULL,
    [UpdatedOn]     DATETIME2 (7)    CONSTRAINT [DF_AuditTenantProperty_UpdatedOn] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_AuditTenantProperty] PRIMARY KEY CLUSTERED ([AuditID] ASC) WITH (FILLFACTOR = 80)
);

