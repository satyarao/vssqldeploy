﻿-- =============================================
-- Author:		Alex Goroshko
-- Create date: 08/30/2016
-- Update date: 10/17/2016
-- Description:	delete all obsolete active alert messages and sources
-- =============================================
CREATE PROCEDURE [ActiveAlerts].[ObsoleteDataCleanUp]

AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [ActiveAlerts].[ActiveAlertMessage]
	WHERE Expiration < GETUTCDATE() OR Expiration IS NULL 
	
	DELETE FROM [ActiveAlerts].[Machine]
	WHERE DATEADD(day, 30, LastReceived) < GETUTCDATE()
END
