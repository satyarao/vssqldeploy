﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 6/28/2016
-- Updated by Igor Gaidukov 8/16/2016
-- Updated by Igor Gaidukov 8/25/2016
-- Updated by Igor Gaidukov 9/08/2016
-- Updated by Igor Gaidukov 10/04/2016
-- Updated by Igor Gaidukov 10/12/2016
-- Updated by Igor Gaidukov 10/20/2016 added @TenantIDs param as ListOfGuid
-- Updated by Igor Gaidukov 10/24/2016 enhanced performance
-- Updated by Igor Gaidukov 11/15/2016 removed AvailableSources, filter alerts by MachineID
-- Updated by Igor Gaidukov 11/17/2016 added partial filter by EventCode
-- Updated by Igor Gaidukov 12/12/2016 added IgnoredEventCodeIDs to filter, added multiple filter by EventCode (deleted partial filter by EventCode)
-- Updated by Igor Gaidukov 12/13/2016 added multiple filter by MppaID
-- Updated by Igor Gaidukov 02/06/2016 added IsOwnerTenant filter
-- Description:	Get Active Alerts
-- =============================================
CREATE PROCEDURE [ActiveAlerts].[GetAlerts]
	-- Add the parameters for the stored procedure here
	@StartTime DATETIME2 = NULL,
	@EndTime DATETIME2 = NULL,
	@SystemTypeIDs ListOfInt READONLY,
	@ActiveAlertTypeIDs ListOfInt READONLY,
	@SeverityIDs ListOfInt READONLY,
	@MachineID INT = NULL,
	@MppaIDs ListOfString READONLY,
	@EventCodeIDs ListOfInt READONLY,
	@TenantIDs ListOfGuid READONLY,
	@SiteID UNIQUEIDENTIFIER = NULL,
	@SessionID NVARCHAR(MAX) = NULL,
	@P97TransactionID UNIQUEIDENTIFIER = NULL,
	@Offset INT = NULL,
	@Limit INT = NULL,
	@OrderByString NVARCHAR(MAX) = NULL,
	@IgnoredEventCodeIDs ListOfInt READONLY,
	@IsOwnerTenant BIT = NULL
AS

DECLARE @SQLString NVARCHAR(MAX);
DECLARE @ParmDefinition NVARCHAR(MAX);
DECLARE @SeverityRecordCount AS INT;
DECLARE @MachineRecordCount AS INT;
DECLARE @SystemTypeRecordCount AS INT;
DECLARE @ActiveAlertTypeRecordCount AS INT;

DECLARE @SeverityID AS TINYINT;
DECLARE @SeverityTemp VARCHAR(max);
DECLARE @MachineTemp VARCHAR(max);
DECLARE @SystemTypeID AS INT;
DECLARE @SystemTypeTemp VARCHAR(max);
DECLARE @ActiveAlertTypeID AS INT;
DECLARE @ActiveAlertTypeTemp VARCHAR(max);
DECLARE @HasFilter BIT = 0;
DECLARE @HasIdFilter BIT = 0;
DECLARE @WhereClause BIT = 0;
DECLARE @numTenants INT;
DECLARE @TenantID UNIQUEIDENTIFIER;
DECLARE @IgnoredEventCodeRecordCount AS INT;
DECLARE @IgnoredEventCodeID AS INT;
DECLARE @IgnoredEventCodeTemp VARCHAR(max);
DECLARE @EventCodeRecordCount AS INT;
DECLARE @EventCodeID AS INT;
DECLARE @EventCodeTemp VARCHAR(max);
DECLARE @numMppaIds INT;
DECLARE @MppaID VARCHAR(9);
DECLARE @MppaIdsTemp VARCHAR(max);

BEGIN

SET @SQLString = 
	N'
	WITH notifications AS (
	  SELECT *
	  FROM ActiveAlerts.ActiveAlertMessage aam ';

	IF @StartTime IS NOT NULL AND @EndTime IS NOT NULL
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 
		SET @SQLString = @SQLString + N' ( CreatedOn >= ''' + CONVERT(varchar(24), @StartTime,126) + ''' AND CreatedOn <= ''' + CONVERT(varchar(24), @EndTime,126) + ''')';

		SET @HasFilter = 1;
		SET @WhereClause = 1;
	END

	IF @MachineID IS NOT NULL
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		SET @SQLString = @SQLString + N' MachineID = '+ CONVERT(VARCHAR, @MachineID);

		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	--
	IF EXISTS (SELECT * FROM @ActiveAlertTypeIDs)
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		SELECT @ActiveAlertTypeRecordCount = COUNT(1) FROM @ActiveAlertTypeIDs;
		IF @ActiveAlertTypeRecordCount = 1
		BEGIN
			SELECT TOP 1 @ActiveAlertTypeID = ID FROM @ActiveAlertTypeIDs;
			SET @SQLString = @SQLString + N' ActiveAlertTypeID = '+ CONVERT(VARCHAR, @ActiveAlertTypeID);
        END
        ELSE IF @ActiveAlertTypeRecordCount > 1
        BEGIN
			SELECT @ActiveAlertTypeTemp = COALESCE(@ActiveAlertTypeTemp + ', ' , '') + CONVERT(VARCHAR, ID) 
				FROM @ActiveAlertTypeIDs;
			SET @SQLString = @SQLString + N' ActiveAlertTypeID IN (' + @ActiveAlertTypeTemp + ') ';
        END
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	--
	IF EXISTS (SELECT * FROM @SystemTypeIDs)
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		SELECT @SystemTypeRecordCount = COUNT(1) FROM @SystemTypeIDs;
		IF @SystemTypeRecordCount = 1
		BEGIN
			SELECT TOP 1 @SystemTypeID = ID FROM @SystemTypeIDs;
			SET @SQLString = @SQLString + N' SystemTypeID = '+ CONVERT(VARCHAR, @SystemTypeID);
        END
        ELSE IF @SystemTypeRecordCount > 1
        BEGIN
			SELECT @SystemTypeTemp = COALESCE(@SystemTypeTemp + ', ' , '') + CONVERT(VARCHAR, ID) 
				FROM @SystemTypeIDs;
			SET @SQLString = @SQLString + N' SystemTypeID IN (' + @SystemTypeTemp + ') ';
        END
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	--
	IF EXISTS (SELECT * FROM @IgnoredEventCodeIDs)
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		SELECT @IgnoredEventCodeRecordCount = COUNT(1) FROM @IgnoredEventCodeIDs;
		IF @IgnoredEventCodeRecordCount = 1
		BEGIN
			SELECT TOP 1 @IgnoredEventCodeID = ID FROM @IgnoredEventCodeIDs;
			SET @SQLString = @SQLString + N' EventCodeID != '+ CONVERT(VARCHAR, @IgnoredEventCodeID);
        END
        ELSE IF @IgnoredEventCodeRecordCount > 1
        BEGIN
			SELECT @IgnoredEventCodeTemp = COALESCE(@IgnoredEventCodeTemp + ', ' , '') + CONVERT(VARCHAR, ID) 
				FROM @IgnoredEventCodeIDs;
			SET @SQLString = @SQLString + N' EventCodeID NOT IN (' + @IgnoredEventCodeTemp + ') ';
        END
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	--
	IF EXISTS (SELECT * FROM @SeverityIDs)
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		select @SeverityRecordCount = COUNT(1) from @SeverityIDs;
		IF @SeverityRecordCount = 1
		BEGIN
			SELECT TOP 1 @SeverityID = ID FROM @SeverityIDs;
			SET @SQLString = @SQLString + N' SeverityTypeID = '+ CONVERT(VARCHAR, @SeverityID);
        END
        ELSE IF @SeverityRecordCount > 1
        BEGIN
			SELECT @SeverityTemp = COALESCE(@SeverityTemp + ', ' , '') + CONVERT(VARCHAR, ID) 
				FROM @SeverityIDs;
			SET @SQLString = @SQLString + N' SeverityTypeID IN (' + @SeverityTemp + ') ';
        END
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	--
	IF EXISTS (SELECT * FROM @EventCodeIDs)
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		select @EventCodeRecordCount = COUNT(1) from @EventCodeIDs;
		IF @EventCodeRecordCount = 1
		BEGIN
			SELECT TOP 1 @EventCodeID = ID FROM @EventCodeIDs;
			SET @SQLString = @SQLString + N' EventCodeID = '+ CONVERT(VARCHAR, @EventCodeID);
        END
        ELSE IF @EventCodeRecordCount > 1
        BEGIN
			SELECT @EventCodeTemp = COALESCE(@EventCodeTemp + ', ' , '') + CONVERT(VARCHAR, ID) 
				FROM @EventCodeIDs;
			SET @SQLString = @SQLString + N' EventCodeID IN (' + @EventCodeTemp + ') ';
        END
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	---
	IF EXISTS (SELECT * FROM @TenantIDs)
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		SELECT @numTenants = COUNT(1) FROM @TenantIDs;
		IF @numTenants = 1
		BEGIN
			SELECT TOP 1 @TenantID = ID FROM @TenantIDs;
			SET @SQLString = @SQLString + N' TenantID = ''' + convert(nvarchar(36), @TenantID)  + '''';
        END
        ELSE IF @numTenants > 1
        BEGIN
			SET @SQLString = @SQLString + N' TenantID IN (SELECT ID FROM @pTenantIDs)';
		END
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	---
	IF @SiteID IS NOT NULL 
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		SET @SQLString = @SQLString + N' StoreID = ''' + CAST(@SiteID AS VARCHAR(36)) + ''' ';
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
    END
	---
	IF @SessionID IS NOT NULL OR EXISTS (SELECT * FROM @MppaIDs) OR @P97TransactionID IS NOT NULL
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END 

		SET @SQLString = @SQLString + N' ( ';

		IF @SessionID IS NOT NULL 
		BEGIN
			SET @SQLString = @SQLString + N' SessionID = ''' + @SessionID + ''' ';
			SET @HasIdFilter = 1;
		END

		IF EXISTS (SELECT * FROM @MppaIDs)
		BEGIN
			IF @HasIdFilter = 1
			BEGIN
				SET @SQLString = @SQLString + N' OR ';
			END
			SELECT @numMppaIds = COUNT(1) FROM @MppaIDs;
			IF @numMppaIds = 1
			BEGIN
				SELECT TOP 1 @MppaID = StringValue FROM @MppaIDs;
				SET @SQLString = @SQLString + N' MppaID = ''' + @MppaID  + '''';
			END
			ELSE IF @numMppaIds > 1
			BEGIN
				SELECT @MppaIdsTemp = COALESCE(@MppaIdsTemp + ', ' , '') + '''' + StringValue + '''' 
					FROM @MppaIDs;
				SET @SQLString = @SQLString + N' MppaID IN (' + @MppaIdsTemp + ') ';
			END
			SET @HasIdFilter = 1;
		END

		IF @P97TransactionID IS NOT NULL 
		BEGIN
			IF @HasIdFilter = 1
			BEGIN
				SET @SQLString = @SQLString + N' OR ';
			END
			SET @SQLString = @SQLString + N' P97TransactionId = ''' + CAST(@P97TransactionID AS VARCHAR(36)) + ''' ';
			SET @HasIdFilter = 1;
		END

		SET @SQLString = @SQLString + N' )';
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
	END
	---
	IF @IsOwnerTenant IS NOT NULL 
	BEGIN
		IF @WhereClause = 0
		BEGIN
			SET @SQLString = @SQLString + N'WHERE '
		END 
		IF @HasFilter = 1
		BEGIN
			SET @SQLString = @SQLString + N' AND '
		END  

		SET @SQLString = @SQLString + N' ( (MppaID IS NULL) OR (MppaID IS NOT NULL AND IsOwnerTenant = ' + CONVERT(VARCHAR, @IsOwnerTenant) + ' ))';
		
		SET @HasFilter = 1;
		SET @WhereClause = 1;
	END
	
	SET @SQLString = @SQLString + N')
	    ,TempCount AS (SELECT COUNT(*) AS Total FROM notifications)
			SELECT EventCode, ActiveAlertTypeId, ActiveAlertTypeName, SystemTypeName, SystemTypeID, SeverityTypeName, SeverityTypeID, EventData, StoreID, TenantID, TenantName, P97TransactionId, SessionID, MachineName, MachineID, MppaID, CreatedOn, EventDescription, Total
			FROM notifications, TempCount';

SET @SQLString = @SQLString + N'
ORDER BY '

IF @OrderByString IS NOT NULL
BEGIN
	SET @SQLString = @SQLString + @OrderByString
END
ELSE 
BEGIN 
	SET @SQLString = @SQLString + N' [CreatedOn] DESC '
END

SET @SQLString = @SQLString + N'
OFFSET ISNULL( ' + CONVERT(VARCHAR, @Offset) +' , 0) ROWS
FETCH NEXT ISNULL( ' + CONVERT(VARCHAR, @Limit) + ', 1000) ROWS ONLY
'

DECLARE @ParamDefinition nvarchar(max);
SET @ParamDefinition = N'@pTenantIDs ListOfGuid READONLY';
EXECUTE sp_executesql @SQLString, @ParamDefinition, @pTenantIDs = @TenantIDs;

END 
