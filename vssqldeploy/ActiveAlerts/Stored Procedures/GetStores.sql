﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 8/2/2016
-- Description:	Get Stores with larger limit
-- Updated: Alex Goroshko 10/17/2016 (added @TenantIDs param as ListOfGuid)
-- Updated: Alex Goroshko 10/24/2016 (implemented CTE)
-- Updated: Alex Goroshko 10/26/2016 (added filter IsMobilePaymentsEnabled = 1)
-- Updated: Alex Goroshko 10/27/2016 (added filter IsOwnedByTenant = 1)
-- Updated: Igor Gaidukov 12/02/2016 (pass TenantIDs to sp as parameter @pTenantIDs)
-- =============================================
CREATE PROCEDURE [ActiveAlerts].[GetStores] 
	-- Add the parameters for the stored procedure here
	@TenantIDs ListOfGuid READONLY,	
	@Limit int = null,
	@Offset int = null,
	@SortingColumnName nvarchar(max) = null,
	@SortingColumnDirection nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @query NVARCHAR(MAX);	
	
	SET @query = N'WITH TempResult AS (
			SELECT Name, MppaId, StoreId, TenantId, Latitude, Longitude FROM dbo.FlatStore WHERE IsActive = 1 AND IsMobilePaymentsEnabled = 1 AND IsOwnedByTenant = 1 ';

	DECLARE @numTenants INT;
	DECLARE @TenantId UNIQUEIDENTIFIER;

	SELECT @numTenants = COUNT(*) FROM @TenantIDs;
	
	IF (@numTenants = 1)
		BEGIN
			SELECT TOP 1 @TenantId = ID FROM @TenantIDs
			SET @query = @query + N' and TenantID = ''' + CONVERT(NVARCHAR(36), @TenantId)  + '''';
		END
	ELSE IF (@numTenants > 1)
		BEGIN
			SET @query = @query + N' and TenantID IN (SELECT ID FROM @pTenantIDs)';
		END
	
	SET @query = @query + N')
	    ,TempCount AS (SELECT COUNT(*) AS Total FROM TempResult)
			SELECT Name, MppaId, StoreId, TenantId, Latitude, Longitude, Total
			FROM TempResult, TempCount';
	
	IF @SortingColumnName IS NOT NULL
		BEGIN
			SET @query = @query + N' ORDER BY ' + @SortingColumnName;

			IF @SortingColumnDirection IS NOT NULL
				BEGIN
					SET @query = @query + ' ' + @SortingColumnDirection;	
				END
			ELSE
				BEGIN
					SET @query = @query + N' ASC';
				END
		END
	ELSE
		BEGIN
			SET @query = @query + N' ORDER BY Name';
			IF @SortingColumnDirection IS NOT NULL
				BEGIN
					SET @query = @query + ' ' + @SortingColumnDirection;	
				END
			ELSE
				BEGIN
					SET @query = @query + N' ASC';
				END
		END

	set @query = @query + ' OFFSET ' + cast((isnull(@Offset, 0)) as varchar(10)) +' ROWS';
	set @query = @query + N' FETCH NEXT ' + cast((isnull(@Limit,1000)) as varchar(10)) + ' ROWS ONLY';

	DECLARE @ParamDefinition nvarchar(max);
	SET @ParamDefinition = N'@pTenantIDs ListOfGuid READONLY';
	EXECUTE sp_executesql @query, @ParamDefinition, @pTenantIDs = @TenantIDs;;
END