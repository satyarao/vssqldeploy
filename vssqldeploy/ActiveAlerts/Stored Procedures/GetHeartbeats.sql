﻿-- =============================================
-- Author:		Igor Gaidukov 
-- Create date: 11/08/2016
-- Update date: 01/06/2016 - use StoreTenants column instead TenantID/TenantName columns
-- Description:	Get filtred heartbeats (store summary)
-- =============================================
CREATE PROCEDURE [ActiveAlerts].[GetHeartbeats]
	@TenantIDs ListOfGuid READONLY,
	@MppaID NVARCHAR(MAX) = NULL,
	@Offset INT = NULL,
	@Limit INT = NULL,
	@SortColumnName NVARCHAR(50) = NULL,
	@SortDirection NVARCHAR(50) = NULL
AS
BEGIN

DECLARE @SQLString NVARCHAR(MAX);
DECLARE @ParmDefinition NVARCHAR(MAX);
DECLARE @HasFilter AS BIT = 0;
DECLARE @WhereClause AS BIT = 0;
DECLARE @TenantsCount INT;
DECLARE @TenantID UNIQUEIDENTIFIER;

SET NOCOUNT ON;
SET @SortColumnName = ISNULL(@SortColumnName, 'LogTimestamp')
SET @SortDirection = ISNULL(@SortDirection, 'desc')

SET @SQLString = 
	N' WITH heartbeats AS (
	  SELECT *
	  FROM ActiveAlerts.Heartbeat
	  CROSS APPLY OPENJSON(ActiveAlerts.Heartbeat.StoreTenants) 
							WITH (  
									TenantID UNIQUEIDENTIFIER N''$.TenantID'',   
									TenantName NVARCHAR(MAX) N''$.TenantName''
								) ';

IF EXISTS (SELECT * FROM @TenantIDs)
BEGIN
	SELECT @TenantsCount = COUNT(1) FROM @TenantIDs;
	IF @TenantsCount = 1
	BEGIN
		SELECT TOP 1 @TenantID = ID FROM @TenantIDs;
		SET @SQLString = @SQLString + N' WHERE TenantID = ''' + convert(nvarchar(36), @TenantID)  + '''';
		SET @HasFilter = 1;
    END
    ELSE IF @TenantsCount > 1
    BEGIN
		SET @SQLString = @SQLString + N' WHERE TenantID IN (SELECT ID FROM @pTenantIDs)';
		SET @HasFilter = 1;
	END
END
	
IF @MppaId IS NOT NULL 
BEGIN
	IF @HasFilter = 1
	BEGIN
		SET @SQLString = @SQLString + N' AND ';
	END
	ELSE
	BEGIN
		SET @SQLString = @SQLString + N' WHERE ';
	END
	SET @SQLString = @SQLString + N' MppaID LIKE ''%' + @MppaId + '%'' ';
	SET @HasFilter = 1;
END

SET @SQLString = @SQLString + N')
	,TempCount AS (SELECT COUNT(*) AS Total FROM heartbeats)
		SELECT MppaID, SiteID AS StoreID, TenantID, TenantName, SiteName AS StoreName, LogTimestamp, Total
		FROM heartbeats, TempCount
		ORDER BY ' + @SortColumnName;
IF @SortDirection = 'desc'
BEGIN
	SET @SQLString = @SQLString + N' DESC ';
END

SET @SQLString = @SQLString + N' OFFSET ' + CAST((ISNULL(@Offset, 0)) as varchar(10)) +' ROWS';
SET @SQLString = @SQLString + N' FETCH NEXT ' + CAST((ISNULL(@Limit,1000)) as varchar(10)) + ' ROWS ONLY';

DECLARE @ParamDefinition nvarchar(max);
SET @ParamDefinition = N'@pTenantIDs ListOfGuid READONLY';
EXECUTE sp_executesql @SQLString, @ParamDefinition, @pTenantIDs = @TenantIDs;

END 
