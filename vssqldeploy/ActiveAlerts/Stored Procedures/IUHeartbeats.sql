﻿
CREATE PROC [ActiveAlerts].[IUHeartbeats]
	@MppaID NVARCHAR(10)
  , @StoreTenants NVARCHAR(MAX)
  , @SiteID UNIQUEIDENTIFIER
  , @SiteName NVARCHAR(MAX)
  , @LogTimestamp DATETIME2(7)
AS BEGIN
SET NOCOUNT ON

MERGE [ActiveAlerts].[Heartbeat] AS target
USING
	(SELECT	@SiteID AS SiteID) AS source
ON (target.SiteID = source.SiteID)
WHEN MATCHED THEN
	UPDATE SET LogTimestamp = @LogTimeStamp
		      ,SiteName = @SiteName
			  ,StoreTenants = @StoreTenants
WHEN NOT MATCHED THEN
	INSERT ([MppaID]
			,[StoreTenants]
			,[SiteID]
			,[SiteName]
			,[LogTimestamp])
	VALUES (
			@MppaID
		  , @StoreTenants
		  , @SiteID
		  , @SiteName
		  , @LogTimestamp
		   );
END
