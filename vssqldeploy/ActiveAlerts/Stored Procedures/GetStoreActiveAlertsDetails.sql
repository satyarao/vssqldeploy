﻿-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 6/27/2016
-- Updated by Igor Gaidukou 9/08/2016 
-- Description:	Get store active alerts required acknowledge
-- =============================================
CREATE PROCEDURE [ActiveAlerts].[GetStoreActiveAlertsDetails]
	 @StoreID UNIQUEIDENTIFIER
	,@SeverityTypeIDs ListOfInt READONLY
	,@Offset INT
	,@Limit INT
AS
BEGIN
   SET NOCOUNT ON;

   WITH TempResult AS(SELECT ActiveAlertID					AS ActiveAlertID,
							 @StoreID						AS StoreID,
							 s.Name							AS StoreName,
							 s.MPPAID						AS MppaID,
						     aam.TenantID					AS TenantID,
							 aam.LogTimestamp				AS ActiveAlertTimestamp,
							 aam.EventCode					AS EventCode,
						     aas.ActiveAlertSourceName		AS SourceName,
						     aam.SeverityTypeID				AS SeverityTypeID, 
						     st.SeverityTypeName			AS SeverityTypeName,
							 aam.MachineName				AS MachineName,
							 aac.ActiveAlertCategoryName	AS Category,
							 aam.Details					AS Details,
							 aam.ExceptionDetails			AS ExceptionDetails
					FROM ActiveAlerts.ActiveAlertMessage aam
					JOIN Store s ON s.StoreID = @StoreID
					JOIN ActiveAlerts.ActiveAlertSource aas ON aas.ActiveAlertSourceID = aam.ActiveAlertSourceID
					LEFT JOIN ActiveAlerts.ActiveAlertCategory aac ON aac.ActiveAlertCategoryID = aam.ActiveAlertCategoryID
					LEFT JOIN ActiveAlerts.SeverityType st 
					ON st.SeverityTypeID = aam.SeverityTypeID
					WHERE aam.SiteID = @StoreId 
						  AND ((NOT EXISTS (SELECT 1 FROM @SeverityTypeIDs)) OR aam.SeverityTypeID IN (SELECT ID FROM @SeverityTypeIDs))
						  AND (aam.IsResolved IS NULL OR aam.IsResolved = 0) 
						  AND aam.SeverityTypeID > 2
	), TempCount AS (
	    SELECT COUNT(*) AS TotalCount FROM TempResult
	)

	SELECT *
	FROM TempResult, TempCount
    ORDER BY  [SeverityTypeID] DESC
    OFFSET @Offset ROWS 
    FETCH NEXT @Limit ROWS ONLY
END
