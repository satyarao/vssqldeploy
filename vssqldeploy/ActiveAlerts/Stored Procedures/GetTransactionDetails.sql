﻿CREATE PROCEDURE [ActiveAlerts].[GetTransactionDetails]
	@BasketID UNIQUEIDENTIFIER
AS

SELECT TOP 1 bp.PaymentCommand as PaymentCommand,
		t.TenantID as TenantId,
		t.Name as Tenant,
		s.Name as Store,
		ui.EmailAddress as UserEmail
FROM	dbo.Basket b
		JOIN dbo.BasketPayment bp ON bp.BasketID = b.BasketID
		JOIN dbo.Store s ON s.StoreID = b.StoreID
		JOIN dbo.UserInfo ui ON ui.UserID = b.UserID
		JOIN dbo.Tenant t ON t.TenantID = s.TenantID
WHERE	b.BasketID = @BasketID
ORDER BY bp.CreatedOn DESC
