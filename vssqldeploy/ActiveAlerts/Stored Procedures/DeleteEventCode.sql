﻿-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 04/26/2017
-- Description:	Delete Event Code
-- =============================================
CREATE PROCEDURE [ActiveAlerts].[DeleteEventCode]
	 @EventCodeID INT
AS
BEGIN
	SET NOCOUNT ON;

    BEGIN --Delete references to Closing codes (adjust closedBy codes)
	DECLARE @DelClosedByResultTable table (
			  EventCodeID INT
			, ClosedByCodeID INT );

	WITH DeletedClosingCode AS (
		SELECT ClosingCodeID
		FROM OPENJSON( (SELECT JiraClosingCodes FROM ActiveAlerts.EventCode WHERE EventCodeID = @EventCodeID) )
			WITH (  ClosingCodeID INT N'$.EventCodeID' ))
	, ClosedByTempTable AS (SELECT EventCodeID, ec.JiraClosedByCodes
		FROM DeletedClosingCode
		LEFT JOIN ActiveAlerts.EventCode ec ON ec.EventCodeID = ClosingCodeID)
	
	INSERT INTO @DelClosedByResultTable(EventCodeID, ClosedByCodeID)
		SELECT EventCodeID, ClosedByCodes.ClosedByCodeID
		FROM ClosedByTempTable AS tt
		CROSS APPLY OPENJSON( tt.JiraClosedByCodes )
				WITH (  ClosedByCodeID INT N'$.EventCodeID' ) AS ClosedByCodes
	UPDATE @DelClosedByResultTable
		SET ClosedByCodeID = NULL
		WHERE ClosedByCodeID = @EventCodeID

		UPDATE [ActiveAlerts].[EventCode]
			SET [JiraClosedByCodes] = (SELECT DISTINCT ClosedByCodeID AS EventCodeID FROM @DelClosedByResultTable rt WHERE rt.EventCodeID = [ActiveAlerts].[EventCode].EventCodeID AND ClosedByCodeID IS NOT NULL FOR JSON PATH)
			WHERE EventCodeID IN (SELECT EventCodeID FROM @DelClosedByResultTable)
	END --Delete references to Closing codes (adjust closedBy codes)

	--Delete references to ClosedBy codes (adjust closing codes)
	BEGIN 
		DECLARE @DelClosingResultTable table (
				  EventCodeID INT
				, ClosingCodeID INT );
		
		WITH DeletedClosedByCode AS (
		SELECT ClosedByCodeID
			FROM OPENJSON( (SELECT JiraClosedByCodes FROM ActiveAlerts.EventCode WHERE EventCodeID = @EventCodeID) )
				WITH (  ClosedByCodeID INT N'$.EventCodeID' ))
		, ClosingTempTable AS ( SELECT EventCodeID, ec.JiraClosingCodes
			FROM DeletedClosedByCode
			LEFT JOIN ActiveAlerts.EventCode ec ON ec.EventCodeID = ClosedByCodeID)

			INSERT INTO @DelClosingResultTable(EventCodeID, ClosingCodeID)
			SELECT EventCodeID, ClosingCodes.ClosingCodeID
			FROM ClosingTempTable AS tt
			CROSS APPLY OPENJSON( tt.JiraClosingCodes )
					WITH (  ClosingCodeID INT N'$.EventCodeID' ) AS ClosingCodes
		
			UPDATE @DelClosingResultTable
			SET ClosingCodeID = NULL
			WHERE ClosingCodeID = @EventCodeID
		
			UPDATE [ActiveAlerts].[EventCode]
				SET [JiraClosingCodes] = (SELECT DISTINCT ClosingCodeID AS EventCodeID FROM @DelClosingResultTable rt WHERE rt.EventCodeID = [ActiveAlerts].[EventCode].EventCodeID AND ClosingCodeID IS NOT NULL FOR JSON PATH)
				WHERE EventCodeID IN (SELECT EventCodeID FROM @DelClosingResultTable)
	END --Delete references to ClosedBy codes (adjust closing codes)

	DELETE FROM ActiveAlerts.EventCode 
	WHERE EventCodeID = @EventCodeID

END
