﻿
CREATE PROC [ActiveAlerts].[IUEventCode]
	  @EventCodeID INT = NULL
	, @EventCode NVARCHAR(128)
	, @SystemTypeID INT = NULL
	, @SeverityTypeID TINYINT
	, @Description NVARCHAR(MAX) = NULL
	, @NonAlertingStores NVARCHAR(MAX) = NULL
	, @IsIgnored BIT
	, @Destinations NVARCHAR(MAX) = NULL
	, @RetentionTimespan INT = NULL
	, @JiraIncidentThreshold INT = NULL
	, @JiraProblemThreshold INT = NULL
	, @CreateSeveralIncidentsWithinTimerange BIT
	, @AlertsCountForJiraIncident INT = NULL
	, @TimerangeForJiraIncident INT = NULL
	, @JiraClosedByCodes NVARCHAR(MAX) = NULL
	, @JiraClosingCodes NVARCHAR(MAX) = NULL
	, @ActiveAlertTypeID INT = NULL
	
AS BEGIN
SET NOCOUNT ON

--Delete references to ClosedBy codes (adjust closing codes)
BEGIN 
	DECLARE @DelClosingResultTable table (
			  EventCodeID INT
			, ClosingCodeID INT );
		
	WITH DeletedClosedByCode AS (
	SELECT ClosedByCodeID
		FROM OPENJSON( (SELECT JiraClosedByCodes FROM ActiveAlerts.EventCode WHERE EventCodeID = @EventCodeID) )
			WITH (  ClosedByCodeID INT N'$.EventCodeID' )
	EXCEPT
	SELECT ClosedByCodeID
		FROM OPENJSON( @JiraClosedByCodes )
			WITH (  ClosedByCodeID INT N'$.EventCodeID' ))
	, ClosingTempTable AS (
	SELECT EventCodeID, ec.JiraClosingCodes
		FROM DeletedClosedByCode
		LEFT JOIN ActiveAlerts.EventCode ec ON ec.EventCodeID = ClosedByCodeID)

		INSERT INTO @DelClosingResultTable(EventCodeID, ClosingCodeID)
		SELECT EventCodeID, ClosingCodes.ClosingCodeID
		FROM ClosingTempTable AS tt
		CROSS APPLY OPENJSON( tt.JiraClosingCodes )
				WITH (  ClosingCodeID INT N'$.EventCodeID' ) AS ClosingCodes
		
		UPDATE @DelClosingResultTable
		SET ClosingCodeID = NULL
		WHERE ClosingCodeID = @EventCodeID
		
		UPDATE [ActiveAlerts].[EventCode]
			SET [JiraClosingCodes] = (SELECT DISTINCT ClosingCodeID AS EventCodeID FROM @DelClosingResultTable rt WHERE rt.EventCodeID = [ActiveAlerts].[EventCode].EventCodeID AND ClosingCodeID IS NOT NULL FOR JSON PATH)
			WHERE EventCodeID IN (SELECT EventCodeID FROM @DelClosingResultTable)
END --Delete references to ClosedBy codes (adjust closing codes)
BEGIN --Delete references to Closing codes (adjust closedBy codes)
	DECLARE @DelClosedByResultTable table (
			  EventCodeID INT
			, ClosedByCodeID INT );

	WITH DeletedClosingCode AS (
		SELECT ClosingCodeID
		FROM OPENJSON( (SELECT JiraClosingCodes FROM ActiveAlerts.EventCode WHERE EventCodeID = @EventCodeID) )
			WITH (  ClosingCodeID INT N'$.EventCodeID' )
		EXCEPT
		SELECT ClosingCodeID
		FROM OPENJSON( @JiraClosingCodes )
			WITH (  ClosingCodeID INT N'$.EventCodeID' ))
	, ClosedByTempTable AS (SELECT EventCodeID, ec.JiraClosedByCodes
		FROM DeletedClosingCode
		LEFT JOIN ActiveAlerts.EventCode ec ON ec.EventCodeID = ClosingCodeID)
		
		
		INSERT INTO @DelClosedByResultTable(EventCodeID, ClosedByCodeID)
		SELECT EventCodeID, ClosedByCodes.ClosedByCodeID
		FROM ClosedByTempTable AS tt
		CROSS APPLY OPENJSON( tt.JiraClosedByCodes )
				WITH (  ClosedByCodeID INT N'$.EventCodeID' ) AS ClosedByCodes
		
		UPDATE @DelClosedByResultTable
		SET ClosedByCodeID = NULL
		WHERE ClosedByCodeID = @EventCodeID

		UPDATE [ActiveAlerts].[EventCode]
			SET [JiraClosedByCodes] = (SELECT DISTINCT ClosedByCodeID AS EventCodeID FROM @DelClosedByResultTable rt WHERE rt.EventCodeID = [ActiveAlerts].[EventCode].EventCodeID AND ClosedByCodeID IS NOT NULL FOR JSON PATH)
			WHERE EventCodeID IN (SELECT EventCodeID FROM @DelClosedByResultTable)
END --Delete references to Closing codes (adjust closedBy codes)

MERGE [ActiveAlerts].[EventCode] AS target
USING
	(SELECT	@SystemTypeID AS SystemTypeID
	, @SeverityTypeID AS SeverityTypeID
	, @Description AS EventDescription
	, @NonAlertingStores AS NonAlertingStores
	, @IsIgnored AS IsIgnored
	, @Destinations AS Destinations
	, @RetentionTimespan AS RetentionTimespan
	, @JiraIncidentThreshold AS JiraIncidentThreshold
	, @JiraProblemThreshold AS JiraProblemThreshold
	, @CreateSeveralIncidentsWithinTimerange AS CreateSeveralIncidentsWithinTimerange
	, @AlertsCountForJiraIncident AS AlertsCountForJiraIncident
	, @TimerangeForJiraIncident AS TimerangeForJiraIncident
	, @JiraClosedByCodes AS JiraClosedByCodes
	, @JiraClosingCodes AS JiraClosingCodes) AS source
ON (target.EventCodeID = @EventCodeID)

WHEN MATCHED THEN
	UPDATE SET  
	  SeverityTypeID = source.SeverityTypeID
	, [Description] = source.EventDescription
	, NonAlertingStores = source.NonAlertingStores
	, IsIgnored = source.IsIgnored
	, Destinations = source.Destinations
	, RetentionTimespan = source.RetentionTimespan
	, JiraIncidentThreshold = source.JiraIncidentThreshold
	, JiraProblemThreshold = source.JiraProblemThreshold
	, CreateSeveralIncidentsWithinTimerange = source.CreateSeveralIncidentsWithinTimerange
	, AlertsCountForJiraIncident = source.AlertsCountForJiraIncident
	, TimerangeForJiraIncident = source.TimerangeForJiraIncident
	, JiraClosedByCodes = source.JiraClosedByCodes
	, JiraClosingCodes = source.JiraClosingCodes
WHEN NOT MATCHED THEN
	INSERT ([EventCode]
           ,[SystemTypeID]
           ,[SeverityTypeID]
           ,[Description]
           ,[NonAlertingStores]
           ,[Destinations]
           ,[RetentionTimespan]
           ,[IsIgnored]
           ,[JiraIncidentThreshold]
           ,[JiraProblemThreshold]
		   ,[CreateSeveralIncidentsWithinTimerange]
		   ,[AlertsCountForJiraIncident]
		   ,[TimerangeForJiraIncident]
           ,[JiraClosedByCodes]
           ,[JiraClosingCodes]
		   ,[ActiveAlertTypeID])
     VALUES
           (@EventCode
           ,@SystemTypeID
           ,@SeverityTypeID
           ,@Description
           ,@NonAlertingStores
           ,@Destinations
           ,@RetentionTimespan
           ,@IsIgnored
           ,@JiraIncidentThreshold
           ,@JiraProblemThreshold
		   ,@CreateSeveralIncidentsWithinTimerange
		   ,@AlertsCountForJiraIncident
		   ,@TimerangeForJiraIncident
           ,@JiraClosedByCodes
           ,@JiraClosingCodes
		   ,@ActiveAlertTypeID);

	DECLARE @CurrentEventCodeID INT;
	DECLARE @ClosingTempTable table (
			  EventCodeID INT
			, ClosingCodeID INT
			, JiraClosingCodes NVARCHAR(MAX) );
	
	DECLARE @ClosedByTempTable table (
			  EventCodeID INT
			, ClosedByCodeID INT
			, JiraClosedByCodes NVARCHAR(MAX) );

	DECLARE @ClosingResultTable table (
			  EventCodeID INT
			, ClosingCodeID INT );

	DECLARE @ClosedByResultTable table (
			  EventCodeID INT
			, ClosedByCodeID INT );

	SET @CurrentEventCodeID = ISNULL(@EventCodeID, SCOPE_IDENTITY())

	--Populate JiraClosingCodes
	IF (@JiraClosedByCodes IS NOT NULL OR @JiraClosedByCodes != '[]')
	BEGIN
		WITH ClosedByTemp AS (
		SELECT ClosedByCodeID
		FROM OPENJSON( (SELECT JiraClosedByCodes FROM ActiveAlerts.EventCode WHERE EventCodeID = @CurrentEventCodeID) )
			WITH (  ClosedByCodeID INT N'$.EventCodeID' ))

		INSERT INTO @ClosingTempTable (EventCodeID, JiraClosingCodes)
		SELECT EventCodeID, ec.JiraClosingCodes
		FROM ClosedByTemp
		LEFT JOIN ActiveAlerts.EventCode ec ON ec.EventCodeID = ClosedByCodeID
		
		INSERT INTO @ClosingResultTable(EventCodeID, ClosingCodeID)
		SELECT EventCodeID, ClosingCodes.ClosingCodeID
		FROM @ClosingTempTable AS tt
		CROSS APPLY OPENJSON( tt.JiraClosingCodes )
				WITH (  ClosingCodeID INT N'$.EventCodeID' ) AS ClosingCodes
		UNION 
		SELECT EventCodeID, ClosingCodeID
		FROM @ClosingTempTable
		
		UPDATE @ClosingResultTable
		SET ClosingCodeID = @CurrentEventCodeID
		WHERE ClosingCodeID IS NULL
		
		UPDATE [ActiveAlerts].[EventCode]
			SET [JiraClosingCodes] = (SELECT DISTINCT ClosingCodeID AS EventCodeID FROM @ClosingResultTable rt WHERE rt.EventCodeID = [ActiveAlerts].[EventCode].EventCodeID FOR JSON PATH)
			WHERE EventCodeID IN (SELECT EventCodeID FROM @ClosingTempTable)
	END

	--Populate JiraClosedByCodes
	IF (@JiraClosingCodes IS NOT NULL OR @JiraClosingCodes != '[]')
	BEGIN
		WITH ClosingTemp AS (
		SELECT ClosingCodeID
		FROM OPENJSON( (SELECT JiraClosingCodes FROM ActiveAlerts.EventCode WHERE EventCodeID = @CurrentEventCodeID) )
			WITH (  ClosingCodeID INT N'$.EventCodeID' ))

		INSERT INTO @ClosedByTempTable (EventCodeID, JiraClosedByCodes)
		SELECT EventCodeID, ec.JiraClosedByCodes
		FROM ClosingTemp
		LEFT JOIN ActiveAlerts.EventCode ec ON ec.EventCodeID = ClosingCodeID
		
		INSERT INTO @ClosedByResultTable(EventCodeID, ClosedByCodeID)
		SELECT EventCodeID, ClosedByCodes.ClosedByCodeID
		FROM @ClosedByTempTable AS tt
		CROSS APPLY OPENJSON( tt.JiraClosedByCodes )
				WITH (  ClosedByCodeID INT N'$.EventCodeID' ) AS ClosedByCodes
		UNION 
		SELECT EventCodeID, ClosedByCodeID
		FROM @ClosedByTempTable
		
		UPDATE @ClosedByResultTable
		SET ClosedByCodeID = @CurrentEventCodeID
		WHERE ClosedByCodeID IS NULL

		UPDATE [ActiveAlerts].[EventCode]
			SET [JiraClosedByCodes] = (SELECT DISTINCT ClosedByCodeID AS EventCodeID FROM @ClosedByResultTable rt WHERE rt.EventCodeID = [ActiveAlerts].[EventCode].EventCodeID FOR JSON PATH)
			WHERE EventCodeID IN (SELECT EventCodeID FROM @ClosedByTempTable)
	END

	SELECT * FROM [ActiveAlerts].[EventCode] WHERE EventCodeID = @CurrentEventCodeID
END
