﻿CREATE TABLE [ActiveAlerts].[JiraIncident] (
    [JiraIncidentID]  INT            IDENTITY (1, 1) NOT NULL,
    [EventCodeID]     INT            NOT NULL,
    [TargetID]        NVARCHAR (50)  NULL,
    [IncidentJiraKey] NVARCHAR (MAX) NULL,
    [CreatedOn]       DATETIME2 (7)  NULL,
    [ProblemID]       INT            NULL,
    [IsResolved]      BIT            CONSTRAINT [JiraIncident_IsResolved_Default] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_JiraIncident] PRIMARY KEY CLUSTERED ([JiraIncidentID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_JiraIncident_EventCode] FOREIGN KEY ([EventCodeID]) REFERENCES [ActiveAlerts].[EventCode] ([EventCodeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_JiraIncident_JiraProblem] FOREIGN KEY ([ProblemID]) REFERENCES [ActiveAlerts].[JiraProblem] ([JiraProblemID])
);

