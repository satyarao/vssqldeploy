﻿CREATE TABLE [ActiveAlerts].[MaintenanceModeLog] (
    [StoreMaintenanceLogID] INT              IDENTITY (1, 1) NOT NULL,
    [TargetID]              UNIQUEIDENTIFIER NOT NULL,
    [MaintenanceModeStatus] BIT              NOT NULL,
    [CreatedOn]             DATETIME2 (7)    CONSTRAINT [DF__StoreMain__Creat__7D3748E1] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_StoreMaintenanceLog] PRIMARY KEY CLUSTERED ([StoreMaintenanceLogID] ASC) WITH (FILLFACTOR = 80)
);

