﻿CREATE TABLE [ActiveAlerts].[JiraProblem] (
    [JiraProblemID]  INT            IDENTITY (1, 1) NOT NULL,
    [EventCodeID]    INT            NOT NULL,
    [TargetID]       NVARCHAR (50)  NULL,
    [ProblemJiraKey] NVARCHAR (MAX) NULL,
    [CreatedOn]      DATETIME2 (7)  NULL,
    [IsResolved]     BIT            CONSTRAINT [IsResolved_Default] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_JiraProblem] PRIMARY KEY CLUSTERED ([JiraProblemID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_JiraProblem_EventCode] FOREIGN KEY ([EventCodeID]) REFERENCES [ActiveAlerts].[EventCode] ([EventCodeID]) ON DELETE CASCADE
);

