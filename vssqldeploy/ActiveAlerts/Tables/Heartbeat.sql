﻿CREATE TABLE [ActiveAlerts].[Heartbeat] (
    [MppaID]       VARCHAR (10)     NOT NULL,
    [SiteID]       UNIQUEIDENTIFIER NOT NULL,
    [LogTimestamp] DATETIME2 (7)    NOT NULL,
    [SiteName]     NVARCHAR (MAX)   NULL,
    [StoreTenants] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_Heartbeats_1] PRIMARY KEY CLUSTERED ([SiteID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [StoreTenants_should_be_JSON] CHECK (isjson([StoreTenants])>(0))
);


GO
CREATE NONCLUSTERED INDEX [ActiveAlertHeartbeat_MppaID]
    ON [ActiveAlerts].[Heartbeat]([MppaID] ASC) WITH (FILLFACTOR = 80);

