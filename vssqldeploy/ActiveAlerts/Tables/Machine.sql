﻿CREATE TABLE [ActiveAlerts].[Machine] (
    [MachineName]      NVARCHAR (100)   NULL,
    [MachineID]        INT              IDENTITY (1, 1) NOT NULL,
    [LastReceived]     DATETIME2 (7)    NULL,
    [TenantIDs]        NVARCHAR (MAX)   CONSTRAINT [DF_Machine_TenantIDs] DEFAULT ('[]') NOT NULL,
    [AssignedTenantID] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MachineID] PRIMARY KEY CLUSTERED ([MachineID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [Machine_TenantIDs_should_be_JSON] CHECK (isjson([TenantIDs])>(0))
);

