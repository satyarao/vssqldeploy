﻿CREATE TABLE [ActiveAlerts].[SystemType] (
    [SystemTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [SystemTypeName] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_SystemType] PRIMARY KEY CLUSTERED ([SystemTypeID] ASC) WITH (FILLFACTOR = 80)
);

