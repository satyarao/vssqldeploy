﻿CREATE TABLE [ActiveAlerts].[ActiveAlertMessage] (
    [ActiveAlertID]       INT              IDENTITY (1, 1) NOT NULL,
    [EventCode]           NVARCHAR (128)   NOT NULL,
    [ActiveAlertTypeID]   INT              NOT NULL,
    [ActiveAlertTypeName] NVARCHAR (128)   NOT NULL,
    [SystemTypeID]        INT              NOT NULL,
    [SystemTypeName]      NVARCHAR (128)   NOT NULL,
    [SeverityTypeID]      INT              NOT NULL,
    [SeverityTypeName]    NVARCHAR (64)    NOT NULL,
    [EventData]           NVARCHAR (MAX)   NULL,
    [StoreID]             UNIQUEIDENTIFIER NULL,
    [TenantID]            UNIQUEIDENTIFIER NULL,
    [TenantName]          NVARCHAR (256)   NULL,
    [P97TransactionId]    UNIQUEIDENTIFIER NULL,
    [SessionID]           NVARCHAR (128)   NULL,
    [MachineID]           INT              NULL,
    [MachineName]         NVARCHAR (128)   NULL,
    [CreatedOn]           DATETIME2 (7)    NOT NULL,
    [Expiration]          DATETIME2 (7)    NOT NULL,
    [MppaID]              NVARCHAR (50)    NULL,
    [EventDescription]    NVARCHAR (MAX)   NULL,
    [EventCodeID]         INT              NULL,
    [IsOwnerTenant]       BIT              NULL,
    CONSTRAINT [PK_ActiveAlert.ActiveAlertMessage] PRIMARY KEY NONCLUSTERED ([ActiveAlertID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [EventData_should_be_JSON] CHECK (isjson([EventData])>(0))
);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_CD027223DA8EDDF8BA01F9294901A86C]
    ON [ActiveAlerts].[ActiveAlertMessage]([MachineID] ASC, [TenantID] ASC, [CreatedOn] ASC)
    INCLUDE([ActiveAlertTypeID], [ActiveAlertTypeName], [EventCode], [EventData], [MachineName], [MppaID], [P97TransactionId], [SessionID], [SeverityTypeID], [SeverityTypeName], [StoreID], [SystemTypeID], [SystemTypeName], [TenantName]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_3CF2B0D03356E550B11872ED790D1127]
    ON [ActiveAlerts].[ActiveAlertMessage]([CreatedOn] ASC)
    INCLUDE([SeverityTypeID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_BCBE08EDFEEE3C66B0F401F9CD9A2E9F]
    ON [ActiveAlerts].[ActiveAlertMessage]([TenantID] ASC, [CreatedOn] ASC)
    INCLUDE([SeverityTypeID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_D8BAB00DF830CD57F858B325ED4744C0]
    ON [ActiveAlerts].[ActiveAlertMessage]([P97TransactionId] ASC, [CreatedOn] ASC)
    INCLUDE([ActiveAlertTypeID], [ActiveAlertTypeName], [EventCode], [EventData], [MachineID], [MachineName], [MppaID], [SessionID], [SeverityTypeID], [SeverityTypeName], [StoreID], [SystemTypeID], [SystemTypeName], [TenantID], [TenantName]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_0057CDA11B7C53965EBE4884E3EEBB92]
    ON [ActiveAlerts].[ActiveAlertMessage]([Expiration] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_67C157E9F9C76F7CD763276F36B74649]
    ON [ActiveAlerts].[ActiveAlertMessage]([MppaID] ASC, [CreatedOn] ASC)
    INCLUDE([ActiveAlertTypeID], [ActiveAlertTypeName], [EventCode], [EventData], [MachineID], [MachineName], [P97TransactionId], [SessionID], [SeverityTypeID], [SeverityTypeName], [StoreID], [SystemTypeID], [SystemTypeName], [TenantID], [TenantName]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_E52E6A6C0590EF917B12BEC603B7A787]
    ON [ActiveAlerts].[ActiveAlertMessage]([CreatedOn] ASC)
    INCLUDE([EventCodeID], [SeverityTypeID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_16C43063C56C9EF5687F079450222B4F]
    ON [ActiveAlerts].[ActiveAlertMessage]([CreatedOn] ASC)
    INCLUDE([ActiveAlertTypeID], [ActiveAlertTypeName], [EventCode], [EventData], [EventDescription], [MachineID], [MachineName], [MppaID], [P97TransactionId], [SessionID], [SeverityTypeID], [SeverityTypeName], [StoreID], [SystemTypeID], [SystemTypeName], [TenantID], [TenantName]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_6DFA6365DADF69706D5903AC5371F545]
    ON [ActiveAlerts].[ActiveAlertMessage]([CreatedOn] ASC)
    INCLUDE([IsOwnerTenant], [MppaID], [SeverityTypeID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ActiveAlertMessage_70D85EA7F00DFD91CBAA16EC94A6A00E]
    ON [ActiveAlerts].[ActiveAlertMessage]([TenantID] ASC, [CreatedOn] ASC)
    INCLUDE([IsOwnerTenant], [MppaID], [SeverityTypeID]) WITH (FILLFACTOR = 80);

