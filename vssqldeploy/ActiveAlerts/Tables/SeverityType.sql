﻿CREATE TABLE [ActiveAlerts].[SeverityType] (
    [SeverityTypeID]   TINYINT       IDENTITY (1, 1) NOT NULL,
    [SeverityTypeName] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_SeverityType] PRIMARY KEY CLUSTERED ([SeverityTypeID] ASC) WITH (FILLFACTOR = 80)
);

