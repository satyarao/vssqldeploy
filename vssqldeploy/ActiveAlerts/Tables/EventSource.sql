﻿CREATE TABLE [ActiveAlerts].[EventSource] (
    [EventSourceID]   INT            IDENTITY (1, 1) NOT NULL,
    [EventSourceName] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_EventSource] PRIMARY KEY CLUSTERED ([EventSourceID] ASC) WITH (FILLFACTOR = 80)
);

