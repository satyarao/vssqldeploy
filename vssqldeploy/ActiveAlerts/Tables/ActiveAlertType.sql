﻿CREATE TABLE [ActiveAlerts].[ActiveAlertType] (
    [ActiveAlertTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [ActiveAlertTypeName] NVARCHAR (200) NOT NULL,
    [Title]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ActiveAlertType] PRIMARY KEY CLUSTERED ([ActiveAlertTypeID] ASC) WITH (FILLFACTOR = 80)
);

