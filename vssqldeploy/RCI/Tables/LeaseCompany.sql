﻿CREATE TABLE [RCI].[LeaseCompany] (
    [LeaseCompanyID]    INT              IDENTITY (1, 1) NOT NULL,
    [LeaseTenantID]     UNIQUEIDENTIFIER NOT NULL,
    [LeaseOwnerID]      UNIQUEIDENTIFIER NOT NULL,
    [Phone]             NVARCHAR (64)    NOT NULL,
    [Fax]               NVARCHAR (64)    NULL,
    [AccountType]       NVARCHAR (64)    NOT NULL,
    [Industry]          NVARCHAR (64)    NOT NULL,
    [StreetAddress]     NVARCHAR (128)   NOT NULL,
    [City]              NVARCHAR (64)    NOT NULL,
    [State]             NVARCHAR (64)    NULL,
    [Postal]            NVARCHAR (16)    NOT NULL,
    [Country]           NVARCHAR (64)    NOT NULL,
    [CompanyID]         NVARCHAR (64)    NOT NULL,
    [CompanyContact]    NVARCHAR (64)    NULL,
    [ContactPhone]      NVARCHAR (64)    NULL,
    [ContactEmail]      NVARCHAR (64)    NULL,
    [VATNumber]         NVARCHAR (64)    NULL,
    [LinkedCompany]     NVARCHAR (64)    NULL,
    [Siret]             NVARCHAR (128)   NULL,
    [ExternalReference] NVARCHAR (64)    NULL,
    [IsActive]          BIT              NOT NULL,
    PRIMARY KEY CLUSTERED ([LeaseCompanyID] ASC),
    FOREIGN KEY ([LeaseTenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    UNIQUE NONCLUSTERED ([LeaseCompanyID] ASC),
    UNIQUE NONCLUSTERED ([LeaseTenantID] ASC)
);

