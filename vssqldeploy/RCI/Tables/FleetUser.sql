﻿CREATE TABLE [RCI].[FleetUser] (
    [FleetUserID]       INT              IDENTITY (1, 1) NOT NULL,
    [FleetTenantID]     UNIQUEIDENTIFIER NOT NULL,
    [UserID]            UNIQUEIDENTIFIER NOT NULL,
    [Title]             NVARCHAR (24)    NULL,
    [Address]           NVARCHAR (256)   NULL,
    [OfficePhoneNumber] NVARCHAR (24)    NULL,
    [Department]        NVARCHAR (256)   NULL,
    [Note]              NVARCHAR (MAX)   NULL,
    [IsActive]          BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_FleetUserID] PRIMARY KEY NONCLUSTERED ([FleetUserID] ASC),
    CONSTRAINT [FK_FleetUser_FleetTenant_FleetTenantID] FOREIGN KEY ([FleetTenantID]) REFERENCES [RCI].[FleetCompany] ([FleetTenantID]),
    CONSTRAINT [FK_FleetUser_UserInfo_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

