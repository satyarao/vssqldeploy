﻿CREATE TABLE [RCI].[FleetVehicleType] (
    [ID]   TINYINT       NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_FleetVehicleType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

