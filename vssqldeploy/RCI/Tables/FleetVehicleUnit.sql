﻿CREATE TABLE [RCI].[FleetVehicleUnit] (
    [VehicleUnitID]   INT            IDENTITY (1, 1) NOT NULL,
    [VehicleUnitName] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_VehicleUnitID] PRIMARY KEY CLUSTERED ([VehicleUnitID] ASC)
);

