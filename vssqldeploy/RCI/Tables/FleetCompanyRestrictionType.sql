﻿CREATE TABLE [RCI].[FleetCompanyRestrictionType] (
    [RestrictionTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [RestrictionTypeName] NVARCHAR (64) NOT NULL,
    CONSTRAINT [PK_RestrictionTypeID] PRIMARY KEY CLUSTERED ([RestrictionTypeID] ASC)
);

