﻿CREATE TABLE [RCI].[FleetVehicleDriver] (
    [ID]        INT              IDENTITY (1, 1) NOT NULL,
    [DriverID]  UNIQUEIDENTIFIER NOT NULL,
    [VehicleID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_FleetVehicleDriver] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_VehicleDriver_Driver] FOREIGN KEY ([DriverID]) REFERENCES [RCI].[FleetDriver] ([DriverID]),
    CONSTRAINT [FK_VehicleDriver_Vehicle] FOREIGN KEY ([VehicleID]) REFERENCES [RCI].[FleetVehicle] ([VehicleID]),
    CONSTRAINT [UQ_DriverID_VehicleID] UNIQUE NONCLUSTERED ([DriverID] ASC, [VehicleID] ASC)
);

