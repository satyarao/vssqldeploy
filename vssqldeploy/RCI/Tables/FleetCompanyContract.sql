﻿CREATE TABLE [RCI].[FleetCompanyContract] (
    [ContractID]              INT              IDENTITY (1, 1) NOT NULL,
    [FleetTenantID]           UNIQUEIDENTIFIER NOT NULL,
    [Number]                  NVARCHAR (128)   NOT NULL,
    [Name]                    NVARCHAR (128)   NOT NULL,
    [StartingDate]            DATETIME2 (7)    NULL,
    [EndDate]                 DATETIME2 (7)    NULL,
    [UseCases]                NVARCHAR (128)   NULL,
    [AuthorizedPaymentMethod] NVARCHAR (128)   NULL,
    [Commision]               NVARCHAR (128)   NULL,
    [BillingPeriod]           NVARCHAR (128)   NULL,
    [IsActive]                BIT              NOT NULL,
    CONSTRAINT [PK_ContractID] PRIMARY KEY CLUSTERED ([ContractID] ASC),
    CONSTRAINT [FK_FleetCompanyID] FOREIGN KEY ([FleetTenantID]) REFERENCES [RCI].[FleetCompany] ([FleetTenantID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_FleetCompanyContract_Name]
    ON [RCI].[FleetCompanyContract]([Name] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_FleetCompanyContract_Number]
    ON [RCI].[FleetCompanyContract]([Number] ASC);

