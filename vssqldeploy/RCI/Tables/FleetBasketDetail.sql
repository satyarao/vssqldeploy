﻿CREATE TABLE [RCI].[FleetBasketDetail] (
    [BasketID]          UNIQUEIDENTIFIER NOT NULL,
    [CustomerDriverID]  NVARCHAR (64)    NULL,
    [CustomerVehicleID] NVARCHAR (64)    NULL,
    [Odometer]          DECIMAL (18)     NULL,
    [AccountID]         NVARCHAR (64)    NULL,
    [CustomerID]        NVARCHAR (64)    NULL,
    CONSTRAINT [PK_BasketID] PRIMARY KEY CLUSTERED ([BasketID] ASC),
    FOREIGN KEY ([BasketID]) REFERENCES [dbo].[Basket] ([BasketID])
);

