﻿CREATE TABLE [RCI].[FleetVehicleUnitType] (
    [VehicleUnitTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [VehicleUnitTypeName] NVARCHAR (128) NOT NULL,
    [VehicleUnit]         INT            NOT NULL,
    CONSTRAINT [PK_VehicleUnitTypeID] PRIMARY KEY CLUSTERED ([VehicleUnitTypeID] ASC),
    CONSTRAINT [FK_VehicleUnit] FOREIGN KEY ([VehicleUnit]) REFERENCES [RCI].[FleetVehicleUnit] ([VehicleUnitID])
);

