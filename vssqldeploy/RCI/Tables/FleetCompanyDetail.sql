﻿CREATE TABLE [RCI].[FleetCompanyDetail] (
    [FleetCompanyDetailID] INT              IDENTITY (1, 1) NOT NULL,
    [FleetTenantID]        UNIQUEIDENTIFIER NOT NULL,
    [Currency]             NVARCHAR (28)    NULL,
    [UsageUnit]            NVARCHAR (28)    NULL,
    [FuelUnit]             NVARCHAR (28)    NULL,
    [FleetPaymentOnFile]   NVARCHAR (28)    NULL,
    [DriverID]             BIT              NULL,
    [Odometer]             BIT              NULL,
    [LicencePlate]         BIT              NULL,
    CONSTRAINT [PK_FleetCompanyDetailID] PRIMARY KEY CLUSTERED ([FleetCompanyDetailID] ASC),
    CONSTRAINT [FK_FleetTenantID] FOREIGN KEY ([FleetTenantID]) REFERENCES [RCI].[FleetCompany] ([FleetTenantID]),
    UNIQUE NONCLUSTERED ([FleetTenantID] ASC)
);

