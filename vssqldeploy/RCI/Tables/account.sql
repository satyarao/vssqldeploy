﻿CREATE TABLE [RCI].[account] (
    [PAN]                 UNIQUEIDENTIFIER NOT NULL,
    [OwnerId]             UNIQUEIDENTIFIER NOT NULL,
    [Limit]               DECIMAL (19, 2)  NOT NULL,
    [Balance]             DECIMAL (19, 2)  NOT NULL,
    [TotalProcessingFees] DECIMAL (19, 2)  NOT NULL,
    [PaymentDeckId]       INT              NOT NULL,
    [PeriodCloseType]     INT              NOT NULL,
    [IsAutoPay]           BIT              NOT NULL,
    [IsActive]            BIT              NOT NULL,
    [CreatedOn]           DATETIME2 (7)    CONSTRAINT [DF_ACCOUNT_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([PAN] ASC),
    CONSTRAINT [FK_Account_Account_Type] FOREIGN KEY ([PaymentDeckId]) REFERENCES [PaymentsEx].[PaymentDeck] ([ID]),
    CONSTRAINT [FK_Period_Close_Type] FOREIGN KEY ([PeriodCloseType]) REFERENCES [RCI].[period_close_type] ([id])
);

