﻿CREATE TABLE [RCI].[UserRCiPaymentSource] (
    [UserPaymentSourceID]  UNIQUEIDENTIFIER NOT NULL,
    [PaymentProviderToken] UNIQUEIDENTIFIER NOT NULL,
    [UserId]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_UserRCiPaymentSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]             BIT              NOT NULL,
    CONSTRAINT [PK_UserRCiPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC),
    CONSTRAINT [FK_UserRCiPaymentSource_account] FOREIGN KEY ([PaymentProviderToken]) REFERENCES [RCI].[account] ([PAN])
);

