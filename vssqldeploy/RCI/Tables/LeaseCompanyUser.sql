﻿CREATE TABLE [RCI].[LeaseCompanyUser] (
    [LeaseCompanyUserID] INT              IDENTITY (1, 1) NOT NULL,
    [LeaseTenantID]      UNIQUEIDENTIFIER NOT NULL,
    [UserID]             UNIQUEIDENTIFIER NOT NULL,
    [Title]              NVARCHAR (256)   NULL,
    [OfficePhoneNumber]  NVARCHAR (24)    NULL,
    [Department]         NVARCHAR (256)   NULL,
    [Note]               NVARCHAR (MAX)   NULL,
    [IsActive]           BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LeaseManangerID] PRIMARY KEY NONCLUSTERED ([LeaseCompanyUserID] ASC),
    CONSTRAINT [FK_LeaseCompanyUser_LeaseTenant_LeaseTenantID] FOREIGN KEY ([LeaseTenantID]) REFERENCES [RCI].[LeaseCompany] ([LeaseTenantID]),
    CONSTRAINT [FK_LeaseCompanyUser_UserInfo_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

