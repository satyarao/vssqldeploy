﻿CREATE TABLE [RCI].[FleetDriver] (
    [DriverID]               UNIQUEIDENTIFIER CONSTRAINT [DF_DriverID] DEFAULT (newid()) NOT NULL,
    [UserID]                 UNIQUEIDENTIFIER NOT NULL,
    [TenantID]               UNIQUEIDENTIFIER NOT NULL,
    [StartDate]              DATETIME2 (7)    NULL,
    [EndDate]                DATETIME2 (7)    NULL,
    [Department]             NVARCHAR (MAX)   NULL,
    [PostalCode]             NVARCHAR (16)    NULL,
    [StateCode]              NVARCHAR (2)     NULL,
    [CreatedBy]              NVARCHAR (255)   NULL,
    [CreatedOn]              DATETIME2 (7)    CONSTRAINT [DF_Driver_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]              NVARCHAR (255)   NULL,
    [UpdatedOn]              DATETIME2 (7)    NULL,
    [IsActive]               BIT              CONSTRAINT [DF_Driver_IsActive] DEFAULT ((1)) NOT NULL,
    [CustomerDriverID]       NVARCHAR (64)    NULL,
    [EmployeeID]             NVARCHAR (64)    NULL,
    [PaymentSourceId]        UNIQUEIDENTIFIER NULL,
    [DriverCategoryID]       INT              NOT NULL,
    [FuelUsageBalance]       INT              DEFAULT ((0)) NOT NULL,
    [ParkingOnUsageBalance]  INT              DEFAULT ((0)) NOT NULL,
    [ParkingOffUsageBalance] INT              DEFAULT ((0)) NOT NULL,
    [CarWashUsageBalance]    INT              DEFAULT ((0)) NOT NULL,
    [EvChargingUsageBalance] INT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FleetDriver] PRIMARY KEY CLUSTERED ([DriverID] ASC),
    CONSTRAINT [FK_Driver_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_Driver_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Tenant_User]
    ON [RCI].[FleetDriver]([TenantID] ASC, [UserID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UserID_TenantID]
    ON [RCI].[FleetDriver]([UserID] ASC, [TenantID] ASC);

