﻿CREATE TABLE [RCI].[FleetVehicleStatus] (
    [ID]   TINYINT       NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_FleetVehicleStatus] PRIMARY KEY CLUSTERED ([ID] ASC)
);

