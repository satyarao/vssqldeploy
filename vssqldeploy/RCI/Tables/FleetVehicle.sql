﻿CREATE TABLE [RCI].[FleetVehicle] (
    [VehicleID]             UNIQUEIDENTIFIER CONSTRAINT [DF_VehicleID] DEFAULT (newid()) NOT NULL,
    [TenantID]              UNIQUEIDENTIFIER NOT NULL,
    [Name]                  NVARCHAR (255)   NOT NULL,
    [VIN]                   NVARCHAR (255)   NOT NULL,
    [StateLicensedIssuedBy] NVARCHAR (255)   NULL,
    [StateCode]             CHAR (2)         NULL,
    [LicensePlateNumber]    NVARCHAR (255)   NOT NULL,
    [VehicleTypeID]         TINYINT          NOT NULL,
    [StatusID]              TINYINT          NOT NULL,
    [OdometerReading]       DECIMAL (9, 2)   NULL,
    [Year]                  CHAR (4)         NULL,
    [Make]                  NVARCHAR (255)   NULL,
    [Model]                 NVARCHAR (255)   NULL,
    [FuelCapacity]          INT              NULL,
    [CompatibleFuels]       NVARCHAR (MAX)   CONSTRAINT [DF_Vehicle_CompatibleFuels] DEFAULT ('[]') NULL,
    [CreatedBy]             NVARCHAR (255)   NULL,
    [CreatedOn]             DATETIME2 (7)    CONSTRAINT [DF_Vehicle_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]             NVARCHAR (255)   NULL,
    [UpdatedOn]             DATETIME2 (7)    NULL,
    [IsActive]              BIT              CONSTRAINT [DF_Vehicle_IsActive] DEFAULT ((1)) NOT NULL,
    [CustomerVehicleID]     NVARCHAR (64)    NULL,
    CONSTRAINT [PK_FleetVehicle] PRIMARY KEY CLUSTERED ([VehicleID] ASC),
    CONSTRAINT [CK_Vehicle_CompatibleFuels_IsJson] CHECK (isjson([CompatibleFuels])>(0)),
    CONSTRAINT [FK_Vehicle_Status] FOREIGN KEY ([StatusID]) REFERENCES [RCI].[FleetVehicleStatus] ([ID]),
    CONSTRAINT [FK_Vehicle_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_Vehicle_Type] FOREIGN KEY ([VehicleTypeID]) REFERENCES [RCI].[FleetVehicleType] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Tenant_VIN]
    ON [RCI].[FleetVehicle]([TenantID] ASC, [VIN] ASC) WHERE ([IsActive]=(1));


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Tenant_CustomerVehicleID]
    ON [RCI].[FleetVehicle]([TenantID] ASC, [CustomerVehicleID] ASC) WHERE ([IsActive]=(1) AND [CustomerVehicleID] IS NOT NULL);

