﻿
--VL - UPDATED 12/19 NOT IN REPO YET
CREATE PROCEDURE [RCi].[sp_get_wallet]
@UserID uniqueidentifier

AS
	DECLARE @results TABLE (USER_PAYMENT_SOURCE_ID uniqueidentifier, LS_USER_PAYMENT_SOURCE_ID int, PROVIDER_ID int, PAYMENT_PROVIDER_TOKEN uniqueidentifier, OWNER_ID uniqueidentifier )

	BEGIN
	INSERT INTO @results ( USER_PAYMENT_SOURCE_ID, LS_USER_PAYMENT_SOURCE_ID, PROVIDER_ID, PAYMENT_PROVIDER_TOKEN, OWNER_ID )
	SELECT usr.[ID], usr.[LS_UserPaymentSourceID], usr.[ProviderID], rci.[PaymentProviderToken], act.[PAN]
	FROM [RCi].[UserRCiPaymentSource] as rci INNER JOIN [PaymentsEx].[LKUserPaymentSource] as usr ON rci.[UserPaymentSourceID] = usr.[ID] 
	INNER JOIN [RCi].[account] as act ON rci.[PaymentProviderToken] = act.[PAN]
	WHERE usr.UserID = @UserID AND usr.ProviderId = 901

	SELECT USER_PAYMENT_SOURCE_ID, LS_USER_PAYMENT_SOURCE_ID, PROVIDER_ID, OWNER_ID
			FROM @results
	END
