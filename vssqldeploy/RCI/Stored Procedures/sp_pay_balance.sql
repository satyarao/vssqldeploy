﻿
--******(TH) New Procedure (NOT IN REPO YET). 
CREATE PROCEDURE [RCi].[sp_pay_balance]
@PAN [uniqueidentifier]

AS
	BEGIN
		BEGIN TRY
			BEGIN TRAN vcard_pay_balance

			DECLARE @STATUS INT = 100
			DECLARE @LATEST_PERIOD_END datetime2

			DECLARE @FINAL_BALANCE         decimal(19,2)
			DECLARE @FINAL_CONV_BALANCE    decimal(19,2)
			DECLARE @TOTAL_FINAL_BALANCE   decimal(19,2)

			DECLARE @PAYMENT_BALANCE       decimal(19,2)
			DECLARE @PAYMENT_CONV_BALANCE  decimal(19,2)
			DECLARE @TOTAL_PAYMENT_BALANCE decimal(19,2)
		
			SELECT @LATEST_PERIOD_END = MAX(PeriodEnd)
			FROM [RCi].[payment_period_summary]
			WHERE [PAN] = @PAN

			SELECT @FINAL_BALANCE = [FinalBalance], @PAYMENT_BALANCE = [PaymentBalance], @FINAL_CONV_BALANCE = [FinalProcessingFeeBalance], @PAYMENT_CONV_BALANCE = [PaymentProcessingFeeBalance]
			FROM [RCi].[payment_period_summary]
			WHERE [PAN] = @PAN AND [PeriodEnd] = @LATEST_PERIOD_END

			SET @TOTAL_FINAL_BALANCE = @FINAL_BALANCE + @FINAL_CONV_BALANCE
			SET @TOTAL_PAYMENT_BALANCE = @PAYMENT_BALANCE + @PAYMENT_CONV_BALANCE

			IF( ( @TOTAL_FINAL_BALANCE - @TOTAL_PAYMENT_BALANCE ) != 0 )
				BEGIN					
					INSERT INTO [RCi].[account_activity]([refnumber],[p97txid],[pan],[amount],[activitytype])
					VALUES( NEWID(), NEWID(), @PAN, @FINAL_BALANCE, 9 )

					UPDATE [RCi].[payment_period_summary]
					SET [PaymentBalance] = @FINAL_BALANCE
					WHERE [PAN] = @PAN AND [PeriodEnd] = @LATEST_PERIOD_END
				
					INSERT INTO [RCi].[account_activity]([refnumber],[p97txid],[pan],[amount],[activitytype])
					VALUES( NEWID(), NEWID(), @PAN, @FINAL_CONV_BALANCE, 10 )

					UPDATE [RCi].[payment_period_summary]
					SET [PaymentProcessingFeeBalance] = @FINAL_CONV_BALANCE
					WHERE [PAN] = @PAN AND [PeriodEnd] = @LATEST_PERIOD_END						
				END
			ELSE
				SET @STATUS = 102 --Balance is already paid.

			COMMIT TRAN vcard_pay_balance
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION vcard_pay_balance
			SET @STATUS = 101
		END CATCH
			

		SELECT @STATUS
	END
