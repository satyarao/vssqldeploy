﻿

CREATE PROCEDURE [RCi].[sp_auth] 
   @PAN           UNIQUEIDENTIFIER, 
   @AMOUNT        DECIMAL(19, 2), 
   @PERIOD        NVARCHAR(50), 
   @MERCHANTID    NVARCHAR(50), 
   @P97TXID       UNIQUEIDENTIFIER,
   @SEQUENCEID    NVARCHAR(50),
   @PARTIAL_AUTH  BIT = 0 
AS 
   BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from  
      -- interfering with SELECT statements.  
      SET nocount ON; 

      IF @PAN IS NULL 
        RAISERROR('PAN CANNOT BE NULL',1,1); 

      IF @AMOUNT IS NULL 
        RAISERROR('AMOUNT CANNOT BE NULL',1,1); 

      IF @MERCHANTID IS NULL 
        RAISERROR('MERCHANTID CANNOT BE NULL',1,1); 

      DECLARE @AUTH_AMOUNT DECIMAL(19, 2) = 0 
      DECLARE @BALANCE DECIMAL(19, 2) 
	  DECLARE @LIMIT DECIMAL(19,2)
	  DECLARE @AVAIL_BALANCE DECIMAL(19,2)
      DECLARE @REF_NUM UNIQUEIDENTIFIER 
      DECLARE @STATUS INT = -1 -- Assume it failed  

      BEGIN TRAN auth; 

      BEGIN try 
          SELECT @BALANCE = x.[balance], @LIMIT = x.[limit]
          FROM   [RCi].[account] x 
          WHERE  @PAN = [x].[pan]
                
          SET @AVAIL_BALANCE = @LIMIT - @BALANCE

		  IF( @AVAIL_BALANCE = 0)
		     BEGIN
		         SET @STATUS = 99	   --zero balance status
			     SET @AMOUNT = 0
		     END
		  ELSE IF( @AMOUNT > @AVAIL_BALANCE ) 
             IF( @PARTIAL_AUTH = 1 ) 
		         BEGIN
                     SET @AUTH_AMOUNT = @AVAIL_BALANCE; 
		             SET @STATUS = 2;  --partial authorized
		         END
             ELSE 
                 SET @STATUS = 3;      --No Partial Auth Allowed  
          ELSE 
		     BEGIN
                 SET @AUTH_AMOUNT = @AMOUNT; 
			     SET @STATUS = 1	   --fully authorized
			 END

          IF ( @AUTH_AMOUNT > 0 ) 
             BEGIN 
                 SET @REF_NUM = Newid(); 

                 -- Create Activity Record  
                 INSERT INTO [RCi].[account_activity] ([refnumber],[pan],[period],[amount], [p97txid],[merchantid],[sequenceid],[activitytype]) 
                      VALUES (@REF_NUM, 
                              @PAN, 
                              @PERIOD, 
                              @AUTH_AMOUNT,
                              @P97TXID,
                              @MERCHANTID, 
				 			  @SEQUENCEID,
                              1); -- AUTH_HOLD                               
				 
                 -- Update Account Balance  
                 EXEC [RCi].[sp_update_balance] @PAN, @AUTH_AMOUNT, 1 -- DEBIT 
            END 

          COMMIT TRAN auth; 
      END try 

      BEGIN catch 
          ROLLBACK TRANSACTION auth 
		  SET @STATUS =-1;
		  SET @AUTH_AMOUNT = 0;
		  SET @REF_NUM = NULL;
      END catch 
	  
      SELECT @STATUS, 
             @REF_NUM, 
             @AUTH_AMOUNT 
  END 

