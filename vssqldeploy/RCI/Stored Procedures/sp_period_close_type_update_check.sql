﻿


--This sproc will update the monthly period summary if the account has
--recently had a period close type change from BiMonthly to Monthly.
CREATE PROCEDURE [RCi].[sp_period_close_type_update_check]
@PAN uniqueidentifier,
@MONTHLY_PERIOD_START datetime2,
@MONTHLY_PERIOD_END datetime2,
@BIMONTHLY_PERIOD_END datetime2

AS
	BEGIN
		DECLARE @STATUS INT = 1
		DECLARE @BI_MONTHLY_SUMMARY_PAYMENT_AMOUNT DECIMAL(19,2)
		DECLARE @BI_MONTHLY_SUMMARY_CONV_PAYMENT_AMOUNT DECIMAL(19,2)

		BEGIN TRY
			IF( ( SELECT DAY( @MONTHLY_PERIOD_END ) ) = 1 AND 
				( SELECT DAY( @BIMONTHLY_PERIOD_END ) ) = 16 AND
				( SELECT MONTH( @MONTHLY_PERIOD_START ) ) = ( SELECT MONTH( @BIMONTHLY_PERIOD_END ) ) AND
				( SELECT YEAR( @MONTHLY_PERIOD_START ) ) = ( SELECT YEAR( @BIMONTHLY_PERIOD_END ) ) AND
				( SELECT [PeriodCloseType] FROM [RCi].[account] WHERE [PAN] = @PAN ) = 1 )
			BEGIN		
				SELECT @BI_MONTHLY_SUMMARY_PAYMENT_AMOUNT = [PaymentBalance], @BI_MONTHLY_SUMMARY_CONV_PAYMENT_AMOUNT = [PaymentProcessingFeeBalance]
				FROM [RCi].[payment_period_summary] 
				WHERE [PeriodEnd] = @BIMONTHLY_PERIOD_END AND [PAN] = @PAN

				IF @BI_MONTHLY_SUMMARY_PAYMENT_AMOUNT IS NOT NULL AND @BI_MONTHLY_SUMMARY_CONV_PAYMENT_AMOUNT IS NOT NULL
					UPDATE [RCi].[payment_period_summary]
					SET [FinalBalance] = [FinalBalance] - @BI_MONTHLY_SUMMARY_PAYMENT_AMOUNT, 
						[FinalProcessingFeeBalance] = [FinalProcessingFeeBalance] - @BI_MONTHLY_SUMMARY_CONV_PAYMENT_AMOUNT
					WHERE [PeriodStart] = @MONTHLY_PERIOD_START AND [PeriodEnd] = @MONTHLY_PERIOD_END AND [PAN] = @PAN
			END
		END TRY
		BEGIN CATCH
			SET @STATUS = -1
		END CATCH

		SELECT @STATUS
	END
