﻿
--******(TH) New Procedure (NOT IN REPO YET). 
CREATE PROCEDURE [RCi].[sp_get_account_activity_entries]
@PAN uniqueidentifier,
@PERIOD_START datetime2,
@PERIOD_END datetime2
AS
	BEGIN
		SELECT [refnumber],[p97txid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp]
		FROM [RCi].[account_activity]
		WHERE 
			[pan] = @PAN AND 
			[timestamp] >= @PERIOD_START AND 
			[timestamp] < @PERIOD_END			
	END
