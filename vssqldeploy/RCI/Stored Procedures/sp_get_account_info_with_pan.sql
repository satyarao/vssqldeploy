﻿
--******(TH) New Procedure (NOT IN REPO YET).
CREATE PROCEDURE [RCi].[sp_get_account_info_with_pan]
@OwnerId [UNIQUEIDENTIFIER],
@PAN     [UNIQUEIDENTIFIER]

AS
	BEGIN
		SELECT [PAN],[Limit],[Balance],[PaymentDeckID],[IsActive] 
		FROM   [RCi].[account] 
		WHERE  [OwnerId] = @OwnerId AND [PAN] = @PAN
	END
