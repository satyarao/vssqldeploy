﻿



/************************************************************************* 
 TEST SPROCS 
*************************************************************************/ 
CREATE PROCEDURE [RCi].[sp_test_populate_activity_tables]
@REF_NUM0 uniqueidentifier,
@REF_NUM1 uniqueidentifier,
@REF_NUM2 uniqueidentifier,
@REF_NUM3 uniqueidentifier,
@REF_NUM4 uniqueidentifier,
@P97TXN_ID uniqueidentifier,
@PAN	  uniqueidentifier,
@MERCHANT_ID uniqueidentifier,
@SEQUENCE_ID NVARCHAR(50),
@PERIOD NVARCHAR(50),
@AUTH_AMT [DECIMAL](19,2),
@CAPTURE_AMT [DECIMAL](19,2),
@REVERSAL_AMT [DECIMAL](19,2),
@CONV_FEE_AMT [DECIMAL](19,2),
@CONV_FEE_REVERSAL_AMT [DECIMAL](19,2),
@TIME_STAMP datetime2


AS
	BEGIN
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM0, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM0, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 2, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM0, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CAPTURE_AMT, 3, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM0, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @REVERSAL_AMT, 4, @TIME_STAMP );		

		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
            VALUES( @REF_NUM0, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CONV_FEE_AMT, 5, @TIME_STAMP ); 
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
            VALUES( @REF_NUM0, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CONV_FEE_REVERSAL_AMT, 6, @TIME_STAMP ); 

		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM1, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );

		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM2, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM2, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 2, @TIME_STAMP );

		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM3, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM3, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 2, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM3, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CAPTURE_AMT, 3, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
            VALUES( @REF_NUM3, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CONV_FEE_AMT, 5, @TIME_STAMP ); 

		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM4, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM4, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @REVERSAL_AMT, 4, @TIME_STAMP );
		INSERT INTO [RCi].[account_activity] ([refnumber],[p97txid],[pan],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
            VALUES( @REF_NUM4, @P97TXN_ID, @PAN, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CONV_FEE_REVERSAL_AMT, 6, @TIME_STAMP ); 
	END
