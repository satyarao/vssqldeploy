﻿

--THE PANS SENT TO THIS SPROC MUST ORIGINATE FROM THE GET_VALID_PANS SPROC ABOVE:
CREATE PROCEDURE [RCi].[sp_account_period_close]
@PAN				   [uniqueidentifier],
@PERIOD_START          [datetime2],
@PERIOD_END            [datetime2]

AS
	BEGIN
		DECLARE @STATUS				   INT = 1

		DECLARE @SUMMARY_TOTAL_BALANCE DECIMAL(19,2)              
		DECLARE @SUMMARY_CONV_TOTAL    DECIMAL(19,2)              
										              
		DECLARE @TOTAL_CAPTURE_AMOUNT           DECIMAL(19,2)              
		DECLARE @TOTAL_REVERSAL_AMOUNT          DECIMAL(19,2)              
		DECLARE @TOTAL_CONV_FEE_CAPTURE_AMOUNT  DECIMAL(19,2)         
		DECLARE @TOTAL_CONV_FEE_REVERSAL_AMOUNT DECIMAL(19,2)
										              
		DECLARE @STARTING_BALANCE      DECIMAL(19,2)		      
		DECLARE @STARTING_CONV_BALANCE DECIMAL(19,2)	          

		BEGIN TRY
			SELECT @TOTAL_CAPTURE_AMOUNT = SUM([amount])
			FROM [RCi].[account_activity]
			WHERE 
				[PAN] = @PAN AND
				[activitytype] = 3 AND 
				[timestamp] >= @PERIOD_START AND
				[timestamp] < @PERIOD_END

			IF( @TOTAL_CAPTURE_AMOUNT IS NULL )
				SET @TOTAL_CAPTURE_AMOUNT = 0

			SELECT @TOTAL_REVERSAL_AMOUNT = SUM([amount])
			FROM [RCi].[account_activity]
			WHERE 
				[PAN] = @PAN AND
				[activitytype] = 4 AND 
				[timestamp] >= @PERIOD_START AND
				[timestamp] < @PERIOD_END

			IF( @TOTAL_REVERSAL_AMOUNT IS NULL )
				SET @TOTAL_REVERSAL_AMOUNT = 0

			SELECT @STARTING_BALANCE = ( [FinalBalance] - [PaymentBalance] )
			FROM [RCi].[payment_period_summary]
			WHERE [PAN] = @PAN AND [PeriodEnd] = @PERIOD_START

			IF( @STARTING_BALANCE IS NULL )
				SET @STARTING_BALANCE = 0






			SELECT @TOTAL_CONV_FEE_CAPTURE_AMOUNT = SUM([amount])
			FROM [RCi].[account_activity]
			WHERE 
				[PAN] = @PAN AND
				[activitytype] = 5 AND --OR [activitytype] = 6) AND
				[timestamp] >= @PERIOD_START AND
				[timestamp] < @PERIOD_END

			IF( @TOTAL_CONV_FEE_CAPTURE_AMOUNT IS NULL )
				SET @TOTAL_CONV_FEE_CAPTURE_AMOUNT = 0

			SELECT @TOTAL_CONV_FEE_REVERSAL_AMOUNT = SUM([amount])
			FROM [RCi].[account_activity]
			WHERE
				[PAN] = @PAN AND
				[activitytype] = 6 AND
				[timestamp] >= @PERIOD_START AND
				[timestamp] < @PERIOD_END

			IF( @TOTAL_CONV_FEE_REVERSAL_AMOUNT IS NULL )
				SET @TOTAL_CONV_FEE_REVERSAL_AMOUNT = 0

			SELECT @STARTING_CONV_BALANCE = ( [FinalProcessingFeeBalance] - [PaymentProcessingFeeBalance] )
			FROM [RCi].[payment_period_summary]
			WHERE [PAN] = @PAN AND [PeriodEnd] = @PERIOD_START

			IF( @STARTING_CONV_BALANCE IS NULL )
				SET @STARTING_CONV_BALANCE = 0

			SET @SUMMARY_TOTAL_BALANCE = ( ( @TOTAL_CAPTURE_AMOUNT - @TOTAL_REVERSAL_AMOUNT ) + @STARTING_BALANCE )
			SET @SUMMARY_CONV_TOTAL = ( ( @TOTAL_CONV_FEE_CAPTURE_AMOUNT - @TOTAL_CONV_FEE_REVERSAL_AMOUNT ) + @STARTING_CONV_BALANCE )

			BEGIN TRAN vcard_account_period_close

			INSERT INTO [RCi].[payment_period_summary]([Id],[PAN],[StartBalance],[StartProcessingFeeBalance],[FinalBalance],[FinalProcessingFeeBalance],[PaymentBalance],[PaymentProcessingFeeBalance],[PeriodStart],[PeriodEnd])
				VALUES(NEWID( ), @PAN,@STARTING_BALANCE,@STARTING_CONV_BALANCE,@SUMMARY_TOTAL_BALANCE,@SUMMARY_CONV_TOTAL,0,0,@PERIOD_START,@PERIOD_END);

			UPDATE [RCi].[account]
			SET [Balance] = 0, [TotalProcessingFees] = 0
			WHERE [PAN] = @PAN

			COMMIT TRAN vcard_account_period_close
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN vcard_account_period_close
			SET @STATUS = -1
		END CATCH

		SELECT @STATUS
	END
