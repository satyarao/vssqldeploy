﻿-- =============================================
-- Author:		Davydovich Raman
-- Create date: 13/12/2018
-- Description:	Get Lease Company Users
-- 08/06/2019:	Davydovich Raman - Remove MR.Claims from procedure
-- 10/08/2019: Vadim Prikich - Remove MR.UserAccounts from procedure
-- =============================================

CREATE PROC [RCI].[GetLeaseCompanyUsers]
	 @LeaseTenantID UNIQUEIDENTIFIER
	,@UserIDs ListOfGuid READONLY 
	,@SearchTerm NVARCHAR(255) = NULL
	,@FirstName NVARCHAR(255) = NULL
	,@LastName NVARCHAR(255) = NULL
	,@IsActive BIT = NULL
	,@UserGroupID INT = NULL
	,@Offset INT
	,@Limit INT

AS
BEGIN
WITH TempResult (UserID, MobilePhoneNumber, FirstName, LastName, Title, OfficePhoneNumber, IsActive, Department, Note) AS 
(
	SELECT ui.UserID, MobileNumber, ui.FirstName, ui.LastName, lcu.Title, lcu.OfficePhoneNumber, lcu.IsActive, lcu.Department, lcu.Note		
	FROM [dbo].[UserInfo] ui
	JOIN [LeaseCompanyUser] lcu ON lcu.UserID = ui.UserID AND lcu.LeaseTenantID = @LeaseTenantID
	LEFT JOIN [AccessControl].[UserRestrictedTenantAssignment] urga ON urga.UserID = ui.UserID
	WHERE urga.[RestrictedTenantID] = @LeaseTenantID
		AND ui.IsDeleted = 0 
		AND (ui.UserID IN (SELECT ID FROM @UserIDs) OR (NOT EXISTS (SELECT * FROM @UserIDs)))
		AND (@FirstName IS NULL OR ui.FirstName = @FirstName)
		AND (@LastName IS NULL OR ui.LastName = @LastName)
		AND (@UserGroupID IS NULL OR EXISTS(SELECT 1 FROM [AccessControl].[UserGroupAssignment] uga WHERE uga.UserID = ui.UserID  AND UserGroupID = @UserGroupID))						
		AND (@IsActive IS NULL OR lcu.IsActive = @IsActive)
),
	TempCount AS (SELECT COUNT(*) AS TotalRows FROM TempResult), 
	ReturnValues AS (
		SELECT  tr.UserID, 
			MobilePhoneNumber, 
			OfficePhoneNumber, 
			FirstName, 
			LastName, 
			Title,
			Note,
			tr.IsActive,
			IIF (uga.UserGroupID IS NULL, NULL, uga.UserGroupID) AS UserGroupID, 
			(SELECT TOP(1) UsergroupName FROM [AccessControl].[UserGroup] ug WHERE  ug.UserGroupID = uga.UserGroupID ) AS UsergroupName,
			Department, 
			(SELECT TOP(1) TotalRows FROM TempCount) AS TotalRows
		FROM TempResult tr
		LEFT JOIN [AccessControl].[UserGroupAssignment] uga ON uga.UserID = tr.UserID
			AND EXISTS (SELECT 1 FROM [AccessControl].[UserGroup] ug WHERE ug.UserGroupID = uga.UserGroupID
			AND ug.UserGroupName IN ('Lease Company Admin', 'Lease Company Manager') 
			AND ug.TenantID = @LeaseTenantID)
	)
	SELECT * FROM ReturnValues rv
	WHERE @SearchTerm IS NULL 
		OR rv.UserID IN (SELECT ID FROM @UserIDs)
		OR rv.FirstName LIKE '%' + @SearchTerm + '%'
		OR rv.LastName LIKE '%' + @SearchTerm + '%'
		OR rv.UsergroupName LIKE '%' + @SearchTerm + '%'					 
	ORDER BY UserID
	OFFSET @Offset ROWS
	FETCH NEXT @Limit ROWS ONLY;
END
