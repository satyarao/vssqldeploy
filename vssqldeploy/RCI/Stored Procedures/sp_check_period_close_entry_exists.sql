﻿
CREATE PROCEDURE [RCi].[sp_check_period_close_entry_exists]
@PAN				   [uniqueidentifier],
@PERIOD_START          [datetime2],
@PERIOD_END            [datetime2]

AS
	BEGIN
		DECLARE @exists BIT = 0
		IF EXISTS ( SELECT * FROM [RCi].[payment_period_summary] WHERE [PAN] = @PAN AND [PeriodStart] = @PERIOD_START AND [PeriodEnd] = @PERIOD_END )
			SET @exists = 1
		SELECT @exists
	END
