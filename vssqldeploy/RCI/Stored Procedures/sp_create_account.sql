﻿
--******(TH) Updated Procedure (NOT IN REPO YET)
CREATE PROCEDURE [RCi].[sp_create_account]
 @OWNER_ID UNIQUEIDENTIFIER,
 @LIMIT DECIMAL(19,2),
 @MOP INT,
 @PERIOD_CLOSE_ID INT,
 @IS_AUTO_PAY BIT,
 @DEFAULT_PERIOD_START datetime2,
 @DEFAULT_PERIOD_END datetime2

 AS
 BEGIN
   DECLARE @PAN UNIQUEIDENTIFIER
   DECLARE @RESULT INT = -1

   BEGIN TRANSACTION create_acct
   BEGIN TRY
	   IF NOT EXISTS(SELECT * FROM [RCi].[account] WHERE [OWNERID] = @OWNER_ID)
	   BEGIN
		   SET @PAN = NEWID()
		
		   INSERT INTO [RCi].[account]([PAN],[OWNERID],[LIMIT],[BALANCE],[TotalProcessingFees],[PaymentDeckId],[PeriodCloseType],[IsAutoPay],[IsActive],[CreatedOn])
		   VALUES( @PAN, @OWNER_ID, @LIMIT, 0, 0, @MOP, @PERIOD_CLOSE_ID, @IS_AUTO_PAY, 1, GETDATE() )   

		   INSERT INTO [RCi].[payment_period_summary]([Id],[PAN],[StartBalance],[StartProcessingFeeBalance],[FinalBalance],[FinalProcessingFeeBalance],[PaymentBalance],[PaymentProcessingFeeBalance],[PeriodStart],[PeriodEnd])
		   VALUES( NEWID(), @PAN, 0, 0, 0, 0, 0, 0, @DEFAULT_PERIOD_START, @DEFAULT_PERIOD_END )

		   COMMIT TRANSACTION create_acct
		   SET @RESULT = 1
	   END
   END TRY

   BEGIN CATCH
    ROLLBACK TRANSACTION create_acct
   END CATCH
   
   SELECT @RESULT, @PAN --return the PAN
 END
