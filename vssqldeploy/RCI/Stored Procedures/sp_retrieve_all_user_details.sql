﻿
CREATE PROCEDURE [RCi].[sp_retrieve_all_user_details]
@USER_ID uniqueidentifier

AS
	BEGIN
		SELECT acct.[OwnerId], acct.[Limit], usr.[UserPaymentSourceID]
		FROM [RCi].[UserRCiPaymentSource] usr
			INNER JOIN [RCi].[account] acct 
			ON usr.[PaymentProviderToken] = acct.[PAN]
		WHERE usr.[UserId] = @USER_ID
	END
