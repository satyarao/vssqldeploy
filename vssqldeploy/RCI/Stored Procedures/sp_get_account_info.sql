﻿
--******(TH) New Procedure (NOT IN REPO YET).
CREATE PROCEDURE [RCi].[sp_get_account_info]
@OwnerId [UNIQUEIDENTIFIER]

AS
	BEGIN
		SELECT [PAN],[Limit],[Balance],[PaymentDeckID],[IsActive] 
		FROM   [RCi].[account] 
		WHERE  [OwnerId] = @OwnerId
	END
