﻿CREATE PROC [RCI].[GetFleetUsers]
	 @FleetTenantID UNIQUEIDENTIFIER
	,@UserIDs ListOfGuid READONLY 
	,@SearchTerm NVARCHAR(255) = NULL
	,@FirstName NVARCHAR(255) = NULL
	,@LastName NVARCHAR(255) = NULL
	,@IsActive BIT = NULL
	,@UserGroupID INT = NULL
	,@Offset INT
	,@Limit INT

AS
BEGIN
WITH TempResult (UserID, MobilePhoneNumber, FirstName, LastName, Title, [Address], OfficePhoneNumber, IsActive, Department, Note) AS 
(
	SELECT ui.UserID, MobileNumber, ui.FirstName, ui.LastName, fu.Title, fu.[Address], fu.OfficePhoneNumber, fu.IsActive, fu.Department, fu.Note			
	FROM [dbo].[UserInfo] ui
	JOIN [FleetUser] fu ON fu.UserID = ui.UserID AND fu.FleetTenantID = @FleetTenantID
	LEFT JOIN [AccessControl].[UserRestrictedTenantAssignment] urga ON urga.UserID = ui.UserID
	WHERE urga.[RestrictedTenantID] = @FleetTenantID
		AND ui.TenantId = '00000000-0000-0000-0000-000000000000' 
		AND ui.IsDeleted = 0 
		AND (ui.UserID IN (SELECT ID FROM @UserIDs) OR (NOT EXISTS (SELECT * FROM @UserIDs)))
		AND (@FirstName IS NULL OR ui.FirstName = @FirstName)
		AND (@LastName IS NULL OR ui.LastName = @LastName)
		AND (@UserGroupID IS NULL OR EXISTS(SELECT 1 FROM [AccessControl].[UserGroupAssignment] uga WHERE uga.UserID = ui.UserID  AND UserGroupID = @UserGroupID))						
		AND (@IsActive IS NULL OR fu.IsActive = @IsActive)
),
	TempCount AS (SELECT COUNT(*) AS TotalRows FROM TempResult), 
	ReturnValues AS (
		SELECT tr.UserID, 
			MobilePhoneNumber, 
			OfficePhoneNumber, 
			FirstName, 
			LastName, 
			Title,
			[Address],
			Note,
			tr.IsActive,
			IIF (uga.UserGroupID IS NULL, NULL, uga.UserGroupID) AS UserGroupID, 
			(SELECT TOP(1) UsergroupName FROM [AccessControl].[UserGroup] ug WHERE  ug.UserGroupID = uga.UserGroupID ) AS UsergroupName,
			Department, 
			(SELECT TOP(1) TotalRows FROM TempCount) AS TotalRows
		FROM TempResult tr
		LEFT JOIN [AccessControl].[UserGroupAssignment] uga ON uga.UserID = tr.UserID
			AND EXISTS (SELECT 1 FROM [AccessControl].[UserGroup] ug WHERE ug.UserGroupID = uga.UserGroupID
			AND ug.UserGroupName IN ('Fleet Admin', 'Fleet Manager') 
			AND ug.TenantID = @FleetTenantID)
	)
	SELECT * FROM ReturnValues rv
	WHERE @SearchTerm IS NULL 
		OR rv.UserID = TRY_CAST(@SearchTerm as UNIQUEIDENTIFIER)
		OR rv.FirstName LIKE '%' + @SearchTerm + '%'
		OR rv.LastName LIKE '%' + @SearchTerm + '%'
		OR rv.UsergroupName LIKE '%' + @SearchTerm + '%'					 
	ORDER BY UserID
	OFFSET @Offset ROWS
	FETCH NEXT @Limit ROWS ONLY;
END
