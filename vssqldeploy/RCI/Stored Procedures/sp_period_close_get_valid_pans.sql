﻿

--THIS SPROC RETURNS THE PANS USED IN THE PERIOD CLOSE SPROC BELOW:
CREATE PROCEDURE [RCi].[sp_period_close_get_valid_pans]
@PERIOD_CLOSE_TYPE INT

AS
	BEGIN
		SELECT [PAN]    
		FROM [RCi].[account]
		WHERE 
			[PeriodCloseType] = @PERIOD_CLOSE_TYPE AND			
			[IsActive] = 1
	END
