﻿ 


CREATE PROCEDURE [RCi].[sp_update_auto_pay]
@PAN [uniqueidentifier],
@IS_AUTO_PAY [BIT]

AS
	BEGIN
		DECLARE @STATUS INT = 1

		BEGIN TRY
			UPDATE [RCi].[account]
			SET [IsAutoPay] = @IS_AUTO_PAY
			WHERE [PAN] = @PAN			
		END TRY
		BEGIN CATCH
			SET @STATUS = -1
		END CATCH

		SELECT @STATUS
	END
