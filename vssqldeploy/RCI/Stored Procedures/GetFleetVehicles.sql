﻿-- =============================================
-- Create date: 07/17/2018 Igor Gaidukov
-- Description: Filter fleet vehicles
-- Update by Davydovich Roman 12/19/2018 - Added @IsActive argument
-- =============================================
CREATE PROCEDURE [RCI].[GetFleetVehicles]
	@TenantID UNIQUEIDENTIFIER = NULL, 
	@CustomerVehicleID NVARCHAR(64) = NULL,
	@VIN NVARCHAR(255) = NULL,
	@Name NVARCHAR(255) = NULL,
	@FromDate DATETIME2(7) = NULL,
	@ToDate DATETIME2(7) = NULL,
	@TypeID TINYINT = NULL,
	@StatusID TINYINT = NULL,
	@DriverIDs ListOfGuid READONLY,
	@Offset INT,
	@Limit INT,	
	@IsActive BIT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	WITH TempResult AS (
		SELECT fv.[VehicleID]
	  ,[CustomerVehicleID]
      ,[TenantID]
      ,[Name]
      ,[VIN]
	  ,[StateCode]
      ,[StateLicensedIssuedBy]
      ,[LicensePlateNumber]
      ,[CreatedOn]
      ,[VehicleTypeID]
      ,[StatusID]
      ,[OdometerReading]
      ,fvd.[DriverID]
      ,[Year]
      ,[Make]
      ,[Model]
	  ,[FuelCapacity]
	  ,[CompatibleFuels]
	FROM [RCI].[FleetVehicle] fv
	LEFT JOIN RCI.FleetVehicleDriver fvd ON fvd.VehicleID = fv.VehicleID 
	WHERE ( @TenantID IS NULL OR fv.TenantID = @TenantID )
		AND ( @VIN IS NULL OR @VIN = fv.VIN )
		AND ( @CustomerVehicleID IS NULL OR @CustomerVehicleID = fv.CustomerVehicleID )
		AND ( @Name IS NULL OR fv.Name LIKE '%' + @Name + '%' )
		AND ( @FromDate IS NULL OR fv.CreatedOn >= @FromDate )
		AND ( @ToDate IS NULL OR fv.CreatedOn <=  @ToDate )
		AND ( @TypeID IS NULL OR fv.VehicleTypeID = @TypeID)
		AND ( @StatusID IS NULL OR fv.StatusID = @StatusID) 
		AND (@IsActive IS NULL OR IsActive = @IsActive)
	),	
	Result AS ( SELECT DISTINCT [VehicleID] AS VehicleId
	  ,[CustomerVehicleID] AS CustomerVehicleId
      ,[TenantID] AS TenantId
      ,[Name]
      ,[VIN]
      ,[StateCode]
      ,[StateLicensedIssuedBy]
      ,[LicensePlateNumber]
      ,[CreatedOn]
      ,[VehicleTypeID] AS VehicleTypeId
      ,[StatusID] AS StatusId
      ,[OdometerReading]
      ,(SELECT ('[' + STUFF(( SELECT ',"' + convert(nvarchar(50), DriverID) + '"' AS [text()]
                            FROM TempResult tr2 WHERE tr1.VehicleID = tr2.VehicleID FOR XML PATH('')
                                    ), 1, 1, '') + ']')) AS [DriverIDs]
      ,[Year]
      ,[Make]
      ,[Model]
	  ,[FuelCapacity]
	  ,[CompatibleFuels]
	FROM TempResult tr1
	WHERE (NOT EXISTS (SELECT 1 FROM @DriverIDs) OR DriverID IN (SELECT ID FROM @DriverIDs))
	),
	ResultCount AS (SELECT COUNT(*) AS Total FROM Result)
	
	SELECT r.*, rc.*
	FROM Result r, ResultCount rc
	ORDER BY Name ASC
	  
	OFFSET @Offset ROWS 
	FETCH NEXT @Limit ROWS ONLY
END

