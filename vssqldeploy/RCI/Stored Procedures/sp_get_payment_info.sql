﻿

CREATE PROCEDURE [RCi].[sp_get_payment_info]
@PaymentSourceId uniqueidentifier
AS
	BEGIN
		SELECT [UserPaymentSourceID], [PaymentProviderToken], [UserId] 
		FROM [RCi].[UserRCiPaymentSource] 
		WHERE [UserPaymentSourceID] = @PaymentSourceId
	END
