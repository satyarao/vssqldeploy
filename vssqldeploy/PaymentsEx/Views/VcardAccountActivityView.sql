﻿
/************************************************************************* 
  Create Views  
*************************************************************************/ 
CREATE VIEW [PaymentsEx].[VcardAccountActivityView] 
AS 
   SELECT a.refnumber, 
         a.merchantid, 
         a.p97txid,
		     a.sequenceId,
         a.pan, 
         a.[timestamp], 
         a.[period], 
         a.amount, 
         a.activitytype, 
         b.[description]
  FROM   PaymentsEx.vcard_account_activity AS a 
         INNER JOIN PaymentsEx.vcard_account_activity_type AS b 
                 ON a.activitytype = b.id 

