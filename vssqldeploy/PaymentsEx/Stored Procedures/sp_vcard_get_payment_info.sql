﻿

CREATE PROCEDURE [PaymentsEx].[sp_vcard_get_payment_info]
@PaymentSourceId uniqueidentifier
AS
	BEGIN
		SELECT [UserPaymentSourceID], [PaymentProviderToken], [UserId] 
		FROM [PaymentsEx].[UserVCardPaymentSource] 
		WHERE [UserPaymentSourceID] = @PaymentSourceId
	END
