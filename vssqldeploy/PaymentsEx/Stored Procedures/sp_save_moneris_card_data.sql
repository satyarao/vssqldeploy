﻿
CREATE PROCEDURE [PaymentsEx].[sp_save_moneris_card_data]     
@ProviderID int,
@UserPaymentSourceId uniqueidentifier,
@PaymentProviderToken nvarchar(100),
@UserID uniqueidentifier,
@CustomerID nvarchar(30),
@FirstSix nvarchar(6),
@LastFour nvarchar(4),
@ExpMonth nchar(2),
@ExpYear nchar(2),
@ZipCode nvarchar(30),
@PaymentDeckID int,
@PaymentTypeID int
AS
	BEGIN
			DECLARE @ResultSet table (Result int, LS_UserPaymentSourceID int)

    BEGIN TRAN moneris_save_card_data

		BEGIN TRY
			INSERT INTO [PaymentsEx].[UserMonerisPaymentSource]([UserPaymentSourceID],[PaymentProviderToken],[UserId],[CustomerID],[FirstSix],[LastFour],[ExpMonth],[ExpYear],[ZipCode],[IsActive])
				VALUES(@UserPaymentSourceID,@PaymentProviderToken,@UserID,@CustomerID,@FirstSix,@LastFour,@ExpMonth,@ExpYear,@ZipCode,1)     
		    
			INSERT INTO @ResultSet (Result,LS_UserPaymentSourceID ) EXEC  [PaymentsEx].[sp_save_lk_card_data] @UserID, @UserPaymentSourceID, @ProviderID, @PaymentTypeID, @PaymentDeckID, NULL, NULL
		    
			COMMIT TRAN moneris_save_card_data
		END TRY

		BEGIN CATCH
			ROLLBACK TRAN moneris_save_card_data
	    END CATCH

		SELECT * FROM @ResultSet
	END
