﻿

CREATE PROCEDURE [PaymentsEx].[sp_payments_express_update_batch_close_info]
	@P97TxnId        [uniqueidentifier],
	@BatchId         [uniqueidentifier],
	@SiteId			 [nvarchar](100),
	@State           [int], 
	@CardType		 [int],
	@Amount          [DECIMAL](19,2)
AS
	BEGIN
		DECLARE @ResultCode int = -1

		BEGIN TRY
			UPDATE [PaymentsEx].[PaymentsExpressBatchCloseInfo]
			SET    [TxnState]	      = @State, 
				   [Amount]			  = @Amount
			WHERE  [P97TransactionId] = @P97TxnId AND
				   [BatchId]          = @BatchId AND
				   [SiteId]			  = @SiteId AND				   
				   [CardType]		  = @CardType

			IF @@ROWCOUNT = 1
				SET @ResultCode = 1
		END TRY

		BEGIN CATCH
		END CATCH

		SELECT @ResultCode
	END
