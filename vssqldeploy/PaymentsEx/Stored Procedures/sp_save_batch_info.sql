﻿
CREATE PROCEDURE [PaymentsEx].[sp_save_batch_info]
	@P97TxnId        [uniqueidentifier],
	@BatchId         [uniqueidentifier],
	@ProviderId      [INT],
	@TenantId		 [uniqueidentifier],
	@SiteId			 [nvarchar](100), 
	@TerminalId      [nvarchar](8),
	@State           [int], 
	@CardType        [int],
	@Amount          [DECIMAL](19,2)
AS
	BEGIN
		DECLARE @ResultCode int = -1

		BEGIN TRY
			INSERT INTO [PaymentsEx].[BatchInfo](
				[P97TransactionId],    
				[BatchId], 
				[ProviderId],		  
				[TenantId],            
				[SiteId],			  
				[TerminalId],          
				[TxnState],            
				[CardType], 
				[Amount],
				[IsClosed],            
				[ClosedDate])
			VALUES(
				@P97TxnId,        
				@BatchId, 
				@ProviderId,
				@TenantId,
				@SiteId,			 
				@TerminalId,      
				@State,           
				@CardType,
				@Amount,
				0,
				NULL) 

			SET @ResultCode = 1
		END TRY

		BEGIN CATCH
		END CATCH

		SELECT @ResultCode
	END
