﻿
CREATE PROCEDURE [PaymentsEx].[sp_get_batch_info]
	@BatchId    [uniqueidentifier],
	@ProviderId [INT],
	@TenantId	[uniqueidentifier],
	@SiteId     [nvarchar](100)
AS
	BEGIN		
		SELECT [TerminalId], [CardType], [Amount], [TxnState]
		FROM   [PaymentsEx].[BatchInfo]
		WHERE  [BatchId]    = @BatchId    AND 
			   [ProviderId]	= @ProviderId AND
			   [TenantId]	= @TenantId   AND
			   [SiteId]     = @SiteId     AND			  
			   [IsClosed]   = 0		      AND
			   [TxnState]  != 0
	END
