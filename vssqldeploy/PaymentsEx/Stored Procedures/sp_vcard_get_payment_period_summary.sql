﻿


--******(TH) New Procedure (NOT IN REPO YET). 
CREATE PROCEDURE [PaymentsEx].[sp_vcard_get_payment_period_summary]
@PAN uniqueidentifier,
@TIME_RANGE_START datetime2,
@TIME_RANGE_END datetime2
AS
	BEGIN
		SELECT [StartBalance],[StartConvenienceBalance],[FinalBalance],[FinalConvenienceFeeBalance],[PaymentBalance],[PaymentConvenienceFeeBalance],[PeriodStart],[PeriodEnd]
		FROM [PaymentsEx].[vcard_payment_period_summary]
		WHERE [PAN] = @PAN AND [PeriodStart] >= @TIME_RANGE_START AND [PeriodEnd] <= @TIME_RANGE_END
		ORDER BY [PeriodEnd] DESC
	END
