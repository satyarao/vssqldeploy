﻿
CREATE PROCEDURE [PaymentsEx].[sp_vcard_get_account_info]
@OwnerId [UNIQUEIDENTIFIER]

AS
	BEGIN
		SELECT [PAN],[Limit],[Balance],[PaymentDeckID],[IsActive] 
		FROM   [PaymentsEx].[vcard_account] 
		WHERE  [OwnerId] = @OwnerId
	END
