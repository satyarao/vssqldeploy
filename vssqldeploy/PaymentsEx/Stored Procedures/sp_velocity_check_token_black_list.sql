﻿
CREATE PROCEDURE [PaymentsEx].[sp_velocity_check_token_black_list]
	@TOKEN    [nvarchar](100),
    @TENANTID [uniqueidentifier],
    @GATEWAY  [nvarchar](100)
AS
	BEGIN
		DECLARE @RESULT INT = -1;

		IF NOT EXISTS( SELECT 1 FROM [PaymentsEx].[VelocityTokenBlackList]
			WHERE 
				[Token]        = @TOKEN    AND
				[TenantId]     = @TENANTID AND
				[TokenGateway] = @GATEWAY )
		BEGIN
			SET @RESULT = 1;
		END

		SELECT @RESULT;
	END
