﻿

CREATE PROCEDURE [PaymentsEx].[sp_deactivate_payments_express_card_data]
@UserPaymentSourceId uniqueidentifier
AS
	BEGIN
		DECLARE @ResultCode int = -1

		BEGIN TRY
		    UPDATE [PaymentsEx].[UserPaymentsExpressPaymentSource]
			SET PaymentProviderToken = '', IsActive = 0
			WHERE UserPaymentSourceID = @UserPaymentSourceId
			SET @ResultCode = 1
		END TRY

		BEGIN CATCH			
	    END CATCH

		SELECT @ResultCode
	END
