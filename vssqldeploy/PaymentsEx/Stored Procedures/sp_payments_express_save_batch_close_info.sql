﻿

CREATE PROCEDURE [PaymentsEx].[sp_payments_express_save_batch_close_info]
	@P97TxnId        [uniqueidentifier],
	@BatchId         [uniqueidentifier],
	@SiteId			 [nvarchar](100), 
	@State           [int], 
	@CardType        [int],
	@Amount          [DECIMAL](19,2)
AS
	BEGIN
		DECLARE @ResultCode int = -1

		BEGIN TRY
			INSERT INTO [PaymentsEx].[PaymentsExpressBatchCloseInfo](
				[P97TransactionId],    
				[BatchId],             
				[SiteId],			  			        
				[TxnState],            
				[CardType], 
				[Amount],
				[IsClosed],            
				[ClosedDate])
			VALUES(
				@P97TxnId,        
				@BatchId,         
				@SiteId,			 			    
				@State,           
				@CardType,
				@Amount,
				0,
				NULL) 

			SET @ResultCode = 1
		END TRY

		BEGIN CATCH
		END CATCH

		SELECT @ResultCode
	END
