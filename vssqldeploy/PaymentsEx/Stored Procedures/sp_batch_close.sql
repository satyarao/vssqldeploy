﻿
CREATE PROCEDURE [PaymentsEx].[sp_batch_close]
	@BatchId    [uniqueidentifier],
	@ProviderId [INT],
	@TenantId	[uniqueidentifier],
	@SiteId     [nvarchar](100)
AS
	BEGIN
		DECLARE @ResultCode int = -1

		BEGIN TRY
			UPDATE [PaymentsEx].[BatchInfo]
			SET    [IsClosed] = 1, [ClosedDate] = getutcdate()
			WHERE  [BatchId]    = @BatchId    AND 
				   [ProviderId]	= @ProviderId AND
			       [TenantId]	= @TenantId   AND
				   [SiteId]     = @SiteId 	
				   
			SET @ResultCode = 1
		END TRY

		BEGIN CATCH
		END CATCH

		SELECT @ResultCode
	END
