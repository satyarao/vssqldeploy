﻿
CREATE PROCEDURE [PaymentsEx].[sp_update_batch_info]
	@P97TxnId        [uniqueidentifier],
	@BatchId         [uniqueidentifier],
	@ProviderId      [INT],
	@TenantId		 [uniqueidentifier],
	@SiteId			 [nvarchar](100),
	@TerminalId      [nvarchar](8),
	@State           [int], 
	@CardType		 [int],
	@Amount          [DECIMAL](19,2)
AS
	BEGIN
		DECLARE @ResultCode int = -1

		BEGIN TRY
			IF @TerminalId IS NULL
				UPDATE [PaymentsEx].[BatchInfo]
				SET    [TxnState]	      = @State, 
					   [Amount]			  = @Amount
				WHERE  [P97TransactionId] = @P97TxnId   AND
					   [BatchId]          = @BatchId    AND
					   [ProviderId]		  = @ProviderId AND
					   [TenantId]		  = @TenantId   AND
					   [SiteId]			  = @SiteId     AND					    
					   [CardType]		  = @CardType   AND
					   [TerminalId]       IS NULL
			ELSE
				UPDATE [PaymentsEx].[BatchInfo]
				SET    [TxnState]	      = @State, 
					   [Amount]			  = @Amount
				WHERE  [P97TransactionId] = @P97TxnId   AND
					   [BatchId]          = @BatchId    AND
					   [ProviderId]		  = @ProviderId AND
					   [TenantId]		  = @TenantId   AND
					   [SiteId]			  = @SiteId     AND					   
					   [CardType]		  = @CardType   AND
					   [TerminalId]       = @TerminalId 

			IF @@ROWCOUNT = 1
				SET @ResultCode = 1
		END TRY

		BEGIN CATCH
		END CATCH

		SELECT @ResultCode
	END
