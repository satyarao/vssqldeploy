﻿                                                                                                          
-- sp

CREATE PROCEDURE [PaymentsEx].[sp_get_worldpay_card_data]
@UserPaymentSourceId uniqueidentifier,
@UserId uniqueidentifier
AS
	BEGIN
		SELECT ups.[ID], ups.[FirstSix], ups.[LastFour], ups.[ExpDate], ups.[PaymentProviderToken], ups.[PaymentTokenExpiry]
        FROM [PaymentsEx].[UserWorldPayPaymentSource] ups
		JOIN [PaymentsEx].[LKUserPaymentSource] lkups ON lkups.[ID] = ups.[ID]
        WHERE ups.[ID] = @UserPaymentSourceId AND lkups.[UserID] = @UserId AND lkups.[IsActive] = 1
	END
