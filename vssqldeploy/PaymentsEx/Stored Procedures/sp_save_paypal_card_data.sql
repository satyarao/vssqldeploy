﻿
CREATE PROCEDURE [PaymentsEx].[sp_save_paypal_card_data]     
	@ProviderID int,
	@UserPaymentSourceId uniqueidentifier,
	@UserID uniqueidentifier,
	@CustomerID nvarchar(256),
	@BillingAgreementID nvarchar(256),
	@Email nvarchar(256),
	@DeviceID nvarchar(256),
	@ImageUrl nvarchar(2048),
	@PaymentDeckID int,
	@PaymentTypeID int
AS
    BEGIN
            DECLARE @Existing_ID UNIQUEIDENTIFIER
			DECLARE @Existing_LS_UserPaymentSourceID INT
			DECLARE @ResultSet table (Result int, LS_UserPaymentSourceID int, ID uniqueidentifier)
			
			BEGIN TRAN paypal_save_card_data
			BEGIN TRY
				
				UPDATE lkups
				SET 
					[IsActive] = 0
				FROM [PaymentsEx].[UserPayPalPaymentSource] provups
				LEFT JOIN [PaymentsEx].[LKUserPaymentSource] lkups ON provups.[ID] = lkups.[ID]
				WHERE lkups.UserID = @UserID

				UPDATE ls_lkups
				SET 
					[IsActive] = 0
				FROM [PaymentsEx].[UserPayPalPaymentSource] provups
				LEFT JOIN [PaymentsEx].[LKUserPaymentSource] lkups ON provups.[ID] = lkups.[ID]
				LEFT JOIN [Payment].[LKUserPaymentSource] ls_lkups ON ls_lkups.[UserPaymentSourceId] = lkups.[LS_UserPaymentSourceID]
				WHERE lkups.UserID = @UserID
				
				SELECT TOP 1
					@Existing_ID = lkups.[ID],
					@Existing_LS_UserPaymentSourceID = lkups.[LS_UserPaymentSourceID]
					FROM [PaymentsEx].[UserPayPalPaymentSource] provups 
					LEFT JOIN [PaymentsEx].[LKUserPaymentSource] lkups ON provups.[ID] = lkups.[ID]
					WHERE lkups.[UserId] = @UserID AND lkups.[PaymentDeckId] = @PaymentDeckId AND provups.[BillingAgreementID] = @BillingAgreementID

				IF @Existing_ID IS NOT NULL
				BEGIN
					UPDATE [PaymentsEx].[LKUserPaymentSource]
					SET
						[IsActive] = 1,
						[UpdatedOn] = GETUTCDATE()
					WHERE [ID] = @Existing_ID

					UPDATE [Payment].[LKUserPaymentSource]
					SET
						[IsActive] = 1,
						[UpdatedOn] = GETUTCDATE()
					WHERE [UserPaymentSourceID] = @Existing_LS_UserPaymentSourceID

					UPDATE [PaymentsEx].[UserPayPalPaymentSource]
					SET
						[CustomerID] = @CustomerID,
						[BillingAgreementID] = @BillingAgreementID,
						[Email] = @Email,
						[DeviceID] = @DeviceID,
						[ImageUrl] = @ImageUrl
					WHERE [ID] = @Existing_ID
					
					INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID, ID) 
					VALUES(1, @Existing_LS_UserPaymentSourceID, @Existing_ID)
				END
				ELSE
				BEGIN
					INSERT INTO @ResultSet (Result,LS_UserPaymentSourceID ) 
						EXEC [PaymentsEx].[sp_save_lk_card_data] @UserID, @UserPaymentSourceID, @ProviderID, @PaymentTypeID, @PaymentDeckID, NULL, NULL

					INSERT INTO [PaymentsEx].[UserPayPalPaymentSource]([ID],[CustomerID],[BillingAgreementID],[Email],[DeviceID],[ImageUrl])
						VALUES(@UserPaymentSourceId, @CustomerID, @BillingAgreementID, @Email, @DeviceID, @ImageUrl)

					UPDATE @ResultSet
					SET ID = @UserPaymentSourceID
				END

			COMMIT TRAN paypal_save_card_data

			SELECT * FROM @ResultSet
			END TRY
			
			BEGIN CATCH
			ROLLBACK TRAN paypal_save_card_data

			SELECT TOP(0) * FROM @ResultSet
			END CATCH
    END
