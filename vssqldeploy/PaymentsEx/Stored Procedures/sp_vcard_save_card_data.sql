﻿
CREATE PROCEDURE [PaymentsEx].[sp_vcard_save_card_data]
@ProviderID int,
@UserPaymentSourceID uniqueidentifier,
@PAN uniqueidentifier,
@UserID uniqueidentifier,
@PaymentDeckID int,
@PaymentTypeID int

AS
	BEGIN
		DECLARE @ResultCode int
		DECLARE @LS_UserPaymentSourceID int
		DECLARE @CardName nvarchar(50)

		SELECT @CardName = [Name] FROM [PaymentsEx].[PaymentDeck] WHERE [ID] = @PaymentDeckID
		SELECT @LS_UserPaymentSourceID = LS_UserPaymentSourceID FROM [PaymentsEx].[LKUserPaymentSource] WHERE ID = @UserPaymentSourceID

		BEGIN TRAN vcard_save_card_data
		BEGIN TRY
			SET @ResultCode = -2
			INSERT INTO [PaymentsEx].[UserVCardPaymentSource]([UserPaymentSourceID],[PaymentProviderToken],[UserId],[CreatedOn],[IsActive])
				VALUES(@UserPaymentSourceID,@PAN,@UserId,GETDATE(),1)
			
			SET @ResultCode = -1 --assume failure.
			IF ( @LS_UserPaymentSourceID IS NULL )
				BEGIN
					INSERT INTO [Payment].[LKUserPaymentSource](UserID, PaymentProcessorID, CardName, IsActive, CreatedOn, UpdatedOn) --NEED TO SWAP THIS OUT FOR Payment.InsertUserPaymentSource AFTER WE GET FUNDING PROVIDER STUFF FROM PORTAL.
					VALUES(@UserID, @ProviderID, @CardName, 1, GETDATE(), GETDATE())
					SET @LS_UserPaymentSourceID = SCOPE_IDENTITY()

					INSERT INTO [PaymentsEx].[LKUserPaymentSource]([ID],[UserID],[PaymentDeckID],[PaymentTypeID],[ProviderID],[LS_UserPaymentSourceID],[IsActive],[CreatedOn],[UpdatedOn]) 
					VALUES(@UserPaymentSourceID,@UserID,@PaymentDeckID,@PaymentTypeID,@ProviderID,@LS_UserPaymentSourceID,1,GETDATE(),GETDATE())
				END
			ELSE
				BEGIN
					UPDATE [Payment].[LKUserPaymentSource]
					SET IsActive = 1, UpdatedOn = GETDATE()
					WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

					UPDATE [PaymentsEx].[LKUserPaymentSource]
					SET IsActive = 1, UpdatedOn = GETDATE()
					WHERE ID = @UserPaymentSourceID
				END

			COMMIT TRAN vcard_save_card_data
			SET @ResultCode = 1
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION vcard_save_card_data
		END CATCH
			
		SELECT @ResultCode, @LS_UserPaymentSourceID
	END
