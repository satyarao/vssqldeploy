﻿

CREATE PROCEDURE [PaymentsEx].[sp_vcard_update_balance]  @PAN         UNIQUEIDENTIFIER, 
                                              @AMOUNT      DECIMAL(19, 2), 
                                              @UPDATE_TYPE BIT 
-- 0 = Credit (-balance), 1 = Debit (+balance) 
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from  
      -- interfering with SELECT statements.  
      SET nocount ON; 

      DECLARE @CURRENT_BAL DECIMAL(19, 2) 
      DECLARE @BALANCE_ADJUST DECIMAL(19, 2) 

      IF @UPDATE_TYPE = 0 
        SET @BALANCE_ADJUST = @AMOUNT  * -1; 
      ELSE 
        SET @BALANCE_ADJUST = @AMOUNT; 

      SELECT @CURRENT_BAL = [balance] 
      FROM   [PaymentsEx].[vcard_account] 
      WHERE  [pan] = @PAN 

      UPDATE [PaymentsEx].[vcard_account] 
      SET    [balance] = @CURRENT_BAL + @BALANCE_ADJUST 
      WHERE  [pan] = @PAN 
    
  END 

