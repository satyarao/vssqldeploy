﻿
CREATE PROCEDURE [PaymentsEx].[sp_payments_express_close_batch]
	@BatchId [uniqueidentifier],
	@SiteId  [nvarchar](100)
AS
	BEGIN
		DECLARE @ResultCode int = -1

		BEGIN TRY
			UPDATE [PaymentsEx].[PaymentsExpressBatchCloseInfo]
			SET    [IsClosed] = 1, [ClosedDate] = getutcdate()
			WHERE  [BatchId] = @BatchId AND 
				   [SiteId] = @SiteId 	
				   
			SET @ResultCode = 1
		END TRY

		BEGIN CATCH
		END CATCH

		SELECT @ResultCode
	END
