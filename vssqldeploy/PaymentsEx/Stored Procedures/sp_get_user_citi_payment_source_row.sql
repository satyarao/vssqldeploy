﻿
CREATE PROCEDURE [PaymentsEx].[sp_get_user_citi_payment_source_row]
@UserPaymentSourceId uniqueidentifier,
@UserId uniqueidentifier
AS
	BEGIN
		SELECT [ID],[PaymentProviderToken],[FirstSix],[LastFour],[ExpDate],[ProductId],[DeviceId],[UserID]
        FROM [PaymentsEx].[UserCitiPaymentSource]
        WHERE ID = @UserPaymentSourceId AND UserID = @UserId
	END
