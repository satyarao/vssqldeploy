﻿
CREATE PROCEDURE [PaymentsEx].[sp_get_paypal_card_data]
	@UserPaymentSourceId uniqueidentifier,
	@UserId uniqueidentifier
AS
	BEGIN
		SELECT ups.[ID], ups.[CustomerID], ups.[BillingAgreementID], ups.[Email], ups.[DeviceID]
        FROM [PaymentsEx].[UserPayPalPaymentSource] ups
		JOIN [PaymentsEx].[LKUserPaymentSource] lkups ON lkups.[ID] = ups.[ID]
        WHERE ups.[ID] = @UserPaymentSourceId AND lkups.[UserID] = @UserId AND lkups.[IsActive] = 1
	END
