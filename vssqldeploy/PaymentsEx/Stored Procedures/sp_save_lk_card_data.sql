﻿
CREATE PROCEDURE [PaymentsEx].[sp_save_lk_card_data]
	@UserID UNIQUEIDENTIFIER,
	@PaymentSourceID UNIQUEIDENTIFIER,
	@ProviderID INT,
	@PaymentTypeID INT,
	@PaymentDeckID INT,
	@DeviceID [nvarchar](255),
	@NickName [nvarchar](64)
AS
 BEGIN
   DECLARE @LS_UserPaymentSourceID int
   DECLARE @CardName nvarchar(50)
   DECLARE @ResultCode int

   SELECT @CardName = [Name] FROM [PaymentsEx].[PaymentDeck] WHERE [ID] = @PaymentDeckID
   SELECT @LS_UserPaymentSourceID = LS_UserPaymentSourceID FROM [PaymentsEx].[LKUserPaymentSource] WHERE ID = @PaymentSourceID

   BEGIN TRAN enroll
   BEGIN TRY

   --INSERTING INTO [Payment].[LKUserPaymentSource]
   IF ( @LS_UserPaymentSourceID IS NULL )
	   BEGIN
		   INSERT INTO [Payment].[LKUserPaymentSource](UserID, PaymentProcessorID, CardName, IsActive, CreatedOn, UpdatedOn, NickName, DeviceID) --NEED TO SWAP THIS OUT FOR Payment.InsertUserPaymentSource AFTER WE GET FUNDING PROVIDER STUFF FROM PORTAL.
		   VALUES(@UserID, @ProviderID, @CardName, 1, GETDATE(), GETDATE(), @NickName, @DeviceID)
		   SET @LS_UserPaymentSourceID = SCOPE_IDENTITY()

		   INSERT INTO [PaymentsEx].[LKUserPaymentSource]([ID],[UserID],[PaymentDeckID],[PaymentTypeID],[ProviderID],[LS_UserPaymentSourceID],[IsActive],[CreatedOn],[UpdatedOn],[DeviceID],[NickName]) 
		   VALUES(@PaymentSourceID,@UserID,@PaymentDeckID,@PaymentTypeID,@ProviderID,@LS_UserPaymentSourceID,1,GETDATE(),GETDATE(),@DeviceID,@NickName)
	   END
   ELSE
	   BEGIN
		   UPDATE [Payment].[LKUserPaymentSource]
		   SET IsActive = 1, UpdatedOn = GETDATE()
		   WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

		   UPDATE [PaymentsEx].[LKUserPaymentSource]
		   SET IsActive = 1, UpdatedOn = GETDATE()
		   WHERE ID = @PaymentSourceID
	   END

   COMMIT TRAN enroll
   SET @ResultCode = 1
   END TRY
   
   BEGIN CATCH
    ROLLBACK TRANSACTION enroll
	SET @ResultCode = -1
   END CATCH

   SELECT @ResultCode,
		  @LS_UserPaymentSourceID as UserPaymentSourceID
END
