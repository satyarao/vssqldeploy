﻿
CREATE PROCEDURE [PaymentsEx].[sp_vcard_create_account]
 @OWNER_ID UNIQUEIDENTIFIER,
 @LIMIT DECIMAL(19,2),
 @MOP INT,
 @PERIOD_CLOSE_ID INT,
 @IS_AUTO_PAY BIT,
 @DEFAULT_PERIOD_START datetime2,
 @DEFAULT_PERIOD_END datetime2

 AS
 BEGIN
   DECLARE @PAN UNIQUEIDENTIFIER = NEWID()
   DECLARE @RESULT INT

   BEGIN TRANSACTION create_acct
   BEGIN TRY
   INSERT INTO [PaymentsEx].[vcard_account]([PAN],[OWNERID],[LIMIT],[BALANCE],[TotalConvenienceFees],[PaymentDeckId],[PeriodCloseType],[IsAutoPay],[IsActive],[CreatedOn])
   VALUES( @PAN, @OWNER_ID, @LIMIT, 0, 0, @MOP, @PERIOD_CLOSE_ID, @IS_AUTO_PAY, 1, GETDATE() )

   --INSERT INTO [PaymentsEx].[UserVCardPaymentSource]([UserPaymentSourceID],[PaymentProviderToken],[UserId],[CreatedOn],[IsActive])  --TODO: I've removed this for now, need to figure out if there's a reason it's here, otherwise delete.
   --VALUES(@PAN, @PAN, @OWNER_ID, GETDATE(), 1)

   INSERT INTO [PaymentsEx].[vcard_payment_period_summary]([Id],[PAN],[StartBalance],[StartConvenienceBalance],[FinalBalance],[FinalConvenienceFeeBalance],[PaymentBalance],[PaymentConvenienceFeeBalance],[PeriodStart],[PeriodEnd])
   VALUES( NEWID(), @PAN, 0, 0, 0, 0, 0, 0, @DEFAULT_PERIOD_START, @DEFAULT_PERIOD_END )

   COMMIT TRANSACTION create_acct
   SET @RESULT = 1
   END TRY

   BEGIN CATCH
    ROLLBACK TRANSACTION create_acct
    SET @RESULT = -1
   END CATCH
   
   SELECT @RESULT, @PAN --return the PAN
 END
