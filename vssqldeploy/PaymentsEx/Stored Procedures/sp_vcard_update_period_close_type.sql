﻿




CREATE PROCEDURE [PaymentsEx].[sp_vcard_update_period_close_type]
@PAN [uniqueidentifier],
@NEW_PERIOD_CLOSE_TYPE [INT],
@DAY_OF_THE_MONTH [INT]

AS
	BEGIN
		DECLARE @STATUS INT = 1
		DECLARE @CURRENT_PERIOD_CLOSE_TYPE INT
		
		SELECT @CURRENT_PERIOD_CLOSE_TYPE = [PeriodCloseType]
		FROM [PaymentsEx].[vcard_account]
		WHERE [PAN] = @PAN
		
		IF( @CURRENT_PERIOD_CLOSE_TYPE = 1 AND @NEW_PERIOD_CLOSE_TYPE = 2 AND @DAY_OF_THE_MONTH >= 16 )			
			IF EXISTS ( SELECT * FROM [PaymentsEx].[vcard_account] WHERE [PAN] = @PAN AND [IsActive] = 1 )
				SET @STATUS = 2  --status code: tells calling entity to create a Bi-Monthly Period Summary for first half of month with this PAN.
			
		BEGIN TRY
			UPDATE [PaymentsEx].[vcard_account]
			SET [PeriodCloseType] = @NEW_PERIOD_CLOSE_TYPE
			WHERE [PAN] = @PAN
		END TRY
		BEGIN CATCH
			SET @STATUS = -1
		END CATCH

		SELECT @STATUS
	END
