﻿
--******(TH) New Procedure (NOT IN REPO YET). 
CREATE PROCEDURE [PaymentsEx].[sp_vcard_get_account_activity_entries]
@PAN uniqueidentifier,
@PERIOD_START datetime2,
@PERIOD_END datetime2
AS
	BEGIN
		SELECT [refnumber],[p97txid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp]
		FROM [PaymentsEx].[vcard_account_activity]
		WHERE 
			[pan] = @PAN AND 
			[timestamp] >= @PERIOD_START AND 
			[timestamp] < @PERIOD_END			
	END
