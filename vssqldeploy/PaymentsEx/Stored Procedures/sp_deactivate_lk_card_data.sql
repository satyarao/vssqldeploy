﻿CREATE PROCEDURE [PaymentsEx].[sp_deactivate_lk_card_data]
  @UserID UNIQUEIDENTIFIER,
  @UserPaymentSourceID UNIQUEIDENTIFIER
AS
BEGIN
  DECLARE @LS_UserPaymentSourceID int
  DECLARE @ResultCode int

  BEGIN TRAN enroll
  BEGIN TRY
  IF EXISTS(SELECT 1 FROM [PaymentsEx].[LKUserPaymentSource] WHERE ID = @UserPaymentSourceID AND UserID = @UserID)
  BEGIN
	  SELECT @LS_UserPaymentSourceID = LS_UserPaymentSourceID FROM [PaymentsEx].[LKUserPaymentSource] WHERE ID = @UserPaymentSourceID AND UserID = @UserID

	  UPDATE [Payment].[LKUserPaymentSource]
	  SET IsActive = 0, UpdatedOn = GETDATE()
	  WHERE UserPaymentSourceID = @LS_UserPaymentSourceID AND UserID = @UserID

	  UPDATE [PaymentsEx].[LKUserPaymentSource]
	  SET IsActive = 0, UpdatedOn = GETDATE()
	  WHERE ID = @UserPaymentSourceID AND UserID = @UserID

	  COMMIT TRAN enroll
	  SET @ResultCode = 1
  END
  ELSE
  BEGIN
	  SET @ResultCode = -1
  END
  END TRY

  BEGIN CATCH
    ROLLBACK TRANSACTION enroll
	SET @ResultCode = -1
   END CATCH

    SELECT @ResultCode
END
