﻿
--******(TH) Updated Procedure (NOT IN REPO YET). NOTE: I changed declined code from -2 to 3 in order to remain consistent.
CREATE PROCEDURE [PaymentsEx].[sp_vcard_capture] -- direct capture (no auth beforehand).
    @PAN				UNIQUEIDENTIFIER,
 	@AMOUNT				DECIMAL(19, 2), 
	@CONVENIENCE_AMOUNT DECIMAL(19, 2), 
    @PERIOD				NVARCHAR(50), 
	@MERCHANTID			NVARCHAR(50), 
	@P97TXID			UNIQUEIDENTIFIER,
	@SEQUENCEID			NVARCHAR(50)
AS 
BEGIN
   --if insufficient, return -2
   --begin transaction
   --create capture record
   --update balance
   --on exception
   -- status = -1
   --return status 1
   DECLARE @STATUS INT = -1 -- Assume it failed  
   DECLARE @AVAIL_BALANCE DECIMAL(19,2)
   DECLARE @REF_NUM UNIQUEIDENTIFIER = NEWID()
  
   SELECT @AVAIL_BALANCE =  x.[limit] - x.[balance]
      FROM [PaymentsEx].[vcard_account] x 
      WHERE @PAN = [x].[pan]
  
      --check available balance
      IF @AVAIL_BALANCE < @AMOUNT
         BEGIN
            SET @STATUS = 3   -- declined
         END

      ELSE
         BEGIN
            BEGIN TRY
               BEGIN TRAN vcard_capture
               INSERT INTO [PaymentsEx].[vcard_account_activity] ([refnumber],[pan],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype]) 
			        VALUES (@REF_NUM, 
			                @PAN, 
			                @P97TXID,
			                @PERIOD, 
			                @AMOUNT,
			                @MERCHANTID, 
							@SEQUENCEID,
			                3); -- Capture
        
              -- DEBIT the account
              EXEC [PaymentsEx].[sp_vcard_update_balance] @PAN, @AMOUNT, 1 

			  -- Create/Update Convenience Fee Records
				INSERT INTO [PaymentsEx].[vcard_account_activity] ([refnumber],[pan],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype])
					VALUES (NEWID(),
							@PAN,
							@P97TXID,
							@PERIOD,
							@CONVENIENCE_AMOUNT,
							@MERCHANTID, 
		  					@SEQUENCEID,
							5);
			  
			  UPDATE [PaymentsEx].[vcard_account]
			  	 SET [totalConvenienceFees] = [totalConvenienceFees] + @CONVENIENCE_AMOUNT			  	     			  	     
			   WHERE [pan] = @PAN

              SET @STATUS = 1
              COMMIT TRAN vcard_capture
           END TRY

           BEGIN CATCH
              ROLLBACK TRAN vcard_capture
              SET @STATUS = -1
              SET @REF_NUM = NULL
           END CATCH
        END

   SELECT @STATUS, @REF_NUM
END
