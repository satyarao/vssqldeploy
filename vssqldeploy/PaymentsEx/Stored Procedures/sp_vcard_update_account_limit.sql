﻿
--******(TH) New Procedure (NOT IN REPO YET).
CREATE PROCEDURE [PaymentsEx].[sp_vcard_update_account_limit]
@LIMIT   [DECIMAL](19,2),
@PAN     [UNIQUEIDENTIFIER],
@OWNERID [UNIQUEIDENTIFIER]

AS
	BEGIN
		DECLARE @STATUS INT = 1; 

		BEGIN TRAN update_limit
		BEGIN TRY
			INSERT [PaymentsEx].[vcard_account_activity]([refnumber],[pan],[amount],[activitytype])
			VALUES( NEWID(), @PAN, @LIMIT, 8 )

			UPDATE [PaymentsEx].[vcard_account] SET [Limit] = @LIMIT WHERE [PAN] = @PAN and [OWNERID] = @OWNERID
			COMMIT TRAN update_limit
		END TRY

		BEGIN CATCH
			ROLLBACK TRANSACTION update_limit
			SET @STATUS = -1
		END CATCH

		SELECT @STATUS
	END
