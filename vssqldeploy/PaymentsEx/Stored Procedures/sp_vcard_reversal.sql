﻿
--******(TH) Updated Procedure (NOT IN REPO YET).
CREATE PROCEDURE [PaymentsEx].[sp_vcard_reversal]  --refund (auth then capture, then call this).
   @PAN				   UNIQUEIDENTIFIER, 
   @MID				   NVARCHAR(50), 
   @AMOUNT			   DECIMAL(19, 2), 
   @CONVENIENCE_AMOUNT DECIMAL(19, 2),
   @PERIOD			   NVARCHAR(50), 
   @P97TXID			   UNIQUEIDENTIFIER,
   @SEQUENCEID		   NVARCHAR(50),
   @REF_NUM			   UNIQUEIDENTIFIER
AS 
   BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from  
      -- interfering with SELECT statements.  
      SET nocount ON; 

      IF @PAN IS NULL 
         RAISERROR('PAN CANNOT BE NULL',1,1); 

      IF @AMOUNT IS NULL 
         RAISERROR('AMOUNT CANNOT BE NULL',1,1); 

      IF @REF_NUM IS NULL
         SET @REF_NUM = NEWID()
	  
      DECLARE @STATUS INT = 1; 

	  -- Create a Credit in the account
	  BEGIN TRAN reversal
	  BEGIN TRY
	     INSERT INTO [PaymentsEx].[vcard_account_activity] ([refnumber],[p97txid],[pan],[period],[amount],[merchantid],[sequenceid],[activitytype]) 
              VALUES (@REF_NUM, 
                      @P97TXID,
                      @PAN, 
                      @PERIOD, 
                      @AMOUNT,
                      @MID, 
					  @SEQUENCEID,
                      4); -- REVERSAL
	 
		 EXEC [PaymentsEx].[sp_vcard_update_balance]  @PAN, @AMOUNT, 0

		 -- Create/Update Convenience Fee Records
		INSERT INTO [PaymentsEx].[vcard_account_activity] ([refnumber],[pan],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype])
			VALUES (NEWID(),
					@PAN,
					@P97TXID,
					@PERIOD,
					@CONVENIENCE_AMOUNT * -1,
					@MID, 
		  			@SEQUENCEID,
					6);
			  
		 UPDATE [PaymentsEx].[vcard_account]
			SET [totalConvenienceFees] = [totalConvenienceFees] - @CONVENIENCE_AMOUNT			 				  	
		  WHERE [pan] = @PAN

		 COMMIT TRAN reversal
	  END TRY

	  BEGIN CATCH
		 ROLLBACK TRANSACTION reversal
		 SET @STATUS = -1
	  END CATCH

	  SELECT @STATUS, @REF_NUM
   END 
