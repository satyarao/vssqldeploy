﻿
CREATE PROCEDURE [PaymentsEx].[sp_get_payment_source_provider]
@PaymentSourceId uniqueidentifier,
@UserId uniqueidentifier
AS
	BEGIN
		SELECT [ProviderID] 
		FROM [PaymentsEx].[LKUserPaymentSource] 
		WHERE [ID] = @PaymentSourceId AND [UserID] = @UserId AND [IsActive] =1
	END
