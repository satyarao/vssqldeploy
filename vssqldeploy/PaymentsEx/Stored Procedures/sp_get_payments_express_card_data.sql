﻿

CREATE PROCEDURE [PaymentsEx].[sp_get_payments_express_card_data]
@UserPaymentSourceId uniqueidentifier
AS
	BEGIN
		SELECT PaymentProviderToken
		FROM [PaymentsEx].[UserPaymentsExpressPaymentSource]
		WHERE UserPaymentSourceID = @UserPaymentSourceId
	END
