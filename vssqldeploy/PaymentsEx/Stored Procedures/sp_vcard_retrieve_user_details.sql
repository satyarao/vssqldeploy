﻿

CREATE PROCEDURE [PaymentsEx].[sp_vcard_retrieve_user_details]
@USER_ID uniqueidentifier,
@USER_PAYMENT_SOURCE_ID uniqueidentifier

AS
	BEGIN
		SELECT acct.[OwnerId], acct.[Limit], usr.[UserPaymentSourceID]
		FROM [PaymentsEx].[UserVCardPaymentSource] usr
			INNER JOIN [PaymentsEx].[vcard_account] acct 
			ON usr.[PaymentProviderToken] = acct.[PAN]
		WHERE usr.[UserId] = @USER_ID AND 
			usr.[UserPaymentSourceID] = @USER_PAYMENT_SOURCE_ID
	END
