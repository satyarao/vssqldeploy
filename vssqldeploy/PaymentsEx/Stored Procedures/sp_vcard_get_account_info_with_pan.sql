﻿

CREATE PROCEDURE [PaymentsEx].[sp_vcard_get_account_info_with_pan]
@OwnerId [UNIQUEIDENTIFIER],
@PAN     [UNIQUEIDENTIFIER]

AS
	BEGIN
		SELECT [PAN],[Limit],[Balance],[PaymentDeckID],[IsActive] 
		FROM   [PaymentsEx].[vcard_account] 
		WHERE  [OwnerId] = @OwnerId AND [PAN] = @PAN
	END
