﻿

--******(TH) Updated Procedure (NOT IN REPO YET).
CREATE PROCEDURE [PaymentsEx].[sp_vcard_capture_auth] -- called after the auth.
   @REF_NUM			   UNIQUEIDENTIFIER, 
   @AMOUNT			   DECIMAL(19, 2),
   @CONVENIENCE_AMOUNT DECIMAL(19, 2),
   @PERIOD			   NVARCHAR(50)
AS 
BEGIN
   --look for auth_hold
     -- if no auth_hold, set @STATUS = -2
   --look for auth_release
     --if auth_release,  set @STATUS = -3

    DECLARE @PAN					UNIQUEIDENTIFIER
	DECLARE @STATUS					INT = -1  --assume error
	DECLARE @AUTH_HOLD_AMOUNT		DECIMAL(19, 2)
	DECLARE @BALANCE_ADJUST			DECIMAL(19, 2)
	DECLARE @MERCHANTID				NVARCHAR(50)
	DECLARE @SEQUENCEID				NVARCHAR(50)
	DECLARE @P97TXID				UNIQUEIDENTIFIER

	SELECT TOP 1 @PAN = [pan] 
	   FROM [PaymentsEx].[vcard_account_activity] 
	   where [refnumber] = @REF_NUM;
	
	-- Find AUTH Record  
	-- IF NOT FOUND, ERROR -1  
	IF @PAN IS NULL
       BEGIN	
          SET @STATUS = -1
       END

    --is it already captured?
    ELSE IF EXISTS (SELECT 1 FROM [PaymentsEx].[vcard_account_activity] x 
				    WHERE x.[pan] = @PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 3)
    BEGIN
       SET @STATUS = -2;
    END

	ELSE
	   BEGIN

		 -- FIND AUTH_RELEASE, if Exists :(
		  IF EXISTS (SELECT 1 FROM [PaymentsEx].[vcard_account_activity] x 
		  			 WHERE x.[pan] = @PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 2)
		     BEGIN
		        SET @STATUS = -3;
		     END

		  -- Determine if no auth record exists
		  ELSE IF NOT EXISTS (SELECT 1 FROM [PaymentsEx].[vcard_account_activity] x 
							  WHERE x.[pan] = @PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 1) 
		     BEGIN
		        SET @STATUS = -4;
		     END

	      -- Auth record exists
		  ELSE
		     BEGIN				
			    SELECT @AUTH_HOLD_AMOUNT = x.[amount], 
					   @P97TXID = x.[p97txid],
					   @MERCHANTID = x.[merchantid],
					   @SEQUENCEID = x.[sequenceid]
			    FROM [PaymentsEx].[vcard_account_activity] x 
				WHERE  x.[pan] = @PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 1 

				-- CREATE AUTH_RELEASE  
				-- Create Activity Record
				-- Create Convenience Fee Record
				BEGIN TRAN capture
				BEGIN TRY
					   INSERT INTO [PaymentsEx].[vcard_account_activity] ([refnumber],[pan],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype]) 
					   VALUES      (@REF_NUM, 
									@PAN, 
									@P97TXID,
									@PERIOD, 
									@AUTH_HOLD_AMOUNT,
									@MERCHANTID, 
									@SEQUENCEID,
									2); -- AUTH_RELEASE
			                   

		  			   -- Create Capture Record  
		  			   INSERT INTO [PaymentsEx].[vcard_account_activity] ([refnumber],[pan],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype]) 
		  			        VALUES (@REF_NUM, 
		  						    @PAN, 
		  						    @P97TXID,
		  						    @PERIOD, 
		  						    @AMOUNT,
		  						    @MERCHANTID, 
		  						    @SEQUENCEID,
		  						    3); -- Capture

				       -- Update the Balance   
					   SET @BALANCE_ADJUST = @AUTH_HOLD_AMOUNT - @AMOUNT; 

					   IF @BALANCE_ADJUST > 0 
			    	      EXEC [PaymentsEx].[sp_vcard_update_balance] @PAN, @BALANCE_ADJUST, 0  -- CREDIT the difference between the AUTH_HOLD and the CAPTURE amount 

						-- Create/Update Convenience Fee Records
						INSERT INTO [PaymentsEx].[vcard_account_activity] ([refnumber],[pan],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype])
						     VALUES (NEWID(),
									 @PAN,
									 @P97TXID,
									 @PERIOD,
									 @CONVENIENCE_AMOUNT,
									 @MERCHANTID, 
		  						     @SEQUENCEID,
									 5);

						UPDATE [PaymentsEx].[vcard_account]
						   SET [totalConvenienceFees] = [totalConvenienceFees] + @CONVENIENCE_AMOUNT							   							   
						 WHERE [pan] = @PAN

						COMMIT TRAN capture
						  SET @STATUS = 1; -- Successful Status = 1 
					END TRY

					BEGIN CATCH
						ROLLBACK TRANSACTION capture
				PRINT ERROR_MESSAGE()
						SET @STATUS = -1; 
					END CATCH
			END
		END
  SELECT @STATUS
END
