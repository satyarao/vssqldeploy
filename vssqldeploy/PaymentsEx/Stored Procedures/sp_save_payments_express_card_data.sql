﻿

CREATE PROCEDURE [PaymentsEx].[sp_save_payments_express_card_data]     
@ProviderID           int,
@UserPaymentSourceId  uniqueidentifier,
@PaymentProviderToken nvarchar(100),
@UserID               uniqueidentifier,
@FirstSix             nvarchar(6),
@LastFour             nvarchar(4),
@ExpDate              datetime2(7),
@DeviceId	          [nvarchar](255),
@NickName             [nvarchar](100),
@PaymentDeckID        int,
@PaymentTypeID        int
AS
	BEGIN
		DECLARE @ResultSet table (Result int, LS_UserPaymentSourceID int)

    BEGIN TRAN payments_express_save_card_data
		BEGIN TRY
			INSERT INTO [PaymentsEx].[UserPaymentsExpressPaymentSource]([UserPaymentSourceID],[PaymentProviderToken],[UserId],[FirstSix],[LastFour],[ExpDate],[IsActive])
				VALUES(@UserPaymentSourceID,@PaymentProviderToken,@UserID,@FirstSix,@LastFour,@ExpDate,1)     		    

			INSERT INTO @ResultSet ( Result,LS_UserPaymentSourceID ) EXEC  [PaymentsEx].[sp_save_lk_card_data] @UserID, @UserPaymentSourceID, @ProviderID, @PaymentTypeID, @PaymentDeckID, @DeviceId, @NickName
		    
			COMMIT TRAN payments_express_save_card_data
		END TRY

		BEGIN CATCH
			ROLLBACK TRAN payments_express_save_card_data
	    END CATCH

		SELECT * FROM @ResultSet
	END
