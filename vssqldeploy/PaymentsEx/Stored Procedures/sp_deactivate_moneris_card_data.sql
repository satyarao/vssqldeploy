﻿

CREATE PROCEDURE [PaymentsEx].[sp_deactivate_moneris_card_data]
@UserPaymentSourceId uniqueidentifier
AS
	BEGIN
		DECLARE @ResultCode int = 1

		BEGIN TRY
		    UPDATE UserMonerisPaymentSource
			SET PaymentProviderToken = '', IsActive = 0
			WHERE UserPaymentSourceID = @UserPaymentSourceId
		END TRY

		BEGIN CATCH
			SET @ResultCode = -1
	    END CATCH

		SELECT @ResultCode
	END
