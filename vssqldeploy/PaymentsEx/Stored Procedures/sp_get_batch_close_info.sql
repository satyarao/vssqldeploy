﻿
CREATE PROCEDURE [PaymentsEx].[sp_get_batch_close_info]
	@BatchId [uniqueidentifier],
	@SiteId  [nvarchar](100)
AS
	BEGIN		
		SELECT [TerminalId], [CardType], [Amount], [TxnState]
		FROM   [PaymentsEx].[MonerisBatchCloseInfo]
		WHERE  [BatchId]    = @BatchId AND 
			   [SiteId]     = @SiteId AND			  
			   [IsClosed]   = 0 AND
			   [TxnState]  != 0
	END
