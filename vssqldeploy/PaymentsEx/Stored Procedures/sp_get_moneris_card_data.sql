﻿

CREATE PROCEDURE [PaymentsEx].[sp_get_moneris_card_data]    
@UserID uniqueidentifier,
@UserPaymentSourceID uniqueidentifier
AS
	BEGIN
		SELECT [PaymentProviderToken],[CustomerID],[ExpMonth],[ExpYear] 
		FROM [PaymentsEx].[UserMonerisPaymentSource] 
		WHERE [UserPaymentSourceID] = @UserPaymentSourceID AND [UserID] = @UserID
	END
