﻿
CREATE PROCEDURE [PaymentsEx].[sp_save_worldpay_card_data]
	@UserID uniqueidentifier,
	@ProviderID int,
	@PaymentTypeID int,
	@PaymentDeckID int,
	@UserPaymentSourceID uniqueidentifier,
	@FirstSix nvarchar(6),
	@LastFour nvarchar(4),
	@ExpDate datetime2(7),
	@PaymentProviderToken nvarchar(100),
	@PaymentTokenExpiry datetime2(7)
AS
	BEGIN
			DECLARE @Existing_ID UNIQUEIDENTIFIER
			DECLARE @Existing_LS_UserPaymentSourceID INT
			DECLARE @ResultSet table (Result int, LS_UserPaymentSourceID int, ID uniqueidentifier)
			
			BEGIN TRAN worldpay_save_card_data
			BEGIN TRY
				SELECT TOP 1
					@Existing_ID = wpups.[ID],
					@Existing_LS_UserPaymentSourceID = lkups.[LS_UserPaymentSourceID]
					FROM [PaymentsEx].[UserWorldPayPaymentSource] wpups
					LEFT JOIN [PaymentsEx].[LKUserPaymentSource] lkups ON lkups.[ID] = wpups.[ID]
					WHERE lkups.[UserId] = @UserID AND wpups.[PaymentProviderToken] = @PaymentProviderToken

				IF @Existing_ID IS NOT NULL
				BEGIN
					UPDATE [PaymentsEx].[LKUserPaymentSource]
					SET
						IsActive = 1,
						UpdatedOn = GETUTCDATE()
					WHERE ID = @Existing_ID
					
					INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID, ID) 
					VALUES(1, @Existing_LS_UserPaymentSourceID, @Existing_ID)
				END
				ELSE
				BEGIN
					INSERT INTO @ResultSet (Result,LS_UserPaymentSourceID ) 
						EXEC [PaymentsEx].[sp_save_lk_card_data] @UserID, @UserPaymentSourceID, @ProviderID, @PaymentTypeID, @PaymentDeckID, NULL, NULL

					INSERT INTO [PaymentsEx].[UserWorldPayPaymentSource]([ID],[FirstSix],[LastFour],[ExpDate],[PaymentProviderToken],[PaymentTokenExpiry])
						VALUES(@UserPaymentSourceId, @FirstSix, @LastFour, @ExpDate, @PaymentProviderToken, @PaymentTokenExpiry)

					UPDATE @ResultSet
					SET ID = @UserPaymentSourceID
				END
			COMMIT TRAN worldpay_save_card_data

			SELECT * FROM @ResultSet
			END TRY
			
			BEGIN CATCH
			ROLLBACK TRAN worldpay_save_card_data

			SELECT TOP(0) * FROM @ResultSet
			END CATCH
	END
