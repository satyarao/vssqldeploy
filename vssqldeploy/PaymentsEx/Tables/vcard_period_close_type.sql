﻿CREATE TABLE [PaymentsEx].[vcard_period_close_type] (
    [id]         INT           NOT NULL,
    [decription] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Period_Close_Type_Id] PRIMARY KEY CLUSTERED ([id] ASC)
);

