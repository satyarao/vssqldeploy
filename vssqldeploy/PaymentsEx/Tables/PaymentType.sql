﻿CREATE TABLE [PaymentsEx].[PaymentType] (
    [ID]   INT           NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PaymentType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

