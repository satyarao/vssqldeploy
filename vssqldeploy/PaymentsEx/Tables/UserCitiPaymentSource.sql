﻿CREATE TABLE [PaymentsEx].[UserCitiPaymentSource] (
    [ID]                   UNIQUEIDENTIFIER NOT NULL,
    [PaymentProviderToken] NVARCHAR (100)   NOT NULL,
    [FirstSix]             NCHAR (6)        NOT NULL,
    [LastFour]             NCHAR (4)        NOT NULL,
    [ExpDate]              DATETIME2 (7)    NULL,
    [ProductId]            NVARCHAR (4)     NOT NULL,
    [DeviceId]             NVARCHAR (256)   NOT NULL,
    [UserID]               UNIQUEIDENTIFIER NOT NULL,
    [CorrelationId]        NVARCHAR (256)   NULL,
    CONSTRAINT [PK_UserCitiPaymentSource] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UserCitiPaymentSource_ID] FOREIGN KEY ([ID]) REFERENCES [PaymentsEx].[LKUserPaymentSource] ([ID])
);

