﻿CREATE TABLE [PaymentsEx].[BatchInfo] (
    [P97TransactionId] UNIQUEIDENTIFIER NOT NULL,
    [BatchId]          UNIQUEIDENTIFIER NOT NULL,
    [ProviderId]       INT              NOT NULL,
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    [SiteId]           NVARCHAR (100)   NOT NULL,
    [TerminalId]       NVARCHAR (8)     NULL,
    [TxnState]         INT              NOT NULL,
    [CardType]         INT              NOT NULL,
    [Amount]           DECIMAL (19, 2)  NOT NULL,
    [IsClosed]         BIT              NOT NULL,
    [ClosedDate]       DATETIME2 (7)    NULL,
    CONSTRAINT [PK_BatchInfo] PRIMARY KEY CLUSTERED ([P97TransactionId] ASC)
);

