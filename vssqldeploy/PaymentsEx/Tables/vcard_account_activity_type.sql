﻿CREATE TABLE [PaymentsEx].[vcard_account_activity_type] (
    [id]          INT           NOT NULL,
    [description] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Account_Activity_Type] PRIMARY KEY CLUSTERED ([id] ASC)
);

