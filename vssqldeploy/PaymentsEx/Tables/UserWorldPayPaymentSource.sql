﻿CREATE TABLE [PaymentsEx].[UserWorldPayPaymentSource] (
    [ID]                   UNIQUEIDENTIFIER NOT NULL,
    [FirstSix]             NVARCHAR (6)     NOT NULL,
    [LastFour]             NVARCHAR (4)     NOT NULL,
    [ExpDate]              DATETIME2 (7)    NOT NULL,
    [PaymentProviderToken] NVARCHAR (100)   NOT NULL,
    [PaymentTokenExpiry]   DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_UserWorldPayPaymentSource] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UserWorldPayPaymentSource_ID] FOREIGN KEY ([ID]) REFERENCES [PaymentsEx].[LKUserPaymentSource] ([ID]) ON DELETE CASCADE
);

