﻿CREATE TABLE [PaymentsEx].[PaymentDeck] (
    [ID]            INT           NOT NULL,
    [Name]          NVARCHAR (50) NOT NULL,
    [PaymentTypeID] INT           NOT NULL,
    CONSTRAINT [PK_PaymentDeck] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PaymentTypeID] FOREIGN KEY ([PaymentTypeID]) REFERENCES [PaymentsEx].[PaymentType] ([ID])
);

