﻿CREATE TABLE [PaymentsEx].[PaymentsExpressBatchCloseInfo] (
    [P97TransactionId] UNIQUEIDENTIFIER NOT NULL,
    [BatchId]          UNIQUEIDENTIFIER NOT NULL,
    [SiteId]           NVARCHAR (100)   NOT NULL,
    [TxnState]         INT              NOT NULL,
    [CardType]         INT              NOT NULL,
    [Amount]           DECIMAL (19, 2)  NOT NULL,
    [IsClosed]         BIT              NOT NULL,
    [ClosedDate]       DATETIME2 (7)    NULL,
    CONSTRAINT [PK_PaymentsExpressBatchCloseInfo] PRIMARY KEY CLUSTERED ([P97TransactionId] ASC)
);

