﻿CREATE TABLE [PaymentsEx].[VelocityTokenBlackList] (
    [Token]        NVARCHAR (100)   NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [TokenGateway] NVARCHAR (100)   NOT NULL,
    [AddedOn]      DATETIME2 (7)    CONSTRAINT [DF_Velocity_Token_BlackList_AddedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_VelocityTokenBlackList] PRIMARY KEY CLUSTERED ([TenantId] ASC, [TokenGateway] ASC, [Token] ASC)
);

