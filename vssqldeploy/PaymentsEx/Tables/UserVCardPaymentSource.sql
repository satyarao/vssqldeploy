﻿CREATE TABLE [PaymentsEx].[UserVCardPaymentSource] (
    [UserPaymentSourceID]  UNIQUEIDENTIFIER NOT NULL,
    [PaymentProviderToken] UNIQUEIDENTIFIER NOT NULL,
    [UserId]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_UserVCardPaymentSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]             BIT              NOT NULL,
    CONSTRAINT [PK_UserVCardPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC),
    CONSTRAINT [FK_UserVCardPaymentSource_account] FOREIGN KEY ([PaymentProviderToken]) REFERENCES [PaymentsEx].[vcard_account] ([PAN])
);

