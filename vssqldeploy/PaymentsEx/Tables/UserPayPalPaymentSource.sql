﻿CREATE TABLE [PaymentsEx].[UserPayPalPaymentSource] (
    [ID]                 UNIQUEIDENTIFIER NOT NULL,
    [CustomerID]         NVARCHAR (256)   NOT NULL,
    [BillingAgreementID] NVARCHAR (256)   NOT NULL,
    [Email]              NVARCHAR (256)   NOT NULL,
    [DeviceID]           NVARCHAR (256)   NULL,
    [ImageUrl]           NVARCHAR (2048)  NULL,
    CONSTRAINT [PK_UserPayPalPaymentSource] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UserPayPalPaymentSource_ID] FOREIGN KEY ([ID]) REFERENCES [PaymentsEx].[LKUserPaymentSource] ([ID]) ON DELETE CASCADE
);

