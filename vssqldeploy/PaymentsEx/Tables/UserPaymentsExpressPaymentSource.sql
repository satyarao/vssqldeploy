﻿CREATE TABLE [PaymentsEx].[UserPaymentsExpressPaymentSource] (
    [UserPaymentSourceID]  UNIQUEIDENTIFIER NOT NULL,
    [PaymentProviderToken] NVARCHAR (100)   NOT NULL,
    [UserId]               UNIQUEIDENTIFIER NOT NULL,
    [FirstSix]             NVARCHAR (6)     NOT NULL,
    [LastFour]             NVARCHAR (4)     NOT NULL,
    [ExpDate]              DATETIME2 (7)    NOT NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_UserPayExpressPaymentSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]             BIT              NOT NULL,
    CONSTRAINT [PK_UserPaymentsExpressPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC)
);

