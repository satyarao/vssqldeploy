﻿CREATE TABLE [PaymentsEx].[LKUserPaymentSource] (
    [ID]                     UNIQUEIDENTIFIER NOT NULL,
    [UserID]                 UNIQUEIDENTIFIER NOT NULL,
    [PaymentDeckID]          INT              NOT NULL,
    [PaymentTypeID]          INT              NOT NULL,
    [ProviderID]             INT              NOT NULL,
    [LS_UserPaymentSourceID] INT              NOT NULL,
    [IsActive]               BIT              NOT NULL,
    [CreatedOn]              DATETIME2 (7)    CONSTRAINT [DF_LKUserPaymentSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]              DATETIME2 (7)    NOT NULL,
    [DeviceID]               NVARCHAR (255)   NULL,
    [NickName]               NVARCHAR (64)    NULL,
    CONSTRAINT [PK_LKUserPaymentSource] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_LKUserPaymentSource_PaymentDeck] FOREIGN KEY ([PaymentDeckID]) REFERENCES [PaymentsEx].[PaymentDeck] ([ID]),
    CONSTRAINT [FK_LKUserPaymentSource_PaymentType] FOREIGN KEY ([PaymentTypeID]) REFERENCES [PaymentsEx].[PaymentType] ([ID]),
    CONSTRAINT [FK_ProviderID_Provider] FOREIGN KEY ([ProviderID]) REFERENCES [PaymentsEx].[Provider] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IDX_LS_UserPaymentSourceID_IDLS_USERPAYMENTSOURCEID]
    ON [PaymentsEx].[LKUserPaymentSource]([ID] ASC, [LS_UserPaymentSourceID] ASC);

