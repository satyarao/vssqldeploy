﻿CREATE TABLE [PaymentsEx].[UserMonerisPaymentSource] (
    [UserPaymentSourceID]  UNIQUEIDENTIFIER NOT NULL,
    [PaymentProviderToken] NVARCHAR (100)   NOT NULL,
    [UserId]               UNIQUEIDENTIFIER NOT NULL,
    [CustomerID]           NVARCHAR (30)    NOT NULL,
    [FirstSix]             NVARCHAR (6)     NOT NULL,
    [LastFour]             NVARCHAR (4)     NOT NULL,
    [ExpMonth]             NCHAR (2)        NOT NULL,
    [ExpYear]              NCHAR (2)        NOT NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_UserMonerisPaymentSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]             BIT              NOT NULL,
    [ZipCode]              NVARCHAR (30)    NULL,
    CONSTRAINT [PK_UserMonerisPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC)
);

