﻿CREATE TABLE [PaymentsEx].[Provider] (
    [ID]   INT           NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Provider] PRIMARY KEY CLUSTERED ([ID] ASC)
);

