﻿CREATE TABLE [PaymentsEx].[vcard_payment_period_summary] (
    [Id]                           UNIQUEIDENTIFIER NOT NULL,
    [PAN]                          UNIQUEIDENTIFIER NOT NULL,
    [StartBalance]                 DECIMAL (19, 2)  NOT NULL,
    [StartConvenienceBalance]      DECIMAL (19, 2)  NOT NULL,
    [FinalBalance]                 DECIMAL (19, 2)  NOT NULL,
    [FinalConvenienceFeeBalance]   DECIMAL (19, 2)  NOT NULL,
    [PaymentBalance]               DECIMAL (19, 2)  NOT NULL,
    [PaymentConvenienceFeeBalance] DECIMAL (19, 2)  NOT NULL,
    [PeriodStart]                  DATETIME2 (7)    NOT NULL,
    [PeriodEnd]                    DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_Period_Archive_Id] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Pan] FOREIGN KEY ([PAN]) REFERENCES [PaymentsEx].[vcard_account] ([PAN])
);

