﻿CREATE   VIEW [Txn].[RecentTransactionDetailsView]
AS
	SELECT
		txn.[TxnID],
		txn.[UserID],
		(
			SELECT
				txn.[TxnID] AS [transaction_id],
				txn.[CurrencyCode] AS [currency],
				bas.[POSDateTimeLocal] AS [pos_datetime_local],
				sto.[Name] AS [store_name],
				common.MPPAID_INT_TO_STRING(sto.[MPPAID]) AS [merchant_id],
				sto.[StreetAddress] AS [address],
				sto.[City] AS [city],
				sto.[StateCode] AS [state],
				sto.[ZipCode] AS [postal_code],
				sto.[CountryIsoCode] As [country_iso_code],
				sto.[Phone] AS [phone_number],
				flb.[Name] AS [fuel_brand],
				(SELECT TOP(1) [AuthorizationCode] FROM [Txn].[TxnPaymentResponse] WHERE [TxnID] = txn.[TxnID] AND [PaymentResponseType] = 'AUTH') AS [auth_code],
				tpi.[ReceiptCardName] AS [card_type],
				tpi.[LastFour] AS [last_four],
				tot.[ItemRewardTotal] AS [total_discount],
				tot.[SubTotal] AS [subtotal],
				tot.[TaxAmount] AS [tax],
				tot.[Total] AS [total],
				--[formatted_total_discounts],
				--[formatted_subtotal],
				--[formatted_tax],
				--[formatted_total],
				--[formatted_pos_datetime_local],
				/*****line_items*****/
				(
					SELECT [AdjustedUnitPrice] AS [unit_price],
						[ProductDescription] AS [item_description],
						--[formatted_unit_price],
						--[formatted_total],					  
						[UnitOfMeasure] AS [unit_of_measure],
						[Quantity] AS [quantity],
						[AdjustedAmount] AS [total],
						CASE
							WHEN LOWER([ReverseSale]) = 'negativeamount'
								THEN [ReverseSale]
							ELSE NULL
						END AS [reverse_sale],
						CASE
							WHEN LOWER([StandardProductCategory]) = 'fuel'
								THEN CAST(1 AS BIT)
							ELSE CAST(0 AS BIT)
						END AS [is_fuel_item],
						(
							SELECT ISNULL((SELECT TOP(1) extO.[ExternalOfferId] FROM [Offer].[ExternalOffer] extO INNER JOIN [Offer].[Offer] ofr ON ofr.[OfferID] = tlid.[OfferID] WHERE extO.[ReferenceId] = ofr.RewardID), tlid.[OfferID]) AS [offer_id],
								[RewardApplied] AS [offer_applied],
								[Quantity] AS [offer_amount],
								[RebateLabel] AS [offer_title],
								[DiscountAmount] AS [offer_discount],
								[PriceAdjustmentID] AS [price_adjustment_id],
								[ProgramID] AS [program_id]
							FROM [Txn].[TxnLineItemDiscount]tlid
							WHERE tli.[TxnLineItemID] = tlid.[TxnLineItemID]
							FOR JSON PATH
						) AS [offers],
						[PosCode] AS [pos_code],
						[LinkedItemID] AS [linked_item_id],
						[SiteLineItemID] AS [item_id]
					FROM [Txn].[TxnLineItem] tli
					WHERE [TxnID] = txn.[TxnID]
					FOR JSON PATH
				) AS [line_items],

				lkt.[StandardName] AS [time_zone],
				lkt.[BaseUtcOffsetSec] AS [utc_offset_seconds],
				/*****car_wash_details*****/
				(
					SELECT bli.[CarWashCode] AS [car_wash_code],
						bli.[CarWashCodeExpiration] AS [car_wash_code_expiration],
						CASE
							WHEN bli.[CarWashCodeExpiration] IS NOT NULL
								THEN CAST(1 AS BIT)
							ELSE CAST(0 AS BIT)
						END AS [car_wash_expires],
						bli.[CarWashText] AS [car_wash_text]
					FROM [dbo].[BasketLineItem] bli
					WHERE bli.[BasketID] = txn.[TxnID] AND 
						(bli.[CarWashCode] IS NOT NULL OR bli.[CarWashCodeExpiration] IS NOT NULL OR bli.[CarWashText] IS NOT NULL)
					FOR JSON PATH
				) AS [car_wash_details],
				/*****receipt_lines*****/
				bas.[AppChannel] AS [app_channel],
				bas.[AppDisplayName] AS [app_display_name],
				--[fuel_ppu_discount_amount]
				(SELECT TOP(1) tli.[UnitOfMeasure] FROM [Txn].[TxnLineItem] tli WHERE tli.[TxnID] = txn.[TxnID] AND LOWER(tli.[StandardProductCategory]) = 'fuel') AS [fuel_unit_of_measure],
				(
					SELECT TOP(1) tlid.[DiscountUnitPrice]
					FROM		[Txn].[TxnLineItemDiscount] tlid
					INNER JOIN	[Txn].[TxnLineItem] tli
					ON			tlid.[TxnLineItemID] = tli.[TxnLineItemID]
					WHERE		[StandardProductCategory] = 'Fuel' AND
								tli.[TxnID] = txn.[TxnID]
				) AS [fuel_ppu_discount_amount],
				--[formatted_fuel_ppu_discount_amount],
				--[fleet_data]
				txn.[TerminalID] AS [dispenser_position],
				bas.[BasketStateID] AS [basket_state_id],
				sto.[TenantID] AS [store_tenant_id],
				stoTen.[Name] AS [store_tenant_name],
				sto.[StoreNumber] AS [store_number],
				txn.[SiteTxnID] AS [pos_transaction_id],
				CASE
					WHEN LOWER(txn.[TerminalID]) = 'inside'
						THEN CAST(1 AS BIT)
					ELSE CAST(0 AS BIT)
				END AS [in_store_purchase] --[InStorePurchase]
				/*****loyalty_program_data*****/
			FROM [Txn].[Txn] txn2
			LEFT JOIN [dbo].[Basket] bas ON bas.[BasketID] = txn.[TxnID]
			LEFT JOIN [dbo].[Store] sto ON sto.[StoreID] = txn.[StoreID]
			LEFT JOIN [dbo].[Tenant] stoTen ON sto.[TenantID] = stoTen.[TenantID]
			LEFT JOIN [dbo].[FuelBrand] flb ON sto.[FuelBrandID] = flb.[FuelBrandID]
			LEFT JOIN [Common].[LKTimezone] lkt ON sto.[TimeZoneID] = lkt.[TimeZoneID]
			LEFT JOIN [Txn].[TxnPaymentInfo] tpi ON txn.[TxnID] = tpi.[TxnID]
			LEFT JOIN [Txn].[TxnTotals] tot ON tot.[TxnID] = txn.[TxnID]
			WHERE txn2.[TxnID] = txn.[TxnID]
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		) AS [Json]
	FROM [Txn].[Txn] txn

