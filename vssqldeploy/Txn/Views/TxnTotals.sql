﻿CREATE   VIEW [Txn].[TxnTotals] 
AS

	SELECT [TxnID],
		CASE
			WHEN [Total] < 0
				THEN 0
			ELSE [Total]
		END AS [Total],
		CASE
			WHEN [SubTotal] < 0
				THEN 0
			ELSE [SubTotal]
		END AS [SubTotal],
		[TaxAmount],
		[ItemRewardTotal]
	FROM
	(
		SELECT [TxnID],
			ISNULL((
				SELECT TOP(1) tpr.[AuthorizedAmount]
				FROM [Txn].[TxnPaymentResponse] tpr
				WHERE [PaymentResponseType] = 'FINAL' AND tbl.[TxnID] = tpr.[TxnID] AND LOWER([PaymentResultCode]) = 'approved'
			 ), 0) AS [Total],
			SUM([CorrectedOriginalAmount]) AS [SubTotal],
			SUM([Taxes]) AS [TaxAmount],
			SUM([Discount]) AS [ItemRewardTotal],
			MAX([CreatedOn]) As [CreatedOn]
		FROM 
		(
			SELECT tli.[TxnID],
				tli.[TxnLineItemID],
				CASE
					WHEN LOWER(tli.[StandardProductCategory]) = 'gst' OR
						LOWER(tli.[StandardProductCategory]) = 'tax'
						THEN 0
					WHEN LOWER(tli.[ReverseSale]) = 'negativeamount'
						THEN 0
					WHEN [Quantity] = 0 AND [OriginalUnitPrice] = 0
						THEN 0
					ELSE tli.[OriginalAmount]
				END AS [CorrectedOriginalAmount],
				CASE
					WHEN LOWER(tli.[StandardProductCategory]) = 'gst' OR
						LOWER(tli.[StandardProductCategory]) = 'tax'
						THEN 0
					WHEN LOWER(tli.[ReverseSale]) = 'negativeamount'
						THEN 0 - tli.[AdjustedAmount]
					WHEN [Quantity] = 0 AND [OriginalUnitPrice] = 0
						THEN 0
					ELSE tli.[AdjustedAmount]
				END AS [CorrectedAdjustedAmount],
				CASE
					WHEN LOWER(tli.[StandardProductCategory]) = 'gst' OR
						LOWER(tli.[StandardProductCategory]) = 'tax'
						THEN tli.[AdjustedAmount]
					ELSE 0
				END AS [Taxes],
				CASE
					WHEN LOWER(tli.[StandardProductCategory]) = 'splittender' OR 
					LOWER(tli.[StandardProductCategory]) = 'gst' OR
					LOWER(tli.[StandardProductCategory]) = 'tax'
						THEN 0
					ELSE
						CASE
							WHEN LOWER(tli.[ReverseSale]) = 'negativeamount'
								THEN tli.[AdjustedAmount]
							ELSE 
								(
									(tli.[OriginalAmount] - tli.[AdjustedAmount]) + 
									(
										SELECT SUM([DiscountAmount])
										FROM [Txn].[TxnLineItemDiscount] tlid
										WHERE tli.[TxnLineItemID] = tlid.[TxnLineItemID] AND [RewardApplied] = 1
									)
								)
						END
				END AS [Discount],
				tli.[CreatedOn]
			FROM [Txn].[TxnLineItem] tli
			LEFT JOIN (SELECT TxnLineItemID, COUNT(TxnLineItemDiscountID) as PriceAdjustmentCount FROM [Txn].TxnLineItemDiscount tlid WHERE [RewardApplied] = 1 GROUP BY TxnLineItemID) stlid 
				ON stlid.TxnLineItemID = tli.TxnLineItemID
		) tbl
		GROUP BY [TxnID]
	)tbl2


