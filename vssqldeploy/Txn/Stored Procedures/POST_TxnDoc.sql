﻿
/******************************************************************************************************************************************************/

CREATE   PROCEDURE [Txn].[POST_TxnDoc] @txn_doc nvarchar(max), @stac_doc nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
BEGIN TRY

	EXEC [Txn].[Serialize_TxnDoc] @txn_doc , @stac_doc , 0, 1;

END TRY
BEGIN CATCH
	PRINT ERROR_MESSAGE()
	DECLARE @Exceptions [StoredProcedureExceptionType]; 
	INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
	(SELECT 
		@txn_doc AS [Txn_Doc],
		@stac_doc AS [Stac_Doc]
	FOR JSON PATH) AS [StoredProcedureInput];
	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
END CATCH
END
