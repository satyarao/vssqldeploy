﻿
/******************************************************************************************************************************************************/

CREATE   PROCEDURE [Txn].[Serialize_TxnDoc] @txn_doc nvarchar(max), @stac_doc nvarchar(max), @isUpdate bit, @isInsert bit
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	DECLARE @TxnDocBody [Txn].[TxnDocBodyType],
		@TxnDocPaymentResponse [Txn].[TxnDocPaymentResponseType],
		@TxnDocFuelEvent [Txn].[TxnDocFuelEventType],
		@TxnDocLoyaltyInstrument [Txn].[TxnDocLoyaltyInstrumentType],
		@TxnDocLineItem [Txn].[TxnDocLineItemType],
		@TxnDocLineItem2 [Txn].[TxnDocLineItemType],
		@StacDocBody [Txn].[StacDocBodyType],
		@StacDocLoyaltyInstrument [Txn].[StacDocLoyaltyInstrumentType],
		@StacDocTxnID uniqueidentifier,
		@TxnDocTxnID uniqueidentifier;


	INSERT INTO @TxnDocBody
		([TxnID],[PartnerTxnID],[SiteTxnID],[LoyaltyTxnID],[StoreTenantID],[AppTenantID],[UserID],[StoreID]
		,[MppaID],[AppDisplayName],[AppChannel],[UMTI],[TransactionOwner],[TransactionOrigin],[SettlementPeriodId]
		,[TransactionDateTime],[TerminalID],[MonetaryAmount],[CurrencyCode],[BatchID],[BatchGuid],[StacString]
		,[StacString2],[StacIssuer],[StacToken],[StacExpiration],[StacStatus],[WalletType],[CardIssuer],[CardType]
		,[FirstSix],[LastFour],[AuthLocation],[FinalLocation],[WalletItemData],[PaymentExpiration]
		,[PaymentProviderToken],[PaymentProviderName],[UserPaymentSourceID],[PaymentType],[ReceiptCardName]
		,[PaymentProcessorID],[FundingProviderID],[FleetData],[UserPaymentSourceExID],[SecurityType],[ExpirationDate]
		,[Dpan],[Data],[ExpirationMonth],[ExpirationYear],[EciIndicator],[PaymentAuthLimit],[StoreOverrunBuffer]
		,[CalculatedAuthLimit],[SessionID],[UserLatitude],[UserLongitude],[DeviceID],[VerificationMethod]
		,[VerificationData],[MaxRetryCount],[DisplayPrompt])
	SELECT ISNULL([TxnID], [TransactionTxnID]) AS [TxnID],
		[PartnerTxnID],
		[SiteTxnID],
		[LoyaltyTxnID],
		[StoreTenantID],
		[AppTenantID],
		[UserID],
		ISNULL([StoreID], [TransactionStoreID]) AS [StoreID],
		ISNULL([MppaID], [TransactionMppaID]) AS [MppaID],
		[AppDisplayName],
		[AppChannel],
		ISNULL([UMTI], [TransactionUMTI]) AS [UMTI],
		[TransactionOwner],
		[TransactionOrigin],
		[SettlementPeriodId],
		[TransactionDateTime],
		[TerminalID],
		[MonetaryAmount],
		[CurrencyCode],
		[BatchID],
		[BatchGuid]

		,[StacString]
		,[StacString2]
		,[StacIssuer]
		,[StacToken]
		,[StacExpiration]
		,[StacStatus]
		,[WalletType]
		,[CardIssuer]
		,[CardType]
		,[FirstSix]
		,[LastFour]
		,[AuthLocation]
		,[FinalLocation]
		,[WalletItemData]
		,[PaymentExpiration]
		,[PaymentProviderToken]
		,[PaymentProviderName]
		,[UserPaymentSourceID]
		,[PaymentType]
		,[ReceiptCardName]
		,[PaymentProcessorID]
		,[FundingProviderID]
		,[FleetData]
		,[UserPaymentSourceExID]
		,[SecurityType]
		,[ExpirationDate]
		,[Dpan]
		,[Data]
		,[ExpirationMonth]
		,[ExpirationYear]
		,[EciIndicator]
		,[PaymentAuthLimit]
		,[StoreOverrunBuffer]
		,[CalculatedAuthLimit]
		,[SessionID]
		,[UserLatitude]
		,[UserLongitude]
		,[DeviceID]
		,[VerificationMethod]
		,[VerificationData]
		,[MaxRetryCount]
		,[DisplayPrompt]
	FROM OPENJSON(@txn_doc)
	WITH
	(
		[TxnID]					uniqueidentifier	'$.p97TransactionId',
		[TransactionTxnID]		uniqueidentifier	'$.transaction.p97TransactionId',

		[PartnerTxnID]			nvarchar(255)		'$.partnerTransactionId',
		[SiteTxnID]				nvarchar(255)		'$.transaction.siteTransactionId',
		[LoyaltyTxnID]			nvarchar(255)		'$.loyaltyTransactionId',
		[StoreTenantID]			uniqueidentifier	'$.storeTenantId',
		[AppTenantID]			uniqueidentifier	'$.appTenantId',
		[UserID]				uniqueidentifier	'$.userId',
		[StoreID]				uniqueidentifier	'$.storeId',
		[TransactionStoreID]	uniqueidentifier	'$.transaction.storeId',
		[MppaID]				nvarchar(10)		'$.mppaId',
		[TransactionMppaID]		nvarchar(10)		'$.transaction.mppaId',
		[AppDisplayName]		nvarchar(255)		'$.createRequest.appDisplayName',
		[AppChannel]			nvarchar(100)		'$.createRequest.appChannel',
		[UMTI]					nvarchar(50)		'$.umti',
		[TransactionUMTI]		nvarchar(50)		'$.transaction.umti',
		[TransactionOwner]		nvarchar(50)		'$.transactionOwner',
		[TransactionOrigin]		nvarchar(50)		'$.transaction.transactionOrigin',
		[SettlementPeriodId]	nvarchar(50)		'$.transaction.settlementPeriodId',
		[TransactionDateTime]	datetime2			'$.transaction.transactionDateTime',
		[TerminalID]			nvarchar(255)		'$.transaction.terminalId',
		[MonetaryAmount]		decimal(7,3)		'$.transaction.amount.Amount',
		[CurrencyCode]			nvarchar(3)			'$.transaction.amount.CurrencyCode',
		[BatchID]				int					'$.batchInformation.batchId',
		[BatchGuid]				uniqueidentifier	'$.batchInformation.batchGuid',

		[StacString]			nvarchar(50)		'$.stacString',
		[StacString2]			nvarchar(50)		'$.createRequest.stacMessage.stacString',
		[StacIssuer]			nvarchar(50)		'$.createRequest.stacMessage.stacIssuer',
		[StacToken]				nvarchar(50)		'$.createRequest.stacMessage.stacToken',
		[StacExpiration]		datetime2			'$.createRequest.stacMessage.stacExpiration',
		[StacStatus]			nvarchar(50)		'$.createRequest.stacMessage.stacStatus',

		[WalletType]			nvarchar(50)		'$.paymentInformation.walletType',
		[CardIssuer]			nvarchar(50)		'$.paymentInformation.cardIssuer',
		[CardType]				nvarchar(50)		'$.paymentInformation.cardType',
		[FirstSix]				nvarchar(6)			'$.paymentInformation.firstSix',
		[LastFour]				nvarchar(4)			'$.paymentInformation.lastFour',
		[AuthLocation]			nvarchar(50)		'$.paymentInformation.authLocation',
		[FinalLocation]			nvarchar(50)		'$.paymentInformation.finalLocation',
		[WalletItemData]		nvarchar(max)		'$.paymentInformation.walletItemData',		--Really Dictionary
		[PaymentExpiration]		datetime2			'$.paymentInformation.expiration',
		[PaymentProviderToken]	nvarchar(50)		'$.paymentInformation.paymentProviderToken',
		[PaymentProviderName]	nvarchar(50)		'$.paymentInformation.paymentProviderName',
		[UserPaymentSourceID]	int					'$.paymentInformation.userPaymentSourceId',
		[PaymentType]			nvarchar(50)		'$.paymentInformation.paymentType',
		[ReceiptCardName]		nvarchar(50)		'$.paymentInformation.receiptCardName',
		[PaymentProcessorID]	int					'$.paymentInformation.paymentProcessorId',
		[FundingProviderID]		int					'$.paymentInformation.fundingProviderId',
		[FleetData]				nvarchar(max)		'$.paymentInformation.fleetData',			--Really Dictionary
		[UserPaymentSourceExID]	uniqueidentifier	'$.paymentInformation.userPaymentSourceExId',
		[SecurityType]			nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.siteLevelPaymentSecurity',
		[ExpirationDate]		nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.expirationDate',
		[Dpan]					nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.dpan',
		[Data]					nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.data',
		[ExpirationMonth]		int					'$.paymentInformation.siteLevelPaymentContext.ExpirationMonth',
		[ExpirationYear]		int					'$.paymentInformation.siteLevelPaymentContext.ExpirationYear',
		[EciIndicator]			nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.eciIndicator',
		[PaymentAuthLimit]		decimal(9,3)		'$.paymentInformation.authLimits.paymentAuthLimit.Amount',
		[StoreOverrunBuffer]	decimal(9,3)		'$.paymentInformation.authLimits.storeOverrunBuffer.Amount',
		[CalculatedAuthLimit]	decimal(9,3)		'$.paymentInformation.authLimits.calculatedAuthLimit.Amount',

		[SessionID]				nvarchar(50)		'$.createRequest.sessionId',
		[UserLatitude]			decimal(10,7)		'$.createRequest.userLatitude',
		[UserLongitude]			decimal(10,7)		'$.createRequest.userLongitude',
		[DeviceID]				nvarchar(50)		'$.createRequest.deviceId',

		[VerificationMethod]	nvarchar(50)		'$.userVerification.verificationMethod',
		[VerificationData]		nvarchar(255)		'$.userVerification.verificationData',
		[MaxRetryCount]			int					'$.userVerification.maxRetryCount',
		[DisplayPrompt]			nvarchar(255)		'$.userVerification.displayPrompt'
	);


	INSERT INTO @StacDocBody
		([TxnID],[PartnerTxnID],[SiteTxnID],[LoyaltyTxnID],[StoreTenantID],[AppTenantID],[UserID],[StoreID]
		,[MppaID],[AppDisplayName],[AppChannel],[UMTI],[TransactionOwner],[BatchID],[BatchGuid],[StacString2]
		,[StacString],[StacIssuer],[StacToken],[StacExpiration],[StacStatus],[WalletType],[CardIssuer],[CardType]
		,[FirstSix],[LastFour],[AuthLocation],[FinalLocation],[WalletItemData],[PaymentExpiration]
		,[PaymentProviderToken],[PaymentProviderName],[UserPaymentSourceID],[PaymentType],[ReceiptCardName]
		,[PaymentProcessorID],[FundingProviderID],[FleetData],[UserPaymentSourceExID],[SecurityType],[ExpirationDate]
		,[Dpan],[Data],[ExpirationMonth],[ExpirationYear],[EciIndicator],[PaymentAuthLimit],[StoreOverrunBuffer]
		,[CalculatedAuthLimit],[SessionID],[UserLatitude],[UserLongitude],[DeviceID],[VerificationMethod]
		,[VerificationData],[MaxRetryCount],[DisplayPrompt])
	SELECT * FROM OPENJSON(@stac_doc)
	WITH
	(
		[TxnID]					uniqueidentifier	'$.p97TransactionId',
		[PartnerTxnID]			nvarchar(255)		'$.partnerTransactionId',
		[SiteTxnID]				nvarchar(255)		'$.transaction.siteTransactionId',
		[LoyaltyTxnID]			nvarchar(255)		'$.loyaltyTransactionId',
		[StoreTenantID]			uniqueidentifier	'$.storeTenantId',
		[AppTenantID]			uniqueidentifier	'$.appTenantId',
		[UserID]				uniqueidentifier	'$.userId',
		[StoreID]				uniqueidentifier	'$.storeId',
		[MppaID]				nvarchar(10)		'$.mppaId',
		[AppDisplayName]		nvarchar(255)		'$.createRequest.appDisplayName',
		[AppChannel]			nvarchar(100)		'$.createRequest.appChannel',
		[UMTI]					nvarchar(50)		'$.umti',
		[TransactionOwner]		nvarchar(50)		'$.transactionOwner',
		

		[BatchID]				int					'$.batchInformation.batchId',
		[BatchGuid]				uniqueidentifier	'$.batchInformation.batchGuid',

		[stacString]			nvarchar(50)		'$.stacString',
		[StacString2]			nvarchar(50)		'$.stac.stacString',
		[StacIssuer]			nvarchar(50)		'$.stac.stacIssuer',
		[StacToken]				nvarchar(50)		'$.stac.stacToken',
		[StacExpiration]		datetime2			'$.stac.stacExpiration',
		[StacStatus]			nvarchar(50)		'$.stac.stacStatus',

		[WalletType]			nvarchar(50)		'$.paymentInformation.walletType',
		[CardIssuer]			nvarchar(50)		'$.paymentInformation.cardIssuer',
		[CardType]				nvarchar(50)		'$.paymentInformation.cardType',
		[FirstSix]				nvarchar(6)			'$.paymentInformation.firstSix',
		[LastFour]				nvarchar(4)			'$.paymentInformation.lastFour',
		[AuthLocation]			nvarchar(50)		'$.paymentInformation.authLocation',
		[FinalLocation]			nvarchar(50)		'$.paymentInformation.finalLocation',
		[WalletItemData]		nvarchar(max)		'$.paymentInformation.walletItemData',		--Really Dictionary
		[PaymentExpiration]		datetime2			'$.paymentInformation.expiration',
		[PaymentProviderToken]	nvarchar(50)		'$.paymentInformation.paymentProviderToken',
		[PaymentProviderName]	nvarchar(50)		'$.paymentInformation.paymentProviderName',
		[UserPaymentSourceID]	int					'$.paymentInformation.userPaymentSourceId',
		[PaymentType]			nvarchar(50)		'$.paymentInformation.paymentType',
		[ReceiptCardName]		nvarchar(50)		'$.paymentInformation.receiptCardName',
		[PaymentProcessorID]	int					'$.paymentInformation.paymentProcessorId',
		[FundingProviderID]		int					'$.paymentInformation.fundingProviderId',
		[FleetData]				nvarchar(max)		'$.paymentInformation.fleetData',			--Really Dictionary
		[UserPaymentSourceExID]	uniqueidentifier	'$.paymentInformation.userPaymentSourceExId',
		[SecurityType]			nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.siteLevelPaymentSecurity',
		[ExpirationDate]		nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.expirationDate',
		[Dpan]					nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.dpan',
		[Data]					nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.data',
		[ExpirationMonth]		int					'$.paymentInformation.siteLevelPaymentContext.ExpirationMonth',
		[ExpirationYear]		int					'$.paymentInformation.siteLevelPaymentContext.ExpirationYear',
		[EciIndicator]			nvarchar(50)		'$.paymentInformation.siteLevelPaymentContext.eciIndicator',
		[PaymentAuthLimit]		decimal(9,3)		'$.paymentInformation.authLimits.paymentAuthLimit.Amount',
		[StoreOverrunBuffer]	decimal(9,3)		'$.paymentInformation.authLimits.storeOverrunBuffer.Amount',
		[CalculatedAuthLimit]	decimal(9,3)		'$.paymentInformation.authLimits.calculatedAuthLimit.Amount',

		[SessionID]				nvarchar(50)		'$.createRequest.sessionId',
		[UserLatitude]			decimal(10,7)		'$.createRequest.userLatitude',
		[UserLongitude]			decimal(10,7)		'$.createRequest.userLongitude',
		[DeviceID]				nvarchar(50)		'$.createRequest.deviceId',

		[VerificationMethod]	nvarchar(50)		'$.userVerification.verificationMethod',
		[VerificationData]		nvarchar(255)		'$.userVerification.verificationData',
		[MaxRetryCount]			int					'$.userVerification.maxRetryCount',
		[DisplayPrompt]			nvarchar(255)		'$.userVerification.displayPrompt'
	);
	IF EXISTS(SELECT * FROM @TxnDocBody WHERE [TxnID] IS NULL)
	BEGIN
		;THROW 50001, 'Txn Doc must include TxnID', 1;
	END
	ELSE IF EXISTS(SELECT * FROM @StacDocBody WHERE [TxnID] IS NULL)
	BEGIN
		;THROW 50004, 'Txn Doc must include TxnID', 1;
	END
	ELSE IF((SELECT COUNT(*) FROM @StacDocBody) > 1)
	BEGIN
		;THROW 50002, 'Only 1 stac doc allowed', 1;
	END
	ELSE IF((SELECT COUNT(*) FROM @TxnDocBody) > 1)
	BEGIN
		;THROW 50003, 'Only 1 txn doc allowed', 1;
	END
	SET @StacDocTxnID = (SELECT TOP (1) TxnID from @StacDocBody);
	SET @TxnDocTxnID = (SELECT TOP (1) TxnID from @TxnDocBody);
	
	IF NOT EXISTS(SELECT TxnID from @TxnDocBody
						UNION
					   SELECT TxnID from @StacDocBody)
	BEGIN
		;THROW 50005, 'No TxnIDs found from either doc',1;
	END
	ELSE IF (@StacDocTxnID IS NOT NULL AND @TxnDocTxnID IS NOT NULL AND @StacDocTxnID != @TxnDocTxnID)
	BEGIN
		;THROW 50006, 'Stac doc TxnID and Txn doc TxnID must be equal', 1;
	END

	MERGE INTO @TxnDocBody AS target
	USING (
		SELECT * FROM @StacDocBody WHERE [TxnID] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
	WHEN MATCHED THEN
		UPDATE SET
			target.[PartnerTxnID] 			= 		ISNULL(source.[PartnerTxnID],target.[PartnerTxnID]),		
			target.[SiteTxnID]				= 		ISNULL(source.[SiteTxnID],target.[SiteTxnID]),				
			target.[LoyaltyTxnID]			= 		ISNULL(source.[LoyaltyTxnID],target.[LoyaltyTxnID]), 
			target.[StoreTenantID]			= 		ISNULL(source.[StoreTenantID],target.[StoreTenantID]), 
			target.[AppTenantID]			= 		ISNULL(source.[AppTenantID],target.[AppTenantID]), 
			target.[UserID]					= 		ISNULL(source.[UserID],target.[UserID]),
			target.[StoreID]				= 		ISNULL(source.[StoreID],target.[StoreID]), 
			target.[MppaID]					= 		ISNULL(source.[MppaID],target.[MppaID]), 
			target.[AppDisplayName]			= 		ISNULL(source.[AppDisplayName],target.[AppDisplayName]), 
			target.[AppChannel]				= 		ISNULL(source.[AppChannel],target.[AppChannel]), 
			target.[UMTI]					= 		ISNULL(source.[UMTI],target.[UMTI]), 
			target.[TransactionOwner]		= 		ISNULL(source.[TransactionOwner],target.[TransactionOwner]),
			target.[TransactionOrigin]		= 		ISNULL(source.[TransactionOrigin],target.[TransactionOrigin]),
			target.[SettlementPeriodId]		= 		ISNULL(source.[SettlementPeriodId],target.[SettlementPeriodId]),
			target.[TransactionDateTime]	= 		ISNULL(source.[TransactionDateTime],target.[TransactionDateTime]),
			target.[TerminalID]				= 		ISNULL(source.[TerminalID],target.[TerminalID]),
			target.[MonetaryAmount]			= 		ISNULL(source.[MonetaryAmount],target.[MonetaryAmount]),
			target.[CurrencyCode]			= 		ISNULL(source.[CurrencyCode], target.[CurrencyCode]),
			target.[BatchID]				= 		ISNULL(source.[BatchID],target.[BatchID]),
			target.[BatchGuid]				= 		ISNULL(source.[BatchGuid],target.[BatchGuid]),
			target.[StacString]				=		ISNULL(source.[StacString],target.[StacString]),
			target.[StacString2]			=		ISNULL(source.[StacString2],target.[StacString2]),
			target.[StacIssuer]				=		ISNULL(source.[StacIssuer],target.[StacIssuer]),
			target.[StacToken]				=		ISNULL(source.[StacToken],target.[StacToken]),
			target.[StacExpiration]			=		ISNULL(source.[StacExpiration],target.[StacExpiration]),
			target.[StacStatus]				=		ISNULL(source.[StacStatus],target.[StacStatus]),
			target.[WalletType]				=		ISNULL(source.[WalletType],target.[WalletType]),
			target.[CardIssuer]				=		ISNULL(source.[CardIssuer],target.[CardIssuer]),
			target.[CardType]				=		ISNULL(source.[CardType],target.[CardType]),
			target.[FirstSix]				=		ISNULL(source.[FirstSix],target.[FirstSix]),
			target.[LastFour]				=		ISNULL(source.[LastFour],target.[LastFour]),
			target.[AuthLocation]			=		ISNULL(source.[AuthLocation],target.[AuthLocation]),
			target.[FinalLocation]			=		ISNULL(source.[FinalLocation],target.[FinalLocation]),
			target.[PaymentExpiration]		=		ISNULL(source.[PaymentExpiration],target.[PaymentExpiration]),
			target.[PaymentProviderToken]	=		ISNULL(source.[PaymentProviderToken],target.[PaymentProviderToken]),
			target.[PaymentProviderName]	=		ISNULL(source.[PaymentProviderName],target.[PaymentProviderName]),
			target.[UserPaymentSourceID]	=		ISNULL(source.[UserPaymentSourceID],target.[UserPaymentSourceID]),
			target.[PaymentType]			=		ISNULL(source.[PaymentType],target.[PaymentType]),
			target.[ReceiptCardName]		=		ISNULL(source.[ReceiptCardName],target.[ReceiptCardName]),
			target.[PaymentProcessorID]		=		ISNULL(source.[PaymentProcessorID],target.[PaymentProcessorID]),
			target.[FundingProviderID]		=		ISNULL(source.[FundingProviderID],target.[FundingProviderID]),
			target.[FleetData]				=		ISNULL(source.[FleetData],target.[FleetData]),
			target.[UserPaymentSourceExID]	=		ISNULL(source.[UserPaymentSourceExID],target.[UserPaymentSourceExID]),
			target.[SecurityType]			=		ISNULL(source.[SecurityType],target.[SecurityType]),
			target.[ExpirationDate]			=		ISNULL(source.[ExpirationDate],target.[ExpirationDate]),
			target.[Dpan]					=		ISNULL(source.[Dpan],target.[Dpan]),
			target.[Data]					=		ISNULL(source.[Data],target.[Data]),
			target.[ExpirationMonth]		=		ISNULL(source.[ExpirationMonth],target.[ExpirationMonth]),
			target.[ExpirationYear]			=		ISNULL(source.[ExpirationYear],target.[ExpirationYear]),
			target.[EciIndicator]			=		ISNULL(source.[EciIndicator],target.[EciIndicator]),
			target.[PaymentAuthLimit]		=		ISNULL(source.[PaymentAuthLimit],target.[PaymentAuthLimit]),
			target.[StoreOverrunBuffer]		=		ISNULL(source.[StoreOverrunBuffer],target.[StoreOverrunBuffer]),
			target.[CalculatedAuthLimit]	=		ISNULL(source.[CalculatedAuthLimit],target.[CalculatedAuthLimit]),
			target.[SessionID]				=		ISNULL(source.[SessionID],target.[SessionID]),
			target.[UserLatitude]			=		ISNULL(source.[UserLatitude],target.[UserLatitude]),
			target.[UserLongitude]			=		ISNULL(source.[UserLongitude],target.[UserLongitude]),
			target.[DeviceID]				=		ISNULL(source.[DeviceID],target.[DeviceID]),
			target.[VerificationMethod]		=		ISNULL(source.[VerificationMethod],target.[VerificationMethod]),
			target.[VerificationData]		=		ISNULL(source.[VerificationData],target.[VerificationData]),
			target.[MaxRetryCount]			=		ISNULL(source.[MaxRetryCount],target.[MaxRetryCount]),
			target.[DisplayPrompt]			=		ISNULL(source.[DisplayPrompt],target.[DisplayPrompt]);
		


	INSERT INTO @TxnDocPaymentResponse
	SELECT @TxnDocTxnID AS [TxnID]
		,'AUTH'AS [PaymentResponseType]
		,[PaymentResultCode]
		,[ResultText]
		,[ReferenceNumber]
		,[AuthorizedAmount]
		,[AuthorizedCurrency]
		,[AuthorizationCode]
		,[RawRequest]
		,[RawRsponse] 
	FROM OPENJSON(@txn_doc, 'lax $.authResponse')
	WITH
	(
		[PaymentResultCode]		nvarchar(50)		'$.resultCode',
		[ResultText]			nvarchar(max)		'$.resultText',
		[ReferenceNumber]		nvarchar(50)		'$.referenceNumber',
		[AuthorizedAmount]		decimal(9,3)		'$.authorizedAmount.Amount',
		[AuthorizedCurrency]	nvarchar(3)			'$.authorizedAmount.CurrencyCode',
		[AuthorizationCode]		nvarchar(50)		'$.authorizationCode',
		[RawRequest]			nvarchar(max)		'$.rawRequest',
		[RawRsponse]			nvarchar(max)		'$.rawResponse'
	)
	WHERE [ResultText] IS NOT NULL 
		OR [ReferenceNumber] IS NOT NULL 
		OR [AuthorizedAmount] IS NOT NULL 
		OR [AuthorizedCurrency] IS NOT NULL 
		OR [AuthorizationCode] IS NOT NULL 
		OR [RawRequest] IS NOT NULL 
		OR [RawRsponse] IS NOT NULL;
	INSERT INTO @TxnDocPaymentResponse
	SELECT @TxnDocTxnID AS [TxnID]
		,'FINAL' AS [PaymentResponseType]
		,[PaymentResultCode]
		,[ResultText]
		,[ReferenceNumber]
		,[AuthorizedAmount]
		,[AuthorizedCurrency]
		,[AuthorizationCode]
		,[RawRequest]
		,[RawRsponse] 
	FROM OPENJSON(@txn_doc, 'lax $.finalResponses')
	WITH
	(
		[PaymentResultCode]		nvarchar(50)		'$.resultCode',
		[ResultText]			nvarchar(max)		'$.resultText',
		[ReferenceNumber]		nvarchar(50)		'$.referenceNumber',
		[AuthorizedAmount]		decimal(9,3)		'$.authorizedAmount.Amount',
		[AuthorizedCurrency]	nvarchar(3)			'$.authorizedAmount.CurrencyCode',
		[AuthorizationCode]		nvarchar(50)		'$.authorizationCode',
		[RawRequest]			nvarchar(max)		'$.rawRequest',
		[RawRsponse]			nvarchar(max)		'$.rawResponse'
	)
	WHERE [ResultText] IS NOT NULL 
		OR [ReferenceNumber] IS NOT NULL 
		OR [AuthorizedAmount] IS NOT NULL 
		OR [AuthorizedCurrency] IS NOT NULL 
		OR [AuthorizationCode] IS NOT NULL 
		OR [RawRequest] IS NOT NULL 
		OR [RawRsponse] IS NOT NULL;


	INSERT INTO @TxnDocFuelEvent
		([TxnID],[EventType],[FuelEventStatus])
	SELECT 
		@TxnDocTxnID AS [TxnID],
		 [EventType],
		 [FuelEventStatus]
	FROM OPENJSON(@txn_doc, 'lax $.fuelEvents')
	WITH
	(
		[EventType]				nvarchar(50)		'$.eventType',
		[FuelEventStatus]		nvarchar(50)		'$.status'
	)
	WHERE [EventType] IS NOT NULL OR [FuelEventStatus] IS NOT NULL;

	INSERT INTO @TxnDocLoyaltyInstrument 
		([TxnID],[LoyaltyPAN])
	SELECT 
		@TxnDocTxnID AS [TxnID],
		[LoyaltyPAN]
	FROM OPENJSON(@txn_doc, 'lax $.loyaltyInformation')
	WITH
	(
		[LoyaltyPAN]			nvarchar(255)		'$.loyaltyPan'
	)
	WHERE [LoyaltyPAN] IS NOT NULL;
	INSERT INTO @TxnDocLoyaltyInstrument
	SELECT 
		@TxnDocTxnID AS [TxnID],
		[LoyaltyPAN],
		[LoyaltyPANType],
		[LoyaltyProgramID],
		[RedemptionValue],
		[ProcessAboveSite]
	FROM OPENJSON(@txn_doc, 'lax $.loyaltyInformation')
	WITH
	(
		[LoyaltyPAN]			nvarchar(255)		'$.loyaltyInstruments.pan',
		[LoyaltyPANType]		nvarchar(50)		'$.loyaltyInformation.loyaltyInstruments.panType',
		[LoyaltyProgramID]		uniqueidentifier	'$.loyaltyInformation.loyaltyInstruments.programId',
		[RedemptionValue]		decimal(9,3)		'$.loyaltyInformation.loyaltyInstruments.redemptionValue',
		[ProcessAboveSite]		bit					'$.loyaltyInformation.loyaltyInstruments.processAboveSite'
	)
	WHERE [LoyaltyPAN] IS NOT NULL 
		OR [LoyaltyProgramID] IS NOT NULL 
		OR [RedemptionValue] IS NOT NULL 
		OR [LoyaltyPANType] IS NOT NULL 
		OR [ProcessAboveSite] IS NOT NULL;

	INSERT INTO @TxnDocLineItem
		([TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier],[ProductCode],[ProductDescription]
		,[StandardProductCategory],[ReverseSale],[EvaluateOnly],[PriceChangeEligible],[LinkedItemID],[OutdoorPosition]
		,[PriceTier],[ServiceLevel],[WashCode],[WashExpiration],[WashText],[UnitOfMeasure],[Quantity]
		,[SellingUnits],[OriginalAmount],[OriginalUnitPrice],[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]
		,[PromotionReason],[DiscountAmount],[DiscountUnitPrice],[PromotionQuantity],[MaximumQuantity],[RebateLabel]
		,[RewardApplied],[PriceAdjustmentID],[ProgramID])
	SELECT 
		@TxnDocTxnID AS [TxnID]
		,a.[SiteLineItemID]			
		,a.[StandardProductCodeID]		
		,a.[PosCode]					
		,a.[PosCodeModifier]			
		,a.[ProductCode]				
		,a.[ProductDescription]		
		,a.[StandardProductCategory]	
		,a.[ReverseSale]				
		,a.[EvaluateOnly]				
		,a.[PriceChangeEligible]		
		,a.[LinkedItemID]				
		,a.[OutdoorPosition]			
		,a.[PriceTier]					
		,a.[ServiceLevel]				
		,a.[WashCode]					
		,a.[WashExpiration]			
		,a.[WashText]					
		,a.[UnitOfMeasure]				
		,a.[Quantity]					
		,a.[SellingUnits]				
		,a.[OriginalAmount]			
		,a.[OriginalUnitPrice]			
		,a.[AdjustedAmount]			
		,a.[AdjustedUnitPrice]			
		,a.[TaxCode]			
		,b.[PromotionReason]	
		,b.[DiscountAmount]	
		,b.[DiscountUnitPrice]	
		,b.[Quantity]			
		,b.[MaximumQuantity]	
		,b.[RebateLabel]		
		,b.[RewardApplied]		
		,b.[PriceAdjustmentID]	
		,b.[ProgramID]					
	FROM OPENJSON(@txn_doc, 'lax $.transaction.saleItems')
	WITH
	(
		[SiteLineItemID]			nvarchar(50)		'$.itemId',
		[StandardProductCodeID]		nvarchar(50)		'$.standardProductCode',
		[PosCode]					nvarchar(50)		'$.posCode',
		[PosCodeModifier]			nvarchar(50)		'$.posCodeModifier',
		[ProductCode]				nvarchar(50)		'$.productCode',
		[ProductDescription]		nvarchar(255)		'$.description',
		[StandardProductCategory]	nvarchar(50)		'$.category',
		[ReverseSale]				nvarchar(50)		'$.reverseSale',
		[EvaluateOnly]				bit					'$.evaluateOnly',
		[PriceChangeEligible]		bit					'$.priceChangeEligible',
		[LinkedItemID]				nvarchar(50)		'$.linkedItemId',
		[OutdoorPosition]			int					'$.fuelDetails.outdoorPosition',
		[PriceTier]					nvarchar(50)		'$.fuelDetails.priceTier',
		[ServiceLevel]				nvarchar(50)		'$.fuelDetails.serviceLevel',
		[WashCode]					nvarchar(50)		'$.washCodeDetails.code',		--[CarWashCode]
		[WashExpiration]			datetime			'$.washCodeDetails.expiration',		--[CarWashCodeExpiration]
		[WashText]					nvarchar(255)		'$.washCodeDetails.text',		--[CarWashText]
		[UnitOfMeasure]				nvarchar(20)		'$.unitMeasure',	--[UnitOfMeasure]
		[Quantity]					decimal(9,3)		'$.quantity',	--[Quantity]
		[SellingUnits]				int					'$.sellingUnits',		--[SellingUnit]
		[OriginalAmount]			decimal(9,3)		'$.originalAmount.amount',
		[OriginalUnitPrice]			decimal(9,3)		'$.originalAmount.unitPrice',
		[AdjustedAmount]			decimal(9,3)		'$.adjustedAmount.amount',
		[AdjustedUnitPrice]			decimal(9,3)		'$.adjustedAmount.unitPrice',
		[TaxCode]					nvarchar(50)		'$.taxCode',
		[PriceAdjustments]			nvarchar(max)		'$.priceAdjustments' AS JSON
	) AS a
	OUTER APPLY OPENJSON(a.[PriceAdjustments])
	WITH
	(
		[PromotionReason]			nvarchar(50)		'$.promotionReason',
		[DiscountAmount]			decimal(9,3)		'$.amount',
		[DiscountUnitPrice]			decimal(9,3)		'$.unitPrice',
		[Quantity]					decimal(9,3)		'$.quantity',
		[MaximumQuantity]			decimal(9,3)		'$.maximumQuantity',
		[RebateLabel]				nvarchar(50)		'$.rebateLabel',
		[RewardApplied]				bit					'$.rewardApplied',
		[PriceAdjustmentID]			nvarchar(50)		'$.promotionAdjustmentId',
		[ProgramID]					nvarchar(50)		'$.programID'
	) AS b


	DELETE FROM @TxnDocLineItem
	WHERE [SiteLineItemID]				IS NULL	
		AND [StandardProductCodeID]		IS NULL
		AND [PosCode]					IS NULL
		AND [PosCodeModifier]			IS NULL
		AND [ProductCode]				IS NULL
		AND [ProductDescription]		IS NULL
		AND [StandardProductCategory]	IS NULL
		AND [ReverseSale]				IS NULL
		AND [EvaluateOnly]				IS NULL
		AND [PriceChangeEligible]		IS NULL
		AND [LinkedItemID]				IS NULL
		AND [OutdoorPosition]			IS NULL
		AND [PriceTier]					IS NULL
		AND [ServiceLevel]				IS NULL
		AND [WashCode]					IS NULL
		AND [WashExpiration]			IS NULL
		AND [WashText]					IS NULL
		AND [UnitOfMeasure]				IS NULL
		AND [Quantity]					IS NULL
		AND [SellingUnits]				IS NULL
		AND [OriginalAmount]			IS NULL
		AND [OriginalUnitPrice]			IS NULL
		AND [AdjustedAmount]			IS NULL
		AND [AdjustedUnitPrice]			IS NULL
		AND [TaxCode]					IS NULL;

	INSERT INTO @TxnDocLineItem2
		([TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
		,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
		,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
		,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
		,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode])
	SELECT NEWID() AS [TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
			,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]
		FROM
		(
			SELECT [TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
				,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
				,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
				,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
				,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]
			FROM @TxnDocLineItem
			GROUP BY [TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
				,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
				,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
				,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
				,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]
		) AS tbl2


	UPDATE A
	SET [TxnLineItemID] = B.[TxnLineItemID]
	--SELECT B.[TxnLineItemID], A.*
	FROM @TxnDocLineItem A
	LEFT JOIN @TxnDocLineItem2 B
		ON A.[TxnID] = B.[TxnID] AND
			ISNULL(A.[SiteLineItemID], '') = ISNULL(B.[SiteLineItemID], '') AND
			ISNULL(A.[StandardProductCodeID], '') = ISNULL(B.[StandardProductCodeID], '') AND
			ISNULL(A.[PosCode], '') = ISNULL(B.[PosCode], '') AND
			ISNULL(A.[PosCodeModifier], '') = ISNULL(B.[PosCodeModifier], '') AND
			ISNULL(A.[ProductCode], '') = ISNULL(B.[ProductCode], '') AND
			ISNULL(A.[ProductDescription], '') = ISNULL(B.[ProductDescription], '') AND
			ISNULL(A.[StandardProductCategory], '') = ISNULL(B.[StandardProductCategory], '') AND
			ISNULL(A.[ReverseSale], '') = ISNULL(B.[ReverseSale], '') AND
			ISNULL(A.[EvaluateOnly], 0) = ISNULL(B.[EvaluateOnly], 0) AND
			ISNULL(A.[PriceChangeEligible], 0) = ISNULL(B.[PriceChangeEligible], 0) AND
			ISNULL(A.[LinkedItemID], '') = ISNULL(B.[LinkedItemID], '') AND
			ISNULL(A.[OutdoorPosition], -1) = ISNULL(B.[OutdoorPosition], -1) AND
			ISNULL(A.[PriceTier], '') = ISNULL(B.[PriceTier], '') AND
			ISNULL(A.[ServiceLevel], '') = ISNULL(B.[ServiceLevel], '') AND
			ISNULL(A.[WashCode], '') = ISNULL(B.[WashCode], '') AND
			ISNULL(A.[WashExpiration], GETUTCDATE()) = ISNULL(B.[WashExpiration], GETUTCDATE()) AND
			ISNULL(A.[WashText], '') = ISNULL(B.[WashText], '') AND
			ISNULL(A.[UnitOfMeasure], '') = ISNULL(B.[UnitOfMeasure], '') AND
			ISNULL(A.[Quantity], -1) = ISNULL(B.[Quantity], -1) AND
			ISNULL(A.[SellingUnits], -1) = ISNULL(B.[SellingUnits], -1) AND
			ISNULL(A.[OriginalAmount], -1) = ISNULL(B.[OriginalAmount], -1) AND
			ISNULL(A.[OriginalUnitPrice], -1) = ISNULL(B.[OriginalUnitPrice], -1) AND
			ISNULL(A.[AdjustedAmount], -1) = ISNULL(B.[AdjustedAmount], -1) AND
			ISNULL(A.[AdjustedUnitPrice], -1) = ISNULL(B.[AdjustedUnitPrice], -1) AND
			ISNULL(A.[TaxCode], '') = ISNULL(B.[TaxCode], '')


	INSERT INTO @StacDocLoyaltyInstrument 
		([TxnID],[LoyaltyPAN])
	SELECT 
		@TxnDocTxnID AS [TxnID],
		[LoyaltyPAN]
	FROM OPENJSON(@stac_doc, 'lax $.loyaltyInformation')
	WITH
	(
		[LoyaltyPAN]			nvarchar(255)		'$.loyaltyPan'
	)
	WHERE [LoyaltyPAN] IS NOT NULL;
	INSERT INTO @StacDocLoyaltyInstrument
	SELECT
		@TxnDocTxnID AS [TxnID],
		[LoyaltyPAN],
		[LoyaltyPANType],
		[LoyaltyProgramID],
		[RedemptionValue],
		[ProcessAboveSite]
	FROM OPENJSON(@stac_doc, 'lax $.loyaltyInformation.loyaltyInstruments')
	WITH
	(
		[LoyaltyPAN]			nvarchar(255)		'$.pan',
		[LoyaltyPANType]		nvarchar(50)		'$.panType',
		[LoyaltyProgramID]		uniqueidentifier	'$.programId',
		[RedemptionValue]		decimal(9,3)		'$.redemptionValue',
		[ProcessAboveSite]		bit					'$.processAboveSite'
	)
	WHERE [LoyaltyPAN] IS NOT NULL 
		OR [LoyaltyProgramID] IS NOT NULL 
		OR [RedemptionValue] IS NOT NULL 
		OR [LoyaltyPANType] IS NOT NULL 
		OR [ProcessAboveSite] IS NOT NULL;



	
	MERGE INTO @TxnDocLoyaltyInstrument AS target
	USING (
		SELECT * FROM @StacDocLoyaltyInstrument WHERE [TxnID] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID 
	AND target.[LoyaltyPAN] = source.[LoyaltyPAN]
	WHEN MATCHED THEN
		UPDATE SET
			target.[LoyaltyPAN]		=	ISNULL(source.[LoyaltyPAN],target.[LoyaltyPAN]),
			target.[LoyaltyPANType] =	ISNULL(source.[LoyaltyPANType],target.[LoyaltyPANType]),
			target.[LoyaltyProgramID] =	ISNULL(source.[LoyaltyProgramID],target.[LoyaltyProgramID]),
			target.[RedemptionValue] =	ISNULL(source.[RedemptionValue],target.[RedemptionValue]),
			target.[ProcessAboveSite] = ISNULL(source.[ProcessAboveSite],target.[ProcessAboveSite]);


	IF (@isUpdate = 1)
	BEGIN
		EXEC [Txn].[TxnDoc_Update] @TxnDocBody, @TxnDocPaymentResponse, @TxnDocFuelEvent, @TxnDocLoyaltyInstrument, @TxnDocLineItem;
	END
	ELSE IF (@isInsert = 1)
	BEGIN
		EXEC [Txn].[TxnDoc_Insert] @TxnDocBody, @TxnDocPaymentResponse, @TxnDocFuelEvent, @TxnDocLoyaltyInstrument, @TxnDocLineItem;
	END

    COMMIT TRANSACTION
	RETURN;
END TRY
BEGIN CATCH
	
	
	DECLARE @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
	(SELECT 
		@txn_doc AS [Txn_Doc],
		@stac_doc AS [Stac_Doc]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
	
	DECLARE @Error_Message nvarchar(4000) = ERROR_MESSAGE(), @Error_Severity INT = ERROR_SEVERITY(), @Error_State INT = ERROR_STATE();
	
	RAISERROR (@Error_Message, 
               @Error_Severity, 
               @Error_State  
              );  
	

END CATCH
END

