﻿
/******************************************************************************************************************************************************/

CREATE   PROCEDURE [Txn].[PUT_TxnDoc] @txn_doc nvarchar(max), @stac_doc nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
BEGIN TRY
	BEGIN TRANSACTION

	EXEC [Txn].[Serialize_TxnDoc] @txn_doc, @stac_doc, 1, 0;

	COMMIT TRANSACTION

END TRY
BEGIN CATCH
	DECLARE @xstate int = XACT_STATE(), @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT 
		@txn_doc AS [Txn_Doc],
		@stac_doc AS [Stac_Doc]
	FOR JSON PATH) AS [StoredProcedureInput];
	
	ROLLBACK TRANSACTION

	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
END CATCH
END
