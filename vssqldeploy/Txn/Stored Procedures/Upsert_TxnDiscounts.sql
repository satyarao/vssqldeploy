﻿

--====================================================================
--UpdatedBy Trace
--11/20/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 401
--====================================================================
CREATE   PROCEDURE [Txn].[Upsert_TxnDiscounts] @json nvarchar(max) AS
BEGIN
BEGIN TRY
	BEGIN TRANSACTION


	DECLARE @finalizedLoyalty TABLE (
		[TxnID]					uniqueidentifier,
		[LineNumber]			nvarchar(50),
		[PosCode]				nvarchar(20),
		[Quantity]				decimal(9,3),
		[OriginalUnitPrice]		decimal(9,3),
		[AdjustedUnitPrice]		decimal(9,3),
		[OfferID]				uniqueidentifier,
		[DiscountAmount]		decimal(9,3),
		[DiscountUnitPrice]		decimal(9,3)
	);

	DECLARE @txnLineItem TABLE (
		[TxnID]					uniqueidentifier,
		[TxnLineItemID]			uniqueidentifier,
		[LineNumber]			nvarchar(50),
		[PosCode]				nvarchar(20),
		[Quantity]				decimal(9,3),
		[OriginalUnitPrice]		decimal(9,3),
		[AdjustedUnitPrice]		decimal(9,3)
	);

	DECLARE @txnLineItemDiscount TABLE (
		[TxnLineItemID]			uniqueidentifier,
		[OfferID]				uniqueidentifier,
		[DiscountAmount]		decimal(9,3),
		[DiscountUnitPrice]		decimal(9,3),
		[Quantity]				decimal(9,3)
	);

	INSERT INTO @finalizedLoyalty
		([TxnID],[LineNumber],[PosCode],[Quantity],[OriginalUnitPrice],[AdjustedUnitPrice],[OfferID],[DiscountAmount],[DiscountUnitPrice])
	SELECT	[TxnID],
			[LineNumber],
			[PosCode],
			[Quantity],
			[OriginalUnitPrice],
			[AdjustedUnitPrice],
			[OfferID],
			[DiscountAmount],
			[DiscountUnitPrice]
	FROM [Txn].[GetRewardsFromFinalizedDiscounts](@json)


	INSERT INTO @txnLineItem
		([TxnID],[TxnLineItemID],[LineNumber],[PosCode],[Quantity],[OriginalUnitPrice],[AdjustedUnitPrice])
	SELECT inp.[TxnID],
		MAX(tli.[TxnLineItemID]),
		inp.[LineNumber],
		inp.[PosCode],
		inp.[Quantity],
		inp.[OriginalUnitPrice],
		inp.[AdjustedUnitPrice]
	FROM @finalizedLoyalty inp
	INNER JOIN [Txn].[TxnLineItem] tli
		ON tli.[TxnID] = inp.[TxnID] AND tli.[SiteLineItemID] = inp.[LineNumber]
	GROUP BY inp.[TxnID], inp.[LineNumber], inp.[PosCode], inp.[Quantity], inp.[OriginalUnitPrice], inp.[AdjustedUnitPrice]

	INSERT INTO @txnLineItemDiscount
		([TxnLineItemID],[OfferID],[DiscountAmount],[DiscountUnitPrice],[Quantity])
	SELECT 
		tli.[TxnLineItemID],
		inp.[OfferID],
		inp.[DiscountAmount],
		inp.[DiscountUnitPrice],
		inp.[Quantity]
	FROM @finalizedLoyalty inp
	INNER JOIN [Txn].[TxnLineItem] tli
		ON tli.[TxnID] = inp.[TxnID] AND tli.[SiteLineItemID] = inp.[LineNumber]


	MERGE INTO [Txn].[TxnLineItemDiscount] AS target
	USING @txnLineItemDiscount AS source
	ON source.[TxnLineItemID] = target.[TxnLineItemID] AND target.[OfferID] = source.[OfferID]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnLineItemDiscountID],[TxnLineItemID],[OfferID],[DiscountAmount],[DiscountUnitPrice],[Quantity],[RewardApplied])
		VALUES
			(NEWID(),[TxnLineItemID],[OfferID],[DiscountAmount],[DiscountUnitPrice],[Quantity],1);

    COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType], @ErrorMessage NVARCHAR(4000), @ErrorSeverity INT, @ErrorState INT;
	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), @ErrorMessage, @ErrorSeverity,
    (SELECT @json AS [Json] FOR JSON PATH) AS [StoredProcedureInput];

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
	
	IF(@@TRANCOUNT > 0)
		ROLLBACK TRANSACTION;

    RAISERROR( @ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END

