﻿
/******************************************************************************************************************************************************/

CREATE   PROCEDURE [Txn].[TxnDoc_Insert] (
	@Txn						[Txn].[TxnDocBodyType]				READONLY, 
	@TxnPaymentResponse			[Txn].[TxnDocPaymentResponseType]	READONLY,
	@TxnFuelEvent				[Txn].[TxnDocFuelEventType]			READONLY,
	@LKTxnLoyaltyInstruments	[Txn].[TxnDocLoyaltyInstrumentType]	READONLY,
	@TxnLineItem				[Txn].[TxnDocLineItemType]			READONLY
) AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
BEGIN TRY
	
	IF EXISTS(SELECT * FROM [Txn].[Txn] db INNER JOIN @Txn v ON db.TxnID = v.TxnID)
	BEGIN
		;THROW 50003, 'ERROR:  Attempting to insert duplicate record', 1;
	END
	
	INSERT INTO [Txn].[Txn]
		([TxnID],[PartnerTxnID],[SiteTxnID],[LoyaltyTxnID],[StoreTenantID],[AppTenantID]
		,[UserID],[StoreID],[MppaID],[AppDisplayName],[AppChannel]
		,[UMTI],[TransactionOwner],[TransactionOrigin],[SettlementPeriodId],[TransactionDateTime]
		,[TerminalID],[MonetaryAmount],[CurrencyCode],[BatchID],[BatchGuid])
	SELECT [TxnID],[PartnerTxnID],[SiteTxnID],[LoyaltyTxnID],[StoreTenantID],[AppTenantID]
		,[UserID],[StoreID],[MppaID],[AppDisplayName],[AppChannel]
		,[UMTI],[TransactionOwner],[TransactionOrigin],[SettlementPeriodId],[TransactionDateTime]
		,[TerminalID],[MonetaryAmount],[CurrencyCode],[BatchID],[BatchGuid]
	FROM @Txn
	
	INSERT INTO [Txn].[TxnSessionInfo]
		([TxnID],[SessionID],[UserLatitude],[UserLongitude],[DeviceID])
	SELECT [TxnID],[SessionID],[UserLatitude],[UserLongitude],[DeviceID]
	FROM @Txn
	WHERE [SessionID] IS NOT NULL OR [UserLatitude] IS NOT NULL OR [UserLongitude] IS NOT NULL OR [DeviceID] IS NOT NULL
	
	INSERT INTO [Txn].[TxnUserVerification]
		([TxnID],[VerificationMethod],[VerificationData],[MaxRetryCount],[DisplayPrompt])
	SELECT [TxnID],[VerificationMethod],[VerificationData],[MaxRetryCount],[DisplayPrompt]
	FROM @Txn
	WHERE [VerificationMethod] IS NOT NULL OR [VerificationData] IS NOT NULL OR [MaxRetryCount] IS NOT NULL OR [DisplayPrompt] IS NOT NULL
	
	INSERT INTO [Txn].[TxnStacInfo]
		([TxnID],[StacString],[StacIssuer],[StacToken],[StacExpiration],[StacStatus])
	SELECT [TxnID],CASE WHEN [StacString] IS NULL THEN [StacString2] ELSE [StacString] END AS [StacStrting],[StacIssuer],[StacToken],[StacExpiration],[StacStatus]
	FROM @Txn
	WHERE [StacString] IS NOT NULL OR [StacString2] IS NOT NULL
	
	INSERT INTO [Txn].[TxnPaymentInfo]
		([TxnID],[WalletType],[CardIssuer],[CardType],[FirstSix],[LastFour],[AuthLocation]
		,[FinalLocation],[WalletItemData],[PaymentExpiration],[PaymentProviderToken],[PaymentProviderName]
		,[UserPaymentSourceID],[PaymentType],[ReceiptCardName],[PaymentProcessorID],[FundingProviderID]
		,[FleetData],[UserPaymentSourceExID],[SecurityType],[ExpirationDate],[Dpan],[Data],[ExpirationMonth]
		,[ExpirationYear],[EciIndicator],[PaymentAuthLimit],[StoreOverrunBuffer],[CalculatedAuthLimit])
	SELECT 
		 [TxnID],[WalletType],[CardIssuer],[CardType],[FirstSix],[LastFour],[AuthLocation]
		,[FinalLocation],[WalletItemData],[PaymentExpiration],[PaymentProviderToken],[PaymentProviderName]
		,[UserPaymentSourceID],[PaymentType],[ReceiptCardName],[PaymentProcessorID],[FundingProviderID]
		,[FleetData],[UserPaymentSourceExID],[SecurityType],[ExpirationDate],[Dpan],[Data],[ExpirationMonth]
		,[ExpirationYear],[EciIndicator],[PaymentAuthLimit],[StoreOverrunBuffer],[CalculatedAuthLimit]
	FROM @Txn
	WHERE [WalletType] IS NOT NULL OR [CardIssuer] IS NOT NULL OR [CardType] IS NOT NULL OR [FirstSix] IS NOT NULL OR [LastFour] IS NOT NULL OR [AuthLocation]
		 IS NOT NULL OR [FinalLocation] IS NOT NULL OR [WalletItemData] IS NOT NULL OR [PaymentExpiration] IS NOT NULL OR [PaymentProviderToken] IS NOT NULL OR [PaymentProviderName]
		 IS NOT NULL OR [UserPaymentSourceID] IS NOT NULL OR [PaymentType] IS NOT NULL OR [ReceiptCardName] IS NOT NULL OR [PaymentProcessorID] IS NOT NULL OR [FundingProviderID]
		 IS NOT NULL OR [FleetData] IS NOT NULL OR [UserPaymentSourceExID] IS NOT NULL OR [SecurityType] IS NOT NULL OR [ExpirationDate] IS NOT NULL OR [Dpan] IS NOT NULL OR [Data] IS NOT NULL OR [ExpirationMonth]
		 IS NOT NULL OR [ExpirationYear] IS NOT NULL OR [EciIndicator] IS NOT NULL OR [PaymentAuthLimit] IS NOT NULL OR [StoreOverrunBuffer] IS NOT NULL OR [CalculatedAuthLimit] IS NOT NULL
	
	IF EXISTS(SELECT * FROM @TxnPaymentResponse)
	BEGIN
		INSERT INTO [Txn].[TxnPaymentResponse]
			([TxnPaymentResponseID],[TxnID],[PaymentResponseType],[PaymentResultCode],[ResultText],[ReferenceNumber]
			,[AuthorizedAmount],[AuthorizedCurrency],[AuthorizationCode],[RawRequest],[RawRsponse])
		SELECT 
			 NEWID() AS [TxnPaymentResponseID],[TxnID],[PaymentResponseType],[PaymentResultCode],[ResultText],[ReferenceNumber]
			,[AuthorizedAmount],[AuthorizedCurrency],[AuthorizationCode],[RawRequest],[RawRsponse]
		FROM @TxnPaymentResponse
	END
	
	IF EXISTS(SELECT * FROM @TxnFuelEvent)
	BEGIN
		INSERT INTO [Txn].[TxnFuelEvent]
			([TxnFuelEventID],[TxnID],[EventType],[FuelEventStatus])
		SELECT 
			NEWID() AS [TxnFuelEventID],[TxnID],[EventType],[FuelEventStatus]
		FROM @TxnFuelEvent
	END

	IF EXISTS(SELECT * FROM @LKTxnLoyaltyInstruments)
	BEGIN
		INSERT INTO [Txn].[LKTxnLoyaltyInstruments]
			([TxnID],[LoyaltyPAN],[LoyaltyPANType],[LoyaltyProgramID],[RedemptionValue],[ProcessAboveSite])
		SELECT 
			[TxnID],[LoyaltyPAN],[LoyaltyPANType],[LoyaltyProgramID],[RedemptionValue],[ProcessAboveSite]
		FROM @LKTxnLoyaltyInstruments
	END

	IF EXISTS(SELECT * FROM @TxnLineItem)
	BEGIN
		INSERT INTO [Txn].[TxnLineItem]
			([TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
			,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode])
		SELECT 
			 [TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
			,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]
		FROM @TxnLineItem
		GROUP BY 
			 [TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
			,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]

		INSERT INTO [Txn].[TxnLineItemDiscount]
			([TxnLineItemDiscountID],[TxnLineItemID],[PromotionReason],[DiscountAmount],[DiscountUnitPrice],[Quantity]
			,[MaximumQuantity],[RebateLabel],[RewardApplied],[PriceAdjustmentID],[ProgramID])
		SELECT 
			NEWID() AS [TxnLineItemDiscountID],[TxnLineItemID],[PromotionReason],[DiscountAmount],[DiscountUnitPrice],[Quantity]
			,[MaximumQuantity],[RebateLabel],[RewardApplied],[PriceAdjustmentID],[ProgramID]
		FROM @TxnLineItem
		WHERE [PromotionReason] IS NOT NULL OR [DiscountAmount] IS NOT NULL OR [DiscountUnitPrice] IS NOT NULL OR [Quantity]
			 IS NOT NULL OR [MaximumQuantity] IS NOT NULL OR [RebateLabel] IS NOT NULL OR [RewardApplied] IS NOT NULL OR [PriceAdjustmentID] IS NOT NULL OR [ProgramID] IS NOT NULL
	
	END 

END TRY
BEGIN CATCH
	PRINT ERROR_MESSAGE()
	DECLARE @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
	(SELECT 
		 (SELECT * FROM @Txn FOR JSON PATH)[Txn]
		,(SELECT * FROM @TxnPaymentResponse FOR JSON PATH )[TxnPaymentResponse]
		,(SELECT * FROM @TxnFuelEvent FOR JSON PATH)[TxnFuelEvent]
		,(SELECT * FROM @LKTxnLoyaltyInstruments FOR JSON PATH)[LKTxnLoyaltyInstruments]
		,(SELECT * FROM @TxnLineItem FOR JSON PATH)[TxnLineItem]
	FOR JSON PATH) AS [StoredProcedureInput]; 
	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
END CATCH
END
