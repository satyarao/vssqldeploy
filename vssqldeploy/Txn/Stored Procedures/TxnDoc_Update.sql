﻿
CREATE   PROCEDURE [Txn].[TxnDoc_Update] (
	@Txn						[Txn].[TxnDocBodyType]				READONLY, 
	@TxnPaymentResponse			[Txn].[TxnDocPaymentResponseType]	READONLY,
	@TxnFuelEvent				[Txn].[TxnDocFuelEventType]			READONLY,
	@LKTxnLoyaltyInstruments	[Txn].[TxnDocLoyaltyInstrumentType]	READONLY,
	@TxnLineItem				[Txn].[TxnDocLineItemType]			READONLY
) AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
BEGIN TRY
	BEGIN TRANSACTION

	MERGE INTO [Txn].[Txn] AS target
	USING (
		SELECT * FROM @Txn WHERE [TxnID] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
	WHEN MATCHED THEN
		UPDATE SET
			target.[PartnerTxnID]			= source.[PartnerTxnID]			,
			target.[SiteTxnID]				= source.[SiteTxnID]			,
			target.[LoyaltyTxnID]			= source.[LoyaltyTxnID]			,
			target.[StoreTenantID]			= source.[StoreTenantID]		,
			target.[AppTenantID]			= source.[AppTenantID]			,
			target.[UserID]					= source.[UserID]				,
			target.[StoreID]				= source.[StoreID]				,
			target.[MppaID]					= source.[MppaID]				,
			target.[AppDisplayName]			= source.[AppDisplayName]		,
			target.[AppChannel]				= source.[AppChannel]			,
			target.[UMTI]					= source.[UMTI]					,
			target.[TransactionOwner]		= source.[TransactionOwner]		,
			target.[TransactionOrigin]		= source.[TransactionOrigin]	,
			target.[SettlementPeriodId]		= source.[SettlementPeriodId]	,
			target.[TransactionDateTime]	= source.[TransactionDateTime]	,
			target.[TerminalID]				= source.[TerminalID]			,
			target.[MonetaryAmount]			= source.[MonetaryAmount]		,
			target.[CurrencyCode]			= source.[CurrencyCode]			,
			target.[BatchID]				= source.[BatchID]				,
			target.[BatchGuid]				= source.[BatchGuid]			
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnID],[PartnerTxnID],[SiteTxnID],[LoyaltyTxnID],[StoreTenantID],[AppTenantID]
			,[UserID],[StoreID],[MppaID],[AppDisplayName],[AppChannel],[UMTI],[TransactionOwner]
			,[TransactionOrigin],[SettlementPeriodId],[TransactionDateTime],[TerminalID]
			,[MonetaryAmount],[CurrencyCode],[BatchID],[BatchGuid])
		VALUES
			([TxnID],[PartnerTxnID],[SiteTxnID],[LoyaltyTxnID],[StoreTenantID],[AppTenantID]
			,[UserID],[StoreID],[MppaID],[AppDisplayName],[AppChannel],[UMTI],[TransactionOwner]
			,[TransactionOrigin],[SettlementPeriodId],[TransactionDateTime],[TerminalID]
			,[MonetaryAmount],[CurrencyCode],[BatchID],[BatchGuid]);

	--------------------[Txn].[TxnSessionInfo]---------------
	MERGE INTO [Txn].[TxnSessionInfo] AS target
	USING (
		SELECT * FROM @Txn 
		WHERE [SessionID] IS NOT NULL OR [UserLatitude] IS NOT NULL OR [UserLongitude] IS NOT NULL OR [DeviceID] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
	WHEN MATCHED THEN
		UPDATE SET
			target.[SessionID]			= source.[SessionID],
			target.[UserLatitude]		= source.[UserLatitude],
			target.[UserLongitude]		= source.[UserLongitude],
			target.[DeviceID]			= source.[DeviceID]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnID],[SessionID],[UserLatitude],[UserLongitude],[DeviceID])
		VALUES
			([TxnID],[SessionID],[UserLatitude],[UserLongitude],[DeviceID]);

	--------------------[Txn].[TxnUserVerification]---------------
	MERGE INTO [Txn].[TxnUserVerification] AS target
	USING (
		SELECT * FROM @Txn 
		WHERE [VerificationMethod] IS NOT NULL OR [VerificationData] IS NOT NULL OR [MaxRetryCount] IS NOT NULL OR [DisplayPrompt] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
	WHEN MATCHED THEN
		UPDATE SET
			target.[VerificationMethod]	= source.[VerificationMethod],
			target.[VerificationData]	= source.[VerificationData],
			target.[MaxRetryCount]		= source.[MaxRetryCount],
			target.[DisplayPrompt]		= source.[DisplayPrompt]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnID],[VerificationMethod],[VerificationData],[MaxRetryCount],[DisplayPrompt])
		VALUES
			([TxnID],[VerificationMethod],[VerificationData],[MaxRetryCount],[DisplayPrompt]);

	--------------------[Txn].[TxnStacInfo]---------------
	MERGE INTO [Txn].[TxnStacInfo] AS target
	USING (
		SELECT * FROM @Txn 
		WHERE [StacString] IS NOT NULL OR [StacString2] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
	WHEN MATCHED THEN
		UPDATE SET
			target.[StacString]			= CASE WHEN source.[StacString] IS NULL THEN source.[StacString2] ELSE source.[StacString] END,
			target.[StacIssuer]			= source.[StacIssuer],
			target.[StacToken]			= source.[StacToken],
			target.[StacExpiration]		= source.[StacExpiration],
			target.[StacStatus]			= source.[StacStatus]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnID],[StacString],[StacIssuer],[StacToken],[StacExpiration],[StacStatus])
		VALUES
			([TxnID],[StacString],[StacIssuer],[StacToken],[StacExpiration],[StacStatus]);

	--------------------[Txn].[TxnPaymentInfo]---------------
	MERGE INTO [Txn].[TxnPaymentInfo] AS target
	USING (
		SELECT * FROM @Txn 
		WHERE [WalletType] IS NOT NULL OR [CardIssuer] IS NOT NULL OR [CardType] IS NOT NULL OR [FirstSix] IS NOT NULL OR [LastFour] IS NOT NULL OR [AuthLocation]
				IS NOT NULL OR [FinalLocation] IS NOT NULL OR [WalletItemData] IS NOT NULL OR [PaymentExpiration] IS NOT NULL OR [PaymentProviderToken] IS NOT NULL OR [PaymentProviderName]
				IS NOT NULL OR [UserPaymentSourceID] IS NOT NULL OR [PaymentType] IS NOT NULL OR [ReceiptCardName] IS NOT NULL OR [PaymentProcessorID] IS NOT NULL OR [FundingProviderID]
				IS NOT NULL OR [FleetData] IS NOT NULL OR [UserPaymentSourceExID] IS NOT NULL OR [SecurityType] IS NOT NULL OR [ExpirationDate] IS NOT NULL OR [Dpan] IS NOT NULL OR [Data] IS NOT NULL OR [ExpirationMonth]
				IS NOT NULL OR [ExpirationYear] IS NOT NULL OR [EciIndicator] IS NOT NULL OR [PaymentAuthLimit] IS NOT NULL OR [StoreOverrunBuffer] IS NOT NULL OR [CalculatedAuthLimit] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
	WHEN MATCHED THEN
		UPDATE SET
			target.[WalletType]				= source.[WalletType],
			target.[CardIssuer]				= source.[CardIssuer],
			target.[CardType]				= source.[CardType],
			target.[FirstSix]				= source.[FirstSix],
			target.[LastFour]				= source.[LastFour],
			target.[AuthLocation]			= source.[AuthLocation],
			target.[FinalLocation]			= source.[FinalLocation],
			target.[WalletItemData]			= source.[WalletItemData],
			target.[PaymentExpiration]		= source.[PaymentExpiration],
			target.[PaymentProviderToken]	= source.[PaymentProviderToken],
			target.[PaymentProviderName]	= source.[PaymentProviderName],
			target.[UserPaymentSourceID]	= source.[UserPaymentSourceID],
			target.[PaymentType]			= source.[PaymentType],
			target.[ReceiptCardName]		= source.[ReceiptCardName],
			target.[PaymentProcessorID]		= source.[PaymentProcessorID],
			target.[FundingProviderID]		= source.[FundingProviderID],
			target.[FleetData]				= source.[FleetData],
			target.[UserPaymentSourceExID]	= source.[UserPaymentSourceExID],
			target.[SecurityType]			= source.[SecurityType],
			target.[ExpirationDate]			= source.[ExpirationDate],
			target.[Dpan]					= source.[Dpan],
			target.[Data]					= source.[Data],
			target.[ExpirationMonth]		= source.[ExpirationMonth],
			target.[ExpirationYear]			= source.[ExpirationYear],
			target.[EciIndicator]			= source.[EciIndicator],
			target.[PaymentAuthLimit]		= source.[PaymentAuthLimit],
			target.[StoreOverrunBuffer]		= source.[StoreOverrunBuffer],
			target.[CalculatedAuthLimit]	= source.[CalculatedAuthLimit]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnID],[WalletType],[CardIssuer],[CardType],[FirstSix],[LastFour],[AuthLocation]
			,[FinalLocation],[WalletItemData],[PaymentExpiration],[PaymentProviderToken],[PaymentProviderName]
			,[UserPaymentSourceID],[PaymentType],[ReceiptCardName],[PaymentProcessorID],[FundingProviderID]
			,[FleetData],[UserPaymentSourceExID],[SecurityType],[ExpirationDate],[Dpan],[Data],[ExpirationMonth]
			,[ExpirationYear],[EciIndicator],[PaymentAuthLimit],[StoreOverrunBuffer],[CalculatedAuthLimit])
		VALUES
			([TxnID],[WalletType],[CardIssuer],[CardType],[FirstSix],[LastFour],[AuthLocation]
			,[FinalLocation],[WalletItemData],[PaymentExpiration],[PaymentProviderToken],[PaymentProviderName]
			,[UserPaymentSourceID],[PaymentType],[ReceiptCardName],[PaymentProcessorID],[FundingProviderID]
			,[FleetData],[UserPaymentSourceExID],[SecurityType],[ExpirationDate],[Dpan],[Data],[ExpirationMonth]
			,[ExpirationYear],[EciIndicator],[PaymentAuthLimit],[StoreOverrunBuffer],[CalculatedAuthLimit]);

	--------------------[Txn].[TxnPaymentResponse]---------------
	MERGE INTO [Txn].[TxnPaymentResponse] AS target
	USING (
		SELECT NEWID() AS [TxnPaymentResponseID],
			[TxnID],
			[PaymentResponseType],[PaymentResultCode],[ResultText],[ReferenceNumber],[AuthorizedAmount]
			,[AuthorizedCurrency],[AuthorizationCode],[RawRequest],[RawRsponse] 
		FROM @TxnPaymentResponse
		WHERE [PaymentResponseType] IS NOT NULL 
	)AS source
	ON target.TxnID = source.TxnID
		AND target.[PaymentResponseType]	= source.[PaymentResponseType]
		AND target.[PaymentResultCode]		= source.[PaymentResultCode]
		AND target.[ResultText]				= source.[ResultText]
		AND target.[ReferenceNumber]		= source.[ReferenceNumber]
		AND target.[AuthorizedAmount]		= source.[AuthorizedAmount]
		AND target.[AuthorizedCurrency]		= source.[AuthorizedCurrency]
		AND target.[AuthorizationCode]		= source.[AuthorizationCode]
		AND target.[RawRequest]				= source.[RawRequest]
		AND target.[RawRsponse] 			= source.[RawRsponse]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnPaymentResponseID],[TxnID],[PaymentResponseType],[PaymentResultCode]
			,[ResultText],[ReferenceNumber],[AuthorizedAmount],[AuthorizedCurrency]
			,[AuthorizationCode],[RawRequest],[RawRsponse])
		VALUES
			([TxnPaymentResponseID],[TxnID],[PaymentResponseType],[PaymentResultCode]
			,[ResultText],[ReferenceNumber],[AuthorizedAmount],[AuthorizedCurrency]
			,[AuthorizationCode],[RawRequest],[RawRsponse]);

	--------------------[Txn].[TxnFuelEvent]---------------
	MERGE INTO [Txn].[TxnFuelEvent] AS target
	USING (
		SELECT NEWID() AS [TxnFuelEventID]
			,[TxnID]
			,[EventType]
			,[FuelEventStatus]
		FROM @TxnFuelEvent
		WHERE [EventType] IS NOT NULL AND [FuelEventStatus] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
		AND target.[EventType]			= source.[EventType]
		AND target.[FuelEventStatus]	= source.[FuelEventStatus]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnFuelEventID],[TxnID],[EventType],[FuelEventStatus])
		VALUES
			([TxnFuelEventID],[TxnID],[EventType],[FuelEventStatus]);

	--------------------[Txn].[LKTxnLoyaltyInstruments]---------------
	MERGE INTO [Txn].[LKTxnLoyaltyInstruments] AS target
	USING (
		SELECT [TxnID],[LoyaltyPAN],[LoyaltyPANType],[LoyaltyProgramID],[RedemptionValue],[ProcessAboveSite]
		FROM @LKTxnLoyaltyInstruments
		WHERE [LoyaltyPAN] IS NOT NULL
	)AS source
	ON target.TxnID = source.TxnID
		AND target.[LoyaltyPAN]	= source.[LoyaltyPAN]
	WHEN MATCHED THEN
		UPDATE SET
			target.[LoyaltyPANType]			= ISNULL(source.[LoyaltyPANType], 	target.[LoyaltyPANType]),
			target.[LoyaltyProgramID]		= ISNULL(source.[LoyaltyProgramID], target.[LoyaltyProgramID]),
			target.[RedemptionValue]		= ISNULL(source.[RedemptionValue], 	target.[RedemptionValue]),
			target.[ProcessAboveSite]		= ISNULL(source.[LoyaltyPANType], 	target.[ProcessAboveSite])
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnID],[LoyaltyPAN],[LoyaltyPANType],[LoyaltyProgramID],[RedemptionValue],[ProcessAboveSite])
		VALUES
			([TxnID],[LoyaltyPAN],[LoyaltyPANType],[LoyaltyProgramID],[RedemptionValue],[ProcessAboveSite]);

	--------------------[Txn].[TxnLineItem]---------------
	DELETE FROM [Txn].[TxnLineItemDiscount]
	WHERE [TxnLineItemID] IN 
		(
			SELECT tli.[TxnLineItemID] 
			FROM [Txn].[TxnLineItem] tli
			INNER JOIN @TxnLineItem source
			ON tli.TxnID = source.TxnID
				AND tli.[SiteLineItemID]			= source.[SiteLineItemID]
				AND tli.[StandardProductCodeID]		= source.[StandardProductCodeID]
				AND tli.[PosCode]					= source.[PosCode]
				AND tli.[PosCodeModifier]			= source.[PosCodeModifier]
				AND tli.[ProductCode]				= source.[ProductCode]
				AND tli.[ProductDescription]		= source.[ProductDescription]
				AND tli.[StandardProductCategory]	= source.[StandardProductCategory]
				AND tli.[Quantity]					= source.[Quantity]
				AND tli.[OriginalUnitPrice]			= source.[OriginalUnitPrice]
				AND tli.[OriginalAmount]			= source.[OriginalAmount]
				AND tli.[AdjustedUnitPrice]			= source.[AdjustedUnitPrice]
				AND tli.[AdjustedAmount]			= source.[AdjustedAmount]
		);
	DELETE FROM [Txn].[TxnLineItem] WHERE [TxnID] = (SELECT TOP(1) [TxnID] FROM @TxnLineItem);
	
	MERGE INTO [Txn].[TxnLineItem] AS target
	USING (
		SELECT [TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
			,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]
		FROM @TxnLineItem
		GROUP BY [TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount],[OriginalUnitPrice]
			,[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]
	)AS source
	ON target.TxnID = source.TxnID
		AND target.[SiteLineItemID]				= source.[SiteLineItemID]
		AND target.[StandardProductCodeID]		= source.[StandardProductCodeID]
		AND target.[PosCode]					= source.[PosCode]
		AND target.[PosCodeModifier]			= source.[PosCodeModifier]
		AND target.[ProductCode]				= source.[ProductCode]
		AND target.[ProductDescription]			= source.[ProductDescription]
		AND target.[StandardProductCategory]	= source.[StandardProductCategory]
		AND target.[Quantity]					= source.[Quantity]
		AND target.[OriginalUnitPrice]			= source.[OriginalUnitPrice]
		AND target.[OriginalAmount]				= source.[OriginalAmount]
		AND target.[AdjustedUnitPrice]			= source.[AdjustedUnitPrice]
		AND target.[AdjustedAmount]				= source.[AdjustedAmount]
		
	WHEN MATCHED THEN
		UPDATE SET
			target.[TxnLineItemID]			= source.[TxnLineItemID],
			target.[ReverseSale]			= ISNULL(source.[ReverseSale], 			target.[ReverseSale]),
			target.[EvaluateOnly]			= ISNULL(source.[EvaluateOnly], 		target.[EvaluateOnly]),
			target.[PriceChangeEligible]	= ISNULL(source.[PriceChangeEligible], 	target.[PriceChangeEligible]),
			target.[LinkedItemID]			= ISNULL(source.[LinkedItemID], 		target.[LinkedItemID]),
			target.[OutdoorPosition]		= ISNULL(source.[OutdoorPosition], 		target.[OutdoorPosition]),
			target.[PriceTier]				= ISNULL(source.[PriceTier], 			target.[PriceTier]),
			target.[ServiceLevel]			= ISNULL(source.[ServiceLevel], 		target.[ServiceLevel]),
			target.[WashCode]				= ISNULL(source.[WashCode], 			target.[WashCode]),
			target.[WashExpiration]			= ISNULL(source.[WashExpiration], 		target.[WashExpiration]),
			target.[WashText]				= ISNULL(source.[WashText], 			target.[WashText]),
			target.[UnitOfMeasure]			= ISNULL(source.[UnitOfMeasure], 		target.[UnitOfMeasure]),
			target.[SellingUnits]			= ISNULL(source.[SellingUnits], 		target.[SellingUnits]),
			target.[TaxCode]				= ISNULL(source.[TaxCode], 				target.[TaxCode])
	WHEN NOT MATCHED BY target THEN
		INSERT
			([TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount]
			,[OriginalUnitPrice],[AdjustedAmount],[AdjustedUnitPrice],[TaxCode])
		VALUES
			([TxnLineItemID],[TxnID],[SiteLineItemID],[StandardProductCodeID],[PosCode],[PosCodeModifier]
			,[ProductCode],[ProductDescription],[StandardProductCategory],[ReverseSale],[EvaluateOnly]
			,[PriceChangeEligible],[LinkedItemID],[OutdoorPosition],[PriceTier],[ServiceLevel],[WashCode]
			,[WashExpiration],[WashText],[UnitOfMeasure],[Quantity],[SellingUnits],[OriginalAmount]
			,[OriginalUnitPrice],[AdjustedAmount],[AdjustedUnitPrice],[TaxCode]);
	
	
	INSERT INTO [Txn].[TxnLineItemDiscount]
		([TxnLineItemDiscountID],[TxnLineItemID],[PromotionReason],[DiscountAmount],[DiscountUnitPrice],[Quantity]
		,[MaximumQuantity],[RebateLabel],[RewardApplied],[PriceAdjustmentID],[ProgramID])
	SELECT NEWID() AS [TxnLineItemDiscountID],[TxnLineItemID],[PromotionReason],[DiscountAmount],[DiscountUnitPrice],[Quantity]
		,[MaximumQuantity],[RebateLabel],[RewardApplied],[PriceAdjustmentID],[ProgramID]
	FROM @TxnLineItem
	WHERE [PromotionReason] IS NOT NULL AND ( [DiscountAmount] IS NOT NULL OR [DiscountUnitPrice] IS NOT NULL OR [Quantity]
			IS NOT NULL OR [MaximumQuantity] IS NOT NULL OR [RebateLabel] IS NOT NULL OR [RewardApplied] IS NOT NULL OR [PriceAdjustmentID] IS NOT NULL OR [ProgramID] IS NOT NULL )
	
	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	DECLARE @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
	(SELECT 
		 (SELECT * FROM @Txn FOR JSON PATH)[Txn]
		,(SELECT * FROM @TxnPaymentResponse FOR JSON PATH )[TxnPaymentResponse]
		,(SELECT * FROM @TxnFuelEvent FOR JSON PATH)[TxnFuelEvent]
		,(SELECT * FROM @LKTxnLoyaltyInstruments FOR JSON PATH)[LKTxnLoyaltyInstruments]
		,(SELECT * FROM @TxnLineItem FOR JSON PATH)[TxnLineItem]
	FOR JSON PATH) AS [StoredProcedureInput];
	
	ROLLBACK TRANSACTION

	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
END CATCH
END
