﻿CREATE TYPE [Txn].[TxnDocLoyaltyInstrumentType] AS TABLE (
    [TxnID]            UNIQUEIDENTIFIER NULL,
    [LoyaltyPAN]       NVARCHAR (255)   NULL,
    [LoyaltyPANType]   NVARCHAR (50)    NULL,
    [LoyaltyProgramID] UNIQUEIDENTIFIER NULL,
    [RedemptionValue]  DECIMAL (9, 3)   NULL,
    [ProcessAboveSite] BIT              NULL);

