﻿CREATE TYPE [Txn].[TxnDocPaymentResponseType] AS TABLE (
    [TxnID]               UNIQUEIDENTIFIER NULL,
    [PaymentResponseType] NVARCHAR (10)    NULL,
    [PaymentResultCode]   NVARCHAR (50)    NULL,
    [ResultText]          NVARCHAR (MAX)   NULL,
    [ReferenceNumber]     NVARCHAR (50)    NULL,
    [AuthorizedAmount]    DECIMAL (9, 3)   NULL,
    [AuthorizedCurrency]  NVARCHAR (3)     NULL,
    [AuthorizationCode]   NVARCHAR (50)    NULL,
    [RawRequest]          NVARCHAR (MAX)   NULL,
    [RawRsponse]          NVARCHAR (MAX)   NULL);

