﻿CREATE TYPE [Txn].[V1PriceAdjustmentType] AS TABLE (
    [SaleItemID]        UNIQUEIDENTIFIER NULL,
    [PromotionReason]   NVARCHAR (50)    NULL,
    [DiscountAmount]    DECIMAL (9, 3)   NULL,
    [DiscountUnitPrice] DECIMAL (9, 3)   NULL,
    [PromotionQuantity] DECIMAL (9, 3)   NULL,
    [MaximumQuantity]   DECIMAL (9, 3)   NULL,
    [RebateLabel]       NVARCHAR (50)    NULL,
    [RewardApplied]     BIT              NULL,
    [PriceAdjustmentID] NVARCHAR (50)    NULL,
    [ProgramID]         NVARCHAR (50)    NULL);

