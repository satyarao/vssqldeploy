﻿CREATE TYPE [Txn].[AboveSiteDiscountRequest] AS TABLE (
    [StoreID]         UNIQUEIDENTIFIER NULL,
    [TransactionID]   UNIQUEIDENTIFIER NULL,
    [MessageSequence] NVARCHAR (50)    NULL,
    [UserID]          UNIQUEIDENTIFIER NULL,
    [RequestTime]     DATETIME2 (7)    NULL);

