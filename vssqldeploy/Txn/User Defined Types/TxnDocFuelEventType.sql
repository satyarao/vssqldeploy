﻿CREATE TYPE [Txn].[TxnDocFuelEventType] AS TABLE (
    [TxnID]           UNIQUEIDENTIFIER NULL,
    [EventType]       NVARCHAR (50)    NULL,
    [FuelEventStatus] NVARCHAR (50)    NULL);

