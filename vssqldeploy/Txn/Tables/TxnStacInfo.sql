﻿CREATE TABLE [Txn].[TxnStacInfo] (
    [TxnID]          UNIQUEIDENTIFIER NOT NULL,
    [StacString]     NVARCHAR (50)    NOT NULL,
    [StacIssuer]     NVARCHAR (50)    NULL,
    [StacToken]      NVARCHAR (50)    NULL,
    [StacExpiration] DATETIME2 (7)    NULL,
    [StacStatus]     NVARCHAR (50)    NULL,
    CONSTRAINT [PK_TxnStacInfo] PRIMARY KEY CLUSTERED ([TxnID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_TxnStacInfo_TxnID] FOREIGN KEY ([TxnID]) REFERENCES [Txn].[Txn] ([TxnID]) ON DELETE CASCADE ON UPDATE CASCADE
);

