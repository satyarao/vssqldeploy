﻿CREATE TABLE [Txn].[TxnLineItemDiscount] (
    [TxnLineItemDiscountID]           UNIQUEIDENTIFIER NOT NULL,
    [TxnLineItemID]                   UNIQUEIDENTIFIER NOT NULL,
    [PromotionReason]                 NVARCHAR (50)    CONSTRAINT [DF_TxnLineItemDiscount_PromotionReason] DEFAULT ('other') NOT NULL,
    [DiscountAmount]                  DECIMAL (9, 3)   CONSTRAINT [DF_TxnLineItemDiscount_DiscountAmount] DEFAULT ((0)) NOT NULL,
    [DiscountUnitPrice]               DECIMAL (9, 3)   CONSTRAINT [DF_TxnLineItemDiscount_DiscountUnitPrice] DEFAULT ((0)) NOT NULL,
    [Quantity]                        DECIMAL (9, 3)   CONSTRAINT [DF_TxnLineItemDiscount_Quantity] DEFAULT ((0)) NOT NULL,
    [MaximumQuantity]                 DECIMAL (9, 3)   CONSTRAINT [DF_TxnLineItemDiscount_MaximumQuantity] DEFAULT ((0)) NOT NULL,
    [RebateLabel]                     NVARCHAR (50)    CONSTRAINT [DF_TxnLineItemDiscount_RebateLabel] DEFAULT ('') NOT NULL,
    [RewardApplied]                   BIT              CONSTRAINT [DF_TxnLineItemDiscount_RewardApplied] DEFAULT ((0)) NOT NULL,
    [PriceAdjustmentID]               NVARCHAR (50)    NULL,
    [ProgramID]                       NVARCHAR (50)    NULL,
    [OfferID]                         UNIQUEIDENTIFIER NULL,
    [ProductDiscountID]               UNIQUEIDENTIFIER NULL,
    [LoyaltyPAN]                      NVARCHAR (50)    NULL,
    [GetRewardsRequestTimeStamp]      DATETIME2 (7)    NULL,
    [FinalizeRewardsRequestTimeStamp] DATETIME2 (7)    NULL,
    [CreatedOn]                       DATETIME2 (7)    CONSTRAINT [DF_TxnLineItemDiscount_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_TxnLineItemDiscount] PRIMARY KEY CLUSTERED ([TxnLineItemDiscountID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_TxnLineItemDiscount_TxnLineItemID] FOREIGN KEY ([TxnLineItemID]) REFERENCES [Txn].[TxnLineItem] ([TxnLineItemID]) ON DELETE CASCADE ON UPDATE CASCADE
);

