﻿CREATE TABLE [Txn].[StdOfferDescriptionItem] (
    [ID]            UNIQUEIDENTIFIER NOT NULL,
    [TxnItemLineID] UNIQUEIDENTIFIER NOT NULL,
    [OfferId]       NVARCHAR (50)    NULL,
    [OfferTenantId] NVARCHAR (50)    NULL,
    [OfferTitle]    NVARCHAR (MAX)   NULL,
    [OfferAmount]   DECIMAL (9, 2)   NULL,
    [OfferDiscount] DECIMAL (9, 2)   NULL,
    CONSTRAINT [PK_StdOfferDescriptionItem] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_StdOfferDescriptionItem_StdTransactionItem] FOREIGN KEY ([TxnItemLineID]) REFERENCES [Txn].[StdTransactionItem] ([TxnItemLineID])
);

