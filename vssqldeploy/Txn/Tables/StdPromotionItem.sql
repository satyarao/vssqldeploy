﻿CREATE TABLE [Txn].[StdPromotionItem] (
    [ID]              UNIQUEIDENTIFIER NOT NULL,
    [TxnItemLineID]   UNIQUEIDENTIFIER NOT NULL,
    [PromotionID]     NVARCHAR (200)   NULL,
    [LoyaltyRewardID] NVARCHAR (200)   NULL,
    [PromotionAmount] DECIMAL (9, 2)   NULL,
    [PromotionReason] NVARCHAR (200)   NULL,
    [Status]          NVARCHAR (50)    NULL,
    CONSTRAINT [PK_StdPromotionItem] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_StdPromotionItem_StdTransactionItem] FOREIGN KEY ([TxnItemLineID]) REFERENCES [Txn].[StdTransactionItem] ([TxnItemLineID])
);

