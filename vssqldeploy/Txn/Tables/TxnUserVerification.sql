﻿CREATE TABLE [Txn].[TxnUserVerification] (
    [TxnID]              UNIQUEIDENTIFIER NOT NULL,
    [VerificationMethod] NVARCHAR (50)    NULL,
    [VerificationData]   NVARCHAR (255)   NULL,
    [MaxRetryCount]      INT              NULL,
    [DisplayPrompt]      NVARCHAR (255)   NULL,
    CONSTRAINT [PK_TxnUserVerification] PRIMARY KEY CLUSTERED ([TxnID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_TxnUserVerification_TxnID] FOREIGN KEY ([TxnID]) REFERENCES [Txn].[Txn] ([TxnID]) ON DELETE CASCADE ON UPDATE CASCADE
);

