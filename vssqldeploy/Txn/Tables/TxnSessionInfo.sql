﻿CREATE TABLE [Txn].[TxnSessionInfo] (
    [TxnID]            UNIQUEIDENTIFIER NOT NULL,
    [SessionID]        NVARCHAR (50)    NULL,
    [UserLatitude]     DECIMAL (10, 7)  NULL,
    [UserLongitude]    DECIMAL (10, 7)  NULL,
    [PreferredCulture] NVARCHAR (50)    NULL,
    [DeviceID]         NVARCHAR (50)    NULL,
    CONSTRAINT [PK_TxnSessionInfo] PRIMARY KEY CLUSTERED ([TxnID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_TxnSessionInfo_TxnID] FOREIGN KEY ([TxnID]) REFERENCES [Txn].[Txn] ([TxnID]) ON DELETE CASCADE ON UPDATE CASCADE
);

