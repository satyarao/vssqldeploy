﻿CREATE TABLE [Txn].[TxnFleetInfo] (
    [TxnID]         UNIQUEIDENTIFIER NOT NULL,
    [FleetTenantID] UNIQUEIDENTIFIER NULL,
    [DriverID]      NVARCHAR (50)    NULL,
    [VehicleID]     NVARCHAR (50)    NULL,
    [FleetID]       NVARCHAR (50)    NULL,
    [Odometer]      DECIMAL (9, 3)   NULL,
    [AccountID]     NVARCHAR (50)    NULL,
    [CustomerID]    NVARCHAR (50)    NULL,
    CONSTRAINT [PK_TxnFleetInfo] PRIMARY KEY CLUSTERED ([TxnID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_TxnFleetInfo_TxnID] FOREIGN KEY ([TxnID]) REFERENCES [Txn].[Txn] ([TxnID]) ON DELETE CASCADE ON UPDATE CASCADE
);

