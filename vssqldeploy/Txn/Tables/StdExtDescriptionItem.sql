﻿CREATE TABLE [Txn].[StdExtDescriptionItem] (
    [ID]                     UNIQUEIDENTIFIER NOT NULL,
    [TxnItemLineID]          UNIQUEIDENTIFIER NOT NULL,
    [RewardId]               NVARCHAR (200)   NULL,
    [RewardPromptTextShort]  NVARCHAR (50)    NULL,
    [RewardPromptTextLong]   NVARCHAR (100)   NULL,
    [RewardLimit]            DECIMAL (9, 2)   NULL,
    [RewardReceiptDescShort] NVARCHAR (100)   NULL,
    [RewardReceiptDescLong]  NVARCHAR (200)   NULL,
    CONSTRAINT [PK_StdExtDescriptionItem] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_StdExtDescriptionItem_StdTransactionItem] FOREIGN KEY ([TxnItemLineID]) REFERENCES [Txn].[StdTransactionItem] ([TxnItemLineID])
);

