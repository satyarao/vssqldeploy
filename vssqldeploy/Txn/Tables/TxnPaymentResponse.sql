﻿CREATE TABLE [Txn].[TxnPaymentResponse] (
    [TxnPaymentResponseID] UNIQUEIDENTIFIER NOT NULL,
    [TxnID]                UNIQUEIDENTIFIER NOT NULL,
    [PaymentResponseType]  NVARCHAR (10)    NOT NULL,
    [PaymentResultCode]    NVARCHAR (50)    NULL,
    [ResultText]           NVARCHAR (MAX)   NULL,
    [ReferenceNumber]      NVARCHAR (50)    NULL,
    [AuthorizedAmount]     DECIMAL (9, 3)   NULL,
    [AuthorizedCurrency]   NVARCHAR (3)     NULL,
    [AuthorizationCode]    NVARCHAR (50)    NULL,
    [RawRequest]           NVARCHAR (MAX)   NULL,
    [RawRsponse]           NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_TxnPaymentResponse] PRIMARY KEY CLUSTERED ([TxnPaymentResponseID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_TxnPaymentResponse_TxnID] FOREIGN KEY ([TxnID]) REFERENCES [Txn].[Txn] ([TxnID]) ON DELETE CASCADE ON UPDATE CASCADE
);

