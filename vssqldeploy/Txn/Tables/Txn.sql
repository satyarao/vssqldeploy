﻿CREATE TABLE [Txn].[Txn] (
    [TxnID]                UNIQUEIDENTIFIER NOT NULL,
    [PartnerTxnID]         NVARCHAR (255)   NULL,
    [SiteTxnID]            NVARCHAR (255)   NULL,
    [LoyaltyTxnID]         NVARCHAR (255)   NULL,
    [StoreTenantID]        UNIQUEIDENTIFIER NULL,
    [AppTenantID]          UNIQUEIDENTIFIER NULL,
    [UserID]               UNIQUEIDENTIFIER NULL,
    [StoreID]              UNIQUEIDENTIFIER NULL,
    [MppaID]               NVARCHAR (10)    NULL,
    [DiscountSystem]       NVARCHAR (50)    NULL,
    [AppDisplayName]       NVARCHAR (255)   CONSTRAINT [DF_Txn_AppDisplayName] DEFAULT ('Not Set') NULL,
    [AppChannel]           NVARCHAR (100)   CONSTRAINT [DF_Txn_AppChannel] DEFAULT ('WhiteLabelMobile') NULL,
    [UMTI]                 NVARCHAR (50)    NULL,
    [TransactionOwner]     NVARCHAR (50)    NULL,
    [TransactionOrigin]    NVARCHAR (50)    NULL,
    [SettlementPeriodId]   NVARCHAR (50)    NULL,
    [TransactionDateTime]  DATETIME2 (7)    NULL,
    [TerminalID]           NVARCHAR (255)   NULL,
    [MonetaryAmount]       DECIMAL (7, 3)   NULL,
    [CurrencyCode]         NVARCHAR (3)     CONSTRAINT [DF_Txn_CurrencyCode] DEFAULT ('USD') NULL,
    [BatchID]              INT              NULL,
    [BatchGuid]            UNIQUEIDENTIFIER NULL,
    [BasketStateID]        TINYINT          NULL,
    [QRCodeText]           NVARCHAR (1024)  NULL,
    [BasketLoyaltyStateID] TINYINT          NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_Txn_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]            DATETIME2 (7)    NULL,
    CONSTRAINT [PK_Txn] PRIMARY KEY CLUSTERED ([TxnID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Txn_AppTenantID] FOREIGN KEY ([AppTenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_Txn_StoreID] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Txn_StoreTenantID] FOREIGN KEY ([StoreTenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_Txn_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

