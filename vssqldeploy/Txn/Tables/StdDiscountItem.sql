﻿CREATE TABLE [Txn].[StdDiscountItem] (
    [ID]                 UNIQUEIDENTIFIER NOT NULL,
    [TxnItemLineID]      UNIQUEIDENTIFIER NOT NULL,
    [DiscountId]         NVARCHAR (200)   NULL,
    [DiscountAmount]     DECIMAL (9, 2)   NOT NULL,
    [DiscountReasonType] NVARCHAR (50)    NULL,
    [Status]             NVARCHAR (50)    NULL,
    CONSTRAINT [PK_StdDiscountItem] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_StdDiscountItem_StdTransactionItem] FOREIGN KEY ([TxnItemLineID]) REFERENCES [Txn].[StdTransactionItem] ([TxnItemLineID])
);

