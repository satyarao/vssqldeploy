﻿CREATE TABLE [Txn].[LKTxnLoyaltyInstruments] (
    [TxnID]            UNIQUEIDENTIFIER NOT NULL,
    [LoyaltyPAN]       NVARCHAR (255)   NOT NULL,
    [LoyaltyPANType]   NVARCHAR (50)    NULL,
    [LoyaltyProgramID] UNIQUEIDENTIFIER NULL,
    [RedemptionValue]  DECIMAL (9, 3)   NULL,
    [ProcessAboveSite] BIT              CONSTRAINT [DF_LKTxnLoyaltyInstruments_ProcessAboveSite] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_LKTxnLoyaltyInstruments] PRIMARY KEY CLUSTERED ([TxnID] ASC, [LoyaltyPAN] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKTxnLoyaltyInstruments_TxnID] FOREIGN KEY ([TxnID]) REFERENCES [Txn].[Txn] ([TxnID]) ON DELETE CASCADE ON UPDATE CASCADE
);

