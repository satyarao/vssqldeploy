﻿CREATE TABLE [Txn].[StdTaxItem] (
    [ID]                 UNIQUEIDENTIFIER NOT NULL,
    [TxnItemLineID]      UNIQUEIDENTIFIER NOT NULL,
    [TaxLevelId]         NVARCHAR (100)   NULL,
    [TaxLevelSequenceId] NVARCHAR (100)   NULL,
    [TaxCollectedAmount] DECIMAL (9, 2)   NULL,
    [TaxRefundedAmount]  DECIMAL (9, 2)   NULL,
    CONSTRAINT [PK_StdTaxItem] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_StdTaxItem_StdTransactionItem] FOREIGN KEY ([TxnItemLineID]) REFERENCES [Txn].[StdTransactionItem] ([TxnItemLineID])
);

