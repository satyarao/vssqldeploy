﻿
-- =============================================
-- Author: Andrei Ramanovich
-- Create date: 03/15/2017
-- Update date: 06/06/2017 Igor Gaidukov - CurrentUserID as input parameter
-- Update date: 08/03/2017 Alex Goroshko - Added IsActive  filter
-- Update date: 05/01/2018 Igor Gaidukov - Added EmailSearchTerm filter
-- Update date: 03/03/2018 Igor Gaidukov - added UserID filter
-- Update date: 12/10/2018 Darya Batalava - added ObjectId, IdentityProviders
-- Update date: 12/13/2018 Igor Kutesnka - added TenantObjectID
-- Update date: 12/13/2018 Davydovich Raman - added UserGroupID
-- Update date: 08/06/2019 Davydovich Raman - Remove MR.Claims
-- Update date: 09/16/2019 Richard Anderson - ObjectId - UNIQUEIDENTIFIER to NVARCHAR(50)
-- Description: Export merchant portal users with assigned groups, filtered by tenant ID
-- =============================================
CREATE PROC [MR].[ExportMerchantPortalUsersByRestrictedTenantID]
	@CurrentUserID UNIQUEIDENTIFIER,
	@UserID UNIQUEIDENTIFIER = NULL,
	@EmailSearchTerm NVARCHAR(255) = NULL,
	@TenantID UNIQUEIDENTIFIER = NULL,
	@IsActive BIT = NULL,
	@UserGroupID INT = NULL
AS
BEGIN
SET NOCOUNT ON

	DECLARE @TenantName nvarchar(255);
	SELECT TOP(1) @TenantName = [Name] FROM [dbo].[Tenant] WHERE [TenantID] = ISNULL(@TenantID, '00000000-0000-0000-0000-000000000000');

	DECLARE @TempUsers TABLE(UserID UNIQUEIDENTIFIER, ObjectId nvarchar(50), Email nvarchar(254), PhoneNumber nvarchar(20), TenantID UNIQUEIDENTIFIER, IsActive BIT, RestrictedTenants nvarchar(max), IdentityProviders nvarchar(max), TotalRows int);

	INSERT INTO @TempUsers
	EXEC [MR].[GetMerchantPortalUsersByRestrictedTenantID] 
		@RestrictedTenantID = @TenantID, 
		@CurrentUserID = @CurrentUserID,
		@UserID = @UserID,
		@EmailSearchTerm = @EmailSearchTerm,
		@Offset = 0,
		@Limit = 999999,
		@UserGroupID = @UserGroupID;

	WITH TempGroups AS -- all users with assigned groups for selected tenant
	(
		SELECT uga.[UserID] as UserID, t.[Name] as TenantName, ug.[UserGroupName] as GroupName
		FROM [AccessControl].[UserGroupAssignment] uga
		JOIN [AccessControl].[UserGroup] ug ON uga.[UserGroupID] = ug.[UserGroupID]
		JOIN [dbo].[Tenant] t ON ug.[TenantID] = t.[TenantID]
		WHERE @TenantID IS NULL OR t.[TenantID] = @TenantID
	)
	SELECT @TenantName AS TenantName
		  ,ui.[EmailAddress] AS Email
		  ,ui.[Name]
		  ,ui.[MobileNumber] AS MobilePhoneNumber
		  ,ui.IsActive
		  ,ui.[CreatedOn]
		  ,CAST(IIF(EXISTS(SELECT 1 FROM [AccessControl].[UserIdentity] aui WHERE aui.UserID = ui.UserID AND (aui.ProviderID = 1 OR aui.ProviderID = 2)), 1, 0) AS bit) AS IsB2CUser
		  ,(SELECT gr.TenantName, gr.GroupName FROM TempGroups gr WHERE (gr.UserID = ui.UserID) FOR JSON PATH) AS UserGroups
	FROM [dbo].[UserInfo] ui
	JOIN [AccessControl].[UserIdentity] aui ON aui.UserID = ui.UserID AND aui.ProviderID = 1
	JOIN @TempUsers tu ON ui.[UserID] = tu.UserID
	WHERE ui.[IsActive] = ISNULL(@IsActive, ui.IsActive)
	ORDER BY ui.[EmailAddress]
END

