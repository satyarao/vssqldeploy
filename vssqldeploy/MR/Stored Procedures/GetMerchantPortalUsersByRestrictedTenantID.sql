﻿
-- =============================================
-- Author:   Alex Goroshko
-- Create date: 10/01/2017
-- Update date: 02/07/2017 Andrei Ramanovich - add case p97 for restricted tenant
-- Update date: 05/04/2017 Igor Gaidukov - select RestrictedTenants form list of available tenants for current user 
-- Update date: 08/02/2017 Alex Goroshko - added IsActive and IsDeleted filters
-- Update date: 03/12/2018 Igor Gaidukov - added UserID filter, added ability to search by keyword
-- Update date: 10/31/2018 Andrei Ramanovich - add ObjectId, Providers
-- Update date: 12/12/2018 Darya Batalava - add filter by UserGroupID
-- Update date: 08/06/2019 Sergei Buday - PLAT-4004 [Identity] Clean up membership reboot stuff that we don't need
-- Update date: 09/06/2019 Andrei Ramanovich - get ObjectId from UserIdentity table
-- Description:   Get merchant portal users by RestrictedTenantID
-- =============================================
CREATE PROC [MR].[GetMerchantPortalUsersByRestrictedTenantID]
	 @RestrictedTenantID UNIQUEIDENTIFIER = NULL
	,@CurrentUserID UNIQUEIDENTIFIER
	,@UserID UNIQUEIDENTIFIER = NULL
	,@EmailSearchTerm NVARCHAR(255) = NULL
	,@SearchTerm NVARCHAR(255) = NULL
	,@IsActive BIT = NULL
	,@Offset INT
	,@Limit INT
	,@UserGroupID INT = NULL

AS
BEGIN
SET NOCOUNT ON

	IF (@RestrictedTenantID IS NULL)
    BEGIN
		WITH TempResult (UserID, ObjectID, Email, MobilePhoneNumber, TenantID, IsActive, RestrictedTenants, IdentityProviders)
		AS (
				SELECT ui.UserID AS UserID, aui.ProviderUserID as ObjectID, ui.EmailAddress, ui.MobileNumber, ui.TenantID AS TenantID, ui.IsActive, (SELECT t.TenantID, t.Name AS TenantName FROM [AccessControl].[UserRestrictedTenantAssignment] urt JOIN Tenant t ON t.TenantID = urt.RestrictedTenantID WHERE ui.userID = urt.UserID FOR JSON AUTO) AS RestrictedTenants, ui.Providers AS IdentityProviders
				FROM [dbo].[UserInfo] ui
				JOIN [AccessControl].[UserIdentity] aui ON aui.UserID = ui.UserID AND aui.ProviderID = 1
				LEFT JOIN [AccessControl].[UserGroupAssignment] uga ON (uga.UserID = ui.UserID AND  uga.UserGroupID = @UserGroupID)
				WHERE ui.TenantID = '00000000-0000-0000-0000-000000000000' 
					AND (@UserGroupID IS NULL OR uga.UserGroupID = @UserGroupID)
					AND ui.IsActive = ISNULL(@IsActive, ui.IsActive) 
					AND ui.IsDeleted = 0 
					AND ui.EmailAddress LIKE IIF(@EmailSearchTerm IS NOT NULL, '%' + @EmailSearchTerm + '%', ui.EmailAddress)
					AND ( @UserID IS NULL OR ui.UserID = @UserID )
					AND ( @SearchTerm IS NULL 
						OR ui.UserID LIKE '%' + @SearchTerm + '%' 
						OR ui.EmailAddress LIKE '%' + @SearchTerm + '%')
		)
		,TempCount AS (SELECT COUNT(*) AS TotalRows FROM TempResult)
 
		SELECT UserID, ObjectID, Email, MobilePhoneNumber, TenantID, IsActive, RestrictedTenants, IdentityProviders, TotalRows
		FROM TempResult, TempCount
		ORDER BY Email
		OFFSET @Offset ROWS
		FETCH NEXT @Limit ROWS ONLY;
    END

	ELSE IF (@RestrictedTenantID = '00000000-0000-0000-0000-000000000000')
	BEGIN
		WITH TempResult (UserID, ObjectID, Email, MobilePhoneNumber, TenantID, IsActive, RestrictedTenants, IdentityProviders)
		AS (
				SELECT ui.UserID AS UserID, aui.ProviderUserID as ObjectID, ui.EmailAddress, ui.MobileNumber, ui.TenantID AS TenantID, ui.IsActive, NULL AS RestrictedTenants, ui.Providers AS IdentityProviders
				FROM [dbo].[UserInfo] ui
				JOIN [AccessControl].[UserIdentity] aui ON aui.UserID = ui.UserID AND aui.ProviderID = 1
				LEFT JOIN [AccessControl].[UserGroupAssignment] uga ON (uga.UserID = ui.UserID AND  uga.UserGroupID = @UserGroupID)
				WHERE ui.TenantID = '00000000-0000-0000-0000-000000000000' 
					AND (@UserGroupID IS NULL OR uga.UserGroupID = @UserGroupID)
					AND ui.IsActive = ISNULL(@IsActive, ui.IsActive) 
					AND ui.IsDeleted = 0 
					AND ui.EmailAddress LIKE IIF(@EmailSearchTerm IS NOT NULL, '%' + @EmailSearchTerm + '%', ui.EmailAddress)
					AND ( @UserID IS NULL OR ui.UserID = @UserID )
					AND ( @SearchTerm IS NULL 
						OR ui.UserID LIKE '%' + @SearchTerm + '%' 
						OR ui.EmailAddress LIKE '%' + @SearchTerm + '%')
					AND NOT EXISTS( SELECT * FROM [AccessControl].[UserRestrictedTenantAssignment] urt WHERE ui.UserID = urt.UserID)
		)
		,TempCount AS (SELECT COUNT(*) AS TotalRows FROM TempResult)
 
		SELECT UserID, ObjectID, Email, MobilePhoneNumber, TenantID, IsActive, RestrictedTenants, IdentityProviders, TotalRows
		FROM TempResult, TempCount
		ORDER BY Email
		OFFSET @Offset ROWS
		FETCH NEXT @Limit ROWS ONLY;
	END

	ELSE
    BEGIN
		WITH TempResult (UserID, ObjectID, Email, MobilePhoneNumber, TenantID, IsActive, RestrictedTenants, IdentityProviders)
		AS (
				SELECT ui.UserID AS UserID, aui.ProviderUserID as ObjectID, ui.EmailAddress, ui.MobileNumber, ui.TenantID AS TenantID, ui.IsActive,
																					    (SELECT t.TenantID, t.Name AS TenantName 
																						FROM [AccessControl].[UserRestrictedTenantAssignment] urt
																						JOIN Tenant t ON t.TenantID = urt.RestrictedTenantID
																						WHERE ui.userID = urt.UserID
																						AND ((t.TenantID = urt.RestrictedTenantID)
																							OR (NOT EXISTS ( SELECT * FROM [AccessControl].[UserRestrictedTenantAssignment] urta WHERE urta.UserID = @CurrentUserID))) FOR JSON AUTO) AS RestrictedTenants,
																						ui.Providers AS IdentityProviders
				FROM [dbo].[UserInfo] ui
				JOIN [AccessControl].[UserIdentity] aui ON aui.UserID = ui.UserID AND aui.ProviderID = 1
				JOIN [AccessControl].[UserRestrictedTenantAssignment] ur ON (ur.UserID = ui.UserID AND ur.RestrictedTenantID = @RestrictedTenantID)
				LEFT JOIN [AccessControl].[UserGroupAssignment] uga ON (uga.UserID = ui.UserID AND  uga.UserGroupID = @UserGroupID)
				WHERE ui.TenantID = '00000000-0000-0000-0000-000000000000' 
					AND (@UserGroupID IS NULL OR uga.UserGroupID = @UserGroupID)
					AND ui.IsActive = ISNULL(@IsActive, ui.IsActive) 
					AND ui.IsDeleted = 0 
					AND ui.EmailAddress LIKE IIF(@EmailSearchTerm IS NOT NULL, '%' + @EmailSearchTerm + '%', ui.EmailAddress)
					AND ( @UserID IS NULL OR ui.UserID = @UserID )
					AND ( @SearchTerm IS NULL 
						OR ui.UserID LIKE '%' + @SearchTerm + '%' 
						OR ui.EmailAddress LIKE '%' + @SearchTerm + '%')
		)
		,TempCount AS (SELECT COUNT(*) AS TotalRows FROM TempResult)
                                               
		SELECT UserID, ObjectID, Email, MobilePhoneNumber, TenantID, IsActive, RestrictedTenants, IdentityProviders, TotalRows
		FROM TempResult, TempCount
		ORDER BY Email
		OFFSET @Offset ROWS
		FETCH NEXT @Limit ROWS ONLY;
    END
END
