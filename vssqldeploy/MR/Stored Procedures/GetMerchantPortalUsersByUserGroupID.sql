﻿-- =============================================
-- Author:   Alex Goroshko
-- Create date: 10/01/2017
-- Update date: 02/08/2017 Andrei Ramanovich - add case p97 for restricted tenant
-- Update date: 10/31/2018 Andrei Ramanovich - add ObjectId
-- Update date: 08/06/2019 Sergei Buday - PLAT-4004 [Identity] Clean up membership reboot stuff that we don't need
-- Update date: 09/06/2019 Andrei Ramanovich - get ObjectId from UserIdentity table
-- Update date: 09/26/2019 Richard Anderson - fix join on accesscontrol.usergroupassignment
-- Description:   Get merchant portal users by UserGroupID
-- =============================================
CREATE PROC [MR].[GetMerchantPortalUsersByUserGroupID]

	  @RestrictedTenantID UNIQUEIDENTIFIER = NULL
	, @EmailSearchTerm NVARCHAR(255) = NULL
	, @UserGroupID INT = NULL
	, @UserGroupIDToExclude INT = NULL
	, @Offset INT
	, @Limit INT
               
AS
BEGIN
    SET NOCOUNT ON
               
    IF(@UserGroupID IS NOT NULL)
                    BEGIN
                                    WITH TempResult (UserID, ObjectID, Email, MobilePhoneNumber, TenantID, RestrictedTenants, IdentityProviders)
                                                                                    AS (
																									SELECT ui.UserID AS UserID, aui.ProviderUserID as ObjectID, ui.EmailAddress, ui.MobileNumber, ui.TenantID AS TenantID, (SELECT t.TenantID, t.Name AS TenantName FROM [AccessControl].[UserRestrictedTenantAssignment] urt JOIN Tenant t ON t.TenantID = urt.RestrictedTenantID WHERE ui.userID = urt.UserID FOR JSON AUTO) AS RestrictedTenants, ui.Providers AS IdentityProviders
                                                                                                    FROM [dbo].[UserInfo] ui
																									JOIN [AccessControl].[UserIdentity] aui ON aui.UserID = ui.UserID AND aui.ProviderID = 1
																									RIGHT JOIN [AccessControl].[UserGroupAssignment] uga ON (uga.UserID = ui.UserID AND  uga.UserGroupID = @UserGroupID) 
                                                                                                    WHERE ui.TenantID = '00000000-0000-0000-0000-000000000000'
																										AND ui.EmailAddress LIKE IIF(@EmailSearchTerm IS NOT NULL, '%' + @EmailSearchTerm + '%', ui.EmailAddress)
                                                                                                    )
                                                    ,TempCount AS (SELECT COUNT(*) AS TotalRows FROM TempResult)
                                                               
                                    SELECT UserID, ObjectID, Email, MobilePhoneNumber, TenantID, RestrictedTenants, IdentityProviders, TotalRows
                                    FROM TempResult, TempCount
                                    ORDER BY Email
                                    OFFSET @Offset ROWS
                                    FETCH NEXT @Limit ROWS ONLY;
                    END
               
    IF(@UserGroupIDToExclude IS NOT NULL)
                    BEGIN
                                    WITH TempResult (UserID, ObjectID, Email, MobilePhoneNumber, TenantID, RestrictedTenants, IdentityProviders)
                                                                                    AS (
																									SELECT ui.UserID AS UserID, aui.ProviderUserID as ObjectID, ui.EmailAddress, ui.MobileNumber, ui.TenantID AS TenantID, (SELECT t.TenantID, t.Name AS TenantName FROM [AccessControl].[UserRestrictedTenantAssignment] urt JOIN Tenant t ON t.TenantID = urt.RestrictedTenantID WHERE ui.userID = urt.UserID FOR JSON AUTO) AS RestrictedTenants, ui.Providers AS IdentityProviders
                                                                                                    FROM [dbo].[UserInfo] ui
																									JOIN [AccessControl].[UserIdentity] aui ON aui.UserID = ui.UserID AND aui.ProviderID = 1
																									LEFT JOIN [AccessControl].[UserGroupAssignment] uga ON (uga.UserID = ui.UserID AND  uga.UserGroupID = @UserGroupID)
                                                                                                    WHERE ui.TenantID = '00000000-0000-0000-0000-000000000000'
																									AND ui.EmailAddress LIKE IIF(@EmailSearchTerm IS NOT NULL, '%' + @EmailSearchTerm + '%', ui.EmailAddress)
                                                                                                    AND NOT EXISTS(SELECT 1 FROM [AccessControl].[UserGroupAssignment] uga WHERE (uga.UserID = ui.UserID AND  uga.UserGroupID = @UserGroupIDToExclude))
																									AND (
																									(@RestrictedTenantID = '00000000-0000-0000-0000-000000000000' AND NOT EXISTS(SELECT 1 FROM [AccessControl].[UserRestrictedTenantAssignment] urt WHERE ui.UserID = urt.UserID))
																									OR (@RestrictedTenantID != '00000000-0000-0000-0000-000000000000' AND EXISTS(SELECT 1 FROM [AccessControl].[UserRestrictedTenantAssignment] urt WHERE ui.UserID = urt.UserID AND urt.RestrictedTenantID = @RestrictedTenantID))
																									)
                                                                                    )
                                                    ,TempCount AS (SELECT COUNT(*) AS TotalRows FROM TempResult)
               
                                    SELECT UserID, ObjectID, Email, MobilePhoneNumber, TenantID, RestrictedTenants, IdentityProviders, TotalRows
                                    FROM TempResult, TempCount
                                    ORDER BY Email
                                    OFFSET @Offset ROWS
                                    FETCH NEXT @Limit ROWS ONLY;
                    END
END
