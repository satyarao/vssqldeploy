﻿CREATE TABLE [MR].[UserCertificates] (
    [Key]        INT            IDENTITY (1, 1) NOT NULL,
    [ParentKey]  INT            NOT NULL,
    [Thumbprint] NVARCHAR (150) NOT NULL,
    [Subject]    NVARCHAR (250) NULL,
    CONSTRAINT [PK_dbo.UserCertificates] PRIMARY KEY CLUSTERED ([Key] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_dbo.UserCertificates_dbo.UserAccounts_ParentKey] FOREIGN KEY ([ParentKey]) REFERENCES [MR].[UserAccounts] ([Key]) ON DELETE CASCADE,
    CONSTRAINT [UK_ParentKey_Thumbprint] UNIQUE NONCLUSTERED ([ParentKey] ASC, [Thumbprint] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentKey]
    ON [MR].[UserCertificates]([ParentKey] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Thumbprint]
    ON [MR].[UserCertificates]([Thumbprint] ASC) WITH (FILLFACTOR = 80);

