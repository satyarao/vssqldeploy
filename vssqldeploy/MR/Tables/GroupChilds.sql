﻿CREATE TABLE [MR].[GroupChilds] (
    [Key]          INT              IDENTITY (1, 1) NOT NULL,
    [ParentKey]    INT              NOT NULL,
    [ChildGroupID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_dbo.GroupChilds] PRIMARY KEY CLUSTERED ([Key] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_dbo.GroupChilds_dbo.Groups_ParentKey] FOREIGN KEY ([ParentKey]) REFERENCES [MR].[Groups] ([Key]) ON DELETE CASCADE,
    CONSTRAINT [UK_ParentKey_ChildGroupID] UNIQUE NONCLUSTERED ([ParentKey] ASC, [ChildGroupID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentKey]
    ON [MR].[GroupChilds]([ParentKey] ASC) WITH (FILLFACTOR = 80);

