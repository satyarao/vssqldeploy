﻿CREATE TABLE [MR].[PasswordResetSecrets] (
    [Key]                   INT              IDENTITY (1, 1) NOT NULL,
    [ParentKey]             INT              NOT NULL,
    [PasswordResetSecretID] UNIQUEIDENTIFIER NOT NULL,
    [Question]              NVARCHAR (150)   NOT NULL,
    [Answer]                NVARCHAR (150)   NOT NULL,
    CONSTRAINT [PK_dbo.PasswordResetSecrets] PRIMARY KEY CLUSTERED ([Key] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_dbo.PasswordResetSecrets_dbo.UserAccounts_ParentKey] FOREIGN KEY ([ParentKey]) REFERENCES [MR].[UserAccounts] ([Key]) ON DELETE CASCADE,
    CONSTRAINT [UK_ParentKey_Question] UNIQUE NONCLUSTERED ([ParentKey] ASC, [Question] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentKey]
    ON [MR].[PasswordResetSecrets]([ParentKey] ASC) WITH (FILLFACTOR = 80);

