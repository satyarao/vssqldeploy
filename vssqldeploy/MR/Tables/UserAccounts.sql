﻿CREATE TABLE [MR].[UserAccounts] (
    [Key]                        INT              IDENTITY (1, 1) NOT NULL,
    [ID]                         UNIQUEIDENTIFIER NOT NULL,
    [Tenant]                     NVARCHAR (50)    NOT NULL,
    [Username]                   NVARCHAR (254)   NOT NULL,
    [Email]                      NVARCHAR (254)   NULL,
    [Created]                    DATETIME         NOT NULL,
    [LastUpdated]                DATETIME         NOT NULL,
    [PasswordChanged]            DATETIME         NULL,
    [RequiresPasswordReset]      BIT              NOT NULL,
    [MobileCode]                 NVARCHAR (100)   NULL,
    [MobileCodeSent]             DATETIME         NULL,
    [MobilePhoneNumber]          NVARCHAR (20)    NULL,
    [AccountTwoFactorAuthMode]   INT              NOT NULL,
    [CurrentTwoFactorAuthStatus] INT              NOT NULL,
    [IsAccountVerified]          BIT              NOT NULL,
    [IsLoginAllowed]             BIT              NOT NULL,
    [IsAccountClosed]            BIT              NOT NULL,
    [AccountClosed]              DATETIME         NULL,
    [LastLogin]                  DATETIME         NULL,
    [LastFailedLogin]            DATETIME         NULL,
    [FailedLoginCount]           INT              NOT NULL,
    [VerificationKey]            NVARCHAR (100)   NULL,
    [VerificationPurpose]        INT              NULL,
    [VerificationKeySent]        DATETIME         NULL,
    [HashedPassword]             NVARCHAR (200)   NULL,
    [LastFailedPasswordReset]    DATETIME         NULL,
    [FailedPasswordResetCount]   INT              NOT NULL,
    [MobilePhoneNumberChanged]   DATETIME         NULL,
    [VerificationStorage]        NVARCHAR (100)   NULL,
    [AccountApproved]            BIT              NULL,
    [AccountRejected]            BIT              NULL,
    CONSTRAINT [PK_dbo.UserAccounts] PRIMARY KEY CLUSTERED ([Key] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ID]
    ON [MR].[UserAccounts]([ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Tenant_Email]
    ON [MR].[UserAccounts]([Tenant] ASC, [Email] ASC) WITH (FILLFACTOR = 80);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Tenant_Username]
    ON [MR].[UserAccounts]([Tenant] ASC, [Username] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Username]
    ON [MR].[UserAccounts]([Username] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_VerificationKey]
    ON [MR].[UserAccounts]([VerificationKey] ASC) WITH (FILLFACTOR = 80);

