﻿CREATE TABLE [MR].[UserClaims] (
    [Key]       INT            IDENTITY (1, 1) NOT NULL,
    [ParentKey] INT            NOT NULL,
    [Type]      NVARCHAR (150) NOT NULL,
    [Value]     NVARCHAR (150) NOT NULL,
    CONSTRAINT [PK_dbo.UserClaims] PRIMARY KEY CLUSTERED ([Key] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_dbo.UserClaims_dbo.UserAccounts_ParentKey] FOREIGN KEY ([ParentKey]) REFERENCES [MR].[UserAccounts] ([Key]) ON DELETE CASCADE,
    CONSTRAINT [UK_ParentKey_Type_Value] UNIQUE NONCLUSTERED ([ParentKey] ASC, [Type] ASC, [Value] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentKey]
    ON [MR].[UserClaims]([ParentKey] ASC) WITH (FILLFACTOR = 80);


GO

CREATE TRIGGER [MR].[IUD_UserGroupAssignment]
   ON  [MR].[UserClaims]
   FOR UPDATE, INSERT, DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @type NVARCHAR(50) = 'UserGroupID';

	IF EXISTS(SELECT 1 FROM inserted WHERE [Type] = @type)
	BEGIN

		MERGE dbo.UserGroupAssignment AS target
		USING ( 
			SELECT [ParentKey] AS [UserAccountKey]
				,[Value] AS UserGroupID
			FROM inserted 
			JOIN AccessControl.UserGroup ug ON ug.UserGroupID = inserted.[Value] 
			WHERE [Type] = @type)  AS source
		ON ( target.[UserAccountKey] = source.[UserAccountKey] AND target.UserGroupID = source.UserGroupID )

		WHEN MATCHED THEN
			UPDATE SET [DateOfAssignment] = GETUTCDATE()

		WHEN NOT MATCHED THEN
			INSERT ( [UserAccountKey]
					,[UserGroupID]
					,[DateOfAssignment])
			VALUES ( 
					source.[UserAccountKey]
					,source.[UserGroupID]
					,GETUTCDATE() );
	END

	IF EXISTS(SELECT 1 FROM deleted WHERE [Type] = @type) AND NOT EXISTS(SELECT 1 FROM inserted WHERE [Type] = @type)
	BEGIN 
		DELETE dbo.UserGroupAssignment
		FROM dbo.UserGroupAssignment uga
		INNER JOIN deleted d ON d.[ParentKey] = uga.[UserAccountKey] 
			AND d.[Value] = uga.[UserGroupID] 
		WHERE d.[Type] = @type	
	END

END
