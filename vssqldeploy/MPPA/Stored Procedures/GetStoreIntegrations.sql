﻿
CREATE PROCEDURE [MPPA].[GetStoreIntegrations] 
	@TenantID UNIQUEIDENTIFIER = NULL,
	@StoreID UNIQUEIDENTIFIER = NULL,
	@MPPAID INT = NULL,
	@IntegrationType NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		[TenantID], 
		[StoreID], 
		[MPPAID],
		[IntegrationType]
	FROM [MPPA].[StoreIntegrationView] as store
	WHERE store.TenantID = ISNULL(@TenantID, store.TenantID) 
	AND store.StoreID = ISNULL(@StoreID, store.StoreID)
	AND store.MPPAID = ISNULL(@MPPAID, store.MPPAID)
	AND store.IntegrationType = ISNULL(@IntegrationType, store.IntegrationType)
	ORDER BY store.MPPAID, store.IntegrationType
END
