﻿CREATE VIEW [MPPA].[StoreIntegrationView] AS
SELECT 
	st.[TenantID], 
	st.[StoreID], 
	st.[MPPAID],
	'Precidia' as IntegrationType
FROM Store st
INNER JOIN dbo.ConfigPRECIDIAPOSLYNX prec on prec.TenantID = st.StoreID
UNION
SELECT 
	st.[TenantID], 
	st.[StoreID], 
	st.[MPPAID],
	'Zipline' as IntegrationType
FROM dbo.Store st
INNER JOIN dbo.ConfigNPCA npca on npca.TenantID = st.StoreID
UNION
SELECT 
	st.[TenantID], 
	st.[StoreID], 
	st.[MPPAID],
	'MCX' as IntegrationType
FROM dbo.Store st
INNER JOIN dbo.MCX_LocationMapping mcx on mcx.StoreID = st.StoreID
UNION
SELECT 
	st.[TenantID], 
	st.[StoreID], 
	st.[MPPAID],
	'Site' as IntegrationType
FROM dbo.Store st
INNER JOIN km.StoreKey siteKey on siteKey.StoreID = st.StoreID
