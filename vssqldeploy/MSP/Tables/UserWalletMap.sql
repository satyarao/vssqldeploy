﻿CREATE TABLE [MSP].[UserWalletMap] (
    [UserId]    UNIQUEIDENTIFIER NOT NULL,
    [WalletId]  NVARCHAR (11)    NOT NULL,
    [IsActive]  BIT              NULL,
    [CreatedOn] DATETIME2 (7)    NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    [CreatedBy] NVARCHAR (MAX)   NULL,
    [UpdatedBy] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_UserWalletMap] PRIMARY KEY CLUSTERED ([UserId] ASC) WITH (FILLFACTOR = 80)
);

