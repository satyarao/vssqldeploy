﻿CREATE TABLE [ContentPush].[LKUser_Channel] (
    [UserID]      UNIQUEIDENTIFIER NOT NULL,
    [ChannelID]   UNIQUEIDENTIFIER NOT NULL,
    [IsActive]    BIT              CONSTRAINT [DF_LKUser_Channel_IsActive] DEFAULT ((1)) NOT NULL,
    [LastUpdated] DATETIME         CONSTRAINT [DF_LKUser_Channel_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKUser_Channel] PRIMARY KEY CLUSTERED ([UserID] ASC, [ChannelID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKUser_Channel_ChannelID] FOREIGN KEY ([ChannelID]) REFERENCES [ContentPush].[Channel] ([ChannelID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LKUser_Channel_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

