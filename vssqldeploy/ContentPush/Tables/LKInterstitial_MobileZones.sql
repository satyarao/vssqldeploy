﻿CREATE TABLE [ContentPush].[LKInterstitial_MobileZones] (
    [MobileZoneID]       UNIQUEIDENTIFIER NOT NULL,
    [InterstitialZoneID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LKInterstitial_MobileZones] PRIMARY KEY NONCLUSTERED ([MobileZoneID] ASC),
    CONSTRAINT [FK_LKInterstitialMobileZones_InterstitialZoneID] FOREIGN KEY ([InterstitialZoneID]) REFERENCES [ContentPush].[InterstitialZone] ([InterstitialZoneID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE CLUSTERED INDEX [IDX_LKInterstitialMobileZones_InterstitialZoneID]
    ON [ContentPush].[LKInterstitial_MobileZones]([InterstitialZoneID] ASC);

