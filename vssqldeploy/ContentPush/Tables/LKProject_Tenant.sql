﻿CREATE TABLE [ContentPush].[LKProject_Tenant] (
    [TenantID]    UNIQUEIDENTIFIER NOT NULL,
    [Environment] NVARCHAR (100)   NOT NULL,
    [AppKey]      NVARCHAR (100)   NOT NULL,
    [IsActive]    BIT              CONSTRAINT [DF_LKProject_Tenant_IsActive] DEFAULT ((1)) NOT NULL,
    [LastUpdated] DATETIME         CONSTRAINT [DF_LKProject_Tenant_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKProject_Tenant] PRIMARY KEY CLUSTERED ([TenantID] ASC, [Environment] ASC, [AppKey] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKProject_Tenant_AppKey] FOREIGN KEY ([AppKey]) REFERENCES [ContentPush].[Project] ([AppKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LKProject_Tenant_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE ON UPDATE CASCADE
);

