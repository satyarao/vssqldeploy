﻿CREATE TABLE [ContentPush].[LKProject_TagGroup] (
    [GroupName]   NVARCHAR (127) NOT NULL,
    [AppKey]      NVARCHAR (100) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_LKProject_TagGroup_IsActive] DEFAULT ((1)) NOT NULL,
    [LastUpdated] DATETIME       CONSTRAINT [DF_LKProject_TagGroup_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKProject_TagGroup] PRIMARY KEY CLUSTERED ([GroupName] ASC, [AppKey] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKProject_TagGroup_AppKey] FOREIGN KEY ([AppKey]) REFERENCES [ContentPush].[Project] ([AppKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LKProject_TagGroup_GroupName] FOREIGN KEY ([GroupName]) REFERENCES [ContentPush].[TagGroup] ([GroupName]) ON DELETE CASCADE ON UPDATE CASCADE
);

