﻿CREATE TABLE [ContentPush].[UnrecognizedObject] (
    [UnrecognizedObjectID]      UNIQUEIDENTIFIER NOT NULL,
    [ObjectName]                NVARCHAR (255)   NOT NULL,
    [UnrecognisedValue]         NVARCHAR (MAX)   NOT NULL,
    [UnrecognisedValueHash]     NVARCHAR (255)   NOT NULL,
    [CreatedOn]                 DATETIME         CONSTRAINT [DF_UnrecognizedObject_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdated]               DATETIME         CONSTRAINT [DF_UnrecognizedObject_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    [TimesEncountered]          INT              CONSTRAINT [DF_UnrecognizedObject_TimesEncountered] DEFAULT ((0)) NOT NULL,
    [IsAddressed]               BIT              CONSTRAINT [DF_UnrecognizedObject_IsAddressed] DEFAULT ((0)) NOT NULL,
    [EncounteredAfterAddressed] BIT              CONSTRAINT [DF_UnrecognizedObject_EncounteredAfterAddressed] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_UnrecognizedObject] PRIMARY KEY CLUSTERED ([UnrecognizedObjectID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [CHK_UnrecognizedObject_UNIQUE] UNIQUE NONCLUSTERED ([ObjectName] ASC, [UnrecognisedValueHash] ASC)
);

