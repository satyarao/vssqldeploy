﻿CREATE TABLE [ContentPush].[PipelineActivity] (
    [PipelineActivityID] UNIQUEIDENTIFIER CONSTRAINT [DF_PipelineActivity_PipelineActivityID] DEFAULT (newid()) NOT NULL,
    [MessageID]          UNIQUEIDENTIFIER NOT NULL,
    [EngageEventID]      UNIQUEIDENTIFIER NOT NULL,
    [FilterDataInfo]     NVARCHAR (MAX)   NULL,
    [IsApply]            BIT              NOT NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_PipelineActivity_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [ErrorMessage]       NVARCHAR (255)   NULL,
    [ErrorStackTrace]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_PipelineActivity] PRIMARY KEY CLUSTERED ([PipelineActivityID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_PipelineActivity_EngageEventID] FOREIGN KEY ([EngageEventID]) REFERENCES [ContentPush].[EngageEvent] ([EngageEventID]),
    CONSTRAINT [FK_PipelineActivity_MessageID] FOREIGN KEY ([MessageID]) REFERENCES [ContentPush].[Message] ([MessageID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [UNQ_PipelineActivity] UNIQUE NONCLUSTERED ([MessageID] ASC, [EngageEventID] ASC)
);

