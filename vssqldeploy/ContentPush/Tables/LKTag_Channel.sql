﻿CREATE TABLE [ContentPush].[LKTag_Channel] (
    [ChannelID]   UNIQUEIDENTIFIER NOT NULL,
    [TagID]       UNIQUEIDENTIFIER NOT NULL,
    [IsActive]    BIT              CONSTRAINT [DF_LKTag_Channel_IsActive] DEFAULT ((1)) NOT NULL,
    [LastUpdated] DATETIME         CONSTRAINT [DF_LKTag_Channel_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKTag_Channel] PRIMARY KEY CLUSTERED ([ChannelID] ASC, [TagID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKTag_Channel_ChannelID] FOREIGN KEY ([ChannelID]) REFERENCES [ContentPush].[Channel] ([ChannelID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LKTag_Channel_TagID] FOREIGN KEY ([TagID]) REFERENCES [ContentPush].[Tag] ([TagID])
);

