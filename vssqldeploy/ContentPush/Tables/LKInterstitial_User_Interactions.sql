﻿CREATE TABLE [ContentPush].[LKInterstitial_User_Interactions] (
    [UserInteractionID]  INT              IDENTITY (1, 1) NOT NULL,
    [InterstitialZoneID] UNIQUEIDENTIFIER NOT NULL,
    [UserID]             UNIQUEIDENTIFIER NOT NULL,
    [OfferID]            UNIQUEIDENTIFIER NOT NULL,
    [UserAction]         NVARCHAR (30)    NULL,
    [Tag]                NVARCHAR (255)   NULL,
    [ViewedOn]           DATETIME2 (7)    CONSTRAINT [DF_InterstitialUserInteraction_ViewedOn] DEFAULT (getutcdate()) NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_InterstitialUserInteraction_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKInterstitial_User_Interactions] PRIMARY KEY CLUSTERED ([UserInteractionID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKInterstitial_User_Interactions_InterstitialZoneID] FOREIGN KEY ([InterstitialZoneID]) REFERENCES [ContentPush].[InterstitialZone] ([InterstitialZoneID]),
    CONSTRAINT [FK_LKInterstitial_User_Interactions_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]),
    CONSTRAINT [FK_LKInterstitial_User_Interactions_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IDX_LKInterstitial_User_Interactions_UserID]
    ON [ContentPush].[LKInterstitial_User_Interactions]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_LKInterstitial_User_Interactions_OfferID]
    ON [ContentPush].[LKInterstitial_User_Interactions]([OfferID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_LKInterstitial_User_Interactions_InterstitialZoneID]
    ON [ContentPush].[LKInterstitial_User_Interactions]([InterstitialZoneID] ASC);

