﻿CREATE TABLE [ContentPush].[LKInterstitial_Targeting_Tag] (
    [InterstitialZoneID] UNIQUEIDENTIFIER NOT NULL,
    [TagID]              UNIQUEIDENTIFIER NOT NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_LKInterstitial_Targeting_Tag_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LKInterstitial_Tag] PRIMARY KEY CLUSTERED ([InterstitialZoneID] ASC, [TagID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKInterstitial_Targeting_Tag_InterstitialZoneID] FOREIGN KEY ([InterstitialZoneID]) REFERENCES [ContentPush].[InterstitialZone] ([InterstitialZoneID]),
    CONSTRAINT [FK_LKInterstitial_Targeting_Tag_TagID] FOREIGN KEY ([TagID]) REFERENCES [ContentPush].[Tag] ([TagID])
);

