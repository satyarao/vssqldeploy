﻿CREATE TABLE [ContentPush].[ChannelActivity] (
    [ChannelActivityID]          UNIQUEIDENTIFIER CONSTRAINT [DF_ChannelActivity_ChannelActivityID] DEFAULT (newid()) NOT NULL,
    [AppKey]                     NVARCHAR (100)   NOT NULL,
    [ChannelID]                  UNIQUEIDENTIFIER NULL,
    [UserID]                     UNIQUEIDENTIFIER NOT NULL,
    [DeviceType]                 NVARCHAR (255)   NULL,
    [RegistrationType]           NVARCHAR (255)   NOT NULL,
    [SubscriptionType]           NVARCHAR (255)   CONSTRAINT [DF_ChannelActivity_SubscriptionType] DEFAULT ('all') NOT NULL,
    [AssociateNamedUserResponse] NVARCHAR (MAX)   NULL,
    [RegisterationResponse]      NVARCHAR (MAX)   NULL,
    [CreatedOn]                  DATETIME         CONSTRAINT [DF_ChannelActivity_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [WarningrMessage]            NVARCHAR (255)   NULL,
    [ErrorMessage]               NVARCHAR (MAX)   NULL,
    [ErrorStackTrace]            NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ChannelActivity] PRIMARY KEY CLUSTERED ([ChannelActivityID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_ChannelActivity_AppKey] FOREIGN KEY ([AppKey]) REFERENCES [ContentPush].[Project] ([AppKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ChannelActivity_ChannelID] FOREIGN KEY ([ChannelID]) REFERENCES [ContentPush].[Channel] ([ChannelID])
);

