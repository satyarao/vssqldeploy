﻿CREATE TABLE [ContentPush].[EngageEvent] (
    [EngageEventID]     UNIQUEIDENTIFIER CONSTRAINT [DF_EngageEvent_EngageEventID] DEFAULT (newid()) NOT NULL,
    [AppKey]            NVARCHAR (100)   NOT NULL,
    [UserID]            UNIQUEIDENTIFIER NULL,
    [ChannelID]         UNIQUEIDENTIFIER NULL,
    [StoreID]           UNIQUEIDENTIFIER NULL,
    [TransactionID]     UNIQUEIDENTIFIER NULL,
    [EventCreatedUtc]   DATETIME         NOT NULL,
    [EventCreatedLocal] DATETIME         NULL,
    [CustomEventType]   NVARCHAR (255)   NOT NULL,
    [DeviceType]        NVARCHAR (255)   NULL,
    [Lattitude]         DECIMAL (18, 15) NULL,
    [Longitude]         DECIMAL (18, 15) NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_EngageEvent_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_EngageEvent] PRIMARY KEY CLUSTERED ([EngageEventID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_EngageEvent_AppKey] FOREIGN KEY ([AppKey]) REFERENCES [ContentPush].[Project] ([AppKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_EngageEvent_ChannelID] FOREIGN KEY ([ChannelID]) REFERENCES [ContentPush].[Channel] ([ChannelID]),
    CONSTRAINT [FK_EngageEvent_StoreID] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_EngageEvent_TransactionID] FOREIGN KEY ([TransactionID]) REFERENCES [dbo].[Basket] ([BasketID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_EngageEvent_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

