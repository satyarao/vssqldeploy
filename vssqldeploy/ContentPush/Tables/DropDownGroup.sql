﻿CREATE TABLE [ContentPush].[DropDownGroup] (
    [Name]        NVARCHAR (127) NOT NULL,
    [Value]       NVARCHAR (127) NULL,
    [IsActive]    BIT            CONSTRAINT [DF_DropDownGroup_IsActive] DEFAULT ((1)) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DropDownGroup] PRIMARY KEY CLUSTERED ([Name] ASC)
);

