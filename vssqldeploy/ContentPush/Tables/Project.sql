﻿CREATE TABLE [ContentPush].[Project] (
    [AppKey]             NVARCHAR (100) NOT NULL,
    [AppSecret]          NVARCHAR (255) NOT NULL,
    [Name]               NVARCHAR (255) NOT NULL,
    [SenderID]           NVARCHAR (255) NULL,
    [iOSBundleID]        NVARCHAR (255) NULL,
    [AndroidPackage]     NVARCHAR (255) NULL,
    [ConnectEnabled]     BIT            NOT NULL,
    [SmsEnabled]         BIT            NOT NULL,
    [EmailEnabled]       BIT            NOT NULL,
    [RichContentEnabled] BIT            NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF_Project_IsActive] DEFAULT ((1)) NOT NULL,
    [Version]            INT            CONSTRAINT [DF_Project_Version] DEFAULT ((1)) NOT NULL,
    [LastUpdated]        DATETIME       CONSTRAINT [DF_Project_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED ([AppKey] ASC) WITH (IGNORE_DUP_KEY = ON)
);

