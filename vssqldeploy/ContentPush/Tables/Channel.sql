﻿CREATE TABLE [ContentPush].[Channel] (
    [ChannelID]             UNIQUEIDENTIFIER NOT NULL,
    [AppKey]                NVARCHAR (100)   NOT NULL,
    [DeviceType]            NVARCHAR (20)    NOT NULL,
    [IsInstalled]           BIT              NOT NULL,
    [OptedIn]               BIT              NOT NULL,
    [AppPackageName]        NVARCHAR (255)   NULL,
    [AppVersion]            NVARCHAR (255)   NULL,
    [BackgroundPushEnabled] BIT              NULL,
    [Carrier]               NVARCHAR (255)   NULL,
    [DeviceModel]           NVARCHAR (255)   NULL,
    [DeviceOs]              NVARCHAR (255)   NULL,
    [IanaTimezone]          NVARCHAR (255)   NULL,
    [LocaleCountryCode]     NVARCHAR (255)   NULL,
    [LocaleLanguageCode]    NVARCHAR (255)   NULL,
    [LocaleTimezone]        NVARCHAR (255)   NULL,
    [LocaleVariant]         NVARCHAR (255)   NULL,
    [LocationEnabled]       BIT              NULL,
    [LocationPermission]    NVARCHAR (255)   NULL,
    [UaSdkVersion]          NVARCHAR (255)   NULL,
    [LastUpdated]           DATETIME         CONSTRAINT [DF_Channel_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_Channel] PRIMARY KEY CLUSTERED ([ChannelID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Channel_AppKey] FOREIGN KEY ([AppKey]) REFERENCES [ContentPush].[Project] ([AppKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

