﻿CREATE TABLE [ContentPush].[DropDown] (
    [Group]       NVARCHAR (127) NOT NULL,
    [Name]        NVARCHAR (127) NOT NULL,
    [Value]       NVARCHAR (127) NULL,
    [IsActive]    BIT            CONSTRAINT [DF_DropDown_IsActive] DEFAULT ((1)) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DropDown] PRIMARY KEY CLUSTERED ([Group] ASC, [Name] ASC),
    CONSTRAINT [FK_DropDown_DropDownGroup] FOREIGN KEY ([Group]) REFERENCES [ContentPush].[DropDownGroup] ([Name])
);

