﻿CREATE TABLE [ContentPush].[InterstitialZone] (
    [InterstitialZoneID]    UNIQUEIDENTIFIER NOT NULL,
    [TenantID]              UNIQUEIDENTIFIER NOT NULL,
    [OfferID]               UNIQUEIDENTIFIER NULL,
    [ZoneInfoID]            UNIQUEIDENTIFIER NOT NULL,
    [CampaignID]            INT              NULL,
    [ZoneName]              NVARCHAR (255)   NOT NULL,
    [MaxOffersDisplayCount] INT              CONSTRAINT [DF_InterstitialZone_MaxOffersDisplayCount] DEFAULT ((1)) NOT NULL,
    [ImageUrl]              NVARCHAR (255)   NULL,
    [Headline]              NVARCHAR (255)   NULL,
    [Description]           NVARCHAR (255)   NULL,
    [ContentPosition]       NVARCHAR (6)     NULL,
    [LinkType]              NVARCHAR (255)   NULL,
    [LinkType2]             NVARCHAR (255)   NULL,
    [BimLinkType]           NVARCHAR (255)   NULL,
    [CtaText]               NVARCHAR (255)   NULL,
    [CtaText2]              NVARCHAR (255)   NULL,
    [Url]                   NVARCHAR (255)   NULL,
    [Url2]                  NVARCHAR (255)   NULL,
    [CardDisplayType]       NVARCHAR (30)    NULL,
    [IsActive]              BIT              CONSTRAINT [DF_InterstitialZone_IsActive] DEFAULT ((1)) NOT NULL,
    [IsDeleted]             BIT              CONSTRAINT [DF_InterstitialZone_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]             DATETIME2 (7)    CONSTRAINT [DF_InterstitialZone_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [LastUpdated]           DATETIME2 (7)    CONSTRAINT [DF_InterstitialZone_LastUpdated] DEFAULT (getutcdate()) NOT NULL,
    [ExpirationDate]        DATETIME2 (7)    NULL,
    CONSTRAINT [PK_InterstitialZone] PRIMARY KEY CLUSTERED ([InterstitialZoneID] ASC),
    CONSTRAINT [C_InterstitialZone_MaxOffersDisplayCount] CHECK ([MaxOffersDisplayCount]>(0)),
    CONSTRAINT [FK_InterstitialZone_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]),
    CONSTRAINT [FK_InterstitialZone_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IDX_InterstitialZone_UserID]
    ON [ContentPush].[InterstitialZone]([TenantID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_InterstitialZone_OfferID]
    ON [ContentPush].[InterstitialZone]([OfferID] ASC) WHERE ([OfferID] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [IDX_InterstitialZone_CampaignID]
    ON [ContentPush].[InterstitialZone]([CampaignID] ASC) WHERE ([CampaignID] IS NOT NULL);

