﻿CREATE TABLE [ContentPush].[Tag] (
    [TagID]       UNIQUEIDENTIFIER CONSTRAINT [DF_Tag_TagID] DEFAULT (newid()) NOT NULL,
    [GroupName]   NVARCHAR (127)   NULL,
    [AppKey]      NVARCHAR (100)   NOT NULL,
    [TenantID]    UNIQUEIDENTIFIER NULL,
    [TagValue]    NVARCHAR (255)   NOT NULL,
    [IsActive]    BIT              CONSTRAINT [DF_Tag_IsActive] DEFAULT ((1)) NOT NULL,
    [Version]     INT              CONSTRAINT [DF_Tag_Version] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_Tag_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [Description] NVARCHAR (255)   NULL,
    CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED ([TagID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Tag_AppKey] FOREIGN KEY ([AppKey]) REFERENCES [ContentPush].[Project] ([AppKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Tag_GroupName] FOREIGN KEY ([GroupName]) REFERENCES [ContentPush].[TagGroup] ([GroupName]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Tag_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [CHK_Tag_UNIQUE] UNIQUE NONCLUSTERED ([GroupName] ASC, [AppKey] ASC, [TenantID] ASC, [TagValue] ASC) WITH (FILLFACTOR = 80)
);

