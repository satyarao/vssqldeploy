﻿CREATE TABLE [ContentPush].[ConnectEvent] (
    [ConnectEventID]   UNIQUEIDENTIFIER NOT NULL,
    [ConnectEventType] NVARCHAR (255)   NOT NULL,
    [AppKey]           NVARCHAR (100)   NOT NULL,
    [EventBody]        NVARCHAR (MAX)   NULL,
    [AssociatedPush]   NVARCHAR (MAX)   NULL,
    [Device]           NVARCHAR (MAX)   NULL,
    [Occurred]         DATETIME         NULL,
    [Offset]           NVARCHAR (255)   NULL,
    [UaProcessedTime]  DATETIME         NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_ConnectEvent_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [ErrorMessage]     NVARCHAR (255)   NULL,
    [ErrorFunction]    NVARCHAR (255)   NULL,
    [ErrorStackTrace]  NVARCHAR (MAX)   NULL,
    [RawResponse]      NVARCHAR (MAX)   NULL,
    [IsProcessed]      BIT              CONSTRAINT [DF_ConnectEvent_IsProcessed] DEFAULT ((0)) NOT NULL,
    [ProcessedTime]    DATETIME         NULL,
    CONSTRAINT [PK_ConnectEvent] PRIMARY KEY CLUSTERED ([ConnectEventID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_ConnectEvent_AppKey] FOREIGN KEY ([AppKey]) REFERENCES [ContentPush].[Project] ([AppKey]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [nci_wi_ConnectEvent_AppKey]
    ON [ContentPush].[ConnectEvent]([AppKey] ASC)
    INCLUDE([CreatedOn], [Offset]) WITH (FILLFACTOR = 80);

