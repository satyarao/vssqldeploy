﻿CREATE TABLE [ContentPush].[TagGroup] (
    [GroupName]   NVARCHAR (127) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_TagGroup_IsActive] DEFAULT ((1)) NOT NULL,
    [Version]     INT            CONSTRAINT [DF_TagGroup_Version] DEFAULT ((1)) NOT NULL,
    [Description] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_TagGroup] PRIMARY KEY CLUSTERED ([GroupName] ASC) WITH (IGNORE_DUP_KEY = ON)
);

