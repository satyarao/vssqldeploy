﻿
	CREATE VIEW [ContentPush].[TenantEnv_Project]
	AS

	SELECT LK.[TenantID],
		LK.[Environment],
		PRJ.[AppKey],
		PRJ.[AppSecret],
		PRJ.[Name],
		PRJ.[SenderID],
		PRJ.[iOSBundleID],
		PRJ.[AndroidPackage],
		PRJ.[ConnectEnabled],
		PRJ.[SmsEnabled],
		PRJ.[EmailEnabled],
		PRJ.[RichContentEnabled]
	FROM [ContentPush].[LKProject_Tenant] LK
	INNER JOIN [ContentPush].[Project] PRJ
		ON LK.[AppKey] = PRJ.[AppKey]
	WHERE LK.[IsActive] = 1 AND PRJ.[IsActive] = 1;