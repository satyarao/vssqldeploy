﻿
		CREATE PROCEDURE [ContentPush].[GetTenantProject] @TenantId UNIQUEIDENTIFIER, @Env NVARCHAR(100)
		AS
		BEGIN
			SELECT * FROM [ContentPush].[TenantEnv_Project] WITH(NOLOCK) WHERE TenantID = @TenantId AND Environment = @Env
		END