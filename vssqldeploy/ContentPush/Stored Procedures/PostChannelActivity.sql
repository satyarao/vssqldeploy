﻿
CREATE PROCEDURE [ContentPush].[PostChannelActivity]
    @AppKey nvarchar(100), @ChannelID uniqueidentifier, @UserID uniqueidentifier, @DeviceType nvarchar(255), 
    @RegistrationType nvarchar(255), @SubscriptionType nvarchar(255),
    @AssociateNamedUserResponse nvarchar(max), @RegisterationResponse nvarchar(max), 
    @WarningMessage nvarchar(255), @ErrorMessage nvarchar(max), @ErrorStackTrace nvarchar(max)
AS
BEGIN
    DECLARE @IsInstalled bit, @OptedIn bit, @NoErrors bit;
    SET @IsInstalled = (CASE WHEN LOWER(@RegistrationType) = 'uninstall' THEN 0 ELSE 1 END);
    SET @OptedIn = (CASE WHEN LOWER(@RegistrationType) = 'optin' THEN 1 ELSE 0 END);
    SET @NoErrors = (CASE WHEN @ErrorMessage IS NULL AND @ErrorStackTrace IS NULL THEN 1 ELSE 0 END);
        

    IF(NOT EXISTS(SELECT * FROM [ContentPush].[Channel] WHERE [ChannelID] = @ChannelID) AND @NoErrors = 1)
    BEGIN
        INSERT INTO [ContentPush].[Channel]
            ([ChannelID], [AppKey], [DeviceType], [IsInstalled], [OptedIn], [LastUpdated])
        VALUES
            (@ChannelID, @AppKey, @DeviceType, @IsInstalled, @OptedIn, GETUTCDATE());
    END

    IF(EXISTS(SELECT * FROM [ContentPush].[Channel] WHERE [ChannelID] = @ChannelID) AND @NoErrors = 1)
    BEGIN
        UPDATE [ContentPush].[Channel]
        SET 
            [IsInstalled] = @IsInstalled,
            [OptedIn] = @OptedIn
        WHERE [ChannelID] = @ChannelID;
    END

    --If we don't have a record of this channel, and do not know it's state, then set channelId = null so insert does not fail
    --This way we can still keep the error, for debugging.
    IF(NOT EXISTS(SELECT * FROM [ContentPush].[Channel] WHERE [ChannelID] = @ChannelID) AND @NoErrors = 0)
    BEGIN
        SET @ChannelID = null;
    END

    INSERT INTO [ContentPush].[ChannelActivity]
        ([AppKey],[ChannelID],[UserID],[DeviceType],[RegistrationType],[SubscriptionType],[AssociateNamedUserResponse],
        [RegisterationResponse],[WarningrMessage],[ErrorMessage],[ErrorStackTrace])
    VALUES
        (@AppKey, @ChannelID, @UserID, @DeviceType, @RegistrationType, @SubscriptionType ,@AssociateNamedUserResponse,  
        @RegisterationResponse, @WarningMessage, @ErrorMessage, @ErrorStackTrace)
END
