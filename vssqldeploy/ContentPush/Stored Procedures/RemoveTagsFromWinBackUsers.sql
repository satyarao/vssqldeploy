﻿
CREATE   PROCEDURE [ContentPush].[RemoveTagsFromWinBackUsers]
	@UserIDString nvarchar(max),
	@TenantID     uniqueidentifier
AS
BEGIN
	DECLARE @UserIDs TABLE (UserID uniqueidentifier)
	INSERT INTO @UserIDs (UserID)  (SELECT value FROM STRING_SPLIT(@UserIDString, ',') WHERE TRY_CONVERT(uniqueidentifier, value) IS NOT NULL)
	
	DECLARE @WinbackTags TABLE (TagID uniqueidentifier)
	INSERT INTO @WinbackTags (TagID)
	(
		SELECT TagID FROM [ContentPush].[Tag] WHERE TagValue in ('account_no_wallet','wallet_no_txn','only_one_old_txn') AND TenantID = @TenantID
	)

	UPDATE	Tags
	SET		Tags.[IsActive] = 0,
			Tags.[LastUpdated] = GETUTCDATE()
	FROM	[ContentPush].[LKTag_User] Tags
	LEFT JOIN @WinbackTags Winback
	ON Tags.[TagID] = Winback.TagID
	LEFT JOIN [ContentPush].[Tag] cpt
	ON Tags.[TagID] = cpt.[TagID]
	WHERE UserID IN (SELECT * FROM @UserIDs)
	AND Winback.TagID IS NULL
	AND Tags.[IsActive] = 1
	AND cpt.GroupName = 'custom_tag';

	WITH ChannelCTE AS
	(
		SELECT ChannelID
		FROM [ContentPush].[LKUser_Channel]
		WHERE UserID IN (SELECT UserID FROM @UserIDs)
	)

	UPDATE Tags
	SET		 Tags.[IsActive] = 0,
			 Tags.[LastUpdated] = GETUTCDATE()
	FROM      [ContentPush].[LKTag_Channel] Tags
	LEFT JOIN @WinbackTags Winback
	ON Tags.[TagID] = Winback.TagID
	LEFT JOIN [ContentPush].[Tag] cpt
	ON Tags.[TagID] = cpt.[TagID]
	WHERE ChannelID IN (SELECT * FROM ChannelCTE)
	AND Winback.TagID IS NULL
	AND Tags.[IsActive] = 1
	AND cpt.GroupName = 'custom_tag';

	WITH UserCTE AS
	(
	   SELECT TagID 
	   FROM  [ContentPush].[LKTag_User]
	   WHERE UserID IN (SELECT * FROM @UserIDs) 
		 AND [IsActive] = 0
	)

	SELECT tag.TagValue
	FROM UserCTE
	INNER JOIN [ContentPush].[Tag] tag
	ON tag.TagID = UserCTE.TagID
	WHERE tag.[GroupName] = 'custom_tag'
END
