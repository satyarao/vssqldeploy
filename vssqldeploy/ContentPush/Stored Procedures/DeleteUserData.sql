﻿
CREATE PROCEDURE [ContentPush].[DeleteUserData] @UserID uniqueidentifier
    AS
        BEGIN

        DECLARE @Table TABLE(
            UserID UNIQUEIDENTIFIER,
            ChannelID UNIQUEIDENTIFIER
        );
        INSERT INTO @Table(UserID, ChannelID) (SELECT UserID, ChannelID FROM [ContentPush].[LKUser_Channel] WHERE [UserID] = @UserID);

        DELETE FROM [ContentPush].[ConnectEvent] 
        WHERE LOWER([Device]) LIKE ('%' + LOWER(convert(nvarchar(36), @UserID)) + '%');
        

        
        DELETE FROM [ContentPush].[LKTag_User]
        WHERE [UserID] = @UserID;

        
        DELETE FROM [ContentPush].[LKTag_Channel]
        WHERE [ChannelID] IN (SELECT [ChannelID] FROM @Table WHERE [UserID] = @UserID)

        DELETE FROM [ContentPush].[ChannelActivity]
        WHERE [ChannelID] IN (SELECT [ChannelID] FROM @Table WHERE [UserID] = @UserID) OR [UserID] = @UserID


        DELETE FROM [ContentPush].[MessageActivity]
        WHERE [PipelineActivityID] IN
        (
            SELECT [PipelineActivityID] 
            FROM [ContentPush].[PipelineActivity] 
            WHERE [EngageEventID] IN 
            (
                SELECT [EngageEventID] 
                FROM [ContentPush].[EngageEvent] 
                WHERE [UserID] = @UserID OR [ChannelID] IN 
                (
                    SELECT [ChannelID] 
                    FROM @Table
                    WHERE [UserID] = @UserID
                )
            )
        );
        DELETE FROM [ContentPush].[PipelineActivity]
        WHERE [EngageEventID] IN 
        (
            SELECT [EngageEventID] 
            FROM [ContentPush].[EngageEvent] 
            WHERE [UserID] = @UserID OR [ChannelID] IN 
            (
                SELECT [ChannelID] 
                FROM @Table
                WHERE [UserID] = @UserID
            )
        );
        DELETE FROM [ContentPush].[EngageEvent]
        WHERE [UserID] = @UserID OR [ChannelID] IN 
        (
            SELECT [ChannelID] 
            FROM @Table
            WHERE [UserID] = @UserID
        );

        

        DELETE FROM [ContentPush].[LKUser_Channel]
        WHERE 
            [UserID] = @UserID;

        DELETE FROM [ContentPush].[Channel]
        WHERE [ChannelID] IN (SELECT [ChannelID] FROM @Table);
    END
