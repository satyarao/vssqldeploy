﻿


CREATE   PROCEDURE [ContentPush].[GetOfferMessages_MessageIds] @MessageIds NVARCHAR(MAX)
AS
BEGIN
	SELECT [MessageID]
		,[TenantID]
		,[OfferID]
		,[CampaignID]
		,[MessageName]
		,[MessageType]
		,[OrchestrationType]
		,[Audience]
		,[DeviceTypes]
		[Notification]
		,[Campaigns]
		,[Options]
		,[InAppMessage]
		,[RichMessage]
		,[MessageFilterType]
		,[CustomEventType]
		,[FilterData]
		,[ControlGroup]
		,[ExperimentDescription]
		,[ExperimentVariants]
		,[ScheduleDateTime]
		,[ScheduleLocalTime]
		,[IsActive]
		,[IsDraft]
		,[IsDeleted]
		,[CreatedOn]
		,[LastUpdated]
		,[AppKey]
		,[TriggerStartDate]
		,[TriggerEndDate]
	FROM [ContentPush].[Message]
	WHERE [MessageID] IN (SELECT value FROM STRING_SPLIT(@MessageIds, ','))
END
