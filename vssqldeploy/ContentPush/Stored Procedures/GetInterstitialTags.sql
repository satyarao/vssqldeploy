﻿

-- Procedures for LKInterstitial_Targeting_Tag
CREATE   PROCEDURE [ContentPush].[GetInterstitialTags]
	@InterstitialZoneID uniqueidentifier
AS
BEGIN
	SELECT [TagID]
		  ,[GroupName]
		  ,[AppKey]
		  ,[TenantID]
		  ,[TagValue]
		  ,[IsActive]
		  ,[Version]
		  ,[CreatedOn]
		  ,[Description]
	FROM [ContentPush].[Tag]
	WHERE TagID IN
		(SELECT TagID
		 FROM [ContentPush].[LKInterstitial_Targeting_Tag]
		 WHERE [InterstitialZoneID] = @InterstitialZoneID
		   AND [IsDeleted] = 0)
END
