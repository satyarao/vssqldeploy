﻿
		CREATE PROCEDURE [ContentPush].[PostConnectEvent] 
			@ConnectEventId uniqueidentifier, @ConnectEventtType nvarchar(255), @AppKey nvarchar(100), @EventBody nvarchar(max),
			@AssociatedPush nvarchar(max), @Device nvarchar(max), @Occurred datetime, @Offset nvarchar(255), @ErrorMessage nvarchar(255),
			@ErrorFunction nvarchar(255), @ErrorStackTrace nvarchar(max), @RawResponse nvarchar(max)
		AS
		BEGIN
			IF NOT EXISTS(SELECT * FROM [ContentPush].[ConnectEvent] WHERE [ConnectEventID] = @ConnectEventId)
			BEGIN
				INSERT INTO [ContentPush].[ConnectEvent]([ConnectEventID],[ConnectEventType],[AppKey],[EventBody],[AssociatedPush],[Device],[Occurred]
					,[Offset],[CreatedOn],[ErrorMessage],[ErrorFunction],[ErrorStackTrace],[RawResponse])
				VALUES
					(@ConnectEventId, @ConnectEventtType, @AppKey, @EventBody, @AssociatedPush, @Device, @Occurred
					,@Offset, GETUTCDATE(), @ErrorMessage, @ErrorFunction, @ErrorStackTrace, @RawResponse);
			END
		END