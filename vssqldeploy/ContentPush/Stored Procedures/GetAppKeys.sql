﻿
		CREATE PROCEDURE [ContentPush].[GetAppKeys] @Env nvarchar(100)
		AS
		BEGIN
			SELECT DISTINCT PRJ.[AppKey] 
			FROM [ContentPush].[Project] PRJ
			INNER JOIN [ContentPush].[LKProject_Tenant] LK 
				ON LK.AppKey = PRJ.AppKey 
			WHERE PRJ.ConnectEnabled = 1 AND LK.Environment = @Env
		END