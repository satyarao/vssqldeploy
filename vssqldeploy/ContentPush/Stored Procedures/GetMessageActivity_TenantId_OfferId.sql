﻿
CREATE PROCEDURE [ContentPush].[GetMessageActivity_TenantId_OfferId]
	@TenantId UNIQUEIDENTIFIER, @OfferId UNIQUEIDENTIFIER
AS
BEGIN
	SELECT ACT.[MessageActivityID],
		ACT.[MessageID],
		ACT.[PipelineActivityID],
		ACT.[RequestObject],
		ACT.[ResponseObject],
		ACT.[MessageDeliveryType],
		ACT.[OperationID],
		ACT.[ScheduleIDs],
		ACT.[ExperimentIDs],
		ACT.[PushIDs],
		ACT.[MessageIDs],
		ACT.[ScheduleItemInfo],
		ACT.[UrlIDs],
		ACT.[ContentUrls],
		ACT.[CreatedOn],
		ACT.[IsPushed],
		ACT.[IsDeleted],
		ACT.[ErrorMessage],
		ACT.[ErrorStackTrace]
	FROM [ContentPush].[MessageActivity] ACT
	INNER JOIN [ContentPush].[Message] MSG
		ON MSG.MessageID = ACT.MessageID
	WHERE MSG.TenantID = @TenantId AND MSG.OfferID = @OfferId
END
