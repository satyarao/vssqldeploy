﻿
		CREATE PROCEDURE [ContentPush].[GetConnectOffset] @AppKey nvarchar(100)
		AS
		BEGIN
			SELECT TOP(1) [Offset] FROM [ContentPush].[ConnectEvent] WHERE [AppKey] = @AppKey ORDER BY CreatedOn DESC
		END