﻿
	CREATE PROCEDURE [ContentPush].[PostEngageEvent_EngageEventID]
		@EngageEventID uniqueidentifier, @AppKey nvarchar(100), @UserID uniqueidentifier, @ChannelID uniqueidentifier, 
		@StoreID uniqueidentifier, @TransactionID uniqueidentifier, @EventCreatedUtc datetime,@EventCreatedLocal datetime, 
		@CustomEventType nvarchar(255), @DeviceType nvarchar(255), @Lattitude decimal(18, 15), @Longitude decimal(18, 15)
	AS
	BEGIN
		IF NOT EXISTS(SELECT * FROM [ContentPush].[EngageEvent] WHERE [EngageEventID] = @EngageEventID)
		BEGIN
			INSERT INTO [ContentPush].[EngageEvent]
				([EngageEventID],[AppKey],[UserID],[ChannelID],[StoreID],[TransactionID],[EventCreatedUtc],
				[EventCreatedLocal],[CustomEventType],[DeviceType],[Lattitude],[Longitude])
			VALUES
				(@EngageEventID, @AppKey, @UserID, @ChannelID, @StoreID, @TransactionID, @EventCreatedUtc,
				@EventCreatedLocal, @CustomEventType, @DeviceType, @Lattitude, @Longitude)
		END
	END