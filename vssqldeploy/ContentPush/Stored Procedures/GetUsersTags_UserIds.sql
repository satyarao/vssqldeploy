﻿-- =============================================
-- Author: Dzmitry Mikhailau
-- Create date: 04/08/2019  
-- Description:	DM-448 - Display assigned tags to Audience List View & Detail View
-- =============================================

CREATE PROCEDURE [ContentPush].[GetUsersTags_UserIds] @UserIds NVARCHAR(MAX)
AS
BEGIN
	SELECT t.TagID as TagId,
		t.GroupName,
		t.IsActive,
		t.TagValue,
		t.Description,
		t.TenantID as TenantId,
		tu.UserID as UserId 
	FROM [ContentPush].[LKTag_User] AS tu
	LEFT JOIN [ContentPush].[Tag] AS t ON t.TagID = tu.TagID 
	WHERE tu.UserID IN (SELECT value FROM STRING_SPLIT(@UserIds, ','))
END 

