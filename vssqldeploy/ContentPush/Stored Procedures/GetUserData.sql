﻿
CREATE PROCEDURE [ContentPush].[GetUserData] @UserID uniqueidentifier
AS
BEGIN
DECLARE @LKTag_UserContent nvarchar(max);
SET @LKTag_UserContent = '[{' + SUBSTRING(
(
    SELECT
            '{"UserID": "' + LOWER(CONVERT(nvarchar(36), LKTag.[UserID])) + '", ' + 
            '"TagID": "' + LOWER(CONVERT(nvarchar(36), TAG.[TagID])) + '", ' + 
            '"GroupName": ' + (CASE WHEN TAG.[GroupName] IS NULL THEN 'null' ELSE ('"' + TAG.[GroupName] + '"') END) + ', ' +
            '"TagValue": "' + TAG.[TagValue] + '", ' + 
            '"IsActive": "' + CASE WHEN TAG.[IsActive] = 1 THEN 'true' ELSE 'false' END + '", ' + 
            '"LastUpdated": "' + CONVERT(nvarchar(36), LKTag.[LastUpdated]) + '"},' 
        AS 'data()'
    FROM [ContentPush].[LKTag_User] LKTag
    INNER JOIN [ContentPush].[Tag] TAG
        ON LKTag.[TagID] = TAG.[TagID]
    WHERE LKTag.UserID = @UserID
    FOR XML PATH('')
), 2, 9999);

---------------------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE @LKTag_ChannelContent nvarchar(max);
SET @LKTag_ChannelContent = '[{' + SUBSTRING(
(
    SELECT
            '{"ChannelID": "' + LOWER(CONVERT(nvarchar(36), LKTag.[ChannelID])) + '", ' + 
            '"TagID": "' + LOWER(CONVERT(nvarchar(36), TAG.[TagID])) + '", ' + 
            '"GroupName": ' + (CASE WHEN TAG.[GroupName] IS NULL THEN 'null' ELSE ('"' + TAG.[GroupName] + '"') END) + ', ' +
            '"TagValue": "' + TAG.[TagValue] + '", ' + 
            '"IsActive": "' + CASE WHEN TAG.[IsActive] = 1 THEN 'true' ELSE 'false' END + '", ' + 
            '"LastUpdated": "' + CONVERT(nvarchar(36), LKTag.[LastUpdated]) + '"},' 
        AS 'data()'
    FROM [ContentPush].[LKTag_Channel] LKTag
    INNER JOIN [ContentPush].[Tag] TAG
        ON LKTag.[TagID] = TAG.[TagID]
    INNER JOIN [ContentPush].[LKUser_Channel] LK
        ON LK.ChannelID = LKTag.ChannelID
    WHERE LK.UserID = @UserID
    FOR XML PATH('')
), 2, 9999);

---------------------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE @EngageEventContent nvarchar(max);
SET @EngageEventContent = '[{' + SUBSTRING(
(
    SELECT
            '{"EngageEventID": "' + LOWER(CONVERT(nvarchar(36), ENG.[EngageEventID])) + '", ' + 
            '"UserID": ' + (CASE WHEN ENG.[UserID] IS NULL THEN 'null' ELSE ('"' + LOWER(CONVERT(nvarchar(36), ENG.[UserID])) + '"') END) + ', ' +
            '"ChannelID": ' + (CASE WHEN ENG.[ChannelID] IS NULL THEN 'null' ELSE ('"' + LOWER(CONVERT(nvarchar(36), ENG.[ChannelID])) + '"') END) + ', ' +
            '"StoreID": ' + (CASE WHEN ENG.[StoreID] IS NULL THEN 'null' ELSE ('"' + LOWER(CONVERT(nvarchar(36), ENG.[StoreID])) + '"') END) + ', ' +
            '"TransactionID": ' + (CASE WHEN ENG.[TransactionID] IS NULL THEN 'null' ELSE ('"' + LOWER(CONVERT(nvarchar(36), ENG.[TransactionID])) + '"') END) + ', ' +
            '"EventCreatedUtc": "' + CONVERT(nvarchar(36), ENG.[EventCreatedUtc]) + '", ' + 
            '"EventCreatedLocal": ' + (CASE WHEN ENG.[EventCreatedLocal] IS NULL THEN 'null' ELSE ('"' + CONVERT(nvarchar(36), ENG.[EventCreatedLocal]) + '"') END) + ', ' +
            '"CustomEventType": "' + ENG.[CustomEventType] + '", ' + 
            '"DeviceType": ' + (CASE WHEN ENG.[DeviceType] IS NULL THEN 'null' ELSE ('"' + ENG.[DeviceType] + '"') END) + ', ' +
            '"Lattitude": ' + (CASE WHEN ENG.[Lattitude] IS NULL THEN 'null' ELSE ('"' + CONVERT(nvarchar(36), ENG.[Lattitude]) + '"') END) + ', ' +
            '"Longitude": ' + (CASE WHEN ENG.[Longitude] IS NULL THEN 'null' ELSE ('"' + CONVERT(nvarchar(36), ENG.[Longitude]) + '"') END) + ', ' +
            '"CreatedOn": "' + CONVERT(nvarchar(36), ENG.[CreatedOn]) + '"},'
        AS 'data()'
    FROM [ContentPush].[EngageEvent] ENG
    LEFT JOIN [ContentPush].[LKUser_Channel] LK
        ON LK.ChannelID = ENG.ChannelID
    WHERE ENG.UserID = @UserID OR LK.UserID = @UserID
    FOR XML PATH('')
), 2, 2147483647);

-----------------------------------------------------------------------------------------------------------------------------------------------------------------

--DECLARE @ChannelActivityContent nvarchar(max);
--SET @ChannelActivityContent = '[{' + SUBSTRING(
--(
--  SELECT
--          '{ "ChannelActivityID": "' + LOWER(CONVERT(nvarchar(36), ACT.[ChannelActivityID])) + '", ' + 
--          '"ChannelID": "' + LOWER(CONVERT(nvarchar(36), ACT.[ChannelID])) + '", ' + 
--          '"UserID": ' + CASE WHEN ACT.[UserID] IS NULL THEN 'null' ELSE ('"' + LOWER(CONVERT(nvarchar(36), ACT.[UserID])) + '"') END + ', ' + 
--          '"DeviceType": ' + (CASE WHEN ACT.[DeviceType] IS NULL THEN 'null' ELSE ('"' + ACT.[DeviceType] + '"') END) + ', ' + 
--          '"OptInDateTime": ' + (CASE WHEN ACT.[OptInDateTime] IS NULL THEN 'null' ELSE ('"' + CONVERT(nvarchar(36), ACT.[OptInDateTime]) + '"') END) + ', ' + 
--          '"OptOutDateTime": ' + (CASE WHEN ACT.[OptOutDateTime] IS NULL THEN 'null' ELSE ('"' + CONVERT(nvarchar(36), ACT.[OptOutDateTime]) + '"') END) + ', ' + 
--          '"AssociateNamedUserRequest": ' + (CASE WHEN ACT.[AssociateNamedUserRequest] IS NULL THEN 'null' ELSE ('"' + ACT.[AssociateNamedUserRequest] + '"') END) + ', ' + 
--          '"AssociateNamedUserResponse": ' + (CASE WHEN ACT.[AssociateNamedUserResponse] IS NULL THEN 'null' ELSE ('"' + ACT.[AssociateNamedUserResponse] + '"') END) + ', ' + 
--          '"RegisterEmailRequest": ' + (CASE WHEN ACT.[RegisterEmailRequest] IS NULL THEN 'null' ELSE ('"' + ACT.[RegisterEmailRequest] + '"') END) + ', ' + 
--          '"RegisterEmailResponse": ' + (CASE WHEN ACT.[RegisterEmailResponse] IS NULL THEN 'null' ELSE ('"' + ACT.[RegisterEmailResponse] + '"') END) + ', ' + 
--          '"RegisterSmsRequest": ' + (CASE WHEN ACT.[RegisterSmsRequest] IS NULL THEN 'null' ELSE ('"' + ACT.[RegisterSmsRequest] + '"') END) + ', ' + 
--          '"RegisterSmsResponse": ' + (CASE WHEN ACT.[RegisterSmsResponse] IS NULL THEN 'null' ELSE ('"' + ACT.[RegisterSmsResponse] + '"') END) + ', ' + 
--          '"CreatedOn": "' + CONVERT(nvarchar(36), ACT.[CreatedOn]) + '"},' 
--      AS 'data()'
--  FROM [ContentPush].[ChannelActivity] ACT
--  INNER JOIN [ContentPush].[LKUser_Channel] LK
--      ON LK.ChannelID = ACT.ChannelID
--  WHERE LK.UserID = @UserID
--  FOR XML PATH('')
--), 2, 2147483647);

---------------------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE @ChannelContent nvarchar(max);
SET @ChannelContent = '[{' + SUBSTRING(
(
    SELECT
            '{ "ChannelID": "' + LOWER(CONVERT(nvarchar(36), CHN.[ChannelID])) + '", ' + 
            '"DeviceType": "' + CHN.[DeviceType] + '", ' + 
            '"IsInstalled": ' + (CASE WHEN CHN.[IsInstalled] = 1 THEN 'true' ELSE 'false' END) + ', ' + 
            '"OptedIn": ' + (CASE WHEN CHN.[OptedIn] = 1 THEN 'true' ELSE 'false' END) + ', ' + 
            '"AppPackageName": ' + (CASE WHEN CHN.[AppPackageName] IS NULL THEN 'null' ELSE '"' + CHN.[AppPackageName] + '"' END) + ', ' + 
            '"AppVersion": ' + (CASE WHEN CHN.[AppVersion] IS NULL THEN 'null' ELSE '"' + CHN.[AppVersion] + '"' END) + ', ' + 
            '"BackgroundPushEnabled": ' + (CASE WHEN CHN.[BackgroundPushEnabled] IS NULL THEN 'null' ELSE (CASE WHEN CHN.[BackgroundPushEnabled] = 1 THEN 'true' ELSE 'false' END) END) + ', ' + 
            '"Carrier": ' + (CASE WHEN CHN.[Carrier] IS NULL THEN 'null' ELSE '"' + CHN.[Carrier] + '"' END) + ', ' + 
            '"DeviceModel": ' + (CASE WHEN CHN.[DeviceModel] IS NULL THEN 'null' ELSE '"' + CHN.[DeviceModel] + '"' END) + ', ' + 
            '"DeviceOs": ' + (CASE WHEN CHN.[DeviceOs] IS NULL THEN 'null' ELSE '"' + CHN.[DeviceOs] + '"' END) + ', ' + 
            '"IanaTimezone": ' + (CASE WHEN CHN.[IanaTimezone] IS NULL THEN 'null' ELSE '"' + CHN.[IanaTimezone] + '"' END) + ', ' + 
            '"LocaleCountryCode": ' + (CASE WHEN CHN.[LocaleCountryCode] IS NULL THEN 'null' ELSE '"' + CHN.[LocaleCountryCode] + '"' END) + ', ' + 
            '"LocaleLanguageCode": ' + (CASE WHEN CHN.[LocaleLanguageCode] IS NULL THEN 'null' ELSE '"' + CHN.[LocaleLanguageCode] + '"' END) + ', ' + 
            '"LocaleTimezone": ' + (CASE WHEN CHN.[LocaleTimezone] IS NULL THEN 'null' ELSE '"' + CHN.[LocaleTimezone] + '"' END) + ', ' + 
            '"LocaleVariant": ' + (CASE WHEN CHN.[LocaleVariant] IS NULL THEN 'null' ELSE '"' + CHN.[LocaleVariant] + '"' END) + ', ' + 
            '"LocationEnabled": ' + (CASE WHEN  CHN.[LocationEnabled] IS NULL THEN 'null' ELSE (CASE WHEN CHN.[LocationEnabled] = 1 THEN 'true' ELSE 'false' END) END) + ', ' + 
            '"LocationPermission": ' + (CASE WHEN CHN.[LocationPermission] IS NULL THEN 'null' ELSE '"' + CHN.[LocationPermission] + '"' END) + ', ' + 
            '"UaSdkVersion": ' + (CASE WHEN CHN.[UaSdkVersion] IS NULL THEN 'null' ELSE '"' + CHN.[UaSdkVersion] + '"' END) + ', ' + 
            '"LastUpdated": "' + CONVERT(nvarchar(36), CHN.[LastUpdated]) + '"},' 
        AS 'data()'
    FROM [ContentPush].[Channel] CHN
    INNER JOIN [ContentPush].[LKUser_Channel] LK
        ON LK.ChannelID = CHN.ChannelID
    WHERE LK.UserID = @UserID
    FOR XML PATH('')
), 2, 2147483647);

---------------------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE @ConnectEventContent nvarchar(max);
SET @ConnectEventContent = '[{' + SUBSTRING(
(
    SELECT
        '{ "ConnectEventID": "' + LOWER(CONVERT(nvarchar(36), CNCT.[ConnectEventID])) + '", ' + 
        '"ConnectEventType": "' + CNCT.[ConnectEventType] + '", ' + 
        '"EventBody": ' + (CASE WHEN [EventBody] IS NULL THEN 'null' ELSE ('"' + REPLACE([EventBody], '"', '\"') + '"') END) + ', ' + 
        '"AssociatedPush": ' + (CASE WHEN [AssociatedPush] IS NULL THEN 'null' ELSE ('"' + REPLACE([AssociatedPush], '"', '\"') + '"') END) + ', ' + 
        '"Device": ' + (CASE WHEN [Device] IS NULL THEN 'null' ELSE ('"' + REPLACE([Device], '"', '\"') + '"') END) + ', ' + 
        '"Occurred": ' + (CASE WHEN [Occurred] IS NULL THEN 'null' ELSE ('"' + CONVERT(nvarchar(36), CNCT.[Occurred]) + '"') END) + ', ' + 
        '"CreatedOn": "' + CONVERT(nvarchar(36), CNCT.[CreatedOn]) + '"},'
        AS 'data()'
    FROM [ContentPush].[ConnectEvent] CNCT
    WHERE LOWER([Device]) LIKE ('%' + LOWER(convert(nvarchar(36), @UserID)) + '%')
    FOR XML PATH('')
), 2, 2147483647);

---------------------------------------------------------------------------------------------------------------------------------------------------------------

SELECT 'ContentPush_UserTag' 
        AS [Name],
        LEFT(@LKTag_UserContent, LEN(@LKTag_UserContent)-1) + ']'
        AS [Content]
UNION
SELECT 'ContentPush_ChannelTag' 
        AS [Name],
        LEFT(@LKTag_ChannelContent, LEN(@LKTag_ChannelContent)-1) + ']'
        AS [Content]
UNION
SELECT 'ContentPush_EngageEvent' 
        AS [Name],
        LEFT(@EngageEventContent, LEN(@EngageEventContent)-1) + ']'
        AS [Content]
--UNION
--SELECT 'ContentPush_ChannelActivity' 
--      AS [Name],
--      LEFT(@ChannelActivityContent, LEN(@ChannelActivityContent)-1) + ']'
--      AS [Content]
UNION
SELECT 'ContentPush_Channel' 
        AS [Name],
        LEFT(@ChannelContent, LEN(@ChannelContent)-1) + ']'
        AS [Content]
UNION
SELECT 'ContentPush_Connect' 
        AS [Name],
        LEFT(@ConnectEventContent, LEN(@ConnectEventContent)-1) + ']'
        AS [Content]

END 
