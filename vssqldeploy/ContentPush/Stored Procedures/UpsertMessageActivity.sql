﻿
	CREATE PROCEDURE [ContentPush].[UpsertMessageActivity] 
		@MessageActivityID uniqueidentifier, @MessageID uniqueidentifier, @PipelineActivityID uniqueidentifier, @RequestObject nvarchar(max), @ResponseObject nvarchar(max), 
		@MessageDeliveryType nvarchar(255), @OperationID uniqueidentifier, @ScheduleIDs nvarchar(max), 
		@ExperimentIDs nvarchar(max), @PushIDs nvarchar(max), @MessageIDs nvarchar(max), @ScheduleItemInfo nvarchar(max), @UrlIDs nvarchar(max), 
		@ContentUrls nvarchar(max), @IsPushed bit, @IsDeleted bit, @ErrorMessage nvarchar(255), @ErrorStackTrace nvarchar(max)
	AS
	BEGIN
		IF NOT EXISTS(SELECT * FROM [ContentPush].[MessageActivity] WHERE [MessageActivityID] = @MessageActivityID)
		BEGIN
			INSERT INTO [ContentPush].[MessageActivity]
				([MessageActivityID],[MessageID],[PipelineActivityID],[RequestObject],[ResponseObject],[MessageDeliveryType],[OperationID],[ScheduleIDs],
				[ExperimentIDs],[PushIDs],[MessageIDs],[ScheduleItemInfo],[UrlIDs],[ContentUrls],[IsPushed],[IsDeleted],[ErrorMessage],[ErrorStackTrace])
			VALUES
				(@MessageActivityID, @MessageID, @PipelineActivityID, @RequestObject, @ResponseObject, @MessageDeliveryType, @OperationID, @ScheduleIDs, 
				@ExperimentIDs, @PushIDs, @MessageIDs, @ScheduleItemInfo, @UrlIDs, @ContentUrls, @IsPushed, @IsDeleted, @ErrorMessage, @ErrorStackTrace)
		END
		ELSE
		BEGIN
			UPDATE [ContentPush].[MessageActivity]
			SET
				[MessageID] = @MessageID,
				[PipelineActivityID] = @PipelineActivityID,
				[RequestObject] = @RequestObject,
				[ResponseObject] = @ResponseObject,
				[MessageDeliveryType] = @MessageDeliveryType,
				[OperationID] = @OperationID,
				[ScheduleIDs] = @ScheduleIDs,
				[ExperimentIDs] = @ExperimentIDs,
				[PushIDs] = @PushIDs,
				[MessageIDs] = @MessageIDs,
				[ScheduleItemInfo] = @ScheduleItemInfo,
				[UrlIDs] = @UrlIDs,
				[ContentUrls] = @ContentUrls,
				[IsPushed] = @IsPushed,
				[IsDeleted] = @IsDeleted,
				[ErrorMessage] = @ErrorMessage,
				[ErrorStackTrace] = @ErrorStackTrace
			WHERE [MessageActivityID] = @MessageActivityID
		END
	END