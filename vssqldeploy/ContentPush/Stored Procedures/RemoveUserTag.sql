﻿
		CREATE PROCEDURE [ContentPush].[RemoveUserTag] @AppKey nvarchar(100), @UserId uniqueidentifier, @TagGroup nvarchar(127), @Tag nvarchar(255)
		AS
		BEGIN
			IF EXISTS(SELECT * FROM [ContentPush].[TagGroup] WHERE [GroupName] = @TagGroup)
			BEGIN
				IF NOT EXISTS(SELECT * FROM [ContentPush].[Tag] WHERE [GroupName] = @TagGroup AND [AppKey] = @AppKey AND [TagValue] = @Tag AND[IsActive] = 1)
				BEGIN
					INSERT INTO [ContentPush].[Tag]([GroupName], [AppKey], [TagValue])
					VALUES(@TagGroup, @AppKey, @Tag)
				END
				DECLARE @TagId uniqueidentifier;
				SET @TagId = (SELECT TOP(1) [TagID] FROM [ContentPush].[Tag] WHERE [GroupName] = @TagGroup AND [AppKey] = @AppKey AND [TagValue] = @Tag AND[IsActive] = 1);
				IF EXISTS(SELECT * FROM [ContentPush].[LKTag_User] 
					WHERE [UserID] = @UserId
						AND [TagID] = @TagId)
				BEGIN
					UPDATE [ContentPush].[LKTag_User]
					SET
						[IsActive] = 0,
						[LastUpdated] = GETUTCDATE()
					WHERE
						[UserID] = @UserId AND
						[TagID] = @TagId AND
						[IsActive] = 1
				END
			END
		END