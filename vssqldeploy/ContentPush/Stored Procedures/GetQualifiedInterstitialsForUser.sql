﻿


CREATE   PROCEDURE [ContentPush].[GetQualifiedInterstitialsForUser]
	@UserID uniqueidentifier,
	@TenantID uniqueidentifier,
	@OfferIDs nvarchar(max)
AS
BEGIN
	DECLARE @OfferID TABLE (OfferID uniqueidentifier)
	INSERT INTO @OfferID (OfferID) (SELECT * FROM STRING_SPLIT(@OfferIDs, ','))

	DECLARE @OfferInterstitials TABLE
	(
		InterstitialZoneID     uniqueidentifier,
		MaxOffersDisplayCount  int
	)
	INSERT INTO @OfferInterstitials (InterstitialZoneID, MaxOffersDisplayCount)
	(
		SELECT [InterstitialZoneID], [MaxOffersDisplayCount]
		FROM [ContentPush].[InterstitialZone] 
		WHERE [OfferID] IN (SELECT OfferID FROM @OfferID)
	)

	DECLARE @IzTags TABLE
	(
		InterstitialZoneID uniqueidentifier,
		TagID uniqueidentifier
	)
	INSERT INTO @IzTags (InterstitialZoneID, TagID)
	(
		SELECT InterstitialZoneID, TagID
		FROM [ContentPush].[LKInterstitial_Targeting_Tag]
		WHERE InterstitialZoneID in (SELECT InterstitialZoneID FROM @OfferInterstitials)
	)

	DECLARE @UserTags TABLE (TagID uniqueidentifier)
	
	INSERT INTO @UserTags (TagID)
	(
		SELECT TagID FROM [ContentPush].[LKTag_User] WHERE [UserID] = @UserID
	)

	DECLARE @RemoveInterstitials TABLE (InterstitialZoneID uniqueidentifier)
	INSERT INTO @RemoveInterstitials (InterstitialZoneID)
	(
		SELECT iz.[InterstitialZoneID] as InterstitialZoneID
		FROM       @OfferInterstitials iz
		LEFT JOIN  @IzTags lkIzTag
		ON         iz.InterstitialZoneID = lkIzTag.InterstitialZoneID
		LEFT JOIN  @UserTags lkTagUser
		ON         lkIzTag.TagID = lkTagUser.TagID
		WHERE lkTagUser.TagID IS NULL
		  AND lkIzTag.TagID IS NOT NULL
		GROUP BY iz.[InterstitialZoneID]
	)
	
	DELETE FROM @OfferInterstitials WHERE InterstitialZoneID in (SELECT InterstitialZoneID FROM @RemoveInterstitials)

	DECLARE @UserInteraction TABLE
	(
		InterstitialZoneID uniqueidentifier,
		InterstitialViewCount int
	)
	INSERT INTO @UserInteraction (InterstitialZoneID, InterstitialViewCount)
	(
		SELECT ui.[InterstitialZoneID], COUNT(ui.[InterstitialZoneID]) as InterstitialViewCount
		FROM       @OfferInterstitials oi
		INNER JOIN [ContentPush].[LKInterstitial_User_Interactions] ui
		on         oi.InterstitialZoneID = ui.InterstitialZoneID
		WHERE UserID = @UserID
		GROUP BY ui.[InterstitialZoneID]
	)

	DELETE FROM @OfferInterstitials WHERE InterstitialZoneID in (SELECT InterstitialZoneID FROM @UserInteraction WHERE InterstitialViewCount > MaxOffersDisplayCount)

	DECLARE  @CurrentDate AS datetime2(7)
	SET @CurrentDate = getutcdate()
	 
	SELECT [InterstitialZoneID],
		    [TenantID],
		    [OfferID],
		    [ZoneInfoID],
		    [CampaignID],
		    [ZoneName],
		    [MaxOffersDisplayCount],
		    [ImageUrl],
		    [Headline],
		    [Description],
		    [ContentPosition],
		    [LinkType],
		    [LinkType2],
		    [BimLinkType],
		    [CtaText],
		    [CtaText2],
		    [Url],
		    [Url2],
		    [CardDisplayType],
		    [IsActive],
		    [IsDeleted],
		    [CreatedOn],
		    [LastUpdated],
		    [ExpirationDate]
	FROM [ContentPush].[InterstitialZone]
	WHERE [InterstitialZoneID] in (SELECT InterstitialZoneID FROM @OfferInterstitials)
	  AND [IsDeleted] = 0
	  AND ([ExpirationDate] IS NULL OR (ExpirationDate >= @CurrentDate))
END
