﻿




CREATE   PROCEDURE [ContentPush].[PostMessage] 
	@MessageID uniqueidentifier,
	@TenantID uniqueidentifier,
	@OfferID uniqueidentifier,
	@CampaignID int,
	@MessageName nvarchar(255),
	@MessageType nvarchar(255),
	@OrchestrationType nvarchar(255),
	@Audience nvarchar(max),
	@DeviceTypes nvarchar(max),
	@Notification nvarchar(max),
	@Campaigns nvarchar(max),
	@Options nvarchar(max),
	@InAppMessage nvarchar(max),
	@RichMessage nvarchar(max),
	@MessageFilterType nvarchar(255),
	@CustomEventType nvarchar(255),
	@FilterData nvarchar(255),
	@ControlGroup float,
	@ExperimentDescription nvarchar(max),
	@ExperimentVariants nvarchar(max),
	@ScheduleDateTime DATETIME,
	@ScheduleLocalTime BIT,
	@IsActive BIT,
	@IsDraft bit,
	@AppKey NVARCHAR(100),
	@TriggerStartDate datetime2(7),
	@TriggerEndDate datetime2(7)
AS
BEGIN
	IF NOT EXISTS(SELECT * FROM [ContentPush].[Message] WHERE [MessageID] = @MessageID)
	BEGIN
		INSERT INTO [ContentPush].[Message]([MessageID],[TenantID],[OfferID],[CampaignID],[MessageName],[MessageType],[OrchestrationType],[Audience]
		,[DeviceTypes],[Notification],[Campaigns],[Options],[InAppMessage],[RichMessage],[MessageFilterType],[CustomEventType],[FilterData],[ControlGroup]
		,[ExperimentDescription],[ExperimentVariants],[ScheduleDateTime],[ScheduleLocalTime],[IsActive],[IsDraft],[AppKey],[TriggerStartDate],[TriggerEndDate])
		VALUES
			(@MessageID, @TenantID, @OfferID, @CampaignID, @MessageName, @MessageType, @OrchestrationType, @Audience, 
			@DeviceTypes, @Notification, @Campaigns, @Options, @InAppMessage, @RichMessage, @MessageFilterType, @CustomEventType, @FilterData, @ControlGroup, 
			@ExperimentDescription, @ExperimentVariants, @ScheduleDateTime, @ScheduleLocalTime, @IsActive, @IsDraft, @AppKey, @TriggerStartDate, @TriggerEndDate)
	END
END
