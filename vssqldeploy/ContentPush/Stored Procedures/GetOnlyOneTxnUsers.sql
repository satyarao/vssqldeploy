﻿
CREATE   PROCEDURE [ContentPush].[GetOnlyOneTxnUsers]
	@TenantID uniqueidentifier
AS
BEGIN
	WITH UsersWithExactlyOneTransaction AS (
	   SELECT      ui.UserID,
				   COUNT(bkt.BasketID) [NumTxns]
	   FROM        [dbo].[UserInfo] ui  WITH(NOLOCK)
	   LEFT JOIN   [dbo].[Basket] bkt	WITH(NOLOCK)
	   ON          bkt.[UserID] = ui.[UserID]
	   WHERE       [TenantID] = @TenantID
	   GROUP BY    ui.[UserID]
   )
   SELECT  DISTINCT UserID
   FROM    UsersWithExactlyOneTransaction AS tbl
   WHERE   [NumTxns] = 1 AND 
		   DATEDIFF(
			   DAY, 
			   (SELECT TOP(1) POSDateTime FROM [dbo].[Basket] lbkt WHERE lbkt.[UserID] = tbl.[UserID]),
			   GETUTCDATE()
			) >= 30
END
