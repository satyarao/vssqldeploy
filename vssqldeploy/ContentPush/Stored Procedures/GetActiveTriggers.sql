﻿



CREATE   PROCEDURE [ContentPush].[GetActiveTriggers] 
	@TenantId uniqueidentifier = NULL
AS
BEGIN
	SELECT [MessageID]
      ,[TenantID]
      ,[OfferID]
      ,[MessageName]
      ,[MessageType]
      ,[OrchestrationType]
      ,[Audience]
      ,[DeviceTypes]
      ,[Notification]
      ,[Campaigns]
      ,[Options]
      ,[InAppMessage]
      ,[RichMessage]
      ,[MessageFilterType]
      ,[CustomEventType]
      ,[ControlGroup]
      ,[ExperimentDescription]
      ,[ExperimentVariants]
      ,[ScheduleDateTime]
      ,[ScheduleLocalTime]
      ,[IsActive]
      ,[IsDeleted]
      ,[CreatedOn]
      ,[LastUpdated]
      ,[AppKey]
	  ,[TriggerStartDate]
	  ,[TriggerEndDate]
      ,[FilterData]
	  FROM [ContentPush].[Message]
	  WHERE [MessageType] = 'trigger'
	  AND [IsActive] = 1 
	  AND [IsDeleted] = 0 
	  AND (@TenantId IS NULL OR [TenantID] = @TenantId)
	  AND [IsDraft] = 0
END
