﻿
CREATE   PROCEDURE [ContentPush].[GetInterstitalInteractionsCounts]
	@InterstitialZoneID uniqueidentifier
AS
BEGIN
	SELECT COUNT(*), UserAction
	FROM [ContentPush].[LKInterstitial_User_Interactions]
	WITH (NOLOCK)
	WHERE [InterstitialZoneID] = @InterstitialZoneID
	GROUP BY [UserAction]
END
