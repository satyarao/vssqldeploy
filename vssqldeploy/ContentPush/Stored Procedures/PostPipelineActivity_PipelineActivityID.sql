﻿
		CREATE PROCEDURE [ContentPush].[PostPipelineActivity_PipelineActivityID]
			@PipelineActivityID uniqueidentifier, @MessageID uniqueidentifier, @EngageEventID uniqueidentifier, 
			@FilterDataInfo nvarchar(max), @IsApply bit, @ErrorMessage nvarchar(255), @ErrorStackTrace nvarchar(max)
		AS
		BEGIN
			IF NOT EXISTS(SELECT * FROM [ContentPush].[PipelineActivity] WHERE [PipelineActivityID] = @PipelineActivityID)
			BEGIN
				INSERT INTO [ContentPush].[PipelineActivity]
					([PipelineActivityID],[MessageID],[EngageEventID],[FilterDataInfo],[IsApply],[ErrorMessage],[ErrorStackTrace])
				VALUES
					(@PipelineActivityID, @MessageID, @EngageEventID, @FilterDataInfo, @IsApply, @ErrorMessage, @ErrorStackTrace)
			END
		END