﻿
		CREATE PROCEDURE [ContentPush].[PostMessageActivity] 
			@MessageID uniqueidentifier, @PipelineActivityID uniqueidentifier, @RequestObject nvarchar(max), @ResponseObject nvarchar(max), 
			@MessageDeliveryType nvarchar(255), @OperationID uniqueidentifier, @ScheduleIDs nvarchar(max), 
			@ExperimentIDs nvarchar(max), @PushIDs nvarchar(max), @MessageIDs nvarchar(max), @ScheduleItemInfo nvarchar(max), @UrlIDs nvarchar(max), 
			@ContentUrls nvarchar(max), @IsPushed bit, @IsDeleted bit, @ErrorMessage nvarchar(255), @ErrorStackTrace nvarchar(max)
		AS
		BEGIN
			INSERT INTO [ContentPush].[MessageActivity]
				([MessageID],[PipelineActivityID],[RequestObject],[ResponseObject],[MessageDeliveryType],[OperationID],[ScheduleIDs],
				[ExperimentIDs],[PushIDs],[MessageIDs],[ScheduleItemInfo],[UrlIDs],[ContentUrls],[IsPushed],[IsDeleted],[ErrorMessage],[ErrorStackTrace])
			VALUES
				(@MessageID, @PipelineActivityID, @RequestObject, @ResponseObject, @MessageDeliveryType, @OperationID, @ScheduleIDs, 
				@ExperimentIDs, @PushIDs, @MessageIDs, @ScheduleItemInfo, @UrlIDs, @ContentUrls, @IsPushed, @IsDeleted, @ErrorMessage, @ErrorStackTrace)
		END