﻿
		CREATE PROCEDURE [ContentPush].[PostNewChannel] 
			@ChannelId UNIQUEIDENTIFIER, @UserId UNIQUEIDENTIFIER, @AppKey NVARCHAR(100), @DeviceType NVARCHAR(20),
			@IsInstalled BIT, @IsActive BIT
		AS
		BEGIN
			IF NOT EXISTS(SELECT * FROM [ContentPush].[Channel] CHN WITH(NOLOCK) WHERE CHN.[ChannelID] = @ChannelId)
				BEGIN
					INSERT INTO [ContentPush].[Channel]([ChannelID],[AppKey],[DeviceType],[IsInstalled],[OptedIn],[LastUpdated])
						VALUES(@ChannelId, @AppKey, @DeviceType, @IsInstalled, @IsActive, GETUTCDATE()); 
				END
			ELSE
				BEGIN
					UPDATE [ContentPush].[Channel]
					SET
						[DeviceType] = @DeviceType,
						[IsInstalled] = @IsInstalled,
						[OptedIn] = @IsActive,
						[LastUpdated] = GETUTCDATE()
					WHERE 
						[ChannelID] = @ChannelId;
				END

			IF NOT EXISTS(SELECT * FROM [ContentPush].[LKUser_Channel] WITH(NOLOCK) WHERE [ChannelID] = @ChannelId)
				BEGIN
					INSERT INTO [ContentPush].[LKUser_Channel]([UserID],[ChannelID],[IsActive],[LastUpdated])
						VALUES(@UserId, @ChannelId, 1, GETUTCDATE())
				END
			ELSE
				BEGIN
					UPDATE [ContentPush].[LKUser_Channel]
					SET 
						[UserID] = @UserId,
						[IsActive] = 1,
						[LastUpdated] = GETUTCDATE()
					WHERE 
						[ChannelID] = @ChannelId;
				END
		END