﻿
CREATE   PROCEDURE [ContentPush].[FixUsersWhoAddedAWallet]
	@UserIDString nvarchar(max),
	@TenantID uniqueidentifier
AS
BEGIN
	DECLARE @NoWalletTagID uniqueidentifier = 	(
													SELECT TagID 
													FROM [ContentPush].[Tag] 
													WHERE [TenantID] = @TenantID 
													AND TagValue= 'account_no_wallet'
												)
	
	DECLARE @NewlyAddedWallets TABLE (UserID uniqueidentifier);
	
	WITH ActualNoWalletUsers AS
	(
		SELECT value AS [UserID] FROM STRING_SPLIT(@UserIDString, ',') WHERE TRY_CONVERT(uniqueidentifier, value) IS NOT NULL
	)
	
	INSERT INTO @NewlyAddedWallets (UserID) (
												SELECT tag.UserID
												FROM [ContentPush].[LKTag_User] tag WITH(NOLOCK)
												LEFT JOIN ActualNoWalletUsers nwu   WITH(NOLOCK)
												ON tag.[UserID] = nwu.[UserID]
												WHERE [TagID] = @NoWalletTagID
												  AND nwu.UserID IS NULL
											)
											
	UPDATE [ContentPush].[LKTag_User]
	SET
	[IsActive]		= 0,
	[LastUpdated] 	= GETUTCDATE()
	WHERE [UserID] IN (SELECT UserID FROM @NewlyAddedWallets)
	  AND [TagID] = @NoWalletTagID
	  
	SELECT UserID FROM @NewlyAddedWallets
END
