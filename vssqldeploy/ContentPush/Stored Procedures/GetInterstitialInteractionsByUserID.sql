﻿
CREATE   PROCEDURE [ContentPush].[GetInterstitialInteractionsByUserID]
	@InterstitialZoneID uniqueidentifier,
	@UserID uniqueidentifier
AS
BEGIN
	SELECT UserAction
	FROM [ContentPush].[LKInterstitial_User_Interactions]
	WITH (NOLOCK)
	WHERE [InterstitialZoneID] = @InterstitialZoneID 
	  AND [UserID] = @UserID
	GROUP BY [UserAction]
END
