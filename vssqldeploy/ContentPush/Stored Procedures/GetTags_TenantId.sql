﻿
CREATE PROCEDURE [ContentPush].[GetTags_TenantId]
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	SELECT [TagID] AS 'tag_id',[GroupName] AS 'group_name',[TagValue] AS 'tag_value'
	FROM [ContentPush].[Tag]
	WHERE [TenantID] = @TenantId AND [GroupName] = 'custom_tag' AND [IsActive] = 1
	FOR JSON PATH
END