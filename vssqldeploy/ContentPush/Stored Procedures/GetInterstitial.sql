﻿
CREATE   PROCEDURE [ContentPush].[GetInterstitial]
	@TenantID uniqueidentifier,
	@InterstitialZoneID uniqueidentifier = NULL
AS
BEGIN
	DECLARE  @CurrentDate AS datetime2(7)
	SET @CurrentDate = getutcdate()
	
	SELECT  iz.[InterstitialZoneID],
		    iz.[TenantID],
			iz.[OfferID],
			iz.[ZoneInfoID],
			iz.[CampaignID],
			iz.[ZoneName],
			iz.[MaxOffersDisplayCount],
			iz.[ImageUrl],
			iz.[Headline],
			iz.[Description],
			iz.[ContentPosition],
			iz.[LinkType],
			iz.[LinkType2],
			iz.[BimLinkType],
			iz.[CtaText],
			iz.[CtaText2],
			iz.[Url],
			iz.[Url2],
			iz.[CardDisplayType],
			iz.[IsActive],
			iz.[IsDeleted],
			iz.[CreatedOn],
			iz.[LastUpdated],
			iz.[ExpirationDate]
	FROM [ContentPush].[InterstitialZone] iz
	WHERE iz.[TenantID] = @TenantID
	  AND iz.[InterstitialZoneID] = ISNULL(@InterstitialZoneID, [InterstitialZoneID])
	  AND iz.[IsDeleted] = 0
	  AND (iz.[ExpirationDate] IS NULL OR (ExpirationDate >= @CurrentDate AND @InterstitialZoneID IS NULL))
	  AND (SELECT COUNT(TagID) FROM [ContentPush].[LKInterstitial_Targeting_Tag] tag WHERE tag.[InterstitialZoneID] = iz.[InterstitialZoneID]) = 0
	FOR JSON PATH
END
