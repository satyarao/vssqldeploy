﻿
-- Procedures for user interactions
CREATE   PROCEDURE [ContentPush].[InsertInterstitialUserInteraction]
	@InterstitialZoneID uniqueidentifier,
	@UserID uniqueidentifier,
	@OfferID uniqueidentifier,
	@UserAction nvarchar(30),
	@Tag nvarchar(255)
AS
BEGIN TRY
	BEGIN TRANSACTION
	
		INSERT INTO [ContentPush].[LKInterstitial_User_Interactions]
			(
				[InterstitialZoneID],
				[UserID],
				[OfferID], 
				[UserAction],
				[Tag]
			)
		VALUES
			(
				@InterstitialZoneID, 
				@UserID, 
				@OfferID, 
				@UserAction,
				@Tag
			)
	
	COMMIT TRANSACTION
	RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @InterstitialZoneID AS [InterstitialZoneID]
		   ,@UserID AS [UserID]
		   ,@OfferID AS [OfferID]
		   ,@UserAction AS [UserAction]
		   ,@Tag AS [Tag]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
