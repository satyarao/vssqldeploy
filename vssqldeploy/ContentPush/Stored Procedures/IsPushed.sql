﻿
		CREATE PROCEDURE [ContentPush].[IsPushed] @MessageId uniqueidentifier
		AS
		BEGIN
			SELECT
				CASE
					WHEN EXISTS(SELECT * FROM [ContentPush].[MessageActivity] WHERE [MessageID] = @MessageId)
						THEN 1
					ELSE 0
				END
		END