﻿
		CREATE PROCEDURE [ContentPush].[PostChannelDetails] 
			@ChannelId UNIQUEIDENTIFIER, @AppKey NVARCHAR(100), @DeviceType NVARCHAR(100), @OptedIn BIT, @AppPackageName NVARCHAR(255),
			@AppVersion NVARCHAR(255), @BackgroundPushEnabled BIT, @Carrier NVARCHAR(255), @DeviceModel NVARCHAR(255), @DeviceOs NVARCHAR(255),
			@IanaTimezone NVARCHAR(255), @LocaleCountryCode NVARCHAR(255), @LocaleLanguageCode NVARCHAR(255), @LocaleTimezone NVARCHAR(255),
			@LocaleVariant NVARCHAR(255), @LocationEnabled BIT, @LocationPermission NVARCHAR(255), @UaSdkVersion NVARCHAR(255), @Occurred DATETIME
		AS
		BEGIN
			IF NOT EXISTS(SELECT * FROM [ContentPush].[Channel] WHERE [ChannelID] = @ChannelId)
			BEGIN
				INSERT INTO [ContentPush].[Channel]([ChannelID],[AppKey],[DeviceType],[IsInstalled],[OptedIn]
					,[AppPackageName],[AppVersion],[BackgroundPushEnabled],[Carrier],[DeviceModel],[DeviceOs],[IanaTimezone],[LocaleCountryCode],[LocaleLanguageCode]
					,[LocaleTimezone],[LocaleVariant],[LocationEnabled],[LocationPermission],[UaSdkVersion],[LastUpdated])
				VALUES
					(@ChannelId, @AppKey, @DeviceType, 1, @OptedIn, 
					@AppPackageName, @AppVersion, @BackgroundPushEnabled, @Carrier, @DeviceModel, @DeviceOs, @IanaTimezone, @LocaleCountryCode, @LocaleLanguageCode,
					@LocaleTimezone, @LocaleVariant, @LocationEnabled, @LocationPermission, @UaSdkVersion, @Occurred);
			END
			ELSE
			BEGIN
				IF EXISTS(SELECT * FROM [ContentPush].[Channel] WHERE [ChannelID] = @ChannelId AND [LastUpdated] < @Occurred)
				BEGIN
					UPDATE [ContentPush].[Channel]
					SET
						[AppPackageName]		 = @AppPackageName,
						[AppVersion]			 = @AppVersion,
						[BackgroundPushEnabled]	 = @BackgroundPushEnabled,
						[Carrier]				 = @Carrier,
						[DeviceModel]			 = @DeviceModel,
						[DeviceOs]				 = @DeviceOs,
						[IanaTimezone]			 = @IanaTimezone,
						[LocaleCountryCode]		 = @LocaleCountryCode,
						[LocaleLanguageCode]	 = @LocaleLanguageCode,
						[LocaleTimezone]		 = @LocaleTimezone,
						[LocaleVariant]			 = @LocaleVariant,
						[LocationEnabled]		 = @LocationEnabled,
						[LocationPermission]	 = @LocationPermission,
						[UaSdkVersion]			 = @UaSdkVersion,
						[LastUpdated]			 = @Occurred
					WHERE [ChannelID] = @ChannelId
				END
			END
		END