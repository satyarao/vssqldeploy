﻿
		CREATE PROCEDURE [ContentPush].[UpsertChannelTag] @AppKey nvarchar(100), @ChannelId uniqueidentifier, @TagGroup nvarchar(127), @Tag nvarchar(255)
		AS
		BEGIN
			IF EXISTS(SELECT * FROM [ContentPush].[TagGroup] WHERE [GroupName] = @TagGroup)
			BEGIN
				IF NOT EXISTS(SELECT * FROM [ContentPush].[Tag] WHERE [GroupName] = @TagGroup AND [AppKey] = @AppKey AND [TagValue] = @Tag AND[IsActive] = 1)
				BEGIN
					INSERT INTO [ContentPush].[Tag]([GroupName], [AppKey], [TagValue])
					VALUES(@TagGroup, @AppKey, @Tag)
				END
				DECLARE @TagId uniqueidentifier;
				SET @TagId = (SELECT TOP(1) [TagID] FROM [ContentPush].[Tag] WHERE [GroupName] = @TagGroup AND [AppKey] = @AppKey AND [TagValue] = @Tag AND[IsActive] = 1);
				IF NOT EXISTS(SELECT * FROM [ContentPush].[LKTag_Channel] 
					WHERE [ChannelID] = @ChannelId 
						AND [TagID] = @TagId)
				BEGIN
					INSERT INTO [ContentPush].[LKTag_Channel]([ChannelID], [TagID], [IsActive], [LastUpdated])
					VALUES (@ChannelId, @TagId, 1, GETUTCDATE());
				END
				ELSE
				BEGIN
					UPDATE [ContentPush].[LKTag_Channel]
					SET
						[IsActive] = 1,
						[LastUpdated] = GETUTCDATE()
					WHERE
						[ChannelID] = @ChannelId AND
						[TagID] = @TagId AND
						[IsActive] = 0
				END
			END
		END