﻿
CREATE PROCEDURE [ContentPush].[GetMessageActivityById] @MessageActivityId uniqueidentifier
AS
BEGIN
	SELECT  ACT.[MessageActivityID],
		ACT.[MessageID],
		ACT.[PipelineActivityID],
		ACT.[RequestObject],
		ACT.[ResponseObject],
		ACT.[MessageDeliveryType],
		ACT.[OperationID],
		ACT.[ScheduleIDs],
		ACT.[ExperimentIDs],
		ACT.[PushIDs],
		ACT.[MessageIDs],
		ACT.[ScheduleItemInfo],
		ACT.[UrlIDs],
		ACT.[ContentUrls],
		ACT.[CreatedOn],
		ACT.[IsPushed],
		ACT.[IsDeleted],
		ACT.[ErrorMessage],
		ACT.[ErrorStackTrace]
	FROM [ContentPush].[MessageActivity] ACT WHERE [MessageActivityID] = @MessageActivityId
END
