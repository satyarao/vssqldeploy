﻿
CREATE   PROCEDURE [ContentPush].[GetInterstitialsByID]
	@InterstitialZoneIDs nvarchar(max)
AS
BEGIN
	With Tags AS
	(
		SELECT InterstitialZoneID,
		(SELECT TagID AS id FROM [ContentPush].[LKInterstitial_Targeting_Tag] t WHERE IzTags.InterstitialZoneID = t.InterstitialZoneID AND [IsDeleted] = 0 FOR JSON PATH) AS Tags
		FROM [ContentPush].[LKInterstitial_Targeting_Tag] IzTags
		WHERE [IsDeleted] = 0
		  AND [InterstitialZoneID] in (SELECT VALUE FROM STRING_SPLIT(@InterstitialZoneIDs, ','))
		GROUP BY InterstitialZoneID
	)

	SELECT  iz.[InterstitialZoneID]															AS [InterstitialZoneID],
			iz.[TenantID]																	AS [TenantID],
			iz.[OfferID]																	AS [OfferID],
			iz.[ZoneInfoID]																	AS [ZoneInfoID],
			iz.[CampaignID]																	AS [CampaignID],
			iz.[ZoneName]																	AS [ZoneName],
			iz.[MaxOffersDisplayCount]														AS [MaxOffersDisplayCount],
			iz.[ImageUrl]																	AS [ImageUrl],
			iz.[Headline]																	AS [Headline],
			iz.[Description]																AS [Description],
			iz.[ContentPosition]															AS [ContentPosition],
			iz.[LinkType]																	AS [LinkType],
			iz.[LinkType2]																	AS [LinkType2],
			iz.[BimLinkType]																AS [BimLinkType],
			iz.[CtaText]																	AS [CtaText],
			iz.[CtaText2]																	AS [CtaText2],
			iz.[Url]																		AS [Url],
			iz.[Url2]																		AS [Url2],
			iz.[CardDisplayType]															AS [CardDisplayType],
			iz.[IsActive]																	AS [IsActive],
			iz.[IsDeleted]																	AS [IsDeleted],
			iz.[CreatedOn]																	AS [CreatedOn],
			iz.[LastUpdated]																AS [LastUpdated],
			iz.[ExpirationDate]																AS [ExpirationDate],
			(SELECT Tags FROM Tags WHERE iz.InterstitialZoneID = Tags.InterstitialZoneID)	AS [tags]
	FROM 	[ContentPush].[InterstitialZone] AS iz
	WHERE 	iz.[InterstitialZoneID] in (SELECT VALUE FROM string_split(@InterstitialZoneIDs, ','))
		AND 	[IsDeleted] = 0
	FOR JSON PATH
END
