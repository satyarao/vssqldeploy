﻿
		CREATE PROCEDURE [ContentPush].[PostCustomTag]
			@TenantId UNIQUEIDENTIFIER, @TagValue nvarchar(255), @Environment nvarchar(100)
		AS
		BEGIN
			IF EXISTS(SELECT * FROM [ContentPush].[LKProject_TagGroup] 
					WHERE [AppKey] = (SELECT [AppKey] FROM [ContentPush].[LKProject_Tenant] WHERE [TenantID] = @TenantId AND [Environment] = @Environment) AND
						[GroupName] = 'custom_tag' AND [IsActive] = 1)
			BEGIN
				INSERT INTO [ContentPush].[Tag]([GroupName],[AppKey],[TenantID],[TagValue])
				VALUES
					('custom_tag', (SELECT [AppKey] FROM [ContentPush].[LKProject_Tenant] WHERE [TenantID] = @TenantId AND [Environment] = @Environment),
					@TenantId, @TagValue)
			END
		END