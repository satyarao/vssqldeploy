﻿-- =============================================
-- Author: Dzmitry Mikhailau 
-- Create date: 04/08/2019
-- Description: DM-647 - Display Opt-in Channels to Audience List & Detail View
-- =============================================

CREATE   PROCEDURE [ContentPush].[GetChannelActivityByUserIds] @UserIds NVARCHAR(MAX)
AS
BEGIN
	SELECT cha.ChannelActivityID AS ChannelActivityId,
		cha.AppKey,
		cha.ChannelID AS ChannelId,
		cha.UserID AS UserId,
		cha.DeviceType,
		cha.RegistrationType,
		cha.SubscriptionType,
		cha.AssociateNamedUserResponse,
		cha.RegisterationResponse,
		cha.CreatedOn,
		cha.WarningrMessage,
		cha.ErrorMessage,
		cha.ErrorStackTrace
	FROM [ContentPush].[ChannelActivity] AS cha
	INNER JOIN [ContentPush].[Channel] AS ch ON ch.ChannelID = cha.ChannelID
	WHERE [UserID] IN (SELECT value FROM STRING_SPLIT(@UserIds, ','))
		  AND ch.OptedIn = 1
END
