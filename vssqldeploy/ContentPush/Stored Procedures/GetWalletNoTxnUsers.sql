﻿
CREATE   PROCEDURE [ContentPush].[GetWalletNoTxnUsers]
	@TenantID uniqueidentifier
AS
BEGIN
   SELECT DISTINCT ui.UserID
   FROM [dbo].[UserInfo] ui 					WITH(NOLOCK)
   LEFT JOIN [Payment].[LKUserPaymentSource] ps WITH(NOLOCK)
   ON ps.[UserID] = ui.[UserID]
   LEFT JOIN [dbo].[Basket] bkt					WITH(NOLOCK)
   ON bkt.[UserID] = ui.[UserID]
   WHERE [TenantID] = @TenantID AND ps.[UserID] IS NOT NULL AND bkt.[UserID] IS NULL
END
