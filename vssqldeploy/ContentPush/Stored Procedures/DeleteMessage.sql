﻿
		CREATE PROCEDURE [ContentPush].[DeleteMessage] @MessageID uniqueidentifier
		AS
		BEGIN
			UPDATE [ContentPush].[Message]
			SET
				[IsDeleted] = 1
			WHERE [MessageID] = @MessageID
		END