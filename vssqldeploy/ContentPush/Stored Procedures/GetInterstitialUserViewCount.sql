﻿
CREATE   PROCEDURE [ContentPush].[GetInterstitialUserViewCount]
	@InterstitialZoneID uniqueidentifier,
	@UserID uniqueidentifier
AS
BEGIN
	SELECT COUNT(*) 
	FROM [ContentPush].[LKInterstitial_User_Interactions] 
	WITH (NOLOCK)
	WHERE [InterstitialZoneID] = @InterstitialZoneID 
	  AND [UserID] = @UserID
END
