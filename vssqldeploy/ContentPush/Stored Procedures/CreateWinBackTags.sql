﻿CREATE   PROCEDURE [ContentPush].[CreateWinBackTags]
	@TenantID uniqueidentifier,
	@AppKey   nvarchar(max)
AS
BEGIN
	IF (SELECT TOP 1 TagID FROM [ContentPush].[Tag] WHERE [TagValue] = 'account_no_wallet' AND TenantID = @TenantID) IS NULL
	BEGIN
	   INSERT INTO [ContentPush].[Tag]
	   ([TagID], [GroupName], [AppKey], [TenantID], [TagValue], [IsActive], [Version])
	   VALUES (NEWID(), 'custom_tag', @AppKey, @TenantID, 'account_no_wallet', 1, 1)
	END

	IF (SELECT TOP 1 TagID FROM [ContentPush].[Tag] WHERE [TagValue] = 'wallet_no_txn' AND TenantID = @TenantID) IS NULL
	BEGIN
	   INSERT INTO [ContentPush].[Tag]
	   ([TagID], [GroupName], [AppKey], [TenantID], [TagValue], [IsActive], [Version])
	   VALUES (NEWID(), 'custom_tag', @AppKey, @TenantID, 'wallet_no_txn', 1, 1)
	END

	IF (SELECT TOP 1 TagID FROM [ContentPush].[Tag] WHERE [TagValue] = 'only_one_old_txn' AND TenantID = @TenantID) IS NULL
	BEGIN
	   INSERT INTO [ContentPush].[Tag]
	   ([TagID], [GroupName], [AppKey], [TenantID], [TagValue], [IsActive], [Version])
	   VALUES (NEWID(), 'custom_tag', @AppKey, @TenantID, 'only_one_old_txn', 1, 1)
	END
END
