﻿
CREATE   PROCEDURE [ContentPush].[FixUsersWhoMadeATxn]
	@UserIDString nvarchar(max),
	@TenantID uniqueidentifier
AS
BEGIN
	DECLARE @NoTxnTagID uniqueidentifier = 	(
													SELECT TagID 
													FROM [ContentPush].[Tag] 
													WHERE [TenantID] = @TenantID 
													AND TagValue= 'wallet_no_txn'
												)
	
	DECLARE @NewTxnUsers TABLE (UserID uniqueidentifier);
	
	WITH ActualNoTxnUsers AS
	(
		SELECT value AS [UserID] FROM STRING_SPLIT(@UserIDString, ',') WHERE TRY_CONVERT(uniqueidentifier, value) IS NOT NULL
	)
	
	INSERT INTO @NewTxnUsers (UserID) (
												SELECT tag.UserID
												FROM [ContentPush].[LKTag_User] tag WITH(NOLOCK)
												LEFT JOIN ActualNoTxnUsers txn		WITH(NOLOCK)
												ON tag.[UserID] = txn.[UserID]
												WHERE [TagID] = @NoTxnTagID
												  AND txn.UserID IS NULL
											)
											
	UPDATE [ContentPush].[LKTag_User]
	SET
	[IsActive]		= 0,
	[LastUpdated] 	= GETUTCDATE()
	WHERE [UserID] IN (SELECT UserID FROM @NewTxnUsers)
	  AND [TagID] = @NoTxnTagID
	  
	SELECT UserID FROM @NewTxnUsers
END
