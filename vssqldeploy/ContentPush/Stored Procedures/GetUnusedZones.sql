﻿
CREATE   PROCEDURE [ContentPush].[GetUnusedZones]
	@OfferID  uniqueidentifier
AS
BEGIN
	WITH Offer_CTE AS
	(
		SELECT TenantID, ZoneName, ZoneInfoID
		FROM [ContentPush].[InterstitialZone] 
		WHERE OfferID = @OfferID 
		AND [IsDeleted] = 0 
		AND [IsActive] = 1
	),
	UsedZone_CTE AS
	(
		SELECT ZoneID, i.ZoneName
		FROM Offer_CTE ofr
		INNER JOIN [dbo].[InterstitialZoneInfo] i
		ON ofr.[ZoneInfoID] = i.[ZoneID]
	)
	
	SELECT ZoneID, ZoneName
	FROM [dbo].[InterstitialZoneInfo]
	WHERE ZoneID NOT IN (SELECT ZoneID FROM UsedZone_CTE)
	AND TenantID = (SELECT TenantID FROM Offer.Offer WHERE OfferID = @OfferID)
END
