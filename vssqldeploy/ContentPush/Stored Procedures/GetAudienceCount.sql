﻿CREATE   PROCEDURE [ContentPush].[GetAudienceCount]
	@DeviceTypes nvarchar(MAX),
	@Tags nvarchar(MAX),
	@TenantID nvarchar(MAX)
AS
BEGIN
	SELECT COUNT(DISTINCT Users.UserID) AudienceCount
		FROM ContentPush.LKUser_Channel Users
			 INNER JOIN
			 ContentPush.LKTag_User LKTag_User
				ON Users.UserID = LKTag_User.UserID
		WHERE
			Users.ChannelID in
			(
				SELECT ChannelID
					FROM [ContentPush].[Channel]
					WHERE OptedIn = 1
					  AND DeviceType IN (SELECT VALUE FROM STRING_SPLIT(@DeviceTypes, ','))
			)
			AND
			LKTag_User.TagID in
			(
				SELECT TagID
					FROM ContentPush.Tag
					WHERE TagValue in (SELECT VALUE FROM STRING_SPLIT(@Tags, ','))
					  AND TenantID = @TenantID
					  AND IsActive = 1
			)
END 
