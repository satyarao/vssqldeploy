﻿
CREATE    PROCEDURE [ContentPush].[UpsertInterstitial]
	@json nvarchar(max)
AS
BEGIN TRY
	IF @json IS NULL
	BEGIN
		;THROW 50005, 'Parameter @json is required and cannot be NULL', 1;
	END
	
	BEGIN TRANSACTION

	DECLARE @interstitialZone TABLE
	(
		[InterstitialZoneID]    uniqueidentifier NOT NULL,
		[TenantID]              uniqueidentifier NOT NULL,
		[OfferID]               uniqueidentifier,
		[ZoneInfoID]            uniqueidentifier NOT NULL,
		[ZoneName]              nvarchar(255)    NOT NULL,
		[MaxOffersDisplayCount] INT              NOT NULL,
		[ImageUrl]              nvarchar(255),
		[Headline]              nvarchar(255),
		[Description]           nvarchar(255),
		[ContentPosition]       nvarchar(6),
		[LinkType]              nvarchar(255),
		[LinkType2]             nvarchar(255),
		[BimLinkType]           nvarchar(255),
		[CtaText]               nvarchar(255),
		[CtaText2]              nvarchar(255),
		[Url]                   nvarchar(255),
		[Url2]                  nvarchar(255),
		[CardDisplayType]       nvarchar(30),
		[IsActive]              BIT              NOT NULL,
		--[IsDeleted]             BIT              NOT NULL,
		--[CreatedOn]             datetime2(7),
		--[LastUpdated]           datetime2(7),
		[ExpirationDate]        datetime2(7)
	);
	
	DECLARE @Tags TABLE
	(
		[InterstitialZoneID] uniqueidentifier NOT NULL,
		[Tags]               nvarchar(max)    NOT NULL,
		[TagID]              uniqueidentifier NOT NULL,
		[IsDeleted]          BIT              NOT NULL
	);

	INSERT INTO @interstitialZone
	SELECT * FROM OPENJSON(@json)
	WITH
	(
		[InterstitialZoneID]       uniqueidentifier '$.interstitialZoneId', 
		[TenantID]                 uniqueidentifier '$.tenantId', 
		[OfferID]                  uniqueidentifier '$.offerId', 
		[ZoneInfoID]               uniqueidentifier '$.zoneInfoId',
		[ZoneName]                 nvarchar(255)    '$.zoneName', 
		[MaxOffersDisplayCount]    INT              '$.maxOffersDisplayCount',
		[ImageUrl]                 nvarchar(255)    '$.imageUrl',
		[Headline]                 nvarchar(255)    '$.headline', 
		[Description]              nvarchar(255)    '$.description', 
		[ContentPosition]          nvarchar(6)      '$.contentPosition', 
		[LinkType]                 nvarchar(255)    '$.linkType', 
		[LinkType2]                nvarchar(255)    '$.linkType2',
		[BimLinkType]              nvarchar(255)    '$.bimLinkType', 
		[CtaText]                  nvarchar(255)    '$.ctaText', 
		[CtaText2]                 nvarchar(255)    '$.ctaText2', 
		[Url]                      nvarchar(255)    '$.url', 
		[Url2]                     nvarchar(255)    '$.url2', 
		[CardDisplayType]          nvarchar(30)     '$.cardDisplayType',  
		[IsActive]                 BIT              '$.isActive', 
		[ExpirationDate]           datetime2(7)     '$.expirationDate'
	);
	
	IF ((SELECT COUNT(*) FROM @interstitialZone) != 1)
	BEGIN
		;THROW 50001, 'Must submit exactly one interstitial', 1;
	END
		
	INSERT INTO @tags
	SELECT *, 0 AS [IsDeleted] FROM OPENJSON(@json)
	WITH
	(
		[InterstitialZoneID]      uniqueidentifier '$.interstitialZoneId',
		[tags]                    nvarchar(max)    AS json
	) AS Interstitial
	CROSS APPLY OPENJSON(Interstitial.tags)
	WITH
	(
		[TagID]                   uniqueidentifier '$.id'
	) AS Tags
	WHERE ([TagID] IS NOT NULL)

	SELECT * FROM @tags

	MERGE INTO [ContentPush].[InterstitialZone] AS target
	USING @interstitialZone AS source
	ON target.[InterstitialZoneID] = source.[InterstitialZoneID]
	WHEN MATCHED THEN
		UPDATE SET
			target.[OfferID]				= source.[OfferID]				,
			target.[ZoneInfoID]				= source.[ZoneInfoID]			,	
			target.[ZoneName]				= source.[ZoneName]				,
			target.[MaxOffersDisplayCount]	= source.[MaxOffersDisplayCount],	
			target.[ImageUrl]				= source.[ImageUrl]				,
			target.[Headline]				= source.[Headline]				,
			target.[Description]			= source.[Description]			,
			target.[ContentPosition]		= source.[ContentPosition]		,
			target.[LinkType]				= source.[LinkType]				,
			target.[LinkType2]				= source.[LinkType2]			,	
			target.[BimLinkType]			= source.[BimLinkType]			,
			target.[CtaText]				= source.[CtaText]				,
			target.[CtaText2]				= source.[CtaText2]				,
			target.[Url]					= source.[Url]					,
			target.[Url2]					= source.[Url2]					,
			target.[CardDisplayType]		= source.[CardDisplayType]		,
			target.[IsActive]				= source.[IsActive]				,
			target.[LastUpdated]			= GETUTCDATE()					,
			target.[ExpirationDate]			= source.[ExpirationDate]			
	WHEN NOT MATCHED BY target THEN
		INSERT
			([InterstitialZoneID],[TenantID],[OfferID],[ZoneInfoID],[ZoneName],[MaxOffersDisplayCount],[ImageUrl],
			[Headline],[Description],[ContentPosition],[LinkType],[LinkType2],[BimLinkType],[CtaText],[CtaText2],
			[Url],[Url2],[CardDisplayType],[IsActive],[ExpirationDate])
		VALUES
			([InterstitialZoneID],[TenantID],[OfferID],[ZoneInfoID],[ZoneName],[MaxOffersDisplayCount],[ImageUrl],
			[Headline],[Description],[ContentPosition],[LinkType],[LinkType2],[BimLinkType],[CtaText],[CtaText2],
			[Url],[Url2],[CardDisplayType],[IsActive],[ExpirationDate]);
	
	DECLARE @InterstitialZoneID uniqueidentifier = (SELECT TOP 1 [InterstitialZoneID] FROM @interstitialZone)
	DECLARE @MaxOffersDisplayCount INT = (SELECT [MaxOffersDisplayCount] FROM @interstitialZone)
	
	DELETE FROM [ContentPush].[LKInterstitial_MobileZones]
	WHERE [InterstitialZoneID] = @InterstitialZoneID
	
	WHILE @MaxOffersDisplayCount > 0
	BEGIN
		INSERT INTO [ContentPush].[LKInterstitial_MobileZones]
		(MobileZoneID, InterstitialZoneID)
		VALUES
		(NEWID(), @InterstitialZoneID)
		SET @MaxOffersDisplayCount = @MaxOffersDisplayCount - 1
	END
			
	MERGE INTO [ContentPush].[LKInterstitial_Targeting_Tag] AS target
	USING @tags AS source
	ON    target.[InterstitialZoneID]         = source.[InterstitialZoneID]
	 AND  target.[TagID]                      = source.[TagID]
	WHEN MATCHED THEN 
		UPDATE SET
		  target.[IsDeleted] = source.[IsDeleted]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([InterstitialZoneID], [TagID], [IsDeleted])
		VALUES
			([InterstitialZoneID], [TagID], [IsDeleted]);
			

	-- Mark tags not passed in the json as deleted
	UPDATE [ContentPush].[LKInterstitial_Targeting_Tag]
	SET
		 [IsDeleted] = 1
	WHERE
		 [InterstitialZoneID] = @InterstitialZoneId
	 AND [TagID] NOT IN (SELECT TagID FROM @Tags)
	
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), @json AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
