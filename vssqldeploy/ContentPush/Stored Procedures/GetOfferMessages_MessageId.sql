﻿


CREATE   PROCEDURE [ContentPush].[GetOfferMessages_MessageId] @MessageId UNIQUEIDENTIFIER
AS
BEGIN
	SELECT [MessageID]
		,[TenantID]
		,[OfferID]
		,[CampaignID]
		,[MessageName]
		,[MessageType]
		,[OrchestrationType]
		,[Audience]
		,[DeviceTypes]
		,[Notification]
		,[Campaigns]
		,[Options]
		,[InAppMessage]
		,[RichMessage]
		,[MessageFilterType]
		,[CustomEventType]
		,[FilterData]
		,[ControlGroup]
		,[ExperimentDescription]
		,[ExperimentVariants]
		,[ScheduleDateTime]
		,[ScheduleLocalTime]
		,[IsActive]
		,[IsDeleted]
		,[IsDraft]
		,[CreatedOn]
		,[LastUpdated]
		,[AppKey]
		,[TriggerStartDate]
		,[TriggerEndDate]
	FROM [ContentPush].[Message]
	WHERE [MessageID] = @MessageId
END
