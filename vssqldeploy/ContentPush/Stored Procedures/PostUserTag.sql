﻿
		CREATE PROCEDURE [ContentPush].[PostUserTag] @AppKey NVARCHAR(100), @UserId UNIQUEIDENTIFIER, @GroupName NVARCHAR(127), @Tag NVARCHAR(255)
		AS
		BEGIN
			IF NOT EXISTS(SELECT * FROM [ContentPush].[Tag] WITH(NOLOCK) WHERE [GroupName] = @GroupName AND [AppKey] = @AppKey AND [TagValue] = @Tag) 
			BEGIN
				INSERT INTO [ContentPush].[Tag]([GroupName], [AppKey], [TagValue])
				VALUES (@GroupName, @AppKey, @Tag);
			END
			IF EXISTS(SELECT * FROM [dbo].[UserInfo] WHERE [UserID] = @UserId)
			BEGIN
				INSERT INTO [ContentPush].[LKTag_User]([UserID],[TagID])
				VALUES (@UserId, (SELECT TOP(1) TagID FROM [ContentPush].[Tag] WITH(NOLOCK) WHERE [GroupName] = @GroupName AND [AppKey] = @AppKey AND [TagValue] = @Tag));
			END
		END