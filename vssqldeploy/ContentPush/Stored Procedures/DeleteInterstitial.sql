﻿
CREATE   PROCEDURE [ContentPush].[DeleteInterstitial]
	@InterstitialZoneID uniqueidentifier
AS
BEGIN
	UPDATE [ContentPush].[InterstitialZone] 
	SET [IsDeleted] = 1,
		[LastUpdated] = getutcdate()
	WHERE [InterstitialZoneID] = @InterstitialZoneID
	  AND [IsDeleted] = 0
END
