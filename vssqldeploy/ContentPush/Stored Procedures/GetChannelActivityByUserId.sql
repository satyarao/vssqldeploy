﻿
CREATE PROCEDURE [ContentPush].[GetChannelActivityByUserId]
    @UserId uniqueidentifier
AS
BEGIN
    SELECT [ChannelActivityID] AS ChannelActivityId,
        [AppKey],
        [ChannelID] AS ChannelId,
        [UserID] AS UserId,
        [DeviceType],
        [RegistrationType],
        [SubscriptionType],
        [AssociateNamedUserResponse],
        [RegisterationResponse],
        [CreatedOn],
        [WarningrMessage],
        [ErrorMessage],
        [ErrorStackTrace]
    FROM [ContentPush].[ChannelActivity] WHERE [UserID] = @UserId
END
