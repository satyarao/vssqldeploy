﻿

CREATE   PROCEDURE [ContentPush].[GetOfferMessages_TenantId_OfferIds] @TenantId UNIQUEIDENTIFIER, @OfferIds NVARCHAR(MAX)
AS
BEGIN
	SELECT [MessageID]
		,[TenantID]
		,[OfferID]
		,[CampaignID]
		,[MessageName]
		,[MessageType]
		,[OrchestrationType]
		,[Audience]
		,[DeviceTypes]
		,[Notification]
		,[Campaigns]
		,[Options]
		,[InAppMessage]
		,[RichMessage]
		,[MessageFilterType]
		,[CustomEventType]
		,[FilterData]
		,[ControlGroup]
		,[ExperimentDescription]
		,[ExperimentVariants]
		,[ScheduleDateTime]
		,[ScheduleLocalTime]
		,[IsActive]
		,[IsDraft]
		,[IsDeleted]
		,[CreatedOn]
		,[LastUpdated]
		,[AppKey]
		,[TriggerStartDate]
		,[TriggerEndDate]
	FROM [ContentPush].[Message]
	WHERE [TenantID] = @TenantId AND CONVERT(NVARCHAR(50), [OfferID]) IN (SELECT value FROM STRING_SPLIT(@OfferIds, ','))
END
