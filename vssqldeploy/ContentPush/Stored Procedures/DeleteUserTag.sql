﻿
		CREATE PROCEDURE [ContentPush].[DeleteUserTag] @AppKey NVARCHAR(100), @UserId UNIQUEIDENTIFIER, @GroupName NVARCHAR(127), @Tag NVARCHAR(255)
		AS
		BEGIN
			DELETE FROM [ContentPush].[LKTag_User]
			WHERE [UserID] = @UserId AND [TagID] = (SELECT TOP(1) TagID FROM [ContentPush].[Tag] WITH(NOLOCK) WHERE [GroupName] = @GroupName AND [AppKey] = @AppKey AND [TagValue] = @Tag);
		END