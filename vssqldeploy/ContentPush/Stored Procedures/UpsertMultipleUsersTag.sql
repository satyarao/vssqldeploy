﻿
CREATE   PROCEDURE [ContentPush].[UpsertMultipleUsersTag] @AppKey nvarchar(100), @UserIdString nvarchar(max), @TagGroup nvarchar(127), @Tag nvarchar(255)
AS
BEGIN
	DECLARE @UserIDs TABLE (UserID uniqueidentifier)
	INSERT INTO @UserIDs (UserID)  (SELECT value FROM STRING_SPLIT(@UserIDString, ',') where TRY_CONVERT(uniqueidentifier, value) IS NOT NULL)

	IF EXISTS(SELECT * FROM [ContentPush].[TagGroup] WHERE [GroupName] = @TagGroup)
	BEGIN
		IF NOT EXISTS(SELECT * FROM [ContentPush].[Tag] WHERE [GroupName] = @TagGroup AND [AppKey] = @AppKey AND [TagValue] = @Tag AND[IsActive] = 1)
		BEGIN
			INSERT INTO [ContentPush].[Tag]([GroupName], [AppKey], [TagValue])
			VALUES(@TagGroup, @AppKey, @Tag)
		END
		DECLARE @TagId uniqueidentifier;
		SET @TagId = (SELECT TOP(1) [TagID] FROM [ContentPush].[Tag] WHERE [GroupName] = @TagGroup AND [AppKey] = @AppKey AND [TagValue] = @Tag AND[IsActive] = 1);
		IF NOT EXISTS(SELECT * FROM [ContentPush].[LKTag_User] 
			WHERE [UserID] IN (SELECT UserID FROM @UserIDs)
				AND [TagID] = @TagId)
		BEGIN
			INSERT INTO [ContentPush].[LKTag_User]([UserID], [TagID], [IsActive], [LastUpdated])
			(SELECT UserID, @TagId, 1, GETUTCDATE() FROM @UserIDs);
		END
		ELSE
		BEGIN
			UPDATE [ContentPush].[LKTag_User]
			SET
				[IsActive] = 1,
				[LastUpdated] = GETUTCDATE()
			WHERE
				[UserID] IN (SELECT UserID FROM @UserIDs) AND
				[TagID] = @TagId AND
				[IsActive] = 0
		END
	END
END
