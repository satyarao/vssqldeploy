﻿
CREATE   PROCEDURE [ContentPush].[GetNoWalletUsers]
	@TenantID uniqueidentifier
AS
BEGIN
   SELECT DISTINCT ui.[UserID] 
   FROM		 [dbo].[UserInfo] ui 				WITH(NOLOCK)
   LEFT JOIN [Payment].[LKUserPaymentSource] ps WITH(NOLOCK)
   ON ps.[UserID] = ui.[UserID]
   WHERE [TenantID] = @TenantID
	 AND ps.[UserID] IS NULL
END
