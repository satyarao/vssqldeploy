﻿
CREATE   PROCEDURE [ContentPush].[GetInterstitialsByOfferID]
	@OfferID nvarchar(max)
AS
BEGIN
	SELECT  [InterstitialZoneID],
		    [TenantID],
			[OfferID],
			[ZoneInfoID],
			[CampaignID],
			[ZoneName],
			[MaxOffersDisplayCount],
			[ImageUrl],
			[Headline],
			[Description],
			[ContentPosition],
			[LinkType],
			[LinkType2],
			[BimLinkType],
			[CtaText],
			[CtaText2],
			[Url],
			[Url2],
			[CardDisplayType],
			[IsActive],
			[IsDeleted],
			[CreatedOn],
			[LastUpdated],
			[ExpirationDate]
	FROM 	[ContentPush].[InterstitialZone]
	WHERE 	[OfferID] = @OfferID
	  AND 	[IsDeleted] = 0
	FOR JSON PATH
END
