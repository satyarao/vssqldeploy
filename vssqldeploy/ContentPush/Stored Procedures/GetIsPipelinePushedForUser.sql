﻿
		CREATE PROCEDURE [ContentPush].[GetIsPipelinePushedForUser] @MessageId uniqueidentifier, @UserId uniqueidentifier
		AS
		BEGIN
			SELECT 
			CASE
			WHEN EXISTS(SELECT * 
				FROM [ContentPush].[PipelineActivity] ACT
				INNER JOIN [ContentPush].[EngageEvent] EVNT
					ON ACT.[EngageEventID] = EVNT.[EngageEventID]
				WHERE ACT.[MessageID] = @MessageId AND EVNT.[UserID] = @UserId AND ACT.[IsApply] = 1)

				THEN 1
			ELSE
				0
			END
		END