﻿
		CREATE PROCEDURE [ContentPush].[DeleteChannelTag] @AppKey NVARCHAR(100), @ChannelId UNIQUEIDENTIFIER, @GroupName NVARCHAR(127), @Tag NVARCHAR(255)
		AS
		BEGIN
			DELETE FROM [ContentPush].[LKTag_Channel]
			WHERE [ChannelID] = @ChannelId AND [TagID] = (SELECT TOP(1) TagID FROM [ContentPush].[Tag] WITH(NOLOCK) WHERE [GroupName] = @GroupName AND [AppKey] = @AppKey AND [TagValue] = @Tag);
		END