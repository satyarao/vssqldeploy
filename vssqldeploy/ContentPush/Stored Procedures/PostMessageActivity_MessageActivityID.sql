﻿
	CREATE PROCEDURE [ContentPush].[PostMessageActivity_MessageActivityID] 
		@MessageActivityID uniqueidentifier, @MessageID uniqueidentifier, @PipelineActivityID uniqueidentifier, @RequestObject nvarchar(max), @ResponseObject nvarchar(max), 
		@MessageDeliveryType nvarchar(255), @OperationID uniqueidentifier, @ScheduleIDs nvarchar(max), 
		@ExperimentIDs nvarchar(max), @PushIDs nvarchar(max), @MessageIDs nvarchar(max), @ScheduleItemInfo nvarchar(max), @UrlIDs nvarchar(max), 
		@ContentUrls nvarchar(max), @IsPushed bit, @IsDeleted bit, @ErrorMessage nvarchar(255), @ErrorStackTrace nvarchar(max)
	AS
	BEGIN
		IF (NOT EXISTS(SELECT * FROM [ContentPush].[MessageActivity] WHERE [MessageActivityID] = @MessageActivityID)) AND
			(EXISTS(SELECT * FROM [ContentPush].[Message] WHERE [MessageID] = @MessageID))
		BEGIN
			INSERT INTO [ContentPush].[MessageActivity]
				([MessageActivityID],[MessageID],[PipelineActivityID],[RequestObject],[ResponseObject],[MessageDeliveryType],[OperationID],[ScheduleIDs],
				[ExperimentIDs],[PushIDs],[MessageIDs],[ScheduleItemInfo],[UrlIDs],[ContentUrls],[IsPushed],[IsDeleted],[ErrorMessage],[ErrorStackTrace])
			VALUES
				(@MessageActivityID, @MessageID, @PipelineActivityID, @RequestObject, @ResponseObject, @MessageDeliveryType, @OperationID, @ScheduleIDs, 
				@ExperimentIDs, @PushIDs, @MessageIDs, @ScheduleItemInfo, @UrlIDs, @ContentUrls, @IsPushed, @IsDeleted, @ErrorMessage, @ErrorStackTrace)
		END
	END