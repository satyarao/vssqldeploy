﻿
----------------------------------------------------------------------BUILD CHANNEL ACTIVITY PROCEDURES------------------------------------------------------------------------

CREATE PROCEDURE [ContentPush].[GetChannelActivityByChannelId]
        @ChannelId uniqueidentifier
AS
BEGIN
    SELECT [ChannelActivityID] AS ChannelActivityId,
        [AppKey],
        [ChannelID] AS ChannelId,
        [UserID] AS UserId,
        [DeviceType],
        [RegistrationType],
        [SubscriptionType],
        [AssociateNamedUserResponse],
        [RegisterationResponse],
        [CreatedOn],
        [WarningrMessage],
        [ErrorMessage],
        [ErrorStackTrace]
    FROM [ContentPush].[ChannelActivity] WHERE [ChannelID] = @ChannelId
END
