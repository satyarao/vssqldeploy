﻿
CREATE   PROCEDURE [ContentPush].[FixUsersWhoMadeAnotherTransaction]
	@UserIDString nvarchar(max),
	@TenantID uniqueidentifier
AS
BEGIN
	DECLARE @OldTxnTagID uniqueidentifier = 	(
													SELECT TagID 
													FROM [ContentPush].[Tag] 
													WHERE [TenantID] = @TenantID 
													AND TagValue= 'only_one_old_txn'
												)
	
	DECLARE @NewTxnUsers TABLE (UserID uniqueidentifier);
	
	WITH ActualOldTxnUsers AS
	(
		SELECT value AS [UserID] FROM STRING_SPLIT(@UserIDString, ',') WHERE TRY_CONVERT(uniqueidentifier, value) IS NOT NULL
	)
	
	INSERT INTO @NewTxnUsers (UserID) (
												SELECT tag.UserID
												FROM [ContentPush].[LKTag_User] tag WITH(NOLOCK)
												LEFT JOIN ActualOldTxnUsers txn		WITH(NOLOCK)
												ON tag.[UserID] = txn.[UserID]
												WHERE [TagID] = @OldTxnTagID
												  AND txn.UserID IS NULL
											)
											
	UPDATE [ContentPush].[LKTag_User]
	SET
	[IsActive]		= 0,
	[LastUpdated] 	= GETUTCDATE()
	WHERE [UserID] IN (SELECT UserID FROM @NewTxnUsers)
	  AND [TagID] = @OldTxnTagID
	  
	SELECT UserID FROM @NewTxnUsers
END
