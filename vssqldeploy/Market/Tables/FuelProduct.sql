﻿CREATE TABLE [Market].[FuelProduct] (
    [TenantID]           UNIQUEIDENTIFIER NOT NULL,
    [PCATSCode]          NVARCHAR (20)    NOT NULL,
    [DefaultDisplayName] NVARCHAR (50)    NULL,
    [DefaultDescription] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_FuelPrices] PRIMARY KEY CLUSTERED ([TenantID] ASC, [PCATSCode] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_FuelProduct_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

