﻿-- =============================================
-- Author:		Darya Batalava
-- Create date: 7/04/2018
-- Description:	Get all Time Zones
-- =============================================
CREATE PROCEDURE [DigitalOffer].[GetTimeZone]
  @TenantID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SupportedTimeZones NVARCHAR(MAX)
	SET @SupportedTimeZones = dbo.GetTenantPropertyValue(@TenantID, 'Tenant.SupportedTimeZones', DEFAULT)

	SELECT TimeZoneID, Identifier, DisplayName, BaseUtcOffsetSec
	FROM [Common].[LKTimeZone] 
	WHERE TimeZoneID in (SELECT StringValue from Common.parseJSON(@SupportedTimeZones))
END



