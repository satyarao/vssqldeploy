﻿CREATE TABLE [zDBA].[MappingCardTypes] (
    [OldCardTypeID] SMALLINT NOT NULL,
    [NewCardTypeID] SMALLINT NOT NULL,
    CONSTRAINT [FK_MappingCardTypes_LKCardType] FOREIGN KEY ([OldCardTypeID]) REFERENCES [dbo].[LKCardType] ([CardTypeID]),
    CONSTRAINT [FK_MappingCardTypes_LKPaymentProcessor] FOREIGN KEY ([NewCardTypeID]) REFERENCES [Payment].[LKPaymentProcessor] ([PaymentProcessorID])
);

