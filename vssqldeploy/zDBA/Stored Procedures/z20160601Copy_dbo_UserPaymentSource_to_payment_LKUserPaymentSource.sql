﻿

-- =============================================
-- Create date: 02/25/2016
-- Description:	Stored Procedure to migrate from [dbo].[UserPaymentSource]
-- to [Payment].[LKUserPaymentSource] and corresponding foreign tables
-- =============================================
CREATE PROCEDURE [zDBA].[z20160601Copy_dbo_UserPaymentSource_to_payment_LKUserPaymentSource]
	@AllowReseed BIT = 0
	--@Override BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM Payment.UserZiplinePaymentSource;
	DELETE FROM Payment.UserPrecidiaPaymentSource;
	DELETE FROM Payment.UserMockPaymentSource;
	DELETE FROM Payment.LKUserPaymentSource;

	DECLARE @Result TABLE 
	( From_UserPaymentSourceID INT
	, From_UserID UNIQUEIDENTIFIER
	, To_UserID UNIQUEIDENTIFIER
	, AffectedRowStatus NVARCHAR(50)
	, StatusID TINYINT
	)

	DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID INT)

	DECLARE @PaymentProcessor TABLE (CardTypeID SMALLINT, CardName NVARCHAR(255))

	DECLARE @RowNum INT, @UserPaymentSourceID INT, @To_UserID UNIQUEIDENTIFIER
	DECLARE @Index INT = 1
	DECLARE @MaxIndex INT = 0

	SELECT @UserPaymentSourceID = MIN(UserPaymentSourceID) FROM [dbo].[UserPaymentSource]
	 
	SELECT @RowNum = COUNT(*) FROM [dbo].[UserPaymentSource]

	PRINT 'Begin Migration'

	DECLARE @From_UserID UNIQUEIDENTIFIER, @PaymentProviderToken NVARCHAR(100), @FirstSix NCHAR(6), @LastFour NCHAR(4), @ExpDate CHAR(4), @IsDefault BIT, @IsActive BIT, @FrontEndProcessor NVARCHAR(64), @FundingProvider NVARCHAR(255), @CardTypeID SMALLINT
	DECLARE @CardTypeName NVARCHAR(255), @PaymentProcessorID SMALLINT, @Email_address NVARCHAR(255);



	WHILE @RowNum >= @Index                        
	BEGIN
		SET @To_UserID = NULL;
		SET @PaymentProcessorID = NULL;
		SET @CardTypeName = NULL;

		--get other info from that row
		WITH UPS AS (
			SELECT
					UserPaymentSourceID
				, ROW_NUMBER() OVER (ORDER BY UserPaymentSourceID) AS RowIndex
				, UserID
				, PaymentProviderToken
				, FirstSix
				, LastFour
				, ExpDate
				, IsDefault
				, IsActive
				, CardTypeID
				, FrontEndProcessorID
			FROM [dbo].[UserPaymentSource]
		)
		SELECT
				@UserPaymentSourceID = UPS.UserPaymentSourceID 
			, @From_UserID = UserID
			, @PaymentProviderToken = PaymentProviderToken
			, @FirstSix = FirstSix
			, @LastFour = LastFour
			, @ExpDate = ExpDate
			, @IsDefault = IsDefault
			, @IsActive = UPS.IsActive
			, @CardTypeID = UPS.CardTypeID
			, @FrontEndProcessor = FEP.FrontEndTypeName
		FROM UPS 
		LEFT JOIN [dbo].[FrontEndProcessor] FEP ON UPS.FrontEndProcessorID = FEP.FrontEndID
		WHERE RowIndex = @Index;

		SELECT 
				@To_UserID = UserID 
			, @FundingProvider = FP.FundingProviderName
		FROM [Payment].[LKUserPaymentSource] LKUPS
		LEFT JOIN [Payment].[LKPaymentProcessor] LKPP ON LKUPS.PaymentProcessorID = LKPP.PaymentProcessorID
		LEFT JOIN [Payment].[FundingProvider] FP ON LKPP.FundingProviderID = FP.FundingProviderID
		WHERE UserPaymentSourceID = @UserPaymentSourceID

		IF EXISTS(SELECT * FROM [Payment].[LKUserPaymentSource] WHERE UserPaymentSourceID = @UserPaymentSourceID AND UserID = @From_UserID)
		BEGIN
			--TODO We should report for any conflicting provider. But does the stored procedure need to update?

			--Update funding source
			IF NOT ((@FrontEndProcessor LIKE 'Mock' AND @FundingProvider LIKE 'Mock') 
				OR (@FrontEndProcessor LIKE 'Precidia' AND @FundingProvider LIKE 'Precidia')
				OR (@FrontEndProcessor LIKE 'NPCA' AND @FundingProvider LIKE 'ZipLine'))
			BEGIN
				--Conflict: Front End Processor is does not match Funding Provider
				INSERT INTO @Result
				VALUES (@UserPaymentSourceID, @From_UserID, @To_UserID, 'conflicting funding provider during update', 1)
			END
		END
		ELSE
		BEGIN
			SET IDENTITY_INSERT [Payment].[LKUserPaymentSource] ON;
			BEGIN TRY
				IF NOT EXISTS(SELECT * FROM [Payment].[LKUserPaymentSource] WHERE UserPaymentSourceID = @UserPaymentSourceID)
				BEGIN			
					--Insert new funding source
					IF @FrontEndProcessor LIKE 'Mock'
					BEGIN
						PRINT CONCAT('Insert Mock     :: ', @UserPaymentSourceID)
						SET @CardTypeName = 'Mock'
						SET @PaymentProcessorID = [Payment].[GetPaymentProcessorID](@CardTypeName)

						Insert into @ReturnedPaymentSource
						Exec [Payment].[InsertUserPaymentSource]
							@UserID = @From_UserID,
							@PaymentProcessorID = @PaymentProcessorID,
							@CardTypeName = @CardTypeName,
							@IsDefault = @IsDefault,
							@IsActive = @IsActive,
							@UserPaymentSourceID = @UserPaymentSourceID

						INSERT INTO [Payment].[UserMockPaymentSource] (UserPaymentSourceID, Authorization_State, Balance)
						VALUES(@UserPaymentSourceID, 'Approved', 0)
					END
					ELSE IF @FrontEndProcessor LIKE 'Precidia'
					BEGIN
						PRINT CONCAT('Insert Precidia :: ', @UserPaymentSourceID)

						--Check by FirstSix
						PRINT CONCAT('Check by FirstSix :: ', @UserPaymentSourceID)
						INSERT INTO @PaymentProcessor
						EXEC [Payment].[GetCCardInfo] @FirstSix, @PaymentProcessorID OUTPUT, @CardTypeName OUTPUT

						IF(@CardTypeName = 'Unknown' AND @FirstSix IS NULL AND @CardTypeID IS NOT NULL)
						BEGIN
							PRINT CONCAT('Unknown. Check by CardType ID :: ', @UserPaymentSourceID)
							--Check by CardType ID
							SELECT @PaymentProcessorID = [PaymentProcessorID],
								@CardTypeName = [PaymentProcessorName]
							FROM [Payment].[LKPrecidiaCardType] precidia
							LEFT JOIN [Payment].[LKPaymentProcessor] lk on precidia.[PrecidiaCardTypeID] = lk.[PaymentProcessorID]
							LEFT JOIN [zDBA].[MappingCardTypes] map on lk.[PaymentProcessorID] = map.[NewCardTypeID]
							WHERE [OldCardTypeID] = @CardTypeID
						END

						IF @PaymentProcessorID IS NOT NULL
						BEGIN
							PRINT CONCAT('Use @PaymentProcessorID :: ', @PaymentProcessorID)
							Insert into @ReturnedPaymentSource
							Exec [Payment].[InsertUserPaymentSource] 
								@UserID = @From_UserID,
								@PaymentProcessorID = @PaymentProcessorID,
								@CardTypeName = @CardTypeName,
								@IsDefault = @IsDefault,
								@IsActive = @IsActive,
								@UserPaymentSourceID = @UserPaymentSourceID

							INSERT INTO [Payment].[UserPrecidiaPaymentSource] (UserPaymentSourceID, PaymentProviderToken, FirstSix, LastFour, ExpDate)
							VALUES(@UserPaymentSourceID, @PaymentProviderToken, @FirstSix, @LastFour, @ExpDate)
						END
						ELSE
						BEGIN
							--Not Found: PaymentProcessorID is null because card type is not defined or FirstSix does not match to any
							INSERT INTO @Result
							VALUES (@UserPaymentSourceID, @From_UserID, @To_UserID, 'Credit Card not found', 2)
						END
					END
					ELSE IF @FrontEndProcessor LIKE 'NPCA'
					BEGIN
						PRINT CONCAT('Insert Zipline  :: ', @UserPaymentSourceID)
						SELECT @Email_address = EmailAddress FROM [dbo].[UserInfo] Where UserID = @From_UserID
						SET @CardTypeName = 'ZipLine';
						SET @PaymentProcessorID = [Payment].[GetPaymentProcessorID](@CardTypeName)

						INSERT INTO @ReturnedPaymentSource
						EXEC [Payment].[InsertUserPaymentSource] 
							@UserID = @From_UserID,
							@PaymentProcessorID = @PaymentProcessorID,
							@CardTypeName = @CardTypeName,
							@IsDefault = @IsDefault,
							@IsActive = @IsActive,
							@UserPaymentSourceID = @UserPaymentSourceID

						INSERT INTO [Payment].[UserZiplinePaymentSource] (UserPaymentSourceID, ZiplineEmailAddress)
						VALUES(@UserPaymentSourceID, @Email_address)
					END
					ELSE
					BEGIN
						--NotSupported: Front End Processor is not supported for the new Funding Provider
						INSERT INTO @Result
						VALUES (@UserPaymentSourceID, @From_UserID, @To_UserID, 'not supported funding provider during insert', 4)
					END
				END
				ELSE
				BEGIN
					--Conflict: Same userpaymentsource id with different user id
					INSERT INTO @Result
					VALUES (@UserPaymentSourceID, @From_UserID, @To_UserID, 'conflicting between users', 5)
				END
			END TRY
			BEGIN CATCH
				--Exception: Error was thrown
				INSERT INTO @Result
				VALUES (@UserPaymentSourceID, @From_UserID, @To_UserID, 'Exception occur during insert', 6)
			END CATCH
			SET IDENTITY_INSERT [Payment].[LKUserPaymentSource] OFF;
		END
		--increase count
		SET @Index = @Index + 1
	END

	IF @AllowReseed = 1 AND @RowNum > 0
	BEGIN
		SELECT @MaxIndex = MAX(UserPaymentSourceID) FROM [Payment].[LKUserPaymentSource]
		DBCC CHECKIDENT ('[Payment].[LKUserPaymentSource]', RESEED, @MaxIndex);
	END

	PRINT 'Done.'

	SELECT * FROM @ReturnedPaymentSource

	--Report Summary
	IF EXISTS(SELECT * FROM @Result)
	BEGIN
		PRINT 'There are rows that cannot be merged in. Please manually copy them over.'
		SELECT * FROM @Result
	END
END

