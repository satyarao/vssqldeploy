﻿CREATE PROCEDURE [Payment].[IUConfigChasePaymentech]
  @ConfigChasePayID INT = NULL
, @TenantID UNIQUEIDENTIFIER
, @OrbitalConnectionUsername NVARCHAR(32) = NULL
, @OrbitalConnectionPassword NVARCHAR(32) = NULL
, @BIN NVARCHAR(6) = NULL
, @MerchantID NVARCHAR(15) = NULL
, @TerminalID NVARCHAR(3) = NULL
, @ChasepayMerchantID NVARCHAR(10) = NULL
, @DeviceSignatureVersion UNIQUEIDENTIFIER = NULL
, @IsActive BIT = NULL
AS
BEGIN
MERGE [Payment].[ConfigChasePaymentech] AS target
USING
	(SELECT	@ConfigChasePayID
		, @TenantID
		, @OrbitalConnectionUsername
		, @OrbitalConnectionPassword
		, @BIN
		, @MerchantID
		, @TerminalID
		, @ChasepayMerchantID
		, @DeviceSignatureVersion
		, @IsActive)
    AS source (ConfigChasePayID
		, TenantID
		, OrbitalConnectionUsername
		, OrbitalConnectionPassword
		, BIN
		, MerchantID
		, TerminalID
		, ChasepayMerchantID
		, DeviceSignatureVersion
		, IsActive)
ON (target.TenantID = source.TenantID)
WHEN MATCHED THEN
	UPDATE SET OrbitalConnectionUsername = ISNULL(source.OrbitalConnectionUsername, target.OrbitalConnectionUsername)
			, OrbitalConnectionPassword = ISNULL(source.OrbitalConnectionPassword, target.OrbitalConnectionPassword)
			, BIN = ISNULL(source.BIN, target.BIN)
			, MerchantID = ISNULL(source.MerchantID, target.MerchantID)
			, TerminalID = ISNULL(source.TerminalID, target.TerminalID)
			, ChasepayMerchantID = ISNULL(source.ChasepayMerchantID, target.ChasepayMerchantID)
			, DeviceSignatureVersion = ISNULL(source.DeviceSignatureVersion, target.DeviceSignatureVersion)
			, IsActive = ISNULL(source.IsActive, target.IsActive)
			, UpdatedOn = GETUTCDATE()
WHEN NOT MATCHED THEN
	INSERT
           ( [TenantID]
			, [OrbitalConnectionUsername]
			, [OrbitalConnectionPassword]
			, [BIN]
			, [MerchantID]
			, [TerminalID]
			, [ChasepayMerchantID]
			, [IsActive]
            , [CreatedOn])
     VALUES
           ( @TenantID
			, @OrbitalConnectionUsername
			, @OrbitalConnectionPassword
			, @BIN
			, @MerchantID
			, @TerminalID
			, @ChasepayMerchantID
			, 1
			, GETUTCDATE());
END


