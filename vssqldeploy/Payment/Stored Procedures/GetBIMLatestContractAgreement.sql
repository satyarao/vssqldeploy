﻿-- =============================================
-- Create date: 2017-03-06
-- Description:	Get the latest metadata of the BIM's terms & conditions
-- =============================================
CREATE PROCEDURE [Payment].[GetBIMLatestContractAgreement] 
	@TenantId UNIQUEIDENTIFIER,
	@UserId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM [Payment].[ConfigBimTenantLevel] WHERE TenantId = @TenantId AND [ContractVersion] IS NOT NULL AND [ContractWebUrl] IS NOT NULL)
	BEGIN
		DECLARE @LatestVersion [nvarchar](250) = NULL;
		DECLARE @AcceptanceState BIT = 0;
		DECLARE @WebUrl NVARCHAR(2083) = NULL;

		SELECT TOP 1
			@LatestVersion = [ContractVersion],
			@WebUrl = [ContractWebUrl]
		FROM [Payment].[ConfigBimTenantLevel]
		WHERE TenantId = @TenantId

		SELECT TOP 1
			 @AcceptanceState = AcceptanceState
		FROM [Payment].[BimCustomerContractAgreement] WHERE [UserId] = @UserId AND [ContractVersion] = @LatestVersion

		SELECT
			 @AcceptanceState AS AcceptanceState
			,@LatestVersion AS ContractVersion
			,@WebUrl AS WebUrl
	END
END
