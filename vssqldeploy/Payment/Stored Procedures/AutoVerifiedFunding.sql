﻿
--AutoVerifiedFunding
CREATE PROC [Payment].[AutoVerifiedFunding]
	@UserID UNIQUEIDENTIFIER
  , @TenantID UNIQUEIDENTIFIER
  , @EmailAddress NVARCHAR(255)
AS 
BEGIN
	SET NOCOUNT ON
	IF dbo.GetTenantPropertyValue(@TenantID, 'AutoVerifiedFunding', DEFAULT) = 'NPCA'
		BEGIN
			EXEC [Payment].[InsertZiplinePaymentSource]
				@UserID = @UserID
			, @ZiplineEmailAddress = @EmailAddress
		END
	ELSE IF dbo.GetTenantPropertyValue(@TenantID, 'AutoVerifiedFunding', DEFAULT) = 'Auto'
		BEGIN
			EXEC [Payment].[InsertMockPaymentSource]
				@UserID = @UserID
			, @AuthorizationState = 'Approved'
			, @Balance = 0
		END
END
