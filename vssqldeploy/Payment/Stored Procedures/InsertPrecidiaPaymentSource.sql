﻿-- =============================================
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- =============================================
CREATE PROCEDURE [Payment].[InsertPrecidiaPaymentSource] 
	@InstallationID NVARCHAR(128) = NULL,
	@UserID UNIQUEIDENTIFIER = NULL,
	@PaymentProviderToken NVARCHAR(100),
	@FirstSix NCHAR(6),
	@LastFour NCHAR(4),
	@ExpDate NCHAR(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @CardTypeName NVARCHAR(255), @PrecidiaCardTypeID SMALLINT
	DECLARE @CardInfoTable Table (
		  CardTypeID SMALLINT
		, CardType NVARCHAR(255)
	);

	IF @FirstSix IS NOT NULL
	BEGIN
		Insert into @CardInfoTable
		EXEC [Payment].[GetCCardInfo] @CardNumber = @FirstSix
		SELECT TOP 1
			  @PrecidiaCardTypeID = CardTypeID
			, @CardTypeName = CardType
		FROM @CardInfoTable
	END

	IF @PrecidiaCardTypeID IS NOT NULL
	BEGIN
		IF @ExpDate IS NULL
		BEGIN
			--Get default expire date if issuer supports it
			SELECT @ExpDate = DefaultExpireDate FROM [Payment].[LKPrecidiaCardType] WHERE PrecidiaCardTypeID = @PrecidiaCardTypeID
		END
		
		DECLARE @Returned_UserPaymentSourceID INT = 0
		DECLARE @TenantID UNIQUEIDENTIFIER = [dbo].[GetTenantIDByUserInfo](@UserID, @InstallationID);
		IF @PrecidiaCardTypeID IN (SELECT StringValue from Common.parseJSON(dbo.GetTenantPropertyValue(@TenantID, 'Payments.Precidia.SupportedCards', DEFAULT)))
		BEGIN
			DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID int)
			Insert into @ReturnedPaymentSource
			Exec [Payment].[InsertUserPaymentSource] 
				@InstallationID = @InstallationID,
				@UserID = @UserID,
				@PaymentProcessorID = @PrecidiaCardTypeID,
				@CardTypeName = @CardTypeName
			
			SELECT @Returned_UserPaymentSourceID = UserPaymentSourceID
			FROM @ReturnedPaymentSource

			IF @Returned_UserPaymentSourceID != 0
			BEGIN
				INSERT INTO [Payment].[UserPrecidiaPaymentSource] (UserPaymentSourceID, PaymentProviderToken, FirstSix, LastFour, ExpDate)
				VALUES(
					@Returned_UserPaymentSourceID, @PaymentProviderToken, @FirstSix, @LastFour, @ExpDate)
			END
		END

		SELECT @Returned_UserPaymentSourceID AS UserPaymentSourceID
	END
	ELSE
	BEGIN
		SELECT 0 AS UserPaymentSourceID
	END
END
