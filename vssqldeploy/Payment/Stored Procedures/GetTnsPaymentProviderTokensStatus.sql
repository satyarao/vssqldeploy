﻿-- =============================================
-- Author:      Dzmitry Mikhailau 
-- Date:        03/25/2019
-- Description: PLAT-5136[Shell_US] - Support blocking TNS payment tokens from being provisioned or used for transactions
-- =============================================
CREATE   PROCEDURE [Payment].[GetTnsPaymentProviderTokensStatus]
	@PaymentProviderToken varchar(100) = null
AS
BEGIN
	SELECT 
        DISTINCT(blk.PaymentProviderToken) as PaymentProviderToken 
        ,ISNULL(blk.IsBlocked, 0) as IsBlocked
        ,blk.CreatedOn as CreatedOn
		,blk.UpdatedOn as UpdatedOn
    FROM [Payment].[LKPaymentTokenTNSBlocked] blk
	WHERE (@PaymentProviderToken IS NULL OR (blk.PaymentProviderToken = @PaymentProviderToken))
END

