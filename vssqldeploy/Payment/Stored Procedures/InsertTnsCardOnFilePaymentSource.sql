﻿
-- =============================================
-- Author:		Sergei Buday
-- Create date: 07/29/2019
-- Update date: 07/29/2019
-- Description:	Add @DeviceID to set of input params
-- =============================================
CREATE PROCEDURE [Payment].[InsertTnsCardOnFilePaymentSource]
	@UserID uniqueidentifier,
	@DeviceID nvarchar(255),
	@LS_ProcessorId INT = NULL,
	@ProviderID int,
	@PaymentTypeID INT,
	@PaymentDeckID INT,
	@UserPaymentSourceID uniqueidentifier,
	@PaymentProviderToken nvarchar(100),
	@FirstSix nvarchar(6),
	@LastFour nvarchar(4),
	@CCExpDate char(4),
	@ExpiryDate datetime2(7),
	@ZipCode nvarchar(9)
AS
BEGIN
	DECLARE @LS_UserPaymentSourceID INT
	DECLARE @ResultSet table (Result int, LS_UserPaymentSourceID int, ID uniqueidentifier)
			
	BEGIN TRAN tns_save_card_data
	BEGIN TRY
		SELECT TOP 1
			@LS_UserPaymentSourceID = lkups.[UserPaymentSourceID]
			FROM [Payment].[UserTNSCardOnFilePaymentSource] prov
			LEFT JOIN [Payment].[LKUserPaymentSource] lkups ON lkups.[UserPaymentSourceId] = prov.[UserPaymentSourceId]
			WHERE lkups.[UserId] = @UserID AND prov.[PaymentProviderToken] = @PaymentProviderToken

		IF @LS_UserPaymentSourceID IS NOT NULL
		BEGIN
			UPDATE [Payment].[LKUserPaymentSource]
			SET
				IsActive = 1,
				UpdatedOn = GETUTCDATE(),
				DeviceID = @DeviceID
			WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

			UPDATE [Payment].[UserTNSCardOnFilePaymentSource]
			SET
				ExpDate = @CCExpDate,
				ExpiryDate = @ExpiryDate,
				ZipCode = @ZipCode
			WHERE UserPaymentSourceID = @LS_UserPaymentSourceID
					
			INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID, ID) 
			VALUES(1, @LS_UserPaymentSourceID, @UserPaymentSourceID)
		END
		ELSE
		BEGIN
			IF @LS_ProcessorId IS NULL
			BEGIN
				SET @LS_ProcessorId = (SELECT TOP 1 PaymentProcessorId FROM Payment.LKPaymentProcessor WHERE FundingProviderId = @ProviderID)
			END

			INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID ) 
				EXEC [Payment].[InsertLKUserPaymentSource_LS] 
					@UserID = @UserID, 
					@PaymentSourceID = @UserPaymentSourceID, 
					@ProviderID = @LS_ProcessorId, 
					@PaymentTypeID = @PaymentTypeID, 
					@PaymentDeckID = @PaymentDeckID,
					@DeviceID = @DeviceID
			SET @LS_UserPaymentSourceID = (Select LS_UserPaymentSourceID FROM @ResultSet)

			INSERT INTO [Payment].[UserTNSCardOnFilePaymentSource] ([UserPaymentSourceId],[PaymentProviderToken],[FirstSix],[LastFour],[ExpDate],[ExpiryDate],[ZipCode])
				VALUES(@LS_UserPaymentSourceID, @PaymentProviderToken, @FirstSix, @LastFour, @CCExpDate, @ExpiryDate, @ZipCode)

			UPDATE @ResultSet
			SET ID = @UserPaymentSourceID
		END
	COMMIT TRAN tns_save_card_data

	SELECT * FROM @ResultSet
	END TRY
			
	BEGIN CATCH
		ROLLBACK TRAN tns_save_card_data

		SELECT TOP(0) * FROM @ResultSet
	END CATCH
END

