﻿
create procedure Payment.GetTokenExConfig
	@TenantId uniqueidentifier
as
begin
	select *
	from Payment.ConfigTokenExTenantLevel
	where TenantId = @TenantId
end
