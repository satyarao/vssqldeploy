﻿
CREATE PROCEDURE [Payment].[InsertVisaCheckoutPaymentSource]
	@UserID uniqueidentifier,
	@LS_PaymentProcessorId INT,
	@ProviderID int,
	@PaymentTypeID INT,
	@PaymentDeckID INT,
	@UserPaymentSourceID uniqueidentifier,
	@PaymentProviderToken nvarchar(100),
	@CallId nvarchar(128),
	@PaymentMethodType nvarchar(32),
	@FirstSix nvarchar(6),
	@LastFour nvarchar(4),
	@ExpDate datetime2(7),
	@ImageUrl nvarchar(max),
	@NickName nvarchar(64),
	@CardType nvarchar(64),
	@CardBrand nvarchar(64),
	@ZipCode nvarchar(9)
AS
BEGIN
	DECLARE @LS_UserPaymentSourceID INT		
	DECLARE @ResultSet table (Result int, LS_UserPaymentSourceID int, UserPaymentSourceID uniqueidentifier)
	
	BEGIN TRY
	    BEGIN TRAN vco_save_card_data
		SELECT TOP 1
			@LS_UserPaymentSourceID = lkups.[UserPaymentSourceID]
			FROM [Payment].[UserVisaCheckoutPaymentSource] prov
			LEFT JOIN [Payment].[LKUserPaymentSource] lkups ON lkups.[UserPaymentSourceId] = prov.[UserPaymentSourceId]
			WHERE lkups.[UserId] = @UserID AND prov.[PaymentProviderToken] = @PaymentProviderToken

		IF @LS_UserPaymentSourceID IS NOT NULL
		BEGIN
			UPDATE [Payment].[LKUserPaymentSource]
			SET
				IsActive = 1,
				UpdatedOn = GETUTCDATE()
			WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

			UPDATE [Payment].[UserVisaCheckoutPaymentSource]
			SET
				CallId = @CallId,
				PaymentMethodType = @PaymentMethodType,
				ExpiryDate = COALESCE(@ExpDate, ExpiryDate),
				ImageUrl = COALESCE(@ImageUrl, ImageUrl),
				NickName = @NickName,
				ZipCode = @ZipCode
			WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

			INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID, UserPaymentSourceID) 
			VALUES(1, @LS_UserPaymentSourceID, @UserPaymentSourceID)
		END
		ELSE
		BEGIN
			INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID ) 
				EXEC [Payment].[InsertLKUserPaymentSource_LS] 
					@UserID = @UserID, 
					@PaymentSourceID = @UserPaymentSourceID, 
					@ProviderID = @LS_PaymentProcessorId, 
					@PaymentTypeID = @PaymentTypeID, 
					@PaymentDeckID = @PaymentDeckID

			SET @LS_UserPaymentSourceID = (Select LS_UserPaymentSourceID FROM @ResultSet)
			
			INSERT INTO [Payment].[UserVisaCheckoutPaymentSource] ([UserPaymentSourceId],[PaymentProviderToken],[CallId],[PaymentMethodType],[FirstSix],[LastFour],[ExpiryDate],[ImageUrl],[NickName],[CardType],[CardBrand],[ZipCode])
				VALUES(@LS_UserPaymentSourceID, @PaymentProviderToken, @CallId, @PaymentMethodType, @FirstSix, @LastFour, @ExpDate, @ImageUrl, @NickName, @CardType, @CardBrand, @ZipCode)

			UPDATE @ResultSet
			SET UserPaymentSourceID = @UserPaymentSourceID
		END

		COMMIT TRAN vco_save_card_data
		SELECT * FROM @ResultSet
	END TRY
			
	BEGIN CATCH
		ROLLBACK TRAN vco_save_card_data

		SELECT TOP(0) * FROM @ResultSet
	END CATCH
END
