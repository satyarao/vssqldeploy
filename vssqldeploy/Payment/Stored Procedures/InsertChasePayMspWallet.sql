﻿
-- =============================================
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- =============================================
CREATE PROCEDURE [Payment].[InsertChasePayMspWallet] 
	@UserID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CardTypeName NVARCHAR(255) = 'Chase Pay'
	DECLARE @PaymentProcessorID SMALLINT = [Payment].[GetPaymentProcessorID](@CardTypeName)

	DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID int)
	Insert into @ReturnedPaymentSource
	Exec [Payment].[InsertUserPaymentSource] 
		@UserID = @UserID,
		@PaymentProcessorID = @PaymentProcessorID,
		@CardTypeName = @CardTypeName

	DECLARE @Returned_UserPaymentSourceID INT
	SELECT @Returned_UserPaymentSourceID = UserPaymentSourceID
	FROM @ReturnedPaymentSource

	SELECT @Returned_UserPaymentSourceID AS UserPaymentSourceID

END
