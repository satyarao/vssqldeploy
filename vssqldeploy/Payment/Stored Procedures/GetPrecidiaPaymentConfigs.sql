﻿CREATE PROCEDURE [Payment].[GetPrecidiaPaymentConfigs] 
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	ConfigID
		, TenantID
		, MerchantIP
		, Lane
		, BuyPassPumpLane
		, Port
		, ClientMAC
		, StoreName
		, UseTokenService
		, UseTCP
		, URI
	FROM	dbo.ConfigPRECIDIAPOSLYNX
	WHERE	IsActive = 1 AND TenantID = @TenantID

	SELECT	TenantID
		, POSTerminalID
		, Lane
	FROM	dbo.ConfigPrecidiaLaneMap
	WHERE TenantID = @TenantID;
END