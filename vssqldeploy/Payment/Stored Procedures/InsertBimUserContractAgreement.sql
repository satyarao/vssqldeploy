﻿-- =============================================
-- Create date: 2017-03-06
-- Description:	Insert/Update user's agreement with BIM's terms & conditions
-- =============================================
CREATE PROCEDURE [Payment].[InsertBimUserContractAgreement]
	@UserId UNIQUEIDENTIFIER,
	@ContractVersion NVARCHAR(250),
	@AcceptanceState BIT,
	@Timestamp datetime2(7)
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT * FROM [Payment].[BimCustomerContractAgreement] WHERE UserId = @UserId AND ContractVersion = @ContractVersion)
	BEGIN
		UPDATE [Payment].[BimCustomerContractAgreement]
		SET 
			AcceptanceState = ISNULL(@AcceptanceState, AcceptanceState),
			Timestamp = ISNULL(@Timestamp, Timestamp)
		WHERE UserId = @UserId AND ContractVersion = @ContractVersion
	END
	ELSE
	BEGIN
		INSERT INTO [Payment].[BimCustomerContractAgreement]([UserId],[ContractVersion],[AcceptanceState],[Timestamp])
		VALUES (@UserId, @ContractVersion, @AcceptanceState, @Timestamp)
	END
END
