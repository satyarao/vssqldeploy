﻿
CREATE PROC [Payment].[GetPrecidiaSupportedCards]
	@TenantID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT	
		  PrecidiaCardTypeID			as PrecidiaCardTypeID
		, LKPP.PaymentProcessorName		as CardType
		, Patterns						as Pattern
		, Precidia.Format				as Format
		, Precidia.Length				as Length
		, Expire						as Expire
		, Luhn							as Luhn
		, LKPP.ImageUrl					as ImageUrl
		, CvvLength						as CvvLength
	FROM	[Payment].[LKPrecidiaCardType] Precidia
	LEFT JOIN [Payment].[LKPaymentProcessor] LKPP ON Precidia.PrecidiaCardTypeID = LKPP.PaymentProcessorID
	WHERE  (@TenantID IS NULL AND [Precidia].[AllowSupport] = 1)
	       OR 
		   (@TenantID IS NOT NULL AND [Precidia].[PrecidiaCardTypeID] IN (SELECT StringValue from Common.parseJSON(dbo.GetTenantPropertyValue(@TenantID, 'Payments.Precidia.SupportedCards', DEFAULT))))
END

