﻿-- =============================================
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- =============================================
CREATE procedure [Payment].[InsertTokenExPaymentSource]
	@UserID uniqueidentifier = null,
	@PaymentProviderToken nvarchar(100),
	@FirstSix nchar(6),
	@LastFour nchar(4),
	@ExpDate nchar(4) = null
	
as
begin
	Set nocount on;
	declare @PaymentProcessorID smallint
	select @PaymentProcessorID = PaymentProcessorID
	from Payment.LKPaymentProcessor
	where PaymentProcessorName = 'TokenEx'

	declare @CardTypeName nvarchar(255), @TokenExCardTypeID smallint
	Declare @CardInfoTable table(
		CardTypeId smallint,
		CardType nvarchar(255)
	);
	
	if @FirstSix is not null
	begin
		insert into @CardInfoTable
		Exec Payment.GetCCardInfo @CardNumber = @FirstSix
		Select top 1
			@TokenExCardTypeID = CardTypeID,
			@CardTypeName = CardType
		From @CardInfoTable
	end
	
	if @TokenExCardTypeID is not null
	begin
		if @ExpDate is null
		begin
			Select @ExpDate = DefaultExpireDate from Payment.LKPrecidiaCardType where PrecidiaCardTypeID = @TokenExCardTypeID
		end
	end

	declare @ReturnedPaymentSource table (UserPaymentSourceID int)
	insert into @ReturnedPaymentSource
	Exec Payment.InsertUserPaymentSource
		@UserID = @UserID,
		@PaymentProcessorID = @PaymentProcessorID,
		@CardTypeName = 'TokenEx'
		
	declare @ReturnedUserPaymentSourceID int		
	select @ReturnedUserPaymentSourceID = UserPaymentSourceID
	from @ReturnedPaymentSource
			
	if @ReturnedUserPaymentSourceID != 0
	begin
		insert into Payment.UserTokenExPaymentSource (UserPaymentSourceID, PaymentProviderToken, FirstSix, LastFour, ExpDate, TokenExCardTypeID)
		values (@ReturnedUserPaymentSourceID, @PaymentProviderToken, @FirstSix, @LastFour, @ExpDate, @TokenExCardTypeID)		
		
		select @ReturnedUserPaymentSourceID as UserPaymentSourceID
	end
	else
	begin
		Select 0 as UserPaymentSourceID
	end
end
