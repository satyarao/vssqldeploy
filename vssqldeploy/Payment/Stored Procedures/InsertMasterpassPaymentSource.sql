﻿
CREATE PROCEDURE [Payment].[InsertMasterpassPaymentSource]
	@UserID uniqueidentifier,
	@LS_PaymentProcessorId INT,
	@ProviderID int,
	@PaymentTypeID INT,
	@PaymentDeckID INT,
	@UserPaymentSourceID uniqueidentifier,
	@PaymentProviderToken nvarchar(100),
	@MasterPassTransactionId nvarchar(100),
	@CCExpDate char(4),
	@ExpiryDate datetime2(7),
	@FirstSix nvarchar(6),
	@LastFour nvarchar(4),
	@ZipCode nvarchar(9),
	@WalletId nvarchar(3)
AS
BEGIN
	DECLARE @LS_UserPaymentSourceID INT		
	DECLARE @ResultSet table (Result int, LS_UserPaymentSourceID int, UserPaymentSourceID uniqueidentifier)
	
	BEGIN TRY
	    BEGIN TRAN masterpass_save_card_data
		SELECT TOP 1
			@LS_UserPaymentSourceID = lkups.[UserPaymentSourceID]
			FROM [Payment].[UserMasterPassPaymentSource] prov
			LEFT JOIN [Payment].[LKUserPaymentSource] lkups ON lkups.[UserPaymentSourceId] = prov.[UserPaymentSourceId]
			WHERE lkups.[UserId] = @UserID AND prov.[PaymentProviderToken] = @PaymentProviderToken

		IF @LS_UserPaymentSourceID IS NOT NULL
		BEGIN
			UPDATE [Payment].[LKUserPaymentSource]
			SET
				IsActive = 1,
				UpdatedOn = GETUTCDATE()
			WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

			UPDATE [Payment].[UserMasterPassPaymentSource]
			SET
				MTID = @MasterPassTransactionId,
				ExpDate = @CCExpDate,
				ExpiryDate = @ExpiryDate,
				ZipCode = @ZipCode,
				WalletId = @WalletId
			WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

			INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID, UserPaymentSourceID) 
			VALUES(1, @LS_UserPaymentSourceID, @UserPaymentSourceID)
		END
		ELSE
		BEGIN
			INSERT INTO @ResultSet (Result, LS_UserPaymentSourceID ) 
				EXEC [Payment].[InsertLKUserPaymentSource_LS] 
					@UserID = @UserID, 
					@PaymentSourceID = @UserPaymentSourceID, 
					@ProviderID = @LS_PaymentProcessorId, 
					@PaymentTypeID = @PaymentTypeID, 
					@PaymentDeckID = @PaymentDeckID

			SET @LS_UserPaymentSourceID = (Select LS_UserPaymentSourceID FROM @ResultSet)
			
			INSERT INTO [Payment].[UserMasterPassPaymentSource] ([UserPaymentSourceId],[PaymentProviderToken],[MTID],[ExpDate],[ExpiryDate],[FirstSix],[LastFour],[ZipCode],[WalletId])
				VALUES(@LS_UserPaymentSourceID, @PaymentProviderToken, @MasterPassTransactionId, @CCExpDate, @ExpiryDate, @FirstSix, @LastFour, @ZipCode, @WalletId)

			UPDATE @ResultSet
			SET UserPaymentSourceID = @UserPaymentSourceID
		END

	COMMIT TRAN masterpass_save_card_data

	SELECT * FROM @ResultSet
	END TRY
			
	BEGIN CATCH
		ROLLBACK TRAN masterpass_save_card_data

		SELECT TOP(0) * FROM @ResultSet
	END CATCH
END
