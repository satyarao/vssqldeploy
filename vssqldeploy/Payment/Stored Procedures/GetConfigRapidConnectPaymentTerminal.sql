﻿
CREATE PROCEDURE [Payment].[GetConfigRapidConnectPaymentTerminal] 
	@StoreId UNIQUEIDENTIFIER
AS
BEGIN
	SELECT	
		[StoreId]
		,[TransactionOrigin] 
		,[MerchantCategoryCode] 
		,[TerminalCategoryCode] 
		,[CardCaptureCapability] 
		,[PartialAuthApprovalCapability]
	FROM [Payment].[ConfigRapidConnectPaymentTerminal]
	WHERE StoreId = @StoreId
END
