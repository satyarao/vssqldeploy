﻿
CREATE PROCEDURE [Payment].[GetUserPayPalPaymentSources]
	@UserId uniqueidentifier,
	@DeviceId nvarchar(256)
AS
	BEGIN
		SELECT TOP 1
			lkups.[LS_UserPaymentSourceID]	as LS_UserPaymentSourceID,  
			ups.[ID]						as ID,  
			ups.[CustomerID]				as CustomerId,
			ups.[BillingAgreementID]		as BillingAgreementId, 
			ups.[Email]						as Email,
			ups.[DeviceID]					as DeviceId, 
			ups.[ImageUrl]					as ImageUrl,
			ls_ups.[Nickname]				as NickName
        FROM [PaymentsEx].[UserPayPalPaymentSource] ups
		LEFT JOIN [PaymentsEx].[LKUserPaymentSource] lkups ON lkups.[ID] = ups.[ID]
		LEFT JOIN [Payment].[LKUserPaymentSource] ls_ups ON lkups.[LS_UserPaymentSourceID] = ls_ups.UserPaymentSourceId
        WHERE lkups.[UserID] = @UserId 
		AND lkups.[IsActive] = 1
		AND ups.[DeviceID] = @DeviceId
		ORDER BY lkups.CreatedOn desc
	END
