﻿
-- =============================================
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- =============================================
CREATE PROCEDURE [Payment].[UpdateUserPaymentSource] 
	@UserPaymentSourceID INT,
	@InstallationID NVARCHAR(128) = NULL,
	@UserID UNIQUEIDENTIFIER = NULL,
	@IsActive BIT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @UserID IS NULL AND @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationID)
	END

	IF @UserID IS NOT NULL
	BEGIN
		UPDATE [Payment].[LKUserPaymentSource]
		SET IsActive = ISNULL(@IsActive, IsActive), 
		UpdatedOn = GETUTCDATE()
		WHERE UserPaymentSourceID = @UserPaymentSourceID AND UserID = @UserID
	END
END
