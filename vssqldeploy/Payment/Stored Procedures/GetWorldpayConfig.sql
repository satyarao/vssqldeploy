﻿
CREATE PROCEDURE [Payment].[GetWorldpayConfig]
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		[TenantId], 
		[MerchantCode], 
		[Username], 
		[Password], 
		[InstallationId],
		[PaymentUri]
	FROM [Payment].[ConfigWorldpayTenantLevel]
	WHERE TenantId = @TenantId
END

