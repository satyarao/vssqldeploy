﻿-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 01/31/2017
-- Description:	Get funding providers and payment processors
-- =============================================
CREATE PROCEDURE [Payment].[GetPaymentProvidersAndPrcessors]
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @TempTable table( FundingProviderID SMALLINT
		, FundingProviderName NVARCHAR(25)
		, PaymentProcessors NVARCHAR(MAX) )

	DECLARE @SupportedProviders NVARCHAR(MAX)
	SET @SupportedProviders = ISNULL(dbo.GetTenantPropertyValue(@TenantID, 'Payments.FundingProvider', DEFAULT), '[]')
	
	IF(@SupportedProviders != '[]' OR @TenantID IS NULL)
	BEGIN
		INSERT INTO @TempTable (FundingProviderID, FundingProviderName)
		SELECT	FundingProviderID,
				DisplayName AS Name
		FROM	[Payment].[FundingProvider] 
		WHERE IsActive = 1 AND ( @TenantID IS NULL OR ( FundingProviderID IN (SELECT StringValue from Common.parseJSON(@SupportedProviders)) ) )

		UPDATE tt
		SET PaymentProcessors = (SELECT PaymentProcessorID AS PaymentProcessorId
										, PaymentProcessorName
										, ImageUrl
										, CardType
										, CardIssuer  
								FROM Payment.LKPaymentProcessor pp
								WHERE pp.FundingProviderID = tt.FundingProviderID
								AND ((pp.FundingProviderID > 1) OR (pp.FundingProviderID = 1 AND pp.PaymentProcessorId in (SELECT StringValue from Common.parseJSON(dbo.GetTenantPropertyValue(@TenantID, 'Payments.Precidia.SupportedCards', DEFAULT))))) FOR JSON PATH)
		FROM @TempTable AS tt
		SELECT * FROM @TempTable

	END
	ELSE
	BEGIN
		SELECT TOP(0) NULL
	END
END
