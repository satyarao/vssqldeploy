﻿
------------------------------------------------------------------------------------
CREATE   PROCEDURE [Payment].[InsertLoyaltyOnlyPaymentSource]
                @UserID UNIQUEIDENTIFIER
AS
BEGIN
                SET NOCOUNT ON;
                
                DECLARE @CardTypeName NVARCHAR(255) = 'Loyalty Only'
                DECLARE @PaymentProcessorID SMALLINT = [Payment].[GetPaymentProcessorID](@CardTypeName)
                
                DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID int)
                Insert into @ReturnedPaymentSource
                Exec [Payment].[InsertUserPaymentSource] 
                                @UserID = @UserID,
                                @PaymentProcessorID = @PaymentProcessorID,
                                @CardTypeName = @CardTypeName

                DECLARE @Returned_UserPaymentSourceID INT
                SELECT @Returned_UserPaymentSourceID = UserPaymentSourceID
                FROM @ReturnedPaymentSource

                IF @Returned_UserPaymentSourceID != 0
                BEGIN
                                INSERT INTO [Payment].[UserLoyaltyOnlyPaymentSource]
                                                                   ([UserPaymentSourceID]
                                                                   ,[Balance])
                                                VALUES
                                                                   (@Returned_UserPaymentSourceID
                                                                   ,0)
                END
                SELECT @Returned_UserPaymentSourceID AS UserPaymentSourceID
END
