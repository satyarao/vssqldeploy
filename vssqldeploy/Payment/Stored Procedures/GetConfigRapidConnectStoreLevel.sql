﻿
CREATE PROCEDURE [Payment].[GetConfigRapidConnectStoreLevel] 
	@StoreId UNIQUEIDENTIFIER
AS
BEGIN
	SELECT	
		[StoreId]
		,[GroupId]
		,[MerchantId]
		,[TerminalId]
		,[CreatedOn]
		,[UpdatedOn]
		,[TppId]
	FROM [Payment].[ConfigRapidConnectStoreLevel]
	WHERE StoreId = @StoreId
END
