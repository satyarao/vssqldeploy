﻿
-- =============================================
-- Create date: 2016-03-24
-- Update date: 2017-11-30 - Andrei Ramanovich - Add CardType, CardIssuer, FirstSix, LastFour, Expire
-- Description:	Update User Mock Payment Data 
-- =============================================
CREATE PROCEDURE [Payment].[UpdateMockPaymentSource]
	@UserID UNIQUEIDENTIFIER,
	@UserPaymentSourceID INT,
	@AuthorizationState NVARCHAR(20) = NULL,
	@Balance MONEY = NULL,
	@CardType nvarchar(50) = NULL,
	@CardIssuer nvarchar(50) = NULL,
	@FirstSix nvarchar(6) = NULL,
	@LastFour nvarchar(4) = NULL,
	@Expire nvarchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE mock
		SET 
			[Authorization_State] = ISNULL(@AuthorizationState, Authorization_State), 
			[Balance] = ISNULL(@Balance, Balance),
			[CardType]= ISNULL(@CardType, CardType),
			[CardIssuer]= ISNULL(@CardIssuer, CardIssuer),
			[FirstSix]= ISNULL(@FirstSix, FirstSix),
			[LastFour]= ISNULL(@LastFour, LastFour),
			[Expire]= ISNULL(@Expire, Expire)
		FROM [Payment].[UserMockPaymentSource] mock
		INNER JOIN [Payment].[LKUserPaymentSource] lk on lk.UserPaymentSourceID = mock.UserPaymentSourceID
		WHERE mock.UserPaymentSourceID = @UserPaymentSourceID AND lk.UserID = @UserID
END

