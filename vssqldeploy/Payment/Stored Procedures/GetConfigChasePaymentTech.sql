﻿CREATE PROCEDURE [Payment].[GetConfigChasePaymentTech] 
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	[ConfigChasePayID]
		,[TenantID]
		,[OrbitalConnectionUsername]
		,[OrbitalConnectionPassword]
		,[BIN]
		,[MerchantID]
		,[TerminalID]
		,[ChasepayMerchantID]
		,[IsActive]
	FROM [Payment].[ConfigChasePaymentTech]
	WHERE IsActive = 1 AND TenantID = @TenantID
END
