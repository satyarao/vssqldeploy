﻿CREATE PROC [Payment].[GetFundingProviderSupport]
	@TenantID UNIQUEIDENTIFIER,
	@StoreID UNIQUEIDENTIFIER = NULL
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @OverallProviders NVARCHAR(MAX);
SET @OverallProviders = ISNULL(dbo.GetTenantPropertyValue(@TenantID, 'Payments.FundingProvider', DEFAULT), '[]');

IF EXISTS(SELECT * FROM [dbo].[TenantProperty] WHERE [TenantID] = @StoreID AND [PropertyName] = 'Payments.FundingProvider')
BEGIN
	DECLARE @SupportedProviders NVARCHAR(MAX);
	SET @SupportedProviders = ISNULL(dbo.GetTenantPropertyValue(@StoreID, 'Payments.FundingProvider', DEFAULT), '[]');
	WITH 
		Support AS
	( 
		SELECT
			FundingProviderID,
			FundingProviderName
		FROM [Payment].[FundingProvider] 
		WHERE IsActive = 1 AND FundingProviderID IN (SELECT StringValue from Common.parseJSON(@SupportedProviders))
	),
		NonSupport AS
	(
		SELECT 
			FundingProviderID,
			FundingProviderName
		FROM [Payment].[FundingProvider] 
		WHERE IsActive = 1 AND FundingProviderID NOT IN (SELECT StringValue from Common.parseJSON(@SupportedProviders))
		AND FundingProviderID IN (SELECT StringValue from Common.parseJSON(@OverallProviders))
	)
	SELECT	FundingProviderID,
			FundingProviderName AS Name,
			CAST(1 AS bit) AS IsSupportedPayment
	FROM	Support
	UNION
	SELECT	FundingProviderID,
			FundingProviderName AS Name,
			CAST(0 AS bit) AS IsSupportedPayment
	FROM	NonSupport
END
ELSE
BEGIN
	SELECT	FundingProviderID,
			FundingProviderName AS Name,
			CAST(1 AS bit) AS IsSupportedPayment
	FROM	[Payment].[FundingProvider] 
	WHERE IsActive = 1 
			AND FundingProviderID IN (SELECT StringValue from Common.parseJSON(@OverallProviders))
END
