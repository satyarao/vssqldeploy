﻿
------------------------------------------------------------------------------


-- =============================================
-- Create date: 2019-04-18
-- Description:    Update User Loyalty Only Payment Data 
-- =============================================
CREATE   PROCEDURE [Payment].[UpdateLoyaltyOnlyPaymentSource]
                @UserID UNIQUEIDENTIFIER,
                @UserPaymentSourceID INT,
                @Balance MONEY = NULL
AS
BEGIN
                SET NOCOUNT ON;

                UPDATE ps
                                SET 
                                                [Balance] = ISNULL(@Balance, Balance)
                                FROM [Payment].[UserLoyaltyOnlyPaymentSource] ps
                                INNER JOIN [Payment].[LKUserPaymentSource] lk on lk.UserPaymentSourceID = ps.UserPaymentSourceID
                                WHERE ps.UserPaymentSourceID = @UserPaymentSourceID AND lk.UserID = @UserID
END


