﻿
-- =============================================
-- Create date: 09/30/2016
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- Description:	HACK: Insert new External Wallet as User Payment Source Id
-- For BasketPayments
-- =============================================
CREATE PROCEDURE [Payment].[InsertExternalWallet] 
	@UserID UNIQUEIDENTIFIER,
	@FundingProviderName NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @PaymentProcessorID SMALLINT;
	DECLARE @DisplayName NVARCHAR(255);

	SELECT @PaymentProcessorID = lkpp.PaymentProcessorID,
		   @DisplayName = fp.DisplayName
	FROM [Payment].[FundingProvider] fp
	LEFT JOIN [Payment].[LKPaymentProcessor] lkpp ON fp.FundingProviderId = lkpp.FundingProviderId 
	where fp.FundingProviderName = @FundingProviderName

	DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID int)
	Insert into @ReturnedPaymentSource
	Exec [Payment].[InsertUserPaymentSource] 
		@UserID = @UserID,
		@PaymentProcessorID = @PaymentProcessorID,
		@CardTypeName = @DisplayName,
		@IsActive = 0 --Hide User Payment Source by deactivating it

	DECLARE @Returned_UserPaymentSourceID INT
	SELECT @Returned_UserPaymentSourceID = UserPaymentSourceID
	FROM @ReturnedPaymentSource

	SELECT @Returned_UserPaymentSourceID AS UserPaymentSourceID

END
