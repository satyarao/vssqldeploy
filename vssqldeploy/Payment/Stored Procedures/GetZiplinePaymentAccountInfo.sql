﻿-- =============================================
-- Create date: 2016/03/04
-- Description:	Get Zipline Info by user payment id
-- =============================================
CREATE PROCEDURE [Payment].[GetZiplinePaymentAccountInfo]
	@InstallationID NVARCHAR(128) = NULL,
	@UserID UNIQUEIDENTIFIER = NULL,
	@UserPaymentSourceID INT
AS
BEGIN
	SET NOCOUNT ON;
	IF @UserID IS NULL AND @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationID)
	END
	
	IF @UserID IS NOT NULL
	BEGIN
		SELECT
			uzps.[ZiplineEmailAddress]
		FROM [Payment].[UserZiplinePaymentSource] uzps
		LEFT JOIN [Payment].[LKUserPaymentSource] LKups on uzps.[UserPaymentSourceID] = LKups.[UserPaymentSourceID]
		WHERE uzps.UserPaymentSourceID = @UserPaymentSourceID AND LKups.IsActive = 1
	END
END
