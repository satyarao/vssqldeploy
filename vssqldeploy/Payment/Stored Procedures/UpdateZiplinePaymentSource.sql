﻿CREATE PROCEDURE [Payment].[UpdateZiplinePaymentSource] 
	@InstallationID NVARCHAR(128) = NULL,
	@UserID UNIQUEIDENTIFIER = NULL,
	@UserPaymentSourceID INT,
	@PINCode NVARCHAR(4)
AS
BEGIN
	SET NOCOUNT ON;

	IF @UserID IS NULL AND @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationID)
	END

	IF @UserID IS NOT NULL
	BEGIN
		UPDATE  zip
		SET 
		PINCode = ISNULL(@PINCode, PINCode), 
		zip.UpdatedOn = GETUTCDATE()
		FROM [Payment].[UserZiplinePaymentSource] zip
		INNER JOIN [Payment].[LKUserPaymentSource] lk on
        lk.UserPaymentSourceID = zip.UserPaymentSourceID
		WHERE zip.UserPaymentSourceID = @UserPaymentSourceID AND lk.UserID = @UserID
	END

END
