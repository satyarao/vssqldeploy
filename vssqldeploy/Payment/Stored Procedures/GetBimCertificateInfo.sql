﻿
CREATE PROCEDURE [Payment].[GetBimCertificateInfo] 
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	 
		 [TenantId]
		,[EnrollmentThumbprint]
		,[PaymentThumbprint]
	FROM [Payment].[ConfigBimTenantLevel]
	WHERE TenantID = @TenantID
END
