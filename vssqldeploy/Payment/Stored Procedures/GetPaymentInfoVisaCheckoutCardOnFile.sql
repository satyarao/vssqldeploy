﻿
CREATE PROC [Payment].[GetPaymentInfoVisaCheckoutCardOnFile]
	@UserId UNIQUEIDENTIFIER,
	@UserPaymentSourceId INT
AS
BEGIN
	SELECT
        pp.[UserPaymentSourceId]
        ,pp.[PaymentProviderToken]
		,pp.[CallId]
		,pp.[ExpiryDate]
		,pp.[FirstSix]
		,pp.[LastFour]
        ,pp.[CardType]
        ,pp.[CardBrand]
		,pp.[ZipCode]
    FROM [Payment].[UserVisaCheckoutPaymentSource] pp
    LEFT JOIN [Payment].[LKUserPaymentSource] ups ON ups.UserPaymentSourceId = pp.UserPaymentSourceId
    WHERE ups.UserPaymentSourceId = @UserPaymentSourceId AND ups.UserId = @UserId AND ups.IsActive = 1
END
