﻿
-- =============================================
-- Author:      Dzmitry Mikhailau 
-- Date:        02/22/2019
-- Description: PLAT-5136[Shell_US] - Support blocking TNS payment tokens from being provisioned or used for transactions
-- =============================================
CREATE   PROCEDURE [Payment].[BlockTnsPaymentProviderToken]
	@PaymentProviderToken varchar(100),
	@IsBlocked bit
AS
BEGIN
	IF NOT EXISTS(SELECT * FROM [Payment].[LKPaymentTokenTNSBlocked] WHERE [PaymentProviderToken] = @PaymentProviderToken)
		BEGIN
			INSERT INTO [Payment].[LKPaymentTokenTNSBlocked]
				([PaymentProviderToken],[IsBlocked])
			VALUES
				(@PaymentProviderToken, @IsBlocked)
		END
	ELSE
		BEGIN
			UPDATE [Payment].[LKPaymentTokenTNSBlocked]
			SET IsBlocked = @IsBlocked, UpdatedOn = GETUTCDATE()
			WHERE [PaymentProviderToken] = @PaymentProviderToken
		END;
			
	IF(@IsBlocked = 1)
		BEGIN
			UPDATE [Payment].[LKUserPaymentSource]
			SET IsActive = 0, UpdatedOn = GETUTCDATE()
			WHERE IsActive = 1
			AND (UserPaymentSourceID IN (
				SELECT UserPaymentSourceID
				FROM [Payment].[UserTNSCardOnFilePaymentSource]
				WHERE PaymentProviderToken = @PaymentProviderToken)
			OR UserPaymentSourceID IN (
				SELECT UserPaymentSourceID
				FROM [Payment].[UserMasterPassPaymentSource]
				WHERE PaymentProviderToken = @PaymentProviderToken)
			OR UserPaymentSourceID IN (
				SELECT UserPaymentSourceID
				FROM [Payment].[UserVisaCheckoutPaymentSource]
				WHERE PaymentProviderToken = @PaymentProviderToken))
		END
END

