﻿
CREATE PROCEDURE [Payment].[GetBimNetworksTechLevelConfig]
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	SELECT	TOP 1
			[TenantId],
			[BaseUri],
			[PassKey],
			[PassPhrase]
	FROM	[Payment].[ConfigBimNetworksTechLevel]
	WHERE	[TenantID] = @TenantID
END
