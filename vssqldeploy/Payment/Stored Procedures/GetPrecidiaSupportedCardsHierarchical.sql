﻿-- =============================================
-- Author:		Alex Goroshko
-- Create date: 4/13/2016
-- Description:	Returns (Store or Tenant) and ParentTenant Precidia Supported Cards
-- =============================================
CREATE PROCEDURE [Payment].[GetPrecidiaSupportedCardsHierarchical]
	@TargetID UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @TargetPrecidiaSupportedCards NVARCHAR(250)
	DECLARE @ParentTenantPrecidiaSupportedCards NVARCHAR(250)
	DECLARE @ParentTenantID NVARCHAR(250)
	DECLARE @ParentTenantName NVARCHAR(250)
	
	SELECT @ParentTenantPrecidiaSupportedCards = DefaultPropertyValue,
		   @ParentTenantID = DefaultTenantID,
		   @ParentTenantName = DefaultTenantName,
		   @TargetPrecidiaSupportedCards = PropertyValue
	FROM dbo.GetTenantPropertyInfo (@TargetID, 'Payments.Precidia.SupportedCards', DEFAULT)
	
	--check if @TenantPrecidiaSupportedCards is null or empty
	IF (ISNULL(@TargetPrecidiaSupportedCards, '') != '')
		BEGIN
			SELECT	
				  PrecidiaCardTypeID			as CardTypeID
				, LKPP.PaymentProcessorName		as CardType
				, LKPP.ImageUrl					as ImageUrl
			FROM	[Payment].[LKPrecidiaCardType] precidia
			JOIN [Payment].[LKPaymentProcessor] LKPP ON Precidia.PrecidiaCardTypeID = LKPP.PaymentProcessorID
			WHERE	[PrecidiaCardTypeID] in (SELECT StringValue from Common.parseJSON(@TargetPrecidiaSupportedCards))
		END
	ELSE 
		BEGIN
			SELECT TOP(0) NULL
		END
	
	--check if @TenantPrecidiaSupportedCards is null or empty
	IF (ISNULL(@ParentTenantPrecidiaSupportedCards, '') != '')
		BEGIN
		SELECT	
				  @ParentTenantID AS ParentTenantID,
				  @ParentTenantName AS ParentTenantName,
				  PrecidiaCardTypeID			as CardTypeID
				, LKPP.PaymentProcessorName		as CardType
				, LKPP.ImageUrl					as ImageUrl
			FROM	[Payment].[LKPrecidiaCardType] Precidia
			JOIN [Payment].[LKPaymentProcessor] LKPP ON Precidia.PrecidiaCardTypeID = LKPP.PaymentProcessorID
			WHERE	[PrecidiaCardTypeID] in (SELECT StringValue from Common.parseJSON(@ParentTenantPrecidiaSupportedCards))
		END
	ELSE 
		BEGIN 
			SELECT TOP(0) NULL
		END
END
