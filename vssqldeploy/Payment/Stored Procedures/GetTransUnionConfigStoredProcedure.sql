﻿
-- =============================================
-- Author:      Matthew Liang
-- Create date: 2017-04-12
-- Update date: 2017-04-21 Andrei Ramanovich - add [CreditAccountRegistrationUrl], [GetRegisteredCardUrl]
-- Description:	Get TransUnion configurations
-- =============================================
CREATE PROCEDURE [Payment].[GetTransUnionConfigStoredProcedure] 
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		[TenantId],
		[BasicAuthUserName],
		[BasicAuthPassword],
		[CreditAccountRegistrationUrl],
		[GetRegisteredCardUrl]
	FROM [Payment].[TransUnionConfig]
	WHERE TenantId = @TenantId
END

