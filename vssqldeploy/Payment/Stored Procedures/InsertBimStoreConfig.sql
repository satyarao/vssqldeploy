﻿
CREATE PROCEDURE [Payment].[InsertBimStoreConfig] 
	@TenantId UNIQUEIDENTIFIER,
	@StoreId UNIQUEIDENTIFIER,
	@MerchantId NVARCHAR(128),
	@OutsideTerminalId NVARCHAR(128),
	@InsideTerminalId NVARCHAR(128)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TerminalPayload NVARCHAR(max) 
	= JSON_QUERY(
	(SELECT 
			@OutsideTerminalId as OutsideTerminalId, 
			@InsideTerminalId as InsideTerminalId
	FOR JSON PATH), '$[0]');

	IF EXISTS(SELECT * FROM [Payment].[ConfigBimStoreLevel] WHERE TenantId = @TenantId and StoreId = @StoreId)
	BEGIN
		UPDATE [Payment].[ConfigBimStoreLevel]
		SET 
			MerchantId = ISNULL(@MerchantId, MerchantId),
			TerminalPayload = ISNULL(@TerminalPayload, TerminalPayload),
			UpdatedOn = GETUTCDate()
		WHERE TenantId = @TenantId and StoreId = @StoreId
	END
	ELSE
	BEGIN
		INSERT INTO [Payment].[ConfigBimStoreLevel]([TenantId],[StoreId],[MerchantId],[TerminalPayload])
		VALUES (@TenantId, @StoreId, @MerchantId, @TerminalPayload)
	END
END
