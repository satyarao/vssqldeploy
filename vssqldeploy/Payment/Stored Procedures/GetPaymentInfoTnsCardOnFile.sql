﻿
CREATE PROC [Payment].[GetPaymentInfoTnsCardOnFile]
	@UserId UNIQUEIDENTIFIER,
	@PaymentSourceNumId INT
AS
BEGIN
	SELECT 
        prov.UserPaymentSourceId	as UserPaymentSourceId
        ,prov.PaymentProviderToken	as PaymentProviderToken
        ,prov.FirstSix				as FirstSix
        ,prov.LastFour				as LastFour
        ,prov.ExpDate				as CCExpDate
        ,prov.ExpiryDate			as ExpiryDate
        ,prov.ZipCode				as ZipCode
    FROM [Payment].[UserTNSCardOnFilePaymentSource] prov
    LEFT JOIN [Payment].[LKUserPaymentSource] LK ON prov.UserPaymentSourceId = LK.UserPaymentSourceId
    WHERE LK.UserId = @UserId and prov.UserPaymentSourceId = @PaymentSourceNumId and LK.IsActive = 1
END
