﻿-- =============================================
-- Create date: 2017-04-16
-- Description:	Get the latest metadata of user's state with the terms & conditions
-- =============================================
CREATE PROCEDURE [Payment].[GetLatestCustomerEnrollmentContractAgreement]
	@TenantId UNIQUEIDENTIFIER,
	@UserId UNIQUEIDENTIFIER,
	@EnrollmentArea smallint
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM [Payment].[LKTenantEnrollmentContract] 
		WHERE [TenantId] = @TenantId 
		AND [EnrollmentArea] = @EnrollmentArea 
		AND [ContractVersion] IS NOT NULL AND [ContractWebUrl] IS NOT NULL)
	BEGIN
		DECLARE @LatestVersion [nvarchar](250) = NULL;
		DECLARE @AcceptanceState BIT = 0;
		DECLARE @WebUrl NVARCHAR(2083) = NULL;

		SELECT TOP 1
			@LatestVersion = [ContractVersion],
			@WebUrl = [ContractWebUrl]
		FROM [Payment].[LKTenantEnrollmentContract] 
		WHERE TenantId = @TenantId AND EnrollmentArea = @EnrollmentArea 

		SELECT TOP 1
			 @AcceptanceState = AcceptanceState
		FROM [Payment].[CustomerEnrollmentContractAgreement] 
		WHERE [UserId] = @UserId 
		AND [ContractVersion] = @LatestVersion
		AND [EnrollmentArea] = @EnrollmentArea

		SELECT
			 @AcceptanceState AS AcceptanceState
			,@LatestVersion AS ContractVersion
			,@WebUrl AS WebUrl
	END
END
