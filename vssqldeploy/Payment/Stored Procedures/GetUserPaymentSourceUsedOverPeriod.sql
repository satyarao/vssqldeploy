﻿
-- =============================================
-- Author:      Andrei Ramanovich
-- Create date: 15/09/2017
-- Description: Get user payments count
-- =============================================
CREATE PROC [Payment].[GetUserPaymentSourceUsedOverPeriod]
	 @UserID UNIQUEIDENTIFIER
	,@FromDate DATETIME2(7) = NULL
	,@ToDate DATETIME2(7) = NULL
AS
BEGIN
SET NOCOUNT ON;

		SELECT LK.UserPaymentSourceID, COUNT(bp.[CreatedOn]) AS PaymentsCount
		FROM [Payment].[LKUserPaymentSource] LK
		JOIN dbo.UserInfo ui ON ui.UserID = @UserID
		JOIN [dbo].[BasketPayment] bp ON bp.[UserPaymentSourceID] = LK.UserPaymentSourceID AND bp.[PaymentCommand] = 'Finalize' AND bp.[ResponseResult] = 'Approved'
		WHERE LK.UserID = @UserID And LK.IsActive = 1 
			AND (@FromDate IS NULL OR @FromDate <= bp.[CreatedOn])
			AND (@ToDate IS NULL OR @ToDate >= bp.[CreatedOn])
		GROUP BY LK.UserPaymentSourceID

END
