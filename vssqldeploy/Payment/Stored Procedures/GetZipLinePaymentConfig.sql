﻿CREATE PROCEDURE [Payment].[GetZipLinePaymentConfig]
	@TenantID UNIQUEIDENTIFIER
  , @APIVersion NVARCHAR(10) = NULL
AS --EXEC Payment.GetZipLinePaymentConfig @TenantID = '00000000-0000-0000-0000-000000000000', @APIVersion = 1
SET NOCOUNT ON

IF (EXISTS(
		SELECT 1
		FROM [dbo].[ConfigNPCA]
		WHERE TenantID = @TenantID
	))
BEGIN
	SELECT	TenantID
			, ConfigID
			, MerchantID
			, StoreName
			, StoreID
			, ISNULL(APIVersion, 1) as APIVersion
			, ISNULL(URI, 'https://mobile.paymentcard.com/MPayAutoActivate/AuthService/AuthTypeBasic') as URI
			, ISNULL(AuthUserName, 'P97User1') as AuthUserName
			, ISNULL(AuthPassword, '46NU|O/?*=-@w]Bt''P') as AuthPassword
			, IsActive
	FROM	dbo.ConfigNPCA
	WHERE	APIVersion = ISNULL(@APIVersion, APIVersion)
			AND IsActive = 1
			AND TenantID = @TenantID
END
ELSE
BEGIN
	SELECT	@TenantID as TenantID
			, 0 as ConfigID
			, null as MerchantID
			, null as StoreName
			, null as StoreID
			, '1' as APIVersion
			, 'https://mobile.paymentcard.com/MPayAutoActivate/AuthService/AuthTypeBasic' as URI
			, 'P97User1' as AuthUserName
			, '46NU|O/?*=-@w]Bt''P' as AuthPassword
			, CAST(0 AS BIT) as IsActive
END
