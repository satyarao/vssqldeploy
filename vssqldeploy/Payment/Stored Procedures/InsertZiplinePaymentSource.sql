﻿-- =============================================
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- =============================================
CREATE PROCEDURE [Payment].[InsertZiplinePaymentSource] 
	@InstallationID NVARCHAR(128) = NULL,
	@UserID UNIQUEIDENTIFIER = NULL,
	@ZiplineEmailAddress NVARCHAR(255),
	@PINCode NVARCHAR(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CardTypeName NVARCHAR(255) = 'ZipLine';
	DECLARE @PaymentProcessorID TINYINT = [Payment].[GetPaymentProcessorID](@CardTypeName)

	DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID int)
	Insert into @ReturnedPaymentSource
	Exec [Payment].[InsertUserPaymentSource] 
		@InstallationID = @InstallationID,
		@UserID = @UserID,
		@PaymentProcessorID = @PaymentProcessorID,
		@CardTypeName = @CardTypeName

	DECLARE @Returned_UserPaymentSourceID INT = 0
	SELECT @Returned_UserPaymentSourceID = UserPaymentSourceID
	FROM @ReturnedPaymentSource

	IF @Returned_UserPaymentSourceID != 0
	BEGIN
		INSERT INTO [Payment].[UserZiplinePaymentSource] (UserPaymentSourceID, ZiplineEmailAddress, PINCode)
		VALUES(@Returned_UserPaymentSourceID, @ZiplineEmailAddress, @PINCode)
	END

	SELECT @Returned_UserPaymentSourceID AS UserPaymentSourceID

END
