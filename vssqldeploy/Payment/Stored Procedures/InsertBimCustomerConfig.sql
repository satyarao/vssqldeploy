﻿
CREATE PROCEDURE [Payment].[InsertBimCustomerConfig] 
	@TenantId UNIQUEIDENTIFIER,
	@UserId UNIQUEIDENTIFIER,
	@CustomerApiKey NVARCHAR(128),
	@ConsumerId NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT * FROM [Payment].[ConfigBimCustomerLevel] WHERE TenantId = @TenantId and UserId = @UserId)
	BEGIN
		UPDATE [Payment].[ConfigBimCustomerLevel]
		SET 
			CustomerApiKey = ISNULL(@CustomerApiKey, CustomerApiKey),
			ConsumerId = ISNULL(@ConsumerId, ConsumerId),
			UpdatedOn = GETUTCDate()
		WHERE TenantId = @TenantId and UserId = @UserId
	END
	ELSE
	BEGIN
		INSERT INTO [Payment].[ConfigBimCustomerLevel]([TenantId],[UserId],[CustomerApiKey], [ConsumerId])
		VALUES (@TenantId, @UserId, @CustomerApiKey, @ConsumerId)
	END
END
