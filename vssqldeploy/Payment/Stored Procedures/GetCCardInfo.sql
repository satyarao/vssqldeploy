﻿-- =============================================
-- Create date: 02/25/2016
-- Description:	Get card information from the [Payment].[LKPrecidiaCardType] and [Payment].[LKPaymentProcessor] using pattern
-- =============================================
CREATE PROCEDURE [Payment].[GetCCardInfo] 
	@CardNumber NVARCHAR(50) = NULL,
	@CardTypeID SMALLINT = NULL OUTPUT,
	@CardType NVARCHAR(255) = NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @RowNum INT,  @Patterns NVARCHAR(max)

	--start with the lowest ID
	SELECT @CardTypeID = MIN(PrecidiaCardTypeID) FROM [Payment].[LKPrecidiaCardType]  
	--get total number of records
	SELECT @RowNum = Count(*) FROM [Payment].[LKPrecidiaCardType]

	--loop until no more records
	WHILE @RowNum > 0                         
	BEGIN
		--get pattern from that row
		SELECT @Patterns = SQLPattern
		FROM [Payment].[LKPrecidiaCardType] WHERE PrecidiaCardTypeID = @CardTypeID AND SQLPattern IS NOT NULL

		--check if pattern matches card number
		IF @Patterns IS NOT NULL AND EXISTS(

			SELECT * FROM (
				SELECT CONCAT(StringValue,'%') as Pattern 
				FROM Common.parseJSON (@Patterns)
				Where StringValue IS NOT NULL AND DATALENGTH(StringValue) != 0
			) P
			Where PATINDEX(P.Pattern, @CardNumber) > 0
		)
		BEGIN
			--if matches then we found our card type and set cardtype name
			SELECT @CardType = PaymentProcessorName 
			FROM [Payment].[LKPaymentProcessor] WHERE PaymentProcessorID = @CardTypeID 
			break 
		END

		--get the next one
		SELECT TOP 1 @CardTypeID=PrecidiaCardTypeID 
		FROM [Payment].[LKPrecidiaCardType] 
		WHERE PrecidiaCardTypeID > @CardTypeID 
		ORDER BY PrecidiaCardTypeID ASC 

		--decrease count
		SET @RowNum = @RowNum - 1 
	END

	--if we never match on any row then set cardtypeid to unknown
	IF @RowNum = 0 
		BEGIN
		--Magic
			SELECT 
				@CardTypeID = [PaymentProcessorID]
				, @CardType = [PaymentProcessorName]
			FROM [Payment].[LKPaymentProcessor]
			Where [PaymentProcessorName] = 'Unknown'


		END

	SELECT	@CardTypeID AS CardTypeID
		  , @CardType   AS CardType
END

