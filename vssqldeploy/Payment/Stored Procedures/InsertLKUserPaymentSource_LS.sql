﻿
-- =============================================
-- Author:		Sergei Buday
-- Create date: 07/29/2019
-- Update date: 07/29/2019
-- Description:	Add @DeviceID to set of input params
-- =============================================
CREATE PROCEDURE [Payment].[InsertLKUserPaymentSource_LS]
	@UserID UNIQUEIDENTIFIER,
	@PaymentSourceID UNIQUEIDENTIFIER,
	@ProviderID INT,
	@PaymentTypeID INT,
	@PaymentDeckID INT,
	@DeviceID nvarchar(255) = NULL
AS
BEGIN
	DECLARE @LS_UserPaymentSourceID int
	DECLARE @CardName nvarchar(50)
	DECLARE @ResultCode int
	
	SELECT @CardName = [Name] FROM [PaymentsEx].[PaymentDeck] WHERE [ID] = @PaymentDeckID
	
	BEGIN TRAN enroll
	BEGIN TRY
	
		INSERT INTO [Payment].[LKUserPaymentSource](UserID, PaymentProcessorID, CardName, IsActive, CreatedOn, UpdatedOn, DeviceID)
		VALUES(@UserID, @ProviderID, @CardName, 1, GETDATE(), GETDATE(), @DeviceID);
	
		SET @LS_UserPaymentSourceID = SCOPE_IDENTITY()
	
	COMMIT TRAN enroll
	SET @ResultCode = 1
	END TRY
	
	BEGIN CATCH
		ROLLBACK TRANSACTION enroll
		SET @ResultCode = -1
	END CATCH
	
	SELECT 
		@ResultCode,
		@LS_UserPaymentSourceID as UserPaymentSourceID
END

