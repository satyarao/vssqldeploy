﻿
CREATE PROC [Payment].[GetPaymentInfoMasterpassCardOnFile]
	@UserId UNIQUEIDENTIFIER,
	@UserPaymentSourceId INT
AS
BEGIN
	SELECT
        pp.[UserPaymentSourceId]
		,pp.[PaymentProviderToken]
		,pp.[MTID]
		,pp.[ExpDate]
		,pp.[ExpiryDate]
		,pp.[FirstSix]
		,pp.[LastFour]
		,pp.[ZipCode]
		,pp.[WalletId]
	FROM [Payment].[UserMasterPassPaymentSource] pp
	LEFT JOIN [Payment].[LKUserPaymentSource] ups ON ups.UserPaymentSourceId = pp.UserPaymentSourceId
	WHERE ups.UserPaymentSourceId = @UserPaymentSourceId AND ups.UserId = @UserId AND ups.IsActive = 1
END
