﻿CREATE PROC [Payment].[GetFrontEndFundingProvider]
	@TenantID UNIQUEIDENTIFIER = NULL
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @SupportedProviders NVARCHAR(MAX)
SET @SupportedProviders = ISNULL(dbo.GetTenantPropertyValue(@TenantID, 'Payments.FundingProvider', DEFAULT), '[]')
IF(@SupportedProviders != '[]' OR @TenantID IS NULL)
BEGIN
	SELECT	FundingProviderID,
			FundingProviderName AS Name
	FROM	[Payment].[FundingProvider] 
	WHERE IsActive = 1 
			AND
		    ( @TenantID IS NULL OR ( FundingProviderID IN (SELECT StringValue from Common.parseJSON(@SupportedProviders)) ) )
END
ELSE
BEGIN
	SELECT TOP(0) NULL
END
