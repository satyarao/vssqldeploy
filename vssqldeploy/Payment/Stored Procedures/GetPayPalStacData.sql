﻿
CREATE PROCEDURE [Payment].[GetPayPalStacData]
	@UserID UNIQUEIDENTIFIER,
	@UserPaymentSourceId INT = NULL,
	@UserPaymentSourceUUId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	DECLARE @ResultSet table (UserPaymentSourceId int, UserPaymentSourceUUId uniqueidentifier, UserCreatedOn datetime2(7), HasLoyaltyFlag bit)

	INSERT INTO @ResultSet (UserPaymentSourceId, UserPaymentSourceUUId, UserCreatedOn)
	SELECT TOP 1 
		ups.[LS_UserPaymentSourceID]    as UserPaymentSourceId,
		prov.[ID]				        as UserPaymentSourceUUId,
		ui.[CreatedOn]					as UserCreatedOn
	FROM [PaymentsEx].[UserPayPalPaymentSource] prov
	LEFT JOIN [PaymentsEx].[LKUserPaymentSource] ups on ups.[ID] = prov.[ID]
	LEFT JOIN [dbo].[UserInfo] ui on ui.[UserId] = ups.[UserId]
	WHERE ups.[UserId] = @UserId AND ups.IsActive = 1 
	AND (
			(@UserPaymentSourceId IS NOT NULL AND ups.[LS_UserPaymentSourceID] = @UserPaymentSourceId)
			OR (@UserPaymentSourceUUId IS NOT NULL AND ups.[ID] = @UserPaymentSourceUUId)
	)

	UPDATE @ResultSet
	SET 
		HasLoyaltyFlag = [Payment].[HasLoyaltyData](@UserId)

	SELECT * FROM @ResultSet
END
