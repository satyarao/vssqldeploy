﻿-- =============================================
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- =============================================
CREATE PROCEDURE [Payment].[InsertUserPaymentSource] 
	@InstallationID NVARCHAR(128) = NULL,
	@UserID UNIQUEIDENTIFIER = NULL,
	@PaymentProcessorID SMALLINT,
	@CardTypeName NVARCHAR(255) = NULL,
	@IsActive BIT = 1
AS
BEGIN
	SET NOCOUNT ON;

	IF @UserID IS NULL AND @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationID)
	END
	
	IF @UserID IS NOT NULL
	BEGIN
		--check if tenant supports funding source which is being added
		IF(1 != [Payment].[IsFundingProviderAllowedByTenant]((SELECT TOP(1) FundingProviderID FROM [Payment].[LKPaymentProcessor]  WHERE PaymentProcessorID = @PaymentProcessorID), (SELECT TenantID FROM dbo.UserInfo WHERE UserID = @UserID)))
		BEGIN
			THROW 100500, 'Tenant does not support funding source', 1;
		END

		DECLARE @ResultID AS INT
		INSERT INTO [Payment].[LKUserPaymentSource](UserID, PaymentProcessorID, CardName, IsActive)
			VALUES(@UserID, @PaymentProcessorID, @CardTypeName, @IsActive)
			SET @ResultID = SCOPE_IDENTITY()

		SELECT @ResultID as UserPaymentSourceID
	END
	ELSE
	BEGIN
		THROW 100501, 'Invalid user', 1;
	END
END
