﻿-- =============================================
-- Create date: 2017-04-16
-- Description:	Insert/Update user's enrollment agreement with terms & conditions
-- =============================================
CREATE PROCEDURE [Payment].[InsertCustomerEnrollmentContractAgreement]
	@UserId UNIQUEIDENTIFIER,
	@EnrollmentArea smallint,
	@ContractVersion NVARCHAR(250),
	@AcceptanceState BIT,
	@Timestamp datetime2(7)
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT * FROM [Payment].[CustomerEnrollmentContractAgreement]
		WHERE UserId = @UserId 
		AND EnrollmentArea = @EnrollmentArea
		AND ContractVersion = @ContractVersion)
	BEGIN
		UPDATE [Payment].[CustomerEnrollmentContractAgreement]
		SET 
			AcceptanceState = ISNULL(@AcceptanceState, AcceptanceState),
			Timestamp = ISNULL(@Timestamp, Timestamp)
		WHERE UserId = @UserId 
		AND EnrollmentArea = @EnrollmentArea
		AND ContractVersion = @ContractVersion
	END
	ELSE
	BEGIN
		INSERT INTO [Payment].[CustomerEnrollmentContractAgreement]([UserId],[EnrollmentArea],[ContractVersion],[AcceptanceState],[Timestamp])
		VALUES (@UserId, @EnrollmentArea, @ContractVersion, @AcceptanceState, @Timestamp)
	END
END
