﻿-- Alter Procedure GetConfigChasePaymentTech
CREATE PROCEDURE [Payment].[GetConfigChasePaymentech] 
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	[ConfigChasePayID]
		,[TenantID]
		,[OrbitalConnectionUsername]
		,[OrbitalConnectionPassword]
		,[BIN]
		,[MerchantID]
		,[TerminalID]
		,[ChasepayMerchantID]
		,[DeviceSignatureVersion]
		,[IsActive]
	FROM [Payment].[ConfigChasePaymentech]
	WHERE IsActive = 1 AND TenantID = @TenantID
END
