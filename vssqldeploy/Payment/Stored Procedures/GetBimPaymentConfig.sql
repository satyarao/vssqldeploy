﻿
CREATE PROCEDURE [Payment].[GetBimPaymentConfig] 
	@TenantId UNIQUEIDENTIFIER,
	@StoreId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	 
		 sl.[TenantId]
		,sl.[StoreId]
		,tl.[InstitutionId]
		,sl.[MerchantId]
		,sl.[TerminalPayload]
	FROM [Payment].[ConfigBimStoreLevel] sl
	LEFT JOIN [Payment].[ConfigBimTenantLevel] tl ON sl.TenantId = tl.TenantId
	WHERE sl.TenantID = @TenantID and sl.StoreId = @StoreId
END
