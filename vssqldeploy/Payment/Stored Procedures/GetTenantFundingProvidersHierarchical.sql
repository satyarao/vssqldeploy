﻿-- =============================================
-- Author:		Alex Goroshko
-- Create date: 4/13/2016
-- Description:	Returns Tenant and ParentTenant Funding Providers
-- =============================================
CREATE PROCEDURE [Payment].[GetTenantFundingProvidersHierarchical]
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @TenantFundingProviders NVARCHAR(250)
	DECLARE @ParentTenantFundingProviders NVARCHAR(250)
	DECLARE @ParentTenantID NVARCHAR(250)
	DECLARE @ParentTenantName NVARCHAR(250)
	
	SELECT @ParentTenantFundingProviders = DefaultPropertyValue,
		   @ParentTenantID = DefaultTenantID,
		   @ParentTenantName = DefaultTenantName,
		   @TenantFundingProviders = PropertyValue
	FROM dbo.GetTenantPropertyInfo (@TenantID, 'Payments.FundingProvider', DEFAULT)
	
	--check if @FundingProviders is null or empty
	IF (ISNULL(@TenantFundingProviders, '') != '')
		BEGIN
			SELECT [FundingProviderID],
				   [FundingProviderName]
			FROM [Payment].[FundingProvider]
			WHERE IsActive = 1
			AND FundingProviderID IN (SELECT StringValue from Common.parseJSON(@TenantFundingProviders));
		END
	ELSE 
		BEGIN
			SELECT TOP(0) NULL
		END
	
	IF (ISNULL(@ParentTenantFundingProviders, '') != '')
		BEGIN
			SELECT @ParentTenantID AS ParentTenantID,
				   @ParentTenantName AS ParentTenantName,
				   [FundingProviderID] AS ParentFundingProviderID,
				   [FundingProviderName] AS ParentFundingProviderName
			FROM [Payment].[FundingProvider]
			WHERE IsActive = 1
			AND FundingProviderID IN (SELECT StringValue from Common.parseJSON(@ParentTenantFundingProviders));
		END
	ELSE 
		BEGIN 
			SELECT TOP(0) NULL
		END
END
