﻿
-- =============================================
-- Description:    Get User Loyalty Only Payment Data 
-- =============================================
CREATE   PROCEDURE [Payment].[GetUserLoyaltyOnlyPaymentSource] 
                @UserPaymentSourceID INT
AS
BEGIN
                SET NOCOUNT ON;
                SELECT TOP 1
                                LK.UserPaymentSourceID                            as UserPaymentSourceID,
                                LK.UserID                                            as UserID,
                                loyalty.[Balance]                                              as Balance
                FROM [Payment].[UserLoyaltyOnlyPaymentSource] loyalty
                LEFT JOIN [Payment].[LKUserPaymentSource] AS LK ON LK.UserPaymentSourceID = loyalty.UserPaymentSourceID
                WHERE LK.IsActive = 1 AND LK.UserPaymentSourceID = @UserPaymentSourceID
END


