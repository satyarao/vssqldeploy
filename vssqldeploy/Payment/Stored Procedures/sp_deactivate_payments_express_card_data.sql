﻿--added pay express deactivation for cloudAIO

CREATE PROCEDURE [Payment].[sp_deactivate_payments_express_card_data]
  @UserID UNIQUEIDENTIFIER,
  @UserPaymentSourceID UNIQUEIDENTIFIER
AS
BEGIN
  DECLARE @LS_UserPaymentSourceID int
  DECLARE @ResultCode int

  BEGIN TRAN unenroll
  BEGIN TRY
  SELECT @LS_UserPaymentSourceID = LS_UserPaymentSourceID FROM [PaymentsEx].[LKUserPaymentSource] WHERE ID = @UserPaymentSourceID

  UPDATE [Payment].[LKUserPaymentSource] 
  SET IsActive = 0, UpdatedOn = GETDATE() 
  WHERE UserPaymentSourceID = @LS_UserPaymentSourceID

  UPDATE [PaymentsEx].[LKUserPaymentSource]
  SET IsActive = 0, UpdatedOn = GETDATE()
  WHERE ID = @UserPaymentSourceID

  UPDATE [PaymentsEx].[UserPaymentsExpressPaymentSource]
  SET PaymentProviderToken = '', IsActive = 0
  WHERE UserPaymentSourceID = @UserPaymentSourceId
  SET @ResultCode = 1

  COMMIT TRAN unenroll
  SET @ResultCode = 1
  END TRY

  BEGIN CATCH
    ROLLBACK TRANSACTION unenroll
	SET @ResultCode = -1
   END CATCH

    SELECT @ResultCode
END
