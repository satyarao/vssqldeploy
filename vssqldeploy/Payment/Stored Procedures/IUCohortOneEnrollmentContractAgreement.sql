﻿
CREATE PROCEDURE [Payment].[IUCohortOneEnrollmentContractAgreement]
	@UserId UNIQUEIDENTIFIER,
	@AcceptanceState BIT
AS 
BEGIN
	IF EXISTS(SELECT * FROM [Payment].[CustomerCohortOneEnrollmentContractAgreement] WHERE UserId = @UserId)
	BEGIN
		UPDATE [Payment].[CustomerCohortOneEnrollmentContractAgreement]
		SET
			AcceptanceState = @AcceptanceState,
			UpdatedOn = GETUTCDATE()
		WHERE UserId = @UserId
	END
	ELSE
	BEGIN
		INSERT INTO [Payment].[CustomerCohortOneEnrollmentContractAgreement] ([UserId], [AcceptanceState])
		VALUES (@UserId, @AcceptanceState)
	END
END
