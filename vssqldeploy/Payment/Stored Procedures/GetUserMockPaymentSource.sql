﻿
-- =============================================
-- Update date: 2017-05-31 - Igor Gaidukov - delete IsDefault functionality
-- Update date: 2017-11-30 - Andrei Ramanovich - Add CardType, CardIssuer, FirstSix, LastFour, Expire
-- Description:	Get User Mock Payment Data 
-- =============================================
CREATE PROCEDURE [Payment].[GetUserMockPaymentSource] 
	@UserPaymentSourceID INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT TOP 1
		LK.UserPaymentSourceID		as UserPaymentSourceID,
		LK.UserID			as UserID,
		mock.[Authorization_State]	as AuthorizationState,
		mock.[Balance]			as Balance,
		mock.[CardType]			as CardType,
		mock.[CardIssuer]		as CardIssuer,
		mock.[FirstSix]			as FirstSix,
		mock.[LastFour]			as LastFour,
		mock.[Expire]			as Expire
	FROM [Payment].[UserMockPaymentSource] mock
	LEFT JOIN [Payment].[LKUserPaymentSource] AS LK ON LK.UserPaymentSourceID = mock.UserPaymentSourceID
	WHERE LK.IsActive = 1 AND LK.UserPaymentSourceID = @UserPaymentSourceID
END

