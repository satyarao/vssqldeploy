﻿-- =============================================
-- Update by: Roman Lavreniuk
-- Update date: 05/08/2018
-- Description: added NickName selection, https://p97networks.atlassian.net/browse/PLAT-2128
-- =============================================

CREATE PROCEDURE [Payment].[GetMobileFundingSource]
	@UserID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @UserID IS NOT NULL
	BEGIN
		SELECT
			LK.UserPaymentSourceID,
			LK.PaymentProcessorID,
			LKPP.PaymentProcessorName,
			FP.FundingProviderID,
			FP.FundingProviderName,
			Uni.FirstText,
			Uni.SecondText,
			Uni.ImageUrl,
			Uni.ExpDate,
            LK.NickName
		FROM [Payment].[LKUserPaymentSource] LK
		LEFT JOIN [Payment].[FundingAccountInfoView] AS Uni ON LK.UserPaymentSourceID = Uni.UserPaymentSourceID
		LEFT JOIN [Payment].[LKPaymentProcessor] LKPP ON LKPP.PaymentProcessorID = LK.PaymentProcessorID
		LEFT JOIN [Payment].[FundingProvider] FP ON FP.FundingProviderID = LKPP.FundingProviderID
		JOIN dbo.UserInfo ui ON ui.UserID = @UserID
		WHERE LK.UserID = @UserID And LK.IsActive = 1 AND 1 = [Payment].[IsFundingProviderAllowedByTenant] (FP.FundingProviderID, ui.TenantID)
		ORDER BY LK.CreatedOn desc
	END
END



