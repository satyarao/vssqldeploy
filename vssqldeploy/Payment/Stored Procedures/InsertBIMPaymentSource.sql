﻿-- =============================================
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- Update date: 10/27/2017 - Ingram Leonards - added UserID check to allow re-load between any app
-- =============================================
CREATE PROCEDURE [Payment].[InsertBIMPaymentSource] 
	@InstallationID NVARCHAR(128) = NULL,
	@UserID UNIQUEIDENTIFIER = NULL,
	@EmailAddress NVARCHAR(255),
	@ConnectionCode NVARCHAR(max),
	@BankId NVARCHAR(max),
	@BankAccountGuid UNIQUEIDENTIFIER,
	@BankRoutingNumber NVARCHAR(max) = NULL,
	@BankAccountNumber NVARCHAR(max) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Returned_UserPaymentSourceID INT = 0;

	IF EXISTS(Select * FROM [Payment].[LKUserPaymentSource] ups
			LEFT JOIN [Payment].[UserBIMPaymentSource] bim ON bim.[UserPaymentSourceID] = ups.[UserPaymentSourceID]
			Where bim.ConnectionCode = @ConnectionCode AND bim.BankAccountGuid = @BankAccountGuid AND ups.UserID = @UserID)
	BEGIN
		;WITH result AS 
		( 
			SELECT TOP 1 
				 ups.UserPaymentSourceID
				,IsActive
				,UpdatedOn
			FROM [Payment].[LKUserPaymentSource] ups
			LEFT JOIN [Payment].[UserBIMPaymentSource] bim ON bim.[UserPaymentSourceID] = ups.[UserPaymentSourceID]
			Where ConnectionCode = @ConnectionCode AND BankAccountGuid = @BankAccountGuid AND ups.UserID = @UserID
			Order by ups.CreatedOn desc
		) 
		UPDATE result
		SET IsActive = 1
			,UpdatedOn = GETUTCDATE()
			,@Returned_UserPaymentSourceID = UserPaymentSourceID
	END
	ELSE
	BEGIN
		DECLARE @CardTypeName NVARCHAR(255) = 'BIM'
		DECLARE @PaymentProcessorID SMALLINT = [Payment].[GetPaymentProcessorID](@CardTypeName)

		DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID int)
		Insert into @ReturnedPaymentSource
		Exec [Payment].[InsertUserPaymentSource] 
			@InstallationID = @InstallationID,
			@UserID = @UserID,
			@PaymentProcessorID = @PaymentProcessorID,
			@CardTypeName = @CardTypeName

		SELECT @Returned_UserPaymentSourceID = UserPaymentSourceID
		FROM @ReturnedPaymentSource

		IF @Returned_UserPaymentSourceID != 0
		BEGIN
			INSERT INTO [Payment].[UserBIMPaymentSource] ([UserPaymentSourceID], [EmailAddress], [ConnectionCode], [BankId], [BankAccountGuid], [BankRoutingNumber], [BankAccountNumber])
			VALUES (@Returned_UserPaymentSourceID, @EmailAddress, @ConnectionCode, @BankId, @BankAccountGuid, @BankRoutingNumber, @BankAccountNumber)
		END
	END

	SELECT @Returned_UserPaymentSourceID AS UserPaymentSourceID

END

