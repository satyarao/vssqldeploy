﻿
-- =============================================
-- Update date: 2017-05-31 - Igor Gaidukov - delete IsDefault functionality
-- Update date: 2017-11-30 - Andrei Ramanovich - Add CardType, CardIssuer, FirstSix, LastFour, Expire
-- Description:	Insert User Mock Payment Data 
-- =============================================
CREATE PROCEDURE [Payment].[InsertMockPaymentSource] 
	@UserID UNIQUEIDENTIFIER,
	@AuthorizationState NVARCHAR(20),
	@Balance MONEY,
	@CardType nvarchar(50) = NULL,
	@CardIssuer nvarchar(50) = NULL,
	@FirstSix nvarchar(6) = NULL,
	@LastFour nvarchar(4) = NULL,
	@Expire nvarchar(4) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CardTypeName NVARCHAR(255) = 'Mock'
	DECLARE @PaymentProcessorID SMALLINT = [Payment].[GetPaymentProcessorID](@CardTypeName)

	DECLARE @ReturnedPaymentSource TABLE (UserPaymentSourceID int)
	Insert into @ReturnedPaymentSource
	Exec [Payment].[InsertUserPaymentSource] 
		@UserID = @UserID,
		@PaymentProcessorID = @PaymentProcessorID,
		@CardTypeName = @CardTypeName

	DECLARE @Returned_UserPaymentSourceID INT
	SELECT @Returned_UserPaymentSourceID = UserPaymentSourceID
	FROM @ReturnedPaymentSource

	IF @Returned_UserPaymentSourceID != 0
	BEGIN
		INSERT INTO [Payment].[UserMockPaymentSource] (UserPaymentSourceID, Authorization_State, Balance, CardType, CardIssuer, FirstSix, LastFour, Expire)
		VALUES(@Returned_UserPaymentSourceID, @AuthorizationState, @Balance, @CardType, @CardIssuer, @FirstSix, @LastFour, @Expire)
	END

	SELECT @Returned_UserPaymentSourceID AS UserPaymentSourceID

END

