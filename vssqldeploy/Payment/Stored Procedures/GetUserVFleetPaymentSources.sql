﻿
CREATE PROCEDURE [Payment].[GetUserVFleetPaymentSources]
	@UserId uniqueidentifier
AS
BEGIN
	SELECT
		ups.[LS_UserPaymentSourceID],
		ups.[ID],
		ups.[ProviderID],
		driver.[TenantId]	as FleetTenantId,
		tnt.[Name]			as FleetTenantName
	FROM [VFleet].[UserVFleetPaymentSource] prov
	LEFT JOIN [PaymentsEx].[LKUserPaymentSource] ups on ups.[ID] = prov.[UserPaymentSourceID]
	LEFT JOIN [RCI].[FleetDriver] driver on driver.[PaymentSourceId] = prov.[UserPaymentSourceID]
	LEFT JOIN [dbo].[Tenant] tnt on tnt.[TenantId] = driver.[TenantId]
	WHERE ups.[UserID] = @UserId AND ups.[IsActive] = 1
	ORDER BY ups.[CreatedOn] desc
END
