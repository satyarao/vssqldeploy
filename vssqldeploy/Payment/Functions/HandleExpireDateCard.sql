﻿
-- =============================================
-- Create date: 2018-01-22
-- Description:	Handle Expiration Date of card if provider does not support it
-- =============================================
CREATE FUNCTION [Payment].[HandleExpireDateCard]
(
	@ExpDate NCHAR(4),
	@PaymentProcessorID SMALLINT = 0
)
RETURNS NCHAR(4)
AS
BEGIN
	IF((SELECT [Expire] FROM [Payment].[LKPrecidiaCardType] 
		where PrecidiaCardTypeID = @PaymentProcessorID) = 0)
	BEGIN
		RETURN NUll
	END
	RETURN @ExpDate
END
