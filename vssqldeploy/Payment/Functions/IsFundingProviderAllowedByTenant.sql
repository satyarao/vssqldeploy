﻿-- =============================================
-- Author:		Alex Goroshko
-- Create date: 4/12/2016
-- Description:	Check if funding provider is allowed by tenant
-- =============================================
CREATE FUNCTION [Payment].[IsFundingProviderAllowedByTenant]
	(
	   @FundingProviderID INT
	 , @TenantID  UNIQUEIDENTIFIER
	)
RETURNS BIT
AS
BEGIN
	DECLARE	@IsAllowed BIT
	SET @IsAllowed = 0

	DECLARE @SupportedProviders NVARCHAR(MAX)
	SET @SupportedProviders = dbo.GetTenantPropertyValue(@TenantID, 'Payments.FundingProvider', DEFAULT)
	
	if(@FundingProviderID IN (SELECT StringValue from Common.parseJSON(@SupportedProviders)) AND EXISTS(SELECT * FROM [Payment].[FundingProvider] WHERE FundingProviderID = @FundingProviderID AND IsActive = 1))
	 SET @IsAllowed = 1

	RETURN @IsAllowed
END
