﻿
CREATE FUNCTION [Payment].[GetChasePayMspUserPaymentSourceId]
(
	@UserID UNIQUEIDENTIFIER
)
RETURNS INT
AS
BEGIN
	DECLARE @Result INT

	DECLARE @CardTypeName NVARCHAR(255) = 'Chase Pay'
	DECLARE @PaymentProcessorId SMALLINT = [Payment].[GetPaymentProcessorID](@CardTypeName)

	SELECT @Result = UserPaymentSourceID
	FROM [Payment].[LKUserPaymentSource]
	WHERE UserID = @UserID and PaymentProcessorID = @PaymentProcessorId

	RETURN ISNULL(@Result, 0)
END
