﻿
CREATE FUNCTION [Payment].[GetBimPaymentApiKey]
(
	@TenantId UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(128)
AS
BEGIN
	DECLARE @Result NVARCHAR(128) = NULL

	SELECT @Result = [PaymentApiKey]
	FROM [Payment].[ConfigBimTenantLevel]
	WHERE TenantId = @TenantId

	RETURN @Result
END
