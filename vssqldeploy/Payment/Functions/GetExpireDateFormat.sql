﻿-- =============================================
-- Create date: 2016-05-13
-- Description:	Get a formatted Card Expiration Date to display
-- =============================================
CREATE FUNCTION [Payment].[GetExpireDateFormat]
(
	@ExpDate NCHAR(4),
	@PaymentProcessorID SMALLINT = 0
)
RETURNS VARCHAR(5)
AS
BEGIN
	IF((SELECT [Expire] FROM [Payment].[LKPrecidiaCardType] 
		where PrecidiaCardTypeID = @PaymentProcessorID) = 0)
	BEGIN
		RETURN ''
	END

	RETURN CASE WHEN @ExpDate IS NOT NULL AND LEN(@ExpDate) = 4 
		THEN 
			concat(SUBSTRING(@ExpDate,1,2), '/', SUBSTRING(@ExpDate,3,2)) 
		ELSE 
			@ExpDate
	END
END
