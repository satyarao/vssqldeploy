﻿
CREATE FUNCTION [Payment].[GetExternalWalletUserPaymentSourceId]
(
	@UserID UNIQUEIDENTIFIER,
	@FundingProviderName NVARCHAR(255)
)
RETURNS INT
AS
BEGIN
	DECLARE @Result INT
	DECLARE @PaymentProcessorId SMALLINT;

	SELECT @PaymentProcessorID = lkpp.PaymentProcessorID
	FROM [Payment].[FundingProvider] fp
	LEFT JOIN [Payment].[LKPaymentProcessor] lkpp ON fp.FundingProviderId = lkpp.FundingProviderId 
	where fp.FundingProviderName = @FundingProviderName

	SELECT @Result = UserPaymentSourceID
	FROM [Payment].[LKUserPaymentSource]
	WHERE UserID = @UserID and PaymentProcessorID = @PaymentProcessorId

	RETURN ISNULL(@Result, 0)
END
