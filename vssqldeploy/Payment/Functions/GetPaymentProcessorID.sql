﻿
CREATE FUNCTION [Payment].[GetPaymentProcessorID]
(
    @Name NVARCHAR(255)
)
RETURNS SMALLINT
AS
BEGIN
    DECLARE @Result SMALLINT

    SELECT @Result = PaymentProcessorID
    FROM [Payment].[LKPaymentProcessor]
    WHERE PaymentProcessorName = @Name

    RETURN @Result
END
