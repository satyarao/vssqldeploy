﻿
CREATE FUNCTION [Payment].[GetBimCohortOneEnrollmentApiKey]
(
	@TenantId UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(128)
AS
BEGIN
	DECLARE @Result NVARCHAR(128) = NULL

	SELECT @Result = [EnrollmentApiKey]
	FROM [Payment].[ConfigBimTenantCohortOneLevel]
	WHERE TenantId = @TenantId

	RETURN @Result
END
