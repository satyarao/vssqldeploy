﻿
-- =============================================
-- Create date: 2017/02/09
-- Description:	Return true if application with tenant is whitelisted
-- =============================================
CREATE FUNCTION [Payment].[IsChasePayAppAllowed] 
(
	@AppBundleId [nvarchar](100),
	@TenantId [UniqueIdentifier]
)
RETURNS BIT
AS
BEGIN
	--TODO Deprecate this tenant property check that excludes application by tenant
	DECLARE @PropertyValue NVARCHAR(250)
	SET @PropertyValue = [dbo].[GetTenantPropertyValue](@TenantId, 'Chasepay.App.Whitelist.Bypass', DEFAULT)
	IF @PropertyValue = 'true'
	BEGIN
		RETURN 1
	END

	IF @AppBundleId IS NOT NULL AND @AppBundleId != ''
	BEGIN
		IF EXISTS(SELECT TOP 1 * FROM [Payment].[ChasePayMobileAppWhitelist] WHERE AppBundleId = @AppBundleId AND TenantId = @TenantId)
		BEGIN
			RETURN 1
		END
	END
	RETURN 0
END
