﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 10-4-2016
-- Description:	Get MCXMID for StoreId from mapping table
-- =============================================
CREATE FUNCTION Payment.GetMCXMIDForStoreId 
(
	-- Add the parameters for the function here
	@StoreId uniqueidentifier
)
RETURNS nvarchar(255)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(255)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = MCXMID
	from Payment.ConfigMCX
	where StoreId = @StoreId

	-- Return the result of the function
	RETURN @Result

END
