﻿-- =============================================
-- Create date: 2016-11-14
-- Description:	Verify the funding source is supported by the tenant/store for purchase
-- =============================================
CREATE FUNCTION [Payment].[VerifyMerchantLinkCard]
(
	@OwnerId UNIQUEIDENTIFIER,
	@UserPaymentSourceId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE	@IsAllowed BIT
	SET @IsAllowed = 0

	DECLARE @SupportedFundingProviders NVARCHAR(MAX)
	SET @SupportedFundingProviders = dbo.GetTenantPropertyValue(@OwnerId, 'Payments.FundingProvider', DEFAULT)
	
	DECLARE @SupportedMerchantLinkCards NVARCHAR(MAX)
	SET @SupportedMerchantLinkCards = dbo.GetTenantPropertyValue(@OwnerId, 'Payments.Precidia.SupportedCards', DEFAULT)

	DECLARE @MerchantCardId SMALLINT = 0
	IF EXISTS(	SELECT 
					lkups.[UserPaymentSourceID]
				FROM [Payment].[LKUserPaymentSource] lkups
				LEFT JOIN [Payment].[LKPaymentProcessor] lkpp On lkups.[PaymentProcessorID] = lkpp.[PaymentProcessorID]
				WHERE @UserPaymentSourceId = lkups.[UserPaymentSourceID] AND lkups.IsActive = 1
				AND lkpp.[FundingProviderID] IN (SELECT StringValue from Common.parseJSON(@SupportedFundingProviders))
				AND lkpp.[PaymentProcessorID] IN (SELECT StringValue from Common.parseJSON(@SupportedMerchantLinkCards))
	)
	BEGIN
		SET @IsAllowed = 1
	END
	RETURN @IsAllowed
END
