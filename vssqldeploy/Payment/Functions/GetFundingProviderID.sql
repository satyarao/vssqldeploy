﻿CREATE FUNCTION [Payment].[GetFundingProviderID] 
(
	@Name NVARCHAR(255)
)
RETURNS TINYINT
AS
BEGIN
	DECLARE @Result TINYINT

	SELECT @Result = FundingProviderID
	FROM [Payment].[FundingProvider]
	WHERE FundingProviderName = @Name

	RETURN @Result

END
