﻿
CREATE FUNCTION [Payment].[GetBimCustomerApiKey]
(
	@TenantId UNIQUEIDENTIFIER,
	@UserId UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(128)
AS
BEGIN
	DECLARE @Result NVARCHAR(128) = NULL

	SELECT @Result = [CustomerApiKey]
	FROM [Payment].[ConfigBimCustomerLevel]
	WHERE TenantId = @TenantId AND UserId = @UserId

	RETURN @Result
END
