﻿-- =============================================
-- Updated by:      Roman Lavreniuk
-- Update date:     03/15/2018
-- Description:     https://p97networks.atlassian.net/browse/PLAT-2104
-- =============================================

CREATE VIEW [Payment].[FundingAccountInfoView] AS
Select  --Precidia
	ups.UserPaymentSourceID, 
	concat('...',up.LastFour) AS FirstText, 
	[Payment].[GetExpireDateFormat](up.ExpDate, ups.PaymentProcessorID) AS SecondText,
	pp.ImageUrl,
	[Payment].[HandleExpireDateCard](up.ExpDate, ups.PaymentProcessorID) AS ExpDate
FROM [Payment].[UserPrecidiaPaymentSource] up
JOIN [Payment].[LKUserPaymentSource] ups on ups.UserPaymentSourceID = up.UserPaymentSourceID
JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = ups.PaymentProcessorID 
UNION
SELECT  --Zipline
	ups.UserPaymentSourceID, 
	ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.Zipline.Name', DEFAULT), 'ZipLine') AS FirstText,
	uz.ZiplineEmailAddress AS SecondText,
	ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.Zipline.Imgurl', DEFAULT), pp.ImageUrl) AS ImageUrl,
	NULL AS ExpDate
FROM [Payment].[UserZiplinePaymentSource] uz
JOIN [Payment].[LKUserPaymentSource] ups on ups.UserPaymentSourceID = uz.UserPaymentSourceID
JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = ups.PaymentProcessorID
JOIN [UserInfo] ui on ui.UserID = ups.UserID
UNION
SELECT  --BIM
	ups.UserPaymentSourceID, 
	ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.BIM.Name', DEFAULT), 'BIM') AS FirstText,
	'' AS SecondText,
	ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.BIM.Imgurl', DEFAULT), pp.ImageUrl) AS ImageUrl,
	NULL AS ExpDate
FROM [Payment].[UserBIMPaymentSource] ub
JOIN [Payment].[LKUserPaymentSource] ups on ups.UserPaymentSourceID = ub.UserPaymentSourceID
JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = ups.PaymentProcessorID
JOIN [UserInfo] ui on ui.UserID = ups.UserID
UNION
SELECT  --Mock
	ups.UserPaymentSourceID,
    (ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.Mock.Name', DEFAULT), 'Mock') + ' ' +
     COALESCE(um.[CardType], um.[CardIssuer], um.[FirstSix] + '...', '')) AS FirstText,
	(CASE
        WHEN um.Authorization_State = 'Balance' THEN CONCAT(um.Authorization_State, ' $', CAST(um.Balance AS VARCHAR(MAX)))
        ELSE um.Authorization_State
        END) AS SecondText,
	ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.Mock.Imgurl', DEFAULT), pp.ImageUrl) AS ImageUrl,
	NULL AS ExpDate
FROM [Payment].[UserMockPaymentSource] um
JOIN [Payment].[LKUserPaymentSource] ups on ups.UserPaymentSourceID = um.UserPaymentSourceID
JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = ups.PaymentProcessorID 
JOIN [UserInfo] ui on ui.UserID = ups.UserID
UNION
SELECT  --TokenEx
	ups.UserPaymentSourceID, 
	concat('...',tkex.LastFour) AS FirstText, 
	[Payment].[GetExpireDateFormat](tkex.ExpDate, ups.PaymentProcessorID) AS SecondText,
	ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.Mock.Imgurl', DEFAULT), pp.ImageUrl) as ImageUrl,
	[Payment].[HandleExpireDateCard](tkex.ExpDate, ups.PaymentProcessorID) AS ExpDate
FROM [Payment].[UserTokenExPaymentSource] tkex
JOIN [Payment].[LKUserPaymentSource] ups on ups.UserPaymentSourceID = tkex.UserPaymentSourceID
JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = tkex.[TokenExCardTypeID]
JOIN [UserInfo] ui on ui.UserID = ups.UserID
UNION
SELECT  --Synchrony
	ups.UserPaymentSourceID, 
	(CASE
        WHEN syn.NickName IS NULL THEN concat('...', syn.LastFour)
        ELSE syn.NickName
        END) AS FirstText, 
	(CASE
        WHEN syn.NickName IS NULL THEN ''
        ELSE concat('...', syn.LastFour)
        END) AS SecondText,
	ISNULL(dbo.GetTenantPropertyValue(ui.TenantID, 'Payment.Synchrony.Imgurl', DEFAULT), pp.ImageUrl) as ImageUrl,
	NULL AS ExpDate
FROM [Payment].[UserSynchronyPaymentSource] syn
JOIN [Payment].[LKUserPaymentSource] ups on ups.UserPaymentSourceID = syn.UserPaymentSourceID
JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = ups.PaymentProcessorID
JOIN [UserInfo] ui on ui.UserID = ups.UserID


