﻿CREATE TABLE [Payment].[ConfigBimStoreLevel] (
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [StoreId]         UNIQUEIDENTIFIER NOT NULL,
    [MerchantId]      NVARCHAR (128)   NOT NULL,
    [TerminalPayload] NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_ConfigBimStoreLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]       DATETIME2 (7)    NULL,
    CONSTRAINT [PK_ConfigBimStoreLevel] PRIMARY KEY CLUSTERED ([TenantId] ASC, [StoreId] ASC) WITH (FILLFACTOR = 80)
);

