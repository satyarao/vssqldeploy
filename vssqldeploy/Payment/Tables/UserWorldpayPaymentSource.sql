﻿CREATE TABLE [Payment].[UserWorldpayPaymentSource] (
    [UserPaymentSourceID]  INT            NOT NULL,
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [FirstSix]             NCHAR (6)      NOT NULL,
    [LastFour]             NCHAR (4)      NOT NULL,
    [ExpDate]              CHAR (4)       NOT NULL,
    CONSTRAINT [PK_UserWorldpayPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC),
    CONSTRAINT [FK_UserWorldpayPaymentSource_UserPaymentSourceID] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

