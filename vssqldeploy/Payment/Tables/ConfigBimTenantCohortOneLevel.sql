﻿CREATE TABLE [Payment].[ConfigBimTenantCohortOneLevel] (
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    [EnrollmentApiKey] NVARCHAR (128)   NOT NULL,
    [CreatedOn]        DATETIME2 (7)    CONSTRAINT [DF_ConfigBimTenantCohortOneLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]        DATETIME2 (7)    NULL,
    CONSTRAINT [PK_ConfigBimTenantCohortOneLevel] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

