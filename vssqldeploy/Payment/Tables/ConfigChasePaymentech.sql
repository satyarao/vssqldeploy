﻿CREATE TABLE [Payment].[ConfigChasePaymentech] (
    [ConfigChasePayID]          INT                                                IDENTITY (1, 1) NOT NULL,
    [TenantID]                  UNIQUEIDENTIFIER                                   NOT NULL,
    [OrbitalConnectionUsername] NVARCHAR (32)                                      NOT NULL,
    [OrbitalConnectionPassword] NVARCHAR (32) MASKED WITH (FUNCTION = 'default()') NOT NULL,
    [BIN]                       NVARCHAR (6)                                       NOT NULL,
    [MerchantID]                NVARCHAR (15)                                      NOT NULL,
    [TerminalID]                NVARCHAR (3)                                       NOT NULL,
    [ChasepayMerchantID]        NVARCHAR (10)                                      NOT NULL,
    [IsActive]                  BIT                                                CONSTRAINT [DF_ConfigChasePaymentTech_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]                 DATETIME2 (7)                                      CONSTRAINT [DF_ConfigChasePaymentTech_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]                 DATETIME2 (7)                                      NULL,
    [DeviceSignatureVersion]    UNIQUEIDENTIFIER                                   CONSTRAINT [DF_ConfigChasePaymentTech_DeviceSignatureVersion] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_ConfigChasePaymentTech] PRIMARY KEY CLUSTERED ([TenantID] ASC) WITH (FILLFACTOR = 80)
);

