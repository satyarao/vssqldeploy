﻿CREATE TABLE [Payment].[FundingProviderGroupOrder] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [GroupKey]   NVARCHAR (255)   NOT NULL,
    [LanguageID] NVARCHAR (10)    NOT NULL,
    [TenantID]   UNIQUEIDENTIFIER NOT NULL,
    [Order]      INT              NOT NULL,
    CONSTRAINT [PK_FundingProviderGroupOrder] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FundingProviderGroupOrder_AppString] FOREIGN KEY ([GroupKey], [LanguageID]) REFERENCES [Localization].[AppStrings] ([AppStringsId], [LanguageId]),
    CONSTRAINT [FK_FundingProviderGroupOrder_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [UK_GroupKey_Language_Tenant] UNIQUE NONCLUSTERED ([GroupKey] ASC, [LanguageID] ASC, [TenantID] ASC),
    CONSTRAINT [UK_Tenant_Order] UNIQUE NONCLUSTERED ([TenantID] ASC, [Order] ASC)
);

