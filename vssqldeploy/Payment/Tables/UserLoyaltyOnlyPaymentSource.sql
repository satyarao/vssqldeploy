﻿CREATE TABLE [Payment].[UserLoyaltyOnlyPaymentSource] (
    [UserPaymentSourceID] INT   NOT NULL,
    [Balance]             MONEY CONSTRAINT [DF_UserLoyaltyOnlyPaymentSource_Balance] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_UserLoyaltyOnlyPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC),
    CONSTRAINT [FK_UserLoyaltyOnlyPaymentSource_UserPaymentSourceID] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

