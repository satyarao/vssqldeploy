﻿CREATE TABLE [Payment].[ConfigRapidConnectPaymentTerminal] (
    [StoreId]                       UNIQUEIDENTIFIER NOT NULL,
    [TransactionOrigin]             NVARCHAR (32)    NOT NULL,
    [MerchantCategoryCode]          NVARCHAR (4)     NOT NULL,
    [TerminalCategoryCode]          NVARCHAR (2)     NULL,
    [TerminalEntryCapability]       NVARCHAR (2)     NULL,
    [CardCaptureCapability]         NVARCHAR (1)     NULL,
    [PartialAuthApprovalCapability] NVARCHAR (1)     NULL,
    CONSTRAINT [PK_ConfigRapidConnectPaymentTerminal] PRIMARY KEY CLUSTERED ([StoreId] ASC, [TransactionOrigin] ASC),
    CONSTRAINT [CHK_ConfigRapidConnectPaymentTerminal_TransactionOrigin] CHECK ([TransactionOrigin]='inside' OR [TransactionOrigin]='outside')
);

