﻿CREATE TABLE [Payment].[ConfigBimTenantLevel] (
    [TenantId]                     UNIQUEIDENTIFIER NOT NULL,
    [EnrollmentApiKey]             NVARCHAR (128)   NOT NULL,
    [PaymentApiKey]                NVARCHAR (128)   NOT NULL,
    [InstitutionId]                NVARCHAR (128)   NOT NULL,
    [CreatedOn]                    DATETIME2 (7)    CONSTRAINT [DF_ConfigBimTenantLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]                    DATETIME2 (7)    NULL,
    [EnrollmentThumbprint]         NVARCHAR (128)   NULL,
    [PaymentThumbprint]            NVARCHAR (128)   NULL,
    [ContractVersion]              NVARCHAR (250)   NULL,
    [ContractWebUrl]               NVARCHAR (2083)  NULL,
    [EnrollmentKeyVaultIdentifier] NVARCHAR (MAX)   NULL,
    [PaymentKeyVaultIdentifier]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ConfigBimTenantLevel] PRIMARY KEY CLUSTERED ([TenantId] ASC) WITH (FILLFACTOR = 80)
);

