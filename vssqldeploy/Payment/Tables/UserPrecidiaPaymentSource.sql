﻿CREATE TABLE [Payment].[UserPrecidiaPaymentSource] (
    [UserPaymentSourceID]  INT            NOT NULL,
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [FirstSix]             NCHAR (6)      NULL,
    [LastFour]             NCHAR (4)      NOT NULL,
    [ExpDate]              CHAR (4)       NOT NULL,
    CONSTRAINT [PK_UserPrecidiaPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserPrecidiaPaymentSource_UserPaymentSourceID] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

