﻿CREATE TABLE [Payment].[ConfigSynchronyStoreLevel] (
    [StoreId]        UNIQUEIDENTIFIER NOT NULL,
    [CardAcceptorId] NVARCHAR (11)    NOT NULL,
    [CreatedOn]      DATETIME2 (7)    CONSTRAINT [DF_ConfigSynchronyStoreLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]      DATETIME2 (7)    NULL,
    CONSTRAINT [PK_ConfigSynchronyStoreLevel] PRIMARY KEY CLUSTERED ([StoreId] ASC)
);

