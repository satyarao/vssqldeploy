﻿CREATE TABLE [Payment].[ConfigTokenExStoreLevel] (
    [StoreId]           UNIQUEIDENTIFIER NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NOT NULL,
    [TokenExMerchantId] NVARCHAR (50)    NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [CreatedOn]         DATETIME2 (7)    NOT NULL,
    [UpdatedOn]         DATETIME2 (7)    NULL,
    CONSTRAINT [PK__ConfigTo__396B457F5BE7D6AC] PRIMARY KEY CLUSTERED ([StoreId] ASC, [TenantId] ASC)
);

