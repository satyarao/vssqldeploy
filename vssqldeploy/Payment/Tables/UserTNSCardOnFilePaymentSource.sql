﻿CREATE TABLE [Payment].[UserTNSCardOnFilePaymentSource] (
    [UserPaymentSourceId]  INT            NOT NULL,
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [FirstSix]             NCHAR (6)      NULL,
    [LastFour]             NCHAR (4)      NOT NULL,
    [ExpDate]              CHAR (4)       NOT NULL,
    [ExpiryDate]           DATETIME2 (7)  NULL,
    [ZipCode]              NVARCHAR (9)   NULL,
    CONSTRAINT [PK_UserTNSCardOnFilePaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceId] ASC),
    CONSTRAINT [FK_UserTNSCardOnFilePaymentSource_UserPaymentSourceId] FOREIGN KEY ([UserPaymentSourceId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

