﻿CREATE TABLE [Payment].[UserMasterPassPaymentSource] (
    [UserPaymentSourceId]  INT            NOT NULL,
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [MTID]                 NVARCHAR (100) NOT NULL,
    [ExpDate]              CHAR (6)       NULL,
    [ExpiryDate]           DATETIME2 (7)  NULL,
    [FirstSix]             NCHAR (6)      NULL,
    [LastFour]             NCHAR (4)      NULL,
    [ZipCode]              NVARCHAR (9)   NULL,
    [WalletId]             NVARCHAR (3)   NULL,
    CONSTRAINT [PK_UserMasterPassPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceId] ASC),
    CONSTRAINT [FK_UserMasterPassPaymentSource_UserPaymentSourceId] FOREIGN KEY ([UserPaymentSourceId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

