﻿CREATE TABLE [Payment].[CustomerEnrollmentContractAgreement] (
    [UserId]          UNIQUEIDENTIFIER NOT NULL,
    [EnrollmentArea]  SMALLINT         NOT NULL,
    [ContractVersion] NVARCHAR (250)   NOT NULL,
    [AcceptanceState] BIT              CONSTRAINT [DF_CustomerEnrollmentContractAgreement_AcceptanceState] DEFAULT ((0)) NOT NULL,
    [Timestamp]       DATETIME2 (7)    CONSTRAINT [DF_CustomerEnrollmentContractAgreement_Timestamp] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_CustomerEnrollmentContractAgreement] PRIMARY KEY CLUSTERED ([UserId] ASC, [EnrollmentArea] ASC, [ContractVersion] ASC)
);

