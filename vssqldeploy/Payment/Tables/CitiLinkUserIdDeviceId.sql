﻿CREATE TABLE [Payment].[CitiLinkUserIdDeviceId] (
    [UserId]       UNIQUEIDENTIFIER NOT NULL,
    [DeviceId]     NVARCHAR (256)   NOT NULL,
    [LinkDateTime] DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_CitiLinkUserIdDeviceId] PRIMARY KEY CLUSTERED ([UserId] ASC, [DeviceId] ASC)
);

