﻿CREATE TABLE [Payment].[TransUnionConfig] (
    [TenantId]                     UNIQUEIDENTIFIER NOT NULL,
    [BasicAuthUserName]            NVARCHAR (255)   NOT NULL,
    [BasicAuthPassword]            NVARCHAR (255)   NOT NULL,
    [CreditAccountRegistrationUrl] NVARCHAR (2083)  NULL,
    [GetRegisteredCardUrl]         NVARCHAR (2083)  NULL,
    CONSTRAINT [PK_TransUnionConfig] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

