﻿CREATE TABLE [Payment].[ConfigBimCustomerLevel] (
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [UserId]         UNIQUEIDENTIFIER NOT NULL,
    [CustomerApiKey] NVARCHAR (128)   NOT NULL,
    [CreatedOn]      DATETIME2 (7)    CONSTRAINT [DF_ConfigBimCustomerLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]      DATETIME2 (7)    NULL,
    [ConsumerId]     NVARCHAR (50)    NULL,
    CONSTRAINT [PK_ConfigBimCustomerLevel] PRIMARY KEY CLUSTERED ([TenantId] ASC, [UserId] ASC) WITH (FILLFACTOR = 80)
);

