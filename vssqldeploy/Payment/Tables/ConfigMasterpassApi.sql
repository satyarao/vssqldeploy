﻿CREATE TABLE [Payment].[ConfigMasterpassApi] (
    [TenantId]           UNIQUEIDENTIFIER NOT NULL,
    [HostUrl]            NVARCHAR (128)   NOT NULL,
    [ConsumerKey]        NVARCHAR (128)   NOT NULL,
    [MerchantCheckoutId] NVARCHAR (64)    NOT NULL,
    [Thumbprint]         NVARCHAR (128)   NOT NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_ConfigMasterpassApi_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_ConfigMasterpassApi] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

