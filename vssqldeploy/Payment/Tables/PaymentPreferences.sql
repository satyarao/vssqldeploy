﻿CREATE TABLE [Payment].[PaymentPreferences] (
    [Id]                           INT              IDENTITY (1, 1) NOT NULL,
    [UserId]                       UNIQUEIDENTIFIER NOT NULL,
    [PreferredFundingProviderId]   SMALLINT         NOT NULL,
    [PreferredUserPaymentSourceId] INT              NULL,
    CONSTRAINT [PK_PreferredPayments] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PreferredPayments_FundingProvider] FOREIGN KEY ([PreferredFundingProviderId]) REFERENCES [Payment].[FundingProvider] ([FundingProviderID]),
    CONSTRAINT [FK_PreferredPayments_LKUserPaymentSource] FOREIGN KEY ([PreferredUserPaymentSourceId], [UserId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID], [UserID]),
    CONSTRAINT [FK_PreferredPayments_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [IX_PreferredPayments] UNIQUE NONCLUSTERED ([UserId] ASC)
);

