﻿CREATE TABLE [Payment].[UserFleetPaymentSource] (
    [UserPaymentSourceId]      INT             NOT NULL,
    [PaymentProviderToken]     NVARCHAR (100)  NOT NULL,
    [FirstSix]                 NCHAR (6)       NOT NULL,
    [LastFour]                 NCHAR (4)       NOT NULL,
    [ExpiryDate]               DATETIME2 (7)   NULL,
    [ShortCardTypeDescription] NVARCHAR (128)  NULL,
    [FullCardTypeDescription]  NVARCHAR (256)  NULL,
    [SmallCardArtUrl]          NVARCHAR (2083) NULL,
    [LargeCardArtUrl]          NVARCHAR (2083) NULL,
    [FleetEntityPayload]       NVARCHAR (64)   NULL,
    [FleetDataPayload]         NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_UserFleetPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceId] ASC),
    CONSTRAINT [FK_UserFleetPaymentSource_UserPaymentSourceId] FOREIGN KEY ([UserPaymentSourceId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

