﻿CREATE TABLE [Payment].[UserPaymentSourceNuDetectScore] (
    [Id]                  INT              IDENTITY (1, 1) NOT NULL,
    [UserPaymentSourceID] INT              NULL,
    [UserId]              UNIQUEIDENTIFIER NOT NULL,
    [UtcDate]             DATETIME         NOT NULL,
    [NuDetectScore]       INT              NOT NULL,
    [NuDetectScoreBand]   VARCHAR (20)     NOT NULL,
    [ScoreSignals]        NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_UserPaymentSourceNuDetectScore] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserPaymentSourceNuDetectScore_LKUserPaymentSource] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserPaymentSourceNuDetectScore_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserPaymentSourceNuDetectScore_UserPaymentSourceID]
    ON [Payment].[UserPaymentSourceNuDetectScore]([UserPaymentSourceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserPaymentSourceNuDetectScore_UserId]
    ON [Payment].[UserPaymentSourceNuDetectScore]([UserId] ASC);

