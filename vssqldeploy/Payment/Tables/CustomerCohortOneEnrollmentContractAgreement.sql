﻿CREATE TABLE [Payment].[CustomerCohortOneEnrollmentContractAgreement] (
    [UserId]          UNIQUEIDENTIFIER NOT NULL,
    [AcceptanceState] BIT              CONSTRAINT [DF_CustomerCohortOneEnrollmentContractAgreement_AcceptanceState] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_CustomerCohortOneEnrollmentContractAgreement_Timestamp] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]       DATETIME2 (7)    NULL,
    CONSTRAINT [PK_CustomerCohortOneEnrollmentContractAgreement] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

