﻿CREATE TABLE [Payment].[ConfigTokenExTenantLevel] (
    [TenantId]      UNIQUEIDENTIFIER NOT NULL,
    [APIKey]        NVARCHAR (MAX)   NOT NULL,
    [AppId]         NVARCHAR (MAX)   NOT NULL,
    [PublicKey]     NVARCHAR (MAX)   NOT NULL,
    [GatewayName]   NVARCHAR (MAX)   NOT NULL,
    [GatewayId]     NVARCHAR (MAX)   NOT NULL,
    [GatewaySubId]  NVARCHAR (MAX)   NOT NULL,
    [PaymentUri]    NVARCHAR (MAX)   NOT NULL,
    [TokenizeUri]   NVARCHAR (MAX)   NOT NULL,
    [BatchCloseUri] NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK__ConfigTo__2E9B47E14D7E63DB] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

