﻿CREATE TABLE [Payment].[ChasePayEnablement] (
    [Id]        INT                IDENTITY (1, 1) NOT NULL,
    [UserId]    UNIQUEIDENTIFIER   NOT NULL,
    [CreatedOn] DATETIMEOFFSET (7) NOT NULL,
    CONSTRAINT [PK_ChasePayEnablement] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ChasePayEnablement_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ChasePayEnablement_CreatedOn]
    ON [Payment].[ChasePayEnablement]([CreatedOn] ASC);

