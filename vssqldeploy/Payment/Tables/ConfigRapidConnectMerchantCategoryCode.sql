﻿CREATE TABLE [Payment].[ConfigRapidConnectMerchantCategoryCode] (
    [StoreId]              UNIQUEIDENTIFIER NOT NULL,
    [TransactionOrigin]    NVARCHAR (32)    NOT NULL,
    [MerchantCategoryCode] NVARCHAR (4)     NOT NULL,
    [TerminalCategoryCode] NVARCHAR (2)     NULL,
    CONSTRAINT [PK_ConfigRapidConnectMerchantCategoryCode] PRIMARY KEY CLUSTERED ([StoreId] ASC, [TransactionOrigin] ASC),
    CONSTRAINT [CHK_ConfigRapidConnectMerchantCategoryCode_TransactionOrigin] CHECK ([TransactionOrigin]='inside' OR [TransactionOrigin]='outside')
);

