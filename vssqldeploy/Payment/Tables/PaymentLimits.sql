﻿CREATE TABLE [Payment].[PaymentLimits] (
    [PaymentProcessorID] SMALLINT         NOT NULL,
    [FundingProviderID]  SMALLINT         NOT NULL,
    [TenantID]           UNIQUEIDENTIFIER NOT NULL,
    [CardAuthLimit]      DECIMAL (9, 2)   DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PaymentLimits] PRIMARY KEY CLUSTERED ([PaymentProcessorID] ASC, [FundingProviderID] ASC, [TenantID] ASC),
    CONSTRAINT [FK_PaymentLimits_FundingProvider] FOREIGN KEY ([FundingProviderID]) REFERENCES [Payment].[FundingProvider] ([FundingProviderID]),
    CONSTRAINT [FK_PaymentLimits_PaymentProcessor] FOREIGN KEY ([PaymentProcessorID]) REFERENCES [Payment].[LKPaymentProcessor] ([PaymentProcessorID])
);

