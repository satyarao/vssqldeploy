﻿CREATE TABLE [Payment].[UserTokenExPaymentSource] (
    [UserPaymentSourceID]  INT            NOT NULL,
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [FirstSix]             NCHAR (6)      NULL,
    [LastFour]             NCHAR (4)      NULL,
    [ExpDate]              CHAR (4)       NULL,
    [TokenExCardTypeID]    SMALLINT       NULL,
    CONSTRAINT [PK__UserToke__283E65EBEAC797DE] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC)
);

