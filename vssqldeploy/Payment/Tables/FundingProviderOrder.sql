﻿CREATE TABLE [Payment].[FundingProviderOrder] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [GroupKey]          NVARCHAR (255)   NOT NULL,
    [LanguageID]        NVARCHAR (10)    NOT NULL,
    [TenantID]          UNIQUEIDENTIFIER NOT NULL,
    [FundingProviderID] SMALLINT         NOT NULL,
    [Order]             INT              NOT NULL,
    CONSTRAINT [PK_FundingProviderOrder] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FundingProviderOrder_AppString] FOREIGN KEY ([GroupKey], [LanguageID]) REFERENCES [Localization].[AppStrings] ([AppStringsId], [LanguageId]),
    CONSTRAINT [FK_FundingProviderOrder_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [UK_FPGroup_Tenant_Order] UNIQUE NONCLUSTERED ([GroupKey] ASC, [LanguageID] ASC, [TenantID] ASC, [Order] ASC),
    CONSTRAINT [UK_GroupKey_Language_Tenant_FPID] UNIQUE NONCLUSTERED ([GroupKey] ASC, [LanguageID] ASC, [TenantID] ASC, [FundingProviderID] ASC)
);

