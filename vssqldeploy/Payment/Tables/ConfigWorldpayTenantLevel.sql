﻿CREATE TABLE [Payment].[ConfigWorldpayTenantLevel] (
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [MerchantCode]   NVARCHAR (MAX)   NOT NULL,
    [Username]       NVARCHAR (MAX)   NOT NULL,
    [Password]       NVARCHAR (MAX)   NOT NULL,
    [InstallationId] NVARCHAR (MAX)   NOT NULL,
    [PaymentUri]     NVARCHAR (MAX)   NOT NULL,
    PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

