﻿CREATE TABLE [Payment].[ConfigSynchronyTenantLevel] (
    [TenantId]               UNIQUEIDENTIFIER NOT NULL,
    [CardAcceptorTerminalId] NVARCHAR (8)     NOT NULL,
    [WalletId]               NVARCHAR (256)   NOT NULL,
    [WsUserName]             NVARCHAR (512)   NULL,
    [WsPassword]             NVARCHAR (512)   NULL,
    [FieldLevelSymmetricKey] NVARCHAR (MAX)   NULL,
    [CreatedOn]              DATETIME2 (7)    CONSTRAINT [DF_ConfigSynchronyTenantLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]              DATETIME2 (7)    NULL,
    [CompanyId]              NVARCHAR (4)     NULL,
    CONSTRAINT [PK_ConfigSynchronyTenantLevel] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

