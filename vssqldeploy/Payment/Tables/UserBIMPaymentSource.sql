﻿CREATE TABLE [Payment].[UserBIMPaymentSource] (
    [UserPaymentSourceID] INT              NOT NULL,
    [EmailAddress]        NVARCHAR (255)   NOT NULL,
    [ConnectionCode]      NVARCHAR (MAX)   NOT NULL,
    [BankId]              NVARCHAR (MAX)   NOT NULL,
    [BankAccountGuid]     UNIQUEIDENTIFIER NOT NULL,
    [BankRoutingNumber]   NVARCHAR (MAX)   NULL,
    [BankAccountNumber]   NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_UserBIMPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserBIMPaymentSource_UserPaymentSourceID] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

