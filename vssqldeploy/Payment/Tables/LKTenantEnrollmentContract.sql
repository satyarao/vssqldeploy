﻿CREATE TABLE [Payment].[LKTenantEnrollmentContract] (
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [EnrollmentArea]  SMALLINT         NOT NULL,
    [ContractVersion] NVARCHAR (250)   NOT NULL,
    [ContractWebUrl]  NVARCHAR (2083)  NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_LKTenantEnrollmentContract_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]       DATETIME2 (7)    NULL,
    CONSTRAINT [PK_LKTenantEnrollmentContract] PRIMARY KEY CLUSTERED ([TenantId] ASC, [EnrollmentArea] ASC)
);

