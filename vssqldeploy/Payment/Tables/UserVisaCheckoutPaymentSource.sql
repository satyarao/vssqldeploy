﻿CREATE TABLE [Payment].[UserVisaCheckoutPaymentSource] (
    [UserPaymentSourceId]  INT            NOT NULL,
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [CallId]               NVARCHAR (128) NOT NULL,
    [PaymentMethodType]    NVARCHAR (32)  NOT NULL,
    [Cryptogram]           NVARCHAR (MAX) NULL,
    [FirstSix]             NCHAR (6)      NULL,
    [LastFour]             NCHAR (4)      NULL,
    [ExpiryDate]           DATETIME2 (7)  NULL,
    [ImageUrl]             NVARCHAR (MAX) NULL,
    [NickName]             NVARCHAR (64)  NULL,
    [CardType]             NVARCHAR (64)  NULL,
    [CardBrand]            NVARCHAR (64)  NULL,
    [ZipCode]              NVARCHAR (9)   NULL,
    CONSTRAINT [PK_UserVisaCheckoutPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceId] ASC),
    CONSTRAINT [FK_UserVisaCheckoutPaymentSource_UserPaymentSourceId] FOREIGN KEY ([UserPaymentSourceId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [callid_index]
    ON [Payment].[UserVisaCheckoutPaymentSource]([CallId] ASC);

