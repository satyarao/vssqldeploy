﻿CREATE TABLE [Payment].[ChasePayMobileAppWhitelist] (
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [AppBundleId] NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_ChasePayMobileAppWhitelist] PRIMARY KEY CLUSTERED ([TenantId] ASC, [AppBundleId] ASC)
);

