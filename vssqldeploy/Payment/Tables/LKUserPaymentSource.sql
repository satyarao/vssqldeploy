﻿CREATE TABLE [Payment].[LKUserPaymentSource] (
    [UserPaymentSourceID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID]              UNIQUEIDENTIFIER NOT NULL,
    [PaymentProcessorID]  SMALLINT         NOT NULL,
    [CardName]            NVARCHAR (255)   NULL,
    [IsActive]            BIT              CONSTRAINT [DF_LKUserPaymentSource_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]           DATETIME2 (7)    CONSTRAINT [DF_LKUserPaymentSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]           DATETIME2 (7)    NULL,
    [NickName]            NVARCHAR (64)    NULL,
    [DeviceID]            NVARCHAR (255)   NULL,
    CONSTRAINT [PK_LKUserPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_LKUserPaymentSource_PaymentProcessorID] FOREIGN KEY ([PaymentProcessorID]) REFERENCES [Payment].[LKPaymentProcessor] ([PaymentProcessorID]),
    CONSTRAINT [FK_LKUserPaymentSource_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [nci_wi_LKUserPaymentSource_4D09D0D611B72CAAD823794F3B22911C]
    ON [Payment].[LKUserPaymentSource]([IsActive] ASC, [UserID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_LKUserPaymentSource]
    ON [Payment].[LKUserPaymentSource]([UserPaymentSourceID] ASC, [UserID] ASC);

