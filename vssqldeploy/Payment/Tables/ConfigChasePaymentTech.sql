﻿CREATE TABLE [Payment].[ConfigChasePaymentTech] (
    [ConfigChasePayID]          INT                                                IDENTITY (1, 1) NOT NULL,
    [TenantID]                  UNIQUEIDENTIFIER                                   NOT NULL,
    [OrbitalConnectionUsername] NVARCHAR (32)                                      NOT NULL,
    [OrbitalConnectionPassword] NVARCHAR (32) MASKED WITH (FUNCTION = 'default()') NOT NULL,
    [BIN]                       NVARCHAR (6)                                       NOT NULL,
    [MerchantID]                NVARCHAR (15)                                      NOT NULL,
    [TerminalID]                NVARCHAR (3)                                       NOT NULL,
    [ChasepayMerchantID]        NVARCHAR (10)                                      NOT NULL,
    [IsActive]                  BIT                                                NOT NULL,
    [CreatedOn]                 DATETIME2 (7)                                      NOT NULL,
    [UpdatedOn]                 DATETIME2 (7)                                      NULL
);

