﻿CREATE TABLE [Payment].[ConfigRapidConnectStoreLevel] (
    [StoreId]    UNIQUEIDENTIFIER NOT NULL,
    [GroupId]    NVARCHAR (13)    NOT NULL,
    [MerchantId] NVARCHAR (16)    NOT NULL,
    [TerminalId] NVARCHAR (8)     NOT NULL,
    [CreatedOn]  DATETIME2 (7)    CONSTRAINT [DF_ConfigRapidConnectStoreLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]  DATETIME2 (7)    NULL,
    [TppId]      NVARCHAR (6)     NOT NULL,
    CONSTRAINT [PK_ConfigRapidConnectStoreLevel] PRIMARY KEY CLUSTERED ([StoreId] ASC, [GroupId] ASC, [MerchantId] ASC, [TerminalId] ASC)
);

