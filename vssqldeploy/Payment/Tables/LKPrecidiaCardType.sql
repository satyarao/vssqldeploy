﻿CREATE TABLE [Payment].[LKPrecidiaCardType] (
    [PrecidiaCardTypeID] SMALLINT       NOT NULL,
    [Patterns]           NVARCHAR (MAX) NULL,
    [Format]             NVARCHAR (MAX) NULL,
    [Length]             NVARCHAR (MAX) NULL,
    [Expire]             BIT            NOT NULL,
    [Luhn]               BIT            NOT NULL,
    [DefaultExpireDate]  NCHAR (4)      NULL,
    [SQLPattern]         NVARCHAR (MAX) NULL,
    [AllowSupport]       BIT            CONSTRAINT [DF_LKPrecidiaCardType_AllowSupport] DEFAULT ((1)) NOT NULL,
    [CvvLength]          INT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LKPrecidiaCardType] PRIMARY KEY CLUSTERED ([PrecidiaCardTypeID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_LKPrecidiaCardType_PrecidiaCardTypeID] FOREIGN KEY ([PrecidiaCardTypeID]) REFERENCES [Payment].[LKPaymentProcessor] ([PaymentProcessorID])
);

