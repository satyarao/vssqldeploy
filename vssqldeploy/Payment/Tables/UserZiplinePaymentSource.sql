﻿CREATE TABLE [Payment].[UserZiplinePaymentSource] (
    [UserPaymentSourceID] INT            NOT NULL,
    [ZiplineEmailAddress] NVARCHAR (255) NOT NULL,
    [PINCode]             NVARCHAR (4)   NULL,
    [UpdatedOn]           DATETIME2 (7)  NULL,
    CONSTRAINT [PK_UserZiplinePaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [Length_UserZiplinePaymentSource_ZiplineEmailAddress] CHECK (datalength([ZiplineEmailAddress])>(4)),
    CONSTRAINT [FK_UserZiplinePaymentSource_UserPaymentSourceID] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

