﻿CREATE TABLE [Payment].[FundingProvider] (
    [FundingProviderID]   SMALLINT       NOT NULL,
    [FundingProviderName] NVARCHAR (255) NOT NULL,
    [IsActive]            BIT            CONSTRAINT [DF_FundingProvider_IsActive] DEFAULT ((0)) NOT NULL,
    [DisplayName]         NVARCHAR (255) NULL,
    CONSTRAINT [PK_FundingProvider] PRIMARY KEY CLUSTERED ([FundingProviderID] ASC) WITH (FILLFACTOR = 80)
);

