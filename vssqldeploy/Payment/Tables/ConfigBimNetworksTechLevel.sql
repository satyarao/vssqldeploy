﻿CREATE TABLE [Payment].[ConfigBimNetworksTechLevel] (
    [TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [BaseUri]    NVARCHAR (2083)  NOT NULL,
    [PassKey]    NVARCHAR (256)   NOT NULL,
    [PassPhrase] NVARCHAR (2083)  NOT NULL,
    [CreatedOn]  DATETIME2 (7)    CONSTRAINT [DF_ConfigBimNetworksTechLevel_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]  DATETIME2 (7)    NULL,
    CONSTRAINT [PK_ConfigBimNetworksTechLevel] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

