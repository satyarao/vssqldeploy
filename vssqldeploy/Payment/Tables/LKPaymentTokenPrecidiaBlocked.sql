﻿CREATE TABLE [Payment].[LKPaymentTokenPrecidiaBlocked] (
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [CreatedOn]            DATETIME2 (7)  CONSTRAINT [DF_LKPaymentTokenPrecidiaBlocked_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PaymentProviderToken] PRIMARY KEY NONCLUSTERED ([PaymentProviderToken] ASC)
);

