﻿CREATE TABLE [Payment].[BimCustomerContractAgreement] (
    [UserId]          UNIQUEIDENTIFIER NOT NULL,
    [ContractVersion] NVARCHAR (250)   NOT NULL,
    [AcceptanceState] BIT              CONSTRAINT [DF_BimCustomerContractAgreement_AcceptanceState] DEFAULT ((0)) NOT NULL,
    [Timestamp]       DATETIME2 (7)    CONSTRAINT [DF_BimCustomerContractAgreement_Timestamp] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_BimCustomerContractAgreement] PRIMARY KEY CLUSTERED ([UserId] ASC, [ContractVersion] ASC)
);

