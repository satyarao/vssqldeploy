﻿CREATE TABLE [Payment].[ProvisionFleetCorPrompt] (
    [UserPaymentSourceId] INT            NOT NULL,
    [PromptId]            INT            NOT NULL,
    [PromptedText]        NVARCHAR (256) NOT NULL,
    [ExpectedDataType]    NVARCHAR (128) NOT NULL,
    [ExpectedLength]      INT            NOT NULL,
    CONSTRAINT [PK_ProvisionFleetCorPrompt] PRIMARY KEY CLUSTERED ([UserPaymentSourceId] ASC, [PromptId] ASC),
    CONSTRAINT [FK_ProvisionFleetCorPrompt_UserPaymentSourceId] FOREIGN KEY ([UserPaymentSourceId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

