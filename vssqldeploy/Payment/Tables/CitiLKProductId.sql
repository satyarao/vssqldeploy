﻿CREATE TABLE [Payment].[CitiLKProductId] (
    [ProductId]   NVARCHAR (4)   NOT NULL,
    [ImageUrl]    NVARCHAR (MAX) NULL,
    [ProductName] NVARCHAR (255) NULL,
    CONSTRAINT [PK_CitiLKProductId] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

