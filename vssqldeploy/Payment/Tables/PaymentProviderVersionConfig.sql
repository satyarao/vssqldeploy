﻿CREATE TABLE [Payment].[PaymentProviderVersionConfig] (
    [FundingProviderId] SMALLINT         NOT NULL,
    [AppTenantId]       UNIQUEIDENTIFIER NOT NULL,
    [MinVersion]        NVARCHAR (82)    NULL,
    [MaxVersion]        NVARCHAR (82)    NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_PaymentProviderVersionConfig_CreatedOn] DEFAULT (getutcdate()) NULL,
    CONSTRAINT [PK_PaymentProviderVersionConfig] PRIMARY KEY CLUSTERED ([FundingProviderId] ASC, [AppTenantId] ASC)
);

