﻿CREATE TABLE [Payment].[LKPaymentTokenTNSBlocked] (
    [ID]                   INT            IDENTITY (1, 1) NOT NULL,
    [PaymentProviderToken] NVARCHAR (100) NOT NULL,
    [IsBlocked]            BIT            CONSTRAINT [DF_InAuthMobileDevice_IsBlocked] DEFAULT ((1)) NOT NULL,
    [CreatedOn]            DATETIME2 (7)  CONSTRAINT [DF_InAuthMobileDevice_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]            DATETIME2 (7)  NULL,
    CONSTRAINT [PK_LoyaltyProgram] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [Id_Unique_LKPaymentTokenTNSBlocked_PaymentProviderToken] UNIQUE NONCLUSTERED ([PaymentProviderToken] ASC)
);

