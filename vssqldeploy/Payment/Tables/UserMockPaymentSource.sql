﻿CREATE TABLE [Payment].[UserMockPaymentSource] (
    [UserPaymentSourceID] INT           NOT NULL,
    [Authorization_State] NVARCHAR (20) NOT NULL,
    [Balance]             MONEY         CONSTRAINT [DF_UserMockPaymentSource_Balance] DEFAULT ((0)) NOT NULL,
    [CardType]            NVARCHAR (50) NULL,
    [CardIssuer]          NVARCHAR (50) NULL,
    [FirstSix]            NVARCHAR (6)  NULL,
    [LastFour]            NVARCHAR (4)  NULL,
    [Expire]              NVARCHAR (4)  NULL,
    CONSTRAINT [PK_UserMockPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserMockPaymentSource_UserPaymentSourceID] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

