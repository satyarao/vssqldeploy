﻿CREATE TABLE [Payment].[LKPaymentProcessor] (
    [PaymentProcessorID]   SMALLINT       NOT NULL,
    [FundingProviderID]    SMALLINT       NOT NULL,
    [PaymentProcessorName] NVARCHAR (255) NOT NULL,
    [ImageUrl]             NVARCHAR (MAX) NULL,
    [CardType]             NVARCHAR (50)  NULL,
    [CardIssuer]           NVARCHAR (50)  NULL,
    [PaymentType]          NVARCHAR (255) NULL,
    [CardImageUrl]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_LKPaymentProcessor] PRIMARY KEY CLUSTERED ([PaymentProcessorID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_LKPaymentProcessor_FundingProvider] FOREIGN KEY ([FundingProviderID]) REFERENCES [Payment].[FundingProvider] ([FundingProviderID])
);

