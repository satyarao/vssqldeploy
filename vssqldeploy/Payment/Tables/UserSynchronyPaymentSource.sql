﻿CREATE TABLE [Payment].[UserSynchronyPaymentSource] (
    [UserPaymentSourceId]  INT              NOT NULL,
    [PaymentProviderToken] NVARCHAR (MAX)   NULL,
    [CardIssuer]           NVARCHAR (64)    NOT NULL,
    [FirstSix]             NVARCHAR (6)     NOT NULL,
    [LastFour]             NVARCHAR (4)     NOT NULL,
    [Cid]                  NVARCHAR (3)     NOT NULL,
    [Cvv2]                 NVARCHAR (3)     NULL,
    [NickName]             NVARCHAR (128)   NULL,
    [ProgramName]          NVARCHAR (128)   NULL,
    [SessionId]            UNIQUEIDENTIFIER NULL,
    [FPan]                 NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_UserSynchronyPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceId] ASC),
    CONSTRAINT [FK_UserSynchronyPaymentSource_UserPaymentSourceID] FOREIGN KEY ([UserPaymentSourceId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);

