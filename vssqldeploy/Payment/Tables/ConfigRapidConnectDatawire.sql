﻿CREATE TABLE [Payment].[ConfigRapidConnectDatawire] (
    [TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [TPPId]      NVARCHAR (6)     NOT NULL,
    [GroupId]    NVARCHAR (13)    NOT NULL,
    [DatawireId] NVARCHAR (32)    NOT NULL,
    [MerchantId] NVARCHAR (16)    NOT NULL,
    [TerminalId] NVARCHAR (8)     NOT NULL,
    [Url]        NVARCHAR (4000)  NOT NULL,
    [CreatedOn]  DATETIME2 (7)    CONSTRAINT [DF_ConfigRapidConnectDatawire_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]  DATETIME2 (7)    NULL,
    CONSTRAINT [PK_ConfigRapidConnectDatawire] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

