﻿
	 CREATE PROCEDURE [Loyalty].[GetLoyaltyProgramGuid] @LoyaltyProgrmInt int
	  AS 
	  BEGIN 
	  SELECT [LoyaltyProgrmGuid]
	  FROM [Loyalty].[LKLoyaltyProgramGuid]
	  WHERE [LoyaltyProgrmInt] = @LoyaltyProgrmInt
	  END