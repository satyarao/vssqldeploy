﻿

-- =============================================
-- Author:		Jamy Ryals
-- Create date: 6/3/2016
-- Description:	Creates P97 Loyalty for a user
-- Updated: Alex Goroshko 9/29/2017 Igor Gaidukov - get LoyaltyProgramID by ResolverName (from LoyaltyProgram table)
-- Updated: 02/17/2016
-- Updated: Jamy Ryals 3/7/2017 added guard at beginning to check for program id
-- Updated: Jack Swink 3/5/2019 changed LoyaltyProgramID to uniqueidentifier.
-- Updated: Satya Rao  9/12/2019 - Added DB Logging to identify issue with missing UserLoyaltyCard entries.
-- =============================================
CREATE   PROCEDURE [Loyalty].[CreateP97LoyaltyForUser]
	@UserID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @LoyaltyProgramID UNIQUEIDENTIFIER
	SELECT @LoyaltyProgramID = LoyaltyProgramID FROM [Loyalty].[LoyaltyProgram] WHERE ResolverName = 'P97Loyalty'

	IF(NOT EXISTS(SELECT 1 FROM [Loyalty].[P97LoyaltyCardProvision] WHERE UserID = @UserID AND LoyaltyProgramID = @LoyaltyProgramID))
	BEGIN
		BEGIN TRANSACTION
			
		DECLARE @LoyaltyISO NVARCHAR(8)
		DECLARE @PreviousLoyaltyPrimaryAccountNumber NVARCHAR(20)
		DECLARE @PreviousLoyaltyMiddle NVARCHAR(20)
		DECLARE @NewLoyaltyMiddle NVARCHAR(20)
		DECLARE @Formatter NVARCHAR(10)
		DECLARE @CheckDigit NVARCHAR(1)
		DECLARE @NewLoyaltyCard NVARCHAR(20)
		DECLARE @InsertCount INT
		DECLARE @Message NVARCHAR(MAX)

		
		SELECT TOP(1) @LoyaltyISO = ISO FROM [Loyalty].[ISOMapping] WHERE LoyaltyProgramID = @LoyaltyProgramID
			
		SELECT TOP 1 @PreviousLoyaltyPrimaryAccountNumber = LoyaltyPrimaryAccountNumber FROM Loyalty.P97LoyaltyCardProvision WITH (TABLOCKX) WHERE LoyaltyProgramID = @LoyaltyProgramID ORDER BY LoyaltyCardID DESC
		IF @PreviousLoyaltyPrimaryAccountNumber IS NULL
		BEGIN
			SELECT @PreviousLoyaltyPrimaryAccountNumber = @LoyaltyISO + '0000000002' 
		END
		SELECT @PreviousLoyaltyMiddle = SUBSTRING(@PreviousLoyaltyPrimaryAccountNumber, LEN(@LoyaltyISO) + 1, LEN(@PreviousLoyaltyPrimaryAccountNumber) - 1 - LEN(@LoyaltyISO))
		SELECT @Formatter = 'd' + CONVERT(VARCHAR, LEN(@PreviousLoyaltyMiddle))
			
		SELECT @NewLoyaltyMiddle = FORMAT(CONVERT(INT, @PreviousLoyaltyMiddle) + 1, @Formatter)
		SELECT @CheckDigit = [Loyalty].[Calculate_Luhn_Digit](@LoyaltyISO + @NewLoyaltyMiddle)
		SELECT @NewLoyaltyCard = @LoyaltyISO + @NewLoyaltyMiddle + @CheckDigit

		INSERT INTO [Loyalty].[P97LoyaltyCardProvision]
					([LoyaltyPrimaryAccountNumber]
					,[LoyaltyProgramID]
					,[ProvisionedOn]
					,[UserID])
				VALUES
					(@NewLoyaltyCard
					,@LoyaltyProgramID
					,GETUTCDATE()
					,@UserID)
			

		INSERT INTO [Loyalty].[UserLoyaltyCard]
					([UserID]
					,[LoyaltyProgramID]
					,[LoyaltyPrimaryAccountNumber])
				VALUES
					(@UserID
					,@LoyaltyProgramID
					,@NewLoyaltyCard)

		SET @InsertCount = @@ROWCOUNT
		BEGIN TRY
			SET @Message = FORMATMESSAGE('Loyalty Card : %s, UserLoyaltyCard Created : %d', @NewLoyaltyCard, @InsertCount)
			EXEC dbo.LogMessage @ModuleName= 'CreateP97LoyaltyForUser', @Message = @Message
		END TRY
		BEGIN CATCH
			-- Empty Catch block
		END CATCH

		COMMIT TRANSACTION
	END

END 
