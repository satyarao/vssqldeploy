﻿
	-- =============================================
	-- Create date: 5/24/2016
	-- Update date: 09/03/2018 Dzmitry Mikhailau - PLAT-3683 - Add DisplayName to Loyalty.ProgramResolverMap
	-- Update date: 11/13/2017 Andrei Ramanovich - add BackgroundUrl
	-- Update date: 12/15/2017 Roman Lavreniuk - add ProgramType
	-- Update date: 12/21/2017 Roman Lavreniuk - get only active loyalty programs for tenant
	-- Update date: 3/05/2019 Jack Swink        - changed LoyaltyProgramID to uniqueidentifier.
	-- Description:	Get loyalty program card support
	-- =============================================
	CREATE PROCEDURE [Loyalty].[GetLoyaltyProgramCardSupport]
		@LoyaltyProgramID UNIQUEIDENTIFIER = NULL,
		@ISO NVARCHAR(50) = NULL,
		@OnlyActive BIT = 1,
		@TenantID UNIQUEIDENTIFIER = NULL
	AS
	BEGIN
		SET NOCOUNT ON;

		SELECT 
			lp.LoyaltyProgramID, 
			ISNULL(prm.DisplayName, lp.DisplayName) as DisplayName, 
			iso.ISO, 
			lp.IsActive, 
			lklpt.TenantID,
			lpcs.RegexPattern,
			lpcs.Format,
			lpcs.Length,
			lpcs.Luhn,
			lpcs.ImageUrl,
			lpcs.BackgroundUrl,
			ISNULL(prm.ProgramType, 'NaN') AS LoyaltyProgramType
		FROM Loyalty.LoyaltyProgramCardSupport lpcs
		LEFT JOIN Loyalty.LoyaltyProgram lp on lpcs.LoyaltyProgramID = lp.LoyaltyProgramID
		LEFT JOIN (SELECT LoyaltyProgramID, MAX(ISO) AS ISO FROM [Loyalty].[ISOMapping] GROUP BY LoyaltyProgramID) AS iso 
			ON lp.LoyaltyProgramID = iso.LoyaltyProgramID
		LEFT JOIN [Loyalty].[LKLoyaltyProgram_Tenant] lklpt ON lklpt.LoyaltyProgramID = lpcs.LoyaltyProgramID
		LEFT JOIN [Loyalty].[ProgramResolverMap] prm ON lp.ResolverName = prm.ResolverName
		WHERE lp.LoyaltyProgramID = ISNULL(@LoyaltyProgramID, lp.LoyaltyProgramID)
		and iso.LoyaltyProgramID =  ISNULL(@LoyaltyProgramID, lp.LoyaltyProgramID)
		and iso.ISO = ISNULL(@ISO, iso.ISO)
		and lp.IsActive IN (@OnlyActive, 1) --Only Active or All
		AND lklpt.TenantID = ISNULL(@TenantID, lklpt.TenantID)
		AND lklpt.IsActive IN (@OnlyActive, 1) --Only Active or All
	END