﻿
	-- =============================================
	-- Author:		Mason Sciotti
	-- Create date: 5/25/2016
	-- Update date: 09/03/2018 Dzmitry Mikhailau - PLAT-3683 - Add DisplayName to Loyalty.ProgramResolverMap
	-- Update date: 12/18/2017 Dzmitry Mikhailau - add ProgramType, ResolverName
	-- Update date: 3/05/2019 Jack Swink         - changed LoyaltyProgramID to uniqueidentifier.
	-- Description:	Get user loyalty card with optional parameters
	-- =============================================
	CREATE PROCEDURE [Loyalty].[GetUserLoyaltyCard]
		@UserID uniqueidentifier,
		@StoreID uniqueidentifier = null,
		@LoyaltyProgramID uniqueidentifier = null
	AS
	BEGIN
		SET NOCOUNT ON;
		SELECT 
			ulc.UserID, 
			ulc.LoyaltyPrimaryAccountNumber as LoyaltyCardPAN, 
			ISNULL(prm.DisplayName, lp.DisplayName) as LoyaltyProgramName, 
			lp.LoyaltyProgramID,
			lp.ResolverName,
			ISNULL(prm.ProgramType, 'NaN') AS ProgramType
		from Loyalty.UserLoyaltyCard ulc 
		LEFT JOIN Loyalty.LoyaltyProgram lp ON ulc.LoyaltyProgramID = lp.LoyaltyProgramID
		LEFT JOIN Loyalty.ProgramResolverMap prm ON lp.ResolverName = prm.ResolverName
		where ulc.UserID = @UserID
		and ulc.IsActive = 1
		and lp.LoyaltyProgramID = ISNULL(@LoyaltyProgramID, lp.LoyaltyProgramID)
		and lp.IsActive = 1
		and (@StoreID IS NULL OR lp.LoyaltyProgramID IN (SELECT map.LoyaltyProgramID FROM Loyalty.LoyaltyProgramToStoreMapping map 
			WHERE lp.LoyaltyProgramID = map.LoyaltyProgramID and map.StoreID = @StoreID and map.IsActive = 1))
		ORDER BY lp.LoyaltyProgramID DESC
	END