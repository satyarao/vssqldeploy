﻿
	-- =============================================
	-- Author:		Dzmitry Mikhailau
	-- Update date: 09/03/2018 Dzmitry Mikhailau - PLAT-3683 - Add DisplayName to Loyalty.ProgramResolverMap
	-- Create date: 6/22/2018
	-- =============================================
	CREATE PROCEDURE [Loyalty].[GetLoyaltyCardByPan]
		@LoyaltyPan nvarchar(255),
		@StoreID uniqueidentifier = null
	AS
	BEGIN
		SET NOCOUNT ON;
		SELECT 
			ulc.UserID, 
			ulc.LoyaltyPrimaryAccountNumber as LoyaltyCardPAN, 
			ISNULL(prm.DisplayName, lp.DisplayName) as LoyaltyProgramName, 
			lp.LoyaltyProgramID,
			lp.ResolverName,
			ISNULL(prm.ProgramType, 'NaN') AS ProgramType
		from Loyalty.UserLoyaltyCard ulc 
		LEFT JOIN Loyalty.LoyaltyProgram lp ON ulc.LoyaltyProgramID = lp.LoyaltyProgramID
		LEFT JOIN Loyalty.ProgramResolverMap prm ON lp.ResolverName = prm.ResolverName
		where ulc.LoyaltyPrimaryAccountNumber = @LoyaltyPan
		and ulc.IsActive = 1
		and lp.IsActive = 1
		and (@StoreID IS NULL OR lp.LoyaltyProgramID IN (SELECT map.LoyaltyProgramID FROM Loyalty.LoyaltyProgramToStoreMapping map 
			WHERE lp.LoyaltyProgramID = map.LoyaltyProgramID and map.StoreID = @StoreID and map.IsActive = 1))
		ORDER BY lp.LoyaltyProgramID DESC
	END