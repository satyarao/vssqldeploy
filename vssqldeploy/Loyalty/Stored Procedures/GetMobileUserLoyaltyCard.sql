﻿
	-- =============================================
	-- Create date: 6/30/2016
	-- Update date: 09/03/2018 Dzmitry Mikhailau - PLAT-3683 - Add DisplayName to Loyalty.ProgramResolverMap
	-- Update date: 11/13/2017 Andrei Ramanovich - add BackgroundUrl
	-- Update date: 12/13/2017 Roman Lavreniuk - add ProgramType
	-- Description:	Get user loyalty card with optional parameters
	-- =============================================
	CREATE PROCEDURE [Loyalty].[GetMobileUserLoyaltyCard]
		@UserID uniqueidentifier,
		@StoreID uniqueidentifier = null
	AS
	BEGIN
		SET NOCOUNT ON;
		SELECT 
			ulc.UserID, 
			ulc.LoyaltyPrimaryAccountNumber as LoyaltyCardPAN, 
			ISNULL(prm.DisplayName, lp.DisplayName) as LoyaltyProgramName, 
			lp.LoyaltyProgramID,
			lpcs.ImageUrl,
			lpcs.BackgroundUrl,
			lp.ResolverName,
			ISNULL(prm.ProgramType, 'NaN') as ProgramType
			--Add bit for is supported store card
		from Loyalty.UserLoyaltyCard ulc 
		LEFT JOIN Loyalty.LoyaltyProgram lp ON ulc.LoyaltyProgramID = lp.LoyaltyProgramID
		INNER JOIN Loyalty.LoyaltyProgramCardSupport lpcs ON ulc.LoyaltyProgramID = lpcs.LoyaltyProgramID
		LEFT JOIN Loyalty.ProgramResolverMap prm ON lp.ResolverName = prm.ResolverName
		where ulc.UserID = @UserID
		and ulc.IsActive = 1
		and (@StoreID IS NULL OR lp.LoyaltyProgramID IN (SELECT map.LoyaltyProgramID FROM Loyalty.LoyaltyProgramToStoreMapping map 
			WHERE lp.LoyaltyProgramID = map.LoyaltyProgramID and map.StoreID = @StoreID))
	END