﻿

-- =============================================
-- Author:		Mason Sciotti
-- Create date: 5/25/2016
-- Description:	Delete loyalty card for a user;
-- Update date: 3/05/2019 Jack Swink        - changed LoyaltyProgramID to uniqueidentifier.
-- Update date: 09/12/2019 Satya Rao        - added DB Logging to identify issue with missing UserLoyaltyCard entries. 
-- =============================================
CREATE   PROCEDURE [Loyalty].[DeleteUserLoyaltyCard] 
	@UserID uniqueidentifier,
	@LoyaltyPrimaryAccountNumber nvarchar(255),
	@LoyaltyProgramID uniqueidentifier = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Message NVARCHAR(MAX)

	DELETE ulc
	FROM Loyalty.UserLoyaltyCard ulc
	--INNER JOIN Loyalty.LoyaltyProgramCardSupport lpcs ON ulc.LoyaltyProgramID = lpcs.LoyaltyProgramID
	where UserID = @UserID
	and LoyaltyPrimaryAccountNumber = @LoyaltyPrimaryAccountNumber
	and ulc.LoyaltyProgramID = ISNULL(@LoyaltyProgramID, ulc.LoyaltyProgramID)

	BEGIN TRY

		; WITH MessageLog ([ParamJson])
		AS
		(
			SELECT UserID, LoyaltyPrimaryAccountNumber, UserName
			FROM 
			(
				SELECT @UserID as UserID, @LoyaltyPrimaryAccountNumber as LoyaltyPrimaryAccountNumber, ISNULL(SUSER_NAME(), '') as UserName
			) params
			FOR JSON PATH
		)
		SELECT	@Message =  [ParamJson]
		FROM	MessageLog

		EXEC dbo.LogMessage @ModuleName= 'DeleteUserLoyaltyCard', @Message = @Message
	END TRY
	BEGIN CATCH
		-- Empty Catch Block
	END CATCH
END
