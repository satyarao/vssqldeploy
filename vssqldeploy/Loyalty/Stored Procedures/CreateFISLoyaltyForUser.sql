﻿
	CREATE PROCEDURE [Loyalty].[CreateFISLoyaltyForUser]
    @UserID UNIQUEIDENTIFIER,
    @TenantID UNIQUEIDENTIFIER
AS 
BEGIN
	DECLARE @FisLinkedLoyaltyCard table([LoyaltyPan] NVARCHAR(20), [CreatedOn] DATETIME, [IsActive] Bit)
	
    SET NOCOUNT ON; 

    DECLARE @LoyaltyProgramID UNIQUEIDENTIFIER
    SELECT @LoyaltyProgramID = lp.LoyaltyProgramID FROM [Loyalty].[LoyaltyProgram] lp
    LEFT JOIN [Loyalty].[LKLoyaltyProgram_Tenant] lklpt on lklpt.LoyaltyProgramID = lp.LoyaltyProgramID
    WHERE lklpt.TenantID = @TenantID
    AND lp.ResolverName = 'FIS'
    
    
    IF(NOT EXISTS(SELECT 1 FROM [Loyalty].[FISLoyaltyCard] WHERE UserID = @UserId AND LoyaltyProgramID = @LoyaltyProgramID)) 
        BEGIN
            BEGIN TRANSACTION 

				DECLARE @LoyaltyISO NVARCHAR(9)
				DECLARE @BinLength NVARCHAR(2)
				DECLARE @PreviousLoyaltyCardID NVARCHAR(20)
				DECLARE @PreviousLoyaltyMiddle NVARCHAR(20)
				DECLARE @NewLoyaltyMiddle NVARCHAR(20)
				DECLARE @Formatter NVARCHAR(10)
				DECLARE @CheckDigit NVARCHAR(1)    
				DECLARE @NewLoyaltyCard NVARCHAR(20)
				DECLARE @InsertCount INT
				DECLARE @Message NVARCHAR(MAX) 


				SELECT TOP(1)  @LoyaltyISO = ISO FROM [Loyalty].[ISOMapping] WHERE LoyaltyProgramID = @LoyaltyProgramID and BinLength = 9 


				SELECT TOP (1) @PreviousLoyaltyCardID = LoyaltyCardID FROM [Loyalty].[FISLoyaltyCard] WITH (TABLOCKX)WHERE LoyaltyProgramID = @LoyaltyProgramID ORDER BY LoyaltyCardID DESC
				IF @PreviousLoyaltyCardID IS NULL
				BEGIN            
					SELECT @PreviousLoyaltyCardID = @LoyaltyISO + '0000000'
				END            
            
				SELECT @PreviousLoyaltyMiddle = SUBSTRING(@PreviousLoyaltyCardID, LEN(@LoyaltyISO) + 1, LEN(@PreviousLoyaltyCardID) - 1 - LEN(@LoyaltyISO))
				SELECT @Formatter = 'd' + CONVERT(VARCHAR, LEN(@PreviousLoyaltyMiddle))
            
				SELECT @NewLoyaltyMiddle = FORMAT(CONVERT(INT, @PreviousLoyaltyMiddle) + 1, @Formatter)
				SELECT @CheckDigit = [Loyalty].[Calculate_Luhn_Digit](@LoyaltyISO + @NewLoyaltyMiddle)
				SELECT @NewLoyaltyCard = @LoyaltyISO + @NewLoyaltyMiddle + @CheckDigit

 

				INSERT INTO [Loyalty].[FISLoyaltyCard](
							[LoyaltyCardID],                        
							[ProvisionedOn],
							[UserID],
							[LoyaltyProgramID]                        
							)
						VALUES(                    
						@NewLoyaltyCard,                    
						GETUTCDATE(),
						@UserID,
						@LoyaltyProgramID
						) 

				INSERT INTO [Loyalty].[UserLoyaltyCard]
						([UserID]
						,[LoyaltyProgramID]
						,[LoyaltyPrimaryAccountNumber]
						,IsActive)
						OUTPUT	INSERTED.[LoyaltyPrimaryAccountNumber], 
								INSERTED.[CreatedOn], 
								INSERTED.[IsActive] 
								INTO @FisLinkedLoyaltyCard

					VALUES
						(@UserID
						,@LoyaltyProgramID
						,@NewLoyaltyCard
						,0)

				SELECT * FROM @FisLinkedLoyaltyCard

            SET @InsertCount = @@ROWCOUNT
            BEGIN TRY 
                SET @Message = FORMATMESSAGE('Loyalty Card : %s, UserLoyaltyCard Created : %d', @NewLoyaltyCard, @InsertCount)
                EXEC dbo.LogMessage @ModuleName = 'CreateFISLoyaltyForUser', @Message = @Message
            END TRY
            BEGIN CATCH
                 --Empty Catch block
            END CATCH
 

            COMMIT TRANSACTION
        END
	END
