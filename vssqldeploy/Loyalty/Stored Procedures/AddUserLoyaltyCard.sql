﻿
	-- =============================================
	-- Author:		Mason Sciotti
	-- Create date: 5/25/2016
	-- Update date: 11/8/2017 Andrei Ramanovich - add 50003 case
	-- Update date: 5/30/2018 Dzmitry Mikhailau - change query for select LoyaltyProgramID. 
	-- Update date: 3/05/2019 Jack Swink        - changed LoyaltyProgramID to uniqueidentifier.
	-- Description:	Add loyalty card for a user; It is assumed that the account number is validated by Luhn algorithm
	-- Exceptions: 
	--  50001 - Card not association with Loyalty Program
	--  50002 - Duplicate card in same program not allowed
	--  50003 - Duplicate card in all KRS programs not allowed
	-- =============================================
	CREATE PROCEDURE [Loyalty].[AddUserLoyaltyCard] 
		@UserID uniqueidentifier,
		@LoyaltyPrimaryAccountNumber nvarchar(255)
	AS
	BEGIN
		SET NOCOUNT ON;
		
		DECLARE @LoyaltyProgramID uniqueidentifier;

		SELECT @LoyaltyProgramID = lpcs.LoyaltyProgramID
		FROM Loyalty.LoyaltyProgramCardSupport lpcs
				LEFT JOIN Loyalty.LoyaltyProgram lp ON lp.LoyaltyProgramID = lpcs.LoyaltyProgramID
				LEFT JOIN [Loyalty].[LKLoyaltyProgram_Tenant] lklpt ON lpcs.LoyaltyProgramID = lklpt.LoyaltyProgramID
				LEFT JOIN [Loyalty].[ISOMapping] isom ON lp.LoyaltyProgramID = isom.LoyaltyProgramID
				WHERE isom.ISO = SUBSTRING(@LoyaltyPrimaryAccountNumber, 1, isom.BinLength) 
				AND LEN(@LoyaltyPrimaryAccountNumber) = isom.PanLength 
				AND lklpt.TenantID IN (SELECT TenantID FROM UserInfo where UserID = @UserId)
				AND lp.IsActive = 1

		IF @LoyaltyProgramID IS NULL
		BEGIN
			THROW 50001, 'Card not associated with Loyalty Program', 1;
		END

		IF EXISTS(SELECT * FROM Loyalty.UserLoyaltyCard WHERE UserID = @UserID and LoyaltyProgramID = @LoyaltyProgramID)
		BEGIN
			THROW 50002, 'Multiple loyalty cards for the same program not allowed', 1;
		END

		IF (
			-- KRS program
			EXISTS(
				SELECT lp.LoyaltyProgramID FROM Loyalty.LoyaltyProgram lp 
				JOIN [Loyalty].[ProgramResolverMap] rm ON lp.ResolverName = rm.ResolverName AND rm.ProgramType = 'KRS'
				WHERE lp.LoyaltyProgramID = @LoyaltyProgramID
			)
			AND
			-- user has KRS card
			EXISTS(
				SELECT uc.LoyaltyProgramID FROM Loyalty.UserLoyaltyCard uc 
				JOIN Loyalty.LoyaltyProgram lp ON lp.LoyaltyProgramID = uc.LoyaltyProgramID 
				JOIN [Loyalty].[ProgramResolverMap] rm ON lp.ResolverName = rm.ResolverName AND rm.ProgramType = 'KRS'
				WHERE uc.UserID = @UserID AND uc.IsActive = 1
			)
		)
		BEGIN
			THROW 50003, 'Multiple loyalty cards for KRS not allowed', 1;
		END

		INSERT INTO Loyalty.UserLoyaltyCard (UserID, LoyaltyProgramID, LoyaltyPrimaryAccountNumber)
		VALUES (@UserID, @LoyaltyProgramID, @LoyaltyPrimaryAccountNumber);
	END