﻿
	-- =============================================
	-- Create date: 11/14/2017 by Igor Gaidukov
	-- Update date: 02/18/2019 Darya Batalava - added limit, offset
	-- Update date: 09/03/2018 Dzmitry Mikhailau - PLAT-3683 - Add DisplayName to Loyalty.ProgramResolverMap
	-- Update date: 3/05/2019 Jack Swink        - changed LoyaltyProgramID to uniqueidentifier.
	-- Description:	Get loyalty program
	-- =============================================
	CREATE PROCEDURE [Loyalty].[GetLoyaltyProgram]
		@LoyaltyProgramID UNIQUEIDENTIFIER = NULL
	   ,@Offset INT = NULL
	   ,@Limit INT = NULL
	AS
	BEGIN
		SET NOCOUNT ON;

		IF @Offset IS NULL SET @Offset = 0;
		IF @Limit IS NULL SET @Limit  = (SELECT COUNT(*) FROM Loyalty.LoyaltyProgram);

		WITH TempResult AS (SELECT 
			lp.LoyaltyProgramID
			,ISNULL(prm.DisplayName, lp.DisplayName) as DisplayName
			,iso.ISO
			,lp.IsActive        
			,lp.CreatedOn
			,lp.UpdatedOn
			,lp.ResolverName
			, CAST( CASE WHEN EXISTS(SELECT 1 FROM Loyalty.LoyaltyProgramCardSupport lpcs WHERE lpcs.LoyaltyProgramID = lp.LoyaltyProgramID AND lpcs.ImageUrl IS NOT NULL) 
				THEN 1
				ELSE 0
				END
				AS BIT) as Asset
		FROM Loyalty.LoyaltyProgram lp
		LEFT JOIN [Loyalty].[ProgramResolverMap] prm ON lp.ResolverName = prm.ResolverName
		LEFT JOIN (SELECT LoyaltyProgramID, MAX(ISO) AS ISO FROM [Loyalty].[ISOMapping] GROUP BY LoyaltyProgramID) AS iso 
			ON lp.LoyaltyProgramID = iso.LoyaltyProgramID
		WHERE @LoyaltyProgramID IS NULL OR (lp.LoyaltyProgramID = @LoyaltyProgramID and iso.LoyaltyProgramID =  ISNULL(@LoyaltyProgramID, lp.LoyaltyProgramID))),
		TempCount AS (SELECT COUNT(*) AS Total FROM TempResult)

			SELECT LoyaltyProgramID,
			DisplayName,
			ISO,
			IsActive,
			CreatedOn,
			UpdatedOn,
			ResolverName,
			Asset,
			Total
			FROM TempResult, TempCount
			ORDER BY LoyaltyProgramID OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY
	END