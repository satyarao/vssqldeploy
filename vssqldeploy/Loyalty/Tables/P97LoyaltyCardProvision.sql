﻿CREATE TABLE [Loyalty].[P97LoyaltyCardProvision] (
    [LoyaltyCardID]               INT              IDENTITY (0, 1) NOT NULL,
    [LoyaltyPrimaryAccountNumber] NVARCHAR (255)   NOT NULL,
    [IsProvisioned]               BIT              CONSTRAINT [DF_LoyaltyCard_IsProvisioned] DEFAULT ((0)) NOT NULL,
    [ProvisionedOn]               DATETIME2 (7)    NULL,
    [UserID]                      UNIQUEIDENTIFIER NOT NULL,
    [LoyaltyProgramID]            UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LoyaltyCard] PRIMARY KEY CLUSTERED ([LoyaltyCardID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_LoyaltyCard_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [FK_P97LoyaltyCardProvision_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON UPDATE CASCADE
);

