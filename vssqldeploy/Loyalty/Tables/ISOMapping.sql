﻿CREATE TABLE [Loyalty].[ISOMapping] (
    [ISOMappingID]     INT              IDENTITY (1, 1) NOT NULL,
    [ISO]              NVARCHAR (50)    NOT NULL,
    [PanType]          NVARCHAR (50)    NOT NULL,
    [BinLength]        INT              NOT NULL,
    [PanLength]        INT              NULL,
    [LuhnCheck]        BIT              NOT NULL,
    [LoyaltyProgramID] UNIQUEIDENTIFIER NOT NULL,
    [BarcodeType]      NVARCHAR (255)   DEFAULT ('UPC-A') NULL,
    CONSTRAINT [PK_ISOMapping] PRIMARY KEY CLUSTERED ([ISOMappingID] ASC),
    CONSTRAINT [FK_ISOMapping_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON UPDATE CASCADE,
    CONSTRAINT [Id_Unique_ISOMapping_ISO] UNIQUE NONCLUSTERED ([ISO] ASC)
);

