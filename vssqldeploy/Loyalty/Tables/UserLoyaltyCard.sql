﻿CREATE TABLE [Loyalty].[UserLoyaltyCard] (
    [UserID]                      UNIQUEIDENTIFIER NOT NULL,
    [LoyaltyPrimaryAccountNumber] NVARCHAR (255)   NOT NULL,
    [IsActive]                    BIT              CONSTRAINT [DF_UserLoyaltyCard_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]                   DATETIME2 (7)    CONSTRAINT [DF_UserLoyaltyCard_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]                   DATETIME2 (7)    NULL,
    [LoyaltyProgramID]            UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_UserLoyalty] PRIMARY KEY CLUSTERED ([UserID] ASC, [LoyaltyProgramID] ASC, [LoyaltyPrimaryAccountNumber] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserLoyalty_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [FK_UserLoyaltyCard_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON UPDATE CASCADE
);

