﻿CREATE TABLE [Loyalty].[LoyaltyProgram] (
    [DisplayName]      NVARCHAR (256)   NOT NULL,
    [IsActive]         BIT              NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [UpdatedOn]        DATETIME2 (7)    NULL,
    [ResolverName]     NVARCHAR (256)   NULL,
    [LoyaltyProgramID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LoyaltyProgram] PRIMARY KEY CLUSTERED ([LoyaltyProgramID] ASC)
);

