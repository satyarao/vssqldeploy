﻿CREATE TABLE [Loyalty].[LoyaltyValidationResources] (
    [Id]                INT              IDENTITY (1, 1) NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NOT NULL,
    [LoyaltyProgramId]  UNIQUEIDENTIFIER NOT NULL,
    [InputType]         NVARCHAR (255)   NOT NULL,
    [ValidationPattern] NVARCHAR (MAX)   NULL,
    [Length]            VARCHAR (MAX)    NULL,
    [Enroll]            BIT              NOT NULL,
    [IsActive]          BIT              NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

