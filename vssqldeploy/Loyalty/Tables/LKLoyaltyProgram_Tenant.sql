﻿CREATE TABLE [Loyalty].[LKLoyaltyProgram_Tenant] (
    [TenantID]             UNIQUEIDENTIFIER NOT NULL,
    [IsActive]             BIT              CONSTRAINT [DF_IsActive] DEFAULT ((1)) NOT NULL,
    [AllowMultipleLoyalty] BIT              CONSTRAINT [DF_LKLoyaltyProgram_Tenant_AllowMultipleLoyalty] DEFAULT ((0)) NULL,
    [Weight]               INT              DEFAULT ((100)) NOT NULL,
    [LoyaltyProgramID]     UNIQUEIDENTIFIER NOT NULL,
    CHECK ([Weight]>(-1) AND [Weight]<(101)),
    CONSTRAINT [FK_LKLoyaltyProgram_Tenant_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON UPDATE CASCADE,
    CONSTRAINT [FK_LKLoyaltyProgram_Tenant_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [Unique_LoyaltyProgramID_TenantID] UNIQUE NONCLUSTERED ([LoyaltyProgramID] ASC, [TenantID] ASC)
);

