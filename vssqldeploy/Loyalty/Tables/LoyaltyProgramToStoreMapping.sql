﻿CREATE TABLE [Loyalty].[LoyaltyProgramToStoreMapping] (
    [StoreID]             UNIQUEIDENTIFIER NOT NULL,
    [IsActive]            BIT              NOT NULL,
    [CreatedOn]           DATETIME2 (7)    NOT NULL,
    [UpdatedOn]           DATETIME2 (7)    NULL,
    [IdentityKey]         INT              IDENTITY (1, 1) NOT NULL,
    [AboveSiteCapable]    BIT              DEFAULT ((0)) NOT NULL,
    [LoyaltyProgramID]    UNIQUEIDENTIFIER NOT NULL,
    [IsStandAloneLoyalty] BIT              NULL,
    CONSTRAINT [PK_LoyaltyProgramToStoreMapping] PRIMARY KEY CLUSTERED ([IdentityKey] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_LoyaltyProgramToStoreMapping_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON UPDATE CASCADE,
    CONSTRAINT [FK_LoyaltyProgramToStoreMapping_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID]),
    CONSTRAINT [Unique_LoyaltyProgramID_StoreID] UNIQUE NONCLUSTERED ([LoyaltyProgramID] ASC, [StoreID] ASC) WITH (FILLFACTOR = 80)
);

