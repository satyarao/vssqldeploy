﻿CREATE TABLE [Loyalty].[LoyaltyCardResources] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [LoyaltyProgramId] UNIQUEIDENTIFIER NOT NULL,
    [ImageType]        NVARCHAR (MAX)   NOT NULL,
    [ImageUrl]         NVARCHAR (MAX)   NOT NULL,
    [IsActive]         BIT              NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

