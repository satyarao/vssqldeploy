﻿CREATE TABLE [Loyalty].[FISLoyaltyCard] (
    [LoyaltyCardID]    NVARCHAR (255)   NOT NULL,
    [ProvisionedOn]    DATETIME         NOT NULL,
    [UserID]           UNIQUEIDENTIFIER NOT NULL,
    [LoyaltyProgramID] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([LoyaltyCardID] ASC)
);

