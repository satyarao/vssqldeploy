﻿CREATE TABLE [Loyalty].[LoyaltyProgramCardSupport] (
    [RegexPattern]     NVARCHAR (MAX)   NULL,
    [Format]           NVARCHAR (MAX)   NULL,
    [Length]           NVARCHAR (MAX)   NULL,
    [Luhn]             BIT              CONSTRAINT [DF_LoyaltyProgramCardSupport_Luhn] DEFAULT ((1)) NOT NULL,
    [ImageUrl]         NVARCHAR (MAX)   NULL,
    [BackgroundUrl]    NVARCHAR (MAX)   NULL,
    [LoyaltyProgramID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LoyaltyProgramCardSupport] PRIMARY KEY CLUSTERED ([LoyaltyProgramID] ASC),
    CONSTRAINT [FK_LoyaltyProgramCardSupport_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON UPDATE CASCADE
);

