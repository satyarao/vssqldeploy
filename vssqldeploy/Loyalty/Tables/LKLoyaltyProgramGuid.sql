﻿CREATE TABLE [Loyalty].[LKLoyaltyProgramGuid] (
    [LoyaltyProgrmGuid] UNIQUEIDENTIFIER NOT NULL,
    [LoyaltyProgrmInt]  INT              NOT NULL,
    CONSTRAINT [PK_LKLoyaltyProgramGuid] PRIMARY KEY CLUSTERED ([LoyaltyProgrmGuid] ASC, [LoyaltyProgrmInt] ASC),
    CONSTRAINT [FK_LKLoyaltyProgramGuid_LoyaltyProgrmGuid] FOREIGN KEY ([LoyaltyProgrmGuid]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON UPDATE CASCADE
);

