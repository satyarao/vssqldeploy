﻿CREATE TABLE [Loyalty].[ProgramResolverMap] (
    [ResolverName] NVARCHAR (256) NOT NULL,
    [ProgramType]  NVARCHAR (256) NOT NULL,
    [DisplayName]  NVARCHAR (256) NULL,
    CONSTRAINT [PK_ProgramResolverMap] PRIMARY KEY CLUSTERED ([ResolverName] ASC),
    UNIQUE NONCLUSTERED ([ResolverName] ASC)
);

