﻿
-------------------------------------------------------------------------------
-- Function returns the USERID associated with a loyalty PAN
-- Author:  Jamy Ryals
-- Parameters:  @loyaltyCard VARCHAR(20) 
-- Outputs:  UNIQUEIDENTIFIER: UserID
-------------------------------------------------------------------------------
CREATE FUNCTION [Loyalty].[GetUserForLoyaltyCard] ( @loyaltyCardId VARCHAR(255) )
RETURNS UNIQUEIDENTIFIER 
AS BEGIN 
	DECLARE @userId UNIQUEIDENTIFIER;

	SELECT @userId = [UserID]
	FROM [Loyalty].[UserLoyaltyCard]
	WHERE [LoyaltyPrimaryAccountNumber] = @loyaltyCardId
RETURN @userId
END
