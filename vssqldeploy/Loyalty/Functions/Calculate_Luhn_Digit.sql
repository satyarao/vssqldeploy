﻿

CREATE FUNCTION [Loyalty].[Calculate_Luhn_Digit] ( @inputString VARCHAR(20) )
RETURNS TINYINT 
AS BEGIN 

-------------------------------------------------------------------------------
-- Function to calculate a valid digit according to the 'MOD 10'
-- check, a.k.a. Luhn's Algorithm.
-- Author:  Jamy Ryals
-- Parameters:  @inputString VARCHAR(20) 
-- Outputs:  TINYINT:    Luhn Check Digit
-------------------------------------------------------------------------------



-------------------------------------------------------------------------------
-- TESTS
--
--SELECT '37144963539843' [Test String], [dbo].[Calculate_Luhn_Digit]('37144963539843') [Check Digit], '1' [Expected] UNION ALL
--SELECT '3714 4963 5398 43' [Test String], [dbo].[Calculate_Luhn_Digit]('3714 4963 5398 43') [Check Digit], '1' [Expected] UNION ALL
--SELECT '979797000000000' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000000' ) [Check Digit], '2' [Expected] UNION ALL
--SELECT '979797000000001' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000001' ) [Check Digit], '0' [Expected] UNION ALL
--SELECT '979797000000002' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000002' ) [Check Digit], '8' [Expected] UNION ALL
--SELECT '979797000000003' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000003' ) [Check Digit], '6' [Expected] UNION ALL
--SELECT '979797000000004' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000004' ) [Check Digit], '4' [Expected] UNION ALL
--SELECT '979797000000005' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000005' ) [Check Digit], '1' [Expected] UNION ALL
--SELECT '979797000000006' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000006' ) [Check Digit], '9' [Expected] UNION ALL
--SELECT '979797000000007' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000007' ) [Check Digit], '7' [Expected] UNION ALL
--SELECT '979797000000008' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000008' ) [Check Digit], '5' [Expected] UNION ALL
--SELECT '979797000000009' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000009' ) [Check Digit], '3' [Expected] UNION ALL
--SELECT '979797000000010' [Test String], [dbo].[Calculate_Luhn_Digit]('979797000000010' ) [Check Digit], '1' [Expected] UNION ALL
--SELECT '1234123412341234' [Test String], [dbo].[Calculate_Luhn_Digit]('1234123412341234') [Check Digit], '6' [Expected]  
-------------------------------------------------------------------------------



SELECT @inputString = @inputString + '0'
DECLARE @result TINYINT;

WITH CTE ( Number) as
(
      SELECT 1
      UNION ALL
      SELECT Number + 1
      FROM CTE
      WHERE Number < 100
)
SELECT @result = 10 - SUM(val3) % 10
FROM (
    SELECT  Position = ROW_NUMBER() OVER (ORDER BY number DESC),
            val1 = CAST(ThisChar AS TINYINT)
    FROM (
        SELECT Number, ThisChar = SUBSTRING(@inputString, number, 1)
        FROM CTE
        WHERE Number BETWEEN 1 AND LEN(@inputString)
    ) AllChars
    WHERE ThisChar BETWEEN '0' AND '9'
) Digits
CROSS APPLY ( SELECT val2 = val1 * (2 - Position % 2) ) x
CROSS APPLY ( SELECT val3 = val2 % 10 + val2 / 10 ) y;
IF @result = 10
  SET @result = 0
RETURN @result
END


