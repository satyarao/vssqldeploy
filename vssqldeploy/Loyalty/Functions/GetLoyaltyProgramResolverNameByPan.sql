﻿
	-- =============================================
	-- Create date: 2016-07-29
	-- Modify date: 2017-08-30 --Multiple ISO mapping for single program via CardSupportTable
	-- Update date: 5/30/2018 Dzmitry Mikhailau - change query for use LoyaltyPrimaryAccountNumber is 7 lenght.
	-- Update date: 9/07/2018 Dzmitry Mikhailau - [PLAT-3727] change query with a mapping using the loyalty.ISOMapping table
	-- Modified to read from Mapper table
	-- Description:	Get Loyalty Program resolver name by Primary Account Number and Tenant
	-- =============================================
	CREATE FUNCTION [Loyalty].[GetLoyaltyProgramResolverNameByPan]
	(
		@TenantID uniqueidentifier,
		@LoyaltyPrimaryAccountNumber nvarchar(255)
	)
	RETURNS NVARCHAR(256)
	AS
	BEGIN
		DECLARE @ResolverName NVARCHAR(256);

		SELECT @ResolverName = COALESCE(prm.ProgramType, lp.ResolverName)
		FROM [Loyalty].[LoyaltyProgram] lp
		LEFT JOIN [Loyalty].[LKLoyaltyProgram_Tenant] lklpt ON lp.LoyaltyProgramID = lklpt.LoyaltyProgramID
		LEFT JOIN [Loyalty].[LoyaltyProgramCardSupport] lpcs ON lp.LoyaltyProgramID = lpcs.LoyaltyProgramID
		LEFT JOIN [Loyalty].[ProgramResolverMap] prm ON lp.ResolverName = prm.ResolverName
		LEFT JOIN [Loyalty].[ISOMapping] isom ON lp.LoyaltyProgramID = isom.LoyaltyProgramID
		WHERE isom.ISO = SUBSTRING(@LoyaltyPrimaryAccountNumber, 1, isom.BinLength) 
		AND LEN(@LoyaltyPrimaryAccountNumber) = isom.PanLength 
		AND lklpt.TenantID = @TenantID 

		RETURN @ResolverName
	END