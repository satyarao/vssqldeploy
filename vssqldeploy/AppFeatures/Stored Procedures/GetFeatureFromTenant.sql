﻿-- =============================================
-- Author:		Kristian Blau
-- Create date: 06/30/2016
-- Description:	Get Tenant Specific Feature Strings Values
-- =============================================
CREATE PROCEDURE [AppFeatures].[GetFeatureFromTenant]
	-- Add the parameters for the stored procedure here
	@FeatureName nvarchar(max) = NULL,
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
END
BEGIN TRAN
BEGIN
	select  coalesce(tenant.FeatureStringsId, base.FeatureStringsId) as FeatureStringsId,
			coalesce(tenant.FeatureString, base.FeatureString) as FeatureString
	from AppFeatures.FeatureStrings base full join AppFeatures.TenantFeatures tenant
		 on base.FeatureStringsId = tenant.FeatureStringsId
	where (@FeatureName IS NULL OR coalesce(tenant.FeatureStringsId, base.FeatureStringsId) like '%' + @FeatureName + '%') AND @TenantID = tenant.TenantId
END
COMMIT TRAN
