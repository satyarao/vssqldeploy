﻿-- =============================================
-- Create date: 2016-06-22
-- Description:	Get App Features based on tenant
-- =============================================
CREATE PROCEDURE [AppFeatures].[GetMobileAppFeatures] 
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT 
		  base.FeatureStringsId
		, COALESCE(atf.FeatureString, base.FeatureString) AS FeatureString
		, base.FeatureString AS BaseFeatureString
		, COALESCE(atf.TenantId, '00000000-0000-0000-0000-000000000000') AS TenantId
	FROM [AppFeatures].[FeatureStrings] base
	LEFT JOIN [AppFeatures].[TenantFeatures] atf ON atf.FeatureStringsId = base.FeatureStringsId and atf.TenantId = @TenantId
	ORDER BY base.FeatureStringsId;
END
