﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 7/18/2016
-- Description:	Update all tenant versions for app features
-- =============================================
-- This sproc is necessary as entity framework was running too slowly to use for mass updates
CREATE PROCEDURE [AppFeatures].[UpdateVersions] 	
AS
BEGIN
	SET NOCOUNT ON;
    
	Update AppFeatures.CurrentVersions
	set [Version] = [Version] + 1
END
