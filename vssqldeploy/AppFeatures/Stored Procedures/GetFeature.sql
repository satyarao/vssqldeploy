﻿-- =============================================
-- Author:		Kristian Blau
-- Create date: 06/30/2016
-- Update date: 06/05/2017 - Igor Gaidukov - add new filters (FeatureString and OverrideString)
-- Update date: 07/10/2019 - Richard Anderson - Added Comment
-- Description:	Get Feature Strings Values
-- =============================================
CREATE PROCEDURE [AppFeatures].[GetFeature]
	@FeatureName nvarchar(max) = NULL,
	@TenantId uniqueidentifier = NULL,
	@FeatureString NVARCHAR(MAX) = NULL,
	@Comment NVARCHAR(MAX) = NULL,
	@OverrideString NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	WITH Base (FeatureStringsId, FeatureString, Comment) AS
	(
		SELECT FeatureStringsId, FeatureString, Comment
		FROM AppFeatures.FeatureStrings
		WHERE @FeatureName IS NULL OR FeatureStringsId LIKE '%' + @FeatureName + '%'
	),
	Overrides (TenantId, FeatureStringsId, FeatureString) AS
	(
		SELECT TenantId, FeatureStringsId, FeatureString
		FROM AppFeatures.TenantFeatures
		WHERE (@FeatureName IS NULL OR FeatureStringsId LIKE '%' + @FeatureName + '%')
			AND (@TenantId IS NULL OR TenantId = @TenantId)
	)

	SELECT DISTINCT
		Base.FeatureStringsId,
		Base.FeatureString AS BaseFeatureString,
		Base.Comment AS Comment,
		Overrides.TenantId,
		Overrides.FeatureString
	FROM Base 
	LEFT OUTER JOIN Overrides ON Base.FeatureStringsId = Overrides.FeatureStringsId
	WHERE (@FeatureString IS NULL OR Base.FeatureString LIKE '%' + @FeatureString + '%')
		AND (@OverrideString IS NULL OR Base.FeatureStringsId IN 
				(
				SELECT FeatureStringsId FROM Overrides WHERE (Overrides.FeatureString LIKE '%' + @OverrideString + '%')
				UNION
				SELECT FeatureStringsId FROM Base WHERE (Base.FeatureString LIKE '%' + @OverrideString + '%') AND NOT EXISTS (SELECT 1 FROM Overrides WHERE Overrides.FeatureStringsId = Base.FeatureStringsId)
				)
			)

	ORDER BY base.FeatureStringsId
END

