﻿-- =============================================
-- Author:		Kristian Blau
-- Create date: 06/30/2016
-- Description:	Delete Feature from TenantFeatures Table
-- =============================================
CREATE PROCEDURE [AppFeatures].[DeleteFeatureFromTenant] 
	-- Add the parameters for the stored procedure here
	@FeatureName nvarchar(max),
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
END
BEGIN TRAN
	DELETE FROM AppFeatures.TenantFeatures
	WHERE FeatureStringsId = @FeatureName AND TenantId = @TenantId
COMMIT TRAN
