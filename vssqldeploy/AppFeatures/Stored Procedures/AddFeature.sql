﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 3/4/2016
-- Description:	Adds a new feature key/value pair
-- =============================================
CREATE PROCEDURE [AppFeatures].[AddFeature] 
	-- Add the parameters for the stored procedure here
	@StringKey nvarchar(max),
	@StringValue nvarchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
END
BEGIN TRAN
IF EXISTS (SELECT * from AppFeatures.FeatureStrings 
		   WHERE FeatureStringsId = @StringKey)
BEGIN
   UPDATE AppFeatures.FeatureStrings 
   SET FeatureString = @StringValue
   WHERE FeatureStringsId = @StringKey
END
ELSE
BEGIN
   INSERT INTO AppFeatures.FeatureStrings (FeatureStringsId, FeatureString)
   VALUES (@StringKey, @StringValue)
END
COMMIT TRAN
