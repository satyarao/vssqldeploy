﻿-- =============================================
-- Author:		Kristian Blau
-- Create date: 06/30/2016
-- Description:	Delete Feature from FeatureStrings Table
-- =============================================
CREATE PROCEDURE [AppFeatures].[DeleteFeature] 
	-- Add the parameters for the stored procedure here
	@FeatureName nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
END
BEGIN TRAN
	DELETE FROM AppFeatures.TenantFeatures
	WHERE FeatureStringsId = @FeatureName
	DELETE FROM AppFeatures.FeatureStrings
	WHERE FeatureStringsId = @FeatureName
COMMIT TRAN
