﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 3/4/2016
-- Description:	Adds a new feature key/value pair for a given tenant
-- =============================================
CREATE PROCEDURE [AppFeatures].[AddFeatureForTenant] 
	-- Add the parameters for the stored procedure here
	@StringKey nvarchar(250),
	@StringValue nvarchar(max),
	@TenantId uniqueidentifier

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
END
BEGIN TRAN
IF EXISTS (SELECT * from AppFeatures.TenantFeatures 
		   WHERE FeatureStringsId = @StringKey AND TenantId = @TenantId)
BEGIN
   UPDATE AppFeatures.TenantFeatures 
   SET FeatureString = @StringValue
   WHERE FeatureStringsId = @StringKey AND TenantId=@TenantId
END
ELSE
BEGIN
   INSERT INTO AppFeatures.TenantFeatures (FeatureStringsId, FeatureString, TenantId)
   VALUES (@StringKey, @StringValue, @TenantId)
END
COMMIT TRAN
