﻿-- =============================================
-- Create date: 2016-06-22
-- Description:	Get App Features info of tenant and version
-- =============================================
CREATE PROCEDURE [AppFeatures].[GetTenantAppFeaturesInfo] 
	@TenantID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;

    SELECT 
		  cv.TenantID
		, Name as TenantName
		, [Version]
	FROM [AppFeatures].[CurrentVersions] cv
	LEFT JOIN [dbo].[Tenant] tnt ON tnt.TenantID = cv.TenantID
	WHERE cv.TenantID = ISNULL(@TenantID, cv.TenantID)
	ORDER BY cv.TenantID;
END
