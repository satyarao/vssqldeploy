﻿CREATE TABLE [AppFeatures].[StoreServiceFilter] (
    [StoreServiceFilterID] INT              IDENTITY (1, 1) NOT NULL,
    [ServiceID]            INT              NOT NULL,
    [TenantID]             UNIQUEIDENTIFIER NOT NULL,
    [FilterType]           NVARCHAR (128)   NOT NULL,
    PRIMARY KEY CLUSTERED ([StoreServiceFilterID] ASC),
    FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[LKService] ([ServiceID]),
    FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

