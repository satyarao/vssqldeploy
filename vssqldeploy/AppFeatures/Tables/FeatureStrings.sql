﻿CREATE TABLE [AppFeatures].[FeatureStrings] (
    [FeatureStringsId] NVARCHAR (250) NOT NULL,
    [FeatureString]    NVARCHAR (MAX) NULL,
    [Comment]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AppParameters.ParameterStrings] PRIMARY KEY CLUSTERED ([FeatureStringsId] ASC) WITH (FILLFACTOR = 80)
);

