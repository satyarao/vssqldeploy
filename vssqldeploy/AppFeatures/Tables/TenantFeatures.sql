﻿CREATE TABLE [AppFeatures].[TenantFeatures] (
    [FeatureStringsId] NVARCHAR (250)   NOT NULL,
    [FeatureString]    NVARCHAR (MAX)   NULL,
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_TenantFeatures] PRIMARY KEY CLUSTERED ([FeatureStringsId] ASC, [TenantId] ASC) WITH (FILLFACTOR = 80)
);

