﻿CREATE TABLE [AppFeatures].[StationImage] (
    [StationImageID] INT              IDENTITY (1, 1) NOT NULL,
    [ImageUrl]       NVARCHAR (256)   NOT NULL,
    [TenantID]       UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_StationImage] PRIMARY KEY CLUSTERED ([StationImageID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_StationImage_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

