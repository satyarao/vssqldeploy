﻿CREATE TABLE [AppFeatures].[CurrentVersions] (
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Version]  INT              NULL,
    CONSTRAINT [PK_CurrentVersions_1] PRIMARY KEY CLUSTERED ([TenantId] ASC) WITH (FILLFACTOR = 80)
);

