﻿CREATE TABLE [PNS].[FirebaseProjects] (
    [FirebaseProjectId] NVARCHAR (255) NOT NULL,
    [FirebaseSenderId]  NVARCHAR (255) NOT NULL,
    [FirebaseApiKey]    NVARCHAR (MAX) NOT NULL,
    [BundleIds]         NVARCHAR (MAX) DEFAULT ('[]') NOT NULL,
    [Version]           INT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_FirebaseProjects] PRIMARY KEY CLUSTERED ([FirebaseProjectId] ASC, [Version] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_PNS_FirebaseProjects_BundleIds_IsJson] CHECK (isjson([BundleIds])>(0))
);

