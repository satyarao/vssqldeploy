﻿CREATE TABLE [PNS].[NotificationToken] (
    [NotificationToken] NVARCHAR (255)   NOT NULL,
    [Platform]          NVARCHAR (255)   NULL,
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [IsActive]          BIT              CONSTRAINT [DF_NotificationToken_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_NotificationToken_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]         DATETIME2 (7)    NULL,
    [BundleId]          NVARCHAR (255)   NULL,
    [Version]           INT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_NotificationToken] PRIMARY KEY CLUSTERED ([UserId] ASC, [NotificationToken] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_NotificationToken_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID])
);

