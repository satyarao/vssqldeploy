﻿CREATE TYPE [PNS].[CanonicalRegistrationType] AS TABLE (
    [OldRegistrationId] NVARCHAR (255) NOT NULL,
    [NewRegistrationId] NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([OldRegistrationId] ASC));

