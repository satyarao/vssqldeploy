﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 2018-03-13
-- Description:	Add mobile app bundle id for firebase id
-- =============================================
CREATE PROCEDURE [PNS].[InsertFirebaseAuthBundleId]
	@FirebaseProjectId [nvarchar](255),
	@Version INT,
	@BundleId NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	-- Select the most actual record
	DECLARE @MaxVersion INT;
	SELECT TOP(1) @MaxVersion = [Version]
	FROM [PNS].[FirebaseProjects]
	WHERE [FirebaseProjectId] = @FirebaseProjectId AND [Version] <= @Version
	ORDER BY [Version] DESC

	UPDATE [PNS].[FirebaseProjects]
	SET [BundleIds] = JSON_MODIFY([BundleIds], 'append $', @BundleId) 
	WHERE [FirebaseProjectId] = @FirebaseProjectId AND [Version] = @MaxVersion
		AND ( NOT EXISTS(SELECT value FROM OPENJSON([BundleIds]) WHERE value = @BundleId) )
END
