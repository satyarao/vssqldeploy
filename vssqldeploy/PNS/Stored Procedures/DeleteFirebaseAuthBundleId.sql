﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 2018-03-13
-- Description:	Delete mobile app bundle id for firebase id
-- =============================================
CREATE PROCEDURE [PNS].[DeleteFirebaseAuthBundleId]
	@FirebaseProjectId [nvarchar](255),
	@Version INT,
	@BundleId NVARCHAR(255)
AS
BEGIN

	-- Select the most actual record
	DECLARE @MaxVersion INT;
	SELECT TOP(1) @MaxVersion = [Version]
	FROM [PNS].[FirebaseProjects]
	WHERE [FirebaseProjectId] = @FirebaseProjectId AND [Version] <= @Version
	ORDER BY [Version] DESC

	-- Make new bundleId list
	DECLARE @NewBundleIds TABLE( BundleId nvarchar(max) NOT NULL );
	INSERT INTO @NewBundleIds (BundleId)
	SELECT newIds.*
	FROM [PNS].[FirebaseProjects] fp
	CROSS APPLY ( SELECT value FROM OPENJSON( fp.[BundleIds] ) EXCEPT SELECT @BundleId ) newIds
	WHERE [FirebaseProjectId] = @FirebaseProjectId AND [Version] = @MaxVersion

	UPDATE [PNS].[FirebaseProjects]
	-- Produces json of simple strings
	SET [BundleIds] = REPLACE( REPLACE( 	  
	  ( ISNULL ( ( SELECT * FROM @NewBundleIds FOR JSON AUTO ), '[]' ) )
	  ,'{"BundleId":','' ), '"}','"' ) 
	WHERE [FirebaseProjectId] = @FirebaseProjectId AND [Version] = @MaxVersion
		AND ( EXISTS(SELECT value FROM OPENJSON([BundleIds]) WHERE value = @BundleId) )
END
