﻿
-- =============================================
-- Create date: 2016-10-02
-- Update date: 2018-03-13 Andrei Ramanovich - add BundleId, Version to input parameters
-- Description:	Add or Update Notification Token to an installation;
-- =============================================
CREATE PROCEDURE [PNS].[UpdateNotificationToken]
	@UserId UNIQUEIDENTIFIER,
	@NotificationToken NVARCHAR(255),
	@Platform NVARCHAR(255),
	@BundleId NVARCHAR(255),
	@Version INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Result BIT = 0;
	BEGIN TRAN
		IF @NotificationToken IS NOT NULL
		BEGIN
			--Add user's new token if not exist
			IF NOT EXISTS(SELECT * FROM [PNS].[NotificationToken] WHERE NotificationToken = @NotificationToken AND UserId = @UserId)
			BEGIN
				INSERT INTO [PNS].[NotificationToken] (NotificationToken, Platform, UserId, BundleId, Version)
				VALUES(@NotificationToken, @Platform, @UserId, @BundleId, @Version)
			END
			ELSE
			BEGIN
				--Add or update token on registration; Default IsNotificationTokenActive to true
				UPDATE [PNS].[NotificationToken] 
				SET 
					  IsActive = 1
					, UpdatedOn = GETUTCDATE()
					, BundleId = @BundleId
					, Version = @Version
				WHERE NotificationToken = @NotificationToken AND UserId = @UserId
			END
			SET @Result = 1
		END 

	COMMIT TRAN 
	SELECT @Result as Success
END
