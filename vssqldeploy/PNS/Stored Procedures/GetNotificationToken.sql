﻿
-- =============================================
-- Create date: 2016-10-02
-- Update date: 2018-03-13 Andrei Ramanovich - add BundleId, Version to output
-- Description:	Get a list of notification tokens and its info
-- =============================================
CREATE PROCEDURE [PNS].[GetNotificationToken] 
	@UserId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT 
		NotificationToken,
		Platform,
		BundleId,
		Version
	FROM [PNS].[NotificationToken]
	WHERE UserID = @UserId AND IsActive = 1
END
