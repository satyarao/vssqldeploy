﻿-- =============================================
-- Create date: 2016-10-02
-- Description:	Remove Notification Token; Return number of installation that was unregister
-- =============================================
CREATE PROCEDURE [PNS].[UnregisterNotificationToken]
	@NotificationToken NVARCHAR(255),
	@UserId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Count INT = 0;
	IF @NotificationToken IS NOT NULL
	BEGIN
		--Set IsNotificationTokenActive as false
		UPDATE [PNS].[NotificationToken]
		SET IsActive = 0
			, UpdatedOn = GETUTCDATE()
		WHERE NotificationToken = @NotificationToken
		AND UserId = ISNULL(@UserId, UserId)
		SET @Count = @@ROWCOUNT
	END
	SELECT @Count as Count
END
