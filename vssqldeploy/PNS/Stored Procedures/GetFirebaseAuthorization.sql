﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 2018-03-13
-- Description:	Get corresponding firebase id and key for mobile app bundle id
-- =============================================
CREATE PROCEDURE [PNS].[GetFirebaseAuthorization]
	@BundleId NVARCHAR(255),
	@Version INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP(1) @BundleId AS BundleId, Version, FirebaseSenderId, FirebaseApiKey
	FROM [PNS].[FirebaseProjects]
	WHERE [Version] <= @Version AND EXISTS(SELECT value FROM OPENJSON([BundleIds]) WHERE value = @BundleId)
	ORDER BY [Version] DESC
END
