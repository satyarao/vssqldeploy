﻿
-- =============================================
-- Create date: 2016-10-03
-- Update date: 2017-04-21
-- Description:	Replace or remove Notification Token
-- =============================================
CREATE PROCEDURE [PNS].[RefreshNotificationToken] 
	@UserId UNIQUEIDENTIFIER,
	@CanonicalRegistration PNS.CanonicalRegistrationType READONLY
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE nt
	SET 
		--Set new non-null registration id
		nt.NotificationToken = ISNULL(cr.NewRegistrationId, nt.NotificationToken)
		--Not active if new registration id is null
	  , nt.IsActive = CASE WHEN cr.NewRegistrationId IS NULL THEN 0 ELSE nt.IsActive END
	FROM [PNS].[NotificationToken] nt
    RIGHT JOIN @CanonicalRegistration cr ON nt.NotificationToken = cr.OldRegistrationId
	WHERE nt.UserID = @UserId
END
