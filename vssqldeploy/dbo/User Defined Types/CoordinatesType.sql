﻿CREATE TYPE [dbo].[CoordinatesType] AS TABLE (
    [Longitude] DECIMAL (10, 7) NOT NULL,
    [Latitude]  DECIMAL (10, 7) NOT NULL,
    [Timestamp] DATETIME2 (7)   NULL);

