﻿CREATE TYPE [dbo].[TermsAndConditionsSectionType] AS TABLE (
    [SectionOrder] INT             NOT NULL,
    [SectionTitle] NVARCHAR (1000) NULL,
    [SectionText]  NVARCHAR (MAX)  NULL);

