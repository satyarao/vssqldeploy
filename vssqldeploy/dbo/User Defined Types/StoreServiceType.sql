﻿CREATE TYPE [dbo].[StoreServiceType] AS TABLE (
    [StoreID]   UNIQUEIDENTIFIER NOT NULL,
    [ServiceID] INT              NOT NULL,
    [IsActive]  BIT              NOT NULL);

