﻿CREATE TYPE [dbo].[StoredProcedureExceptionType] AS TABLE (
    [ErrorProcedure]       NVARCHAR (255) NULL,
    [ErrorLine]            INT            NULL,
    [ErrorNumber]          INT            NULL,
    [ErrorMessage]         NVARCHAR (255) NULL,
    [StoredProcedureInput] NVARCHAR (MAX) NULL,
    [ErrorSeverity]        INT            NULL);

