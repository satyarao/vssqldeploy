﻿CREATE TYPE [dbo].[BasketPaymentType] AS TABLE (
    [BasketID]            UNIQUEIDENTIFIER NULL,
    [UserPaymentSourceID] INT              NULL,
    [PaymentCommand]      NVARCHAR (50)    NULL,
    [Amount]              DECIMAL (9, 2)   NULL,
    [AuthCode]            NVARCHAR (50)    NULL,
    [ResponseResult]      NVARCHAR (50)    NULL,
    [ResponseResultText]  NVARCHAR (500)   NULL,
    [RefData]             NVARCHAR (MAX)   NULL,
    [RecNum]              NVARCHAR (50)    NULL,
    [CreatedOn]           DATETIME2 (7)    NULL,
    [IsSiteLevelPayment]  BIT              NULL);

