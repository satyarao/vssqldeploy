﻿CREATE TYPE [dbo].[MessageDescriptionType] AS TABLE (
    [StoreID]     UNIQUEIDENTIFIER NOT NULL,
    [Action]      NVARCHAR (256)   NOT NULL,
    [Version]     INT              NOT NULL,
    [IsEncrypted] BIT              NOT NULL,
    [IsActive]    BIT              NOT NULL);

