﻿CREATE TABLE [dbo].[LKStoreInstallType] (
    [StoreInstallTypeID]   INT            IDENTITY (1, 1) NOT NULL,
    [StoreInstallTypeName] NVARCHAR (150) NOT NULL,
    [IsActive]             BIT            CONSTRAINT [DF_LKStoreInstallType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKStoreInstallType] PRIMARY KEY CLUSTERED ([StoreInstallTypeID] ASC) WITH (FILLFACTOR = 80)
);

