﻿CREATE TABLE [dbo].[ChangeTrackerVersion] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [ChangeTracker] NVARCHAR (50) NOT NULL,
    [Version]       INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

