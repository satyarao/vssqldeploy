﻿CREATE TABLE [dbo].[BasketPayment] (
    [BasketPaymentID]     INT              IDENTITY (1, 1) NOT NULL,
    [BasketID]            UNIQUEIDENTIFIER NOT NULL,
    [UserPaymentSourceID] INT              NOT NULL,
    [PaymentCommand]      NVARCHAR (50)    NOT NULL,
    [Amount]              DECIMAL (9, 2)   NOT NULL,
    [AuthCode]            NVARCHAR (50)    NULL,
    [ResponseResult]      NVARCHAR (50)    NOT NULL,
    [ResponseResultText]  NVARCHAR (500)   NULL,
    [RefData]             NVARCHAR (MAX)   NULL,
    [RecNum]              NVARCHAR (50)    NULL,
    [CreatedOn]           DATETIME2 (7)    CONSTRAINT [DF_BasketPayment_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsSiteLevelPayment]  BIT              CONSTRAINT [DF__BasketPay__IsSit__0CA474FA] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BasketPayment] PRIMARY KEY CLUSTERED ([BasketPaymentID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_BasketPayment_Basket] FOREIGN KEY ([BasketID]) REFERENCES [dbo].[Basket] ([BasketID]),
    CONSTRAINT [FK_BasketPayment_Payment_LKUserPaymentSource] FOREIGN KEY ([UserPaymentSourceID]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID])
);


GO
CREATE NONCLUSTERED INDEX [nci_wi_BasketPayment_F9032F12D40184EB5543AC41642A4753]
    ON [dbo].[BasketPayment]([BasketID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_BasketPayment_BasketID_IsSiteLevelPayment]
    ON [dbo].[BasketPayment]([BasketID] ASC, [IsSiteLevelPayment] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_BasketPayment_PaymentCommand_ResponseResult]
    ON [dbo].[BasketPayment]([PaymentCommand] ASC, [ResponseResult] ASC) WITH (FILLFACTOR = 80);

