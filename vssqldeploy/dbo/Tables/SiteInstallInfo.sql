﻿CREATE TABLE [dbo].[SiteInstallInfo] (
    [SiteInstallInfoID]  INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]           UNIQUEIDENTIFIER NULL,
    [StoreID]            UNIQUEIDENTIFIER NULL,
    [IsSelfServe]        BIT              CONSTRAINT [DF_SiteInstallInfo_IsSelfServe] DEFAULT ((1)) NOT NULL,
    [IsInStoreEnabled]   BIT              CONSTRAINT [DF_SiteInstallInfo_IsInStoreEnabled] DEFAULT ((0)) NOT NULL,
    [ForecourtIP]        NVARCHAR (64)    NULL,
    [SecondNetworkCom]   NVARCHAR (16)    NULL,
    [VirtualPrinterPort] NVARCHAR (16)    NULL,
    [DispenserTypeID]    TINYINT          NULL,
    [POSTypeID]          TINYINT          NULL,
    [FCTypeID]           TINYINT          NULL,
    [IsActive]           BIT              CONSTRAINT [DF_SiteInstallInfo_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_SiteInstallInfo_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]          DATETIME2 (7)    CONSTRAINT [DF_SiteInstallInfo_UpdatedOn] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]          NVARCHAR (128)   CONSTRAINT [DF_SiteInstallInfo_CreatedBy] DEFAULT (N'SP') NOT NULL,
    [UpdatedBy]          NVARCHAR (128)   NULL,
    CONSTRAINT [PK__SiteInst__8DC19D9ABE348618] PRIMARY KEY CLUSTERED ([SiteInstallInfoID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__SiteInsta__Store__37661AB1] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID]),
    CONSTRAINT [FK__SiteInsta__Tenan__385A3EEA] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

