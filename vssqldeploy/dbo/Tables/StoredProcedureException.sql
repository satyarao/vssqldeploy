﻿CREATE TABLE [dbo].[StoredProcedureException] (
    [ErrorProcedure]       NVARCHAR (255) NULL,
    [ErrorLine]            INT            NULL,
    [ErrorNumber]          INT            NULL,
    [ErrorMessage]         NVARCHAR (255) NULL,
    [StoredProcedureInput] NVARCHAR (MAX) NULL,
    [ErrorSeverity]        INT            NULL,
    [CreatedOn]            DATETIME2 (7)  CONSTRAINT [DF_StoredProcedureException_CreatedOn] DEFAULT (getutcdate()) NOT NULL
);


GO
CREATE CLUSTERED INDEX [IDX_StoredProcedureException_CreatedOn]
    ON [dbo].[StoredProcedureException]([CreatedOn] DESC);

