﻿CREATE TABLE [dbo].[AcceptanceStatuses] (
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [TermsConditionsId] UNIQUEIDENTIFIER NOT NULL,
    [AcceptanceStatus]  INT              NOT NULL,
    [StatusDateTime]    DATETIME         NULL,
    [IsActive]          BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_AcceptanceStatuses_UserId] PRIMARY KEY CLUSTERED ([UserId] ASC, [TermsConditionsId] ASC),
    CONSTRAINT [FK_AcceptanceStatuses_TermsAndConditionsDetails_TermsConditionsId] FOREIGN KEY ([TermsConditionsId]) REFERENCES [dbo].[TermsAndConditionsDetails] ([TermsConditionsId]) ON DELETE CASCADE,
    CONSTRAINT [FK_AcceptanceStatuses_UserInfo_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);

