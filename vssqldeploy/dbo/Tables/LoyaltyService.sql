﻿CREATE TABLE [dbo].[LoyaltyService] (
    [ProgramId]     NVARCHAR (16)    NOT NULL,
    [ProgramName]   NVARCHAR (128)   NOT NULL,
    [ServiceStatus] NVARCHAR (128)   NOT NULL,
    [StoreId]       UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LoyaltyService] PRIMARY KEY CLUSTERED ([ProgramId] ASC, [StoreId] ASC),
    CONSTRAINT [FK_LoyaltyService_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreID]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[LoyaltyService] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);


GO
CREATE NONCLUSTERED INDEX [IX_LoyaltyService_StoreId]
    ON [dbo].[LoyaltyService]([StoreId] ASC);

