﻿CREATE TABLE [dbo].[HoursOfOperation] (
    [StoreID]        UNIQUEIDENTIFIER NOT NULL,
    [SundayOpen]     TIME (0)         NULL,
    [SundayClose]    TIME (0)         NULL,
    [MondayOpen]     TIME (0)         NULL,
    [MondayClose]    TIME (0)         NULL,
    [TuesdayOpen]    TIME (0)         NULL,
    [TuesdayClose]   TIME (0)         NULL,
    [WednesdayOpen]  TIME (0)         NULL,
    [WednesdayClose] TIME (0)         NULL,
    [ThursdayOpen]   TIME (0)         NULL,
    [ThursdayClose]  TIME (0)         NULL,
    [FridayOpen]     TIME (0)         NULL,
    [FridayClose]    TIME (0)         NULL,
    [SaturdayOpen]   TIME (0)         NULL,
    [SaturdayClose]  TIME (0)         NULL,
    [IsActive]       BIT              CONSTRAINT [DF_HoursOfOperation_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy]      NVARCHAR (50)    NULL,
    [CreatedOn]      DATETIME2 (7)    CONSTRAINT [DF_HoursOfOperation_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]      NVARCHAR (50)    NULL,
    [UpdatedOn]      DATETIME2 (7)    NULL,
    CONSTRAINT [PK_HoursOfOperation] PRIMARY KEY CLUSTERED ([StoreID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_HoursOfOperation_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);


GO
ALTER TABLE [dbo].[HoursOfOperation] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

