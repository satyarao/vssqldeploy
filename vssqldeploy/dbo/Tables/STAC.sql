﻿CREATE TABLE [dbo].[STAC] (
    [Token]               UNIQUEIDENTIFIER CONSTRAINT [DF_SingleUsePaymentToken_Token] DEFAULT (newid()) NOT NULL,
    [UserPaymentSourceID] INT              NULL,
    [StoreID]             UNIQUEIDENTIFIER NULL,
    [Amount]              DECIMAL (9, 2)   NULL,
    [Expiration]          DATETIME2 (7)    NOT NULL,
    [TokenStatusID]       INT              NOT NULL,
    CONSTRAINT [PK_SingleUsePaymentToken] PRIMARY KEY CLUSTERED ([Token] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_SingleUsePaymentToken_UserPaymentSource] FOREIGN KEY ([TokenStatusID]) REFERENCES [dbo].[LKTokenStatus] ([ID])
);

