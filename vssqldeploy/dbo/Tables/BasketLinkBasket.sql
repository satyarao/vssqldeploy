﻿CREATE TABLE [dbo].[BasketLinkBasket] (
    [BasketID1] UNIQUEIDENTIFIER NOT NULL,
    [BasketID2] UNIQUEIDENTIFIER NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_BasketLinkBasket_CreatedOn] DEFAULT (getutcdate()) NOT NULL
);

