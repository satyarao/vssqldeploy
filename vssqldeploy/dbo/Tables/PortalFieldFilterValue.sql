﻿CREATE TABLE [dbo].[PortalFieldFilterValue] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [PortalFieldFilterID] INT            NOT NULL,
    [FilterFieldValue]    NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_PortalFieldFilterValue] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PortalFieldFilterValue_PortalFieldFilter_Cascade] FOREIGN KEY ([PortalFieldFilterID]) REFERENCES [dbo].[PortalFieldFilter] ([ID]) ON DELETE CASCADE
);

