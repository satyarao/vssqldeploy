﻿CREATE TABLE [dbo].[ReportTransactionMap] (
    [ID]                        INT              IDENTITY (1, 1) NOT NULL,
    [P97TransactionId]          UNIQUEIDENTIFIER NULL,
    [ReportTransactionIdLength] INT              NOT NULL,
    [ReportTransactionId]       AS               ([dbo].[SetReportTransactionId]([ID],[ReportTransactionIdLength])),
    PRIMARY KEY CLUSTERED ([ID] ASC),
    UNIQUE NONCLUSTERED ([P97TransactionId] ASC)
);

