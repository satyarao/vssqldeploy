﻿CREATE TABLE [dbo].[MxConfiguration] (
    [StoreID]  UNIQUEIDENTIFIER CONSTRAINT [DF_MxConfiguration_StoreID] DEFAULT (newid()) NOT NULL,
    [Domain]   NVARCHAR (MAX)   NULL,
    [AuthType] NVARCHAR (MAX)   NOT NULL,
    [Username] NVARCHAR (MAX)   NOT NULL,
    [Password] NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_MxConfiguration] PRIMARY KEY CLUSTERED ([StoreID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_MxConfiguration_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);

