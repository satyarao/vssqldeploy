﻿CREATE TABLE [dbo].[TermsAndConditionsDetails] (
    [TermsConditionsId] UNIQUEIDENTIFIER NOT NULL,
    [ApplicationId]     UNIQUEIDENTIFIER NULL,
    [ServiceProviderId] INT              NOT NULL,
    [HostingType]       INT              NOT NULL,
    [Version]           NVARCHAR (30)    NOT NULL,
    [DateTime]          DATETIME         NOT NULL,
    [Url]               NVARCHAR (1000)  NULL,
    [Title]             NVARCHAR (500)   NULL,
    [Preamble]          NVARCHAR (3000)  NULL,
    [IsActive]          BIT              CONSTRAINT [DF_TermsAndConditionsDetails_IsActive] DEFAULT ((1)) NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_TermsAndConditionsDetails] PRIMARY KEY CLUSTERED ([TermsConditionsId] ASC),
    CONSTRAINT [FK_TermsAndConditionsDetails_Application] FOREIGN KEY ([ApplicationId]) REFERENCES [AccessControl].[Application] ([ApplicationID]),
    CONSTRAINT [FK_TermsAndConditionsDetails_ServiceProviders] FOREIGN KEY ([ServiceProviderId]) REFERENCES [dbo].[ServiceProvider] ([ServiceProviderId]) ON DELETE CASCADE,
    CONSTRAINT [FK_TermsAndConditionsDetails_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantID])
);

