﻿CREATE TABLE [dbo].[FleetDriverLicense] (
    [Id]            INT              IDENTITY (1, 1) NOT NULL,
    [UserId]        UNIQUEIDENTIFIER NOT NULL,
    [TenantId]      UNIQUEIDENTIFIER NOT NULL,
    [LicenseNumber] NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_dbo.FleetDriverLicense] PRIMARY KEY CLUSTERED ([Id] ASC)
);

