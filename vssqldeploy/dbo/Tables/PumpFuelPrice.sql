﻿CREATE TABLE [dbo].[PumpFuelPrice] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [FuelPriceStoreId] UNIQUEIDENTIFIER NOT NULL,
    [PCATSCode]        NVARCHAR (20)    NOT NULL,
    [PumpStoreId]      UNIQUEIDENTIFIER NOT NULL,
    [PumpNumber]       INT              NOT NULL,
    CONSTRAINT [PK_PumpFuelPrice] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PumpFuelPrice_FuelPrice] FOREIGN KEY ([FuelPriceStoreId], [PCATSCode]) REFERENCES [dbo].[FuelPrice] ([StoreID], [PCATSCode]) ON DELETE CASCADE,
    CONSTRAINT [FK_PumpFuelPrice_Pump] FOREIGN KEY ([PumpStoreId], [PumpNumber]) REFERENCES [dbo].[Pump] ([StoreId], [PumpNumber]) ON DELETE CASCADE,
    CONSTRAINT [FK_PumpFuelPrice_Store_FuelPriceFtoreId] FOREIGN KEY ([FuelPriceStoreId]) REFERENCES [dbo].[Store] ([StoreID]),
    CONSTRAINT [FK_PumpFuelPrice_Store_PumpStoreId] FOREIGN KEY ([PumpStoreId]) REFERENCES [dbo].[Store] ([StoreID])
);


GO
CREATE NONCLUSTERED INDEX [IX_PumpFuelPrice_StoreId_PumpNumber]
    ON [dbo].[PumpFuelPrice]([PumpStoreId] ASC, [PumpNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PumpFuelPrice_StoreId_PCATSCode]
    ON [dbo].[PumpFuelPrice]([FuelPriceStoreId] ASC, [PCATSCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PumpFuelPrice_PumpStoreId]
    ON [dbo].[PumpFuelPrice]([PumpStoreId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PumpFuelPrice_FuelPriceStoreId]
    ON [dbo].[PumpFuelPrice]([FuelPriceStoreId] ASC);

