﻿CREATE TABLE [dbo].[RuleFilterCondition] (
    [ConditionID]               UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID]              UNIQUEIDENTIFIER NOT NULL,
    [Name]                      NVARCHAR (100)   NOT NULL,
    [Description]               NVARCHAR (100)   NOT NULL,
    [RuleFilterConditionTypeID] TINYINT          NOT NULL,
    [Value]                     NVARCHAR (1024)  NOT NULL,
    [Discriminator]             NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_RuleFilterCondition] PRIMARY KEY CLUSTERED ([ConditionID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_RuleFilterCondition_LKRuleFilterConditionType] FOREIGN KEY ([RuleFilterConditionTypeID]) REFERENCES [dbo].[LKRuleFilterConditionType] ([RuleFilterConditionTypeID]),
    CONSTRAINT [FK_RuleFilterCondition_RuleFilter] FOREIGN KEY ([RuleFilterID]) REFERENCES [dbo].[RuleFilter] ([RuleFilterID]) ON DELETE CASCADE
);

