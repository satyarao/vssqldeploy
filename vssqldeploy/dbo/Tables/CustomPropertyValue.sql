﻿CREATE TABLE [dbo].[CustomPropertyValue] (
    [ID]         INT            IDENTITY (1, 1) NOT NULL,
    [PropertyID] INT            NOT NULL,
    [TargetID]   NVARCHAR (150) NOT NULL,
    [Value]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_PropertyIDTargetID] PRIMARY KEY CLUSTERED ([PropertyID] ASC, [TargetID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PropertyValue_Property] FOREIGN KEY ([PropertyID]) REFERENCES [dbo].[CustomProperty] ([ID]),
    CONSTRAINT [UK_PropertyID_TargetID] UNIQUE NONCLUSTERED ([PropertyID] ASC, [TargetID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[CustomPropertyValue] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

