﻿CREATE TABLE [dbo].[DbVersion] (
    [Version]   NVARCHAR (256) NOT NULL,
    [UpdatedBy] NVARCHAR (128) NOT NULL,
    [UpdatedOn] DATETIME2 (7)  NOT NULL
);

