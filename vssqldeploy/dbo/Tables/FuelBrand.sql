﻿CREATE TABLE [dbo].[FuelBrand] (
    [FuelBrandID] INT            NOT NULL,
    [Name]        NVARCHAR (MAX) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_FuelBrand_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy]   NVARCHAR (50)  NULL,
    [CreatedOn]   DATETIME2 (7)  CONSTRAINT [DF_FuelBrand_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]   NVARCHAR (50)  NULL,
    [UpdatedOn]   DATETIME2 (7)  NULL,
    CONSTRAINT [PK_FuelBrand] PRIMARY KEY CLUSTERED ([FuelBrandID] ASC) WITH (FILLFACTOR = 80)
);

