﻿CREATE TABLE [dbo].[LKNotificationMethod] (
    [NotificationMethodID] INT            NOT NULL,
    [Description]          NVARCHAR (MAX) NOT NULL,
    [IsActive]             BIT            NOT NULL,
    CONSTRAINT [PK_dbo.LKNotificationMethod] PRIMARY KEY CLUSTERED ([NotificationMethodID] ASC) WITH (FILLFACTOR = 80)
);

