﻿CREATE TABLE [dbo].[EmailSubscription] (
    [EmailSubscriptionID]  INT              IDENTITY (1, 1) NOT NULL,
    [UserID]               UNIQUEIDENTIFIER NOT NULL,
    [SubscriptionType]     NVARCHAR (255)   NOT NULL,
    [TenantID]             UNIQUEIDENTIFIER NULL,
    [IsDaily]              BIT              CONSTRAINT [EmailSubscription_IsDaily_Default] DEFAULT ((0)) NOT NULL,
    [IsWeekly]             BIT              CONSTRAINT [EmailSubscription_IsWeekly_Default] DEFAULT ((0)) NOT NULL,
    [IsMonthly]            BIT              CONSTRAINT [EmailSubscription_IsMonthly_Default] DEFAULT ((0)) NOT NULL,
    [IsAttachmentRequired] BIT              NULL,
    [CreatedOn]            DATETIME2 (7)    NULL,
    [UpdatedOn]            DATETIME2 (7)    NULL,
    CONSTRAINT [PK_JiraIncident] PRIMARY KEY CLUSTERED ([EmailSubscriptionID] ASC),
    CONSTRAINT [FK_EmailSubscription_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_EmailSubscription_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

