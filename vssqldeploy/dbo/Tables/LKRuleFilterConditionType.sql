﻿CREATE TABLE [dbo].[LKRuleFilterConditionType] (
    [RuleFilterConditionTypeID] TINYINT       NOT NULL,
    [Name]                      NVARCHAR (50) NOT NULL,
    [IsActive]                  BIT           CONSTRAINT [DF_LKRuleFilterConditionType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKRuleFilterConditionType] PRIMARY KEY CLUSTERED ([RuleFilterConditionTypeID] ASC)
);

