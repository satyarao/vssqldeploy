﻿CREATE TABLE [dbo].[TermsOfUse] (
    [TermsOfUseID] INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]     UNIQUEIDENTIFIER NOT NULL,
    [DateVersion]  DATETIME         CONSTRAINT [DF_TermsOfUse_DateVersion] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_TermsOfUse] PRIMARY KEY CLUSTERED ([TermsOfUseID] ASC),
    CONSTRAINT [FK_TermsOfUse_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);


GO
CREATE NONCLUSTERED INDEX [IX_TermsOfUse_DateVersion]
    ON [dbo].[TermsOfUse]([DateVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TermsOfUse_TenantID]
    ON [dbo].[TermsOfUse]([TenantID] ASC);

