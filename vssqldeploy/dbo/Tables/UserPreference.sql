﻿CREATE TABLE [dbo].[UserPreference] (
    [UserID]              UNIQUEIDENTIFIER NOT NULL,
    [PaperReceipt]        BIT              CONSTRAINT [DF_UserPreference_PromptReceipt] DEFAULT ((1)) NOT NULL,
    [EmailReceipt]        BIT              CONSTRAINT [DF_UserPreference_EmailReceipt] DEFAULT ((0)) NOT NULL,
    [NotificationReceipt] BIT              CONSTRAINT [DF_UserPreference_NotificationReceipt] DEFAULT ((0)) NOT NULL,
    [FuelGrade]           NVARCHAR (50)    CONSTRAINT [DF_UserPreference_FuelGrade] DEFAULT (N'R') NULL,
    [IsActive]            BIT              CONSTRAINT [DF_UserPreference_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]           DATETIME2 (7)    CONSTRAINT [DF_UserPreference_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [UpdatedOn]           DATETIME2 (7)    NULL,
    [CreatedBy]           NVARCHAR (50)    NULL,
    [UpdatedBy]           NVARCHAR (50)    NULL,
    [OptInMarketingEmail] BIT              CONSTRAINT [DF__UserPrefe__OptIn__1FDA725B] DEFAULT ((0)) NOT NULL,
    [Sms]                 BIT              DEFAULT ((0)) NOT NULL,
    [OptInMarketingSms]   BIT              DEFAULT ((0)) NOT NULL,
    [Push]                BIT              DEFAULT ((1)) NOT NULL,
    [OptInMarketingPush]  BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_UserPreference_1] PRIMARY KEY CLUSTERED ([UserID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserPreference_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

