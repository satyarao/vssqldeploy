﻿CREATE TABLE [dbo].[UserGeoLocationLog] (
    [LocationEventID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserID]          UNIQUEIDENTIFIER NULL,
    [ChannelID]       UNIQUEIDENTIFIER NULL,
    [Latitude]        DECIMAL (9, 6)   NOT NULL,
    [Longitude]       DECIMAL (9, 6)   NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_UserGeoLocationLog_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [EventType]       NVARCHAR (100)   CONSTRAINT [DF_UserGeoLocationLog_EventType] DEFAULT ('App Home') NOT NULL,
    CONSTRAINT [PK_UserGeoLocationLog] PRIMARY KEY CLUSTERED ([LocationEventID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_UserGeoLocationLog_UserID]
    ON [dbo].[UserGeoLocationLog]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_UserGeoLocationLog_CreatedOn]
    ON [dbo].[UserGeoLocationLog]([CreatedOn] ASC);

