﻿CREATE TABLE [dbo].[Tenant] (
    [TenantID]                    UNIQUEIDENTIFIER    CONSTRAINT [DF_Company_Id] DEFAULT (newid()) NOT NULL,
    [Name]                        NVARCHAR (255)      NOT NULL,
    [OrgID]                       [sys].[hierarchyid] NULL,
    [OrgLevel]                    AS                  ([OrgID].[GetLevel]()) PERSISTED,
    [OrgIDString]                 AS                  ([OrgID].[ToString]()),
    [TenantTypeID]                INT                 NOT NULL,
    [ContactID]                   UNIQUEIDENTIFIER    NULL,
    [MerchantID]                  NVARCHAR (50)       NULL,
    [IsActive]                    BIT                 NOT NULL,
    [CreatedBy]                   NVARCHAR (128)      NULL,
    [CreatedOn]                   DATETIME2 (7)       CONSTRAINT [DF_Company_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                   NVARCHAR (128)      NULL,
    [UpdatedOn]                   DATETIME2 (7)       NULL,
    [ActiveAlertsMaintenanceMode] BIT                 CONSTRAINT [DF_Tenant_ActiveAlertsLockout] DEFAULT ((0)) NOT NULL,
    [TotalLicenseCount]           INT                 NULL,
    [DistributionChannels]        NVARCHAR (MAX)      CONSTRAINT [DF_Tenant_DistributionChannels] DEFAULT ('[]') NULL,
    [HeaderLogo]                  NVARCHAR (MAX)      NULL,
    [FooterLogo]                  NVARCHAR (MAX)      NULL,
    CONSTRAINT [PK_dbo.Company] PRIMARY KEY CLUSTERED ([TenantID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [DistributionChannels_should_be_JSON] CHECK (isjson([DistributionChannels])>(0)),
    CONSTRAINT [FK_Tenant_LKTenantType] FOREIGN KEY ([TenantTypeID]) REFERENCES [dbo].[LKTenantType] ([TenantTypeID]),
    CONSTRAINT [UK_TenantName] UNIQUE NONCLUSTERED ([Name] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [tenant_idx_orgID]
    ON [dbo].[Tenant]([OrgID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [tenant_idx_orgLevel_orgID]
    ON [dbo].[Tenant]([OrgLevel] ASC, [OrgID] ASC) WITH (FILLFACTOR = 80);


GO

-- Charles Teague 11/10/2016
-- When we create a tenant we need to enter initial data for app features and strings

CREATE TRIGGER Tenant_Insert 
   ON  Tenant 
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @insertedTenantID AS UNIQUEIDENTIFIER,
	@appVersion AS INT = 1,
	@englishLanuageCode AS char(2) = 'en';

	SELECT @insertedTenantID = Inserted.TenantID FROM INSERTED;
	if not exists (select * from [AppFeatures].[CurrentVersions] where TenantId = @insertedTenantID)
	BEGIN
		INSERT [AppFeatures].[CurrentVersions] (TenantId,Version) VALUES (@insertedTenantID,@appVersion);
	END
	if not exists (select * from Localization.SupportedLanguage WHERE TenantId = @insertedTenantID)
	BEGIN
		INSERT Localization.SupportedLanguage (SupportedLanguageId, TenantId) VALUES (@englishLanuageCode,@insertedTenantID);
	END 

END

