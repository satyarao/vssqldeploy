﻿CREATE TABLE [dbo].[StoreServiceStatusLog] (
    [ID]          BIGINT           IDENTITY (1, 1) NOT NULL,
    [StoreId]     UNIQUEIDENTIFIER NOT NULL,
    [ActualDate]  DATETIME2 (7)    NOT NULL,
    [ServiceType] INT              NOT NULL,
    [ServiceName] NVARCHAR (256)   NULL,
    [Status]      NVARCHAR (MAX)   NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [SiteServiceStatusLog_StoreId]
    ON [dbo].[StoreServiceStatusLog]([StoreId] ASC)
    INCLUDE([ID], [ServiceType], [ServiceName], [Status]);


GO
CREATE NONCLUSTERED INDEX [SiteServiceStatusLog_ActualDate]
    ON [dbo].[StoreServiceStatusLog]([ServiceType] ASC, [StoreId] ASC, [ServiceName] ASC, [ActualDate] DESC)
    INCLUDE([ID]);

