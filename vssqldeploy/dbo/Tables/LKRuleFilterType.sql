﻿CREATE TABLE [dbo].[LKRuleFilterType] (
    [RuleFilterTypeID] TINYINT       NOT NULL,
    [Name]             NVARCHAR (50) NOT NULL,
    [IsActive]         BIT           CONSTRAINT [DF_LKRuleFilterType_IsActive] DEFAULT ((1)) NOT NULL,
    [IsOfferTrigger]   BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LKRuleFilterType] PRIMARY KEY CLUSTERED ([RuleFilterTypeID] ASC)
);

