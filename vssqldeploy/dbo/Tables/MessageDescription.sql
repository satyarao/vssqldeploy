﻿CREATE TABLE [dbo].[MessageDescription] (
    [StoreID]     UNIQUEIDENTIFIER NOT NULL,
    [Action]      NVARCHAR (256)   NOT NULL,
    [Version]     INT              NOT NULL,
    [IsEncrypted] BIT              NOT NULL,
    [IsActive]    BIT              NOT NULL,
    [CreatedOn]   DATETIME2 (7)    NOT NULL,
    [CreatedBy]   NVARCHAR (50)    NULL,
    [UpdatedOn]   DATETIME2 (7)    NULL,
    [UpdatedBy]   NVARCHAR (50)    NULL,
    CONSTRAINT [PK_MessageDescription_1] PRIMARY KEY CLUSTERED ([StoreID] ASC, [Action] ASC, [Version] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_MessageDescription_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);

