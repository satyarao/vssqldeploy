﻿CREATE TABLE [dbo].[ConfigNPCA] (
    [ConfigID]     INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]     UNIQUEIDENTIFIER NOT NULL,
    [MerchantID]   NVARCHAR (50)    NOT NULL,
    [StoreName]    NVARCHAR (30)    NULL,
    [StoreID]      NVARCHAR (50)    NOT NULL,
    [APIVersion]   NVARCHAR (10)    NOT NULL,
    [URI]          NVARCHAR (250)   NULL,
    [AuthUserName] NVARCHAR (50)    NULL,
    [AuthPassword] VARCHAR (MAX)    NULL,
    [IsActive]     BIT              CONSTRAINT [DF_ConfigNPCA_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME2 (7)    CONSTRAINT [DF_ConfigNPCA_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]    DATETIME2 (7)    NULL,
    CONSTRAINT [PK_ConfigNPCA_TenantID] PRIMARY KEY CLUSTERED ([TenantID] ASC) WITH (FILLFACTOR = 80)
);

