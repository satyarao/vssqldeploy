﻿CREATE TABLE [dbo].[LKTokenStatus] (
    [ID]          INT            NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_LKTokenStatus] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

