﻿CREATE TABLE [dbo].[UserPin] (
    [UserPinID]          INT              IDENTITY (1, 1) NOT NULL,
    [UserID]             UNIQUEIDENTIFIER NOT NULL,
    [PinCode]            NVARCHAR (50)    NOT NULL,
    [OneTimeCode]        NVARCHAR (256)   NULL,
    [ExpirationDate]     DATETIME2 (7)    NULL,
    [IsActive]           BIT              NOT NULL,
    [CreatedBy]          NVARCHAR (50)    NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_UserPin_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]          NVARCHAR (50)    NULL,
    [UpdatedOn]          DATETIME2 (7)    NULL,
    [MissedPinCodeCount] INT              DEFAULT ((0)) NOT NULL,
    [UserPinStatusID]    TINYINT          DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_UserPin] PRIMARY KEY CLUSTERED ([UserPinID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserPin_LKUserPinStatus] FOREIGN KEY ([UserPinStatusID]) REFERENCES [dbo].[LKUserPinStatus] ([UserPinStatusID]),
    CONSTRAINT [FK_UserPin_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

