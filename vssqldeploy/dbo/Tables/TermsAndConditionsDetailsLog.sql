﻿CREATE TABLE [dbo].[TermsAndConditionsDetailsLog] (
    [DetailLogId]       INT              IDENTITY (1, 1) NOT NULL,
    [TermsConditionsId] UNIQUEIDENTIFIER NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NULL,
    [ApplicationId]     UNIQUEIDENTIFIER NULL,
    [HostingType]       INT              NOT NULL,
    [Version]           NVARCHAR (30)    NOT NULL,
    [DateTime]          DATETIME         NOT NULL,
    [ServiceProviderId] INT              NOT NULL,
    [Url]               NVARCHAR (1000)  NULL,
    [Title]             NVARCHAR (500)   NULL,
    [Preamble]          NVARCHAR (3000)  NULL,
    CONSTRAINT [PK_TermsAndConditionsDetailsLog_DetailLogId] PRIMARY KEY CLUSTERED ([DetailLogId] ASC),
    CONSTRAINT [FK_TermsAndConditionsDetailsLog_TermsAndConditionsDetails_TermsConditionsId] FOREIGN KEY ([TermsConditionsId]) REFERENCES [dbo].[TermsAndConditionsDetails] ([TermsConditionsId]) ON DELETE CASCADE
);

