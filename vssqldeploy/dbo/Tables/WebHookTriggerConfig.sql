﻿CREATE TABLE [dbo].[WebHookTriggerConfig] (
    [ApplicationId]             UNIQUEIDENTIFIER NOT NULL,
    [AppTenantId]               UNIQUEIDENTIFIER NOT NULL,
    [StoreTenantId]             UNIQUEIDENTIFIER NOT NULL,
    [SendToAppTenant]           BIT              NOT NULL,
    [SendToStoreTenant]         BIT              NOT NULL,
    [SendToApplicationProvider] BIT              NOT NULL,
    [EventType]                 INT              NOT NULL,
    CONSTRAINT [WebHookTriggerConfigKey] PRIMARY KEY CLUSTERED ([ApplicationId] ASC, [AppTenantId] ASC, [StoreTenantId] ASC, [EventType] ASC)
);

