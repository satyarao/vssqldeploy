﻿CREATE TABLE [dbo].[SiteBootStrapRecord] (
    [PartitionKey]     NVARCHAR (50) NULL,
    [RowKey]           NVARCHAR (50) NULL,
    [Timestamp]        NVARCHAR (50) NULL,
    [SiteControllerId] NVARCHAR (50) NULL
);

