﻿CREATE TABLE [dbo].[BasketVersion] (
    [BasketID] UNIQUEIDENTIFIER NOT NULL,
    [Version]  INT              NOT NULL,
    PRIMARY KEY CLUSTERED ([BasketID] ASC) WITH (FILLFACTOR = 80)
);

