﻿CREATE TABLE [dbo].[BasketLineItem] (
    [BasketLineItemID]         UNIQUEIDENTIFIER NOT NULL,
    [BasketID]                 UNIQUEIDENTIFIER NOT NULL,
    [POSLineItemID]            NVARCHAR (50)    NOT NULL,
    [UPC]                      NVARCHAR (50)    NULL,
    [PCATSCode]                NVARCHAR (50)    NULL,
    [UnitOfMeasure]            NVARCHAR (20)    NULL,
    [ItemDescription]          NVARCHAR (255)   NOT NULL,
    [SellingUnit]              DECIMAL (9, 2)   NULL,
    [RegularPrice]             DECIMAL (9, 3)   NULL,
    [SalesPrice]               DECIMAL (9, 3)   NULL,
    [Quantity]                 DECIMAL (9, 3)   NOT NULL,
    [ExtendedPrice]            DECIMAL (9, 3)   NOT NULL,
    [TaxAmount]                DECIMAL (9, 2)   NULL,
    [TaxRate]                  DECIMAL (9, 2)   NULL,
    [ExtendedAmount]           DECIMAL (9, 2)   NOT NULL,
    [LoyaltyRewardID]          NVARCHAR (MAX)   NULL,
    [LoyaltyRewardAmount]      DECIMAL (18, 3)  NULL,
    [BasketLineItemStateID]    INT              CONSTRAINT [DF_BasketLineItem_BasketLineItemStateID] DEFAULT ((1)) NOT NULL,
    [CreatedOn]                DATETIME2 (7)    CONSTRAINT [DF_BasketLineItem_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]                DATETIME2 (7)    NULL,
    [CarWashCode]              NVARCHAR (20)    NULL,
    [CarWashCodeExpiration]    DATETIME2 (7)    NULL,
    [LoyaltyDescription]       NVARCHAR (MAX)   NULL,
    [LoyaltyRewardUnitAmount]  DECIMAL (9, 2)   NULL,
    [LoyaltyRewardQuantity]    DECIMAL (9, 3)   NULL,
    [LoyaltyRewardSellingUnit] DECIMAL (9, 2)   NULL,
    [CarWashText]              NVARCHAR (255)   NULL,
    [OtherLoyaltyRewardAmount] DECIMAL (9, 2)   NULL,
    [P97RewardDiscount]        DECIMAL (9, 2)   DEFAULT (NULL) NULL,
    [ExternalRewardDiscount]   DECIMAL (9, 2)   DEFAULT (NULL) NULL,
    CONSTRAINT [PK_BasketLineItem] PRIMARY KEY CLUSTERED ([BasketLineItemID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_BasketLineItem_Basket] FOREIGN KEY ([BasketID]) REFERENCES [dbo].[Basket] ([BasketID]),
    CONSTRAINT [FK_BasketLineItem_LKBasketLineItemState] FOREIGN KEY ([BasketLineItemStateID]) REFERENCES [dbo].[LKBasketLineItemState] ([BasketLineItemStateID])
);


GO
CREATE NONCLUSTERED INDEX [nci_wi_BasketLineItem_6B971BE5B1198F450DE44E4593F88954]
    ON [dbo].[BasketLineItem]([BasketID] ASC)
    INCLUDE([BasketLineItemStateID], [CarWashCode], [CarWashCodeExpiration], [ExtendedAmount], [ExtendedPrice], [ItemDescription], [LoyaltyRewardAmount], [LoyaltyRewardID], [PCATSCode], [POSLineItemID], [Quantity], [RegularPrice], [SalesPrice], [SellingUnit], [TaxAmount], [TaxRate], [UnitOfMeasure], [UPC], [LoyaltyRewardUnitAmount], [LoyaltyRewardQuantity], [LoyaltyRewardSellingUnit]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_BasketLineItem_BasketID_BasketLineItemID]
    ON [dbo].[BasketLineItem]([BasketID] ASC, [BasketLineItemID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_BasketLineItem_CreatedOn]
    ON [dbo].[BasketLineItem]([CreatedOn] ASC) WITH (FILLFACTOR = 80);


GO

CREATE TRIGGER BasketLineItem_LoyaltyDescription_UPDATE
ON BasketLineItem
AFTER UPDATE
AS
BEGIN
	DELETE 
	FROM Reports.BasketLineItem_LoyaltyDescription 
	WHERE EXISTS (SELECT * FROM INSERTED WHERE INSERTED.BasketLineItemID = Reports.BasketLineItem_LoyaltyDescription.BasketLineItemID )

	INSERT INTO Reports.BasketLineItem_LoyaltyDescription (BasketLineItemID, OfferId, OfferAmount, OfferTenantId, OfferTitle, OfferDiscount)
	SELECT b.BasketLineItemID, pt.OfferId, pt.OfferAmount, pt.OfferTenantId, pt.OfferTitle, pt.OfferDiscount from INSERTED b
	CROSS APPLY OPENJSON( (b.LoyaltyDescription)) WITH (OfferId nvarchar(max), OfferTenantId uniqueidentifier,  OfferTitle nvarchar(max), OfferDiscount decimal(9,2), OfferAmount decimal(9,2)) as pt
END

GO
CREATE TRIGGER [dbo].[BasketLineItem_Update] ON [dbo].[BasketLineItem]
	FOR UPDATE
AS
SET NOCOUNT ON

INSERT	INTO dbo.BasketLineItemLog
		SELECT	*
		FROM	deleted

GO

CREATE TRIGGER BasketLineItem_LoyaltyDescription_INSERT
ON BasketLineItem
AFTER INSERT
AS
INSERT INTO Reports.BasketLineItem_LoyaltyDescription (BasketLineItemID, OfferId, OfferAmount, OfferTenantId, OfferTitle, OfferDiscount)
SELECT b.BasketLineItemID, pt.OfferId, pt.OfferAmount, pt.OfferTenantId, pt.OfferTitle, pt.OfferDiscount from INSERTED b
CROSS APPLY OPENJSON( (b.LoyaltyDescription)) WITH (OfferId nvarchar(max), OfferTenantId uniqueidentifier,  OfferTitle nvarchar(max), OfferDiscount decimal(9,2), OfferAmount decimal(9,2)) as pt
