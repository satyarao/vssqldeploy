﻿CREATE TABLE [dbo].[UserAccount] (
    [UserAccountID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID]        UNIQUEIDENTIFIER NOT NULL,
    [AccountID]     UNIQUEIDENTIFIER NOT NULL,
    [AccountTypeID] INT              NOT NULL,
    [IsActive]      BIT              CONSTRAINT [DF_UserAccount_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy]     VARCHAR (255)    NULL,
    [CreatedOn]     DATETIME2 (7)    CONSTRAINT [DF_UserAccount_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]     VARCHAR (255)    NULL,
    [UpdatedOn]     DATETIME2 (7)    NULL,
    CONSTRAINT [PK_UserAccount] PRIMARY KEY CLUSTERED ([UserAccountID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserAccount_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID]),
    CONSTRAINT [FK_UserAccount_LKAccountType] FOREIGN KEY ([AccountTypeID]) REFERENCES [dbo].[LKAccountType] ([AccountTypeID]),
    CONSTRAINT [FK_UserAccount_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserAccount]
    ON [dbo].[UserAccount]([UserID] ASC, [AccountID] ASC, [AccountTypeID] ASC) WITH (FILLFACTOR = 80);

