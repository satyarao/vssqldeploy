﻿CREATE TABLE [dbo].[UserTrackLocation] (
    [TrackId]        BIGINT            IDENTITY (1, 1) NOT NULL,
    [UserId]         UNIQUEIDENTIFIER  NOT NULL,
    [Longitude]      DECIMAL (10, 7)   NOT NULL,
    [Latitude]       DECIMAL (10, 7)   NOT NULL,
    [GeoCoordinates] [sys].[geography] NOT NULL,
    [GeoText]        AS                ([GeoCoordinates].[STAsText]()),
    [Timestamp]      DATETIME2 (7)     NOT NULL,
    CONSTRAINT [PK_UserTrackLocation] PRIMARY KEY CLUSTERED ([TrackId] DESC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON)
);

