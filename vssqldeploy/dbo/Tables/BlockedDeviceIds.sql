﻿CREATE TABLE [dbo].[BlockedDeviceIds] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [DeviceId]    NVARCHAR (MAX)   NOT NULL,
    [Source]      NVARCHAR (255)   NOT NULL,
    [IsEncrypted] BIT              NOT NULL,
    [IsUnknown]   BIT              NOT NULL,
    [IsActive]    BIT              NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

