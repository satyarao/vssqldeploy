﻿CREATE TABLE [dbo].[DeviceProperty] (
    [PropertyName]  NVARCHAR (128)   NOT NULL,
    [TargetID]      UNIQUEIDENTIFIER NOT NULL,
    [DeviceID]      NVARCHAR (128)   NOT NULL,
    [PropertyValue] NVARCHAR (250)   NOT NULL,
    CONSTRAINT [PK_DeviceProperty_1] PRIMARY KEY NONCLUSTERED ([PropertyName] ASC, [TargetID] ASC, [DeviceID] ASC),
    CONSTRAINT [FK_DeviceProperty_LKProperty] FOREIGN KEY ([PropertyName]) REFERENCES [dbo].[LKProperty] ([PropertyName])
);

