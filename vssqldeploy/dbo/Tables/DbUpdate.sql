﻿CREATE TABLE [dbo].[DbUpdate] (
    [DbUpdateID] INT            IDENTITY (1, 1) NOT NULL,
    [FileName]   NVARCHAR (128) NOT NULL,
    [FileHash]   NVARCHAR (512) NOT NULL,
    [UpdatedOn]  DATETIME2 (7)  CONSTRAINT [DF_DbUpdate_UpdatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_DbUpdate] PRIMARY KEY CLUSTERED ([DbUpdateID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UC_DbUpdate_FileName] UNIQUE NONCLUSTERED ([FileName] ASC) WITH (FILLFACTOR = 80)
);

