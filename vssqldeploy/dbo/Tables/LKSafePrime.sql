﻿CREATE TABLE [dbo].[LKSafePrime] (
    [SafePrimeID] INT            NOT NULL,
    [SafePrime]   NVARCHAR (MAX) NOT NULL,
    [Generator]   NVARCHAR (MAX) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_LKSafePrime_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKSafePrime] PRIMARY KEY CLUSTERED ([SafePrimeID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE TRIGGER [dbo].[LKSafePrimeUpdate] ON [dbo].[LKSafePrime]
	AFTER INSERT, UPDATE
AS
SET NOCOUNT ON
IF EXISTS ( SELECT	*
			FROM	INSERTED
			WHERE	IsActive = 1 )
	BEGIN
		UPDATE	sp
		SET		IsActive = 0
		FROM	dbo.LKSafePrime sp
		WHERE	sp.IsActive = 1
				AND sp.SafePrimeID <> (SELECT	SafePrimeID
									   FROM		Inserted)
	END
