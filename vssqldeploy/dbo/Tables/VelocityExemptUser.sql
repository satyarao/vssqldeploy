﻿CREATE TABLE [dbo].[VelocityExemptUser] (
    [VelocityExemptUserID]           INT              IDENTITY (1, 1) NOT NULL,
    [UserId]                         UNIQUEIDENTIFIER NOT NULL,
    [TenantId]                       UNIQUEIDENTIFIER NOT NULL,
    [AddCardToWalletVelocityCheck]   BIT              NOT NULL,
    [PurchaseVelocityCheck]          BIT              NOT NULL,
    [PumpReserveVelocityCheck]       BIT              NOT NULL,
    [TnsCofCheck]                    BIT              NOT NULL,
    [TnsMasterPassCheck]             BIT              NOT NULL,
    [TnsVisaCheckoutCheck]           BIT              NOT NULL,
    [InsideStoreQRCodeVelocityCheck] BIT              NOT NULL,
    [InsideQRCodeVelocityCheck]      BIT              NOT NULL,
    [TotalCardsAddedCheck]           BIT              NOT NULL,
    [UseWalletCardVelocityCheck]     BIT              NOT NULL,
    [GetAllUserDataVelocityCheck]    BIT              NOT NULL,
    [NuDetectServiceCheck]           BIT              NOT NULL,
    [OfferMaxRedemptionCheck]        BIT              NOT NULL,
    CONSTRAINT [PK_VelocityExemptUser] PRIMARY KEY NONCLUSTERED ([VelocityExemptUserID] ASC),
    CONSTRAINT [UC_VelocityExemptUser_Person] UNIQUE NONCLUSTERED ([UserId] ASC, [TenantId] ASC)
);

