﻿CREATE TABLE [dbo].[SecurityQuestion] (
    [QuestionID] INT            NOT NULL,
    [GroupID]    INT            NOT NULL,
    [Question]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_SecurityQuestion] PRIMARY KEY CLUSTERED ([QuestionID] ASC) WITH (FILLFACTOR = 80)
);

