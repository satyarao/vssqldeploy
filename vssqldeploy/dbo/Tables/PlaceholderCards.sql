﻿CREATE TABLE [dbo].[PlaceholderCards] (
    [TenantID]      UNIQUEIDENTIFIER NOT NULL,
    [OrdersPayload] NVARCHAR (300)   NOT NULL,
    CONSTRAINT [PK_PlaceholderCards] PRIMARY KEY CLUSTERED ([TenantID] ASC),
    CONSTRAINT [CK_PlaceholderCards_OrdersPayload_IsJson] CHECK (isjson([OrdersPayload])>(0)),
    CONSTRAINT [FK_PlaceholderCards_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

