﻿CREATE TABLE [dbo].[LKDispenserType] (
    [DispenserTypeID]   TINYINT       IDENTITY (1, 1) NOT NULL,
    [DispenserTypeName] NVARCHAR (64) NOT NULL,
    [IsActive]          BIT           CONSTRAINT [DF_LKDispenserType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__LKDispen__9E92844C41F8CAD6] PRIMARY KEY CLUSTERED ([DispenserTypeID] ASC) WITH (FILLFACTOR = 80)
);

