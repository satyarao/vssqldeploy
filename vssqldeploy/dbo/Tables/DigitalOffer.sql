﻿CREATE TABLE [dbo].[DigitalOffer] (
    [OfferID]             UNIQUEIDENTIFIER NOT NULL,
    [TenantID]            UNIQUEIDENTIFIER NOT NULL,
    [OfferStartDate]      DATETIME2 (7)    NOT NULL,
    [OfferExpirationDate] DATETIME2 (7)    NOT NULL,
    [OfferDescription]    NVARCHAR (256)   NULL,
    [ImageURL]            NVARCHAR (MAX)   NULL,
    [IsActive]            BIT              CONSTRAINT [DF_DigitalOffer_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]           DATETIME2 (7)    CONSTRAINT [DF_DigitalOffer_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]           DATETIME2 (7)    CONSTRAINT [DF_DigitalOffer_UpdatedOn] DEFAULT (getutcdate()) NULL,
    [CreatedBy]           NVARCHAR (50)    CONSTRAINT [DF_DigitalOffer_CreatedBy] DEFAULT (N'Manual') NULL,
    [UpdatedBy]           NVARCHAR (50)    CONSTRAINT [DF_DigitalOffer_UpdatedBy] DEFAULT (N'Manual') NULL,
    CONSTRAINT [PK_Offer_1] PRIMARY KEY CLUSTERED ([OfferID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__DigitalOf__Tenan__774B959C] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

