﻿CREATE TABLE [dbo].[SiteInfoLog] (
    [ConfigID]          INT              NOT NULL,
    [StoreID]           UNIQUEIDENTIFIER NOT NULL,
    [MacAddress]        VARCHAR (150)    NOT NULL,
    [IPAddress]         VARCHAR (50)     NULL,
    [HostName]          NVARCHAR (50)    NOT NULL,
    [HeartBeatDateTime] DATETIME2 (7)    NULL,
    [HeartBeatStatus]   NVARCHAR (50)    NULL,
    [InstallDateTime]   DATETIME2 (7)    NOT NULL,
    [RestartDateTime]   DATETIME2 (7)    NOT NULL
);

