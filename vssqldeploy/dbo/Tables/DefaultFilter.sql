﻿CREATE TABLE [dbo].[DefaultFilter] (
    [DefaultFilterID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID]          UNIQUEIDENTIFIER NOT NULL,
    [TenantID]        UNIQUEIDENTIFIER NULL,
    [FilterTypeID]    TINYINT          NOT NULL,
    [FilterID]        INT              NOT NULL,
    [CreatedOn]       DATETIME2 (7)    NOT NULL,
    [UpdatedOn]       DATETIME2 (7)    NULL,
    CONSTRAINT [PK_DefaultFilter] PRIMARY KEY CLUSTERED ([DefaultFilterID] ASC),
    CONSTRAINT [FK_DefaultFilter_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_DefaultFilter_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [UK_User_Tenant_FilterType] UNIQUE NONCLUSTERED ([UserID] ASC, [TenantID] ASC, [FilterTypeID] ASC)
);

