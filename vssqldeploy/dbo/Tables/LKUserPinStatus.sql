﻿CREATE TABLE [dbo].[LKUserPinStatus] (
    [UserPinStatusID] TINYINT       NOT NULL,
    [Name]            NVARCHAR (50) NOT NULL,
    [IsActive]        BIT           CONSTRAINT [DF_LKUserPinStatus_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKUserPinStatus] PRIMARY KEY CLUSTERED ([UserPinStatusID] ASC)
);

