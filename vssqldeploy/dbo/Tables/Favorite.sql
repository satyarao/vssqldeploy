﻿CREATE TABLE [dbo].[Favorite] (
    [FavID]     INT              IDENTITY (1, 1) NOT NULL,
    [UserID]    UNIQUEIDENTIFIER NOT NULL,
    [StoreID]   UNIQUEIDENTIFIER NOT NULL,
    [IsActive]  BIT              NOT NULL,
    [CreatedBy] NVARCHAR (50)    NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_Favorite_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy] NVARCHAR (50)    NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    CONSTRAINT [PK_Favorite] PRIMARY KEY CLUSTERED ([FavID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Favorite_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

