﻿CREATE TABLE [dbo].[JanrainUserAccess] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [UserID]       UNIQUEIDENTIFIER NOT NULL,
    [AccessToken]  NVARCHAR (MAX)   NOT NULL,
    [RefreshToken] NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]    DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [ExpiresOn]    DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_JanrainUserAccess] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_JanrainUserAccess_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

