﻿CREATE TABLE [dbo].[Account] (
    [AccountID] UNIQUEIDENTIFIER CONSTRAINT [DF_Account_Id] DEFAULT (newid()) NOT NULL,
    [Name]      NVARCHAR (255)   NOT NULL,
    [IsActive]  BIT              CONSTRAINT [DF_Account_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_Account_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy] NVARCHAR (50)    NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    [UpdatedBy] NVARCHAR (50)    NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([AccountID] ASC) WITH (FILLFACTOR = 80)
);

