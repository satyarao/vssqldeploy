﻿CREATE TABLE [dbo].[AppData] (
    [ID]       INT              IDENTITY (1, 1) NOT NULL,
    [TenantId] UNIQUEIDENTIFIER NOT NULL,
    [Key]      NVARCHAR (255)   NOT NULL,
    [Value]    NVARCHAR (MAX)   NOT NULL,
    [Platform] NVARCHAR (100)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

