﻿CREATE TABLE [dbo].[PortalsVersion] (
    [Version]   INT            NOT NULL,
    [UpdatedBy] NVARCHAR (128) NOT NULL,
    [UpdatedOn] DATETIME2 (7)  NOT NULL
);

