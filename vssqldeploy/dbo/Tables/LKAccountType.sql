﻿CREATE TABLE [dbo].[LKAccountType] (
    [AccountTypeID] INT           NOT NULL,
    [AccountType]   NVARCHAR (50) NOT NULL,
    [IsActive]      BIT           CONSTRAINT [DF_LKAccountType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKAccountType] PRIMARY KEY CLUSTERED ([AccountTypeID] ASC) WITH (FILLFACTOR = 80)
);

