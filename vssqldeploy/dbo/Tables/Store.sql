﻿CREATE TABLE [dbo].[Store] (
    [StoreID]                     UNIQUEIDENTIFIER CONSTRAINT [DF_Store_Id] DEFAULT (newid()) NOT NULL,
    [TenantID]                    UNIQUEIDENTIFIER NOT NULL,
    [Name]                        NVARCHAR (256)   NOT NULL,
    [MPPAID]                      INT              CONSTRAINT [DF__Store__MPPAID__2B554987] DEFAULT (NEXT VALUE FOR [store_seq]) NOT NULL,
    [StoreNumber]                 NVARCHAR (64)    NULL,
    [FuelBrandID]                 INT              NOT NULL,
    [Phone]                       NVARCHAR (20)    NULL,
    [NumberOfPumps]               TINYINT          NOT NULL,
    [RegularPCATSCode]            NVARCHAR (20)    NULL,
    [MidgradePCATSCode]           NVARCHAR (20)    NULL,
    [PremiumPCATSCode]            NVARCHAR (20)    NULL,
    [DieselPCATSCode]             NVARCHAR (20)    NULL,
    [E85PCATSCode]                NVARCHAR (20)    NULL,
    [MerchantID]                  NVARCHAR (50)    NULL,
    [TimeZoneID]                  INT              NULL,
    [DisplayOnMobile]             BIT              NOT NULL,
    [IsSiteBillable]              BIT              CONSTRAINT [DF_Store_IsSiteBillable] DEFAULT ((0)) NOT NULL,
    [CreatedBy]                   NVARCHAR (255)   NULL,
    [CreatedOn]                   DATETIME2 (7)    CONSTRAINT [DF_Store_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                   NVARCHAR (255)   NULL,
    [UpdatedOn]                   DATETIME2 (7)    NULL,
    [StoreInstallTypeID]          INT              NULL,
    [StoreInstallDate]            DATETIME2 (7)    NULL,
    [StationImageID]              INT              NULL,
    [SiteServiceID]               NVARCHAR (50)    NULL,
    [ActiveAlertsMaintenanceMode] BIT              CONSTRAINT [DF_Store_ActiveAlertsLockout] DEFAULT ((0)) NOT NULL,
    [UnavailablePumps]            NVARCHAR (MAX)   CONSTRAINT [DF_Store_UnavailablePumps] DEFAULT ('[]') NULL,
    [StoreTenants]                NVARCHAR (MAX)   NULL,
    [IsDeleted]                   BIT              DEFAULT ((0)) NOT NULL,
    [ActiveAlertsIgnored]         BIT              DEFAULT ((0)) NOT NULL,
    [StreetAddress]               NVARCHAR (128)   NOT NULL,
    [City]                        NVARCHAR (64)    NOT NULL,
    [StateCode]                   CHAR (2)         NULL,
    [ZipCode]                     NVARCHAR (16)    NULL,
    [County]                      NVARCHAR (100)   NULL,
    [CountryIsoCode]              CHAR (2)         NOT NULL,
    [Latitude]                    DECIMAL (10, 7)  NOT NULL,
    [Longitude]                   DECIMAL (10, 7)  NOT NULL,
    [ContactInfo]                 NVARCHAR (MAX)   NULL,
    [IsBootstrapped]              BIT              CONSTRAINT [DF_Store_IsBootstrapped] DEFAULT ((0)) NOT NULL,
    [AllowInsidePayment]          BIT              DEFAULT ((0)) NOT NULL,
    [AllowOutsidePayment]         BIT              DEFAULT ((0)) NOT NULL,
    [EposIds]                     NVARCHAR (MAX)   NULL,
    [SiteUnavailablePumps]        NVARCHAR (MAX)   CONSTRAINT [DF_Store_SiteUnavailablePumps] DEFAULT ('[]') NULL,
    [AllowMultipleLoyalty]        BIT              CONSTRAINT [DF_Store_AllowMultipleLoyalty] DEFAULT ((0)) NULL,
    [PaymentsOffline]             BIT              DEFAULT ((0)) NOT NULL,
    [DisplayFullServiceDialog]    BIT              CONSTRAINT [DF_Store_DisplayFullServiceDialog] DEFAULT ((0)) NOT NULL,
    [StreetAddress2]              NVARCHAR (128)   NULL,
    [FuelServiceStatus]           NVARCHAR (128)   DEFAULT ('ENABLED') NOT NULL,
    [CarWashServiceStatus]        NVARCHAR (128)   DEFAULT ('ENABLED') NOT NULL,
    [CurrencyCode]                NVARCHAR (3)     NULL,
    CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED ([StoreID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_Store_ContactInfo_IsJson] CHECK (isjson([ContactInfo])>(0)),
    CONSTRAINT [CK_TenantIdNonEqualsStoreId] CHECK ([dbo].[IsTenant]([StoreID])=(0)),
    CONSTRAINT [Store_EposIds_should_be_JSON] CHECK (isjson([EposIds])>(0)),
    CONSTRAINT [Store_SiteUnavailablePumps_should_be_JSON] CHECK (isjson([SiteUnavailablePumps])>(0)),
    CONSTRAINT [Store_UnavailablePumps_should_be_JSON] CHECK (isjson([UnavailablePumps])>(0)),
    CONSTRAINT [StoreTenants_should_be_JSON] CHECK (isjson([StoreTenants])>(0)),
    CONSTRAINT [FK_Store_FuelBrand] FOREIGN KEY ([FuelBrandID]) REFERENCES [dbo].[FuelBrand] ([FuelBrandID]),
    CONSTRAINT [FK_Store_StationImage] FOREIGN KEY ([StationImageID]) REFERENCES [AppFeatures].[StationImage] ([StationImageID]),
    CONSTRAINT [FK_Store_StoreInstallType] FOREIGN KEY ([StoreInstallTypeID]) REFERENCES [dbo].[LKStoreInstallType] ([StoreInstallTypeID]),
    CONSTRAINT [FK_Store_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);


GO
ALTER TABLE [dbo].[Store] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Store_MPPAID]
    ON [dbo].[Store]([MPPAID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Store_Name]
    ON [dbo].[Store]([Name] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_Store_0968DEEE819E69E4DDF866DC3DC7352A]
    ON [dbo].[Store]([StoreNumber] ASC, [TenantID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_Store_TenantID]
    ON [dbo].[Store]([TenantID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_Store_StoreID_StoreInstallDate]
    ON [dbo].[Store]([StoreID] ASC, [StoreInstallDate] ASC) WITH (FILLFACTOR = 80);

