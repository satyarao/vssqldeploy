﻿CREATE TABLE [dbo].[PortalFieldFilter] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [PortalFilterID] INT            NOT NULL,
    [FieldName]      NVARCHAR (256) NOT NULL,
    [FilterType]     NVARCHAR (256) NULL,
    CONSTRAINT [PK_PortalFieldFilter] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PortalFieldFilter_PortalFilter_Cascade] FOREIGN KEY ([PortalFilterID]) REFERENCES [dbo].[PortalFilter] ([ID]) ON DELETE CASCADE
);

