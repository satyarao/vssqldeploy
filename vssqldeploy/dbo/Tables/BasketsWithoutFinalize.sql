﻿CREATE TABLE [dbo].[BasketsWithoutFinalize] (
    [BasketID]       UNIQUEIDENTIFIER NOT NULL,
    [Amount]         DECIMAL (18)     NOT NULL,
    [ResponseResult] NVARCHAR (50)    NOT NULL,
    [CreatedOn]      DATETIME2 (7)    NOT NULL
);

