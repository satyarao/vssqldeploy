﻿CREATE TABLE [dbo].[FlatBasket] (
    [FlatBasketID]           UNIQUEIDENTIFIER CONSTRAINT [DF_FlatBasket_FlatBasketID] DEFAULT (newid()) NOT NULL,
    [TransactionID]          UNIQUEIDENTIFIER NOT NULL,
    [StoreTenantID]          UNIQUEIDENTIFIER NOT NULL,
    [AppTenantID]            UNIQUEIDENTIFIER NOT NULL,
    [BasketStateName]        NVARCHAR (50)    NOT NULL,
    [BasketStateID]          TINYINT          NOT NULL,
    [UserID]                 UNIQUEIDENTIFIER NULL,
    [StoreMppaID]            NVARCHAR (50)    NOT NULL,
    [UserEmail]              NVARCHAR (255)   NULL,
    [POSOperatorID]          NVARCHAR (50)    NOT NULL,
    [POSTerminalID]          NVARCHAR (50)    NOT NULL,
    [POSTransactionID]       NVARCHAR (50)    NOT NULL,
    [PartnerTransactionID]   NVARCHAR (100)   NULL,
    [StoreName]              NVARCHAR (64)    NOT NULL,
    [StoreAddress]           NVARCHAR (128)   NOT NULL,
    [StoreCity]              NVARCHAR (64)    NOT NULL,
    [StoreID]                UNIQUEIDENTIFIER NOT NULL,
    [StoreNumber]            NVARCHAR (10)    NULL,
    [POSDateTimeLocal]       DATETIME2 (7)    NULL,
    [POSDateTime]            DATETIME2 (7)    NOT NULL,
    [OriginalPrice]          DECIMAL (9, 2)   NULL,
    [ProductDiscount]        DECIMAL (9, 2)   NULL,
    [FuelDiscount]           DECIMAL (9, 2)   NULL,
    [TotalDiscount]          DECIMAL (9, 2)   NULL,
    [SubTotal]               DECIMAL (9, 2)   NULL,
    [TaxAmount]              DECIMAL (9, 2)   NULL,
    [TotalAmount]            FLOAT (53)       NULL,
    [FundingProviderID]      SMALLINT         NOT NULL,
    [FundingProviderName]    NVARCHAR (255)   NOT NULL,
    [CardType]               NVARCHAR (255)   NOT NULL,
    [BatchID]                INT              NULL,
    [SettlementPeriodID]     NVARCHAR (50)    NOT NULL,
    [Stac]                   NVARCHAR (50)    NULL,
    [IsSiteBillable]         BIT              NOT NULL,
    [AuthCode]               NVARCHAR (MAX)   CONSTRAINT [DF_FlatBasket_AuthCode] DEFAULT ('[]') NULL,
    [IsSiteLevelPayment]     BIT              NOT NULL,
    [VisibleByTenants]       NVARCHAR (MAX)   CONSTRAINT [DF_FlatBasket_VisibleByTenants] DEFAULT ('[]') NOT NULL,
    [PaymentInfo]            NVARCHAR (MAX)   CONSTRAINT [DF_FlatBasket_PaymentInfo] DEFAULT ('[]') NOT NULL,
    [OfferName]              NVARCHAR (MAX)   NULL,
    [OfferID]                NVARCHAR (MAX)   NULL,
    [BasketLoyaltyStateID]   TINYINT          NOT NULL,
    [BasketLoyaltyStateName] NVARCHAR (50)    NOT NULL,
    [BasketLoyaltyPrograms]  NVARCHAR (MAX)   CONSTRAINT [DF_FlatBasket_BasketLoyaltyPrograms] DEFAULT ('[]') NOT NULL,
    [StoreMerchantID]        NVARCHAR (50)    NULL,
    [AppDisplayName]         NVARCHAR (255)   DEFAULT ('Not Set') NOT NULL,
    [AppChannel]             NVARCHAR (100)   DEFAULT ('WhiteLabelMobile') NOT NULL,
    [Odometer]               DECIMAL (18)     NULL,
    [LicensePlate]           NVARCHAR (MAX)   NULL,
    [LicenseNumber]          NVARCHAR (MAX)   NULL,
    [CustomerDriverID]       NVARCHAR (64)    NULL,
    [FleetTenantID]          UNIQUEIDENTIFIER NULL,
    [NuDetectScore]          INT              NULL,
    [NuDetectScoreBand]      VARCHAR (20)     NULL,
    [ScoreSignals]           NVARCHAR (MAX)   NULL,
    [OtherRewardTotal]       DECIMAL (9, 2)   NULL,
    [CurrencyIsoCode]        CHAR (3)         DEFAULT ('USD') NOT NULL,
    [NumberOfGallons]        DECIMAL (9, 3)   NULL,
    [Version]                INT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FlatBasket] PRIMARY KEY NONCLUSTERED ([FlatBasketID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FlatBasket_AuthCode_isJSON] CHECK (isjson([AuthCode])>(0)),
    CONSTRAINT [FlatBasket_BasketLoyaltyPrograms_isJSON] CHECK (isjson([BasketLoyaltyPrograms])>(0)),
    CONSTRAINT [FlatBasket_PaymentInfo_isJSON] CHECK (isjson([PaymentInfo])>(0)),
    CONSTRAINT [FlatBasket_VisibleByTenants_isJSON] CHECK (isjson([VisibleByTenants])>(0)),
    CONSTRAINT [UK_TransactionID_Value] UNIQUE NONCLUSTERED ([TransactionID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[FlatBasket] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO

CREATE TRIGGER FlatBasket_VisibleByTenants_UPDATE
ON FlatBasket
AFTER UPDATE
AS
BEGIN
	DELETE 
	FROM Reports.FlatBasket_VisibleByTenants 
	WHERE EXISTS (SELECT * FROM INSERTED WHERE INSERTED.FlatBasketID = Reports.FlatBasket_VisibleByTenants.FlatBasketID )

	INSERT INTO Reports.FlatBasket_VisibleByTenants(FlatBasketID, PartnerTenantId, TransactionID)
	SELECT FlatBasketID, pt.TenantId, TransactionID from INSERTED fb
	CROSS APPLY OPENJSON (fb.VisibleByTenants) WITH (TenantId uniqueidentifier '$') as pt
END

GO

CREATE TRIGGER FlatBasket_VisibleByTenants_INSERT
ON FlatBasket
AFTER INSERT
AS
INSERT INTO Reports.FlatBasket_VisibleByTenants(FlatBasketID, PartnerTenantId, TransactionID)
SELECT FlatBasketID, pt.TenantId, TransactionID from INSERTED fb
CROSS APPLY OPENJSON (fb.VisibleByTenants) WITH (TenantId uniqueidentifier '$') as pt
