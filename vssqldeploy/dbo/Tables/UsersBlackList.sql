﻿CREATE TABLE [dbo].[UsersBlackList] (
    [UserID]    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]  BIT              DEFAULT ((1)) NOT NULL,
    [CreatedBy] NVARCHAR (50)    NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_UsersBlackList_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy] NVARCHAR (50)    NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

