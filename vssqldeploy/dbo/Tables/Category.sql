﻿CREATE TABLE [dbo].[Category] (
    [CategoryID]  INT           NOT NULL,
    [Description] NVARCHAR (50) NOT NULL,
    [IsActive]    BIT           CONSTRAINT [DF_Category_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([CategoryID] ASC) WITH (FILLFACTOR = 80)
);

