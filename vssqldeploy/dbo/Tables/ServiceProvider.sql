﻿CREATE TABLE [dbo].[ServiceProvider] (
    [ServiceProviderId] INT              IDENTITY (1, 1) NOT NULL,
    [ServiceId]         INT              NOT NULL,
    [ProviderTenantId]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedBy]         VARCHAR (50)     CONSTRAINT [DF_ServiceProvider_CreatedBy] DEFAULT (suser_name()) NOT NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_ServiceProvider_CreatedOn] DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([ServiceProviderId] ASC),
    CONSTRAINT [FK_ServiceProvider_LKSiteServices] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[LKSiteServices] ([ServiceId]),
    CONSTRAINT [FK_ServiceProvider_Tenant] FOREIGN KEY ([ProviderTenantId]) REFERENCES [dbo].[Tenant] ([TenantID])
);

