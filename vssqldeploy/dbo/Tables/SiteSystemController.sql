﻿CREATE TABLE [dbo].[SiteSystemController] (
    [PartitionKey]                  NVARCHAR (50) NULL,
    [RowKey]                        NVARCHAR (50) NULL,
    [Timestamp]                     NVARCHAR (50) NULL,
    [SiteControllerSoftwareVersion] NVARCHAR (50) NULL
);

