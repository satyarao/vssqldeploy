﻿CREATE TABLE [dbo].[Filter] (
    [FilterID]           INT              IDENTITY (1, 1) NOT NULL,
    [FilterName]         NVARCHAR (256)   NOT NULL,
    [TenantID]           UNIQUEIDENTIFIER NULL,
    [UserID]             UNIQUEIDENTIFIER NULL,
    [IsPrivate]          BIT              CONSTRAINT [DF_Filter_IsPrivate] DEFAULT ((0)) NOT NULL,
    [FilterTypeID]       TINYINT          NOT NULL,
    [FilterText]         NVARCHAR (MAX)   NOT NULL,
    [CreatedBy]          NVARCHAR (128)   NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_Filter_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]          NVARCHAR (128)   NULL,
    [UpdatedOn]          DATETIME2 (7)    NULL,
    [IsActive]           BIT              CONSTRAINT [DF_Filter_IsActive] DEFAULT ((1)) NOT NULL,
    [VisibleByUsers]     NVARCHAR (MAX)   CONSTRAINT [DF_Filter_VisibleByUsers] DEFAULT ('[]') NOT NULL,
    [AvailableForOffers] BIT              CONSTRAINT [DF_Filter_AvailableForOffers] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Filter] PRIMARY KEY CLUSTERED ([FilterID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [Filter_VisibleByUsers_should_be_JSON] CHECK (isjson([VisibleByUsers])>(0)),
    CONSTRAINT [FK_Filter_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_Filter_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_FilterName_TenantID_FilterTypeID]
    ON [dbo].[Filter]([FilterName] ASC, [TenantID] ASC, [FilterTypeID] ASC) WHERE ([IsActive]<>(0)) WITH (FILLFACTOR = 80);

