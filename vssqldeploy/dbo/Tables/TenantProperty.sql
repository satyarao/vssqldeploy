﻿CREATE TABLE [dbo].[TenantProperty] (
    [TenantID]      UNIQUEIDENTIFIER NOT NULL,
    [PropertyName]  NVARCHAR (128)   NOT NULL,
    [PropertyValue] NVARCHAR (250)   NOT NULL,
    CONSTRAINT [PK_TenantProperty_1] PRIMARY KEY NONCLUSTERED ([TenantID] ASC, [PropertyName] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_TenantProperty_LKProperty] FOREIGN KEY ([PropertyName]) REFERENCES [dbo].[LKProperty] ([PropertyName])
);


GO
CREATE CLUSTERED INDEX [IX_TenantProperty]
    ON [dbo].[TenantProperty]([TenantID] ASC, [PropertyName] ASC, [PropertyValue] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_TenantProperty_24CA38355D86E53A0CDDFECD21BDB748]
    ON [dbo].[TenantProperty]([PropertyName] ASC) WITH (FILLFACTOR = 80);


GO

	CREATE TRIGGER [dbo].[TenantProperty_InsertUpdateDelete] on [dbo].[TenantProperty] AFTER INSERT,UPDATE,DELETE
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @action nvarchar(50)

		SET @action = 'INSERT'; -- Set Action to Insert by default.
	
		IF EXISTS(SELECT * FROM DELETED)
			BEGIN
				SET @action = 
					CASE
						WHEN EXISTS(SELECT * FROM INSERTED) THEN 'UPDATE' -- Set Action to Updated.
						ELSE 'DELETE' -- Set Action to Deleted.       
					END
			END
		ELSE 
			IF NOT EXISTS(SELECT * FROM INSERTED) RETURN; -- Nothing updated or inserted.

		IF @action='INSERT' OR @action='UPDATE'
			BEGIN
							INSERT into [Audit].[TenantProperty]
								([PropertyName], [PropertyValue], [TenantID], [Operation], [UpdatedBy])
								SELECT i.PropertyName, i.PropertyValue, i.TenantID, @action, SUSER_SNAME()
								FROM INSERTED i 
			END
		ELSE
			BEGIN
							INSERT into [Audit].[TenantProperty]
								([PropertyName], [PropertyValue], [TenantID], [Operation], [UpdatedBy])
								SELECT i.PropertyName, i.PropertyValue, i.TenantID, @action, SUSER_SNAME()
								FROM DELETED i
			END
	END

	

