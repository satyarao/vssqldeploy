﻿CREATE TABLE [dbo].[DBMessageLog] (
    [ModuleName]     VARCHAR (100)  NOT NULL,
    [Message]        NVARCHAR (MAX) NULL,
    [MessageType]    VARCHAR (20)   DEFAULT ('INFO') NOT NULL,
    [LoggedDateTime] DATETIME2 (7)  DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_DBMessageLog_LoggedDateTime_ModuleName] PRIMARY KEY CLUSTERED ([LoggedDateTime] ASC, [ModuleName] ASC)
);

