﻿CREATE TABLE [dbo].[TransactionReprocessingState] (
    [TransactionId]     UNIQUEIDENTIFIER NOT NULL,
    [LoyaltyFinalize]   BIT              NULL,
    [WithoutFinalize]   BIT              NULL,
    [P66EPOSSettlement] BIT              NULL,
    [Timestamp]         DATETIME         NULL,
    [IsProcessed]       BIT              NULL,
    CONSTRAINT [PK_TransactionReprocessingState] PRIMARY KEY CLUSTERED ([TransactionId] ASC)
);

