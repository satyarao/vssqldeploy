﻿CREATE TABLE [dbo].[ConfigPRECIDIAPOSLYNX] (
    [ConfigID]        INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]        UNIQUEIDENTIFIER NOT NULL,
    [StoreName]       NVARCHAR (255)   NULL,
    [MerchantIP]      NVARCHAR (15)    NULL,
    [URI]             NVARCHAR (250)   NULL,
    [Lane]            NVARCHAR (50)    NOT NULL,
    [Port]            INT              NULL,
    [ClientMAC]       NVARCHAR (50)    NOT NULL,
    [IsActive]        BIT              CONSTRAINT [DF_ConfigPRECIDIAPOSLYNX_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_ConfigPRECIDIAPOSLYNX_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]       DATETIME2 (7)    NULL,
    [UseTokenService] BIT              CONSTRAINT [DF__ConfigPRE__UseTo__58C70E7C] DEFAULT ((1)) NOT NULL,
    [UseTCP]          BIT              CONSTRAINT [DF__ConfigPRE__UseTC__59BB32B5] DEFAULT ((0)) NOT NULL,
    [BuyPassPumpLane] TINYINT          NULL,
    CONSTRAINT [PK_ConfigPRECIDIAPOSLYNX_1] PRIMARY KEY CLUSTERED ([TenantID] ASC, [Lane] ASC) WITH (FILLFACTOR = 80)
);

