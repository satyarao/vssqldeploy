﻿CREATE TABLE [dbo].[PaymentSystemProductCodes] (
    [PaymentCode]    NCHAR (3)      NOT NULL,
    [Description]    NVARCHAR (MAX) NOT NULL,
    [Deprecated]     BIT            NOT NULL,
    [Grade]          NVARCHAR (MAX) NOT NULL,
    [ProductGroupID] TINYINT        NULL,
    CONSTRAINT [PK_PaymentSystemProductCodes] PRIMARY KEY CLUSTERED ([PaymentCode] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PaymentSystemProductCodes_ProductGroup] FOREIGN KEY ([ProductGroupID]) REFERENCES [dbo].[PaymentSystemProductGroup] ([ProductGroupID])
);

