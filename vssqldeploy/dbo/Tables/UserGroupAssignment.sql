﻿CREATE TABLE [dbo].[UserGroupAssignment] (
    [UserGroupAssignmentID] INT           IDENTITY (1, 1) NOT NULL,
    [UserAccountKey]        INT           NOT NULL,
    [UserGroupID]           INT           NOT NULL,
    [DateOfAssignment]      DATETIME2 (7) NULL,
    CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED ([UserGroupAssignmentID] ASC),
    CONSTRAINT [FK_UserGroupAssignment_UserAccount] FOREIGN KEY ([UserAccountKey]) REFERENCES [MR].[UserAccounts] ([Key]),
    CONSTRAINT [FK_UserGroupAssignment_UserGroup] FOREIGN KEY ([UserGroupID]) REFERENCES [AccessControl].[UserGroup] ([UserGroupID]),
    CONSTRAINT [UK_AccountKey_Group] UNIQUE NONCLUSTERED ([UserAccountKey] ASC, [UserGroupID] ASC)
);

