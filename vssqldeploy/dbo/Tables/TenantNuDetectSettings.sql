﻿CREATE TABLE [dbo].[TenantNuDetectSettings] (
    [TenantId]               UNIQUEIDENTIFIER NOT NULL,
    [YellowLockoutThreshold] INT              NOT NULL,
    [RedLockoutThreshold]    INT              NOT NULL,
    [CardLockout]            BIT              DEFAULT ((0)) NOT NULL,
    [TransactionLockout]     BIT              DEFAULT ((0)) NOT NULL,
    [PassiveMode]            BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TenantNuDetectSettings] PRIMARY KEY NONCLUSTERED ([TenantId] ASC),
    CONSTRAINT [FK_TenantNuDetectSettings_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE
);

