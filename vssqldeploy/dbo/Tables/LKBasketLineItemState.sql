﻿CREATE TABLE [dbo].[LKBasketLineItemState] (
    [BasketLineItemStateID] INT           NOT NULL,
    [Name]                  NVARCHAR (50) NOT NULL,
    [IsActive]              BIT           NOT NULL,
    CONSTRAINT [PK_LKBasketLineState] PRIMARY KEY CLUSTERED ([BasketLineItemStateID] ASC) WITH (FILLFACTOR = 80)
);

