﻿CREATE TABLE [dbo].[ProductCodeType] (
    [ProductCodeTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ProductCodeTypeName] NVARCHAR (50) NOT NULL,
    [IsActive]            BIT           NOT NULL,
    [CreatedOn]           DATETIME2 (7) NOT NULL,
    [UpdatedOn]           DATETIME2 (7) NULL,
    CONSTRAINT [PK_ProductCodeType] PRIMARY KEY CLUSTERED ([ProductCodeTypeID] ASC),
    CONSTRAINT [Id_Unique_ProductCodeType_ProductCodeTypeName] UNIQUE NONCLUSTERED ([ProductCodeTypeName] ASC)
);

