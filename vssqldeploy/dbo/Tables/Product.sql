﻿CREATE TABLE [dbo].[Product] (
    [ProductConfigID]    INT            NOT NULL,
    [ProductCode]        NVARCHAR (50)  NOT NULL,
    [ProductDescription] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductConfigID] ASC, [ProductCode] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Product_LKProductConfig] FOREIGN KEY ([ProductConfigID]) REFERENCES [dbo].[LKProductConfig] ([ProductConfigID])
);

