﻿CREATE TABLE [dbo].[Location] (
    [LocationID]                 UNIQUEIDENTIFIER  NOT NULL,
    [VerticalID]                 INT               NOT NULL,
    [VendorTenantID]             UNIQUEIDENTIFIER  NOT NULL,
    [PaymentSettingsTenantID]    UNIQUEIDENTIFIER  NOT NULL,
    [AppTenantID]                UNIQUEIDENTIFIER  NOT NULL,
    [VendorLocationID]           NVARCHAR (100)    NOT NULL,
    [AppTenantLocationIDs]       NVARCHAR (MAX)    NULL,
    [MerchantID]                 NVARCHAR (100)    NULL,
    [DetailDocumentID]           NVARCHAR (100)    NOT NULL,
    [DetailDocumentPartitionKey] NVARCHAR (100)    NOT NULL,
    [Name]                       NVARCHAR (100)    NOT NULL,
    [Language]                   NVARCHAR (50)     NOT NULL,
    [Geo]                        [sys].[geography] NOT NULL,
    [Latitude]                   DECIMAL (10, 7)   NOT NULL,
    [Longitude]                  DECIMAL (10, 7)   NOT NULL,
    [StreetAddress]              NVARCHAR (128)    NOT NULL,
    [City]                       NVARCHAR (64)     NOT NULL,
    [StateCode]                  CHAR (2)          NULL,
    [PostalCode]                 NVARCHAR (16)     NULL,
    [CountryIsoCode]             CHAR (2)          NULL,
    [BrandName]                  NVARCHAR (128)    NULL,
    [Services]                   NVARCHAR (MAX)    NULL,
    [Products]                   NVARCHAR (MAX)    NULL,
    [DisplayOnMobile]            BIT               NOT NULL,
    [ImageUrl]                   NVARCHAR (256)    NULL,
    [AllowMobilePayments]        BIT               NOT NULL,
    [MobilePaymentsOffline]      BIT               NOT NULL,
    [IsDeleted]                  BIT               NOT NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([LocationID] ASC),
    CONSTRAINT [FK_Location_LocationVertical] FOREIGN KEY ([VerticalID]) REFERENCES [dbo].[LocationVertical] ([VerticalID])
);


GO
ALTER TABLE [dbo].[Location] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

