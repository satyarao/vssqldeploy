﻿CREATE TABLE [dbo].[LinkedAccounts] (
    [Key]               INT            IDENTITY (1, 1) NOT NULL,
    [ParentKey]         INT            NOT NULL,
    [ProviderName]      NVARCHAR (30)  NOT NULL,
    [ProviderAccountID] NVARCHAR (100) NOT NULL,
    [LastLogin]         DATETIME       NOT NULL,
    CONSTRAINT [PK_dbo.LinkedAccounts] PRIMARY KEY CLUSTERED ([Key] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_dbo.LinkedAccounts_dbo.UserAccounts_ParentKey] FOREIGN KEY ([ParentKey]) REFERENCES [dbo].[UserAccounts] ([Key]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentKey]
    ON [dbo].[LinkedAccounts]([ParentKey] ASC) WITH (FILLFACTOR = 80);

