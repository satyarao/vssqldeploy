﻿CREATE TABLE [dbo].[MppaBatch] (
    [BatchID]            INT              IDENTITY (1, 1) NOT NULL,
    [IsClosed]           BIT              NOT NULL,
    [CreatedOn]          DATETIME2 (7)    NULL,
    [UpdatedOn]          DATETIME2 (7)    NULL,
    [StoreID]            UNIQUEIDENTIFIER NOT NULL,
    [IsMisMatch]         BIT              CONSTRAINT [DF__MppaBatch__IsMis__73D8C730] DEFAULT ((0)) NOT NULL,
    [TotalTransactions]  INT              NULL,
    [TotalAmount]        DECIMAL (9, 2)   NULL,
    [SettlementPeriodID] NVARCHAR (50)    NULL,
    [BusinessDate]       DATETIME2 (7)    NULL,
    [RefData]            NVARCHAR (MAX)   NULL,
    [UMTI]               NVARCHAR (50)    NULL,
    [BatchGuid]          UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_batchId] PRIMARY KEY NONCLUSTERED ([BatchID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__MppaBatch__Store__72E4A2F7] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);

