﻿CREATE TABLE [dbo].[LocationVertical] (
    [VerticalID] INT           NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_LocationVertical] PRIMARY KEY CLUSTERED ([VerticalID] ASC)
);

