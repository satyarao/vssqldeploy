﻿CREATE TABLE [dbo].[EVChargeConnector] (
    [EVChargeConnectorId]   INT           IDENTITY (1, 1) NOT NULL,
    [ServiceProviderId]     INT           NOT NULL,
    [SiteId]                INT           NULL,
    [EVConnectorIdentifier] VARCHAR (50)  NOT NULL,
    [Name]                  VARCHAR (50)  NULL,
    [SpeedInkW]             VARCHAR (30)  NOT NULL,
    [Mode]                  VARCHAR (20)  NULL,
    [Status]                VARCHAR (50)  NOT NULL,
    [CreatedBy]             VARCHAR (100) CONSTRAINT [DF_EVChargeConnector_CreatedBy] DEFAULT (suser_name()) NOT NULL,
    [CreatedOn]             DATETIME2 (7) CONSTRAINT [DF_EVChargeConnector_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [UpdatedBy]             VARCHAR (50)  NULL,
    [UpdatedOn]             DATETIME2 (7) NULL,
    CONSTRAINT [PK_EVChargeConnector_EVChargeConnectorId] PRIMARY KEY CLUSTERED ([EVChargeConnectorId] ASC),
    CONSTRAINT [FK_EVChargeConnector_ServiceProvider] FOREIGN KEY ([ServiceProviderId]) REFERENCES [dbo].[ServiceProvider] ([ServiceProviderId]),
    CONSTRAINT [FK_EVChargeConnector_SiteDetail] FOREIGN KEY ([SiteId]) REFERENCES [Common].[SiteDetail] ([SiteId])
);

