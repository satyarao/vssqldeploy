﻿CREATE TABLE [dbo].[LKFrontEndProcessorType] (
    [ID]          INT            NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL
);

