﻿CREATE TABLE [dbo].[LKBasketType] (
    [BasketTypeID] TINYINT       NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [IsActive]     BIT           CONSTRAINT [DF_LKBasketType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKBasketType] PRIMARY KEY CLUSTERED ([BasketTypeID] ASC) WITH (FILLFACTOR = 80)
);

