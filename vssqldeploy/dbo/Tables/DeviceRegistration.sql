﻿CREATE TABLE [dbo].[DeviceRegistration] (
    [DeviceRegistrationID]      UNIQUEIDENTIFIER CONSTRAINT [DF_DeviceRegistration_DeviceRegistrationID] DEFAULT (newid()) NOT NULL,
    [UserID]                    UNIQUEIDENTIFIER NOT NULL,
    [InstallationID]            NVARCHAR (128)   NOT NULL,
    [VerificationID]            INT              NOT NULL,
    [PhoneNumber]               NVARCHAR (50)    NULL,
    [EmailAddress]              NVARCHAR (255)   NULL,
    [IsActive]                  BIT              CONSTRAINT [DF_DeviceRegistration_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy]                 NVARCHAR (50)    NULL,
    [CreatedOn]                 DATETIME2 (7)    CONSTRAINT [DF_DeviceRegistration_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                 NVARCHAR (50)    NULL,
    [UpdatedOn]                 DATETIME2 (7)    NULL,
    [NotificationToken]         NVARCHAR (255)   NULL,
    [IsNotificationTokenActive] BIT              NULL,
    CONSTRAINT [PK_DeviceRegistration] PRIMARY KEY CLUSTERED ([DeviceRegistrationID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_DeviceRegistration_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [FK_DeviceRegistration_Verification] FOREIGN KEY ([VerificationID]) REFERENCES [dbo].[Verification] ([VerificationID])
);

