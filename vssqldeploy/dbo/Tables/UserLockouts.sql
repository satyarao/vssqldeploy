﻿CREATE TABLE [dbo].[UserLockouts] (
    [UserId]            UNIQUEIDENTIFIER NOT NULL,
    [BlockTransactions] BIT              NOT NULL,
    [BlockCardAddition] BIT              NOT NULL,
    CONSTRAINT [PK_UserLockouts] PRIMARY KEY NONCLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_UserLockouts_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);

