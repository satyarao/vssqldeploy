﻿CREATE TABLE [dbo].[SimulatedStore] (
    [StoreID]            UNIQUEIDENTIFIER NOT NULL,
    [IsSimulationActive] BIT              NOT NULL,
    [CreatedOn]          DATETIME2 (7)    NOT NULL,
    [UpdatedOn]          DATETIME2 (7)    NULL,
    CONSTRAINT [PK_SimulatedStore] PRIMARY KEY CLUSTERED ([StoreID] ASC),
    CONSTRAINT [FK_SimulatedStore_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);

