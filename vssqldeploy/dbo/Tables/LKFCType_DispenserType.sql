﻿CREATE TABLE [dbo].[LKFCType_DispenserType] (
    [FCTypeID]        TINYINT NOT NULL,
    [DispenserTypeID] TINYINT NOT NULL,
    CONSTRAINT [FK__LKFCType___Dispe__6621099A] FOREIGN KEY ([DispenserTypeID]) REFERENCES [dbo].[LKDispenserType] ([DispenserTypeID]),
    CONSTRAINT [FK__LKFCType___FCTyp__652CE561] FOREIGN KEY ([FCTypeID]) REFERENCES [dbo].[LKFCType] ([FCTypeID])
);


GO

CREATE TRIGGER [dbo].[Unique_LKFCType_DispenserType] ON [dbo].[LKFCType_DispenserType]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON
	IF EXISTS
	(
		SELECT 1
		FROM [dbo].[LKFCType_DispenserType] AS LK
		INNER JOIN INSERTED AS I
		ON LK.FCTypeID = I.FCTypeID
		AND LK.DispenserTypeID = I.DispenserTypeID
	)
	BEGIN
		RETURN
	END
	--No Duplicates Found, Begin inserting
	INSERT INTO [dbo].[LKFCType_DispenserType]
	SELECT * FROM INSERTED
END

