﻿CREATE TABLE [dbo].[Pump] (
    [StoreId]      UNIQUEIDENTIFIER NOT NULL,
    [PumpNumber]   INT              NOT NULL,
    [ServiceLevel] NVARCHAR (250)   NOT NULL,
    [PumpStatus]   NVARCHAR (250)   NOT NULL,
    CONSTRAINT [PK_Pump] PRIMARY KEY NONCLUSTERED ([StoreId] ASC, [PumpNumber] ASC),
    CONSTRAINT [FK_Pump_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreID]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[Pump] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);


GO
CREATE NONCLUSTERED INDEX [IX_Pump_StoreId]
    ON [dbo].[Pump]([StoreId] ASC);

