﻿CREATE TABLE [dbo].[TermsAndConditionSections] (
    [SectionId]         INT              IDENTITY (1, 1) NOT NULL,
    [SectionOrder]      INT              NOT NULL,
    [SectionTitle]      NVARCHAR (100)   NULL,
    [SectionText]       NVARCHAR (MAX)   NULL,
    [TermsConditionsId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_TermsAndConditionSections] PRIMARY KEY CLUSTERED ([SectionId] ASC),
    CONSTRAINT [FK_TermsAndConditionSections_TermsAndConditionsDetails] FOREIGN KEY ([TermsConditionsId]) REFERENCES [dbo].[TermsAndConditionsDetails] ([TermsConditionsId]) ON DELETE CASCADE
);

