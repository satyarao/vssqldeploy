﻿CREATE TABLE [dbo].[SettlementData] (
    [SettlementId]            UNIQUEIDENTIFIER NOT NULL,
    [MppaId]                  NVARCHAR (25)    NULL,
    [PaymentInfoKey]          NVARCHAR (50)    NULL,
    [Timestamp]               DATETIME         NULL,
    [ASLoyaltyPresent]        NVARCHAR (10)    NULL,
    [CampaignId]              UNIQUEIDENTIFIER NULL,
    [DiscountAmount]          DECIMAL (18, 2)  NULL,
    [DiscountUnitAmount]      DECIMAL (18, 2)  NULL,
    [EPOSTransactionId]       NVARCHAR (50)    NULL,
    [FinalAmount]             DECIMAL (18, 2)  NULL,
    [FuelVolume]              DECIMAL (18, 3)  NULL,
    [LoyaltyId]               NVARCHAR (50)    NULL,
    [LoyaltyTransactionId]    UNIQUEIDENTIFIER NULL,
    [P97TransactionId]        UNIQUEIDENTIFIER NULL,
    [POSCode]                 NVARCHAR (50)    NULL,
    [SellingUnits]            DECIMAL (18, 3)  NULL,
    [ShortTransactionId]      NVARCHAR (22)    NULL,
    [Status]                  INT              NULL,
    [SystemProductCode]       NVARCHAR (50)    NULL,
    [TenantId]                NVARCHAR (50)    NULL,
    [TransactionUTCTimeStamp] DATETIME         NULL,
    [UMTI]                    NVARCHAR (50)    NULL,
    [UnitMeasure]             NVARCHAR (50)    NULL,
    [POSOperatorId]           NVARCHAR (50)    NULL,
    [PaymentMethod]           NVARCHAR (50)    NULL,
    [POSCodeFormat]           NVARCHAR (50)    NULL,
    [MrnNumber]               NVARCHAR (50)    NULL,
    [StoreId]                 NVARCHAR (50)    NULL,
    [AppTenantId]             NVARCHAR (50)    NULL,
    CONSTRAINT [PK_SettlementData] PRIMARY KEY CLUSTERED ([SettlementId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_TenantId]
    ON [dbo].[SettlementData]([TenantId] ASC) WHERE ([TenantId] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [idx_Timestamp]
    ON [dbo].[SettlementData]([Timestamp] ASC) WHERE ([TenantId] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [idx_PaymentInfoKey]
    ON [dbo].[SettlementData]([PaymentInfoKey] ASC) WHERE ([TenantId] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [idx_MppaId]
    ON [dbo].[SettlementData]([MppaId] ASC) WHERE ([TenantId] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [idx_P97TransactionId]
    ON [dbo].[SettlementData]([P97TransactionId] ASC) WHERE ([TenantId] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [idx_Status]
    ON [dbo].[SettlementData]([Status] ASC) WHERE ([TenantId] IS NOT NULL);

