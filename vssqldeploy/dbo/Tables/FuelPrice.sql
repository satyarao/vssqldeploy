﻿CREATE TABLE [dbo].[FuelPrice] (
    [StoreID]             UNIQUEIDENTIFIER NOT NULL,
    [PCATSCode]           NVARCHAR (20)    NOT NULL,
    [CashPrice]           SMALLMONEY       NULL,
    [CreditPrice]         SMALLMONEY       NULL,
    [UOM]                 NVARCHAR (50)    NOT NULL,
    [OctaneRating]        TINYINT          NOT NULL,
    [IsActive]            BIT              NOT NULL,
    [CreatedBy]           NVARCHAR (50)    NULL,
    [CreatedOn]           DATETIME2 (7)    CONSTRAINT [DF_FuelPrice_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]           NVARCHAR (50)    NULL,
    [UpdatedOn]           DATETIME2 (7)    NULL,
    [DisplayName]         NVARCHAR (50)    NULL,
    [Description]         NVARCHAR (MAX)   NULL,
    [StandardProductCode] NVARCHAR (150)   NULL,
    CONSTRAINT [PK_FuelPrices] PRIMARY KEY CLUSTERED ([StoreID] ASC, [PCATSCode] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_FuelPrice_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);


GO
ALTER TABLE [dbo].[FuelPrice] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_FuelPrice]
    ON [dbo].[FuelPrice]([StoreID] ASC) WITH (FILLFACTOR = 80);

