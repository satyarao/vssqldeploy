﻿CREATE TABLE [dbo].[FrontEndProcessor] (
    [FrontEndID]       TINYINT       IDENTITY (1, 1) NOT NULL,
    [FrontEndTypeName] NVARCHAR (64) NOT NULL,
    [IsActive]         BIT           CONSTRAINT [DF_FrontEndProcessor_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__FrontEnd__92CDA1985301BAA1] PRIMARY KEY CLUSTERED ([FrontEndID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE TRIGGER [dbo].[Unique_FrontEndProcessor] ON [dbo].[FrontEndProcessor]
INSTEAD OF INSERT
AS
BEGIN
	SET NOCOUNT ON
	IF EXISTS
	(
		SELECT 1
		FROM [dbo].[FrontEndProcessor] AS LK
		INNER JOIN INSERTED AS I
		ON LK.FrontEndTypeName = I.FrontEndTypeName
	)
	BEGIN
		UPDATE [dbo].[FrontEndProcessor]
		SET IsActive = I.IsActive
		FROM [dbo].[FrontEndProcessor] LK
		INNER JOIN INSERTED AS I
		ON LK.FrontEndTypeName = I.FrontEndTypeName
		RETURN
	END
	--No Duplicates Found, Begin inserting
	INSERT INTO [dbo].[FrontEndProcessor] (FrontEndTypeName, IsActive)
	SELECT FrontEndTypeName, IsActive FROM INSERTED
END
