﻿CREATE TABLE [dbo].[TenantCertificate] (
    [ID]        INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]  UNIQUEIDENTIFIER NOT NULL,
    [CertData]  NVARCHAR (MAX)   NOT NULL,
    [IsActive]  BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn] DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    CONSTRAINT [PK_TenantCertificate] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_TenantCertificate_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);


GO

CREATE TRIGGER [dbo].[TenantCertificate_Insert_Update] ON [dbo].[TenantCertificate]
			FOR INSERT, UPDATE
		AS BEGIN
			SET NOCOUNT ON;
			IF UPDATE ([CertData]) 
			BEGIN
				UPDATE tc
				SET tc.[CertData] = REPLACE(I.[CertData], '\n', CHAR(10))
				FROM [dbo].[TenantCertificate] tc
				INNER JOIN Inserted I ON tc.[ID] = I.[ID]
			END 
		END
