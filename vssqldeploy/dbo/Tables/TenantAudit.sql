﻿CREATE TABLE [dbo].[TenantAudit] (
    [AuditID]     INT              IDENTITY (1, 1) NOT NULL,
    [UserID]      UNIQUEIDENTIFIER NOT NULL,
    [TenantID]    UNIQUEIDENTIFIER NOT NULL,
    [IPAddress]   VARCHAR (48)     NOT NULL,
    [URL]         NVARCHAR (1024)  NOT NULL,
    [Category]    NVARCHAR (MAX)   NULL,
    [RequestType] NVARCHAR (10)    NOT NULL,
    [StatusCode]  INT              NOT NULL,
    [BodyJson]    NVARCHAR (MAX)   NOT NULL,
    [Received]    DATETIME2 (7)    NOT NULL,
    [Sent]        DATETIME2 (7)    NULL,
    [ModelsJson]  NVARCHAR (MAX)   NULL,
    [ModelsId]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_TenantAudit] PRIMARY KEY CLUSTERED ([AuditID] ASC),
    CONSTRAINT [CK_TenantAudit_Category_IsJson] CHECK (isjson([Category])>(0)),
    CONSTRAINT [FK_TenantAudit_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_TenantAudit_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);


GO
ALTER TABLE [dbo].[TenantAudit] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

