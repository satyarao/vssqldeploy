﻿CREATE TABLE [dbo].[FlatStore] (
    [TenantID]                    UNIQUEIDENTIFIER  NOT NULL,
    [StoreID]                     UNIQUEIDENTIFIER  NOT NULL,
    [Name]                        NVARCHAR (256)    NOT NULL,
    [StoreNumber]                 NVARCHAR (64)     NULL,
    [MerchantId]                  NVARCHAR (50)     NULL,
    [Phone]                       NVARCHAR (20)     NULL,
    [NumberOfPumps]               TINYINT           NOT NULL,
    [Latitude]                    DECIMAL (10, 7)   NOT NULL,
    [Longitude]                   DECIMAL (10, 7)   NOT NULL,
    [StreetAddress]               NVARCHAR (128)    NOT NULL,
    [City]                        NVARCHAR (64)     NOT NULL,
    [StateCode]                   CHAR (2)          NULL,
    [PostalCode]                  NVARCHAR (16)     NULL,
    [CountryIsoCode]              CHAR (2)          NULL,
    [FuelBrand]                   NVARCHAR (128)    NOT NULL,
    [Services]                    NVARCHAR (MAX)    NULL,
    [FlatStoreID]                 UNIQUEIDENTIFIER  CONSTRAINT [DF__FlatStore__FlatS__0564AAC9] DEFAULT (newid()) NOT NULL,
    [DisplayOnMobile]             BIT               CONSTRAINT [DF_FlatStore_IsActive] DEFAULT ((1)) NOT NULL,
    [County]                      NVARCHAR (100)    NULL,
    [MppaID]                      NVARCHAR (50)     NULL,
    [StoreInstallTypeID]          INT               NULL,
    [OwnerID]                     UNIQUEIDENTIFIER  NOT NULL,
    [IsOwnedByTenant]             AS                (CONVERT([bit],case when [TenantID]=[OwnerID] then (1) else (0) end,(0))),
    [StationImageUrl]             NVARCHAR (256)    NULL,
    [ActiveAlertsMaintenanceMode] BIT               CONSTRAINT [DF_FlatStore_ActiveAlertsLockout] DEFAULT ((0)) NOT NULL,
    [UnavailablePumps]            NVARCHAR (MAX)    CONSTRAINT [DF_FlatStore_UnavailablePumps] DEFAULT ('[]') NULL,
    [IsSiteBillable]              BIT               CONSTRAINT [DF_FlatStore_IsSiteBillable] DEFAULT ((1)) NOT NULL,
    [FuelProducts]                NVARCHAR (MAX)    NULL,
    [OperatingHours]              NVARCHAR (MAX)    NULL,
    [ActiveAlertsIgnored]         BIT               CONSTRAINT [DF__FlatStore__Activ__291761CD] DEFAULT ((0)) NOT NULL,
    [Geo]                         [sys].[geography] NOT NULL,
    [AllowInsidePayment]          BIT               DEFAULT ((0)) NOT NULL,
    [AllowOutsidePayment]         BIT               DEFAULT ((0)) NOT NULL,
    [IsBootstrapped]              BIT               CONSTRAINT [DF_FlatStore_IsBootstrapped] DEFAULT ((0)) NOT NULL,
    [IsDeleted]                   BIT               DEFAULT ((0)) NOT NULL,
    [EposIds]                     NVARCHAR (MAX)    NULL,
    [StateName]                   NVARCHAR (64)     NULL,
    [SiteUnavailablePumps]        NVARCHAR (MAX)    CONSTRAINT [DF_FlatStore_SiteUnavailablePumps] DEFAULT ('[]') NULL,
    [AllowMultipleLoyalty]        BIT               CONSTRAINT [DF_FlatStore_AllowMultipleLoyalty] DEFAULT ((0)) NULL,
    [PaymentsOffline]             BIT               DEFAULT ((0)) NOT NULL,
    [DisplayFullServiceDialog]    BIT               CONSTRAINT [DF_FlatStore_DisplayFullServiceDialog] DEFAULT ((0)) NOT NULL,
    [CustomProperties]            NVARCHAR (MAX)    NULL,
    [CarWashItems]                NVARCHAR (MAX)    NULL,
    [FuelingPoints]               NVARCHAR (MAX)    NULL,
    [LoyaltyServices]             NVARCHAR (MAX)    NULL,
    [StreetAddress2]              NVARCHAR (128)    NULL,
    [FuelServiceStatus]           NVARCHAR (128)    DEFAULT ('ENABLED') NOT NULL,
    [CarWashServiceStatus]        NVARCHAR (128)    DEFAULT ('ENABLED') NOT NULL,
    CONSTRAINT [PK_FlatStore] PRIMARY KEY NONCLUSTERED ([FlatStoreID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FlatStore_EposIds_should_be_JSON] CHECK (isjson([EposIds])>(0)),
    CONSTRAINT [FlatStore_SiteUnavailablePumps_should_be_JSON] CHECK (isjson([UnavailablePumps])>(0)),
    CONSTRAINT [FlatStore_UnavailablePumps_should_be_JSON] CHECK (isjson([UnavailablePumps])>(0)),
    CONSTRAINT [OperatingHours_should_be_JSON] CHECK (isjson([OperatingHours])>(0)),
    CONSTRAINT [FK_FlatStore_OwnerID_Tenant] FOREIGN KEY ([OwnerID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_FlatStore_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID]),
    CONSTRAINT [FK_FlatStore_StoreInstallType] FOREIGN KEY ([StoreInstallTypeID]) REFERENCES [dbo].[LKStoreInstallType] ([StoreInstallTypeID]),
    CONSTRAINT [FK_FlatStore_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);


GO
ALTER TABLE [dbo].[FlatStore] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_FlatStore]
    ON [dbo].[FlatStore]([StoreID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_FlatStore_1704C20DA2220F322EC5297CFD3DB616]
    ON [dbo].[FlatStore]([IsOwnedByTenant] ASC, [MppaID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_FlatStore_2E08314D5DC1A888C341E3402496D46D]
    ON [dbo].[FlatStore]([DisplayOnMobile] ASC)
    INCLUDE([Latitude], [Longitude], [MppaID], [Name], [StoreID], [TenantID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_FlatStore_FF4CBA70665F54822797B315DA971DDC]
    ON [dbo].[FlatStore]([TenantID] ASC)
    INCLUDE([OwnerID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_FlatStore_219F0594606568D6750269836688F044]
    ON [dbo].[FlatStore]([OwnerID] ASC)
    INCLUDE([TenantID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatStore_StoreID_TenantID_StoreInstallTypeID]
    ON [dbo].[FlatStore]([StoreID] ASC, [TenantID] ASC, [StoreInstallTypeID] ASC)
    INCLUDE([Name], [StoreNumber], [StreetAddress], [City], [StateCode], [PostalCode], [OwnerID], [IsSiteBillable]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatStore_TenantID]
    ON [dbo].[FlatStore]([TenantID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatStore_Name]
    ON [dbo].[FlatStore]([Name] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatStore_StoreInstallTypeID]
    ON [dbo].[FlatStore]([StoreInstallTypeID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatStore_OwnerID]
    ON [dbo].[FlatStore]([OwnerID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatStore_IsSiteBillable]
    ON [dbo].[FlatStore]([IsSiteBillable] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatStore_BillingReportIndex]
    ON [dbo].[FlatStore]([TenantID] ASC, [StoreID] ASC, [Name] ASC, [StoreInstallTypeID] ASC, [OwnerID] ASC, [IsSiteBillable] ASC) WITH (FILLFACTOR = 80);

