﻿CREATE TABLE [dbo].[TestTable]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NCHAR(10) NULL, 
    [Description] VARCHAR(50) NULL, 
    [IsActive] BIT NULL,
    CreatedDate datetime
)
