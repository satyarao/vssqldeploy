﻿CREATE TABLE [dbo].[UserNonce] (
    [UserID]    UNIQUEIDENTIFIER NOT NULL,
    [Nonce]     NVARCHAR (50)    NOT NULL,
    [IsActive]  BIT              NOT NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    CONSTRAINT [PK_UserNonce] PRIMARY KEY CLUSTERED ([UserID] ASC),
    CONSTRAINT [FK_UserNonce_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

