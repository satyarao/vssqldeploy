﻿CREATE TABLE [dbo].[WebHookEventNotifyConfig] (
    [WebHookConfigId] INT            NOT NULL,
    [EventType]       INT            NOT NULL,
    [ServiceType]     INT            NOT NULL,
    [NotifyStates]    NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [EventNotigyConfigKey] PRIMARY KEY CLUSTERED ([WebHookConfigId] ASC, [EventType] ASC, [ServiceType] ASC)
);

