﻿CREATE TABLE [dbo].[SqlDeployTest]
(
	[Id] INT NOT NULL , 
	[Code] VARCHAR(20) NOT NULL DEFAULT 'Test',
    [Name] NCHAR(10) NULL, 
    [Description] VARCHAR(500) NULL, 
    [Col1] NCHAR(10) NULL, 
    CONSTRAINT [PK_SqlDeployTest] PRIMARY KEY ([Id])
)
