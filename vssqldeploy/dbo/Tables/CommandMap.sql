﻿CREATE TABLE [dbo].[CommandMap] (
    [CommandMapID] INT              IDENTITY (1, 1) NOT NULL,
    [CommandType]  NVARCHAR (255)   NOT NULL,
    [CommandKey]   UNIQUEIDENTIFIER NOT NULL,
    [ResolverName] NVARCHAR (100)   NOT NULL
);

