﻿CREATE TABLE [dbo].[LKTenantOfferFilterType] (
    [TenantOfferFilterTypeID] INT           NOT NULL,
    [Name]                    NVARCHAR (50) NOT NULL,
    [IsActive]                BIT           CONSTRAINT [DF_LKTenantOfferFilterType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKTenantOfferFilterType] PRIMARY KEY CLUSTERED ([TenantOfferFilterTypeID] ASC)
);

