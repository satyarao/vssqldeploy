﻿CREATE TABLE [dbo].[MCX_LocationMapping] (
    [StoreID]           UNIQUEIDENTIFIER NOT NULL,
    [MCXFuelLocationId] NVARCHAR (50)    NOT NULL,
    [MCXPOSLocationId]  NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_MCX_LocationMapping] PRIMARY KEY CLUSTERED ([StoreID] ASC) WITH (FILLFACTOR = 80)
);

