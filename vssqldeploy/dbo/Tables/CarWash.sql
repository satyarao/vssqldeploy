﻿CREATE TABLE [dbo].[CarWash] (
    [StoreID]                  UNIQUEIDENTIFIER NOT NULL,
    [POSCode]                  NVARCHAR (50)    NOT NULL,
    [Modifier]                 NVARCHAR (10)    NULL,
    [POSCodeFormat]            NVARCHAR (10)    NULL,
    [Description]              NVARCHAR (MAX)   NOT NULL,
    [PaymentSystemProductCode] NVARCHAR (50)    NULL,
    [Amount]                   DECIMAL (19, 4)  NULL,
    [CurrencyCode]             NVARCHAR (10)    NULL,
    [UnitOfMeasure]            NVARCHAR (20)    NOT NULL,
    [UnitPrice]                DECIMAL (19, 4)  NULL,
    [TaxCode]                  NVARCHAR (20)    NULL,
    [IsActive]                 BIT              CONSTRAINT [DF_CarWash_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]                DATETIME2 (7)    CONSTRAINT [DF_CarWash_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]                DATETIME2 (7)    NULL,
    [Quantity]                 DECIMAL (19, 4)  NULL,
    [MerchandiseCode]          NVARCHAR (150)   NULL,
    [SellingUnits]             INT              DEFAULT ((1)) NOT NULL,
    [LineItemId]               NVARCHAR (150)   DEFAULT ('') NOT NULL,
    [CarWashCode]              NVARCHAR (150)   NULL,
    [CarWashExpirationDate]    DATETIME         NULL,
    [CarWashText]              NVARCHAR (MAX)   NULL,
    [StandardProductCode]      NVARCHAR (150)   NULL,
    CONSTRAINT [PK_CarWash] PRIMARY KEY CLUSTERED ([StoreID] ASC, [POSCode] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CarWash_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);


GO
ALTER TABLE [dbo].[CarWash] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);


GO

CREATE   TRIGGER [U_CarWash_UpdatedOn] ON [dbo].[CarWash] AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  UPDATE [dbo].[CarWash] SET UpdatedOn = getutcdate() FROM INSERTED
END
