﻿CREATE TABLE [dbo].[UserDeviceDetails] (
    [DeviceID]   UNIQUEIDENTIFIER NOT NULL,
    [TenantID]   UNIQUEIDENTIFIER NOT NULL,
    [UserID]     UNIQUEIDENTIFIER NOT NULL,
    [Platform]   NVARCHAR (250)   NULL,
    [OSVersion]  NVARCHAR (100)   NULL,
    [BundleID]   NVARCHAR (250)   NULL,
    [MACAddress] NVARCHAR (MAX)   NULL,
    [CreatedOn]  DATETIME2 (7)    CONSTRAINT [DF_UserDeviceDetails_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]  DATETIME2 (7)    NULL,
    CONSTRAINT [PK_UserDeviceDetails] PRIMARY KEY CLUSTERED ([DeviceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserDeviceDetails_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_UserDeviceDetails_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

