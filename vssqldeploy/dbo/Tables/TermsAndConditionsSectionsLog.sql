﻿CREATE TABLE [dbo].[TermsAndConditionsSectionsLog] (
    [SectionLogId] INT             IDENTITY (1, 1) NOT NULL,
    [SectionOrder] INT             NOT NULL,
    [SectionTitle] NVARCHAR (1000) NULL,
    [SectionText]  NVARCHAR (MAX)  NULL,
    [DetailLogId]  INT             NOT NULL,
    CONSTRAINT [PK_SectionsDetailsLog_SectionLogId] PRIMARY KEY CLUSTERED ([SectionLogId] ASC),
    CONSTRAINT [FK_TermsAndConditionsSectionsLog_TermsAndConditionsDetailsLog_DetailLogId] FOREIGN KEY ([DetailLogId]) REFERENCES [dbo].[TermsAndConditionsDetailsLog] ([DetailLogId]) ON DELETE CASCADE
);

