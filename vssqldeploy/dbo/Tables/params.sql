﻿CREATE TABLE [dbo].[params] (
    [EnvironmentName]    VARCHAR (9)    NOT NULL,
    [TenantID]           VARCHAR (36)   NOT NULL,
    [APIVersion]         NVARCHAR (50)  NOT NULL,
    [Command]            NVARCHAR (50)  NOT NULL,
    [URI]                NVARCHAR (250) NULL,
    [HttpHeader]         NVARCHAR (250) NULL,
    [EnvironmentPurpose] NVARCHAR (50)  NOT NULL,
    [CreatedOn]          DATETIME2 (7)  NOT NULL,
    [EndPointID]         INT            IDENTITY (1, 1) NOT NULL,
    [IsActive]           BIT            NOT NULL
);

