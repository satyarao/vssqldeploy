﻿CREATE TABLE [dbo].[BasketMppaBatchMapping] (
    [BasketId]        UNIQUEIDENTIFIER NOT NULL,
    [MppaBatchNumber] INT              NOT NULL,
    CONSTRAINT [PK_BasketMppaBatchMapping] PRIMARY KEY CLUSTERED ([BasketId] ASC, [MppaBatchNumber] ASC) WITH (FILLFACTOR = 80)
);

