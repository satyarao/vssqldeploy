﻿CREATE TABLE [dbo].[PaymentSystemProductGroup] (
    [ProductGroupID] TINYINT        IDENTITY (1, 1) NOT NULL,
    [GroupCode]      NCHAR (3)      NOT NULL,
    [GradeName]      NVARCHAR (MAX) NOT NULL,
    [Deprecated]     BIT            NOT NULL,
    CONSTRAINT [PK_PaymentSystemProductGroup] PRIMARY KEY CLUSTERED ([ProductGroupID] ASC),
    CONSTRAINT [UK_GroupCode] UNIQUE NONCLUSTERED ([GroupCode] ASC)
);

