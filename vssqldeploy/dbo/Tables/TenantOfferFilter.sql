﻿CREATE TABLE [dbo].[TenantOfferFilter] (
    [TenantOfferFilterID]     INT              NOT NULL,
    [AppTenantID]             UNIQUEIDENTIFIER NOT NULL,
    [TenantOfferFilterTypeID] INT              NOT NULL,
    [PartnerTenantId]         UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                BIT              NOT NULL,
    CONSTRAINT [PK_TenantOfferFilterID] PRIMARY KEY CLUSTERED ([TenantOfferFilterID] ASC),
    CONSTRAINT [FK_TenantOfferFilter_AppTenantID] FOREIGN KEY ([AppTenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_TenantOfferFilter_PartnerTenantID] FOREIGN KEY ([PartnerTenantId]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_TenantOfferFilter_TenantOfferFilterTypeID] FOREIGN KEY ([TenantOfferFilterTypeID]) REFERENCES [dbo].[LKTenantOfferFilterType] ([TenantOfferFilterTypeID]),
    CONSTRAINT [UQ_TenantRelationshipMap] UNIQUE NONCLUSTERED ([AppTenantID] ASC, [PartnerTenantId] ASC)
);

