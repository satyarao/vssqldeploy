﻿CREATE TABLE [dbo].[GroupChilds] (
    [Key]          INT              IDENTITY (1, 1) NOT NULL,
    [ParentKey]    INT              NOT NULL,
    [ChildGroupID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_dbo.GroupChilds] PRIMARY KEY CLUSTERED ([Key] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_dbo.GroupChilds_dbo.Groups_ParentKey] FOREIGN KEY ([ParentKey]) REFERENCES [dbo].[Groups] ([Key]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentKey]
    ON [dbo].[GroupChilds]([ParentKey] ASC) WITH (FILLFACTOR = 80);

