﻿CREATE TABLE [dbo].[MobileAppVersion] (
    [AppTenantId]   UNIQUEIDENTIFIER NOT NULL,
    [AppVersion]    NVARCHAR (82)    NOT NULL,
    [CreatedOn]     DATETIME2 (7)    NOT NULL,
    [UpdatedByUser] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_AppTenantId_AppVersion] PRIMARY KEY CLUSTERED ([AppTenantId] ASC, [AppVersion] ASC)
);

