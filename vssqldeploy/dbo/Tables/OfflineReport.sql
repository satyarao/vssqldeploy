﻿CREATE TABLE [dbo].[OfflineReport] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [ReportType]  INT              NOT NULL,
    [Arguments]   NVARCHAR (MAX)   NOT NULL,
    [BlobName]    NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]   DATETIME2 (7)    NOT NULL,
    [PublicToken] UNIQUEIDENTIFIER NOT NULL
);

