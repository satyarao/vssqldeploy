﻿CREATE TABLE [dbo].[CustomProperty] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (255)   NOT NULL,
    [Key]              NVARCHAR (255)   NOT NULL,
    [Description]      NVARCHAR (MAX)   NULL,
    [DataObjectTypeID] TINYINT          NOT NULL,
    [FieldTypeID]      TINYINT          NOT NULL,
    [IsRequired]       BIT              CONSTRAINT [DF_CustomProperty_IsRequired] DEFAULT ((0)) NOT NULL,
    [DataCollectionID] TINYINT          NULL,
    [AccessID]         TINYINT          NULL,
    [TenantID]         UNIQUEIDENTIFIER NULL,
    [Options]          NVARCHAR (MAX)   NULL,
    [IsActive]         BIT              CONSTRAINT [DF_CustomProperty_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy]        NVARCHAR (50)    NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [UpdatedBy]        NVARCHAR (50)    NULL,
    [UpdatedOn]        DATETIME2 (7)    NULL,
    CONSTRAINT [PK_FundingProviderGroup] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [CK_CustomProperty_Options_IsJson] CHECK (isjson([Options])>(0)),
    CONSTRAINT [FK_CustomProperty_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Tenant_Key_DataObjectType]
    ON [dbo].[CustomProperty]([TenantID] ASC, [Key] ASC, [DataObjectTypeID] ASC) WHERE ([IsActive]=(1));

