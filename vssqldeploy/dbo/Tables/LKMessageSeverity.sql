﻿CREATE TABLE [dbo].[LKMessageSeverity] (
    [SeverityID]  TINYINT       NOT NULL,
    [Description] NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_SEVERITY] PRIMARY KEY CLUSTERED ([SeverityID] ASC) WITH (FILLFACTOR = 80)
);

