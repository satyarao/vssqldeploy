﻿CREATE TABLE [dbo].[ExcentusMemberTokens] (
    [AccountNumber]       INT                NOT NULL,
    [MemberId]            NVARCHAR (MAX)     NOT NULL,
    [MemberAccessToken]   NVARCHAR (MAX)     NOT NULL,
    [TokenExpirationDate] DATETIMEOFFSET (7) NOT NULL,
    [MemberRefreshToken]  NVARCHAR (MAX)     NOT NULL,
    [UserId]              UNIQUEIDENTIFIER   NOT NULL,
    CONSTRAINT [PK_ExcentusMemberTokens_UserId] PRIMARY KEY NONCLUSTERED ([UserId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [AccountNumber_index]
    ON [dbo].[ExcentusMemberTokens]([AccountNumber] ASC);

