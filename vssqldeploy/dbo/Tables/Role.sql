﻿CREATE TABLE [dbo].[Role] (
    [RoleID]          UNIQUEIDENTIFIER NOT NULL,
    [Name]            NVARCHAR (50)    NOT NULL,
    [Description]     NVARCHAR (MAX)   NULL,
    [ApplicationName] NVARCHAR (256)   NOT NULL,
    [CreatedOn]       DATETIME2 (7)    NOT NULL,
    [CreatedBy]       NVARCHAR (50)    NOT NULL,
    [UpdatedOn]       DATETIME2 (7)    NULL,
    [UpdatedBy]       NVARCHAR (50)    NULL,
    [IsActive]        BIT              NOT NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([RoleID] ASC) WITH (FILLFACTOR = 80)
);

