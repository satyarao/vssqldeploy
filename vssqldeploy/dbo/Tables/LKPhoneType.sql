﻿CREATE TABLE [dbo].[LKPhoneType] (
    [PhoneTypeID] INT           NOT NULL,
    [Name]        NVARCHAR (10) NOT NULL,
    [IsActive]    BIT           NOT NULL,
    CONSTRAINT [PK_PhoneType_1] PRIMARY KEY CLUSTERED ([PhoneTypeID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PhoneType]
    ON [dbo].[LKPhoneType]([Name] ASC) WITH (FILLFACTOR = 80);

