﻿CREATE TABLE [dbo].[SigV4] (
    [Id]                INT              IDENTITY (1, 1) NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NOT NULL,
    [KeyId]             NVARCHAR (255)   NOT NULL,
    [AccessKey]         NVARCHAR (MAX)   NOT NULL,
    [AccessControlList] NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]         DATETIME2 (7)    NOT NULL,
    [IsActive]          BIT              NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [SigV4_AccessControlList_should_be_JSON] CHECK (isjson([AccessControlList])>(0))
);

