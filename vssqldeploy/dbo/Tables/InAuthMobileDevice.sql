﻿CREATE TABLE [dbo].[InAuthMobileDevice] (
    [InAuthMobileDeviceID] INT              IDENTITY (1, 1) NOT NULL,
    [DeviceID]             UNIQUEIDENTIFIER NOT NULL,
    [UserID]               UNIQUEIDENTIFIER NOT NULL,
    [TenantID]             UNIQUEIDENTIFIER NOT NULL,
    [InAuthPermanentID]    NVARCHAR (450)   NOT NULL,
    [DeviceResponse]       NVARCHAR (MAX)   NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_InAuthMobileDevice_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_InAuthMobileDevice] PRIMARY KEY CLUSTERED ([InAuthMobileDeviceID] ASC),
    CONSTRAINT [FK_InAuthMobileDevice_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_InAuthMobileDevice_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [Id_Unique_InAuthMobileDevice_InAuthPermanentID] UNIQUE NONCLUSTERED ([InAuthPermanentID] ASC)
);

