﻿CREATE TABLE [dbo].[Verification] (
    [VerificationID]   INT            IDENTITY (1, 1) NOT NULL,
    [VerificationCode] NVARCHAR (50)  NOT NULL,
    [ExpirationDate]   DATETIME2 (7)  NOT NULL,
    [IsVerified]       BIT            CONSTRAINT [DF_Verification_IsVerified] DEFAULT ((0)) NOT NULL,
    [IsActive]         BIT            NOT NULL,
    [CreatedBy]        NVARCHAR (50)  NULL,
    [CreatedOn]        DATETIME2 (7)  CONSTRAINT [DF_Verification_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        NVARCHAR (50)  NULL,
    [UpdatedOn]        DATETIME2 (7)  NULL,
    [AttemptCount]     INT            DEFAULT ((0)) NOT NULL,
    [Email]            NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Verification] PRIMARY KEY CLUSTERED ([VerificationID] ASC) WITH (FILLFACTOR = 80)
);

