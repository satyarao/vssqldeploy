﻿CREATE TABLE [dbo].[JanrainAccessCode] (
    [AccessCode] NVARCHAR (50)    NOT NULL,
    [UserID]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]  DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [ExpiresOn]  DATETIME2 (7)    NOT NULL,
    [IsActive]   BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_JanrainAccessCode] PRIMARY KEY CLUSTERED ([AccessCode] ASC),
    CONSTRAINT [FK_JanrainAccessCode_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

