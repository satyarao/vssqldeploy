﻿CREATE TABLE [dbo].[PromoCards] (
    [PromoID]               UNIQUEIDENTIFIER CONSTRAINT [DF_PromoCards_PromoID] DEFAULT (newid()) NOT NULL,
    [TenantID]              UNIQUEIDENTIFIER NOT NULL,
    [Target]                NVARCHAR (10)    NOT NULL,
    [Title]                 NVARCHAR (120)   NOT NULL,
    [Subtitle]              NVARCHAR (120)   NOT NULL,
    [Description]           NVARCHAR (MAX)   NULL,
    [ImageURL]              NVARCHAR (1024)  NOT NULL,
    [TemplateImagePosition] NVARCHAR (10)    NOT NULL,
    [ExpirationDate]        DATETIME2 (7)    NULL,
    [Order]                 INT              CONSTRAINT [DF_PromoCards_Order] DEFAULT ((100)) NOT NULL,
    [StoreRulesPayload]     NVARCHAR (MAX)   NULL,
    [LinksPayload]          NVARCHAR (MAX)   DEFAULT ('[]') NOT NULL,
    CONSTRAINT [PK_PromoCards] PRIMARY KEY CLUSTERED ([PromoID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_PromoCards_LinksPayload_IsJson] CHECK (isjson([LinksPayload])>(0)),
    CONSTRAINT [CK_PromoCards_StoreRulesPayload_IsJson] CHECK (isjson([StoreRulesPayload])>(0)),
    CONSTRAINT [FK_PromoCards_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

