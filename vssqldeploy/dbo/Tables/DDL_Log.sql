﻿CREATE TABLE [dbo].[DDL_Log] (
    [EventType]      NVARCHAR (100) NULL,
    [TSQLCommand]    NVARCHAR (MAX) NULL,
    [PostTime]       DATETIME2 (2)  NULL,
    [DB_User]        NVARCHAR (100) NULL,
    [AppName]        NVARCHAR (200) NULL,
    [ObjectSchema]   VARCHAR (50)   NULL,
    [ObjectName]     VARCHAR (200)  NULL,
    [CheckInUser]    VARCHAR (50)   NULL,
    [CheckInComment] VARCHAR (500)  NULL,
    [ScriptFileName] VARCHAR (500)  NULL
);

