﻿CREATE TABLE [dbo].[WebHookConfig] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [TargetId]         UNIQUEIDENTIFIER NOT NULL,
    [SendHook]         BIT              NOT NULL,
    [URL]              NVARCHAR (MAX)   NOT NULL,
    [VerboseDetail]    BIT              NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [UpdatedOn]        DATETIME2 (7)    NULL,
    [IsActive]         BIT              NOT NULL,
    [HeaderDictionary] NVARCHAR (MAX)   NULL,
    [HookType]         NVARCHAR (MAX)   NOT NULL
);

