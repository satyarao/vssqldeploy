﻿CREATE TABLE [dbo].[JwtTestUsers] (
    [UserID]       UNIQUEIDENTIFIER NOT NULL,
    [TenantID]     UNIQUEIDENTIFIER NOT NULL,
    [EmailAddress] NVARCHAR (255)   NOT NULL,
    [Passphrase]   NVARCHAR (255)   NOT NULL,
    [ObjectID]     NVARCHAR (255)   NULL,
    CONSTRAINT [PK_JwtTestUsers] PRIMARY KEY CLUSTERED ([UserID] ASC, [TenantID] ASC),
    CONSTRAINT [FK_JwtTestUsers_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_JwtTestUsers_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

