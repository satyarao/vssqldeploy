﻿CREATE TABLE [dbo].[ApiClient] (
    [ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_ApiClients_ID] DEFAULT (newid()) NOT NULL,
    [Name]      NVARCHAR (250)   NOT NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_ApiClient_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy] NVARCHAR (50)    NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    [UpdatedBy] NVARCHAR (50)    NULL,
    CONSTRAINT [PK_ApiClients] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

