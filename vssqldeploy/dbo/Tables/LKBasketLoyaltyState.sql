﻿CREATE TABLE [dbo].[LKBasketLoyaltyState] (
    [BasketLoyaltyStateID] TINYINT       NOT NULL,
    [Name]                 NVARCHAR (50) NOT NULL,
    [IsActive]             BIT           CONSTRAINT [DF_LKBasketLoyaltyState_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKBasketLoyaltyState] PRIMARY KEY CLUSTERED ([BasketLoyaltyStateID] ASC)
);

