﻿CREATE TABLE [dbo].[LKProductConfig] (
    [ProductConfigID] INT           NOT NULL,
    [ProductConfig]   NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_LKProductConfig] PRIMARY KEY CLUSTERED ([ProductConfigID] ASC) WITH (FILLFACTOR = 80)
);

