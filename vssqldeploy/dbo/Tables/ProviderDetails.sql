﻿CREATE TABLE [dbo].[ProviderDetails] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [TenantId]      UNIQUEIDENTIFIER NOT NULL,
    [Provider]      NVARCHAR (255)   NOT NULL,
    [URL]           NVARCHAR (MAX)   NULL,
    [ContactText]   NVARCHAR (MAX)   NULL,
    [ContactNumber] NVARCHAR (MAX)   NULL,
    [ContactEmail]  NVARCHAR (MAX)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

