﻿CREATE TABLE [dbo].[StoreBillableLog] (
    [ID]                   INT              IDENTITY (1, 1) NOT NULL,
    [StoreID]              UNIQUEIDENTIFIER NOT NULL,
    [TenantID]             UNIQUEIDENTIFIER NOT NULL,
    [LastActivationDate]   DATETIME2 (7)    NULL,
    [LastDeactivationDate] DATETIME2 (7)    NULL,
    CONSTRAINT [PK_StoreBillableStateLog] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_StoreBillableLog_StoreID] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID]),
    CONSTRAINT [FK_StoreBillableLog_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [UK_Tenant_Store] UNIQUE NONCLUSTERED ([StoreID] ASC, [TenantID] ASC) WITH (FILLFACTOR = 80)
);

