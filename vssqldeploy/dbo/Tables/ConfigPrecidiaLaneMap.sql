﻿CREATE TABLE [dbo].[ConfigPrecidiaLaneMap] (
    [TenantID]      UNIQUEIDENTIFIER NOT NULL,
    [POSTerminalID] NVARCHAR (50)    NOT NULL,
    [Lane]          NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_ConfigPrecidiaLaneMap] PRIMARY KEY CLUSTERED ([TenantID] ASC, [POSTerminalID] ASC) WITH (FILLFACTOR = 80)
);

