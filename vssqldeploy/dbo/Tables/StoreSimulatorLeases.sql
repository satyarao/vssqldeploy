﻿CREATE TABLE [dbo].[StoreSimulatorLeases] (
    [StoreSimulatorLeaseID] INT              IDENTITY (1, 1) NOT NULL,
    [StoreId]               UNIQUEIDENTIFIER NOT NULL,
    [UserId]                UNIQUEIDENTIFIER NOT NULL,
    [LeaseEndDate]          DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_StoreSimulatorLeases] PRIMARY KEY NONCLUSTERED ([StoreSimulatorLeaseID] ASC),
    CONSTRAINT [UC_StoreSimulatorLeases_StoreId] UNIQUE NONCLUSTERED ([StoreId] ASC),
    CONSTRAINT [UC_StoreSimulatorLeases_UserId] UNIQUE NONCLUSTERED ([UserId] ASC)
);

