﻿CREATE TABLE [dbo].[RuleFilter] (
    [RuleFilterID]     UNIQUEIDENTIFIER NOT NULL,
    [TenantID]         UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterTypeID] TINYINT          NOT NULL,
    [Name]             NVARCHAR (100)   NOT NULL,
    [Description]      NVARCHAR (100)   NOT NULL,
    [CreatedOn]        DATETIME2 (7)    CONSTRAINT [DF_RuleFilter_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]        DATETIME2 (7)    NULL,
    [Discriminator]    NVARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_RuleFilter] PRIMARY KEY CLUSTERED ([RuleFilterID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_RuleFilter_LKRuleFilterType] FOREIGN KEY ([RuleFilterTypeID]) REFERENCES [dbo].[LKRuleFilterType] ([RuleFilterTypeID]),
    CONSTRAINT [FK_RuleFilter_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

