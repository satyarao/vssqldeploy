﻿CREATE TABLE [dbo].[InAuthLog] (
    [LogID]                UNIQUEIDENTIFIER NOT NULL,
    [InAuthMobileDeviceID] INT              NOT NULL,
    [DeviceResponse]       NVARCHAR (MAX)   NULL,
    [DeviceIpAddress]      NVARCHAR (255)   NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_InAuthLog_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_InAuthLog] PRIMARY KEY CLUSTERED ([LogID] ASC),
    CONSTRAINT [FK_InAuthLog_InAuthMobileDevice] FOREIGN KEY ([InAuthMobileDeviceID]) REFERENCES [dbo].[InAuthMobileDevice] ([InAuthMobileDeviceID])
);

