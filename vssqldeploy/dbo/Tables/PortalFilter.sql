﻿CREATE TABLE [dbo].[PortalFilter] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [FilterName]         NVARCHAR (256)   NOT NULL,
    [TenantID]           UNIQUEIDENTIFIER NULL,
    [UserID]             UNIQUEIDENTIFIER NULL,
    [IsPrivate]          BIT              CONSTRAINT [DF_PortalFilter_IsPrivate] DEFAULT ((0)) NOT NULL,
    [FilterTypeID]       TINYINT          NOT NULL,
    [CreatedBy]          NVARCHAR (128)   NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_PortalFilter_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]          NVARCHAR (128)   NULL,
    [UpdatedOn]          DATETIME2 (7)    NULL,
    [IsActive]           BIT              CONSTRAINT [DF_PortalFilter_IsActive] DEFAULT ((1)) NOT NULL,
    [VisibleByUsers]     NVARCHAR (MAX)   CONSTRAINT [DF_PortalFilter_VisibleByUsers] DEFAULT ('[]') NOT NULL,
    [AvailableForOffers] BIT              CONSTRAINT [DF_PortalFilter_AvailableForOffers] DEFAULT ((1)) NOT NULL,
    [IsDefault]          BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PortalFilter] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [PortalFilter_VisibleByUsers_should_be_JSON] CHECK (isjson([VisibleByUsers])>(0)),
    CONSTRAINT [FK_PortalFilter_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_PortalFilter_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

