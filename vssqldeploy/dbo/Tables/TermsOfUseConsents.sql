﻿CREATE TABLE [dbo].[TermsOfUseConsents] (
    [TermsOfUseConsentID] INT            IDENTITY (1, 1) NOT NULL,
    [TermsOfUseID]        INT            NOT NULL,
    [ObjectID]            NVARCHAR (255) NOT NULL,
    [DateTime]            DATETIME       NOT NULL,
    [Consent]             BIT            NOT NULL
);

