﻿CREATE TABLE [dbo].[InterstitialZoneInfo] (
    [ZoneID]                UNIQUEIDENTIFIER NOT NULL,
    [ZoneName]              NVARCHAR (100)   NOT NULL,
    [TenantID]              UNIQUEIDENTIFIER NOT NULL,
    [MaxOffersDisplayCount] INT              NULL,
    [CreatedOn]             DATETIME2 (7)    CONSTRAINT [DF_InterstitialZoneInfo_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]             DATETIME2 (7)    NULL,
    CONSTRAINT [PK_InterstitialZoneInfo] PRIMARY KEY CLUSTERED ([ZoneID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_InterstitialZoneInfo_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

