﻿CREATE TABLE [dbo].[Basket] (
    [BasketID]              UNIQUEIDENTIFIER NOT NULL,
    [UserID]                UNIQUEIDENTIFIER NULL,
    [StoreID]               UNIQUEIDENTIFIER NOT NULL,
    [POSStoreID]            NVARCHAR (50)    NOT NULL,
    [POSOperatorID]         NVARCHAR (50)    NOT NULL,
    [POSTransactionID]      NVARCHAR (50)    NOT NULL,
    [POSTerminalID]         NVARCHAR (50)    NOT NULL,
    [POSTillID]             NVARCHAR (50)    NOT NULL,
    [POSDateTime]           DATETIME2 (7)    NOT NULL,
    [POSDateTimeLocal]      DATETIME2 (7)    NULL,
    [SubTotal]              DECIMAL (9, 2)   NULL,
    [TaxAmount]             DECIMAL (9, 2)   NULL,
    [Total]                 DECIMAL (9, 2)   NULL,
    [BasketStateID]         TINYINT          NOT NULL,
    [QRCodeText]            NVARCHAR (1024)  NULL,
    [CreatedOn]             DATETIME2 (7)    CONSTRAINT [DF_Basket_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]             DATETIME2 (7)    NULL,
    [BatchID]               INT              NULL,
    [PartnerTransactionID]  NVARCHAR (100)   NULL,
    [StacString]            NVARCHAR (50)    NULL,
    [StoreTenantID]         UNIQUEIDENTIFIER NOT NULL,
    [IsSiteBillable]        BIT              NOT NULL,
    [LoyaltyTransactionId]  NVARCHAR (50)    NULL,
    [ItemRewardTotal]       DECIMAL (9, 2)   NULL,
    [FuelRewardTotal]       DECIMAL (9, 2)   NULL,
    [PreRewardSubTotal]     DECIMAL (9, 2)   NULL,
    [AppTenantID]           UNIQUEIDENTIFIER NULL,
    [BasketLoyaltyStateID]  TINYINT          NOT NULL,
    [BasketLoyaltyPrograms] NVARCHAR (MAX)   CONSTRAINT [DF_Basket_BasketLoyaltyPrograms] DEFAULT ('[]') NOT NULL,
    [AppDisplayName]        NVARCHAR (255)   DEFAULT ('Not Set') NOT NULL,
    [AppChannel]            NVARCHAR (100)   DEFAULT ('WhiteLabelMobile') NOT NULL,
    [FleetTenantID]         UNIQUEIDENTIFIER NULL,
    [OtherRewardTotal]      DECIMAL (9, 2)   NULL,
    [CurrencyIsoCode]       CHAR (3)         DEFAULT ('USD') NOT NULL,
    CONSTRAINT [PK_Basket] PRIMARY KEY CLUSTERED ([BasketID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [Basket_BasketLoyaltyPrograms_isJSON] CHECK (isjson([BasketLoyaltyPrograms])>(0)),
    CONSTRAINT [FK_Basket_LKBasketLoyaltyState] FOREIGN KEY ([BasketLoyaltyStateID]) REFERENCES [dbo].[LKBasketLoyaltyState] ([BasketLoyaltyStateID]),
    CONSTRAINT [FK_Basket_LKBasketState] FOREIGN KEY ([BasketStateID]) REFERENCES [dbo].[LKBasketState] ([BasketStateID]),
    CONSTRAINT [FK_Basket_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Basket_POSDateTime]
    ON [dbo].[Basket]([POSDateTime] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Basket_StoreID]
    ON [dbo].[Basket]([StoreID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [nci_wi_Basket_2E76843819E8855A735D3673CAA7F781]
    ON [dbo].[Basket]([UserID] ASC, [POSDateTime] ASC)
    INCLUDE([BasketID], [BasketStateID], [POSDateTimeLocal], [POSOperatorID], [POSTransactionID], [StoreID], [Total]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_Basket_POSDateTime_BasketStateID]
    ON [dbo].[Basket]([POSDateTime] ASC, [BasketStateID] ASC);


GO
CREATE NONCLUSTERED INDEX [Reports_Basket_StoreID_StoreTenantID_IsSiteBillable_CreatedOn]
    ON [dbo].[Basket]([StoreID] ASC, [StoreTenantID] ASC, [IsSiteBillable] ASC, [CreatedOn] ASC);


GO
CREATE NONCLUSTERED INDEX [Reports_Basket_StoreID_BasketStateID_StoreTenantID_IsSiteBillable_CreatedOn]
    ON [dbo].[Basket]([StoreID] ASC, [BasketStateID] ASC, [StoreTenantID] ASC, [IsSiteBillable] ASC, [CreatedOn] ASC);


GO

CREATE TRIGGER [dbo].[Basket_Insert_Update] ON [dbo].[Basket]
	FOR INSERT, UPDATE
AS
SET NOCOUNT ON

IF EXISTS (SELECT * FROM DELETED) -- We are in the update trigger
begin
	INSERT	INTO dbo.BasketLog
		SELECT	*
		FROM	deleted
END
