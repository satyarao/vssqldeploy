﻿CREATE TABLE [dbo].[StoreService] (
    [StoreID]   UNIQUEIDENTIFIER NOT NULL,
    [ServiceID] INT              NOT NULL,
    [IsActive]  BIT              CONSTRAINT [DF_StoreService_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy] NVARCHAR (50)    NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_StoreService_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy] NVARCHAR (50)    NULL,
    [UpdatedOn] DATETIME2 (7)    NULL,
    CONSTRAINT [PK_StoreService] PRIMARY KEY CLUSTERED ([StoreID] ASC, [ServiceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_StoreService_Service] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[LKService] ([ServiceID]),
    CONSTRAINT [FK_StoreServices_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);


GO
ALTER TABLE [dbo].[StoreService] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

