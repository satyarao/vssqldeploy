﻿CREATE TABLE [dbo].[MppaSource] (
    [SourceId]  INT            NOT NULL,
    [SorceName] NVARCHAR (MAX) NOT NULL,
    [IsActive]  BIT            CONSTRAINT [DF_MppaSource_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy] NVARCHAR (50)  NULL,
    [CreatedOn] DATETIME2 (7)  CONSTRAINT [DF_MppaSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy] NVARCHAR (50)  NULL,
    [UpdatedOn] DATETIME2 (7)  NULL,
    CONSTRAINT [PK_MppaSource] PRIMARY KEY CLUSTERED ([SourceId] ASC) WITH (FILLFACTOR = 80)
);

