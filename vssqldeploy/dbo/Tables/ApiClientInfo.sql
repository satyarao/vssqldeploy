﻿CREATE TABLE [dbo].[ApiClientInfo] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [ApiClientID]     UNIQUEIDENTIFIER NOT NULL,
    [HostName]        NVARCHAR (250)   NOT NULL,
    [ApiVersion]      INT              NOT NULL,
    [MacAddress]      NVARCHAR (150)   NOT NULL,
    [InstallDateTime] DATETIME2 (7)    NOT NULL,
    [RestartDateTime] DATETIME2 (7)    NULL,
    [IsRunning]       BIT              CONSTRAINT [DF_ApiClientInfo_IsRunning] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ApiClientInfo_1] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ApiClientInfo_ApiClient] FOREIGN KEY ([ApiClientID]) REFERENCES [dbo].[ApiClient] ([ID])
);

