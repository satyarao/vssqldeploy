﻿CREATE TABLE [dbo].[OfferCategory] (
    [CategoryID]   UNIQUEIDENTIFIER NOT NULL,
    [CategoryName] NVARCHAR (255)   NOT NULL,
    [TenantID]     UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([CategoryID] ASC)
);

