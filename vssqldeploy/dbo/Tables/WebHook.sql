﻿CREATE TABLE [dbo].[WebHook] (
    [WebHookId]        INT            IDENTITY (1, 1) NOT NULL,
    [SendHook]         BIT            NOT NULL,
    [URL]              NVARCHAR (MAX) NOT NULL,
    [VerboseDetail]    BIT            NOT NULL,
    [NotifyStates]     NVARCHAR (MAX) NOT NULL,
    [CreatedOn]        DATETIME2 (7)  NOT NULL,
    [UpdatedOn]        DATETIME2 (7)  NULL,
    [IsActive]         BIT            NOT NULL,
    [HeaderDictionary] NVARCHAR (MAX) NULL,
    [HookType]         NVARCHAR (100) CONSTRAINT [DF_WebHook_HookType] DEFAULT ('p97v1') NOT NULL,
    CONSTRAINT [PK_WebHook] PRIMARY KEY CLUSTERED ([WebHookId] ASC)
);

