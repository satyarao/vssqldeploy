﻿CREATE TABLE [dbo].[SRP] (
    [InstallationID]                NVARCHAR (128) NOT NULL,
    [Salt]                          NVARCHAR (MAX) NOT NULL,
    [SafePrime]                     NVARCHAR (MAX) NOT NULL,
    [Generator]                     NVARCHAR (MAX) NOT NULL,
    [Verifier]                      NVARCHAR (MAX) NOT NULL,
    [SessionTokenVerifier]          NVARCHAR (MAX) NULL,
    [EphemeralSessionTokenVerifier] NVARCHAR (MAX) NULL,
    [PrivateServerEphemeral1]       NVARCHAR (MAX) NULL,
    [PrivateServerEphemeral2]       NVARCHAR (MAX) NULL,
    [PublicServerEphemeral1]        NVARCHAR (MAX) NULL,
    [PublicServerEphemeral2]        NVARCHAR (MAX) NULL,
    [RemainingAttempts]             INT            CONSTRAINT [DF__SRP__RemainingAt__5CD6CB2B] DEFAULT ((3)) NOT NULL,
    [LockedOn]                      DATETIME2 (7)  NULL,
    [IsActive]                      BIT            CONSTRAINT [DF_SRP_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedBy]                     NVARCHAR (MAX) NOT NULL,
    [CreatedOn]                     DATETIME2 (7)  CONSTRAINT [DF_SRP_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                     NVARCHAR (MAX) NULL,
    [UpdatedOn]                     DATETIME2 (7)  NULL,
    CONSTRAINT [PK_SRP] PRIMARY KEY CLUSTERED ([InstallationID] ASC) WITH (FILLFACTOR = 80)
);

