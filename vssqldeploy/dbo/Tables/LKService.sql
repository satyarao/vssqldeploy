﻿CREATE TABLE [dbo].[LKService] (
    [ServiceID] INT              NOT NULL,
    [Name]      NVARCHAR (255)   NOT NULL,
    [IsActive]  BIT              NOT NULL,
    [TenantId]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([ServiceID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Service]
    ON [dbo].[LKService]([Name] ASC) WITH (FILLFACTOR = 80);

