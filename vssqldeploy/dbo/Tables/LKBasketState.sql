﻿CREATE TABLE [dbo].[LKBasketState] (
    [BasketStateID] TINYINT       NOT NULL,
    [Name]          NVARCHAR (50) NOT NULL,
    [IsActive]      BIT           CONSTRAINT [DF_LKBasketState_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKTransactionState] PRIMARY KEY CLUSTERED ([BasketStateID] ASC) WITH (FILLFACTOR = 80)
);

