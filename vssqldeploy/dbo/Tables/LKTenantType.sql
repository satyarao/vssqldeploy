﻿CREATE TABLE [dbo].[LKTenantType] (
    [TenantTypeID] INT           NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [IsSystem]     BIT           CONSTRAINT [DF_LKTenantType_IsSystem] DEFAULT ((0)) NOT NULL,
    [IsActive]     BIT           CONSTRAINT [DF_LKTenantType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKTenantType] PRIMARY KEY CLUSTERED ([TenantTypeID] ASC) WITH (FILLFACTOR = 80)
);

