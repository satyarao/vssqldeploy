﻿CREATE TABLE [dbo].[LKSiteServices] (
    [ServiceId]   INT           IDENTITY (1, 1) NOT NULL,
    [ServiceName] VARCHAR (100) NOT NULL,
    [CreatedBy]   VARCHAR (50)  CONSTRAINT [DF_LKSiteServices_CreatedBy] DEFAULT (suser_name()) NOT NULL,
    [CreatedDate] DATETIME      CONSTRAINT [DF_LKSiteServices_CreatedOn] DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([ServiceId] ASC)
);

