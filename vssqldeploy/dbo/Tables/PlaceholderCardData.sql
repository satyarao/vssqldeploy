﻿CREATE TABLE [dbo].[PlaceholderCardData] (
    [Type]          INT              NOT NULL,
    [PlaceholderID] UNIQUEIDENTIFIER NOT NULL,
    [Target]        NVARCHAR (10)    NOT NULL,
    [Order]         INT              NOT NULL,
    [TenantIds]     NVARCHAR (1024)  NULL,
    CONSTRAINT [PK_PlaceholderCardData] PRIMARY KEY CLUSTERED ([Type] ASC),
    CONSTRAINT [Placeholder_TenantIds_should_be_JSON] CHECK (isjson([TenantIds])>(0)),
    CONSTRAINT [UQ__Placehol__4881DE7D48B68E17] UNIQUE NONCLUSTERED ([PlaceholderID] ASC)
);

