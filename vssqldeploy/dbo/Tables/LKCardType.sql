﻿CREATE TABLE [dbo].[LKCardType] (
    [CardTypeID] SMALLINT      IDENTITY (1, 1) NOT NULL,
    [CardType]   NVARCHAR (50) NULL,
    [RegEx]      NVARCHAR (50) NOT NULL,
    [CardIssuer] NVARCHAR (50) NOT NULL,
    [IsActive]   BIT           CONSTRAINT [DF_LKCardType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKCardType] PRIMARY KEY CLUSTERED ([CardTypeID] ASC) WITH (FILLFACTOR = 80)
);

