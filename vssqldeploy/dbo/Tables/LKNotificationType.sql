﻿CREATE TABLE [dbo].[LKNotificationType] (
    [NotificationTypeID] INT            NOT NULL,
    [Description]        NVARCHAR (MAX) NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF_LKNotificationType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_dbo.LKNotificationType] PRIMARY KEY CLUSTERED ([NotificationTypeID] ASC) WITH (FILLFACTOR = 80)
);

