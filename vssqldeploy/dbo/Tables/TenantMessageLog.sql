﻿CREATE TABLE [dbo].[TenantMessageLog] (
    [TenantMessageLogID] INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]           UNIQUEIDENTIFIER NOT NULL,
    [CorrelationID]      UNIQUEIDENTIFIER NULL,
    [SequenceNumber]     INT              NULL,
    [SeverityID]         TINYINT          NOT NULL,
    [CategoryID]         INT              NULL,
    [MessageText]        NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_TenantMessageLog_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_MESSAGE_LOG] PRIMARY KEY CLUSTERED ([TenantMessageLogID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_MESSAGE_LOG_SEVERITY] FOREIGN KEY ([SeverityID]) REFERENCES [dbo].[LKMessageSeverity] ([SeverityID]),
    CONSTRAINT [FK_TenantMessageLog_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

