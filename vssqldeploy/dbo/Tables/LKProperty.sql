﻿CREATE TABLE [dbo].[LKProperty] (
    [PropertyName]    NVARCHAR (128) NOT NULL,
    [TenantTypeID]    INT            NULL,
    [Ascend]          BIT            CONSTRAINT [DF_LKProperty_Ascend] DEFAULT ((1)) NOT NULL,
    [DefaultValue]    NVARCHAR (MAX) NULL,
    [PropertyComment] NVARCHAR (MAX) NULL,
    [IsActive]        BIT            CONSTRAINT [DF_LKProperty_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LKProperty] PRIMARY KEY CLUSTERED ([PropertyName] ASC) WITH (FILLFACTOR = 80)
);


GO

	CREATE TRIGGER [LKProperty_InsertUpdateDelete] on [dbo].[LKProperty] AFTER INSERT,UPDATE,DELETE
	AS
	BEGIN
		SET NOCOUNT ON;

		DECLARE @action nvarchar(50)

		SET @action = 'INSERT'; -- Set Action to Insert by default.
	
		IF EXISTS(SELECT * FROM DELETED)
			BEGIN
				SET @action = 
					CASE
						WHEN EXISTS(SELECT * FROM INSERTED) THEN 'UPDATE' -- Set Action to Updated.
						ELSE 'DELETE' -- Set Action to Deleted.       
					END
			END
		ELSE 
			IF NOT EXISTS(SELECT * FROM INSERTED) RETURN; -- Nothing updated or inserted.

		IF @action='INSERT' OR @action='UPDATE'
			BEGIN
							INSERT into [Audit].[LKProperty]
								([PropertyName], [TenantTypeID], [Ascend], [DefaultValue], [PropertyComment], [IsActive], [Operation], [UpdatedBy])
								select i.PropertyName, i.TenantTypeID, i.Ascend, i.DefaultValue, i.PropertyComment, i.IsActive, @action, SUSER_SNAME()
								FROM [dbo].[LKProperty] t
								inner join inserted i on i.PropertyName=t.PropertyName
			END
		ELSE
			BEGIN
							INSERT into [Audit].[LKProperty]
								([PropertyName], [TenantTypeID], [Ascend], [DefaultValue], [PropertyComment], [IsActive], [Operation], [UpdatedBy])
								select i.PropertyName, i.TenantTypeID, i.Ascend, i.DefaultValue, i.PropertyComment, i.IsActive, @action, SUSER_SNAME()
								FROM DELETED i
			END
	END

	