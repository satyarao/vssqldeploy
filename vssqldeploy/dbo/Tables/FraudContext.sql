﻿CREATE TABLE [dbo].[FraudContext] (
    [Id]                  INT              IDENTITY (1, 1) NOT NULL,
    [UserId]              UNIQUEIDENTIFIER NOT NULL,
    [DeviceId]            NVARCHAR (256)   NOT NULL,
    [AppTenantId]         UNIQUEIDENTIFIER NOT NULL,
    [UDID]                NVARCHAR (256)   NULL,
    [Action]              NVARCHAR (50)    NOT NULL,
    [DateTimeUtc]         DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL,
    [TransactionId]       UNIQUEIDENTIFIER NULL,
    [NuDataRawRequest]    NVARCHAR (MAX)   NULL,
    [NudataRawResponse]   NVARCHAR (MAX)   NULL,
    [UserPaymentSourceId] INT              NULL,
    [AppBundleId]         NVARCHAR (256)   NOT NULL,
    [AppVersionNumber]    NVARCHAR (50)    NOT NULL,
    [Latitude]            DECIMAL (19, 16) NOT NULL,
    [Longitude]           DECIMAL (19, 16) NOT NULL,
    [ClientOs]            NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_FraudContext] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_FraudContext_Basket] FOREIGN KEY ([TransactionId]) REFERENCES [dbo].[Basket] ([BasketID]) ON DELETE CASCADE,
    CONSTRAINT [FK_FraudContext_LKUserPaymentSource] FOREIGN KEY ([UserPaymentSourceId]) REFERENCES [Payment].[LKUserPaymentSource] ([UserPaymentSourceID]) ON DELETE CASCADE,
    CONSTRAINT [FK_FraudContext_Tenant] FOREIGN KEY ([AppTenantId]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE,
    CONSTRAINT [FK_FraudContext_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_FraudContext_TransactionId]
    ON [dbo].[FraudContext]([TransactionId] ASC) WHERE ([TransactionId] IS NOT NULL);

