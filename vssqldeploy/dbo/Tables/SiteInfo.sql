﻿CREATE TABLE [dbo].[SiteInfo] (
    [ConfigID]          INT              IDENTITY (1, 1) NOT NULL,
    [StoreID]           UNIQUEIDENTIFIER NOT NULL,
    [MacAddress]        VARCHAR (150)    NOT NULL,
    [IPAddress]         VARCHAR (50)     NULL,
    [HostName]          NVARCHAR (50)    NOT NULL,
    [HeartBeatDateTime] DATETIME2 (7)    NULL,
    [HeartBeatStatus]   NVARCHAR (50)    NULL,
    [InstallDateTime]   DATETIME2 (7)    CONSTRAINT [DF_SiteInfo_InstallDateTime] DEFAULT (getutcdate()) NOT NULL,
    [RestartDateTime]   DATETIME2 (7)    CONSTRAINT [DF_SiteInfo_RestartDateTime] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_SiteInfo] PRIMARY KEY CLUSTERED ([ConfigID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_SiteInfo_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);


GO
CREATE TRIGGER [dbo].[SiteInfo_Update] ON [dbo].[SiteInfo]
	FOR UPDATE
AS
SET NOCOUNT ON

INSERT	INTO dbo.SiteInfoLog
SELECT	*
FROM	deleted
