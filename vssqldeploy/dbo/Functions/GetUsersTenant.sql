﻿
CREATE   FUNCTION [dbo].[GetUsersTenant]( @UserId uniqueidentifier )
RETURNS uniqueidentifier
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT	TOP(1) [TenantID] 
		FROM	[dbo].[UserInfo] WITH(NOLOCK) 
		WHERE	[UserID] = @UserId
	)
	
END
