﻿-- =============================================
-- Author:      Tom
-- Description: convert primary key to alphanumeric value
-- =============================================
CREATE FUNCTION [dbo].[SetReportTransactionId]
	(
		 @PrimaryKey BigInt,
		 @Length int
	)
RETURNS varchar(255)
AS
BEGIN
	DECLARE @alphabet as char(34)
	SET @alphabet = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ'
	DECLARE @Result as varchar(255)
	WHILE @PrimaryKey > 0
	BEGIN
		SET @Result = Concat(SUBSTRING('0123456789ABCDEFGHJKLMNPQRSTUVWXYZ', @PrimaryKey % 34 + 1 , 1), @Result)
		SET @PrimaryKey = @PrimaryKey / 34 
	END
	RETURN RIGHT(REPLICATE('0', @Length) + @Result, @Length)
END
