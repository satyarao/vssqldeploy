﻿
CREATE FUNCTION [dbo].[GetTenantPropertyID] 
(
	@TenantID UNIQUEIDENTIFIER,
	@PropertyName NVARCHAR(50)
)
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE	@ParentTenantID UNIQUEIDENTIFIER
	DECLARE	@PropertyValue NVARCHAR(MAX)

	SELECT	@PropertyValue = PropertyValue
	FROM	dbo.TenantProperty
	WHERE	TenantID = @TenantID
			AND PropertyName = @PropertyName

	IF @PropertyValue IS NULL
		AND ( SELECT	Ascend
			  FROM		dbo.LKProperty
			  WHERE		PropertyName = @PropertyName
			) = 1
		BEGIN
			DECLARE	@StoreID UNIQUEIDENTIFIER
			SELECT	@StoreID = TenantID
			FROM	dbo.Store
			WHERE	StoreID = @TenantID
			IF @StoreID IS NOT NULL
				SET @TenantID = @StoreID
			
			SELECT TOP 1
					@ParentTenantID = tp.TenantID
			FROM	dbo.Tenant start
					JOIN dbo.Tenant parent ON start.OrgID.IsDescendantOf(parent.OrgID) = 1
					LEFT JOIN dbo.TenantProperty tp ON tp.TenantID = parent.TenantID
													   AND PropertyName = @PropertyName
			WHERE	START.TenantID = @TenantID
					AND tp.PropertyName IS NOT NULL
			ORDER BY parent.OrgLevel DESC
		END
	ELSE
		BEGIN
			SET @ParentTenantID = @TenantID
		END
	RETURN @ParentTenantID
END
