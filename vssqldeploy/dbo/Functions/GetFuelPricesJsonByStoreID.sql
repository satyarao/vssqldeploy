﻿
-- =============================================
-- Author:		Roman Lavreniuk
-- Create date: 02/27/2019
-- Description:	https://p97networks.atlassian.net/browse/XOM-43
-- Formerly: 1.0.1171_1.0.1172 on mppadev
-- =============================================

CREATE   FUNCTION [dbo].[GetFuelPricesJsonByStoreID]
(
	@StoreID UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Result NVARCHAR(MAX)
	SELECT @Result = (SELECT
				js.PCATSCode,
				COALESCE([dbo].[GetTenantPropertyValue](st.StoreId, 'ProductName.' + js.PCATSCode, null), js.DisplayName, mfp.[DefaultDisplayName] , c.[Description]) AS Name,
				js.CashPrice AS "Price.CashPrice", 
				js.CreditPrice AS "Price.CreditPrice", 
				js.UOM,
				js.OctaneRating,
				c.Grade AS GradeType,
				COALESCE(js.Description, mfp.[DefaultDescription]) AS Description,
                js.StandardProductCode
			FROM [dbo].[FuelPrice] js 
			LEFT JOIN PaymentSystemProductCodes c ON js.PCATSCode = c.PaymentCode
			LEFT JOIN [dbo].[Store] st ON st.StoreId = js.StoreId
			LEFT JOIN [Market].[FuelProduct] mfp ON st.TenantId = mfp.TenantId AND js.PCATSCode = mfp.PCATSCode
			WHERE js.StoreId = @StoreID AND js.IsActive = 1
			FOR JSON PATH)
	RETURN @Result
END
