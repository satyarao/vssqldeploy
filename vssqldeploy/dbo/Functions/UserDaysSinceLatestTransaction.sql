﻿
CREATE   FUNCTION [dbo].[UserDaysSinceLatestTransaction]
( 
	@UserId uniqueidentifier, 
	@TransactionStartDate datetime2(7) = NULL, 
	@TransactionEndDate datetime2(7) = NULL
)
RETURNS int
WITH SCHEMABINDING
AS
BEGIN
	-- Returns the days since the users latest transaction in a time window.  
	-- Returns 0 if the user did not transact in the time window
	RETURN
	(
		SELECT	DATEDIFF
				(
					DAY,
					ISNULL
					(
						(
							SELECT TOP (1) T.[CreatedOn]
							FROM		[Txn].[Txn] T WITH(NOLOCK)
							WHERE		T.[UserID] = @UserId AND
										(
											@TransactionStartDate IS NULL OR
											T.[CreatedOn] > @TransactionStartDate
										)
										AND
										(
											@TransactionEndDate IS NULL OR
											T.[CreatedOn] < @TransactionEndDate
										)
							ORDER BY	T.[CreatedOn] DESC
						),
						GETUTCDATE()
					),
					GETUTCDATE()
				)
		
	)
END
