﻿
-- =============================================
-- Author:		Roman Lavreniuk
-- Create date: 02/27/2019
-- Description:	https://p97networks.atlassian.net/browse/XOM-43
-- Formerly 1.0.1166_1.0.1167 on mppadev
-- =============================================

CREATE   FUNCTION [dbo].[GetPumpJsonByStoreID]
(
    @StoreID UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
    DECLARE @Result NVARCHAR(MAX) = 
    (
        SELECT [PumpNumber],
               [ServiceLevel],
               [PumpStatus]
        FROM [dbo].[Pump]
        WHERE [StoreId] = @StoreID
        FOR JSON PATH
    );
    RETURN @Result
END

