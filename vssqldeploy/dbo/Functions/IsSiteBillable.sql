﻿-- =============================================
-- Create date: 2017/03/21
-- Update date: 2017/05/29 - use IsSiteBillable instead of IsPaymentBillable (for Basket table) - function was renamed from IsStorePaymentBillable to IsSiteBillable
-- Description:	Check if Store allows payments to be billable. Defaults to 0 if no tenant-specific store is found
-- =============================================
CREATE FUNCTION [dbo].[IsSiteBillable] 
(
	@TenantId UNIQUEIDENTIFIER,
	@StoreId UNIQUEIDENTIFIER
)
RETURNS BIT
AS
BEGIN
	DECLARE	@IsSiteBillable BIT;

	SELECT @IsSiteBillable = Store.IsSiteBillable
	FROM [dbo].[Store] Store 
	WHERE Store.TenantID = @TenantId AND Store.StoreID = @StoreId

	IF @IsSiteBillable IS NULL
	BEGIN
		DECLARE @JsonPayload NVARCHAR(max);

		SELECT @JsonPayload = StoreTenants
		FROM [dbo].[Store] Store where StoreId = @StoreId

		SELECT @IsSiteBillable = IsSiteBillable
		FROM OPENJSON ( @JsonPayload )  
		WITH (   
				TenantId			UNIQUEIDENTIFIER '$.TenantID' ,  
				IsSiteBillable   BIT				 '$.IsSiteBillable'
		)
		WHERE TenantId = @TenantId
	END

	RETURN ISNULL(@IsSiteBillable, 0)
END
