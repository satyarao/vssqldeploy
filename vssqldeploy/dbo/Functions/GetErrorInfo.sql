﻿
CREATE FUNCTION GetErrorInfo ( )
RETURNS @error TABLE
    (
      ErrorNumber INT
    , ErrorSeverity INT
    , ErrorState INT
    , ErrorProcedure NVARCHAR(128)
    , ErrorLine INT
    , ErrorMessage NVARCHAR(4000)
    , ReturnMessage NVARCHAR(MAX)
    )
AS
    BEGIN
        INSERT  INTO @error
                SELECT  ERROR_NUMBER()
                      , ERROR_SEVERITY()
                      , ERROR_STATE()
                      , ERROR_PROCEDURE()
                      , ERROR_LINE()
                      , ERROR_MESSAGE()
                      , ISNULL(ERROR_PROCEDURE(),'') + ', ' + ' Error number: ' + CAST(ERROR_NUMBER() AS VARCHAR(10)) + ', '
                        + 'Actual line number: ' + CAST(ERROR_LINE() AS VARCHAR(10)) + ', ' + ERROR_MESSAGE() + ', '
                        + 'Error Severity: ' + CAST(ERROR_SEVERITY() AS VARCHAR(10)) + ', ' + 'Error State: '
                        + CAST(ERROR_STATE() AS VARCHAR(10)) 

        RETURN
    END
