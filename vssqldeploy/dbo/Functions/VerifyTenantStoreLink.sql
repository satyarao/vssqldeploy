﻿-- =============================================
-- Create date: 03/22/2017
-- Description:	verify tenant id for store id
-- =============================================
CREATE FUNCTION [dbo].[VerifyTenantStoreLink]
(
	@TenantId UNIQUEIDENTIFIER,
	@StoreId UNIQUEIDENTIFIER
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsValid BIT = 0;

	SET @IsValid = CASE WHEN EXISTS(SELECT * FROM [dbo].[Store] Store 
				WHERE Store.TenantID = @TenantId AND Store.StoreID = @StoreId)
		THEN 1 ELSE 0 END

	IF @IsValid = 0
	BEGIN
		DECLARE @JsonPayload NVARCHAR(max);

		SELECT @JsonPayload = StoreTenants
		FROM [dbo].[Store] Store where StoreId = @StoreId

		SET @IsValid = CASE WHEN EXISTS(SELECT * FROM OPENJSON ( @JsonPayload )  
					WITH (   
							TenantId			UNIQUEIDENTIFIER '$.TenantID'
					)
					WHERE TenantId = @TenantId)
			THEN 1 ELSE 0 END
	END

	RETURN @IsValid
END
