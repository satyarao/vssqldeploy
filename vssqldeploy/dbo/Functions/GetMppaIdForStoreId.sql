﻿
CREATE FUNCTION [dbo].[GetMppaIdForStoreId]
	(
	  @StoreID UNIQUEIDENTIFIER
	)
RETURNS INT
AS
BEGIN
	DECLARE	@MppaId INT
	SET @MppaId = 0
	SELECT	@MppaId = [MPPAID]
	FROM	[dbo].[Store]
	WHERE	[StoreID] = @StoreID

	RETURN @MppaId
END

