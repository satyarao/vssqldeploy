﻿CREATE FUNCTION [dbo].[utvf_SPlit] (@String VARCHAR(MAX), @delimiter CHAR)

RETURNS @SplitValues TABLE
(
    Asset_ID VARCHAR(MAX) NOT NULL
)

AS
BEGIN
	DECLARE @FoundIndex INT
	DECLARE @ReturnValue VARCHAR(MAX)

	SET @FoundIndex = CHARINDEX(@delimiter, @String)

	WHILE (@FoundIndex <> 0)
	BEGIN
			SET @ReturnValue = SUBSTRING(@String, 0, @FoundIndex)
			INSERT @SplitValues (Asset_ID) VALUES (@ReturnValue)
			SET @String = SUBSTRING(@String, @FoundIndex + 1, len(@String) - @FoundIndex)
			SET @FoundIndex = CHARINDEX(@delimiter, @String)
	END

	INSERT @SplitValues (Asset_ID) VALUES (@String)

    RETURN
END
