﻿-- =============================================
-- Author:		Sergey Buday
-- Create date: 01/18/2019
-- Description:	PLAT-4745 App Tenant User Verification Setting (Extend @PropertyName according to new size of tied column)
-- =============================================
CREATE FUNCTION [dbo].[GetTenantPropertyInfo] 
(	
	@TargetID UNIQUEIDENTIFIER,
	@PropertyName NVARCHAR(128),
	@HierarchyDepth SMALLINT = NULL
)
RETURNS @TenantPropertyInfo TABLE 
(
	TenantID UNIQUEIDENTIFIER,
	PropertyName NVARCHAR(128),
	PropertyValue NVARCHAR(max),
	DefaultTenantID UNIQUEIDENTIFIER,
	DefaultTenantName NVARCHAR(255),
	DefaultPropertyValue NVARCHAR(max)
)
AS
BEGIN
	DECLARE @PropertyValue NVARCHAR(max);
	DECLARE @TenantID UNIQUEIDENTIFIER = @TargetID;
	DECLARE @DefaultPropertyValue NVARCHAR(max);

	SELECT @PropertyValue = dbo.GetTenantPropertyExclusiveValue(@TargetID, @PropertyName)

	DECLARE	@ParentTenantID UNIQUEIDENTIFIER;
	DECLARE @ParentTenantName NVARCHAR(255);
	IF ( SELECT	Ascend
			  FROM		dbo.LKProperty
			  WHERE		PropertyName = @PropertyName
			) = 1
		BEGIN
			DECLARE	@StoreID UNIQUEIDENTIFIER;
			SELECT	@StoreID = TenantID
			FROM	dbo.Store
			WHERE	StoreID = @TargetID
			IF @StoreID IS NOT NULL
				BEGIN
					SET @TenantID = @StoreID
					IF @HierarchyDepth IS NOT NULL
						SET @HierarchyDepth = @HierarchyDepth - 1
				END

			
			SELECT TOP 1
					@ParentTenantID = tp.TenantID
			FROM	dbo.Tenant start
					JOIN dbo.Tenant parent ON start.OrgID.IsDescendantOf(parent.OrgID) = 1 AND parent.TenantID <> @TargetID
					LEFT JOIN dbo.TenantProperty tp ON tp.TenantID = parent.TenantID
													   AND PropertyName = @PropertyName
			WHERE	START.TenantID = @TenantID
					AND tp.PropertyName IS NOT NULL
					AND start.OrgLevel - parent.OrgLevel <= COALESCE(@HierarchyDepth, start.OrgLevel - parent.OrgLevel)
			ORDER BY parent.OrgLevel DESC

			SELECT @ParentTenantName = Name
			FROM dbo.Tenant
			WHERE TenantID = @ParentTenantID
		END

		SELECT @DefaultPropertyValue = dbo.GetTenantPropertyExclusiveValue(@ParentTenantID, @PropertyName)

		IF(@DefaultPropertyValue IS NULL)
		BEGIN
			IF(@TargetID = '00000000-0000-0000-0000-000000000000') --P97 Tenant
			BEGIN
				SELECT TOP 1 @DefaultPropertyValue = DefaultValue FROM dbo.LKProperty WHERE PropertyName = @PropertyName
			END
			ELSE
			BEGIN
				SELECT TOP 1 @DefaultPropertyValue = DefaultValue FROM dbo.LKProperty WHERE PropertyName = @PropertyName AND Ascend = 1
			END
		END 


	INSERT @TenantPropertyInfo 
	SELECT
		@TargetID as TenantID,
		@PropertyName as PropertyName,
		@PropertyValue as PropertyValue,
		@ParentTenantID as DefaultTenantID,
		@ParentTenantName as DefaultPropertyName,
		@DefaultPropertyValue as DefaultPropertyValue
	RETURN;
END

