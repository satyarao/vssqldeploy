﻿
-- Author:		Hapeyeu Dzmitry
-- Create date: 10/17/2019
-- Description:	Check if @TenantId is tenant id
CREATE    FUNCTION dbo.IsTenant(@TenantId uniqueidentifier)
RETURNS BIT
BEGIN
	IF EXISTS(select TenantId from Tenant where TenantId = @TenantId)
	RETURN 1

	RETURN 0
END
