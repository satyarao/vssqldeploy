﻿
CREATE FUNCTION [dbo].[GetOperatingHoursJsonByStoreID]
(
	@StoreID UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Payload NVARCHAR(max) = 
		(SELECT 
				  h.SundayOpen AS SundayOpen
				, h.SundayClose AS SundayClose
				, h.MondayOpen AS MondayOpen
				, h.MondayClose AS MondayClose
				, h.TuesdayOpen AS TuesdayOpen
				, h.TuesdayClose AS TuesdayClose
				, h.WednesdayOpen AS WednesdayOpen
				, h.WednesdayClose AS WednesdayClose
				, h.ThursdayOpen AS ThursdayOpen
				, h.ThursdayClose AS ThursdayClose
				, h.FridayOpen AS FridayOpen
				, h.FridayClose AS FridayClose
				, h.SaturdayOpen AS SaturdayOpen
				, h.SaturdayClose AS SaturdayClose
		FROM [dbo].[HoursOfOperation] h 
		WHERE h.StoreID = @StoreID
		FOR JSON PATH);
	RETURN JSON_QUERY(@Payload, '$[0]')
END
