﻿-- =============================================
-- Create date: 2016-06-06
-- Description:	Get Tenant ID based on User ID or Installation ID
-- =============================================
CREATE FUNCTION [dbo].[GetTenantIDByUserInfo]
(
	@UserID UNIQUEIDENTIFIER = NULL,
	@InstallationID NVARCHAR(128) = NULL
)
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @TenantID UNIQUEIDENTIFIER = NULL

	IF @UserID IS NULL AND @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationID)
	END

	IF @UserID IS NOT NULL
	BEGIN
		SELECT @TenantID = TenantID 
		FROM UserInfo
		WHERE UserID = @UserID
	END

	RETURN @TenantID

END
