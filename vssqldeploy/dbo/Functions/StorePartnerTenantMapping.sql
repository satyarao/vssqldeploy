﻿

CREATE   FUNCTION [dbo].[StorePartnerTenantMapping] ( @TenantID uniqueidentifier = NULL, @StoreID uniqueidentifier = NULL )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		[StoreID],
					[TenantID],
					[MPPAID] AS [MppaID],
					[City],
					[StateCode],
					[ZipCode]
		FROM		[dbo].[Store] WITH(NOLOCK)
		WHERE		[StoreID] = ISNULL(@StoreID, [StoreID]) AND
					[TenantID] = ISNULL(@TenantID, [TenantID])

		UNION

		SELECT		[StoreID],
					items.[TenantID],
					stre.[MPPAID] AS [MppaID],
					stre.[City],
					stre.[StateCode],
					stre.[ZipCode]
		FROM		[dbo].[Store] stre WITH(NOLOCK)
		CROSS APPLY	(
						SELECT	st.[TenantID] 
						FROM	OPENJSON (stre.[StoreTenants])  
						WITH	([TenantID] sys.uniqueidentifier N'TenantId') as st
					) items
		WHERE		[StoreID] = ISNULL(@StoreID, [StoreID]) AND
					items.[TenantID] = ISNULL(@TenantID, items.[TenantID])