﻿CREATE FUNCTION [dbo].[GetTenantFromURL] ( @BaseURI NVARCHAR(250) )
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE	@TenantID UNIQUEIDENTIFIER
	SELECT	@TenantID = TenantID
	FROM	dbo.TenantProperty
	WHERE	(PropertyName = 'Web.BaseUri' or PropertyName = 'Web.Portal.BaseUri')
			AND PropertyValue = @BaseURI
	RETURN ISNULL(@TenantID, '00000000-0000-0000-0000-000000000000')
END
