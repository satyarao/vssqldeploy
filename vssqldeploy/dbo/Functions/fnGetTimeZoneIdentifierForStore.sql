﻿CREATE FUNCTION [dbo].[fnGetTimeZoneIdentifierForStore]
	(@StoreID UNIQUEIDENTIFIER)
RETURNS VARCHAR(100)
AS
BEGIN
	RETURN ISNULL((SELECT tz.Identifier FROM Common.LKTimezone tz
	JOIN Store s on s.TimeZoneID = tz.TimeZoneID
	WHERE s.StoreID = @StoreID), 'UTC')
END
