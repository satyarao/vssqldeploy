﻿-- =============================================
-- Author:		Sergey Buday
-- Create date: 01/22/2019
-- Description:	PLAT-4745 App Tenant User Verification Setting (Extend @PropertyName according to new size of tied column)
-- =============================================
CREATE FUNCTION [dbo].[GetTenantPropertyExclusiveValue]
	(
	  @TenantID UNIQUEIDENTIFIER
	, @PropertyName NVARCHAR(128)
	)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE	@PropertyValue NVARCHAR(MAX)

	SELECT	@PropertyValue = PropertyValue
	FROM	dbo.TenantProperty
	WHERE	TenantID = @TenantID
			AND PropertyName = @PropertyName
	RETURN @PropertyValue
END

