﻿
-- =============================================
-- Author:		Roman Lavreniuk
-- Create date: 02/27/2019
-- Description:	https://p97networks.atlassian.net/browse/XOM-43
-- Formerly: 1.0.1168_1.0.1169 on mppadev
-- =============================================

CREATE   FUNCTION [dbo].[GetLoyaltyServicesJsonByStoreID]
(
    @StoreID UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
    DECLARE @Result NVARCHAR(MAX) =
    (
        SELECT [ProgramId]
              ,[ProgramName]
              ,[ServiceStatus]
        FROM [dbo].[LoyaltyService]
        WHERE [StoreId] = @StoreID
        FOR JSON PATH
    );
    RETURN @Result
END

