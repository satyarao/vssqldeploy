﻿
CREATE FUNCTION [dbo].[GetDeviceRegistrationID]
	(
	  @InstallationID NVARCHAR(128)
	)
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE	@DRID UNIQUEIDENTIFIER
	SELECT	@DRID = DeviceRegistrationID
	FROM	dbo.DeviceRegistration
	WHERE	InstallationID = @InstallationID
	RETURN @DRID
END

