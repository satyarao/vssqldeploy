﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsTimeoutExpired] 
(
	-- Add the parameters for the function here
	@Timeout int
	,@DatePresented Datetime2(7)
)
RETURNS BIT
AS
BEGIN

	IF(DATEDIFF(minute, @DatePresented,GetUTCDate()) > @Timeout AND @Timeout <> 0)
		BEGIN
			RETURN 1
		END
	
	RETURN 0

END
