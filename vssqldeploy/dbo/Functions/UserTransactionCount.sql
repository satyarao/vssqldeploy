﻿
CREATE   FUNCTION [dbo].[UserTransactionCount]( @UserId uniqueidentifier, @CompletedTransactionStartDate datetime2(7) = NULL )
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	RETURN
	(
		SELECT	COUNT(T.[TxnID]) 
		FROM	[Txn].[Txn] T WITH(NOLOCK)
		WHERE	T.[UserID] = @UserId AND
				(
					@CompletedTransactionStartDate IS NULL OR
					T.[CreatedOn] > @CompletedTransactionStartDate
				)
	)
END
