﻿
CREATE   FUNCTION [dbo].[GetFuelPricesForPump]
(
	@StoreId UNIQUEIDENTIFIER,
	@PumpNumber INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Result NVARCHAR(MAX)

	SELECT @Result = (
		SELECT
			fp.[StoreID],
			fp.[PCATSCode],
			fp.[CashPrice],
			fp.[CreditPrice],
			fp.[UOM],
			fp.[OctaneRating],
			fp.[IsActive],
			fp.[CreatedBy],
			fp.[CreatedOn],
			fp.[UpdatedBy],
			fp.[UpdatedOn],
			fp.[DisplayName],
			fp.[Description],
			fp.[StandardProductCode]
		FROM [dbo].[FuelPrice] fp
		INNER JOIN [dbo].[PumpFuelPrice] pfp
		ON pfp.[FuelPriceStoreId] = fp.[StoreID] AND pfp.[PCATSCode] = fp.[PCATSCode]
		WHERE pfp.[PumpStoreId] = @StoreId AND pfp.[PumpNumber] = @PumpNumber
		FOR JSON PATH);

	RETURN @Result
END
