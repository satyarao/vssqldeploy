﻿-- =============================================
-- Author:		Mason Sciotti
-- Create date: 6/27/2016
-- Update date: 11/22/2016 - Select TenantID from dbo.Store instead dbo.FlatStore
-- Description:	get tenant id for store id
-- =============================================
CREATE FUNCTION [dbo].[GetTenantIdForStoreId]
(
	-- Add the parameters for the function here
	@StoreId uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	-- Declare the return variable here
	DECLARE @TenantId uniqueidentifier = null

	-- Add the T-SQL statements to compute the return value here	
	Select @TenantId = TenantID
	From dbo.Store
	where StoreID = @StoreId

	-- Return the result of the function
	RETURN @TenantId

END

