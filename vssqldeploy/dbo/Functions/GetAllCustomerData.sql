﻿
-- =============================================
-- Author:  Sergei Buday
-- Create date: 08/06/2019 
-- Description:	PLAT-4004 [Identity] Clean up membership reboot stuff that we don't need
-- =============================================
CREATE FUNCTION [dbo].[GetAllCustomerData]
	(
		@UserID UNIQUEIDENTIFIER,
		@TenantID UNIQUEIDENTIFIER
	)
RETURNS NVARCHAR(max)
AS
BEGIN
	RETURN (
	SELECT

		(
			SELECT [EmailAddress],[MobileNumber],[FirstName],[LastName],[ImageUri],[IsActive],[CreatedBy],[CreatedOn],[UpdatedBy],[UpdatedOn],[Name],[IsDeleted],[LastSignin],[AgreementVersion],[CountryIsoCode],[StateCode],[CountryUpdatedOn] 
			FROM [dbo].[UserInfo] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS UserInfo

		,(
			SELECT up.[PinCode],up.[OneTimeCode],up.[ExpirationDate],up.[IsActive],up.[CreatedBy],up.[CreatedOn],up.[UpdatedBy],up.[UpdatedOn],up.[MissedPinCodeCount],
				ups.[Name] as UserPinStatusName
			FROM [dbo].[UserPin] up 
			JOIN [dbo].[LKUserPinStatus] ups ON up.[UserPinStatusID] = ups.[UserPinStatusID] 
			WHERE up.[UserID] = @UserID FOR JSON PATH
		) AS UserPin

		,(
			SELECT [PaperReceipt],[EmailReceipt],[NotificationReceipt],[FuelGrade],[IsActive],[CreatedOn],[CreatedBy],[UpdatedBy],[UpdatedOn],[OptInMarketingEmail],[Sms],[OptInMarketingSms],[Push],[OptInMarketingPush]
			FROM [dbo].[UserPreference] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS UserPreference

		, (
			SELECT [IPAddress],[URL],[Category],[RequestType],[StatusCode],[Received],[Sent] 
			FROM [dbo].[TenantAudit] WHERE [UserID] = @UserID FOR JSON PATH
		) AS TenantAudit

		, (
			SELECT s.[Name] as StoreName, b.[POSOperatorID] as TransactionOrigin, b.[POSTerminalID] as Terminal, b.[POSDateTime] as DateTime, b.[POSDateTimeLocal] as DateTimeLocal,
				b.[SubTotal],b.[TaxAmount],b.[Total],b.[CreatedOn],b.[UpdatedOn],b.[ItemRewardTotal],b.[FuelRewardTotal],b.[PreRewardSubTotal],b.[BasketLoyaltyPrograms],b.[AppDisplayName],b.[AppChannel]
				, (
					SELECT bli.[UnitOfMeasure],bli.[ItemDescription],bli.[SellingUnit],bli.[RegularPrice],bli.[SalesPrice],bli.[Quantity],bli.[ExtendedPrice],bli.[TaxAmount],bli.[TaxRate],bli.[ExtendedAmount],bli.[LoyaltyRewardAmount],bli.[CreatedOn],bli.[UpdatedOn],bli.[CarWashCode],bli.[CarWashCodeExpiration],bli.[LoyaltyRewardUnitAmount],bli.[LoyaltyRewardQuantity],bli.[LoyaltyRewardSellingUnit] 
					FROM [dbo].[BasketLineItem] bli 
					WHERE bli.[BasketID] = b.[BasketID] FOR JSON PATH
				) As LineItems
			FROM [dbo].[Basket] b 
			LEFT JOIN [dbo].[Store] s ON s.[StoreID] = b.[StoreID]
			WHERE b.[UserID] = @UserID AND b.[AppTenantID] = @TenantID FOR JSON PATH
		) AS Basket

		,(
			SELECT [FilterName],[FilterText],[CreatedBy],[CreatedOn],[UpdatedBy],[UpdatedOn],[IsActive],[IsPrivate] 
			FROM [dbo].[Filter]
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS UserFilter

		,(
			SELECT [SubscriptionType],[IsDaily],[IsWeekly],[IsMonthly],[IsAttachmentRequired],[CreatedOn],[UpdatedOn]
			FROM [dbo].[EmailSubscription] 
			WHERE [UserID] = @UserID AND ([TenantID] IS NULL OR [TenantID] = @TenantID) FOR JSON PATH
		) AS EmailSubscription

		, (
			SELECT [AccessCode],[CreatedOn],[ExpiresOn],[IsActive] 
			FROM [dbo].[JanrainAccessCode] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS JanrainAccessCode
		, (
			SELECT [AccessToken],[RefreshToken],[CreatedOn],[ExpiresOn] 
			FROM [dbo].[JanrainUserAccess] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS JanrainUserAccess

		, (
			SELECT [AccountNumber],[MemberId],[MemberAccessToken],[TokenExpirationDate],[MemberRefreshToken] 
			FROM [dbo].[ExcentusMemberTokens] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS ExcentusMemberTokens

		, (
			SELECT ulc.[LoyaltyPrimaryAccountNumber],ulc.[IsActive],ulc.[CreatedOn],ulc.[UpdatedOn], lp.DisplayName, ISNULL(p97loyalty.[IsProvisioned], 0) AS P97LoyaltyProvisioned
			FROM [Loyalty].[UserLoyaltyCard] ulc 
			LEFT JOIN [Loyalty].[LoyaltyProgram] lp ON lp.[LoyaltyProgramID] = ulc.[LoyaltyProgramID]
			LEFT JOIN [Loyalty].[P97LoyaltyCardProvision] p97loyalty ON p97loyalty.[LoyaltyPrimaryAccountNumber] = ulc.[LoyaltyPrimaryAccountNumber] AND p97loyalty.[UserID]= ulc.[UserID]
			WHERE ulc.[UserID] = @UserID FOR JSON PATH
		) AS UserLoyaltyCard

		, (
			SELECT [ContractVersion],[AcceptanceState],[Timestamp] 
			FROM [Payment].[BimCustomerContractAgreement] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS BimCustomerContractAgreement

		, (
			SELECT [CustomerApiKey],[CreatedOn],[UpdatedOn],[ConsumerId] 
			FROM [Payment].[ConfigBimCustomerLevel] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS ConfigBimCustomerLevel

		, (
			SELECT [ContractVersion],[AcceptanceState],[Timestamp] 
			FROM [Payment].[CustomerEnrollmentContractAgreement] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS CustomerEnrollmentContractAgreement

		,(
			SELECT lkups.[CardName],lkups.[IsActive],lkups.[CreatedOn],lkups.[UpdatedOn],lkups.[NickName],
			pp.[PaymentProcessorName],pp.[ImageUrl],pp.[CardIssuer],
			upiv.[FirstSix],upiv.[LastFour],upiv.[ExpDate]
			FROM [Payment].[LKUserPaymentSource] lkups
			JOIN [Payment].[LKPaymentProcessor] pp ON lkups.[PaymentProcessorID] = pp.[PaymentProcessorID]
			JOIN [Compatible].[UserPaymentInfoView] upiv ON upiv.[UserPaymentSourceID] = lkups.[UserPaymentSourceID]
			WHERE lkups.[UserID] = @UserID FOR JSON PATH
		) AS UserPaymentSource

		,(
			SELECT [NotificationToken],[Platform],[IsActive],[CreatedOn],[UpdatedOn],[BundleId],[Version] 
			FROM [PNS].[NotificationToken] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS FirebaseNotificationToken

		,(
			SELECT uie.[EntryValue], uie.[CreatedOn], dm.Name as DataTypeName 
			FROM [UserInput].[Entry] uie 
			JOIN [UserInput].[DataModel] dm ON dm.[DataModelId] = uie.[DataModelId]
			WHERE uie.[UserID] = @UserID FOR JSON PATH
		) AS UserInputEntry

		,(
			SELECT [Longitude],[Latitude],[GeoText],[Timestamp] 
			FROM [dbo].[UserTrackLocation] 
			WHERE [UserID] = @UserID FOR JSON PATH
		) AS UserTrackLocation

		,(
			SELECT LKTag.[UserID], TAG.[TagID], TAG.[GroupName], TAG.[TagValue], TAG.[IsActive], LKTag.[LastUpdated]
			FROM [ContentPush].[LKTag_User] LKTag
			JOIN [ContentPush].[Tag] TAG
				ON LKTag.[TagID] = TAG.[TagID]
			WHERE LKTag.UserID = @UserID FOR JSON PATH
		) AS UserTags

		,(
			SELECT LKTag.[ChannelID], TAG.[TagID], TAG.[GroupName], TAG.[TagValue], TAG.[IsActive],LKTag.[LastUpdated]
			FROM [ContentPush].[LKTag_Channel] LKTag
			INNER JOIN [ContentPush].[Tag] TAG
				ON LKTag.[TagID] = TAG.[TagID]
			INNER JOIN [ContentPush].[LKUser_Channel] LK
				ON LK.ChannelID = LKTag.ChannelID
			WHERE LK.UserID = @UserID FOR JSON PATH
		) AS ChannelTags

		,(
			SELECT ENG.[EngageEventID], ENG.[UserID], ENG.[ChannelID], ENG.[StoreID], ENG.[TransactionID], ENG.[EventCreatedUtc], ENG.[EventCreatedLocal], ENG.[CustomEventType], ENG.[DeviceType], ENG.[Lattitude], ENG.[Longitude], ENG.[CreatedOn]
			FROM [ContentPush].[EngageEvent] ENG
			LEFT JOIN [ContentPush].[LKUser_Channel] LK
				ON LK.ChannelID = ENG.ChannelID
			WHERE ENG.UserID = @UserID OR LK.UserID = @UserID FOR JSON PATH
		) AS EngageEvents

		,(
			SELECT ACT.[ChannelActivityID], ACT.[ChannelID], ACT.[UserID], ACT.[DeviceType], ACT.[RegistrationType], ACT.[SubscriptionType], ACT.[CreatedOn]
			FROM [ContentPush].[ChannelActivity] ACT
			INNER JOIN [ContentPush].[LKUser_Channel] LK
				ON LK.ChannelID = ACT.ChannelID
			WHERE LK.UserID = @UserID FOR JSON PATH
		) AS ChannelActivity

		,(
			SELECT CHN.[ChannelID], CHN.[DeviceType], CHN.[IsInstalled], CHN.[OptedIn], CHN.[AppPackageName], CHN.[AppVersion], CHN.[BackgroundPushEnabled], CHN.[Carrier], CHN.[DeviceModel], CHN.[DeviceOs], CHN.[IanaTimezone], CHN.[LocaleCountryCode], CHN.[LocaleLanguageCode], CHN.[LocaleTimezone], CHN.[LocaleVariant], CHN.[LocationEnabled], CHN.[LocationPermission], CHN.[UaSdkVersion], CHN.[LastUpdated]
			FROM [ContentPush].[Channel] CHN
			INNER JOIN [ContentPush].[LKUser_Channel] LK
				ON LK.ChannelID = CHN.ChannelID
			WHERE LK.UserID = @UserID FOR JSON PATH
		) AS Channels

		,(
			SELECT CNCT.[ConnectEventID], CNCT.[ConnectEventType], [EventBody], [AssociatedPush], [Device], CNCT.[Occurred], CNCT.[CreatedOn]
			FROM [ContentPush].[ConnectEvent] CNCT
			WHERE LOWER([Device]) LIKE ('%' + LOWER(convert(nvarchar(36), @UserID)) + '%') FOR JSON PATH
		) AS ConnectData

		-- , (SELECT * FROM [dbo].[FlatBasket] fb WHERE fb.[UserID] = @UserID FOR JSON PATH) AS FlatBasket
		-- ,(SELECT * FROM [dbo].[DefaultFilter] WHERE [UserID] = @UserID FOR JSON PATH) AS DefaultFilter
		-- ,(SELECT * FROM [MSP].[UserWalletMap] uwm WHERE uwm.[UserID] = @UserID FOR JSON PATH) AS MCXUserWallet
		-- ,(SELECT * FROM [Payment].[PaymentPreferences] pp WHERE pp.[UserID] = @UserID FOR JSON PATH) AS PaymentPreferences

	FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
	)
END
