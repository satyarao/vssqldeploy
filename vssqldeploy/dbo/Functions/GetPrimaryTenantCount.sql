﻿CREATE FUNCTION [dbo].[GetPrimaryTenantCount]
	(
	  @StoreID UNIQUEIDENTIFIER
	)
RETURNS INT
AS
BEGIN
	DECLARE	@Cnt INT
	SET @Cnt = 0
	SELECT	@cnt = COUNT(*)
	FROM	dbo.TenantStoreLink
	WHERE	StoreID = @StoreID
			AND TenantLinkTypeID = 1

	RETURN @Cnt
END
