﻿CREATE FUNCTION [dbo].[GetPrimaryTenant]
	(
	  @StoreID UNIQUEIDENTIFIER
	)
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE	@TenantID UNIQUEIDENTIFIER
	SELECT	@TenantID = TenantID
	FROM	dbo.TenantStoreLink
	WHERE	StoreID = @StoreID
			AND TenantLinkTypeID = 1

	RETURN @TenantID
END
