﻿
-- =============================================
-- Author:		Sergey Buday
-- Create date: 07/07/2019
-- Description:	Get Tenant Property depends on DevieID
-- =============================================
CREATE FUNCTION [dbo].[GetDevicePropertyValue]
	(
	  @TargetID UNIQUEIDENTIFIER
	, @PropertyName NVARCHAR(128)
	, @DeviceID NVARCHAR(128)
	)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE	@PropertyValue NVARCHAR(MAX)

	IF ( SELECT	IsActive FROM dbo.LKProperty WHERE PropertyName = @PropertyName) = 0
		BEGIN 
			RETURN NULL
		END 

	IF @PropertyValue IS NULL
		BEGIN 
			SELECT	@PropertyValue = PropertyValue FROM	dbo.DeviceProperty WHERE TargetID = @TargetID AND PropertyName = @PropertyName  AND DeviceID = @DeviceID
		END 

	IF @PropertyValue IS NULL
		BEGIN
			SELECT	@PropertyValue = dbo.GetTenantPropertyValue(@TargetID, @PropertyName, NULL)
		END

	RETURN @PropertyValue
END

