﻿
CREATE FUNCTION [dbo].[GetUserIDFromUserPaymentSourceID]
	(
	  @UserPaymentSourceID INT
	)
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE	@UserID UNIQUEIDENTIFIER
	SELECT	@UserID = UserID
	FROM	[Payment].[LKUserPaymentSource]
	WHERE	UserPaymentSourceID = @UserPaymentSourceID
	RETURN @UserID

END
