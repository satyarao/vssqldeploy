﻿CREATE FUNCTION [dbo].[IsCurrentDateTime]
(
	@StartDateTime DATETIME2(7),
	@EndDateTime DATETIME2(7)
)
RETURNS BIT
AS
BEGIN
	IF (@StartDateTime is NULL AND @EndDateTime is NULL)
		RETURN 1;

	DECLARE @CurrentDateTime DATETIME2(7)
	SET @CurrentDateTime = GETUTCDATE();

	IF (@StartDateTime is NULL)
		IF (DATEADD(day, 1, @EndDateTime) >= @CurrentDateTime)
			RETURN 1
		ELSE
			RETURN 0;
	IF (@EndDateTime is NULL)
		IF (@StartDateTime <= @CurrentDateTime)
			RETURN 1
		ELSE
			RETURN 0;
	IF (@StartDateTime <= @CurrentDateTime AND DATEADD(day, 1, @EndDateTime) >= @CurrentDateTime)
		RETURN 1;
	RETURN 0;
END
