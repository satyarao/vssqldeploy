﻿

-- =============================================
-- Author:		Darya Batalava
-- Create date: 08/28/2018
-- Description:	Retrieve an array formatted list of Store Custom Properties
-- =============================================
CREATE FUNCTION [dbo].[GetStoreCustomPropertiesArray] 
(
	@StoreID UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(max)
AS
BEGIN
	RETURN '[' + STUFF(( SELECT ',''' +( CONVERT(varchar(5), svc.[PropertyID]) + ' || '  + svc.[Value] )+ '''' AS [text()]
						FROM	[dbo].[CustomPropertyValue] svc INNER JOIN [CustomProperty] cp ON svc.PropertyID = cp.ID
						WHERE	svc.[TargetID] = @StoreID AND cp.IsActive = 1
					  FOR
						XML	PATH('')
					  ), 1, 1, '') + ']';
END

