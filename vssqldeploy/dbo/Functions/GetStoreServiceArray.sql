﻿-- =============================================
-- Author:		Matthew, Ingram
-- Create date: 05/14/2015
-- Update date: 03/13/2019 - Updated to take in special characters
-- Update date: 04/04/2019 - Davydovich Raman: Fix return value format 
-- Description:	Retrieve an array formatted list of Store Services
-- =============================================

CREATE FUNCTION [dbo].[GetStoreServiceArray] 
(
	@StoreID UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(max)
AS
BEGIN
	RETURN(
		SELECT '[' + STUFF(
			(SELECT ',''' + svc.Name + '''' 
			FROM dbo.LKService svc
				INNER JOIN StoreService ss ON svc.ServiceID = ss.ServiceId
			WHERE	ss.StoreId = @StoreID AND ss.IsActive = 1

			FOR XML PATH(''),TYPE).value('(./text())[1]','NVARCHAR(MAX)'),
				1, 1, '') + ']');
END
