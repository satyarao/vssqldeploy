﻿
CREATE FUNCTION [dbo].[GetTenantPropertyValue]
	(
	  @TenantID UNIQUEIDENTIFIER
	, @PropertyName NVARCHAR(128)
	, @HierarchyDepth SMALLINT = NULL
	)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE	@PropertyValue NVARCHAR(MAX)

	IF ( SELECT	IsActive
		 FROM		dbo.LKProperty
		 WHERE		PropertyName = @PropertyName
		) = 0
	BEGIN 
		RETURN NULL
	END 

	SELECT	@PropertyValue = PropertyValue
	FROM	dbo.TenantProperty
	WHERE	TenantID = @TenantID
			AND PropertyName = @PropertyName

	IF @PropertyValue IS NULL
		AND ( SELECT	Ascend
			  FROM		dbo.LKProperty
			  WHERE		PropertyName = @PropertyName
			) = 1
		BEGIN
			DECLARE	@ParentTenantID UNIQUEIDENTIFIER
			SELECT	@ParentTenantID = TenantID
			FROM	dbo.Store
			WHERE	StoreID = @TenantID
			IF @ParentTenantID IS NOT NULL
				BEGIN
					SET @TenantID = @ParentTenantID
					IF @HierarchyDepth IS NOT NULL
						SET @HierarchyDepth = @HierarchyDepth - 1
				END
			SELECT TOP 1
					@PropertyValue = tp.PropertyValue
			FROM	dbo.Tenant start
					JOIN dbo.Tenant parent ON start.OrgID.IsDescendantOf(parent.OrgID) = 1
					LEFT JOIN dbo.TenantProperty tp ON tp.TenantID = parent.TenantID
													   AND PropertyName = @PropertyName
			WHERE	START.TenantID = @TenantID
					AND tp.PropertyName IS NOT NULL
					AND start.OrgLevel - parent.OrgLevel <= COALESCE(@HierarchyDepth, start.OrgLevel - parent.OrgLevel)
			ORDER BY parent.OrgLevel DESC
		END
		IF(@PropertyValue IS NULL)
		BEGIN
			 SELECT TOP 1 @PropertyValue = DefaultValue FROM dbo.LKProperty WHERE PropertyName = @PropertyName AND Ascend = 1
		END 

	RETURN @PropertyValue
END

