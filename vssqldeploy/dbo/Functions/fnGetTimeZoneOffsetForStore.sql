﻿
CREATE FUNCTION [dbo].[fnGetTimeZoneOffsetForStore]
	(@StoreID UNIQUEIDENTIFIER)
RETURNS INT
AS
BEGIN
	RETURN ISNULL((SELECT tz.BaseUtcOffsetSec FROM Common.LKTimezone tz
	JOIN Store s on s.TimeZoneID = tz.TimeZoneID
	WHERE s.StoreID = @StoreID), 0)
END
