﻿
-- =============================================
-- Author:		Roman Lavreniuk
-- Create date: 02/27/2019
-- Description:	https://p97networks.atlassian.net/browse/XOM-43
-- Formerly 1.0.1167_1.0.1167 on mppadev
-- =============================================

CREATE   FUNCTION [dbo].[GetCarWashJsonByStoreID]
(
    @StoreID UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
    DECLARE @Result NVARCHAR(MAX) =
    (
        SELECT [POSCode]
              ,[Modifier]
              ,[POSCodeFormat]
              ,[Description]
              ,[PaymentSystemProductCode]
              ,[Amount]
              ,[CurrencyCode]
              ,[UnitOfMeasure]
              ,[UnitPrice]
              ,[TaxCode]
              ,[IsActive]
              ,[CreatedOn]
              ,[UpdatedOn]
              ,[StandardProductCode]
        FROM [dbo].[CarWash]
        WHERE [StoreID] = @StoreID

        FOR JSON PATH
    );
    RETURN @Result
END

