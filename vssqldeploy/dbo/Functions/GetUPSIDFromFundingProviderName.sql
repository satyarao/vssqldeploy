﻿CREATE FUNCTION [dbo].[GetUPSIDFromFundingProviderName]
(	
	-- Add the parameters for the function here
	@FundingProviderName nvarchar(255)
)

RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	with FundingProviderIDs as ( 
		select FundingProviderID
		from Payment.FundingProvider
		where FundingProviderName = @FundingProviderName
	),

	PaymentProcessorIDs as (
		select PaymentProcessorID
		from Payment.LKPaymentProcessor
		where FundingProviderID in (select * from FundingProviderIDs)
	),

	UserPaymentSourceIDs as (
		select UserPaymentSourceID
		from Payment.LKUserPaymentSource
		where PaymentProcessorID in (select * from PaymentProcessorIDs)
	)
	
	select UserPaymentSourceID
	from UserPaymentSourceIDs
)

