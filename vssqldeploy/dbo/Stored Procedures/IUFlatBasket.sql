﻿-- =============================================
-- Update date: 02/23/2018 Alex Goroshko - returned back ISNULL(source.VisibleByTenants, '[]') to fix bug about inserting NULL value into not nullable column
-- Update date: 04/12/2018 Andrei Ramanovich - added AppDisplayName, AppChannel
-- Update date: 07/30/2018 Roman Lavreniuk - added FleetTenantID as parameter
-- Update date: 08/17/2018 Roman Lavreniuk - added NuDetectScore and NuDetectScoreBand as parameter
-- Update date: 08/31/2018 William White - added OtherRewardDiscount as parameter
-- Update date: 09/05/2018 Jamy Ryals - Add Score Signals as pameter
-- Update date: 10/01/2018 Matt Liang - Update funding provider id from TINYINT to SMALLINT
-- Update date: 12/27/2018 Roman Lavreniuk - added CurrencyIsoCode as parameter
-- Update date: 01/21/2019 Roman Davydovich - added NumberOfGallons support
-- Update date: 05/17/2019 Roman Davydovich - added @BasketVersion check
-- Update date: 05/21/2019 Roman Davydovich - added @NumberOfGallons as argument
-- Update date: 06/26/2019 Darya Batalava - cahnged @TotalAmount from decimal to float
-- =============================================
CREATE PROC [dbo].[IUFlatBasket]
    @BasketPayment BasketPaymentType READONLY,
    @TransactionID uniqueidentifier,
    @StoreTenantID uniqueidentifier = NULL,
    @AppTenantID uniqueidentifier = NULL,
    @BasketStateName nvarchar(50) = NULL,
    @BasketStateID tinyint = NULL,
    @BasketLoyaltyStateName nvarchar(50) = NULL,
    @BasketLoyaltyStateID tinyint = NULL,
	@BasketLoyaltyPrograms nvarchar(max) = NULL,
    @UserID uniqueidentifier = NULL,
    @StoreMppaID nvarchar(50) = NULL,
    @UserEmail nvarchar(255) = NULL,
    @POSOperatorID nvarchar(50) = NULL,
    @POSTerminalID nvarchar(50) = NULL,
    @POSTransactionID nvarchar(50) = NULL,
    @PartnerTransactionID nvarchar(100) = NULL,
    @StoreName nvarchar(64) = NULL,
    @StoreAddress nvarchar(128) = NULL,
    @StoreCity nvarchar(64) = NULL,
    @StoreID uniqueidentifier = NULL,
    @StoreNumber nvarchar(64) = NULL,
	@StoreMerchantID nvarchar(50) = NULL,
    @POSDateTime datetime2(7) = NULL,
    @OriginalPrice [decimal](9, 2) = NULL,
    @ProductDiscount [decimal](9, 2) = NULL,
    @FuelDiscount [decimal](9, 2) = NULL,
    @SubTotal [decimal](9, 2) = NULL,
    @TaxAmount [decimal](9, 2) = NULL,
    @TotalAmount [float] = NULL,
    @UserPaymentSourceID INT = NULL,
    @BatchID int = NULL,
    @SettlementPeriodID nvarchar(50) = NULL,
    @Stac nvarchar(50) = NULL,
    @AuthCode nvarchar(max) = NULL,
    @IsSiteLevelPayment bit = NULL,
    @VisibleByTenants nvarchar(max) = NULL,
    @OfferIDs nvarchar(max) = NULL,
    @OfferNames nvarchar(max) = NULL,
    @AppDisplayName NVARCHAR(255),
    @AppChannel NVARCHAR(100),
	@CardType NVARCHAR(255) = NULL,
    @FleetTenantID UNIQUEIDENTIFIER = NULL,
    @NuDetectScore INT = NULL,
    @NuDetectScoreBand VARCHAR(20) = NULL,
	@ScoreSignals nvarchar(max) = NULL,
	@OtherRewardTotal [decimal](9, 2) = NULL,
    @CurrencyIsoCode CHAR(3),
	@Version INT,
	@NumberOfGallons [decimal](9, 2)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @BasketVersion INT = (SELECT [VERSION] FROM BasketVersion WHERE BasketID = @TransactionID);
	IF(@BasketVersion IS NOT NULL AND @BasketVersion != @Version)
	BEGIN
		RETURN;
	END
	
	DECLARE @FundingProviderID SMALLINT = NULL
    DECLARE @FundingProviderName nvarchar(255) = NULL
    DECLARE @PaymentInfo nvarchar(max) = NULL
    IF @UserPaymentSourceID IS NOT NULL
    BEGIN
        SELECT  @FundingProviderID = lkpp.FundingProviderID,
                @FundingProviderName = REPLACE(fp.FundingProviderName, 'Precidia', 'Merchant Link')
        FROM [Payment].[LKUserPaymentSource] lkups 
            JOIN [Payment].[LKPaymentProcessor] lkpp ON  lkpp.[PaymentProcessorID] = lkups.[PaymentProcessorID]
            JOIN [Payment].[FundingProvider] fp ON  fp.[FundingProviderID] = lkpp.[FundingProviderID] 
            WHERE lkups.UserPaymentSourceID = @UserPaymentSourceID
    END 
    IF EXISTS ( SELECT 1 FROM @BasketPayment )
    BEGIN
        DECLARE @tempPaymentInfo BasketPaymentType
        INSERT INTO @tempPaymentInfo ([PaymentCommand], [Amount], [ResponseResult], [ResponseResultText], [CreatedOn], [AuthCode], [RecNum], [IsSiteLevelPayment] )
            SELECT [PaymentCommand], [Amount], [ResponseResult], [ResponseResultText], [CreatedOn], PaymentInfo.[AuthCode], [RecNum], PaymentInfo.[IsSiteLevelPayment] 
                FROM FlatBasket fb 
                CROSS APPLY OPENJSON (fb.PaymentInfo) 
                    WITH (  
                        [PaymentCommand] nvarchar(50) N'$.PaymentCommand',
                        [Amount] decimal(9,2) N'$.Amount',
                        [ResponseResult] nvarchar(50) N'$.ResponseResult',
                        [ResponseResultText] nvarchar(50) N'$.ResponseResultText',
                        [CreatedOn] datetime2(7) N'$.PaymentDateTime',
                        [AuthCode] nvarchar(50) N'$.AuthCode',
                        [RecNum] nvarchar(50) N'$.RecNum',
                        [IsSiteLevelPayment] bit N'$.IsSiteLevelPayment'
                     ) AS PaymentInfo
                WHERE fb.TransactionID = @TransactionID
                UNION 
                SELECT [PaymentCommand], [Amount], [ResponseResult], [ResponseResultText], GETUTCDATE(), [AuthCode], [RecNum], [IsSiteLevelPayment] 
                FROM @BasketPayment
        SET @PaymentInfo = (SELECT [PaymentCommand]
                                ,[Amount]
                                ,[ResponseResult]
                                ,[ResponseResultText]
                                ,[CreatedOn] AS [PaymentDateTime]
                                ,[AuthCode]
                                ,[RecNum]
                                ,[IsSiteLevelPayment]
                                 FROM @tempPaymentInfo FOR JSON PATH)
    END
    IF @AuthCode IS NOT NULL
    BEGIN
        DECLARE @AuthCodeArray ListOfString
        
        INSERT INTO  @AuthCodeArray
        SELECT value FROM FlatBasket fb 
            CROSS APPLY OPENJSON (fb.AuthCode)
            WHERE fb.TransactionID = @TransactionID
            UNION SELECT @AuthCode
            SET @AuthCode = (SELECT ('[' + STUFF(( SELECT ',"' + stringValue + '"' AS [text()]
                            FROM @AuthCodeArray FOR XML PATH('')
                                    ), 1, 1, '') + ']'))
    END
    DECLARE @TimeZoneID INT
    SELECT  @TimeZoneID = ISNULL(TimeZoneID, 36)
    FROM    dbo.Store
    WHERE   StoreID = @StoreID
    DECLARE @POSDateTimeLocal DATETIME2
    SELECT  @POSDateTimeLocal = Common.fnConvertUtcToLocalByTimezoneID(@TimeZoneID, @POSDateTime)
    MERGE dbo.FlatBasket AS target
    USING
        (SELECT @TransactionID AS TransactionID,
                @StoreTenantID AS StoreTenantID,
                @AppTenantID AS AppTenantID,
                @BasketStateName AS BasketStateName,
                @BasketStateID AS BasketStateID,
                @BasketLoyaltyStateName AS BasketLoyaltyStateName,
                @BasketLoyaltyStateID AS BasketLoyaltyStateID,
                @BasketLoyaltyPrograms AS BasketLoyaltyPrograms,
                @UserID AS UserID,
                @StoreMppaID AS StoreMppaID,
                @UserEmail AS UserEmail,
                @POSOperatorID AS POSOperatorID,
                @POSTerminalID AS POSTerminalID,
                @POSTransactionID AS POSTransactionID,
                @PartnerTransactionID AS PartnerTransactionID,
                @StoreName AS StoreName,
                @StoreAddress AS StoreAddress,
                @StoreCity AS StoreCity,
                @StoreID AS StoreID,
                @StoreNumber AS StoreNumber,
				@StoreMerchantID AS StoreMerchantID,
                @POSDateTime AS POSDateTime,
                @POSDateTimeLocal AS POSDateTimeLocal,
                @OriginalPrice AS OriginalPrice,
                @ProductDiscount AS ProductDiscount,
                @FuelDiscount AS FuelDiscount,
                (@ProductDiscount + @FuelDiscount) AS TotalDiscount,
                @SubTotal AS SubTotal,
                @TaxAmount AS TaxAmount,
                @TotalAmount AS TotalAmount,
                @FundingProviderID AS FundingProviderID,
                @FundingProviderName AS FundingProviderName,
                @CardType AS CardType,
                @BatchID AS BatchID,
                @SettlementPeriodID AS SettlementPeriodID,
                @Stac AS Stac,
                @AuthCode AS AuthCode,
                @IsSiteLevelPayment AS IsSiteLevelPayment,
                @VisibleByTenants AS VisibleByTenants,
                @PaymentInfo AS PaymentInfo,
                @OfferNames AS OfferName,
                @OfferIDs AS OfferID,
                @AppDisplayName as AppDisplayName,
                @AppChannel as AppChannel,
                @FleetTenantID AS FleetTenantID,
                @NuDetectScore AS NuDetectScore,
                @NuDetectScoreBand AS NuDetectScoreBand,
				@ScoreSignals AS ScoreSignals,
                @OtherRewardTotal AS OtherRewardTotal,
                @CurrencyIsoCode AS CurrencyIsoCode,
				@Version as [Version],
				@NumberOfGallons as NumberOfGallons)  AS source
        ON ( (target.TransactionID = source.TransactionID))
        WHEN MATCHED THEN
            UPDATE SET [StoreTenantID] = ISNULL(source.StoreTenantID, target.StoreTenantID)
                  ,[AppTenantID] = ISNULL(source.AppTenantID, target.AppTenantID)
                  ,[BasketStateName] = ISNULL(source.BasketStateName, target.BasketStateName)
                  ,[BasketStateID] = ISNULL(source.BasketStateID, target.BasketStateID)
                  ,[BasketLoyaltyStateName] = ISNULL(source.BasketLoyaltyStateName, target.BasketLoyaltyStateName)
                  ,[BasketLoyaltyStateID] = ISNULL(source.BasketLoyaltyStateID, target.BasketLoyaltyStateID)
                  ,[BasketLoyaltyPrograms] = ISNULL(source.BasketLoyaltyPrograms, target.BasketLoyaltyPrograms)
                  ,[UserID] = ISNULL(source.UserID, target.UserID)
                  ,[StoreMppaID] = ISNULL(source.StoreMppaID, target.StoreMppaID)
                  ,[UserEmail] = ISNULL(source.UserEmail, target.UserEmail)
                  ,[POSOperatorID] = ISNULL(source.POSOperatorID, target.POSOperatorID)
                  ,[POSTerminalID] = ISNULL(source.POSTerminalID, target.POSTerminalID)
                  ,[POSTransactionID] = ISNULL(source.POSTransactionID, target.POSTransactionID)
                  ,[PartnerTransactionID] = ISNULL(source.PartnerTransactionID, target.PartnerTransactionID)
                  ,[StoreName] = ISNULL(source.StoreName, target.StoreName)
                  ,[StoreAddress] = ISNULL(source.StoreAddress, target.StoreAddress)
                  ,[StoreCity] = ISNULL(source.StoreCity, target.StoreCity)
                  ,[StoreID] = ISNULL(source.StoreID, target.StoreID)
                  ,[StoreNumber] = ISNULL(source.StoreNumber, target.StoreNumber)
                  ,[StoreMerchantID] = ISNULL(source.[StoreMerchantID], target.[StoreMerchantID])
                  ,[POSDateTimeLocal] = ISNULL(source.POSDateTimeLocal, target.POSDateTimeLocal)
                  ,[POSDateTime] = ISNULL(source.POSDateTime, target.POSDateTime)
                  ,[OriginalPrice] = ISNULL(source.OriginalPrice, target.OriginalPrice)
                  ,[ProductDiscount] = ISNULL(source.ProductDiscount, target.ProductDiscount)
                  ,[FuelDiscount] = ISNULL(source.FuelDiscount, target.FuelDiscount)
                  ,[TotalDiscount] = ISNULL(source.TotalDiscount, target.TotalDiscount)
                  ,[SubTotal] = ISNULL(source.SubTotal, target.SubTotal)
                  ,[TaxAmount] = ISNULL(source.TaxAmount, target.TaxAmount)
                  ,[TotalAmount] = ISNULL(source.TotalAmount, target.TotalAmount)
                  ,[FundingProviderID] = ISNULL(source.FundingProviderID, target.FundingProviderID)
                  ,[FundingProviderName] = ISNULL(source.FundingProviderName, target.FundingProviderName)
                  ,[CardType] = ISNULL(source.CardType, target.CardType)
                  ,[BatchID] = ISNULL(source.BatchID, target.BatchID)
                  ,[SettlementPeriodID] = ISNULL(source.SettlementPeriodID, target.SettlementPeriodID)
                  ,[Stac] = ISNULL(source.Stac, target.Stac)
                  ,[AuthCode] = ISNULL(source.AuthCode, target.AuthCode)
                  ,[IsSiteLevelPayment] = ISNULL(source.IsSiteLevelPayment,target.IsSiteLevelPayment)
                  ,[VisibleByTenants] = ISNULL(source.VisibleByTenants, target.VisibleByTenants)
                  ,[PaymentInfo]  = ISNULL(source.PaymentInfo, target.PaymentInfo)
                  ,[OfferID] = source.[OfferID]
                  ,[OfferName] = source.[OfferName]
                  ,[AppDisplayName] = ISNULL(source.AppDisplayName, target.AppDisplayName)
                  ,[AppChannel] = ISNULL(source.AppChannel, target.AppChannel)
                  ,[FleetTenantID] = ISNULL(source.FleetTenantID, target.FleetTenantID)
                  ,[NuDetectScore] = ISNULL(source.NuDetectScore, target.NuDetectScore)
                  ,[NuDetectScoreBand] = ISNULL(source.NuDetectScoreBand, target.NuDetectScoreBand)
				  ,[ScoreSignals] = ISNULL(source.ScoreSignals, target.ScoreSignals)
                  ,[OtherRewardTotal] = ISNULL(source.OtherRewardTotal, target.OtherRewardTotal)
                  ,[CurrencyIsoCode] = source.CurrencyIsoCode
				  ,[NumberOfGallons] = source.[NumberOfGallons]
				  ,[Version] = source.[Version]
        WHEN NOT MATCHED THEN
            INSERT ([TransactionID]
               ,[StoreTenantID]
               ,[AppTenantID]
               ,[BasketStateName]
               ,[BasketStateID]
               ,[BasketLoyaltyStateName]
               ,[BasketLoyaltyStateID]
               ,[BasketLoyaltyPrograms]
               ,[UserID]
               ,[StoreMppaID]
               ,[UserEmail]
               ,[POSOperatorID]
               ,[POSTerminalID]
               ,[POSTransactionID]
               ,[PartnerTransactionID]
               ,[StoreName]
               ,[StoreAddress]
               ,[StoreCity]
               ,[StoreID]
               ,[StoreNumber]
               ,[StoreMerchantID]
               ,[POSDateTimeLocal]
               ,[POSDateTime]
               ,[OriginalPrice]
               ,[ProductDiscount]
               ,[FuelDiscount]
               ,[TotalDiscount]
               ,[SubTotal]
               ,[TaxAmount]
               ,[TotalAmount]
               ,[FundingProviderID]
               ,[FundingProviderName]
               ,[CardType]
               ,[BatchID]
               ,[SettlementPeriodID]
               ,[Stac]
               ,[IsSiteBillable]
               ,[AuthCode]
               ,[IsSiteLevelPayment]
               ,[VisibleByTenants]
               ,[PaymentInfo]
               ,[OfferID]
               ,[OfferName]
               ,[AppDisplayName]
               ,[AppChannel]
               ,[FleetTenantID]
               ,[NuDetectScore]
               ,[NuDetectScoreBand]
			   ,[ScoreSignals]
               ,[OtherRewardTotal]
               ,[CurrencyIsoCode]
			   ,[NumberOfGallons]
			   ,[Version])
             VALUES
               (source.TransactionID
               ,source.StoreTenantID
               ,source.AppTenantID
               ,source.BasketStateName
               ,source.BasketStateID
               ,source.BasketLoyaltyStateName
               ,source.BasketLoyaltyStateID
               ,source.BasketLoyaltyPrograms
               ,source.UserID
               ,source.StoreMppaID
               ,source.UserEmail
               ,source.POSOperatorID
               ,source.POSTerminalID
               ,source.POSTransactionID
               ,source.PartnerTransactionID
               ,source.StoreName
               ,source.StoreAddress
               ,source.StoreCity
               ,source.StoreID
               ,source.StoreNumber
               ,source.StoreMerchantID
               ,source.POSDateTimeLocal
               ,source.POSDateTime
               ,source.OriginalPrice
               ,source.ProductDiscount
               ,source.FuelDiscount
               ,source.TotalDiscount
               ,source.SubTotal
               ,source.TaxAmount
               ,source.TotalAmount
               ,source.FundingProviderID
               ,source.FundingProviderName
               ,source.CardType
               ,source.BatchID
               ,source.SettlementPeriodID
               ,source.Stac
               , [dbo].[IsSiteBillable] (source.StoreTenantID, source.StoreID)
               ,source.AuthCode
               ,source.IsSiteLevelPayment
               ,ISNULL(source.VisibleByTenants, '[]')
               ,source.PaymentInfo
               ,source.OfferID
               ,source.OfferName
               ,source.AppDisplayName
               ,source.AppChannel
               ,source.FleetTenantID
               ,source.NuDetectScore
               ,source.NuDetectScoreBand
			   ,source.ScoreSignals
               ,source.OtherRewardTotal
               ,source.CurrencyIsoCode
			   ,source.[Version]
			   ,source.[NumberOfGallons]);
END

