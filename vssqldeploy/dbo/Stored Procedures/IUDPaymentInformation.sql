﻿CREATE PROCEDURE [dbo].[IUDPaymentInformation] 
  @TenantID UNIQUEIDENTIFIER
, @FrontEndID TINYINT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @FrontEndName NVARCHAR(250);
	DECLARE @PropertyName NVARCHAR(50);
	SET @PropertyName = 'OutsidePaymentProvider'

	SET @FrontEndName = (SELECT TOP 1 FrontEndTypeName 
						FROM [dbo].[FrontEndProcessor]
						WHERE FrontEndID = @FrontEndID)
	--Non-existant FrontEndID would mean that the tenant doesnt have a provider yet or is unknown

	IF EXISTS(
		SELECT 1
		FROM [dbo].[TenantProperty]
		WHERE TenantID = @TenantID AND PropertyName = @PropertyName
	)
	BEGIN
		UPDATE [dbo].[TenantProperty]
		SET PropertyValue = @FrontEndName
		WHERE TenantID = @TenantID AND PropertyName = @PropertyName
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[TenantProperty]
					([TenantID]
					,[PropertyName]
					,[PropertyValue])
				VALUES
					(@TenantID
					, @PropertyName
					, @FrontEndName);
	END
END


/*

EXEC	[dbo].[IUDPaymentInformation] 
  @TenantID = '00000000-0000-0000-0000-000000000000',
  @FrontEndID = 2

*/
