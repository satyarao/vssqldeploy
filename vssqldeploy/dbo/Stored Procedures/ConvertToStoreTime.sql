﻿

CREATE   PROCEDURE [dbo].[ConvertToStoreTime] (
	@ID uniqueidentifier,
	@Time time output
)
AS
BEGIN
 WITH T AS ( 
	SELECT TOP 1 LKT.StandardName 
	FROM dbo.Store S
	INNER JOIN Common.LKTimezone LKT ON S.TimeZoneID = LKT.TimeZoneID 
	WHERE S.StoreID = @ID
)

 SELECT @Time = CAST(GETUTCDATE() AT TIME ZONE T.StandardName AS TIME)
 FROM T
 RETURN

END
