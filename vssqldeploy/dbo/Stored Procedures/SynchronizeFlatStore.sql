﻿-- =============================================
-- Author:      Igor Gaidukov
-- Create Date: 11/29/2017
-- Update Date: 05/15/2018 - Update FuelProducts, OperatingHours, Services if store exists
-- Description: Synchronize FlatStore table and StoreService, Store, HoursOFOperations, FuelPrice tables
-- =============================================
CREATE PROCEDURE [dbo].[SynchronizeFlatStore]
(
    @StoreIDs ListOfGuid  READONLY
)
AS
BEGIN
    SET NOCOUNT ON

    MERGE dbo.FlatStore AS target
USING (
	SELECT 
		StoreOverrides.ConsumerTenantID											AS TenantID, 
		store.StoreID															AS StoreID,
		COALESCE(StoreOverrides.Name, store.Name)								AS Name, 
		COALESCE(StoreOverrides.StoreNumber, store.StoreNumber)					AS StoreNumber, 
		store.MerchantId														AS MerchantID,
		store.Phone																AS Phone,
		store.NumberOfPumps														AS NumberOfPumps,
		store.Latitude															AS Latitude,
		store.Longitude															AS Longitude,
		store.StreetAddress														AS StreetAddress,
		store.City																AS City,
		store.StateCode															AS StateCode,
		store.ZipCode															AS PostalCode,
		store.CountryIsoCode													AS CountryIsoCode,
		FuelBrand.Name															AS FuelBrand,
		COALESCE(StoreOverrides.DisplayOnMobile, store.DisplayOnMobile)			AS DisplayOnMobile, 
		store.County															AS County,
		(SELECT FORMAT(store.MppaID, '0000-####'))								AS MppaID,
		store.StoreInstallTypeID												AS StoreInstallTypeID,
		store.TenantID															AS OwnerID,
		si.ImageUrl																AS StationImageUrl,
		COALESCE(StoreOverrides.AllowInsidePayment, store.AllowInsidePayment)	AS AllowInsidePayment, 
		COALESCE(StoreOverrides.AllowOutsidePayment, store.AllowOutsidePayment)	AS AllowOutsidePayment, 
		COALESCE(StoreOverrides.ActiveAlertsMaintenanceMode, store.ActiveAlertsMaintenanceMode)	AS ActiveAlertsMaintenanceMode, 
		store.UnavailablePumps													AS UnavailablePumps,
		COALESCE(StoreOverrides.IsSiteBillable, store.IsSiteBillable)			AS IsSiteBillable, 
		COALESCE(StoreOverrides.ActiveAlertsIgnored, store.ActiveAlertsIgnored)	AS ActiveAlertsIgnored,
		store.IsDeleted															AS IsDeleted,
		store.IsBootstrapped													AS IsBootstrapped,
		store.EposIds															AS EposIds,
		store.SiteUnavailablePumps												AS SiteUnavailablePumps
	FROM Store store
	CROSS APPLY OPENJSON(store.StoreTenants) 
				WITH (  
						ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID',   
						StoreNumber NVARCHAR(16) N'$.StoreNumber',  
						Name NVARCHAR(64) N'$.Name',  
						DisplayOnMobile BIT N'$.DisplayOnMobile',  
						StationImageID INT N'$.StationImageID',  
						AllowInsidePayment BIT N'$.AllowInsidePayment',  
						AllowOutsidePayment BIT N'$.AllowOutsidePayment',  
						ActiveAlertsMaintenanceMode BIT N'$.ActiveAlertsMaintenanceMode',  
						IsSiteBillable     BIT N'$.IsSiteBillable',  
						ActiveAlertsIgnored BIT N'$.ActiveAlertsIgnored'
					) 
	AS StoreOverrides
	JOIN FuelBrand ON FuelBrand.FuelBrandID = store.FuelBrandID
	LEFT JOIN AppFeatures.Stationimage si ON COALESCE(StoreOverrides.StationImageID, store.StationImageID) = si.StationImageID
	WHERE store.StoreTenants IS NOT NULL AND ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR store.StoreID IN (SELECT ID FROM @StoreIDs) )
	UNION
	SELECT store.TenantID, 
		store.StoreID, 
		store.Name, 
		StoreNumber, 
		MerchantID, 
		Phone, 
		NumberOfPumps, 
		Latitude, 
		Longitude, 
		StreetAddress, 
		City, 
		StateCode, 
		ZipCode AS PostalCode, 
		CountryIsoCode, 
		FuelBrand.Name AS FuelBrand, 
		store.DisplayOnMobile, 
		County, 
		(SELECT FORMAT(store.MppaID, '0000-####')) AS MppaID, 
		StoreInstallTypeID, 
		store.TenantID AS OwnerID, 
		si.ImageUrl AS StationImageUrl, 
		AllowInsidePayment, 
		AllowOutsidePayment, 
		ActiveAlertsMaintenanceMode, 
		UnavailablePumps, 
		IsSiteBillable, 
		ActiveAlertsIgnored, 
		store.IsDeleted, 
		store.IsBootstrapped, 
		store.EposIds, 
		SiteUnavailablePumps
	FROM Store store
	JOIN FuelBrand ON FuelBrand.FuelBrandID = store.FuelBrandID
	LEFT JOIN AppFeatures.Stationimage si ON store.StationImageID = si.StationImageID
	WHERE ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR store.StoreID IN (SELECT ID FROM @StoreIDs) )
) AS source
ON (target.TenantID = source.TenantID AND target.StoreID = source.StoreID)
	
WHEN NOT MATCHED BY TARGET THEN
	INSERT(TenantID, 
			StoreID,
			Name,
			StoreNumber,
			MerchantID,	
			Phone,		  
			NumberOfPumps,
			Latitude,
			Longitude,
			StreetAddress,
			City,
			StateCode,
			StateName,
			PostalCode,
			CountryIsoCode,
			FuelBrand,
			DisplayOnMobile,
			County,
			MppaID,
			StoreInstallTypeID,
			OwnerID,
			StationImageUrl,
			AllowInsidePayment,
			AllowOutsidePayment,
			ActiveAlertsMaintenanceMode,
			UnavailablePumps,
			IsSiteBillable,
			ActiveAlertsIgnored,
			FuelProducts,
			OperatingHours,
			[Services],
			[Geo],
			IsDeleted,
			IsBootstrapped,
			EposIds,
			SiteUnavailablePumps)
	VALUES(source.TenantID, 
		source.StoreID, 
		source.Name, 
		source.StoreNumber, 
		source.MerchantID, 
		source.Phone, 
		source.NumberOfPumps, 
		source.Latitude, 
		source.Longitude, 
		source.StreetAddress, 
		source.City, 
		source.StateCode, 
		[dbo].[GetStateNameByCode] ( source.StateCode ),
		source.PostalCode, 
		source.CountryIsoCode, 
		source.FuelBrand, 
		source.DisplayOnMobile, 
		source.County, 
		source.MppaID, 
		source.StoreInstallTypeID,
		source.OwnerID, 
		source.StationImageUrl, 
		source.AllowInsidePayment, 
		source.AllowOutsidePayment, 
		source.ActiveAlertsMaintenanceMode, 
		source.UnavailablePumps, 
		source.IsSiteBillable, 
		source.ActiveAlertsIgnored, 
		dbo.GetFuelPricesJsonByStoreID(source.StoreID), 
		dbo.GetOperatingHoursJsonByStoreID(source.StoreID), 
		dbo.GetStoreServiceArray(source.StoreID), 
		Common.GetGeographyPoint(source.Latitude, source.Longitude), 
		source.IsDeleted, 
		source.IsBootstrapped, 
		source.EposIds, 
		SiteUnavailablePumps)



WHEN MATCHED THEN
	UPDATE 
	SET	
		target.Name = source.Name, 
		target.StoreNumber = source.StoreNumber, 
		target.MerchantID = source.MerchantID, 
		target.Phone = source.Phone, 
		target.NumberOfPumps = source.NumberOfPumps, 
		target.Latitude = source.Latitude, 
		target.Longitude = source.Longitude, 
		target.StreetAddress = source.StreetAddress, 
		target.City = source.City, 
		target.StateCode = source.StateCode,
		target.StateName = [dbo].[GetStateNameByCode] ( source.StateCode ),
		target.PostalCode = source.PostalCode, 
		target.CountryIsoCode = source.CountryIsoCode, 
		target.FuelBrand = source.FuelBrand, 
		target.DisplayOnMobile = source.DisplayOnMobile, 
		target.County = source.County, 
		target.MppaID = source.MppaID, 
		target.StoreInstallTypeID = source.StoreInstallTypeID, 
		target.OwnerID = source.OwnerID, 
		target.StationImageUrl = source.StationImageUrl, 
		target.AllowInsidePayment = source.AllowInsidePayment, 
		target.AllowOutsidePayment = source.AllowOutsidePayment, 
		target.ActiveAlertsMaintenanceMode = source.ActiveAlertsMaintenanceMode, 
		target.UnavailablePumps = source.UnavailablePumps, 
		target.IsSiteBillable = source.IsSiteBillable, 
		target.ActiveAlertsIgnored = source.ActiveAlertsIgnored,
		target.Geo = [Common].[GetGeographyPoint](source.Latitude, source.Longitude),
		target.IsDeleted = source.IsDeleted,
		target.IsBootstrapped = source.IsBootstrapped,
		target.EposIds = source.EposIds,
		target.SiteUnavailablePumps = source.SiteUnavailablePumps,
		target.FuelProducts = dbo.GetFuelPricesJsonByStoreID(source.StoreID), 
		target.OperatingHours = dbo.GetOperatingHoursJsonByStoreID(source.StoreID),
		target.[Services] = dbo.GetStoreServiceArray(source.StoreID)
	;

DELETE fs
FROM dbo.FlatStore AS fs
WHERE EXISTS (
	SELECT StoreID, TenantID FROM
	(SELECT store.StoreID, StoreTenants.ConsumerTenantID AS TenantID FROM Store store 
	CROSS APPLY OPENJSON(store.StoreTenants) 
				WITH (  
						ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID'   
					) 
			AS StoreTenants
	WHERE ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR store.StoreID IN (SELECT ID FROM @StoreIDs) )
	UNION
	SELECT store.StoreID, store.TenantID FROM Store store
	) AS blah
	WHERE fs.StoreID = blah.StoreID 
	AND ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR blah.StoreID IN (SELECT ID FROM @StoreIDs) )
	AND fs.TenantID NOT IN (SELECT StoreTenants.ConsumerTenantID AS TenantID FROM Store store
						CROSS APPLY OPENJSON(store.StoreTenants) 
									WITH (  
											ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID'   
										) 
								AS StoreTenants
						WHERE fs.StoreID = store.StoreID AND ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR store.StoreID IN (SELECT ID FROM @StoreIDs) )
						UNION
						SELECT store.TenantID FROM Store store
						WHERE fs.StoreID = store.StoreID AND ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR store.StoreID IN (SELECT ID FROM @StoreIDs) )
						))
END
