﻿
CREATE PROCEDURE [dbo].[GetParentTenant]
(
  @ChildTenantID UNIQUEIDENTIFIER
)
AS
BEGIN
    SELECT parentTenant.TenantID
    ,parentTenant.Name
    ,parentTenant.TenantTypeID
	,parentTenant.DistributionChannels
	,parentTenant.HeaderLogo
	,parentTenant.FooterLogo
    ,lktp.Name AS TenantTypeName
    ,parentTenant.IsActive
    ,parentTenant.ActiveAlertsMaintenanceMode
    ,childTenant.TenantID AS ChildTenantID
    ,childTenant.Name AS ChildTenantName
	, [dbo].[GetTenantPropertyValue](parentTenant.TenantID, 'Licensing.SalesChannel', null) AS SalesChannel
  FROM dbo.Tenant childTenant
  JOIN dbo.LKTenantType lktp ON childTenant.TenantTypeID = lktp.TenantTypeID
  LEFT JOIN dbo.Tenant parentTenant ON childTenant.OrgID.IsDescendantOf(parentTenant.OrgID) = 1
          AND parentTenant.TenantID <> childTenant.TenantID
          AND childTenant.OrgLevel - parentTenant.OrgLevel = 1 
 WHERE childTenant.TenantID = @ChildTenantID
END
