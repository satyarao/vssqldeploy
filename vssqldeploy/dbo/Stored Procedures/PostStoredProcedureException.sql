﻿

CREATE   PROCEDURE [dbo].[PostStoredProcedureException] @Exceptions StoredProcedureExceptionType READONLY
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

	INSERT INTO [dbo].[StoredProcedureException]
		([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [StoredProcedureInput], [ErrorSeverity])
	SELECT
		 [ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [StoredProcedureInput], [ErrorSeverity]
	FROM @Exceptions
END
