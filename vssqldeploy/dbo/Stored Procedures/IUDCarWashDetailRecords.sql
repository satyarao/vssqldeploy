﻿
CREATE PROCEDURE dbo.IUDCarWashDetailRecords
	@Records AS [dbo].[CarWashType] READONLY
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRAN car_wash_details_save_record
	BEGIN TRY		

		DELETE FROM [dbo].[CarWash]
		WHERE [StoreId] IN (SELECT DISTINCT [StoreId] FROM @Records)

		INSERT [dbo].[CarWash] (
			[StoreID],
			[POSCode],
			[Modifier],
			[POSCodeFormat],
			[Description],
			[PaymentSystemProductCode],
			[Amount],
			[CurrencyCode],
			[UnitOfMeasure],
			[UnitPrice],
			[TaxCode],
			[IsActive],
			[CreatedOn],
			[Quantity],
			[MerchandiseCode],
			[SellingUnits],
			[LineItemId],
			[CarWashCode],
			[CarWashExpirationDate],
			[CarWashText],
			[StandardProductCode]
		)
		SELECT 
			SRC.[StoreID],
			SRC.[POSCode],
			SRC.[Modifier],
			SRC.[POSCodeFormat],
			SRC.[Description],
			SRC.[PaymentSystemProductCode],
			SRC.[Amount],
			SRC.[CurrencyCode],
			SRC.[UnitOfMeasure],
			SRC.[UnitPrice],
			SRC.[TaxCode],
			SRC.[IsActive],
			GETUTCDATE(),
			SRC.[Quantity],
			SRC.[MerchandiseCode],
			SRC.[SellingUnits],
			SRC.[LineItemId],
			SRC.[CarWashCode],
			SRC.[CarWashExpirationDate],
			SRC.[CarWashText],
			SRC.[StandardProductCode]
		FROM @Records SRC

		COMMIT TRAN car_wash_details_save_record

	END TRY
	BEGIN CATCH

		ROLLBACK TRAN car_wash_details_save_record;
		THROW;

	END CATCH
END
