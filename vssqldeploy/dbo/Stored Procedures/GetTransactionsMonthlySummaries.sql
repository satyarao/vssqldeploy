﻿-- =============================================
-- Create date: 1/27/2016
-- Update date: 10/21/2016
-- Update date: 04/14/2018 Andrei Ramanovich - Add @Channel
-- Description:	Gets monthly summaries for user's transactions
-- =============================================
CREATE PROCEDURE [dbo].[GetTransactionsMonthlySummaries] 
		@UserID UNIQUEIDENTIFIER
	   ,@StartDate DATE = NULL
	   ,@EndDate DATE = NULL
	   ,@Channel NVARCHAR(100) = NULL
AS

BEGIN
	SELECT YEAR(POSDateTime) as YearDate
		  ,MONTH(POSDateTime) as MonthDate
		  ,'$' + CAST(SUM(Total) AS VARCHAR(10)) as FormattedMonthlyTotalAmount
		  ,COUNT(b.BasketID) as MonthlyTotalTransactions
	FROM dbo.Basket b
	LEFT JOIN [dbo].[LKBasketState] lkbs on lkbs.BasketStateID = b.BasketStateID
	WHERE b.UserID = @UserID
	AND CONVERT(DATE, b.POSDateTime) BETWEEN ISNULL(@StartDate, CONVERT(DATE, '1/1/1753')) AND ISNULL(@EndDate, CONVERT(DATE, GETUTCDATE()))
	AND (@Channel IS NULL OR b.AppChannel = @Channel)
	AND lkbs.Name IN ('Complete')
	GROUP BY Year(POSDateTime), Month(POSDateTime)
	ORDER BY Year(POSDateTime), Month(POSDateTime)
END	

