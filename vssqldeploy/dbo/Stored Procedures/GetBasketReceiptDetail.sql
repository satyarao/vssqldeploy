﻿

CREATE PROCEDURE [dbo].[GetBasketReceiptDetail]
	@BasketID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
		
	DECLARE @AuthCode NVARCHAR(50);
	SET @AuthCode = (SELECT TOP 1 AuthCode FROM dbo.BasketPayment 
							where BasketID = @BasketID Order by BasketPaymentID desc)
	SELECT	
		b.BasketID
		--Store
		, b.StoreID
		, b.AppTenantID as TenantID
		, store.Name
		, store.Phone
		, store.StreetAddress as PrimaryAddress
		, store.City
		, store.StateCode
		, store.ZipCode AS PostalCode
		, store.MerchantId
		, POSDateTimeLocal
		, 'UTC' as TimeZoneName
		--User
		, ui.UserID as UserID
		, ui.EmailAddress as EmailAddress
		, @AuthCode as AuthCode
	FROM	dbo.Basket b
	LEFT JOIN dbo.Store store ON store.StoreID = b.StoreID
	LEFT JOIN dbo.UserInfo ui ON b.UserID = ui.UserID
	WHERE	b.BasketID = @BasketID
END
