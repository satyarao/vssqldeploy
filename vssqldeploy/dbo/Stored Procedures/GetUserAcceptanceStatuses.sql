﻿CREATE     PROCEDURE [dbo].[GetUserAcceptanceStatuses] @TenantId UNIQUEIDENTIFIER, @ApplicationId UNIQUEIDENTIFIER, @UserId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO dbo.AcceptanceStatuses 
	SELECT  @UserId, 
			TermsConditionsId, 
			2, -- NOT ACCEPTED STATUS 
			GETDATE(), 
			1 -- IsActive = 1
	FROM dbo.TermsAndConditionsDetails 
	WHERE  IsActive = 1 
		   AND (
				(TenantId = @TenantId AND ApplicationId IS NULL) -- if application id is null T&C for all tenant's applications
				OR ApplicationId = @ApplicationId
			   )
		   AND TermsConditionsId NOT IN 
			(
				SELECT TermsConditionsId 
				FROM AcceptanceStatuses 
				WHERE UserId = @UserId
			)
	
	SELECT  statuses.AcceptanceStatus AS AcceptanceStatus,
			provider.ServiceProviderId AS ServiceProviderId,
			tenant.Name AS ServiceProviderName,
			service.ServiceName AS ServiceType
	FROM dbo.AcceptanceStatuses statuses
		 INNER JOIN dbo.TermsAndConditionsDetails details 
		 ON statuses.TermsConditionsId = details.TermsConditionsId
		 INNER JOIN dbo.ServiceProvider provider
		 ON details.ServiceProviderId = provider.ServiceProviderId
		 INNER JOIN dbo.Tenant tenant
		 ON provider.ProviderTenantId = tenant.TenantID
		 INNER JOIN LKSiteServices service
		 ON provider.ServiceId = service.ServiceId
	WHERE statuses.UserId = @UserId
		  AND (
				(details.TenantId = @TenantId AND details.ApplicationId IS NULL) -- if application id is null T&C for all tenant's applications
				OR details.ApplicationId = @ApplicationId
			   )
		  AND statuses.IsActive = 1
END

