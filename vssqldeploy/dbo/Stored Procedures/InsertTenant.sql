﻿
-- =============================================
-- Update date: 7/11/2018 Darya Batalava - added ability to insert distribution channels
-- Update date: 11/28/2018 Darya Batalava - added ability to insert logo
-- =============================================
CREATE PROCEDURE [dbo].[InsertTenant]
	@ParentTenantID UNIQUEIDENTIFIER
  , @TenantID UNIQUEIDENTIFIER
  , @TenantName VARCHAR(255)
  , @TenantTypeID INT
  , @DistributionChannels NVARCHAR(MAX) = NULL
  , @HeaderLogo NVARCHAR(MAX) = NULL
  , @FooterLogo NVARCHAR(MAX) = NULL
AS 
BEGIN
/*
DECLARE @TenantID UNIQUEIDENTIFIER
SELECT @TenantID = NEWID()

exec [dbo].[InsertTenant]
	@ParentTenantID = '00000000-0000-0000-0000-000000000000'
  , @TenantID = @TenantID
  , @TenantName = 'Merchant Customer Exchange'
  , @TenantTypeID = 5
*/

SET NOCOUNT ON;

DECLARE	@parent HIERARCHYID
DECLARE	@lastchild HIERARCHYID
DECLARE	@child HIERARCHYID

SELECT	@parent = OrgID
FROM	dbo.tenant
WHERE	tenantid = @parentTenantID

SELECT	@lastchild = ( SELECT	MAX(OrgID)
					   FROM		dbo.tenant
					   WHERE	OrgID.GetAncestor(1) = @parent
					 );

SET @child = @parent.GetDescendant(@lastchild, NULL);

	--    select @parent, @lastchild, @child
INSERT	INTO dbo.tenant
		( NAME
		, TenantID
		, DistributionChannels
		, orgID
		, TenantTypeID
		, isactive
		, createdBy
		, createdon
		, HeaderLogo
		, FooterLogo
		)
VALUES	( @TenantName
		, @TenantID
		, @DistributionChannels
		, @child
		, @TenantTypeID
		, 1
		, 'InsertTenant'
		, GETDATE()
		, @HeaderLogo
		, @FooterLogo
		)

END





