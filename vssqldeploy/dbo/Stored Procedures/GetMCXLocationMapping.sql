﻿
CREATE PROCEDURE [dbo].[GetMCXLocationMapping]
	@TenantId UNIQUEIDENTIFIER = NULL,
	@StoreId UNIQUEIDENTIFIER = NULL,
	@StoreLocationId NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @StoreId IS NOT NULL
	BEGIN
    	SELECT lm.StoreID
		  ,s.TenantID 
		  ,lm.MCXFuelLocationId as  FuelLocationId
		  ,lm.MCXPOSLocationId as PosLocationId
		  FROM MCX_LocationMapping lm
		  JOIN Store s ON s.StoreID = lm.StoreID
		  WHERE lm.StoreID = @StoreId
	END
	ELSE IF @StoreLocationId IS NOT NULL
	BEGIN
		SELECT lm.StoreID
		  ,s.TenantID
		  ,lm.MCXFuelLocationId as FuelLocationId
		  ,lm.MCXPOSLocationId as PosLocationId
		  FROM MCX_LocationMapping lm
		  JOIN Store s ON s.StoreID = lm.StoreID
		  WHERE lm.MCXFuelLocationId = @StoreLocationId OR
			 lm.MCXPOSLocationId = @StoreLocationId
	END
	ELSE
	BEGIN
		SELECT lm.StoreID
		  ,s.TenantID 
		  ,lm.MCXFuelLocationId as  FuelLocationId
		  ,lm.MCXPOSLocationId as PosLocationId
		  FROM MCX_LocationMapping lm
		  JOIN Store s ON s.StoreID = lm.StoreID AND s.TenantID = COALESCE(@TenantId, s.TenantID)
	END
END

