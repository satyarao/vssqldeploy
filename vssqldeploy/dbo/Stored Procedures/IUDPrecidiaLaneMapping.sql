﻿CREATE PROCEDURE [dbo].[IUDPrecidiaLaneMapping]
	@TargetID UNIQUEIDENTIFIER,
	@POSTerminalID NVARCHAR(50),
	@Lane NVARCHAR(50),
	@Delete BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	IF @Delete = 1
	BEGIN
		DELETE FROM dbo.ConfigPrecidiaLaneMap WHERE TenantID = @TargetID AND POSTerminalID = @POSTerminalID
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT TOP 1 * FROM dbo.ConfigPrecidiaLaneMap WHERE TenantID = @TargetID AND POSTerminalID = @POSTerminalID)
		BEGIN
			UPDATE dbo.ConfigPrecidiaLaneMap
			SET Lane = @Lane
			WHERE TenantID = @TargetID AND POSTerminalID = @POSTerminalID
		END
		ELSE
		BEGIN
			INSERT INTO dbo.ConfigPrecidiaLaneMap VALUES (@TargetID, @POSTerminalID, @Lane)
		END 
		
	END
END
