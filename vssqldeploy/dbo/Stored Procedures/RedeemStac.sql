﻿-- ===========================================================
-- Author:		Daniel DiPaolo
-- Create date: 2015/07/24
-- Description:	Redeems a STAC if redeemable, else expires it.
-- ===========================================================
CREATE PROCEDURE [dbo].[RedeemStac] 
	-- Add the parameters for the stored procedure here
	@STAC uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @RedeemResult TINYINT;   
	
	-- Will return the following
	-- 2 = already redeemed
	-- 1 = expired
	-- 0 = redeemed successful

	SELECT
		@RedeemResult = CASE
			WHEN TokenStatusID <> 1 THEN 2
			WHEN Expiration < GETUTCDATE() THEN 1
			ELSE 0
		END
	FROM
		STAC
	WHERE
		Token = @STAC

	UPDATE
		STAC
	SET 
		TokenStatusID = CASE 
			WHEN Expiration < GETUTCDATE()THEN  3   -- expiration has passed	
			ELSE 2
		END
	WHERE
		Token = @STAC

	SELECT @RedeemResult;
END
