﻿
CREATE PROCEDURE IncrementBasketVersion
	@BasketID UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @CurrentVersion INT = (SELECT [Version] FROM BasketVersion WHERE [BasketID] = @BasketID);
	DECLARE @Version INT;
	IF(@CurrentVersion IS NULL)
	BEGIN
		SET @Version = 1;
		INSERT BasketVersion (BasketID, Version) VALUES(@BasketID, @Version);
	END
	ELSE
	BEGIN
		SET @Version = @CurrentVersion + 1;
		UPDATE BasketVersion SET Version = @Version WHERE [BasketID] = @BasketID
	END
	SELECT @Version;
END
