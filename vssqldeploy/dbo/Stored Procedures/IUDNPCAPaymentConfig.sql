﻿CREATE PROCEDURE [dbo].[IUDNPCAPaymentConfig]
	@TenantID [uniqueidentifier],
	@MerchantID [nvarchar](50),
    @StoreName [nvarchar](30) = NULL,
    @StoreID [nvarchar](50),
    @APIVersion [nvarchar](10) = NULL,
    @URI [nvarchar](250) = NULL,
    @AuthUserName [nvarchar](50) = NULL,
    @Password [nvarchar](50) = NULL,
    @IsActive [bit] = NULL
AS 
BEGIN
	SET NOCOUNT ON;

	IF (EXISTS(
		SELECT 1
		FROM [dbo].[ConfigNPCA]
		WHERE TenantID = @TenantID
	))
	BEGIN
		UPDATE [dbo].[ConfigNPCA]
		SET StoreName = @StoreName,
			StoreID = ISNULL(@StoreID, StoreID),
			URI = ISNULL(@URI, URI),
			AuthUserName = ISNULL(@AuthUserName, AuthUserName),
			AuthPassword = ISNULL(@Password, AuthPassword),
			IsActive = ISNULL(@IsActive, 1),
			UpdatedOn = GETUTCDATE(),
			MerchantID = @MerchantID,
			APIVersion = ISNULL(@APIVersion, APIVersion)
		WHERE TenantID = @TenantID
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[ConfigNPCA]
			( TenantID
			, MerchantID
			, StoreName
			, StoreID
			, APIVersion
			, URI
			, AuthUserName
			, AuthPassword
			, IsActive
			, CreatedOn
		   ) 
		VALUES (@TenantID
			, @MerchantID
			, @StoreName
			, @StoreID
			, ISNULL(@APIVersion, 1)
			, ISNULL(@URI, 'https://mobile.paymentcard.com/MPayAutoActivate/AuthService/AuthTypeBasic')
			, ISNULL(@AuthUserName, 'P97User1')
			, ISNULL(@Password, '46NU|O/?*=-@w]Bt''P')
			, ISNULL(@IsActive, 1)
			, GETUTCDATE()
		   );
	END
END

--EXEC [dbo].[IUDNPCAPaymentConfig] 
--@TenantID = '00000000-0000-0000-0000-000000000001',
--@MerchantID = N'SHGO',
--@StoreName = N'Store',
--@StoreID = N'000000',
--@APIVersion = N'1',
--@URI = N'https://mobile.paymentcard.com/MPayAutoActivate/AuthService/AuthTypeBasic',
--@AuthUserName = NULL,
--@Password = NULL
