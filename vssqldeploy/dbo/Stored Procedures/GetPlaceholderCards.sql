﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 3/27/2017
-- Update date: 5/17/2017 - add card tenant support 
-- Update date: 9/29/2017 - added multiple tenants support 
-- Description:	Returns placeholder cards for a tenant
-- =============================================
CREATE PROCEDURE [dbo].[GetPlaceholderCards]
	@TenantID UNIQUEIDENTIFIER
	,@LoggedIn bit = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT cd.[Type]
	,cd.[PlaceholderID]
	,cd.[Target]
	,CAST( ISNULL ( 
		(SELECT value FROM OPENJSON((SELECT pc.[OrdersPayload] FROM  [dbo].[PlaceholderCards] pc WHERE pc.[TenantID] = @TenantID), '$') where [key] = cd.[Type])
		, cd.[Order] 
	) AS int) as 'Order'

	FROM [dbo].[PlaceholderCardData] cd
	WHERE 
	(cd.[TenantIds] IS NULL OR @TenantID IN (SELECT value FROM OPENJSON (cd.[TenantIds])))
	AND
	(@LoggedIn IS NULL OR 
	(@LoggedIn = 1 AND cd.Target IN ('authorized', 'all')) OR 
	(@LoggedIn = 0 AND cd.Target IN ('anonymous', 'all'))
	)
	ORDER BY 'Order' ASC, cd.[PlaceholderID] ASC
END

