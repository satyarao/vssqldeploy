﻿CREATE PROCEDURE [dbo].[GetActiveStoreIdsForTenant]
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT StoreId
	FROM [dbo].[Store]
	WHERE TenantID = @TenantID AND DisplayOnMobile = 1
END

