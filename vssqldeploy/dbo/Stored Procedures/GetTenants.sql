﻿-- =============================================
-- Udpate date: 2/23/2018 Igor Gaidukov - added ability to search by keyword (SearchTerm), added ability to filter by SalesChannel and ParentTenantID
-- Udpate date: 7/11/2018 Darya Batalava - added ability to get distribution channels
-- Update date: 10/2/2018 Vadim Prikich - changed only search term flow, to search against TenantName and TenantId. 
-- Update date: 11/29/2018 Darya Batalava - added ability to get logo
-- Update date: 8/1/2019 Alex Goroshko - added fuzzy search on searchterm against tenantId column
-- =============================================
CREATE PROCEDURE [dbo].[GetTenants]
  @RestrictedTenantIDs ListOfGuid READONLY
 ,@IsActive BIT = NULL
 ,@TenantTypeID INT = NULL
 ,@SearchTerm NVARCHAR(MAX) = NULL
 ,@TenantName NVARCHAR(MAX) = NULL
 ,@IsUnderMaintenanceMode BIT = NULL
 ,@SalesChannel NVARCHAR(255) = NULL
 ,@ParentTenantID UNIQUEIDENTIFIER = NULL
AS 

BEGIN
 SELECT childTenant.TenantID
    ,childTenant.Name
    ,childTenant.TenantTypeID
	,childTenant.DistributionChannels
	,childTenant.HeaderLogo
	,childTenant.FooterLogo
    ,lktp.Name AS TenantTypeName
    ,childTenant.IsActive
    ,childTenant.ActiveAlertsMaintenanceMode
    ,parentTenant.TenantID AS ParentTenantID
    ,parentTenant.Name AS ParentTenantName
	, [dbo].[GetTenantPropertyValue](childTenant.TenantID, 'Licensing.SalesChannel', null) AS SalesChannel
  FROM dbo.Tenant childTenant
  JOIN dbo.LKTenantType lktp ON childTenant.TenantTypeID = lktp.TenantTypeID
  LEFT JOIN dbo.Tenant parentTenant ON childTenant.OrgID.IsDescendantOf(parentTenant.OrgID) = 1
          AND parentTenant.TenantID <> childTenant.TenantID
          AND childTenant.OrgLevel - parentTenant.OrgLevel = 1

  WHERE ( @IsActive IS NULL OR childTenant.IsActive = @IsActive)
    AND (@TenantTypeID IS NULL OR childTenant.TenantTypeID = @TenantTypeID)
	AND (@SearchTerm IS NULL OR childTenant.Name LIKE '%'+@SearchTerm+'%'
						     OR childTenant.TenantID LIKE '%'+@SearchTerm+'%')
	AND (@TenantName IS NULL OR childTenant.Name LIKE '%'+ @TenantName +'%')
	AND (@IsUnderMaintenanceMode IS NULL OR childTenant.ActiveAlertsMaintenanceMode = @IsUnderMaintenanceMode)
	AND (childTenant.TenantID IN (SELECT ID FROM @RestrictedTenantIDs) OR ( NOT EXISTS (SELECT * FROM @RestrictedTenantIDs) ))
	AND (@SalesChannel IS NULL OR [dbo].[GetTenantPropertyValue](childTenant.TenantID, 'Licensing.SalesChannel', null) = @SalesChannel)
	AND (@ParentTenantID IS NULL OR parentTenant.TenantID = @ParentTenantID)
	
  ORDER BY childTenant.Name ASC
END





