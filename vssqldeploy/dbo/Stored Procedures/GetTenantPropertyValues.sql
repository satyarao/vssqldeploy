﻿-- Author:		Hapeyeu Dzmitry
-- Create date: 10/17/2019
-- Update date: 04/11/2019 - Hapeyeu Dzmitry get property values for stores
-- Description:	Get property values for tenants and stores
CREATE     PROCEDURE [dbo].[GetTenantPropertyValues]
	(
	  @TargetId UNIQUEIDENTIFIER,
	  @PropertyName NVARCHAR(128),
	  @TopTenantInHierarchy UNIQUEIDENTIFIER = null
	)
AS
BEGIN
	
	DECLARE @PropertyValueTables TABLE 
    (
		tenantId UNIQUEIDENTIFIER NOT NULL,
		propertyName NVARCHAR(128) NOT NULL,
		propertyValue NVARCHAR(MAX) NULL
    )

	IF NOT EXISTS ( SELECT	IsActive FROM dbo.LKProperty WHERE PropertyName = @PropertyName)
	BEGIN 
		INSERT INTO @PropertyValueTables
		SELECT parent.TenantID, @PropertyName, NULL 
		FROM dbo.Tenant child 
		JOIN dbo.Tenant parent 
		ON child.OrgID.IsDescendantOf(parent.OrgID) = 1
		WHERE child.TenantID = @TargetId

		IF(dbo.IsTenant(@TargetId) = 0)
			INSERT INTO @PropertyValueTables
			SELECT StoreId, @PropertyName, NULL
			FROM Store
			WHERE TenantId = @TargetId

	END 
	ELSE
	BEGIN
		DECLARE @DefaultValue NVARCHAR(MAX)
		DECLARE @MaxHierarchyLevel HIERARCHYID

		SELECT @DefaultValue = DefaultValue 
		FROM dbo.LKProperty 
		WHERE PropertyName = @PropertyName

		IF @TopTenantInHierarchy IS NOT NULL
			SELECT @MaxHierarchyLevel = OrgID 
			FROM dbo.Tenant 
			WHERE TenantId = @TopTenantInHierarchy
		ELSE
			SELECT @MaxHierarchyLevel = Min(OrgID)
			FROM dbo.Tenant

		IF (SELECT	Ascend FROM dbo.LKProperty WHERE PropertyName = @PropertyName) = 1
		BEGIN
			
			DECLARE @TenantID UNIQUEIDENTIFIER
			
			IF(dbo.IsTenant(@TargetID) = 0)
				SELECT @TenantID = TenantId 
				FROM Store 
				WHERE StoreID = @TargetID
			ELSE
				SET @TenantID = @TargetId

			INSERT INTO @PropertyValueTables
			SELECT  parent.TenantID, 
					@PropertyName, 
					COALESCE(dbo.GetTenantPropertyValue(parent.TenantId, @PropertyName, null), @DefaultValue)
			FROM	dbo.Tenant child
					JOIN dbo.Tenant parent ON child.OrgID.IsDescendantOf(parent.OrgID) = 1
			WHERE	child.TenantID = @TenantID
					AND	parent.OrgId >= @MaxHierarchyLevel
					
			ORDER BY parent.OrgLevel DESC

			IF @TenantID <> @TargetID -- if @TargetID is store
			BEGIN
				INSERT INTO @PropertyValueTables
				SELECT StoreId, @PropertyName, COALESCE(dbo.GetTenantPropertyValue(StoreId, @PropertyName, null), @DefaultValue)
				FROM Store
				WHERE TenantID = @TenantID
			END

		END
		ELSE
		BEGIN
			DECLARE @PropertyValue NVARCHAR(MAX)

			SELECT @PropertyValue = PropertyValue 
			FROM dbo.TenantProperty 
			WHERE PropertyName = @PropertyName 
				  AND TenantID = @TargetId

			INSERT INTO @PropertyValueTables
			VALUES (@TargetId, @PropertyName, COALESCE(@PropertyValue, @DefaultValue))
			
		END
	END

	SELECT  tenantId, 
			propertyName, 
			propertyValue 
	FROM @PropertyValueTables
END


