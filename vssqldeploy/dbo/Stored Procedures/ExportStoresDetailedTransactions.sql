﻿-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 5/19/2017
-- Description:	Returns store transactions details for export
-- Update date: 5/22/2017 Alex Goroshko - added StoreNumber field to return
-- Update date: 12/08/2017 - added ability to show offers to partners
-- Update date: 10/23/2017 - Igor Gaidukov - use StoreTenantID instead of TenantID
-- Update date: 01/11/2018 - Igor Gaidukov - added OfferId as a parameter
-- Update date: 07/09/2018 - Alex Goroshko - OfferId param is NVARCHAR instead of UNIQUEIDENTIFIER
-- Update date: 10/22/2018 - Will White - fixed query to only match a single authorization
-- Update date: 04/19/2019 - Davydovich Raman - improve perfomance
-- Update date: 07/30/2019 - Davydovich Raman - Added filter parametres
-- =============================================
CREATE PROCEDURE [dbo].[ExportStoresDetailedTransactions]
	@TenantID UNIQUEIDENTIFIER NULL
	, @FromDate DATETIME2(7)
	, @ToDate DATETIME2(7)
	, @OfferId NVARCHAR(MAX) = NULL
	, @OfferName NVARCHAR(MAX) = NULL
	, @StoreName NVARCHAR(MAX) = NULL
	, @TimeOffset INT
AS
BEGIN
	SET NOCOUNT ON;

WITH RedeemedOffers AS
(
	SELECT b.[BasketID], bli.BasketLineItemID, TempOfferItems.OfferId, TempOfferItems.OfferTenantId,  TempOfferItems.OfferTitle, TempOfferItems.OfferDiscount, TempOfferItems.OfferAmount
	FROM [dbo].[Basket] b 
	JOIN [dbo].[BasketLineItem] bli ON bli.BasketID = b.BasketID
	OUTER APPLY
	(
		SELECT OfferId, OfferTenantId, OfferTitle, OfferDiscount, OfferAmount 
		FROM Reports.BasketLineItem_LoyaltyDescription ld
		WHERE ld.BasketLineItemID = bli.BasketLineItemID
	) TempOfferItems
	OUTER APPLY
	(
		SELECT pt.PartnerTenantId
		FROM Reports.FlatBasket_VisibleByTenants pt
		WHERE pt.TransactionID = b.BasketID AND	pt.PartnerTenantId = @TenantID

	) AS VisibleByTenants
	WHERE b.[POSDateTime] BETWEEN @FromDate AND @ToDate
		AND ( @OfferId IS NULL OR TempOfferItems.OfferId = @OfferId )
		AND ( @OfferName IS NULL OR TempOfferItems.OfferTitle = @OfferName )

		AND (@TenantID IS NULL
					OR
				( b.StoreTenantID = @TenantID ) 
					OR 
				( b.StoreTenantID != @TenantID AND b.AppTenantId = @TenantID)
					OR
				(b.StoreTenantID != @TenantID AND b.AppTenantId != @TenantID AND ( TempOfferItems.OfferTenantId = @TenantId OR VisibleByTenants.PartnerTenantId = @TenantId))
			)
), 
RedeemedOffersResult AS
(
		SELECT BasketID, BasketLineItemID,
			ISNULL(STUFF((SELECT(
				( SELECT (',' + OfferTitle) AS OfferTitle FROM RedeemedOffers ro2 WHERE ro2.BasketID = ro1.BasketID AND ro2.BasketLineItemID = ro1.BasketLineItemID
			FOR XML PATH, TYPE).value('.[1]', 'nvarchar(max)'))), 1, 1, ''), '') AS OfferNames
			,ISNULL(STUFF((SELECT(
				( SELECT (',' + convert(nvarchar(max), OfferAmount)) AS OfferAmount FROM RedeemedOffers  ro2 WHERE ro2.BasketID = ro1.BasketID AND ro2.BasketLineItemID = ro1.BasketLineItemID
			FOR XML PATH, TYPE).value('.[1]', 'nvarchar(max)'))), 1, 1, ''), '') AS OfferDiscounts
			FROM RedeemedOffers ro1
			GROUP BY BasketID, BasketLineItemID
)

SELECT 	b.BasketID AS TransactionID
		,lkbs.Name as "Status" 
		,b.UserID  
		,ui.EmailAddress AS UserEmail
		,b.POSOperatorID
		,b.POSTerminalID
		,b.POSTransactionID
		,b.PartnerTransactionID
		,store.Name AS StoreName
		,store.StoreNumber AS StoreNumber
		,store.StreetAddress as StoreAddress
		,store.City as StoreCity
		,store.StoreID
		,fs.MppaID AS StoreMppaID
		,DATEADD(mi, -(@TimeOffset), b.POSDateTime) AS DateTimeLocal
		,b.POSDateTime 
		,b.POSDateTimeLocal
		,bli.[ItemDescription]
		,FORMAT(ISNULL((bli.[LoyaltyRewardAmount]), 0), 'C', 'en-us') as LineItemTotalDiscount
		,FORMAT(bli.[ExtendedAmount], 'C', 'en-us') as LineItemTotal  
		,ror.OfferNames
		,ror.OfferDiscounts
		,FORMAT(b.TaxAmount, 'C', 'en-us') as TransactionTaxAmount
		,FORMAT(b.Total, 'C', 'en-us') as TotalTransactionAmount  
		,FORMAT(bpAuth.Amount, 'C', 'en-us') as PreAuthAmount

		,REPLACE(fp.FundingProviderName, 'Precidia', 'Merchant Link') AS PaymentProvider
		,lkpp.PaymentProcessorName AS CardType
		,b.BatchID
		,b.POSTillID as SettlementPeriodID
		,b.StacString as Stac
		,bp.BasketPaymentID AS TransactionPaymentID
		,bp.CreatedOn as PaymentDateTime
		,DATEADD(mi, -(@TimeOffset), bp.CreatedOn) AS PaymentDateTimeLocal

	  FROM [dbo].[BasketLineItem] bli
		JOIN RedeemedOffersResult ror ON ror.BasketID = bli.BasketID AND ror.BasketLineItemID = bli.BasketLineItemID
		JOIN [dbo].[Basket] b ON b.[BasketID] = bli.[BasketID]
		JOIN [dbo].[Store] store ON b.StoreID = store.StoreID  
		JOIN [dbo].[FlatStore] fs ON fs.StoreID = b.StoreID AND fs.TenantID = store.TenantID  
		JOIN [dbo].[LKBasketState] lkbs ON lkbs.BasketStateID = b.BasketStateID  
		JOIN [dbo].[BasketPayment] bp ON bp.BasketID = b.BasketID AND bp.PaymentCommand = 'Finalize'
		JOIN [dbo].[BasketPayment] bpAuth ON bpAuth.BasketPaymentID = (Select TOP 1 BasketPaymentID FROM [dbo].[BasketPayment] WHERE BasketID = b.BasketID AND PaymentCommand = 'Authorize' and ResponseResult = 'Approved')
		JOIN [Payment].[LKUserPaymentSource] lkups ON  lkups.UserPaymentSourceID = bp.UserPaymentSourceID   
		JOIN [Payment].[LKPaymentProcessor] lkpp ON  lkpp.[PaymentProcessorID] = lkups.[PaymentProcessorID] 
		JOIN [Payment].[FundingProvider] fp ON  fp.[FundingProviderID] = lkpp.[FundingProviderID]  
		JOIN [UserInfo] ui ON  b.[UserID] = ui.[UserID]

	  WHERE (b.[POSDateTime] BETWEEN @FromDate AND @ToDate) 
		AND (b.[BasketStateID] = 3)
		AND (@StoreName is null OR store.Name like '%' + @StoreName + '%')
	  ORDER BY bli.[CreatedOn] DESC
	  OPTION(RECOMPILE)
END

