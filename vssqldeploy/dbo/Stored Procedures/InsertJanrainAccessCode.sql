﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 2018-04-05
-- Description:	Insert janrain one time code for user
-- =============================================
CREATE PROCEDURE [dbo].[InsertJanrainAccessCode]
    @UserID UNIQUEIDENTIFIER,
    @AccessCode nvarchar(50),
	@ExpiresOn datetime2(7)
AS
BEGIN
	SET NOCOUNT ON;
	-- Started getting AccessCode collisions so need to check if one already exist and if it is expired or not active, just update it
	IF EXISTS (SELECT * FROM [dbo].[JanrainAccessCode] WHERE AccessCode = @AccessCode AND (ExpiresOn < GETUTCDATE() OR IsActive = 0))
	BEGIN
		UPDATE [dbo].[JanrainAccessCode] SET UserID = @UserID, ExpiresOn = @ExpiresOn, IsActive = 1 WHERE AccessCode = @AccessCode
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[JanrainAccessCode] ([AccessCode],[UserID],[ExpiresOn])
		VALUES (@AccessCode,@UserID,@ExpiresOn)
	END
END
