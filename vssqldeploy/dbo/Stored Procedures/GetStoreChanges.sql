﻿
CREATE PROCEDURE GetStoreChanges
AS
BEGIN
	DECLARE @CurrentVersion INT = (SELECT CHANGE_TRACKING_CURRENT_VERSION());
	DECLARE @BaseVersion INT = (SELECT [Version] FROM ChangeTrackerVersion WHERE [ChangeTracker] = 'Store')
	
	DECLARE @Error NVARCHAR(MAX);
	BEGIN TRY
		EXEC IUFlatStore @Version = @BaseVersion
	END TRY
	BEGIN CATCH
		SET @Error = ERROR_MESSAGE();	
	END CATCH
	
	UPDATE ChangeTrackerVersion SET [Version] = @CurrentVersion  WHERE [ChangeTracker] = 'Store'

	IF @Error IS NOT NULL
	BEGIN
		DECLARE @Messsage NVARCHAR(MAX) = 'BaseVersion: ' + CONVERT(NVARCHAR(MAX), @BaseVersion) + ', Error Message: ' + @Error
		RAISERROR(@Messsage, 11, 0)
	END
END
