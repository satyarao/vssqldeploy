﻿CREATE   PROCEDURE [dbo].[UpdateTermsAndConditions]
@TermsConditionsId UNIQUEIDENTIFIER,
@Version NVARCHAR(30), 
@DateTime DATETIME,
@Url NVARCHAR(1000), 
@Title NVARCHAR(100), 
@Preamble NVARCHAR(300),
@Sections TermsAndConditionsSectionType READONLY
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @generated_keys TABLE(Id INT)
	DECLARE @logDetailId INT
	DECLARE @serviceProviderId UNIQUEIDENTIFIER
	DECLARE @hostingType int

	--move T&C details into Log
	INSERT INTO dbo.TermsAndConditionsDetailsLog 
	(
		[TermsConditionsId], 
		[HostingType], 
		[Version], 
		[DateTime], 
		[ServiceProviderId],
		[Url], 
		[Title], 
		[Preamble],
		[ApplicationId],
		[TenantId]
	)
	OUTPUT INSERTED.DetailLogId INTO @generated_keys
	SELECT  [TermsConditionsId], 
			[HostingType], 
			[Version], 
			[DateTime], 
			[ServiceProviderId],
			[Url], 
			[Title], 
			[Preamble],
			[ApplicationId],
			[TenantId]
	FROM [dbo].[TermsAndConditionsDetails]
	WHERE TermsConditionsId = @TermsConditionsId

	
	SELECT @logDetailId = id 
	FROM @generated_keys

	--move section into log
	INSERT INTO dbo.TermsAndConditionsSectionsLog
	SELECT  [SectionOrder], 
			[SectionTitle], 
			[SectionText], 
			@logDetailId
	FROM dbo.TermsAndConditionSections 
	WHERE TermsConditionsId = @TermsConditionsId

	--delete previous sections
	DELETE FROM dbo.TermsAndConditionSections 
	WHERE TermsConditionsId = @TermsConditionsId

	--update T&C details
	UPDATE dbo.TermsAndConditionsDetails 
	SET Version = @Version, 
		DateTime = @DateTime, 
		[Url] = @Url, 
		Title = @Title, 
		Preamble = @Preamble
	WHERE TermsConditionsId = @TermsConditionsId
	
	INSERT INTO dbo.TermsAndConditionSections (SectionOrder, SectionTitle, SectionText, TermsConditionsId)
	SELECT  SectionOrder, 
			SectionTitle, 
			SectionText, 
			@TermsConditionsId
	FROM @Sections
	
	UPDATE dbo.AcceptanceStatuses 
	SET AcceptanceStatus = 3 -- 3 - not current status
	WHERE TermsConditionsId = @TermsConditionsId 
END