﻿
-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 08/31/2015
-- Update date: 03/23/2017
-- Description:	Get user transactions with paging
-- =============================================
CREATE PROCEDURE [dbo].[GetUserTransactions] 
	@UserID UNIQUEIDENTIFIER
	,@Offset INT
	,@Limit INT
	,@StartDate DATE = NULL
	,@EndDate DATE = NULL
	,@BasketStateId TINYINT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	WITH TempResult AS (SELECT  
							b.BasketID AS TransactionID
							,b.StoreId
							,b.POSTransactionID
							,b.POSDateTimeLocal
							,b.POSDateTime
							,store.Name AS StoreName
							,b.BasketStateID
							,lkbs.Name as BasketStateName
							,Total as TotalAmount
							,store.StreetAddress as Address
							, CASE WHEN b.POSOperatorID IN ('PZOutside', 'Outside') THEN CAST(0 AS bit)
								ELSE CAST(1 AS bit) END as InStorePurchase
					FROM   dbo.Basket b
							JOIN [dbo].[Store] store ON b.StoreID = store.StoreID
							JOIN dbo.LKBasketState lkbs ON lkbs.BasketStateID = b.BasketStateID
					WHERE  b.UserID = @UserID
							AND b.BasketStateID = ISNULL(@BasketStateId, b.BasketStateID)
							AND CONVERT(DATE, b.POSDateTime) BETWEEN ISNULL(@StartDate, CONVERT(DATE, '1/1/1753')) AND ISNULL(@EndDate, CONVERT(DATE, GETUTCDATE()))
					)
		,TempCount AS (SELECT COUNT(*) AS TotalRowCount FROM TempResult)
	
	SELECT 
		TransactionID, 
		StoreId,
		POSTransactionID, 
		POSDateTimeLocal, 
		POSDateTime, 
		StoreName, 
		BasketStateID, 
		BasketStateName, 
		TotalAmount, 
		TotalRowCount, 
		Address, 
		InStorePurchase
	FROM TempResult, TempCount
	ORDER BY POSDateTimeLocal DESC
	OFFSET @Offset ROWS 
	FETCH NEXT @Limit ROWS ONLY
END
