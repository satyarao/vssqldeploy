﻿-- =============================================
-- Update date: 01/30/2018 Igor Gaidukov -  save last 4 of the Credit Card number of the user
-- Update date: 23/09/2019 Raman Yauseyeu - duplicate check added
-- Update date: 10/18/2019 Matthew Liang - remove check for duplicate basket payment records since its unnecessary and incorrect implementation.
-- =============================================
CREATE PROC [dbo].[InsertBasketPayment]
	@BasketPayment BasketPaymentType READONLY
AS 

BEGIN
SET NOCOUNT ON
INSERT	INTO dbo.BasketPayment
		( BasketID
		, UserPaymentSourceID
		, PaymentCommand
		, Amount
		, AuthCode
		, ResponseResult
		, ResponseResultText
		, RefData
		, RecNum
		, CreatedOn
		, IsSiteLevelPayment
		)
		SELECT	BasketID
			  , UserPaymentSourceID
			  , PaymentCommand
			  , Amount
			  , AuthCode
			  , ResponseResult
			  , ResponseResultText
			  , RefData
			  , RecNum
			  , CASE WHEN CreatedOn = '0001-01-01 00:00:00.0000000' THEN GETUTCDATE()
					 ELSE CreatedOn
				END
			  , IsSiteLevelPayment
		FROM	@BasketPayment
END

