﻿
-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 10/31/2016
-- Update date: 11/11/2016 - removed installation settings
-- Update date: 12/29/2016 - removed installation settings
-- Updated: Igor Gaidukov 07/25/2017 use AllowInsidePayment and AllowOutsidePayment instead of IsMobilePaymentsEnabled
-- Updated: 10/03/2017 Igor Gaidukov - deleted DisplayOnMobile, AllowInsidePaymentn AllowOutsidePayment settings for partners (partners does not override this settings)
-- Description:	Update admin settings of store
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAdminStoresSettings]
	@TenantID UNIQUEIDENTIFIER,
	@StoreID UNIQUEIDENTIFIER,
	@SiteServiceID NVARCHAR(50),
	@ActiveAlertsMaintenanceMode BIT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OwnerID UNIQUEIDENTIFIER; 
	
	SET @OwnerID = (SELECT TenantID FROM dbo.Store WHERE StoreID = @StoreID)

	IF (@OwnerID = @TenantID)
		BEGIN
			UPDATE [dbo].[Store]
			SET [SiteServiceID] = @SiteServiceID,
				[ActiveAlertsMaintenanceMode] = @ActiveAlertsMaintenanceMode,
				[UpdatedOn] = GETUTCDATE(),
				[UpdatedBy] = 'SP UpdateAdminStoresSettings'
			WHERE StoreID = @StoreID
		END
	ELSE 
		BEGIN
			DECLARE @OwnersMaintenanceMode BIT; 
			SET @OwnersMaintenanceMode = (SELECT ActiveAlertsMaintenanceMode FROM dbo.Store WHERE StoreID = @StoreID)

			DECLARE @StoreOverrides table(
				TenantID UNIQUEIDENTIFIER,  
				StoreNumber NVARCHAR(64),
				Name NVARCHAR(64),  
				StationImageID INT,  
				ActiveAlertsMaintenanceMode BIT,  
				IsSiteBillable     BIT ,  
				ActiveAlertsIgnored BIT
			);
			
			INSERT INTO @StoreOverrides (TenantID, StoreNumber, Name, StationImageID, ActiveAlertsMaintenanceMode, IsSiteBillable, ActiveAlertsIgnored)
			SELECT ConsumerTenantID, StoreNumber, Name, StationImageID, ActiveAlertsMaintenanceMode, IsSiteBillable, ActiveAlertsIgnored
			FROM OPENJSON((SELECT StoreTenants FROM dbo.Store WHERE StoreID = @StoreID)) 
				WITH (  
								ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID',   
								StoreNumber NVARCHAR(64) N'$.StoreNumber',  
								Name NVARCHAR(64) N'$.Name',  
								StationImageID INT N'$.StationImageID',  
								ActiveAlertsMaintenanceMode BIT N'$.ActiveAlertsMaintenanceMode',  
								IsSiteBillable     BIT N'$.IsSiteBillable',  
								ActiveAlertsIgnored BIT N'$.ActiveAlertsIgnored'
						      )

			UPDATE @StoreOverrides
				SET [ActiveAlertsMaintenanceMode] = @ActiveAlertsMaintenanceMode
			WHERE TenantID = @TenantID

			UPDATE [dbo].[Store]
			SET [SiteServiceID] = @SiteServiceID,
				[StoreTenants] = (SELECT * FROM @StoreOverrides FOR JSON PATH) ,
				[UpdatedOn] = GETUTCDATE(),
				[UpdatedBy] = 'SP UpdateMobilePaymentsState'
			WHERE StoreID = @StoreID
		END
END


