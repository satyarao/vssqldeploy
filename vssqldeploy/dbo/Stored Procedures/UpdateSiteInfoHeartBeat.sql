﻿CREATE PROC [dbo].[UpdateSiteInfoHeartBeat]
	@StoreID UNIQUEIDENTIFIER
  , @HeartBeatDateTime DATETIME2
  , @HeartBeatStatus NVARCHAR(50)
AS /*
EXEC dbo.UpdateSiteInfoHeartBeat
	@StoreID = 'A46DCB93-0F57-4432-A0AC-15182D774E0F'
  , @HeartBeatDateTime = '2014-11-25 23:12:39'
  , @HeartBeatStatus = N'Stopped'
*/
UPDATE	dbo.SiteInfo
SET		HeartBeatDateTime = @HeartBeatDateTime
	  , HeartBeatStatus = @HeartBeatStatus
WHERE	StoreID = @StoreID
