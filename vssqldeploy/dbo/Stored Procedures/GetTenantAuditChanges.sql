﻿
CREATE PROCEDURE GetTenantAuditChanges
AS
BEGIN
	DECLARE @CurrentVersion INT = (SELECT CHANGE_TRACKING_CURRENT_VERSION());
	DECLARE @BaseVersion INT = (SELECT [Version] FROM ChangeTrackerVersion WHERE [ChangeTracker] = 'TenantAudit')
	
	SELECT CONVERT(nvarchar(50), CT.[AuditID]) AS [AuditID], CONVERT(nvarchar(50), [UserID]) AS[UserID], CONVERT(nvarchar(50), [TenantID]) AS[TenantID],[IPAddress], [URL], [Category], [RequestType], [StatusCode], [BodyJson], [Received], [Sent], [ModelsJson], [ModelsId]
	FROM [dbo].[TenantAudit] AS ta RIGHT OUTER JOIN CHANGETABLE(CHANGES [dbo].[TenantAudit], @BaseVersion) AS CT ON ta.AuditID = CT.AuditID AND (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U')

	UPDATE ChangeTrackerVersion SET [Version] = @CurrentVersion  WHERE [ChangeTracker] ='TenantAudit'
END
