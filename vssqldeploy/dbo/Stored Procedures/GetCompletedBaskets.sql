﻿
	CREATE PROCEDURE [dbo].[GetCompletedBaskets]
		@TenantID UNIQUEIDENTIFIER
	, @FromDate DATE = NULL
	, @ToDate DATE = NULL
	AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	SELECT	b.StoreID
		, POSTransactionID
		, POSDateTime
		, Total
		, s.Name
		, s.StreetAddress
		, s.City
		, s.StateCode
		, s.ZipCode AS PostalCode
		, CASE WHEN b.POSOperatorID <> 'PZOutside' THEN CAST(1 AS BIT)
				ELSE CAST(0 AS BIT)
			END AS InStorePurchase
	FROM	dbo.Basket b
			JOIN dbo.Store s ON s.StoreID = b.StoreID
	WHERE	s.TenantID = @TenantID
			AND b.CreatedOn >= ISNULL(@FromDate, CONVERT(DATE, GETDATE() - 1))
			AND b.CreatedOn <= ISNULL(DATEADD("d", 1, @ToDate), CONVERT(DATE, GETDATE()))
			AND BasketStateID = 3
	--ORDER BY b.StoreID
	--	  , POSDateTime DESC

