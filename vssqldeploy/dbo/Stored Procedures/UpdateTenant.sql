﻿-- =============================================
-- Udpate date: 7/11/2018 Darya Batalava - added ability to update distribution channels
-- Udpate date: 11/29/2018 Darya Batalava - added ability to update logo
-- =============================================
CREATE PROCEDURE [dbo].[UpdateTenant]
    @TenantID UNIQUEIDENTIFIER
  , @TenantName VARCHAR(255) = NULL
  , @TenantTypeID INT = NULL
  , @IsActive BIT
  , @ActiveAlertsMaintenanceMode BIT
  , @DistributionChannels NVARCHAR(MAX) = NULL
  , @HeaderLogo NVARCHAR(MAX) = NULL
  , @FooterLogo NVARCHAR(MAX) = NULL
AS 

	SET NOCOUNT ON;

	UPDATE dbo.Tenant
	SET	 Name = ISNULL(@TenantName, Name)
		, TenantTypeID = ISNULL(@TenantTypeID, TenantTypeID)
		, IsActive = @IsActive
		, UpdatedBy = 'InsertTenant'
		, UpdatedOn = GETUTCDATE()
		, ActiveAlertsMaintenanceMode = @ActiveAlertsMaintenanceMode
		, DistributionChannels = @DistributionChannels
		, HeaderLogo = @HeaderLogo
		, FooterLogo = @FooterLogo
	WHERE TenantID = @TenantID
	
	





