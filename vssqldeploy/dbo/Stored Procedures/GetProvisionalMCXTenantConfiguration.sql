﻿CREATE PROCEDURE [dbo].[GetProvisionalMCXTenantConfiguration] 
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT @TenantID as TenantID,
		dbo.GetTenantPropertyValue(@TenantID, 'mcx_fuel_api_prepender', 0) as MCX_FuelAPI_Prepender,
		dbo.GetTenantPropertyValue(@TenantID, 'mcx_fuel_api_uri', 0) as MCX_FuelAPI_Uri,
		dbo.GetTenantPropertyValue(@TenantID, 'mcx_pos_api_prepender', 0) as MCX_POSAPI_Prepender,
		dbo.GetTenantPropertyValue(@TenantID, 'mcx_pos_api_uri', 0) as MCX_POSAPI_Uri,
		dbo.GetTenantPropertyValue(@TenantID, 'mcx_max_try_count', 0) as MaxTryCount
END
