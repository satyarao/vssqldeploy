﻿
CREATE PROCEDURE [dbo].[GetParentTenantID]
	@TenantID UNIQUEIDENTIFIER
  , @AllParents BIT = 0
AS
BEGIN
	DECLARE	@child HIERARCHYID

	SELECT	@child = OrgID
	FROM	dbo.Tenant
	WHERE	TenantID = @TenantID

	IF @AllParents = 0
		BEGIN
			SELECT TOP 1
					Tenantid
				  , OrgLevel
			FROM	dbo.Tenant
			WHERE	@child.IsDescendantOf(OrgID) = 1
					AND Tenantid <> @TenantID
			ORDER BY OrgLevel DESC
		END
	ELSE
		BEGIN
			SELECT	Tenantid
				  , OrgLevel
			FROM	dbo.Tenant
			WHERE	@child.IsDescendantOf(OrgID) = 1
					AND Tenantid <> @TenantID
			ORDER BY OrgLevel --desc
		END
END
