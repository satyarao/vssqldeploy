﻿
-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 10/31/2016
-- Update date: 12/29/2016
-- Updated: Igor Gaidukov 07/25/2017 use AllowInsidePayment and AllowOutsidePayment instead of IsMobilePaymentsEnabled
-- Updated: Igor Gaidukov 10/03/2017 deleted DisplayOnMobile, AllowInsidePaymentn AllowOutsidePayment settings for partners (partners does not override this settings)
-- Updated: Andrei Ramanovich 02/19/2018 add PaymentsOffline
-- Description:	Update mobile payments state
-- =============================================
CREATE PROCEDURE [dbo].[UpdateMobilePaymentsState]
	@StoreID UNIQUEIDENTIFIER,
	@TenantID UNIQUEIDENTIFIER,
	@AllowInsidePayment BIT,
	@AllowOutsidePayment BIT,
	@PaymentsOffline BIT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OwnerID UNIQUEIDENTIFIER; 
	
	SET @OwnerID = (SELECT TenantID FROM dbo.Store WHERE StoreID = @StoreID)

	IF (@OwnerID = @TenantID)
		BEGIN
			UPDATE [dbo].[Store]
			SET [AllowInsidePayment] = @AllowInsidePayment,
				[AllowOutsidePayment] = @AllowOutsidePayment,
				[PaymentsOffline] = ISNULL(@PaymentsOffline, [PaymentsOffline]),
				[UpdatedOn] = GETUTCDATE(),
				[UpdatedBy] = 'SP UpdateMobilePaymentsState'
			WHERE StoreID = @StoreID
		END
END
