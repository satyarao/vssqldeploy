﻿
-- =============================================
-- Author:		JAMY RYALS
-- Create date: 2/24/2016
-- Description:	Deletes Tenant Connection info if it matches
-- =============================================
CREATE PROCEDURE [dbo].[DeleteTenantConnectionInfoExclusive]
	@TenantID uniqueidentifier, 
	@EndpointAddress nvarchar(250),
	@ChannelType nvarchar(250),
	@ClientType nvarchar(250) 
AS
BEGIN	
	SET NOCOUNT ON;

	DECLARE @CURRENT_EndpointAddress nvarchar(250) = dbo.GetTenantPropertyExclusiveValue(@TenantID, 'EndpointAddress')
	DECLARE @CURRENT_ChannelType nvarchar(250) = dbo.GetTenantPropertyExclusiveValue(@TenantID, 'ChannelType')
	DECLARE @CURRENT_ClientType nvarchar(250) = dbo.GetTenantPropertyExclusiveValue(@TenantID, 'ClientType')
	
	IF (@CURRENT_EndpointAddress = @EndpointAddress AND
		@CURRENT_ChannelType = @ChannelType AND
		@CURRENT_ClientType = @ClientType)
	BEGIN
		DELETE FROM [dbo].[TenantProperty]
		WHERE 
			TenantID = @TenantID
			AND (
				PropertyName = 'EndpointAddress' OR
				PropertyName = 'ChannelType' OR
				PropertyName = 'ClientType'
			)
	END
END

