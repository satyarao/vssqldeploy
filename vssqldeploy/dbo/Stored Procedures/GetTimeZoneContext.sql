﻿CREATE PROCEDURE [dbo].[GetTimeZoneContext]
	@StoreID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		ISNULL(tz.Identifier, 'UTC') as TimeZoneIdentifier,
		ISNULL(tz.AbbreviatedDisplayName, 'UTC') as TimeZoneAbbreviation,
		ISNULL(tz.BaseUtcOffsetSec, 0) as TimeZoneOffset
	FROM Common.LKTimezone tz
	JOIN Store s on s.TimeZoneID = tz.TimeZoneID
	WHERE s.StoreID = @StoreID
END
