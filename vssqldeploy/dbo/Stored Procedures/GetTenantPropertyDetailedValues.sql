﻿-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 4/29/2016
-- Update date: 01/18/2019 - PLAT-4745 App Tenant User Verification Setting (Extend @PropertyName according to new size of tied column)
-- Update: Darya Batalava 10/08/2019 - added @SortingColumn and @SortingDirection
-- Description:	Returns all tenant properties values 
-- =============================================
CREATE PROCEDURE [dbo].[GetTenantPropertyDetailedValues] 
	@TargetIDs ListOfGuid READONLY, 
	@PropertyName NVARCHAR(128),
	@SortingColumn NVARCHAR(128) NULL,
	@SortingDirection NVARCHAR(4) NULL
AS
	BEGIN
		SET NOCOUNT ON;
		SELECT * FROM(
		SELECT t.TenantID AS TargetID, Name , 'tenant' AS TargetType, TenantPropertyInfo.[PropertyName], TenantPropertyInfo.[PropertyValue], TenantPropertyInfo.[DefaultTenantID] AS ParentTenantID, TenantPropertyInfo.[DefaultTenantName] AS ParentTenantName, TenantPropertyInfo.[DefaultPropertyValue] AS ParentTenantPropertyValue
		FROM Tenant t
		CROSS APPLY dbo.GetTenantPropertyInfo(t.TenantID, @PropertyName, DEFAULT) as TenantPropertyInfo
		WHERE IsActive = 1 AND ( NOT EXISTS (SELECT * FROM @TargetIDs) OR t.TenantID IN (SELECT ID FROM @TargetIDs)) 
		
		UNION ALL
		
		SELECT tp.TenantID AS TargetID, s.Name, 'store' AS TargetType, TenantPropertyInfo.[PropertyName], TenantPropertyInfo.[PropertyValue], TenantPropertyInfo.[DefaultTenantID] AS ParentTenantID,
		TenantPropertyInfo.[DefaultTenantName] AS ParentTenantName, TenantPropertyInfo.[DefaultPropertyValue] AS ParentTenantPropertyValue
		FROM TenantProperty tp
		JOIN Store s ON s.StoreID = tp.TenantID
		CROSS APPLY dbo.GetTenantPropertyInfo(tp.TenantID, @PropertyName, DEFAULT) as TenantPropertyInfo
		WHERE tp.TenantID NOT IN (SELECT t.TenantID FROM Tenant t) AND tp.PropertyName = @PropertyName AND (NOT EXISTS (SELECT * FROM @TargetIDs) OR s.TenantID IN (SELECT ID FROM @TargetIDs))  
		) t
	ORDER BY
	CASE WHEN @SortingColumn = 'targetName' AND @SortingDirection = 'asc' THEN t.Name END ASC,
	CASE WHEN @SortingColumn = 'targetName' AND @SortingDirection = 'desc' THEN t.Name END DESC,
	CASE WHEN @SortingColumn = 'value' AND @SortingDirection = 'asc' THEN t.PropertyValue END ASC,
	CASE WHEN @SortingColumn = 'value' AND @SortingDirection = 'desc' THEN t.PropertyValue END DESC
	END
