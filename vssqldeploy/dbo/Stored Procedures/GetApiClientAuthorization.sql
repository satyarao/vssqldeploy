﻿-- =============================================
-- Author:		Daniel DiPaolo
-- Create date: 2015/04/29
-- Description:	Inserts/updates API Client info record
-- =============================================
CREATE PROCEDURE [dbo].[GetApiClientAuthorization] 
	@ApiClientID uniqueidentifier
	,@HostName nvarchar(250)
	,@ApiVersion int
	,@MacAddress nvarchar(150)

AS
BEGIN
	SET NOCOUNT ON;

DECLARE @ConfigID INT

DECLARE @OldHostName NVARCHAR(250)
DECLARE @OldApiVersion INT
DECLARE @OldClientIsRunning BIT

DECLARE @HostNameChanged BIT
DECLARE @ApiVersionChanged BIT

DECLARE @Response BIT
DECLARE @ResponseReason NVARCHAR(250)

IF NOT EXISTS (	SELECT ApiClientID
				FROM dbo.ApiClientInfo
				WHERE ApiClientID = @ApiClientID )
	BEGIN
		-- no record for that API Client ID = new install, client has never been online
		INSERT INTO dbo.ApiClientInfo
			(ApiClientID
			,HostName
			,ApiVersion
			,MacAddress
			,InstallDateTime
			,IsRunning)
		VALUES
			(@ApiClientID
			,@HostName
			,@ApiVersion
			,@MacAddress
			,GETUTCDATE()
			,1)

		SET @Response = 1
		SET @ResponseReason = 'New Client'
	END
ELSE
	BEGIN
		-- record exists, API Client has been authed at some point
		
		-- first, see if it's the same machine it was last running on
		-- regardless of the isrunning state (in the event of a host failure
		-- where the host doesn't deauth the old client, we want to allow
		-- them to restart anyway)
		SELECT @ConfigID = ID
			  ,@OldHostName = HostName
			  ,@OldApiVersion = ApiVersion
			  ,@OldClientIsRunning = IsRunning
		FROM
			dbo.ApiClientInfo
		WHERE
			ApiClientID = @ApiClientID
			AND EXISTS ( SELECT s.ListValue
						 FROM	Common.FnSplit(@MacAddress, '|', 0) s
								JOIN Common.FnSplit(MacAddress, '|', 0) d on d.ListValue = s.ListValue )
		
		-- we found the old client info record, update it
		IF @ConfigID IS NOT NULL
			BEGIN
				IF @OldHostName <> @HostName
					SET @HostNameChanged = 1
				IF @OldApiVersion <> @ApiVersion
					SET @ApiVersionChanged = 1
				

				UPDATE dbo.ApiClientInfo
				SET
					RestartDateTime = GETUTCDATE()
					, HostName = @HostName
					, ApiVersion = @ApiVersion
					, MacAddress = @MacAddress
					, IsRunning = 1  -- need to set in case this client de-auth'ed but is being reused on same host
				WHERE
					ID = @ConfigID

				SET @Response = 1
				SET @ResponseReason = 
					CASE WHEN @OldClientIsRunning = 1 THEN
						'Unexpected restart on same host'
					ELSE 
						'Normal restart'
					END +
					CASE WHEN @HostNameChanged = 1 THEN
						', HostName Change: ' + @OldHostName + ' => ' + @HostName
					ELSE
						''
					END +
					CASE WHEN @ApiVersionChanged = 1 THEN
						', API Version Change: ' + CONVERT(nvarchar, @OldApiVersion) + ' => ' + CONVERT(nvarchar, @ApiVersion)
					ELSE
						''
					END
					
			END
		ELSE
			-- no client record that matches the current host was found
			BEGIN
			-- look for ANY record for this client ID and if none are running, 
			-- update record as if it's a new client
			IF NOT EXISTS (SELECT 1 
							FROM
								dbo.ApiClientInfo
							WHERE
								ApiClientID = @ApiClientID
								AND IsRunning = 1)
				BEGIN
					UPDATE dbo.ApiClientInfo
					SET 
						ApiClientID = @ApiClientID
						,HostName = @HostName
						,ApiVersion = @ApiVersion
						,MacAddress = @MacAddress
						,InstallDateTime = GETUTCDATE()
						,RestartDateTime = null
						,IsRunning = 1
					WHERE
						ApiClientID = @ApiClientID
					SET @Response = 1
					SET @ResponseReason = 'Host change, no old clients running on previous hosts'
				END
			-- otherwise, fail
			ELSE
				BEGIN
					SET @Response = 0
					SET @ResponseReason = 'ApiClient already running on different host'
				END
			END
	END
	
SELECT	@Response AS Success
	  , @ResponseReason AS ResultText
END
