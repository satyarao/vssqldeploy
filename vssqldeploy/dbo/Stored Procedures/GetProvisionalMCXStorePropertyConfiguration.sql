﻿CREATE PROCEDURE [dbo].[GetProvisionalMCXStorePropertyConfiguration] 
	@StoreID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		@StoreID										as StoreID,
		--MCX_FuelAPI_Password
		MCX_FuelAPI_Password.[PropertyValue]			as MCX_FuelAPI_Password,
		--MCX_FuelAPI_Password.[DefaultTenantID]			as MCX_FuelAPI_Password_DefaultTenantID,
		--MCX_FuelAPI_Password.[DefaultTenantName]		as MCX_FuelAPI_Password_DefaultTenantName,
		--MCX_FuelAPI_Password.[DefaultPropertyValue]		as MCX_FuelAPI_Password_DefaultValue,

		--MCX_FuelAPI_Prepender
		MCX_FuelAPI_Prepender.[PropertyValue]			as MCX_FuelAPI_Prepender,
		MCX_FuelAPI_Prepender.[DefaultTenantID]			as MCX_FuelAPI_Prepender_DefaultTenantID,
		MCX_FuelAPI_Prepender.[DefaultTenantName]		as MCX_FuelAPI_Prepender_DefaultTenantName,
		MCX_FuelAPI_Prepender.[DefaultPropertyValue]	as MCX_FuelAPI_Prepender_DefaultValue,

		--MCX_FuelAPI_Uri
		MCX_FuelAPI_Uri.[PropertyValue]					as MCX_FuelAPI_Uri,
		MCX_FuelAPI_Uri.[DefaultTenantID]				as MCX_FuelAPI_Uri_DefaultTenantID,
		MCX_FuelAPI_Uri.[DefaultTenantName]				as MCX_FuelAPI_Uri_DefaultTenantName,
		MCX_FuelAPI_Uri.[DefaultPropertyValue]			as MCX_FuelAPI_Uri_DefaultValue,

		--MCX_FuelAPI_UserName
		MCX_FuelAPI_UserName.[PropertyValue]			as MCX_FuelAPI_UserName,
		--MCX_FuelAPI_UserName.[DefaultTenantID]			as MCX_FuelAPI_UserName_DefaultTenantID,
		--MCX_FuelAPI_UserName.[DefaultTenantName]		as MCX_FuelAPI_UserName_DefaultTenantName,
		--MCX_FuelAPI_UserName.[DefaultPropertyValue]		as MCX_FuelAPI_UserName_DefaultValue,

		--MCX_POSAPI_Password
		MCX_POSAPI_Password.[PropertyValue]				as MCX_POSAPI_Password,
		--MCX_POSAPI_Password.[DefaultTenantID]			as MCX_POSAPI_Password_DefaultTenantID,
		--MCX_POSAPI_Password.[DefaultTenantName]			as MCX_POSAPI_Password_DefaultTenantName,
		--MCX_POSAPI_Password.[DefaultPropertyValue]		as MCX_POSAPI_Password_DefaultValue,

		--MCX_POSAPI_Prepender
		MCX_POSAPI_Prepender.[PropertyValue]			as MCX_POSAPI_Prepender,
		MCX_POSAPI_Prepender.[DefaultTenantID]			as MCX_POSAPI_Prepender_DefaultTenantID,
		MCX_POSAPI_Prepender.[DefaultTenantName]		as MCX_POSAPI_Prepender_DefaultTenantName,
		MCX_POSAPI_Prepender.[DefaultPropertyValue]		as MCX_POSAPI_Prepender_DefaultValue,

		--MCX_POSAPI_Uri
		MCX_POSAPI_Uri.[PropertyValue]					as MCX_POSAPI_Uri,
		MCX_POSAPI_Uri.[DefaultTenantID]				as MCX_POSAPI_Uri_DefaultTenantID,
		MCX_POSAPI_Uri.[DefaultTenantName]				as MCX_POSAPI_Uri_DefaultTenantName,
		MCX_POSAPI_Uri.[DefaultPropertyValue]			as MCX_POSAPI_Uri_DefaultValue,

		--MCX_POSAPI_UserName
		MCX_POSAPI_UserName.[PropertyValue]				as MCX_POSAPI_UserName,
		--MCX_POSAPI_UserName.[DefaultTenantID]			as MCX_POSAPI_UserName_DefaultTenantID,
		--MCX_POSAPI_UserName.[DefaultTenantName]			as MCX_POSAPI_UserName_DefaultTenantName,
		--MCX_POSAPI_UserName.[DefaultPropertyValue]		as MCX_POSAPI_UserName_DefaultValue,

		--MaxTryCount
		MaxTryCount.[PropertyValue]						as MaxTryCount,
		MaxTryCount.[DefaultTenantID]					as MaxTryCount_DefaultTenantID,
		MaxTryCount.[DefaultTenantName]					as MaxTryCount_DefaultTenantName,
		MaxTryCount.[DefaultPropertyValue]				as MaxTryCount_DefaultValue
	From dbo.Store store
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_fuel_api_password', 0) as MCX_FuelAPI_Password
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_fuel_api_prepender', 1) as MCX_FuelAPI_Prepender
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_fuel_api_uri', 1) as MCX_FuelAPI_Uri
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_fuel_api_user_name', 0) as MCX_FuelAPI_UserName
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_pos_api_password', 0) as MCX_POSAPI_Password
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_pos_api_prepender', 1) as MCX_POSAPI_Prepender
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_pos_api_uri', 1) as MCX_POSAPI_Uri
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_pos_api_user_name', 0) as MCX_POSAPI_UserName
	CROSS APPLY dbo.GetTenantPropertyInfo(@StoreID, 'mcx_max_try_count', 1) as MaxTryCount
	WHERE store.StoreID = @StoreID
END
