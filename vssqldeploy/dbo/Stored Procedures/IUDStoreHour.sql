﻿
CREATE PROC [dbo].[IUDStoreHour]
	@StoreID UNIQUEIDENTIFIER
  , @SundayOpen TIME(0) = NULL
  , @SundayClose TIME(0) = NULL
  , @MondayOpen TIME(0) = NULL
  , @MondayClose TIME(0) = NULL
  , @TuesdayOpen TIME(0) = NULL
  , @TuesdayClose TIME(0) = NULL
  , @WednesdayOpen TIME(0) = NULL
  , @WednesdayClose TIME(0) = NULL
  , @ThursdayOpen TIME(0) = NULL
  , @ThursdayClose TIME(0) = NULL
  , @FridayOpen TIME(0) = NULL
  , @FridayClose TIME(0) = NULL
  , @SaturdayOpen TIME(0) = NULL
  , @SaturdayClose TIME(0) = NULL
  , @IsActive BIT = 1
AS /*
DECLARE @open TIME(0)
DECLARE @close TIME(0)
SET @open = '10:00:00'
SET @close = '10:00:00'
SELECT * FROM dbo.HoursOfOperation WHERE StoreID = '00000000-0000-0000-0000-000000000000'
EXEC dbo.IUDStoreHour @StoreID = '00000000-0000-0000-0000-000000000000', -- uniqueidentifier
	@SundayOpen = @open, 
	@SundayClose = @close,
	@MondayOpen = @open,
	@MondayClose = @close,
	@TuesdayOpen = @open,
	@TuesdayClose = @close,
	@WednesdayOpen = @open,
	@WednesdayClose = @close,
	@ThursdayOpen = @open,
	@ThursdayClose = @close,
	@FridayOpen = @open,
	@FridayClose = @close,
	@SaturdayOpen = @open,
	@SaturdayClose = @close,
	@IsActive = 1
SELECT * FROM dbo.HoursOfOperation WHERE StoreID = '00000000-0000-0000-0000-000000000000'
*/
BEGIN
	SET NOCOUNT ON

	MERGE dbo.HoursOfOperation AS target
	USING
		( SELECT	@StoreID
				  , @SundayOpen
				  , @SundayClose
				  , @MondayOpen
				  , @MondayClose
				  , @TuesdayOpen
				  , @TuesdayClose
				  , @WednesdayOpen
				  , @WednesdayClose
				  , @ThursdayOpen
				  , @ThursdayClose
				  , @FridayOpen
				  , @FridayClose
				  , @SaturdayOpen
				  , @SaturdayClose
				  , @IsActive
		) AS source ( StoreID, SundayOpen, SundayClose, MondayOpen, MondayClose, TuesdayOpen, TuesdayClose,
					  WednesdayOpen, WednesdayClose, ThursdayOpen, ThursdayClose, FridayOpen, FridayClose, SaturdayOpen,
					  SaturdayClose, IsActive )
	ON ( target.StoreID = source.StoreID )
	WHEN MATCHED THEN
		UPDATE SET
			   SundayOpen = source.SundayOpen
			 , SundayClose = source.SundayClose
			 , MondayOpen = source.MondayOpen
			 , MondayClose = source.MondayClose
			 , TuesdayOpen = source.TuesdayOpen
			 , TuesdayClose = source.TuesdayClose
			 , WednesdayOpen = source.WednesdayOpen
			 , WednesdayClose = source.WednesdayClose
			 , ThursdayOpen = source.ThursdayOpen
			 , ThursdayClose = source.ThursdayClose
			 , FridayOpen = source.FridayOpen
			 , FridayClose = source.FridayClose
			 , SaturdayOpen = source.SaturdayOpen
			 , SaturdayClose = source.SaturdayClose
			 , UpdatedBy = N'SP'
			 , UpdatedOn = GETUTCDATE()
	WHEN NOT MATCHED THEN
		INSERT ( StoreID
			   , SundayOpen
			   , SundayClose
			   , MondayOpen
			   , MondayClose
			   , TuesdayOpen
			   , TuesdayClose
			   , WednesdayOpen
			   , WednesdayClose
			   , ThursdayOpen
			   , ThursdayClose
			   , FridayOpen
			   , FridayClose
			   , SaturdayOpen
			   , SaturdayClose
			   , IsActive
			   , CreatedBy 
			   )
		VALUES ( source.StoreID
			   , source.SundayOpen
			   , source.SundayClose
			   , source.MondayOpen
			   , source.MondayClose
			   , source.TuesdayOpen
			   , source.TuesdayClose
			   , source.WednesdayOpen
			   , source.WednesdayClose
			   , source.ThursdayOpen
			   , source.ThursdayClose
			   , source.FridayOpen
			   , source.FridayClose
			   , source.SaturdayOpen
			   , source.SaturdayClose
			   , source.IsActive
			   , N'SP'
			   );
END

