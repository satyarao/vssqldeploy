﻿-- =============================================
-- Author:		Sergei Buday
-- Updated date: 01/18/2019
-- Description:	PLAT-4745 App Tenant User Verification Setting (Extend @PropertyName according to new size of tied column)
-- =============================================
CREATE PROCEDURE [dbo].[IUDTenantProperty]
	@TenantID UNIQUEIDENTIFIER = NULL,
	@PropertyName NVARCHAR(128),
	@PropertyValue NVARCHAR(250),
	@Delete bit = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @Delete = 1
	BEGIN
		DELETE FROM [TenantProperty] WHERE [TenantID] = COALESCE(@TenantID, TenantID) AND [PropertyName] = @PropertyName
	END
	ELSE IF @TenantID IS NOT NULL
	BEGIN
		IF EXISTS( SELECT * from [TenantProperty] WHERE [TenantID] = @TenantID AND [PropertyName] = @PropertyName )
		  UPDATE [TenantProperty] SET [PropertyValue] = @PropertyValue WHERE [TenantID] = @TenantID AND [PropertyName] = @PropertyName
		ELSE
		  INSERT INTO [TenantProperty] VALUES(@TenantID,@PropertyName,@PropertyValue)
	END
END

