﻿
CREATE PROCEDURE [dbo].[GetStoreTxnTotals]
	@StoreID UNIQUEIDENTIFIER,
	@BatchId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		COUNT(b.BasketID)			as OverallTotalCount
		,ISNULL(SUM(b.Total), 0)	as OverallTotalAmount
	FROM dbo.Basket b 
	WHERE b.BatchId = @BatchId
	AND b.StoreId = @StoreID
	AND b.BasketStateID = 3 -- Only completed transactions

END
