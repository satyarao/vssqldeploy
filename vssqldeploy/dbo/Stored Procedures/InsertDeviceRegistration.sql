﻿CREATE PROC [dbo].[InsertDeviceRegistration]
	@UserID UNIQUEIDENTIFIER
  , @InstallationID NVARCHAR(128)
  , @VerificationID INT
  , @CreatedOn DATETIME2
  , @IsActive BIT = 1
AS /*

DECLARE @UserID UNIQUEIDENTIFIER
SELECT @UserID = UserID FROM dbo.UserInfo

DECLARE @VerificationID INT
SELECT @VerificationID = VerificationID FROM dbo.Verification WHERE UserID = @UserID

SELECT * FROM dbo.DeviceRegistration WHERE UserID = @UserID

EXEC dbo.InsertDeviceRegistration @UserID = @UserID, -- uniqueidentifier
	@InstallationID = N'CB8F6F433DAE5B2A40D416BA8E582DDD2BE253A7373430343036', -- nvarchar(128)
	@VerificationID = @VerificationID, -- int
	@CreatedOn = '2014-11-12 17:45:15'

SELECT * FROM dbo.DeviceRegistration WHERE UserID = @UserID

*/
SET NOCOUNT ON
INSERT	INTO dbo.DeviceRegistration
		(
		 UserID
	   , InstallationID
	   , VerificationID
--	   , PhoneNumber
--	   , EmailAddress
	   , IsActive
--	   , CreatedBy
	   , CreatedOn
--	   , UpdatedBy
--	   , UpdatedOn
		)
VALUES	(
		 @UserID
	   , @InstallationID
	   , @VerificationID
	   , @IsActive
	   , @CreatedOn
		)
