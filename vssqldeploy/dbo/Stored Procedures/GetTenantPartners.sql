﻿
-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 2017-01-31
-- Update date: 2017-03-14 - add input tenantId to partners
-- Description:	Retrieves partners for tenant
-- =============================================

CREATE PROCEDURE [dbo].[GetTenantPartners]
	@TenantID UNIQUEIDENTIFIER
AS

BEGIN
	SET NOCOUNT ON;

	SELECT t.[TenantID], t.[Name]
	FROM [dbo].[Tenant] t
	WHERE t.[TenantID] IN
	(
		SELECT [TenantID]
		FROM [dbo].[FlatStore]
		WHERE [OwnerID] = @TenantID
		UNION
		SELECT [OwnerID]
		FROM [dbo].[FlatStore]
		WHERE [TenantID] = @TenantID
		UNION
		SELECT @TenantID
	)
END

