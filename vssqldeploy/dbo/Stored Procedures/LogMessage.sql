﻿/*

	Exec dbo.LogMessage @ModuleName = 'Loyao', @Message = 'Test Log'

*/



CREATE   PROC dbo.LogMessage
	@ModuleName		AS NVARCHAR(100) 
	,@Message		AS NVARCHAR(MAX) = ''
	,@MessageType	AS VARCHAR(20) = 'INFO'
AS
BEGIN
SET NOCOUNT ON 
	IF ( NULLIF(@ModuleName, '') IS NULL)
		RETURN

	BEGIN TRY

		INSERT INTO dbo.DBMessageLog
		(
			ModuleName, [Message], MessageType,LoggedDateTime
		)
		VALUES 
		(
			@ModuleName, @Message, @MessageType, getutcdate()
		)

	END TRY
	BEGIN CATCH
		-- 'Empty Catch Block'
		PRINT FORMATMESSAGE( 'ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY() : (%s, %i, %i, %s, %i)',  ISNULL(ERROR_PROCEDURE(), 'NA'), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY())
	END CATCH

END
	 
