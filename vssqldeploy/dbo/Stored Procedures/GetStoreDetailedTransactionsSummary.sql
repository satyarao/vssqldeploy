﻿
-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 4/27/2017
-- Update date: 5/11/2017 - only for completed transactions
-- Update date: 5/18/2017 - changes from WP-5338
-- Update date: 5/19/2017 - cast offerID to guid
-- Update date: 9/14/2017 - update Basket.TenantID
-- Update date: 12/08/2017 - Igor Gaidukov - added ability to show offers to partners
-- Update date: 01/11/2018 - Igor Gaidukov - added OfferId as a parameter
-- Description:	Returns store transactions details
-- =============================================
CREATE PROCEDURE [dbo].[GetStoreDetailedTransactionsSummary]
	@StoreID UNIQUEIDENTIFIER
	, @TenantID UNIQUEIDENTIFIER NULL
	, @FromDate DATETIME2(7)
	, @ToDate DATETIME2(7)
	, @OfferId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;

	WITH TransactionOffers AS
	(
		SELECT bli.[BasketID], TempOfferItems.OfferId, TempOfferItems.OfferTitle, TempOfferItems.OfferDiscount, TempOfferItems.OfferAmount
		FROM [dbo].[BasketLineItem] bli 
		OUTER APPLY 
			(SELECT OfferId, OfferTitle, OfferDiscount, OfferAmount, OfferTenantId FROM OPENJSON( (SELECT bli.[LoyaltyDescription]) ) WITH (OfferId nvarchar(max), OfferTitle nvarchar(max), OfferDiscount decimal(9,2), OfferAmount decimal(9,2), OfferTenantId uniqueidentifier) ) TempOfferItems
		OUTER APPLY
		(
			SELECT value as PartnerTenantId 
			FROM OPENJSON ( 
				( SELECT fb.VisibleByTenants FROM FlatBasket fb WHERE fb.TransactionID = bli.BasketID) )
			WHERE value = @TenantID

		) AS VisibleByTenants
		JOIN [dbo].[Basket] b ON b.[BasketID] = bli.[BasketID]
		WHERE (@TenantID IS NULL
				OR
			( b.StoreTenantID = @TenantID ) 
				OR 
			( b.StoreTenantID != @TenantID AND b.AppTenantId = @TenantID)
				OR  
			(b.StoreTenantID != @TenantID AND b.AppTenantId != @TenantID AND ( TempOfferItems.OfferTenantId = @TenantId OR (TempOfferItems.OfferTenantId IS NOT NULL AND  VisibleByTenants.PartnerTenantId = @TenantId) ))	)
		AND ( @OfferId IS NULL OR TempOfferItems.OfferId = @OfferId )
		AND (b.[POSDateTime] BETWEEN @FromDate AND @ToDate) 
		AND (b.[StoreID] = @StoreID) 
		AND (b.[BasketStateID] = 3)
	)
	SELECT 
		CAST(offers.OfferId AS UNIQUEIDENTIFIER) as OfferID
		, MIN( offers.OfferTitle ) as OfferTitle
		, MAX( ISNULL(offers.OfferDiscount, 0) ) as OfferDiscount
		, COUNT(DISTINCT(offers.BasketID)) as Redeemed
		, SUM( ISNULL(offers.OfferAmount, 0) ) as TotalDiscount
	FROM TransactionOffers offers
	JOIN [dbo].[Basket] b ON b.[BasketID] = offers.BasketID
	GROUP BY offers.OfferId HAVING OfferId IS NOT NULL
	ORDER BY TotalDiscount DESC, OfferDiscount DESC
END

