﻿CREATE PROCEDURE dbo.GetBasketPayment
	@BasketID UNIQUEIDENTIFIER
AS /*
EXEC dbo.GetBasketPayment @BasketID = 'E14FDEE4-1425-4743-8BA6-57CB16F3720B'
EXEC dbo.GetBasketPayment @BasketID = '73B6C2A3-A6BC-4D66-9881-C240AEC71FA2'
*/
DECLARE	@BasketTotal DECIMAL(9, 2)
DECLARE	@CCPREAUTH DECIMAL(9, 2)
DECLARE	@CCFINAL DECIMAL(9, 2)
DECLARE	@CCSALE DECIMAL(9, 2)

SELECT	@BasketTotal = total
FROM	dbo.Basket b
WHERE	b.BasketID = @BasketID

SELECT	@CCPREAUTH = SUM(bp.Amount)
FROM	dbo.BasketPayment bp
WHERE	bp.BasketID = @BasketID
		AND PaymentCommand = 'CCPREAUTH'
		AND bp.ResponseResult = 'APPROVED'

SELECT	@CCFINAL = SUM(bp.Amount)
FROM	dbo.BasketPayment bp
WHERE	bp.BasketID = @BasketID
		AND PaymentCommand = 'CCFINAL'
		AND bp.ResponseResult = 'APPROVED'

SELECT	@CCSALE = SUM(bp.Amount)
FROM	dbo.BasketPayment bp
WHERE	bp.BasketID = @BasketID
		AND PaymentCommand = 'CCSALE'
		AND bp.ResponseResult = 'APPROVED'

SELECT	@BasketTotal AS BasketTotal
	  , ISNULL(@CCPREAUTH, 0) AS CCPREAUTH
	  , ISNULL(@CCFINAL, 0) AS CCFINAL
	  , ISNULL(@CCSALE, 0) AS CCSALE
