﻿
CREATE PROCEDURE [dbo].[GetCommand] 
	@CommandType NVARCHAR(255),
	@CommandKey UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
    DECLARE @ResolverName NVARCHAR(100)

	SELECT	@ResolverName = ResolverName
	FROM	dbo.CommandMap
	WHERE	CommandType = @CommandType
			AND CommandKey = @CommandKey

	IF @ResolverName IS NULL
		BEGIN
			DECLARE @ParentTenantID UNIQUEIDENTIFIER
			SELECT	@ParentTenantID = TenantID
			FROM	dbo.Store
			WHERE	StoreID = @CommandKey

			IF @ParentTenantID IS NOT NULL
					SET @CommandKey = @ParentTenantID

			SELECT TOP 1
					@ResolverName = map.ResolverName
			FROM	dbo.Tenant start
					JOIN dbo.Tenant parent ON start.OrgID.IsDescendantOf(parent.OrgID) = 1
					LEFT JOIN dbo.CommandMap map ON map.CommandKey = parent.TenantID
													AND map.CommandType = @CommandType
			WHERE	start.TenantID = @CommandKey
					AND map.ResolverName IS NOT NULL
			ORDER BY parent.OrgLevel DESC
		END
	SELECT @ResolverName
END
