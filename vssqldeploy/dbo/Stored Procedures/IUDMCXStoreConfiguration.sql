﻿-- =============================================
-- Author:		Alex Goroshko
-- Create date: 3/24/2016
-- Description:	IUD MCX store configuration
-- =============================================
CREATE PROCEDURE [dbo].[IUDMCXStoreConfiguration]
	@StoreID				UNIQUEIDENTIFIER,
	@Fuel_api_password  NVARCHAR(max) = NULL,
	@Fuel_api_prepender NVARCHAR(max) = NULL,
	@Fuel_api_uri	    NVARCHAR(max) = NULL,
	@Fuel_api_username NVARCHAR(max) = NULL,
	@Pos_api_password   NVARCHAR(max) = NULL,
	@Pos_api_prepender  NVARCHAR(max) = NULL,
	@Pos_api_uri		NVARCHAR(max) = NULL,
	@Pos_api_username  NVARCHAR(max) = NULL,
	@MaxTryCount NVARCHAR(max) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_fuel_api_password'	,@PropertyValue = @Fuel_api_password
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_fuel_api_prepender'	,@PropertyValue = @Fuel_api_prepender
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_fuel_api_uri'		,@PropertyValue = @Fuel_api_uri
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_fuel_api_user_name'	,@PropertyValue = @Fuel_api_username
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_pos_api_password'	,@PropertyValue = @Pos_api_password
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_pos_api_prepender'	,@PropertyValue = @Pos_api_prepender
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_pos_api_uri'			,@PropertyValue = @Pos_api_uri
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_pos_api_user_name'	,@PropertyValue = @Pos_api_username
			EXEC [dbo].[IUDTenantProperty_Trim] @TenantID = @StoreID, @PropertyName = 'mcx_max_try_count'		,@PropertyValue = @MaxTryCount
		COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(4000) = ERROR_MESSAGE(),
		@ErrorNumber int = ERROR_NUMBER(),
		@ErrorSeverity int = ERROR_SEVERITY(),
		@ErrorState int = ERROR_STATE(),
		@ErrorLine int = ERROR_LINE(),
		@ErrorProcedure nvarchar(200) = ISNULL(ERROR_PROCEDURE(), '-');
		SET @ErrorMessage = N'Error: %d, Level: %d, State: %d, Procedure: %s, Line: %d, ' + 'Message: ' + @ErrorMessage;
		ROLLBACK
		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine)
	END CATCH
END
