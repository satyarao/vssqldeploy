﻿-- =============================================
-- Author:		Daniel DiPaolo
-- Create date: 2015/04/24
-- Description:	Inserts, updates, or deletes a tenant's connection info.
-- Used when a store declares its own connection info rather than it being 
-- provisioned prior to bringing a store online. Initially created for
-- "SkyWizard" initiative.
-- =============================================
CREATE PROCEDURE [dbo].[IUDTenantConnectionInfo] 
	-- Add the parameters for the stored procedure here
	@TenantID uniqueidentifier, 
	@EndpointAddress nvarchar(250),
	@ChannelType nvarchar(250),
	@ClientType nvarchar(250) = 'mCommerce', -- not required, default to mCommerce
	@IsActive bit = 1  -- default to insert/update
AS
BEGIN
	SET NOCOUNT ON;
-- using the "IsActive" naming convention to match with other IUD__ stored
-- procs, though those typically actually set an "IsActive" column for a 
-- soft delete whereas this one does not, this one does destructive deletes
IF @IsActive = 1
BEGIN
	-- merge endpoint address
	MERGE [dbo].[TenantProperty] as target
	USING 
		(SELECT
			@TenantID as TenantID,
			@EndpointAddress as EndpointAddress) AS source
	ON	target.TenantID = source.TenantID
		AND target.PropertyName = 'EndpointAddress'
	WHEN MATCHED THEN
		UPDATE	SET PropertyValue = @EndpointAddress
	WHEN NOT MATCHED THEN
		INSERT (TenantID,
				PropertyName,
				PropertyValue)
		VALUES (@TenantID,
				'EndpointAddress',
				@EndpointAddress);

	-- merge channel type
	MERGE [dbo].[TenantProperty] as target
	USING
		(SELECT
			@TenantID as TenantID,
			@ChannelType as ChannelType) AS source
	ON	target.TenantID = source.TenantID
		AND target.PropertyName = 'ChannelType'
	WHEN MATCHED THEN
		UPDATE	SET PropertyValue = @ChannelType
	WHEN NOT MATCHED THEN
		INSERT (TenantID,
				PropertyName,
				PropertyValue)
		VALUES (@TenantID,
				'ChannelType',
				@ChannelType);

	-- merge client type
	MERGE [dbo].[TenantProperty] as target
	USING
		(SELECT
			@TenantID as TenantID,
			@ClientType as ClientType) AS source
	ON	target.TenantID = source.TenantID
		AND target.PropertyName = 'ClientType'
	WHEN MATCHED THEN
		UPDATE SET PropertyValue = @ClientType
	WHEN NOT MATCHED THEN
		INSERT	(TenantID,	PropertyName,	PropertyValue)
		VALUES	(@TenantID,	'ClientType',	@ClientType);

END -- IF @IsActive = 1
ELSE
BEGIN -- @IsActive = 0
	DELETE FROM [dbo].[TenantProperty]
	WHERE
		TenantID = @TenantID
		AND PropertyName IN ('EndpointAddress', 'ChannelType', 'ClientType')
END

END
