﻿CREATE PROC [dbo].[GenerateStoreKey]
    (@StoreID UNIQUEIDENTIFIER)
AS
BEGIN

	DECLARE    @b64 VARCHAR(MAX)
	declare @ciphertext varbinary(max)
	--declare @certName sysname = 'Cert_config_2014'
	declare @symkeyName sysname = 'ConfigSymmetricKey'
	declare @symkeyGuid uniqueidentifier 
	--declare @cmd nvarchar(MAX)
	declare @data nvarchar(MAX) = CONVERT(nvarchar(MAX),@StoreID,1)
	OPEN SYMMETRIC KEY [ConfigSymmetricKey] DECRYPTION BY CERTIFICATE [Cert_config_2014]
	SELECT @symkeyGuid = key_guid(@symkeyName)
	SELECT @ciphertext = encryptbykey(@symkeyGuid, @data)
 --SET @cmd = 'SELECT CAST( decryptbykeyautocert(cert_id(''' + replace(@certName, '''', '''''') + '''), null, ' + 
 --sys.fn_varbintohexstr(@ciphertext) + ') AS nvarchar(MAX));'
 --print @cmd
	SELECT @b64 = Common.BinaryToBase64(CONVERT(VARBINARY(MAX), sys.fn_varbintohexstr(@ciphertext), 1))
	SELECT @b64 = RIGHT(@b64,43) + '='

	CLOSE SYMMETRIC KEY [ConfigSymmetricKey]
	DELETE	FROM KM.StoreKey
	WHERE	StoreID = @StoreID
	
	INSERT	INTO KM.StoreKey
	( StoreID, EncKey )
	VALUES	( @StoreID, @b64 )
	END

