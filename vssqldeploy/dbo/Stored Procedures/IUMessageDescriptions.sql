﻿

-- =============================================
-- Author:		Jamy Ryals
-- Create date: 01/26/2015
-- Description:	Insert or update MessageDescriptions
-- =============================================
CREATE PROCEDURE [dbo].[IUMessageDescriptions]
	-- Add the parameters for the stored procedure here
	@MessageDescriptions MessageDescriptionType READONLY
AS
BEGIN	
	SET NOCOUNT ON;
	
	UPDATE dbo.MessageDescription
	SET IsActive = 0, UpdatedOn = GETDATE(), UpdatedBy = N'SP'
	WHERE StoreID IN (SELECT DISTINCT StoreID FROM @MessageDescriptions)

	MERGE dbo.MessageDescription AS TARGET
	USING (SELECT	StoreID AS StoreID, -- uniqueidentifier
					Action AS Action, -- nvarchar(256)
					Version AS Version, -- nvarchar(50)
					IsEncrypted AS IsEncrypted, -- bit
					IsActive AS IsActive, -- bit
					GETDATE() AS CreatedOn, -- datetime2
					N'SP' AS CreatedBy, -- nvarchar(50)
					NULL AS UpdatedOn, -- datetime2
					NULL  AS UpdatedBy -- nvarchar(50)
					FROM @MessageDescriptions) AS SOURCE
	ON (TARGET.StoreID = SOURCE.StoreID AND
		TARGET.Action = SOURCE.Action AND
		TARGET.Version = SOURCE.Version)
	WHEN MATCHED THEN 
		UPDATE 
		SET TARGET.IsEncrypted = SOURCE.IsEncrypted,
			TARGET.IsActive = SOURCE.IsActive,
			TARGET.UpdatedOn = GETDATE(),
			TARGET.UpdatedBy = N'SP'
	WHEN NOT MATCHED THEN
		INSERT   (StoreID ,
			        Action ,
			        Version ,
			        IsEncrypted ,
			        IsActive ,
			        CreatedOn ,
			        CreatedBy ,
			        UpdatedOn ,
			        UpdatedBy
			    )
		VALUES  ( SOURCE.StoreID , -- StoreID - uniqueidentifier
			        SOURCE.Action , -- Action - nvarchar(256)
			        SOURCE.Version , -- Version - nvarchar(50)
			        SOURCE.IsEncrypted , -- IsEncrypted - bit
			        SOURCE.IsActive , -- IsActive - bit
			        GETDATE() , -- CreatedOn - datetime2
			        N'SP' , -- CreatedBy - nvarchar(50)
			        NULL , -- UpdatedOn - datetime2
			        NULL  -- UpdatedBy - nvarchar(50)
			    );
END
