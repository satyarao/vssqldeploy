﻿CREATE PROCEDURE [dbo].[GetStoreConnectionInfo]
	@StoreID UNIQUEIDENTIFIER
AS /*
exec dbo.GetStoreConnectionInfo 'AA913650-785D-42A1-A6E3-00014AC3EA2D'
*/

SET NOCOUNT ON	

SELECT	ISNULL(dbo.GetTenantPropertyValue(@StoreID, 'ChannelType', DEFAULT), 'SBR') AS ChannelType
	  , ISNULL(dbo.GetTenantPropertyValue(@StoreID, 'ClientType', DEFAULT), 'MCOMMERCE') AS ClientType
	  , dbo.GetTenantPropertyValue(@StoreID, 'EndpointAddress', DEFAULT) AS EndpointAddress
	  , dbo.GetTenantPropertyValue(@StoreID, 'SBR_AccountName', DEFAULT) AS SBRAccountName
	  , dbo.GetTenantPropertyValue(@StoreID, 'SBR_AccountKey', DEFAULT) AS SBRAccountKey
	  , MPPAID
	  FROM Store WHERE StoreID = @StoreID
