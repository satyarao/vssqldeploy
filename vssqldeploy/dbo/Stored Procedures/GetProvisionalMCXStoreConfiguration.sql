﻿CREATE PROCEDURE [dbo].[GetProvisionalMCXStoreConfiguration] 
	@StoreID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT @StoreID as StoreID,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_fuel_api_password', 1) as MCX_FuelAPI_Password,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_fuel_api_prepender', 1) as MCX_FuelAPI_Prepender,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_fuel_api_uri', 1) as MCX_FuelAPI_Uri,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_fuel_api_user_name', 1) as MCX_FuelAPI_UserName,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_pos_api_password', 1) as MCX_POSAPI_Password,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_pos_api_prepender', 1) as MCX_POSAPI_Prepender,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_pos_api_uri', 1) as MCX_POSAPI_Uri,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_pos_api_user_name', 1) as MCX_POSAPI_UserName,
		dbo.GetTenantPropertyValue(@StoreID, 'mcx_max_try_count', 1) as MaxTryCount

END
