﻿-- =============================================
-- Author:  Igor Gaidukov  
-- Create date: 18/01/2018  
-- Udpate date: 9/20/2017 Alex Goroshko - Remove P97 tenant ignorance   
-- Udpate date: 1/25/2018 Igor Gaidukov - Added output parameters   
-- Udpate date: 2/21/2018 Igor Gaidukov - added ability to search mobile users by keyword
-- Udpate date: 8/10/2018 Roman Lavreniuk - changed order to DESC
-- Udpate date: 8/23/2018 Roman Lavreniuk - return user's last NuDetect score and lockout status
-- Udpate date: 10/10/2018 Roman Lavreniuk - added NuDetect Score Filter
-- Description:	Get users which were registered in user portal (mobile app)
-- =============================================
CREATE PROCEDURE [dbo].[GetMobileUsers]  
	  @TenantIDs ListOfGuid READONLY
	, @StartDate DATETIME2 = NULL 
	, @EndDate DATETIME2 = NULL
    , @FromNuDetectScore INT = NULL
    , @ToNuDetectScore INT = NULL
	, @UserIDs ListOfGuid READONLY
	, @SearchTerm NVARCHAR(MAX) = NULL
	, @Offset INT
	, @Limit INT
AS
BEGIN
	SET NOCOUNT ON;

	WITH TempResult AS ( SELECT ui.UserID
		, ui.TenantID
		, t.Name AS TenantName
		, ui.EmailAddress AS Email
		, ui.FirstName
		, ui.LastName
		, ui.MobileNumber AS PhoneNumber
		, ui.CreatedOn AS CreatedOn
		, ui.IsActive
        , ls.NuDetectScore AS NuDetectScore
        , ls.NuDetectScoreBand AS NuDetectScoreBand
        , ls.ScoreSignals AS ScoreSignals
        , ul.BlockTransactions AS BlockTransactions
        , ul.BlockCardAddition AS BlockCardAddition

		FROM dbo.UserInfo ui
			JOIN dbo.Tenant t ON t.TenantID = ui.TenantID
            LEFT JOIN NuDetect.UserLastScore ls ON ui.UserID = ls.UserId
            LEFT JOIN dbo.UserLockouts ul ON ui.UserID = ul.UserId
		WHERE (( @StartDate IS NULL AND @EndDate IS NULL) OR (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND ui.CreatedOn >= @StartDate AND ui.CreatedOn <= @EndDate ))
			AND (ui.TenantID IN (SELECT ID FROM @TenantIDs) OR (NOT EXISTS (SELECT * FROM @TenantIDs)))
			AND (@SearchTerm IS NULL 
                OR ui.UserID LIKE '%' + @SearchTerm + '%' 
                OR ui.FirstName LIKE '%' + @SearchTerm + '%'
				OR ui.LastName LIKE '%' + @SearchTerm + '%'
                OR (ui.UserID IN (SELECT ID FROM @UserIDs) AND (EXISTS (SELECT * FROM @UserIDs) ) ) )
            AND (ui.UserID IN (SELECT ID FROM @UserIDs) OR (NOT EXISTS (SELECT * FROM @UserIDs) ) )
            AND (@FromNuDetectScore IS NULL AND @ToNuDetectScore IS NULL
                OR @FromNuDetectScore IS NULL AND ls.NuDetectScore <= @ToNuDetectScore
                OR @ToNuDetectScore IS NULL AND ls.NuDetectScore >= @FromNuDetectScore
                OR ls.NuDetectScore >= @FromNuDetectScore AND ls.NuDetectScore <= @ToNuDetectScore)
			) -- only user of user portal
		, TempCount AS (SELECT COUNT(*) AS Total FROM TempResult)


   SELECT UserID
          , TenantID
          , TenantName
          , FirstName
          , Email
          , LastName
          , PhoneNumber
          , CreatedOn
          , IsActive
          , Total
          , NuDetectScore
          , NuDetectScoreBand
          , ScoreSignals
          , BlockTransactions
          , BlockCardAddition
		FROM TempResult, TempCount
		ORDER BY CreatedOn DESC OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY
END





