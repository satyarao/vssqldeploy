﻿CREATE PROCEDURE [dbo].[GetFundingProviderId]
	@TransactionID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT CAST(lkpp.FundingProviderID AS INT) AS FundingProviderID
	
	FROM [dbo].[BasketPayment] bp 
		JOIN [Payment].[LKUserPaymentSource] lkups ON  lkups.UserPaymentSourceID = bp.UserPaymentSourceID 
        JOIN [Payment].[LKPaymentProcessor] lkpp ON  lkpp.[PaymentProcessorID] = lkups.[PaymentProcessorID]
        WHERE bp.BasketID = @TransactionID

END

