﻿CREATE   PROCEDURE [dbo].[SetInActiveTermsAndConditions] @TermsConditionsId UNIQUEIDENTIFIER
AS
BEGIN
	UPDATE dbo.TermsAndConditionsDetails 
	SET IsActive = 0 
	WHERE TermsConditionsId = @TermsConditionsId

	UPDATE dbo.AcceptanceStatuses 
	SET IsActive = 0 
	WHERE TermsConditionsId = @TermsConditionsId
END

