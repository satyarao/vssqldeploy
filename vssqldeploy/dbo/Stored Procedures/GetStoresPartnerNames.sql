﻿
-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 2017-03-20
-- Description:	Retrieves partners names for stores
-- =============================================
CREATE PROCEDURE [dbo].[GetStoresPartnerNames]
	@StoreIDs ListOfGuid READONLY
AS

BEGIN
	SET NOCOUNT ON;

	SELECT 	s.[StoreID]
			,ot.[Name] as OwnerName
			,(
				SELECT t.[Name] 
				FROM [dbo].[FlatStore] fs
				JOIN [dbo].[Tenant] t ON fs.[TenantID] = t.[TenantID] 
				WHERE (s.[StoreID] = fs.[StoreID] AND fs.[IsOwnedByTenant] = 0) 
				FOR JSON PATH
			) AS PartnerNames
	FROM @StoreIDs ids
	JOIN [dbo].[Store] s ON s.[StoreID] = ids.ID
	JOIN [dbo].[Tenant] ot ON s.[TenantID] = ot.[TenantID]
END

