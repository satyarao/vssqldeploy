﻿-- =============================================
-- Author: Igor Gaidukov   
-- Create date: 04/07/2018
-- Update date: 04/17/2018 - Alex Goroshko -  added UserGroupModuleName to return set
-- Update date: 04/19/2018 - Igor Gaidukov -  added user's CreatedOn and Name to return set
-- Description:   Get permission billing report data
-- =============================================

CREATE PROCEDURE [dbo].[GetPermissionReportData]
	  @TenantID UNIQUEIDENTIFIER = NULL
	, @FromDate DATETIME2
	, @ToDate DATETIME2
	, @Offset INT
	, @Limit INT
	AS 
	BEGIN
		WITH TempUsers AS 
		( 
			SELECT ua.[Key] AS UserAccountKey, ui.[Name] AS UserName, Email, ui.CreatedOn, uc.[Value] AS RestrictedTenantID						
			FROM [MR].[UserAccounts] ua
			JOIN [dbo].[UserInfo] ui on ui.UserID = ua.ID
			LEFT JOIN MR.UserClaims uc ON (ua.[Key] = uc.[ParentKey] AND uc.[Type] = 'RestrictedTenantID')
			WHERE ua.Tenant = '00000000-0000-0000-0000-000000000000' 
				AND ui.IsActive = 1 
				AND ui.IsDeleted = 0 
				AND (
						@TenantID IS NULL 
						OR 
						(@TenantID = '00000000-0000-0000-0000-000000000000' AND NOT EXISTS( SELECT * FROM [MR].[UserClaims] uc WHERE ua.[Key] = uc.[ParentKey] AND uc.[Type] = 'RestrictedTenantID'))
						OR 
						(@TenantID IS NOT NULL AND uc.[Value] = @TenantID) )
		), TempReport AS
		(
			SELECT 
				ISNULL(usr.RestrictedTenantID, grp.TenantID) AS TenantID,
				usr.[UserAccountKey],
				usr.UserName,
				usr.[Email] AS UserEmail,
				usr.[CreatedOn],
				grp.[UserGroupID],
				grp.[UserGroupName],
				module.[UserGroupModuleName]
			FROM TempUsers usr
				LEFT JOIN MR.UserClaims claim ON claim.ParentKey = usr.UserAccountKey AND claim.[Type] = 'UserGroupID'
				JOIN AccessControl.UserGroup grp ON grp.UserGroupID = claim.[Value]
				JOIN AccessControl.UserGroupModule module ON module.UserGroupModuleID = grp.UserGroupModuleID
			WHERE (usr.RestrictedTenantID IS NULL OR usr.RestrictedTenantID = grp.TenantID)
		), TempCount AS (SELECT COUNT(*) AS Total FROM TempReport)

		SELECT CAST(tr.TenantID AS UNIQUEIDENTIFIER),
			t.[Name] AS TenantName,
			tr.UserAccountKey,
			UserName,
			UserEmail,
			tr.CreatedOn,
			tr.UserGroupID,
			UserGroupName,
			UserGroupModuleName,
			DateOfAssignment,
			(SELECT Total FROM TempCount) AS Total
		FROM TempReport tr
		JOIN dbo.Tenant t ON tr.TenantID = t.TenantID
		LEFT JOIN [dbo].[UserGroupAssignment] uga ON tr.UserAccountKey = uga.UserAccountKey AND tr.UserGroupID = uga.UserGroupID
		WHERE (uga.[DateOfAssignment] IS NULL OR uga.[DateOfAssignment] BETWEEN @FromDate AND @ToDate) 
		ORDER BY TenantName ASC
		OFFSET @Offset ROWS 
		FETCH NEXT @Limit ROWS ONLY
	END
