﻿-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 12/13/2016
-- Update date: 03/15/2017 - return several parameters instead of only storeId
-- Updated: 07/25/2017 Igor Gaidukov - use AllowInsidePayment and AllowOutsidePayment instead of IsMobilePaymentsEnabled
-- Updated: 09/26/2017 Alex Goroshko - added EposIds
-- Updated: 10/02/2017 Igor Gaidukov - use DisplayOnMobile instead of IsActive
-- Updated: 12/19/2017 Igor Gaidukov - added AllowMultipleLoyalty setting
-- Updated: 02/19/2018 Andrei Ramanovich - added PaymentsOffline setting
-- Updated: 05/04/2018 Igor Gaidukov - added IsDeleted setting
-- Description:	Create new store
-- =============================================
CREATE PROCEDURE [dbo].[CreateStore] 
	  @StoreID UNIQUEIDENTIFIER = NULL
	, @TenantID UNIQUEIDENTIFIER
	, @Name nvarchar(256)
	, @StoreNumber nvarchar(64) = NULL
	, @FuelBrandID INT = NULL
	, @NumberOfPumps TINYINT = NULL
	, @MerchantID NVARCHAR(50) = NULL
	, @Phone nvarchar(20) = NULL
	, @TimeZoneID int
	, @DisplayOnMobile BIT = NULL
	, @StreetAddress nvarchar(128)
	, @City nvarchar(64)
	, @StateCode char(2) = NULL
	, @ZipCode nvarchar(16)
	, @County nvarchar(100) = NULL
	, @CountryIsoCode char(2)
	, @Latitude decimal(10,7) 
	, @Longitude decimal(10,7)
	, @AllowInsidePayment bit = NULL
	, @AllowOutsidePayment bit = NULL
	, @EposIds NVARCHAR(MAX) = NULL	
	, @AllowMultipleLoyalty bit = NULL
	, @PaymentsOffline bit = NULL
	, @IsDeleted bit = NULL
	AS
	BEGIN
		SET NOCOUNT ON;
		
		IF(@StoreID IS NULL)
			BEGIN
				SET @StoreID = newid();
			END
		
		INSERT INTO [dbo].[Store]
						([StoreID]
						,[TenantID]
						,[Name]
						,[StoreNumber]
						,[FuelBrandID]
						,[MerchantID]
						,[Phone]
						,[NumberOfPumps]
						,[RegularPCATSCode]
						,[MidgradePCATSCode]
						,[PremiumPCATSCode]
						,[DieselPCATSCode]
						,[E85PCATSCode]
						,[StreetAddress]
						,[City]
						,[StateCode]
						,[ZipCode]
						,[County]
						,[CountryIsoCode]
						,[Latitude]
						,[Longitude]
						,[TimeZoneID]
						,[DisplayOnMobile]
						,[CreatedBy]
						,[CreatedOn]
						,[AllowInsidePayment]
						,[AllowOutsidePayment]
						,[EposIds]
						,[AllowMultipleLoyalty]
						,[PaymentsOffline]
						,[IsDeleted])
					VALUES
						( @StoreID
						, @TenantID
						, @Name
						, @StoreNumber
						, ISNULL(@FuelBrandID, CAST((SELECT dbo.GetTenantPropertyValue(@TenantID, 'FuelBrandID', DEFAULT)) AS INT))
						, @MerchantID
						, @Phone
						, ISNULL(@NumberOfPumps, 2)
						, '001'
						, '002'
						, '003'
						, '021'
						, '026'
						, @StreetAddress
						, @City
						, @StateCode
						, @ZipCode
						, @County
						, @CountryIsoCode
						, @Latitude
						, @Longitude
						, @TimeZoneID
						, @DisplayOnMobile
						, N'[dbo].[CreateStore]'
						, GETUTCDATE()
						, ISNULL(@AllowInsidePayment, 0)
						, ISNULL(@AllowOutsidePayment, 0)
						, @EposIds
						, ISNULL(@AllowMultipleLoyalty, 0)
						, ISNULL(@PaymentsOffline, 0)
						, ISNULL(@IsDeleted, 0));
	
	EXEC [dbo].[GenerateStoreKey] @StoreID = @StoreID;
		
	SELECT  StoreID AS StoreId,
			TenantID AS TenantId,
			Name,
			MPPAID AS MppaId 
	FROM [dbo].[Store] WHERE [StoreID] = @StoreID;
END
