﻿
-- =============================================
-- Author:      Igor Kutsenka
-- Udpate date: 2/20/2019 Dzmitry Mikhailau - change @ObjectId from uniqueidentifier to nvarchar(255) 
-- Date:        8/17/2018
-- Description: Get Consent for user and add item to terms of use for tenant if it isn't exists
-- =============================================
CREATE PROCEDURE [dbo].[GetAndValidateTermsOfUseUserConsented]
	@ObjectId nvarchar(255) 
	,@TenantId uniqueidentifier
AS
BEGIN
	BEGIN TRAN
		IF NOT EXISTS (SELECT TOP (1) TermsOfUseID FROM dbo.TermsOfUse WHERE TenantID = @TenantId)
		BEGIN
			INSERT INTO dbo.TermsOfUse 
			(TenantID, DateVersion)
			VALUES(@TenantId, GETUTCDATE())
		END
		
		SELECT TOP (1) 
			touc.Consent
		FROM  dbo.TermsOfUseConsents touc 
		WHERE 
		touc.TermsOfUseID = 
			(
				SELECT TOP(1)  TermsOfUseID tou
				FROM dbo.TermsOfUse tou
				WHERE tou.TenantID = @TenantId
				ORDER BY tou.DateVersion DESC
			)
		AND touc.ObjectId = @ObjectId
		ORDER BY touc.[DateTime] DESC
	COMMIT
END

