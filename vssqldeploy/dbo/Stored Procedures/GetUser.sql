﻿-- =============================================
-- Update date: 07/17/2018 Igor Kutesnka - added TenantObjectID
-- Update date: 02/04/2019 Roman Lavreniuk - added IsBlocked
-- =============================================
CREATE PROCEDURE [dbo].[GetUser]
	@UserId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT UserID, TenantID, EmailAddress, MobileNumber, FirstName, LastName, Name, IsDeleted, LastSignin, AgreementVersion, IsBlocked, IsActive, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn
	FROM dbo.UserInfo
	WHERE UserID = @UserId
END
