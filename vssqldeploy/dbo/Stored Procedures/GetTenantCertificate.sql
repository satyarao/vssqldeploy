﻿
-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 7/05/2018
-- Update date: 12/11/2018 Elijah Kuranets support multitenancy
-- Description:	Returns tenant certificate data
-- =============================================
CREATE PROCEDURE [dbo].[GetTenantCertificate] 
	(
	@TenantID UNIQUEIDENTIFIER,
	@HierarchyDepth SMALLINT = NULL
	)
AS
BEGIN
	DECLARE @Certificates TABLE ( CertData nvarchar(MAX) NOT NULL );

	INSERT INTO @Certificates
	SELECT  CertData
	FROM	dbo.TenantCertificate
	WHERE	TenantID = @TenantID

	IF NOT EXISTS (SELECT * FROM @Certificates)
		BEGIN
			SELECT  tc.CertData
			FROM	dbo.Tenant start
					JOIN dbo.Tenant parent ON start.OrgID.IsDescendantOf(parent.OrgID) = 1
					LEFT JOIN dbo.TenantCertificate tc ON tc.TenantID = parent.TenantID
			WHERE	START.TenantID = @TenantID
					AND tc.CertData IS NOT NULL
					AND start.OrgLevel - parent.OrgLevel <= COALESCE(@HierarchyDepth, start.OrgLevel - parent.OrgLevel)
			ORDER BY parent.OrgLevel DESC
		END

	UPDATE @Certificates SET CertData = REPLACE(CertData, CHAR(13), '')

	SELECT * FROM @Certificates
END
