﻿/*

EXEC dbo.LoadEVSitesData
	@ProviderName = 'PlugSurfing'
	,@EVSiteDataJson = '{ssdf}'

*/

CREATE   PROC dbo.LoadEVSitesData
	@ProviderName			VARCHAR(100) 
	,@EVSiteDataJson		NVARCHAR(MAX)
AS 
BEGIN

	SET NOCOUNT ON 

	DECLARE @ServiceId INT
	DECLARE @ProviderTenantId	UNIQUEIDENTIFIER
	DECLARE @Result				VARCHAR(50)		
	DECLARE @ErrorMessage		VARCHAR(500)	
	
	SELECT @ProviderTenantId = t.TenantID FROM dbo.Tenant t WHERE t.Name = 'PlugSurfing'
	SELECT @ServiceId = ls.ServiceID FROM dbo.LKSiteServices ls WHERE ls.ServiceName = 'EVCharging';

	IF (@ProviderTenantId IS NULL OR @ProviderName != 'PlugSurfing')
	BEGIN
		--RAISERROR('Invalid Provider Name.', 16, 1);
		THROW 100001, 'Invalid Provider Name.', 1;
		RETURN -1;
	END

	IF (ISJSON(@EVSiteDataJson) = 0)
	BEGIN
		THROW 100002, 'Invalid Json format for site data.', 1;
		--RAISERROR('Invalid Json data', 16, 1);
		RETURN -1;		
	END;

	BEGIN TRY
		BEGIN TRANSACTION LoadEVSitesData

		DROP TABLE IF EXISTS #EVCJsonData;
		DROP TABLE IF EXISTS #SiteDetail;
		DROP TABLE IF EXISTS #EVChargeConnector;

		CREATE TABLE #EVCJsonData
		(
			SiteIdentifier			NVARCHAR(100)
			,SiteName				NVARCHAR(200)
			,SiteDisplayName		NVARCHAR(200)
			,IsActive				BIT
			,StreetNumber			NVARCHAR(50)
			,StreetName				NVARCHAR(100)
			,City					NVARCHAR(50)
			,StateCode				NVARCHAR(50)
			,ZipCode				NVARCHAR(50)
			,CountryIsoCode			NVARCHAR(10)
			,Latitude				[decimal](10, 7)
			,Longitude				[decimal](10, 7)
			,Phone					NVARCHAR(50)
			,EVConnectorIdentifier	NVARCHAR(50)
			,EVConnectorStatus		NVARCHAR(50)
			,EVConnectorName		NVARCHAR(50)
			,EVConnectorSpeed		NVARCHAR(50)
			,EVConnectorMode		NVARCHAR(50)
		)

		CREATE TABLE #SiteDetail
		(
			SiteIdentifier		NVARCHAR(200)
			,[Name]				VARCHAR(200)
			,DisplayName		VARCHAR(200)
			,IsActive			BIT
			,StreetNumber		VARCHAR(50)
			,StreetName			VARCHAR(200)
			,City				VARCHAR(100)
			,StateCode			VARCHAR(50)
			,ZipCode			VARCHAR(50)
			,CountryIsoCode		VARCHAR(50)
			,TenantId			UNIQUEIDENTIFIER
			,Latitude			DECIMAL(10,7)
			,Longitude			DECIMAL(10,7)
			,Phone				VARCHAR(50)
			,Email				VARCHAR(100)
			,WebUrl				VARCHAR(200)

			,RankID				INT
		)
		CREATE UNIQUE CLUSTERED INDEX IX_SiteDetails ON #SiteDetail
		(
			Longitude ,
			Latitude 	
		)
		WITH
		(
			IGNORE_DUP_KEY = ON 
		)



		CREATE TABLE #EVChargeConnector
		(
			ServiceProviderId		INT,
			SiteId					INT,
			EVConnectorIdentifier	VARCHAR(50),
			EVConnectorStatus		VARCHAR(50),
			EVConnectorName			VARCHAR(50),
			EVConnectorSpeed		VARCHAR(50),
			EVConnectorMode			VARCHAR(50)
		)

		INSERT INTO #EVCJsonData
		(
			SiteIdentifier
			,SiteName 
			,SiteDisplayName 
			,IsActive 
			,StreetNumber
			,StreetName
			,City
			,StateCode 
			,ZipCode 
			,CountryIsoCode	
			,Latitude	
			,Longitude	
			,Phone	
			,EVConnectorIdentifier	 
			,EVConnectorStatus	
			,EVConnectorName	
			,EVConnectorSpeed	
			,EVConnectorMode	
		)

		SELECT	
				SiteIdentifier,
				SiteName, 
				ISNULL(SiteDisplayName, SiteName) as SiteDisplayName,
				1 as IsActive, 
				StreetNumber, 
				StreetName, 
				City, 
				Null as StateCode, 
				ZipCode, 
				CountryIsoCode, 
				Latitude, 
				Longitude, 
				Phone,
				EVConnectorIdentifier, 
				EVConnectorStatus, 
				EVConnectorName, 
				EVConnectorSpeed, 
				EVConnectorMode
		FROM OPENJSON(@EVSiteDataJson) 
		WITH 
		(
			[stations] nvarchar(max) '$.stations' as json
		) evcData
		CROSS APPLY OPENJSON(evcData.stations) stn_arr
		CROSS APPLY OPENJSON(stn_arr.value)
		WITH
		(
			SiteIdentifier			NVARCHAR(100)		'$.id',
			SiteName				NVARCHAR(100)		'$.name',
			SiteDisplayName			NVARCHAR(100)		'$.readableName',
			StreetNumber			VARCHAR(100)		'$.address.street_number',
			StreetName				NVARCHAR(100)		'$.address.street',
			ZipCode					NVARCHAR(50)		'$.address.zip',
			City					NVARCHAR(100)		'$.address.city',
			CountryIsoCode			VARCHAR(5)			'$.address.country',
			Latitude				[decimal](10, 7)	'$.latitude',
			Longitude				[decimal](10, 7)	'$.longitude',
			Phone					NVARCHAR(20)		'$.contact.phone',
			Connecctors				NVARCHAR(MAX)		'$.connectors' AS JSON
		) stn
		CROSS APPLY OPENJSON(Connecctors)
		WITH
		(
			EVConnectorIdentifier	VARCHAR(50)			'$.id',
			EVConnectorStatus		VARCHAR(50)			'$.status',
			EVConnectorName			VARCHAR(50)			'$.name',
			EVConnectorSpeed		VARCHAR(50)			'$.speed',
			EVConnectorMode			VARCHAR(50)			'$.mode'
		) cnctr

		PRINT FORMATMESSAGE('Total Rows Inserted into #EVCJsonData : %d', @@ROWCOUNT)


		---- FOR TESTING
		--select * from #EVCJsonData where EVConnectorIdentifier= '92964'
		--update #EVCJsonData 
		--set EVConnectorSpeed= '22kW'
		--where EVConnectorIdentifier= '92964'

		INSERT INTO #SiteDetail
		(
			SiteIdentifier
			,[Name] 
			,DisplayName	 
			,IsActive	
			,StreetNumber
			,StreetName
			,City	 
			,StateCode	 
			,ZipCode	 
			,CountryIsoCode	 
			,TenantId	
			,Latitude	
			,Longitude	
			,Phone	
			,Email	
			,WebUrl	
			,RankID
		)

		SELECT DISTINCT
			evc.SiteIdentifier,
			evc.SiteName, 
			evc.SiteDisplayName, 
			evc.IsActive, 
			evc.StreetNumber,
			evc.StreetName,
			evc.City, 
			evc.StateCode, 
			evc.ZipCode, 
			evc.CountryIsoCode, 
			@ProviderTenantId, -- TenantID
			evc.Latitude, 
			evc.Longitude, 
			evc.Phone, 
			NULL, -- Email
			NULL, -- WebUrl
			RANK() OVER (
				PARTITION BY [Latitude], [Longitude] 
				ORDER BY [Latitude], [Longitude]
			) AS RankID
		FROM #EVCJsonData evc
		WHERE evc.Latitude IS NOT NULL AND evc.Longitude IS NOT NULL
	
		PRINT 'Loaded #SiteDetail'

		-- Remove Duplicates
		DELETE #SiteDetail WHERE RankID != 1

		MERGE INTO Common.SiteDetail AS tgt
		USING #SiteDetail AS src
			ON tgt.Latitude= src.Latitude AND
			tgt.Longitude = src.Longitude 
		WHEN MATCHED AND 
		(
			tgt.StreetNumber != ISNULL(src.StreetNumber, '')
			OR tgt.StreetName != ISNULL(src.StreetName, '')
			OR tgt.City != ISNULL(src.City, '')
			OR tgt.Phone != ISNULL(src.Phone, '')
		)
		THEN 
			UPDATE SET 
				tgt.StreetNumber = ISNULL(src.StreetNumber, ''),
				tgt.StreetName = ISNULL(src.StreetName, ''),
				tgt.City = ISNULL(src.City, ''),
				tgt.Phone = ISNULL(src.Phone, ''),
				tgt.UpdatedOn = GETDATE(),
				tgt.UpdatedBy = SUSER_NAME()
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
			(
				SiteIdentifier
				,Name
				,DisplayName
				,IsActive
				,StreetNumber
				,StreetName
				,City
				,StateCode
				,ZipCode
				,CountryIsoCode
				,Latitude
				,Longitude
				,Phone
				,Email
				,WebUrl
				,DataProviderName
				,CreatedBy
				,CreatedOn
			) 
			VALUES
			(
				src.SiteIdentifier
				,src.Name
				,src.DisplayName
				,src.IsActive
				,src.StreetNumber
				,src.StreetName
				,src.City
				,src.StateCode
				,src.ZipCode
				,src.CountryIsoCode
				,src.Latitude
				,src.Longitude
				,src.Phone
				,src.Email
				,src.WebUrl
				,@ProviderName
				,SUSER_NAME()
				,GETDATE()
			);

			PRINT 'Merged Into SiteDetail'

		INSERT INTO #EVChargeConnector
		(
			ServiceProviderId
			,SiteId
			,EVConnectorIdentifier
			,EVConnectorStatus
			,EVConnectorName
			,EVConnectorSpeed
			,EVConnectorMode
		)

		SELECT DISTINCT
			sp.ServiceProviderId
			,sd.SiteId
			,evjson.EVConnectorIdentifier	
			,evjson.EVConnectorStatus	
			,evjson.EVConnectorName	
			,evjson.EVConnectorSpeed	
			,evjson.EVConnectorMode
		FROM #EVCJsonData evjson 
			INNER JOIN Common.SiteDetail sd ON 
				evjson.SiteName = sd.Name 
				and ISNULL(evjson.Longitude, 0) = ISNULL(sd.Longitude, 0)
				AND ISNULL(evjson.Latitude, 0) = ISNULL(sd.Latitude,0)
				--and ISNULL(evjson.City, '') = ISNULL(sd.City , '')
				and ISNULL(evjson.ZipCode, '') = ISNULL(sd.ZipCode, '')
			INNER JOIN dbo.ServiceProvider sp ON 
				sp.ProviderTenantId = @ProviderTenantId 
				AND sp.ServiceId = @ServiceId

		PRINT 'Inserted Into #EVChargeConnector'

		MERGE INTO [dbo].[EVChargeConnector] as tgt
		USING #EVChargeConnector src
			ON tgt.ServiceProviderId = src.ServiceProviderId AND tgt.EVConnectorIdentifier = src.EVConnectorIdentifier and tgt.SiteId = src.SiteId
		WHEN MATCHED 
		AND 
			(	
				tgt.Name != src.EVConnectorName
				OR tgt.SpeedInkW != ISNULL(src.EVConnectorSpeed, '0.0kW')
				OR tgt.Mode != src.EVConnectorMode
				OR tgt.Status != src.EVConnectorStatus
			)
		THEN
			UPDATE SET
				tgt.Name = src.EVConnectorName
				,tgt.SpeedInkW = ISNULL(src.EVConnectorSpeed, '0.0kW')
				,tgt.Mode = src.EVConnectorMode
				,tgt.Status = src.EVConnectorStatus
				,tgt.UpdatedBy = SUSER_NAME()
				,tgt.UpdatedOn = GETDATE()
		WHEN NOT MATCHED BY TARGET THEN
			INSERT
			(
				ServiceProviderId
				,SiteId
				,EVConnectorIdentifier	
				,Name	
				,SpeedInkW	
				,Mode	
				,Status	
				,CreatedBy	
				,CreatedOn	
			)
			VALUES
			(
				src.ServiceProviderId
				,src.SiteId
				,src.EVConnectorIdentifier
				,src.EVConnectorName
				,ISNULL(src.EVConnectorSpeed, '0.0kW')
				,src.EVConnectorMode
				,src.EVConnectorStatus
				,SUSER_NAME()
				,GETDATE()
			);

		COMMIT TRANSACTION LoadEVSitesData;

		PRINT 'Merged Into EVChargeConnector'
	END TRY
	BEGIN CATCH

		DECLARE @ErrorNumber INT
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @Result = 'Failed'
		SET @ErrorNumber = ERROR_NUMBER()
		

		DECLARE @Exceptions [StoredProcedureExceptionType];
		INSERT INTO @Exceptions([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
		SELECT ERROR_PROCEDURE(), ERROR_LINE(), @ErrorNumber, LEFT(@ErrorMessage, 255), ERROR_SEVERITY(), NULL AS [StoredProcedureInput];

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION LoadEVSitesData

		EXEC [dbo].[PostStoredProcedureException] @Exceptions;

		THROW 100003, @ErrorMessage, 1;
		RETURN -1
		
	END CATCH

	SET @ErrorMessage = NULL
	SET @Result = 'Success'

	DROP TABLE IF EXISTS #EVCJsonData
	DROP TABLE IF EXISTS #SiteDetail
	DROP TABLE IF EXISTS #EVChargeConnector

	RETURN 0;
END

