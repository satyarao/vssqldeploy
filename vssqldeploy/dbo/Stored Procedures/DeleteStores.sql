﻿-- =============================================
-- Create date: 5/12/2017
-- Description:	Deletes stores specified in the passed in list of IDs

--Example Usage
--	DECLARE @StoreIDs ListOfGuid;
--	INSERT INTO @StoreIDs
--	SELECT storeid FROM store WHERE mppaid in (05734644);
--	exec deletestores @StoreIDs

-- =============================================
CREATE PROCEDURE [dbo].[DeleteStores]
	@StoreIDs ListOfGuid READONLY,
	@DeleteTransactions BIT = 0
AS
BEGIN

BEGIN TRANSACTION [DeleteStores]
BEGIN TRY
	IF EXISTS(select 1 from basket where storeid in (select ID from @StoreIDs)) AND @DeleteTransactions = 0
	BEGIN
		RAISERROR('Transactions exist for one or more stores you are attempting to delete. No stores have been deleted.', 16, 1);
	END

	delete from flatstore where storeid in (select ID from @StoreIDs)
	delete from hoursofoperation where storeid in (select ID from @StoreIDs)
	delete from storeservice where storeid in (select ID from @StoreIDs)
	delete from KM.storekey where storeid in (select ID from @StoreIDs)
	delete from siteinstallinfo where storeid in (select ID from @StoreIDs)
	delete from siteinfo where storeid in (select ID from @StoreIDs)
	delete from fuelprice where storeid in (select ID from @StoreIDs)
	delete from loyalty.loyaltyprogramtostoremapping where storeid in (select ID from @StoreIDs)
	delete from basketlineitem where basketid in (select basketid from basket where storeid in (select ID from @StoreIDs))
	delete from basketpayment where basketid in (select basketid from basket where storeid in (select ID from @StoreIDs))
	delete from basket where storeid in (select ID from @StoreIDs)
	delete from mppabatch where storeid in (select ID from @StoreIDs)
	delete from confignpca where tenantid in (select ID from @StoreIDs)
	delete from ConfigPrecidiaLaneMap where tenantid in (select ID from @StoreIDs)
	delete from ConfigPRECIDIAPOSLYNX where tenantid in (select ID from @StoreIDs)
	delete from payment.ConfigMCX where storeid in (select ID from @StoreIDs)
	delete from payment.ConfigBimStoreLevel where storeid in (select ID from @StoreIDs)
	delete from payment.ConfigSynchronyStoreLevel where storeid in (select ID from @StoreIDs)
	delete from payment.ConfigTokenExStoreLevel where storeid in (select ID from @StoreIDs)
	delete from store where storeid in (select ID from @StoreIDs)
	delete from TenantProperty where TenantID in (select ID from @StoreIDs)

	COMMIT TRANSACTION [DeleteStores]
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION [DeleteStores]
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END
