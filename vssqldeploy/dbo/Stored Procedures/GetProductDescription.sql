﻿CREATE PROC [dbo].[GetProductDescription]
	@TenantID UNIQUEIDENTIFIER
  , @ProductCode NVARCHAR(50)
AS
DECLARE	@ProductConfigID INT
SELECT	@ProductConfigID = ProductConfigID
FROM	dbo.StoreConfig
WHERE	StoreID = @TenantID
SET @ProductConfigID = ISNULL(@ProductConfigID, 1)

DECLARE	@ProductDescription NVARCHAR(100)

SELECT	@ProductDescription = ProductDescription
FROM	dbo.Product
WHERE	ProductConfigID = @ProductConfigID
		AND ProductCode = @ProductCode

SELECT	ISNULL(@ProductDescription, @ProductCode) AS ProductDecription
