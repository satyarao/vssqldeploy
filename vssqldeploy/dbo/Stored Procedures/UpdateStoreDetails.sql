﻿
-- =============================================
-- Author:		Daniel DiPaolo
-- Create date: 2015/05/13
-- Updated 12/13/2016 Alex Goroshko - Address updates
-- Updated 01/04/2017 Commited TRAN rows
-- Updated 11/10/2017 Ihar Kutesenko - Site Unavaliable Pumps
-- Updated 02/27/2019 Roman Lavrneiuk - MppaID, StreetAddress2, FuelServiceStatus, CarWashServiceStatus
-- Description:	Updates an existing store's details
-- Formerly 1.0.1161_1.0.1162 in mppadev
-- =============================================

CREATE   PROCEDURE [dbo].[UpdateStoreDetails] 
	-- Add the parameters for the stored procedure here
	@StoreID uniqueidentifier,
	@StoreName nvarchar(256) = NULL,
	@StoreNumber nvarchar(64) = NULL,
	@MerchantID nvarchar(50) = NULL,
    @MppaID int = NULL,
	@NumberOfPumps tinyint = NULL,
	@StreetAddress nvarchar(128) = NULL,
    @StreetAddress2 nvarchar(128) = NULL,
    @FuelServiceStatus nvarchar(128) = NULL,
    @CarWashServiceStatus nvarchar(128) = NULL,
	@City nvarchar (64) = NULL,
	@StateCode char(2) = NULL,
	@ZipCode nvarchar(16) = NULL,
	@Latitude decimal(10,7) = NULL,
	@Longitude decimal(10,7) = NULL,
	@SiteUnavailablePumps nvarchar(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ResultCode INT
	DECLARE @Reason NVARCHAR(MAX)
	
	IF NOT EXISTS (SELECT 1 
					FROM dbo.Store 
					WHERE 
						StoreID = @StoreID)
	BEGIN
		SET @ResultCode = 1
		SET @Reason = 'No store found matching Store ID: ' + CONVERT(NVARCHAR(36), @StoreID)
	END
	ELSE
	BEGIN
		--BEGIN TRAN StoreUpdate
		BEGIN TRY
			UPDATE dbo.Store SET
				Name = ISNULL(@StoreName, Name)
				, StoreNumber = ISNULL(@StoreNumber, StoreNumber)
				, MerchantID = ISNULL(@MerchantID, MerchantID)
                , MPPAID = ISNULL(@MppaID, MPPAID)
				, NumberOfPumps = ISNULL(@NumberOfPumps, NumberOfPumps)
				, StreetAddress = ISNULL(@StreetAddress, StreetAddress)
                , StreetAddress2 = ISNULL(@StreetAddress2, StreetAddress2)
                , FuelServiceStatus = ISNULL(@FuelServiceStatus, FuelServiceStatus)
                , CarWashServiceStatus = ISNULL(@CarWashServiceStatus, CarWashServiceStatus)
				, City = ISNULL(@City, City)
				, StateCode = ISNULL(@StateCode, StateCode)
				, ZipCode = ISNULL(@ZipCode, ZipCode)
				, Latitude = ISNULL(@Latitude, Latitude)
				, Longitude = ISNULL(@Longitude, Longitude)
				, SiteUnavailablePumps = ISNULL(@SiteUnavailablePumps, SiteUnavailablePumps)
				, UpdatedOn = GETUTCDATE()
				, UpdatedBy = '[dbo].[UpdateStoreDetails]'
			WHERE
				StoreID = @StoreID
		END TRY
		BEGIN CATCH
			--ROLLBACK TRAN StoreUpdate
			SET @ResultCode = 2
			SET @Reason = 'Exception while updating store table ' + ERROR_MESSAGE()
			SELECT @ResultCode, @Reason
			RETURN
		END CATCH
	
		--COMMIT TRAN StoreUpdate
		SET @ResultCode = 0  -- success!
		SET @Reason = 'Successfully updated store'
	END
	SELECT @ResultCode, @Reason
END

