﻿-- =============================================
-- Author:      Igor Gaidukov
-- Create Date: 11/24/2017
-- Updated: 12/19/2017 Igor Gaidukov - added AllowMultipleLoyalty setting
-- Updated: 01/22/2018 - Igor Gaidukov - changed type of storeInstallDate to datetime2
-- Updated: 02/19/2018 Andrei Ramanovich - added PaymentsOffline setting
-- Updated: 03/05/2018 Igor Gaidukov - return rows which were changed
-- Updated: 03/28/2018 Igor Gaidukov - AzureSearch key is value from CHANGETABLE (fix for deletion of store)
-- Updated: 05/21/2018 Igor Gaidukov - added DisplayFullServiceDialog setting
-- Updated: 08/28/2018 Darya Batalava - added Store Custom Properties
-- Updated: 05/21/2019 Vadim Prikich - Currency code added only in StoreChanges
-- Updated: 09/09/2019 Vadim Prikich - Status added only in StoreChanges
-- Updated: 10/22/2019 Ingram Leonards - Status removed from StoreChanges
-- Updated: 11/13/2019 Richard Anderson - Added DeletedStores temp variable, Update return data, Add new column for SYS_CHANGE_OPERATION
-- Description: Check changes of tables related to FlatStore table and inset or update FlatStore if is needed (This SP used instead of triggers)
-- =============================================
CREATE PROCEDURE [dbo].[IUFlatStore]
(
   @Version INT = 1
)
AS
BEGIN
    SET NOCOUNT ON

	DECLARE @SuccessResult BIT = 0

	DECLARE @StoreServicesChanges table(
			[StoreID] [uniqueidentifier] NOT NULL,
			[ServiceID] [int] NOT NULL,
			[IsActive] [bit] NULL,
			[CreatedBy] [nvarchar](50) NULL,
			[CreatedOn] [datetime2](7) NULL,
			[UpdatedBy] [nvarchar](50) NULL,
			[UpdatedOn] [datetime2](7) NULL );

	DECLARE @CustomPropertiesChanges table(
			[TargetID] [uniqueidentifier] NOT NULL,
			[PropertyID] [int] NOT NULL,
			[Value] [nvarchar](MAX) NULL );

	DECLARE @HoursOfOperationChanges table(
		[StoreID] [uniqueidentifier] NOT NULL,
		[SundayOpen] [time](0) NULL,
		[SundayClose] [time](0) NULL,
		[MondayOpen] [time](0) NULL,
		[MondayClose] [time](0) NULL,
		[TuesdayOpen] [time](0) NULL,
		[TuesdayClose] [time](0) NULL,
		[WednesdayOpen] [time](0) NULL,
		[WednesdayClose] [time](0) NULL,
		[ThursdayOpen] [time](0) NULL,
		[ThursdayClose] [time](0) NULL,
		[FridayOpen] [time](0) NULL,
		[FridayClose] [time](0) NULL,
		[SaturdayOpen] [time](0) NULL,
		[SaturdayClose] [time](0) NULL,
		[IsActive] [bit] NULL,
		[CreatedBy] [nvarchar](50) NULL,
		[CreatedOn] [datetime2](7) NULL,
		[UpdatedBy] [nvarchar](50) NULL,
		[UpdatedOn] [datetime2](7) NULL	);

	DECLARE @FuelPriceChanges table(
		[StoreID] [uniqueidentifier] NOT NULL,
		[PCATSCode] [nvarchar](20) NOT NULL,
		[CashPrice] [smallmoney] NULL,
		[CreditPrice] [smallmoney] NULL,
		[UOM] [nvarchar](50) NOT NULL,
		[OctaneRating] [tinyint] NOT NULL,
		[IsActive] [bit] NOT NULL,
		[CreatedBy] [nvarchar](50) NULL,
		[CreatedOn] [datetime2](7) NOT NULL,
		[UpdatedBy] [nvarchar](50) NULL,
		[UpdatedOn] [datetime2](7) NULL,
		[DisplayName] [nvarchar](50) NULL,
		[Description] [nvarchar](max) NULL,
        [StandardProductCode] [nvarchar](150) NULL);

    DECLARE @PumpChanges table(
        [StoreId] [uniqueidentifier] NOT NULL,
	    [PumpNumber] [int] NOT NULL,
	    [ServiceLevel] [nvarchar](250) NOT NULL,
	    [PumpStatus] [nvarchar](250) NOT NULL);

    DECLARE @CarWashChanges table(
        [StoreID] [uniqueidentifier] NOT NULL,
	    [POSCode] [nvarchar](50) NOT NULL,
	    [Modifier] [nvarchar](10) NULL,
	    [POSCodeFormat] [nvarchar](10) NULL,
	    [Description] [nvarchar](max) NOT NULL,
	    [PaymentSystemProductCode] [nvarchar](50) NULL,
	    [Amount] [decimal](19, 4) NULL,
	    [CurrencyCode] [nvarchar](10) NULL,
	    [UnitOfMeasure] [nvarchar](20) NOT NULL,
	    [UnitPrice] [decimal](19, 4) NULL,
	    [TaxCode] [nvarchar](20) NULL,
	    [IsActive] [bit] NOT NULL,
	    [CreatedOn] [datetime2](7) NOT NULL,
	    [UpdatedOn] [datetime2](7) NULL,
	    [Quantity] [decimal](19, 4) NULL,
	    [MerchandiseCode] [nvarchar](150) NULL,
	    [SellingUnits] [bigint] NOT NULL,
	    [LineItemId] [nvarchar](150) NOT NULL,
	    [CarWashCode] [nvarchar](150) NULL,
	    [CarWashExpirationDate] [datetime] NULL,
	    [CarWashText] [nvarchar](max) NULL,
        [StandardProductCode] [nvarchar](150) NULL);
        
    DECLARE @LoyaltyServiceChanges table(
        [ProgramId] NVARCHAR(128) NOT NULL,
        [ProgramName] NVARCHAR(128) NOT NULL,
        [ServiceStatus] NVARCHAR(128) NOT NULL,
        [StoreId] UNIQUEIDENTIFIER NOT NULL);

	DECLARE @StoreChanges table(
		[StoreID] [uniqueidentifier] NOT NULL,
		[TenantID] [uniqueidentifier] NOT NULL,
		[Name] [nvarchar](256) NOT NULL,
		[MPPAID] [int] NOT NULL,
		[StoreNumber] [nvarchar](64) NULL,
		[FuelBrandID] [int] NOT NULL,
		[Phone] [nvarchar](20) NULL,
		[NumberOfPumps] [tinyint] NOT NULL,
		[RegularPCATSCode] [nvarchar](20) NULL,
		[MidgradePCATSCode] [nvarchar](20) NULL,
		[PremiumPCATSCode] [nvarchar](20) NULL,
		[DieselPCATSCode] [nvarchar](20) NULL,
		[E85PCATSCode] [nvarchar](20) NULL,
		[MerchantID] [nvarchar](50) NULL,
		[TimeZoneID] [int] NULL,
		[DisplayOnMobile] [bit] NOT NULL,
		[IsSiteBillable] [bit] NOT NULL,
		[CreatedBy] [nvarchar](50) NULL,
		[CreatedOn] [datetime2](7) NOT NULL,
		[UpdatedBy] [nvarchar](50) NULL,
		[UpdatedOn] [datetime2](7) NULL,
		[StoreInstallTypeID] [int] NULL,
		[StoreInstallDate] [datetime2](7) NULL,
		[StationImageID] [int] NULL,
		[SiteServiceID] [nvarchar](50) NULL,
		[ActiveAlertsMaintenanceMode] [bit] NOT NULL,
		[UnavailablePumps] [nvarchar](max) NULL,
		[StoreTenants] [nvarchar](max) NULL,
		[IsDeleted] [bit] NOT NULL,
		[ActiveAlertsIgnored] [bit] NOT NULL,
		[StreetAddress] [nvarchar](128) NOT NULL,
		[City] [nvarchar](64) NOT NULL,
		[StateCode] [char](2) NULL,
		[ZipCode] [nvarchar](16) NULL,
		[County] [nvarchar](100) NULL,
		[CountryIsoCode] [char](2) NOT NULL,
		[Latitude] [decimal](10, 7) NOT NULL,
		[Longitude] [decimal](10, 7) NOT NULL,
		[ContactInfo] [nvarchar](max) NULL,
		[IsBootstrapped] [bit] NOT NULL,
		[AllowInsidePayment] [bit] NOT NULL,
		[AllowOutsidePayment] [bit] NOT NULL,
		[EposIds] [nvarchar](max) NULL,
		[SiteUnavailablePumps] [nvarchar](max) NULL,
		[AllowMultipleLoyalty] BIT NULL,
		[PaymentsOffline] BIT NOT NULL,
		[DisplayFullServiceDialog] BIT NOT NULL,
		[StreetAddress2] [nvarchar](128) NULL,
        [FuelServiceStatus] [nvarchar](128) NOT NULL,
        [CarWashServiceStatus] [nvarchar](128) NOT NULL,
		[CurrencyCode] [nvarchar](3) NULL);

	DECLARE @DeletedStores table(
		[TenantID] [uniqueidentifier] NOT NULL,
		[StoreID] [uniqueidentifier] NOT NULL,
		[Name] [nvarchar](256) NOT NULL,
		[StoreNumber] [nvarchar](64) NULL,
		[MerchantId] [nvarchar](50) NULL,
		[Phone] [nvarchar](20) NULL,
		[NumberOfPumps] [int] NOT NULL,
		[Latitude] [decimal](10, 7) NOT NULL,
		[Longitude] [decimal](10, 7) NOT NULL,
		[StreetAddress] [nvarchar](128) NOT NULL,
		[City] [nvarchar](64) NOT NULL,
		[StateCode] [char](2) NULL,
		[PostalCode] [nvarchar](16) NULL,
		[CountryIsoCode] [char](2) NULL,
		[FuelBrand] [nvarchar](128) NOT NULL,
		[Services] [nvarchar](max) NULL,
		[FlatStoreID] [uniqueidentifier] NOT NULL,
		[DisplayOnMobile] [bit] NOT NULL,
		[County] [nvarchar](100) NULL,
		[MppaID] [nvarchar](50) NULL,
		[StoreInstallTypeID] [int] NULL,
		[OwnerID] [uniqueidentifier] NOT NULL,
		[IsOwnedByTenant] [bit] NOT NULL,
		[StationImageUrl] [nvarchar](256) NULL,
		[ActiveAlertsMaintenanceMode] [bit] NOT NULL,
		[UnavailablePumps] [nvarchar](max) NULL,
		[IsSiteBillable] [bit] NOT NULL,
		[FuelProducts] [nvarchar](max) NULL,
		[OperatingHours] [nvarchar](max) NULL,
		[ActiveAlertsIgnored] [bit] NOT NULL,
		[Geo] [geography] NOT NULL,
		[AllowInsidePayment] [bit] NOT NULL,
		[AllowOutsidePayment] [bit] NOT NULL,
		[IsBootstrapped] [bit] NOT NULL,
		[IsDeleted] [bit] NOT NULL,
		[EposIds] [nvarchar](max) NULL,
		[StateName] [nvarchar](64) NULL,
		[SiteUnavailablePumps] [nvarchar](max) NULL,
		[AllowMultipleLoyalty] [bit] NULL,
		[PaymentsOffline] [bit] NOT NULL,
		[DisplayFullServiceDialog] [bit] NOT NULL,
		[CustomProperties] [nvarchar](max) NULL,
		[CarWashItems] [nvarchar](max) NULL,
		[FuelingPoints] [nvarchar](max) NULL,
		[LoyaltyServices] [nvarchar](max) NULL,
		[StreetAddress2] [nvarchar](128) NULL,
		[FuelServiceStatus] [nvarchar](128) NOT NULL,
		[CarWashServiceStatus] [nvarchar](128) NOT NULL);
	
	INSERT INTO @StoreServicesChanges ([StoreID]
		   ,[ServiceID]
		   ,[IsActive]
		   ,[CreatedBy]
		   ,[CreatedOn]
		   ,[UpdatedBy]
		   ,[UpdatedOn])
	SELECT ISNULL(ss.[StoreID], CT.StoreID)
		   ,ISNULL(ss.[ServiceID], CT.ServiceID)
		   ,[IsActive]
		   ,[CreatedBy]
		   ,[CreatedOn]
		   ,[UpdatedBy]
		   ,[UpdatedOn] FROM[dbo].[StoreService] AS ss
	RIGHT OUTER JOIN CHANGETABLE(CHANGES[dbo].[StoreService], @version) AS CT 
		ON ss.StoreID = CT.StoreID AND ss.ServiceID = CT.ServiceID
	WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U' OR CT.SYS_CHANGE_OPERATION = 'D')

	INSERT INTO @CustomPropertiesChanges (
			[TargetID]
		   ,[PropertyID]
		   ,[Value])
	SELECT ISNULL(cpv.TargetID, CT.TargetID)
		   ,cpv.[PropertyID]
		   ,cpv.[Value]
		 FROM [dbo].[CustomPropertyValue] AS cpv
	RIGHT OUTER JOIN CHANGETABLE(CHANGES[dbo].[CustomPropertyValue], @version) AS CT 
		ON cpv.TargetID = CT.TargetID
	JOIN [dbo].[Store] as s ON cpv.[TargetID] = s.[StoreID]
	WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U' OR CT.SYS_CHANGE_OPERATION = 'D')
	
	INSERT INTO @HoursOfOperationChanges ([StoreID]
		   ,[SundayOpen]
		   ,[SundayClose]
		   ,[MondayOpen]
		   ,[MondayClose]
		   ,[TuesdayOpen]
		   ,[TuesdayClose]
		   ,[WednesdayOpen]
		   ,[WednesdayClose]
		   ,[ThursdayOpen]
		   ,[ThursdayClose]
		   ,[FridayOpen]
		   ,[FridayClose]
		   ,[SaturdayOpen]
		   ,[SaturdayClose]
		   ,[IsActive]
		   ,[CreatedBy]
		   ,[CreatedOn]
		   ,[UpdatedBy]
		   ,[UpdatedOn])
	SELECT ISNULL(ho.[StoreID], CT.StoreID)
		   ,[SundayOpen]
		   ,[SundayClose]
		   ,[MondayOpen]
		   ,[MondayClose]
		   ,[TuesdayOpen]
		   ,[TuesdayClose]
		   ,[WednesdayOpen]
		   ,[WednesdayClose]
		   ,[ThursdayOpen]
		   ,[ThursdayClose]
		   ,[FridayOpen]
		   ,[FridayClose]
		   ,[SaturdayOpen]
		   ,[SaturdayClose]
		   ,[IsActive]
		   ,[CreatedBy]
		   ,[CreatedOn]
		   ,[UpdatedBy]
		   ,[UpdatedOn] FROM[dbo].[HoursOfOperation] AS ho
	RIGHT OUTER JOIN CHANGETABLE(CHANGES[dbo].[HoursOfOperation], @version) AS CT 
		ON ho.StoreID = CT.StoreID
	WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U' OR CT.SYS_CHANGE_OPERATION = 'D')

	INSERT INTO @FuelPriceChanges
	SELECT fp.* FROM [dbo].[FuelPrice] AS fp
	RIGHT OUTER JOIN CHANGETABLE(CHANGES [dbo].[FuelPrice], @version) AS CT 
		ON fp.StoreID = CT.StoreID AND fp.PCATSCode = CT.PCATSCode
	WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U')

    INSERT INTO @PumpChanges
    SELECT p.*
    FROM [dbo].[Pump] AS p
    RIGHT OUTER JOIN CHANGETABLE(CHANGES [dbo].[Pump], @version) AS CT
        ON p.StoreId = CT.StoreID AND p.PumpNumber = CT.PumpNumber
    WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U')

    INSERT INTO @CarWashChanges
    SELECT cw.*
    FROM [dbo].[CarWash] AS cw
    RIGHT OUTER JOIN CHANGETABLE(CHANGES [dbo].[CarWash], @version) AS CT
        ON cw.StoreId = CT.StoreID AND cw.POSCode = CT.POSCode
    WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U')

    INSERT INTO @LoyaltyServiceChanges
    SELECT ls.*
    FROM [dbo].[LoyaltyService] AS ls
    RIGHT OUTER JOIN CHANGETABLE(CHANGES [dbo].[LoyaltyService], @version) AS CT
        ON ls.[ProgramId] = CT.[ProgramId]
    WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U')

	INSERT INTO @StoreChanges
	SELECT s.* FROM [dbo].[Store] AS s
	RIGHT OUTER JOIN CHANGETABLE(CHANGES [dbo].[Store], @version) AS CT 
		ON s.StoreID = CT.StoreID
	WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U')

	IF EXISTS(SELECT 1 FROM @StoreServicesChanges)
	BEGIN 
		PRINT 'StoreService need update'
		SET @SuccessResult = 1;
	
		UPDATE	fs
		SET		[Services] = dbo.GetStoreServiceArray(fs.StoreID)
		FROM	FlatStore fs
		INNER JOIN (SELECT DISTINCT StoreId FROM @StoreServicesChanges) c ON fs.StoreId = c.StoreId
	END

		IF EXISTS(SELECT 1 FROM @CustomPropertiesChanges)
	BEGIN 
		PRINT 'CustomProperty need update'
		SET @SuccessResult = 1;
	
		UPDATE	fs
		SET		[CustomProperties] = dbo.GetStoreCustomPropertiesArray(fs.StoreID)
		FROM	FlatStore fs
		INNER JOIN (SELECT DISTINCT TargetID FROM @CustomPropertiesChanges) c ON fs.StoreId = c.TargetId
	END


	IF EXISTS(SELECT 1 FROM @HoursOfOperationChanges)
	BEGIN 
		PRINT 'HoursOfOperation need update'
		SET @SuccessResult = 1;

		UPDATE	fs
		SET OperatingHours = [dbo].[GetOperatingHoursJsonByStoreID](fs.StoreId)
		FROM FlatStore fs
		INNER JOIN @HoursOfOperationChanges c ON fs.StoreId = c.StoreId
	END

	IF EXISTS(SELECT 1 FROM @FuelPriceChanges)
	BEGIN 
		PRINT 'FuelPrices need update'
		SET @SuccessResult = 1;

		UPDATE fs
		SET [FuelProducts] = fpd.Payload
		FROM [dbo].[FlatStore] fs
		INNER JOIN (
			SELECT 
			c.StoreId,
			dbo.GetFuelPricesJsonByStoreID(c.StoreId) as Payload
			FROM @FuelPriceChanges c
			Group by StoreId
		) fpd ON fs.[StoreID] = fpd.StoreId
	END

	IF EXISTS(SELECT 1 FROM @PumpChanges)
    BEGIN
        PRINT 'Pumps need update'
        SET @SuccessResult = 1;

        UPDATE fs
        SET [FuelingPoints] = [dbo].[GetPumpJsonByStoreID](fs.StoreID)
        FROM [dbo].[FlatStore] fs
        INNER JOIN @PumpChanges c ON fs.StoreId = c.StoreId
    END
    
    IF EXISTS (SELECT 1 FROM @CarWashChanges)
    BEGIN
        PRINT 'CarWash need update'
        SET @SuccessResult = 1;

        UPDATE fs
        SET [CarWashItems] = [dbo].[GetCarWashJsonByStoreID](fs.StoreID)
        FROM [dbo].[FlatStore] fs
        INNER JOIN @CarWashChanges c ON fs.StoreId = c.StoreId
    END

    IF EXISTS (SELECT 1 FROM @LoyaltyServiceChanges)
    BEGIN
        PRINT 'LoyaltyServices need update'
        SET @SuccessResult = 1;

        UPDATE fs
        SET [LoyaltyServices] = [dbo].[GetLoyaltyServicesJsonByStoreID](fs.StoreID)
        FROM [dbo].[FlatStore] fs
        INNER JOIN @LoyaltyServiceChanges ls ON fs.StoreId = ls.StoreId
    END
    
    IF EXISTS(SELECT 1 FROM @StoreChanges)
	BEGIN 
		PRINT 'Store need update'
		SET @SuccessResult = 1;

		MERGE dbo.FlatStore AS target
		USING (
			SELECT 
				StoreOverrides.ConsumerTenantID														AS TenantID, 
				c.StoreID																			AS StoreID,
				COALESCE(StoreOverrides.Name, c.Name)												AS Name, 
				COALESCE(StoreOverrides.StoreNumber, c.StoreNumber)									AS StoreNumber, 
				c.MerchantId																		AS MerchantID,
				c.Phone																				AS Phone,
				c.NumberOfPumps																		AS NumberOfPumps,
				c.Latitude																			AS Latitude,
				c.Longitude																			AS Longitude,
				c.StreetAddress																		AS StreetAddress,
                c.StreetAddress2                                                                    AS StreetAddress2,
                c.FuelServiceStatus                                                                 AS FuelServiceStatus,
                c.CarWashServiceStatus                                                              AS CarWashServiceStatus,
				c.City																				AS City,
				c.StateCode																			AS StateCode,
				c.ZipCode																			AS PostalCode,
				c.CountryIsoCode																	AS CountryIsoCode,
				FuelBrand.Name																		AS FuelBrand,
				COALESCE(StoreOverrides.DisplayOnMobile, c.DisplayOnMobile)							AS DisplayOnMobile, 
				c.County																			AS County,
				(SELECT FORMAT(c.MppaID, '0000-####'))												AS MppaID,
				c.StoreInstallTypeID																AS StoreInstallTypeID,
				c.TenantID																			AS OwnerID,
				si.ImageUrl																			AS StationImageUrl,
				COALESCE(StoreOverrides.AllowInsidePayment, c.AllowInsidePayment)					AS AllowInsidePayment, 
				COALESCE(StoreOverrides.AllowOutsidePayment, c.AllowOutsidePayment)					AS AllowOutsidePayment, 
				COALESCE(StoreOverrides.ActiveAlertsMaintenanceMode, c.ActiveAlertsMaintenanceMode)	AS ActiveAlertsMaintenanceMode, 
				c.UnavailablePumps																	AS UnavailablePumps,
				COALESCE(StoreOverrides.IsSiteBillable, c.IsSiteBillable)							AS IsSiteBillable, 
				COALESCE(StoreOverrides.ActiveAlertsIgnored, c.ActiveAlertsIgnored)					AS ActiveAlertsIgnored,
				c.IsDeleted																			AS IsDeleted,
				c.IsBootstrapped																	AS IsBootstrapped,
				c.EposIds																			AS EposIds,
				c.SiteUnavailablePumps																AS SiteUnavailablePumps,
				c.AllowMultipleLoyalty																AS AllowMultipleLoyalty,
				c.PaymentsOffline																	AS PaymentsOffline,
				c.DisplayFullServiceDialog															AS DisplayFullServiceDialog
			FROM @StoreChanges c
			CROSS APPLY OPENJSON(c.StoreTenants) 
						WITH (  
								ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID',   
								StoreNumber NVARCHAR(16) N'$.StoreNumber',  
								Name NVARCHAR(64) N'$.Name',  
								DisplayOnMobile BIT N'$.DisplayOnMobile',  
								StationImageID INT N'$.StationImageID',  
								AllowInsidePayment BIT N'$.AllowInsidePayment',  
								AllowOutsidePayment BIT N'$.AllowOutsidePayment',  
								ActiveAlertsMaintenanceMode BIT N'$.ActiveAlertsMaintenanceMode',  
								IsSiteBillable     BIT N'$.IsSiteBillable',  
								ActiveAlertsIgnored BIT N'$.ActiveAlertsIgnored'
							) 
			AS StoreOverrides
			JOIN FuelBrand ON FuelBrand.FuelBrandID = c.FuelBrandID
			LEFT JOIN AppFeatures.Stationimage si ON COALESCE(StoreOverrides.StationImageID, c.StationImageID) = si.StationImageID
			WHERE c.StoreTenants IS NOT NULL
			UNION
			SELECT c.TenantID, 
				c.StoreID, 
				c.Name, 
				StoreNumber, 
				MerchantID, 
				Phone, 
				NumberOfPumps, 
				Latitude, 
				Longitude, 
				StreetAddress, 
                StreetAddress2,
                FuelServiceStatus,
                CarWashServiceStatus,
				City, 
				StateCode, 
				ZipCode AS PostalCode, 
				CountryIsoCode, 
				FuelBrand.Name AS FuelBrand, 
				c.DisplayOnMobile, 
				County, 
				(SELECT FORMAT(c.MppaID, '0000-####')) AS MppaID, 
				StoreInstallTypeID, 
				c.TenantID AS OwnerID, 
				si.ImageUrl AS StationImageUrl, 
				AllowInsidePayment, 
				AllowOutsidePayment, 
				ActiveAlertsMaintenanceMode, 
				UnavailablePumps, 
				IsSiteBillable, 
				ActiveAlertsIgnored, 
				c.IsDeleted, 
				c.IsBootstrapped, 
				c.EposIds, 
				SiteUnavailablePumps,
				AllowMultipleLoyalty,
				PaymentsOffline,
				DisplayFullServiceDialog
			FROM @StoreChanges c
			JOIN FuelBrand ON FuelBrand.FuelBrandID = c.FuelBrandID
			LEFT JOIN AppFeatures.Stationimage si ON c.StationImageID = si.StationImageID
		) AS source
		ON (target.TenantID = source.TenantID AND target.StoreID = source.StoreID)
	
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(TenantID, 
					StoreID,
					Name,
					StoreNumber,
					MerchantID,	
					Phone,		  
					NumberOfPumps,
					Latitude,
					Longitude,
					StreetAddress,
                    StreetAddress2,
                    FuelServiceStatus,
                    CarWashServiceStatus,
                    City,
					StateCode,
					StateName,
					PostalCode,
					CountryIsoCode,
					FuelBrand,
					DisplayOnMobile,
					County,
					MppaID,
					StoreInstallTypeID,
					OwnerID,
					StationImageUrl,
					AllowInsidePayment,
					AllowOutsidePayment,
					ActiveAlertsMaintenanceMode,
					UnavailablePumps,
					IsSiteBillable,
					ActiveAlertsIgnored,
					FuelProducts,
                    FuelingPoints,
                    CarWashItems,
					OperatingHours,
					[Services],
					[Geo],
					IsDeleted,
					IsBootstrapped,
					EposIds,
					SiteUnavailablePumps,
					AllowMultipleLoyalty,
					PaymentsOffline,
					DisplayFullServiceDialog)
			VALUES(source.TenantID, 
				source.StoreID, 
				source.Name, 
				source.StoreNumber, 
				source.MerchantID, 
				source.Phone, 
				source.NumberOfPumps, 
				source.Latitude, 
				source.Longitude, 
				source.StreetAddress, 
                source.StreetAddress2,
                source.FuelServiceStatus,
                source.CarWashServiceStatus,
				source.City, 
				source.StateCode, 
				[dbo].[GetStateNameByCode] ( source.StateCode ),
				source.PostalCode, 
				source.CountryIsoCode, 
				source.FuelBrand, 
				source.DisplayOnMobile, 
				source.County, 
				source.MppaID, 
				source.StoreInstallTypeID,
				source.OwnerID, 
				source.StationImageUrl, 
				source.AllowInsidePayment, 
				source.AllowOutsidePayment, 
				source.ActiveAlertsMaintenanceMode, 
				source.UnavailablePumps, 
				source.IsSiteBillable, 
				source.ActiveAlertsIgnored, 
				dbo.GetFuelPricesJsonByStoreID(source.StoreID), 
                dbo.GetPumpJsonByStoreID(source.StoreID),
                dbo.GetCarWashJsonByStoreID(source.StoreID),
				dbo.GetOperatingHoursJsonByStoreID(source.StoreID), 
				dbo.GetStoreServiceArray(source.StoreID), 
				Common.GetGeographyPoint(source.Latitude, source.Longitude), 
				source.IsDeleted, 
				source.IsBootstrapped, 
				source.EposIds, 
				SiteUnavailablePumps,
				AllowMultipleLoyalty,
				PaymentsOffline,
				DisplayFullServiceDialog)

		WHEN MATCHED THEN
			UPDATE 
			SET	
				target.Name = source.Name, 
				target.StoreNumber = source.StoreNumber, 
				target.MerchantID = source.MerchantID, 
				target.Phone = source.Phone, 
				target.NumberOfPumps = source.NumberOfPumps, 
				target.Latitude = source.Latitude, 
				target.Longitude = source.Longitude, 
				target.StreetAddress = source.StreetAddress, 
                target.StreetAddress2 = source.StreetAddress2,
                target.FuelServiceStatus = source.FuelServiceStatus,
                target.CarWashServiceStatus = source.CarWashServiceStatus,
				target.City = source.City, 
				target.StateCode = source.StateCode,
				target.StateName = [dbo].[GetStateNameByCode] ( source.StateCode ),
				target.PostalCode = source.PostalCode, 
				target.CountryIsoCode = source.CountryIsoCode, 
				target.FuelBrand = source.FuelBrand, 
				target.DisplayOnMobile = source.DisplayOnMobile, 
				target.County = source.County, 
				target.MppaID = source.MppaID, 
				target.StoreInstallTypeID = source.StoreInstallTypeID, 
				target.OwnerID = source.OwnerID, 
				target.StationImageUrl = source.StationImageUrl, 
				target.AllowInsidePayment = source.AllowInsidePayment, 
				target.AllowOutsidePayment = source.AllowOutsidePayment, 
				target.ActiveAlertsMaintenanceMode = source.ActiveAlertsMaintenanceMode, 
				target.UnavailablePumps = source.UnavailablePumps, 
				target.IsSiteBillable = source.IsSiteBillable, 
				target.ActiveAlertsIgnored = source.ActiveAlertsIgnored,
				target.Geo = [Common].[GetGeographyPoint](source.Latitude, source.Longitude),
				target.IsDeleted = source.IsDeleted,
				target.IsBootstrapped = source.IsBootstrapped,
				target.EposIds = source.EposIds,
				target.SiteUnavailablePumps = source.SiteUnavailablePumps,
				target.AllowMultipleLoyalty = source.AllowMultipleLoyalty,
				target.PaymentsOffline = source.PaymentsOffline,
				target.DisplayFullServiceDialog = source.DisplayFullServiceDialog
			;

		INSERT INTO @DeletedStores
		SELECT *
		FROM dbo.FlatStore AS fs
		WHERE EXISTS (			
			SELECT StoreID, TenantID FROM
			(SELECT c.StoreID, StoreTenants.ConsumerTenantID AS TenantID FROM @StoreChanges c 
			CROSS APPLY OPENJSON(c.StoreTenants) 
						WITH (  
								ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID'   
							) 
					AS StoreTenants
			UNION
			SELECT c.StoreID, c.TenantID FROM @StoreChanges c
			) AS blah
			WHERE fs.StoreID = blah.StoreID AND 
			fs.TenantID NOT IN (SELECT StoreTenants.ConsumerTenantID AS TenantID FROM @StoreChanges c  
								CROSS APPLY OPENJSON(c.StoreTenants) 
											WITH (  
													ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID'   
												) 
										AS StoreTenants
								WHERE fs.StoreID = c.StoreID
								UNION
								SELECT c.TenantID FROM @StoreChanges c
								WHERE fs.StoreID = c.StoreID 
								))

		DELETE fs FROM dbo.FlatStore as fs 
		INNER JOIN @DeletedStores e
		ON fs.Storeid = e.Storeid and fs.TenantId = e.TenantId 
	END

	IF(@SuccessResult = 1)
	BEGIN
		SELECT 
		   LOWER(CONVERT(nvarchar(50), CT.[FlatStoreID] )) AS [FlatStoreID]  
		  ,ISNULL(fs.[TenantID], ds.[TenantID]) as [TenantID]
		  ,ISNULL(fs.[StoreID], ds.[StoreID]) as [StoreID]
		  ,fs.[Name]
		  ,ISNULL(fs.[StoreNumber], ds.[StoreNumber]) as [StoreNumber]
		  ,fs.[MerchantId]
		  ,fs.[Phone]
		  ,CONVERT(NVARCHAR(32), fs.[NumberOfPumps]) AS [NumberOfPumps]
		  ,CONVERT(NVARCHAR(50), fs.[Latitude]) AS [Latitude]
		  ,CONVERT(NVARCHAR(50), fs.[Longitude]) AS [Longitude]
		  ,fs.[StreetAddress]
          ,fs.[StreetAddress2]
          ,fs.[FuelServiceStatus]
          ,fs.[CarWashServiceStatus]
		  ,fs.[City]
		  ,fs.[StateCode]
		  ,fs.[PostalCode]
		  ,fs.[CountryIsoCode]
		  ,fs.[FuelBrand]
		  ,fs.[Services]
		  ,fs.[DisplayOnMobile]
		  ,fs.[County]
		  ,fs.[MppaID]
		  ,fs.[StoreInstallTypeID]
		  ,fs.[OwnerID]
		  ,fs.[IsOwnedByTenant]
		  ,fs.[StationImageUrl]
		  ,fs.[ActiveAlertsMaintenanceMode]
		  ,fs.[UnavailablePumps]
		  ,fs.[IsSiteBillable]
		  ,fs.[FuelProducts]
          ,fs.[FuelingPoints]
          ,fs.[CarWashItems]
          ,fs.[LoyaltyServices]
		  ,fs.[OperatingHours]
		  ,fs.[ActiveAlertsIgnored]
		  ,CONVERT([varbinary](MAX), fs.[Geo]) AS [Geo] 
		  ,fs.[AllowInsidePayment]
		  ,fs.[AllowOutsidePayment]
		  ,fs.[IsBootstrapped]
		  ,fs.[IsDeleted]
		  ,fs.[EposIds]
		  ,fs.[StateName]
		  ,fs.[SiteUnavailablePumps]
		  ,fs.[AllowMultipleLoyalty]
		  ,fs.[PaymentsOffline]
		  ,fs.[DisplayFullServiceDialog]
		  ,fs.[CustomProperties]
		  ,CT.SYS_CHANGE_OPERATION AS [ChangeOperation]
		FROM [dbo].[FlatStore] fs
		RIGHT OUTER JOIN CHANGETABLE(CHANGES[dbo].[FlatStore], @Version) AS CT 
		ON fs.FlatStoreID = CT.FlatStoreID
		LEFT JOIN @DeletedStores ds ON ds.FlatStoreID = LOWER(CONVERT(nvarchar(50), CT.[FlatStoreID] ))
		WHERE (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U' OR CT.SYS_CHANGE_OPERATION = 'D')
	END
END
