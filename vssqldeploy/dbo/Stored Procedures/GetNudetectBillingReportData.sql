﻿-- =============================================
-- Author:      
-- Create date: 09/12/2018 - Vadim Prikich 
-- Description: Get Nudetect Billing report.  
-- =============================================

CREATE PROCEDURE [dbo].[GetNudetectBillingReportData]
	@TenantID UNIQUEIDENTIFIER
	, @FromDate DATETIME2
	, @ToDate DATETIME2
	, @Offset INT
	, @Limit INT
	
AS 
BEGIN
SET NOCOUNT ON;	
	DECLARE @TempTable table(
		TenantID UNIQUEIDENTIFIER NOT NULL,
		TenantName NVARCHAR(255),
		StoreID UNIQUEIDENTIFIER NOT NULL,
		AboveSiteAuthAttemptsCnt INT,
		TotalAuthAttemptsCnt INT
		);
	
WITH StoreData AS (SELECT t.TenantID
				, t.Name AS TenantName
				, fs.IsOwnedByTenant
				, fs.OwnerID
				, fs.StoreID
				, fs.Name AS StoreName	
				, fs.StoreNumber	
				, fs.StreetAddress
				, fs.City
				, fs.StateCode
				, fs.PostalCode AS ZipCode 
				, s.StoreInstallDate
				, case when StoreInstallDate >= @FromDate AND StoreInstallDate <= @ToDate then CAST(1 as BIT) else CAST(0 AS BIT) end as InstalledDuringFilteredPeriod
				, fs.IsSiteBillable
	
							  
				, ISNULL((SELECT COUNT(*) 
							FROM dbo.Basket b
							JOIN dbo.BasketPayment bp ON bp.BasketID = b.BasketID
							WHERE b.StoreID = fs.StoreID 
								AND b.StoreTenantID = fs.TenantID 
								AND b.CreatedOn >= @FromDate AND b.CreatedOn <=  @ToDate 
								AND b.IsSiteBillable = 1 AND ( (s.StoreInstallDate >= @FromDate AND s.StoreInstallDate <= @ToDate AND fs.IsOwnedByTenant = 1) OR fs.IsSiteBillable = 1 )
								AND NOT EXISTS (SELECT 1 FROM BasketPayment bpp WHERE b.BasketID = bpp.BasketID AND bpp.IsSiteLevelPayment = 1 )
								AND ((bp.PaymentCommand = 'Authorize' AND bp.ResponseResult IN ('Approved','Declined')))
							)
							, 0) AS AboveSiteAuthAttemptsCnt
	   
			FROM FlatStore fs
			JOIN dbo.Store s ON fs.StoreID = s.StoreID
			JOIN dbo.Tenant t ON t.TenantID = fs.TenantID
			), 
   
   
TenantStatistics AS (SELECT SUM(AboveSiteAuthAttemptsCnt) as TotalAuthAttemptsCnt FROM StoreData )

	INSERT INTO @TempTable (TenantID, TenantName, StoreID, AboveSiteAuthAttemptsCnt, TotalAuthAttemptsCnt)
						  
		SELECT TenantID, TenantName, StoreID, AboveSiteAuthAttemptsCnt, TotalAuthAttemptsCnt
		FROM StoreData, TenantStatistics
		WHERE ( (StoreInstallDate >= @FromDate AND StoreInstallDate <= @ToDate AND IsOwnedByTenant = 1) OR IsSiteBillable = 1 )

SELECT AllTenantReport.TenantID, TenantName, TotalAuthAttemptsCnt, tf.FeatureStringsId, tf.FeatureString 
FROM 
(SELECT TenantID,TenantName,
SUM (AboveSiteAuthAttemptsCnt) AS TotalAuthAttemptsCnt
FROM @TempTable GROUP BY TenantID, TenantName) AS AllTenantReport

LEFT JOIN [AppFeatures].[TenantFeatures] AS tf ON AllTenantReport.TenantID = tf.TenantID AND tf.FeatureStringsId = 'FEATURE_NUDATA_ENABLED'
WHERE (@TenantID IS NULL OR AllTenantReport.TenantID = @TenantID) and
((tf.FeatureString IS NULL AND EXISTS(SELECT 1 FROM [AppFeatures].[FeatureStrings] WHERE FeatureStringsId = 'FEATURE_NUDATA_ENABLED' AND FeatureString = 'true'))
OR 
(tf.FeatureString IS NOT NULL AND tf.FeatureString = 'true'))


END

