﻿CREATE PROCEDURE [dbo].[GetProvisionalSiteConfiguration]
	@TenantID UNIQUEIDENTIFIER,
	@StoreID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @AesKey VARCHAR(128) = [dbo].[GetGeneratedStoreKey] (@StoreID);
	DECLARE @XSLT_Blob_Name NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@StoreID, 'mppa_xslt', DEFAULT);
	DECLARE @Endpoint_Type NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@StoreID, 'Endpoint.Type', DEFAULT);
	DECLARE @ApiVersion NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@TenantID, 'Endpoint.API.Version', DEFAULT);
	DECLARE @UriTemplateEndpoint NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@TenantID, 'Endpoint.Uri.Template', DEFAULT);
	DECLARE @SBR_EndpointNamespace NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@TenantID, 'EndpointAddress', DEFAULT);
	DECLARE @SBR_AccountKey NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@TenantID, 'SBR_AccountKey', DEFAULT);
	DECLARE @SBR_AccountName NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@TenantID, 'SBR_AccountName', DEFAULT);
	DECLARE @Currency_Code NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@TenantID, 'Region.Code.Currency', DEFAULT);
	DECLARE @Language_Code NVARCHAR(250) = [dbo].[GetTenantPropertyValue] (@TenantID, 'Region.Code.Language', DEFAULT);

	SELECT 
			MPPAID
		  , @AesKey as AesKey
		  , @XSLT_Blob_Name as XSLTBlobName
		  , @Endpoint_Type as EndpointType
		  , @ApiVersion as APIVersion
		  , @UriTemplateEndpoint as UriEndpointTemplate
		  , @SBR_EndpointNamespace as SBREndpointNamespace
		  , @SBR_AccountKey as SBRAccountKey
		  , @SBR_AccountName as SBRAccountName
		  , @Currency_Code as CurrencyCode
		  , @Language_Code as LanguageCode
		  FROM [dbo].[Store]
		  WHERE StoreID = @StoreID AND
				TenantID = @TenantID 

	
END
