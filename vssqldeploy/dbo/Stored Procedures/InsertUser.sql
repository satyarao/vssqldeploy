﻿
-- =============================================
-- Author:      Roman Lavreniuk
-- Create date: 11/13/2018
-- Update date: 12/7/2018 Andrei Ramanovich - Add @Providers
-- Update date: 12/13/2018 Igor Kutsenka - Added @TenantObjectID
-- Update date: 06/10/2019 Vadim Prikich - Added @CreateOn 
-- Description: https://p97networks.atlassian.net/browse/PLAT-4144 
-- =============================================
CREATE PROC [dbo].[InsertUser]
    @UserID UNIQUEIDENTIFIER
  , @TenantID UNIQUEIDENTIFIER
  , @EmailAddress NVARCHAR(255)
  , @EmailAddressHash CHAR(44)
  , @Name NVARCHAR(255) = NULL
  , @FirstName NVARCHAR(255) = NULL
  , @LastName NVARCHAR(255) = NULL
  , @MobileNumber NVARCHAR(50) = NULL
  , @Providers NVARCHAR(max) = NULL
  , @CreatedOn DateTime2 = null
AS 
BEGIN
    SET NOCOUNT ON

    BEGIN TRY
        
        IF EXISTS ( SELECT  UserID
                    FROM    dbo.UserInfo
                    WHERE   UserID = @UserID )
            BEGIN
                UPDATE  dbo.UserInfo
                SET     EmailAddress = @EmailAddress
                      , EmailAddressHash = @EmailAddressHash
                      , Name = ISNULL(@Name, Name)
                      , FirstName = ISNULL(@FirstName, FirstName)
                      , LastName = ISNULL(@LastName, LastName) 
                      , MobileNumber = ISNULL(@MobileNumber, MobileNumber) 
                      , IsActive = 1
                      , UpdatedBy = 'Reg'
                      , UpdatedOn = GETUTCDATE()
                      , Providers = ISNULL(@Providers, Providers)
                      , IsDeleted = 0
                WHERE   UserID = @UserID
            END 
        ELSE
            BEGIN
                BEGIN TRAN
                INSERT  INTO dbo.UserInfo
                        ( UserID
                        , TenantID
                        , EmailAddress
                        , EmailAddressHash
                        , Name
                        , FirstName
                        , LastName
                        , MobileNumber
                        , Providers
                        , CreatedOn
                        , CreatedBy
                        , IsActive
                        )
                VALUES  ( @UserID
                        , @TenantID
                        , @EmailAddress
                        , @EmailAddressHash
                        , @Name
                        , @FirstName
                        , @LastName
                        , @MobileNumber
                        , @Providers
                        , ISNULL(@CreatedOn, GETUTCDATE())
                        , 'Reg'
                        , 1
                        )

                EXEC dbo.IUDUserPreference @UserID = @UserID

                COMMIT TRANSACTION
                BEGIN TRY
                    EXEC [Payment].[AutoVerifiedFunding] @UserID = @UserID, @TenantID = @TenantID, @EmailAddress = @EmailAddress
                END TRY
                BEGIN CATCH
                    --Do Nothing
                END CATCH
            END
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
        DECLARE @ErrorMessage nvarchar(4000) = ERROR_MESSAGE(),
            @ErrorNumber int = ERROR_NUMBER(),
            @ErrorSeverity int = ERROR_SEVERITY(),
            @ErrorState int = ERROR_STATE(),
            @ErrorLine int = ERROR_LINE(),
            @ErrorProcedure nvarchar(200) = ISNULL(ERROR_PROCEDURE(), '-');
        SET @ErrorMessage = N'Error: %d, Level: %d, State: %d, Procedure: %s, Line: %d, ' + 'Message: ' + @ErrorMessage;
        RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine)
    END CATCH
END
