﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 2018-04-04
-- Description:	Check if tenant has specified parent tenant
-- =============================================
CREATE PROCEDURE [dbo].[IsChildTenant]
	@ParentTenantID UNIQUEIDENTIFIER,
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT CAST(child.OrgID.IsDescendantOf(parent.OrgID) AS bit)
	FROM dbo.Tenant child
	CROSS APPLY dbo.Tenant parent
	WHERE child.TenantID = @TenantID AND parent.TenantID = @ParentTenantID
END
