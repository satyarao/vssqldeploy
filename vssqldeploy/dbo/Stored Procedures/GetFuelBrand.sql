﻿CREATE PROCEDURE [dbo].[GetFuelBrand]
AS 
SELECT	FuelBrandID AS Id
	, Name
FROM	dbo.FuelBrand
WHERE IsActive = 1;

--EXEC dbo.GetFuelBrand
