﻿
-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 10/31/2016
-- Update date: 12/14/2016 removed Fax column
-- Updated: 07/25/2017 Igor Gaidukov - use AllowInsidePayment and AllowOutsidePayment instead of IsMobilePaymentsEnabled
-- Updated: 08/07/2017  Andrei Ramanovich - add IsDeleted
-- Updated: 09/26/2017  Alex Goroshko - added EposIds, removed NumberOfPumps and UnavailablePumps
-- Updated: 10/02/2017 Igor Gaidukov - use DisplayOnMobile instead of IsActive
-- Updated: 10/03/2017 Igor Gaidukov - deleted DisplayOnMobile, AllowInsidePaymentn AllowOutsidePayment settings for partners (partners does not override this settings)
-- Updated: 12/19/2017 Igor Gaidukov - added AllowMultipleLoyalty setting
-- Updated: 05/21/2018 Igor Gaidukov - added DisplayFullServiceDialog setting
-- Description:	Update general settings of store
-- =============================================
CREATE PROCEDURE [dbo].[UpdateGeneralStoresSettings]
	@TenantID UNIQUEIDENTIFIER,
	@StoreID UNIQUEIDENTIFIER,
	@FuelBrandID INT,
	@StationImageID INT,
	@Name NVARCHAR(256),
	@StoreNumber NVARCHAR(64),
	@Phone NVARCHAR(20),
	@Fax NVARCHAR(20),
	@DisplayOnMobile BIT,
	@IsDeleted BIT,
	@EposIds NVARCHAR(MAX),
	@AllowMultipleLoyalty BIT,
	@DisplayFullServiceDialog BIT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OwnerID UNIQUEIDENTIFIER

	SELECT @OwnerID = TenantID
	FROM dbo.Store WHERE StoreID = @StoreID

	IF (@OwnerID = @TenantID)
		BEGIN
			UPDATE [dbo].[Store]
			SET [FuelBrandID] = @FuelBrandID,
				[StationImageID] = @StationImageID,
				[Name] = @Name,
				[StoreNumber] = @StoreNumber,
				[Phone] = @Phone,
				[EposIds] = @EposIds,
				[AllowMultipleLoyalty] = @AllowMultipleLoyalty,
				[DisplayFullServiceDialog] = @DisplayFullServiceDialog,
				[DisplayOnMobile] = @DisplayOnMobile,
				[IsDeleted] = @IsDeleted,
				[UpdatedOn] = GETUTCDATE(),
				[UpdatedBy] = 'SP UpdateGeneralStoresSettings'
			WHERE StoreID = @StoreID
		END
	ELSE 
		BEGIN
			DECLARE @StoreOverrides table(
				TenantID UNIQUEIDENTIFIER,
				StoreNumber NVARCHAR(64),   
				Name NVARCHAR(64),  
				StationImageID INT,  
				ActiveAlertsMaintenanceMode BIT,  
				IsSiteBillable     BIT ,  
				ActiveAlertsIgnored BIT
			);
			
			INSERT INTO @StoreOverrides (TenantID, StoreNumber, Name, StationImageID, ActiveAlertsMaintenanceMode, IsSiteBillable, ActiveAlertsIgnored)
			SELECT ConsumerTenantID, StoreNumber, Name, StationImageID, ActiveAlertsMaintenanceMode, IsSiteBillable, ActiveAlertsIgnored
			FROM OPENJSON((SELECT StoreTenants FROM dbo.Store WHERE StoreID = @StoreID)) 
				WITH (  
						ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID',   
						StoreNumber NVARCHAR(64) N'$.StoreNumber',  
						Name NVARCHAR(64) N'$.Name',  
						StationImageID INT N'$.StationImageID',  
						ActiveAlertsMaintenanceMode BIT N'$.ActiveAlertsMaintenanceMode',  
						IsSiteBillable     BIT N'$.IsSiteBillable',  
						ActiveAlertsIgnored BIT N'$.ActiveAlertsIgnored'						)
				
			UPDATE @StoreOverrides
			SET [StoreNumber] = @StoreNumber
			WHERE TenantID = @TenantID

			UPDATE [dbo].[Store]
			SET [FuelBrandID] = @FuelBrandID,
				[StationImageID] = @StationImageID,
				[Phone] = @Phone,
				[EposIds] = @EposIds,
				[AllowMultipleLoyalty] = @AllowMultipleLoyalty,
				[DisplayFullServiceDialog] = @DisplayFullServiceDialog,
				[DisplayOnMobile] = @DisplayOnMobile, 
				[StoreTenants] = (SELECT * FROM @StoreOverrides FOR JSON PATH) ,
				[UpdatedOn] = GETUTCDATE(),
				[UpdatedBy] = 'SP UpdateGeneralStoresSettings'
			WHERE StoreID = @StoreID
		END
END
