﻿
-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 6/06/2018
-- Description:	Update user location based on his transactions for the last 6 months
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserLocation] 
	@UserID uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TransactionsTimeLimit DATETIME2(7) = DATEADD(Month, -6, GETUTCDATE());
	DECLARE @UpdateTimeLimit DATETIME2(7) = DATEADD(Month, -1, GETUTCDATE());
	
	UPDATE ui
	SET ui.[CountryIsoCode] = loc.CountryIsoCode, ui.[StateCode] = loc.StateCode, ui.[CountryUpdatedOn] = GETUTCDATE()
	FROM [dbo].[UserInfo] ui
	CROSS APPLY
	(
		SELECT TOP(1) fb.[UserID], fs.CountryIsoCode, fs.StateCode, COUNT(*) as Count
		FROM [dbo].[FlatBasket] fb
		JOIN [dbo].[FlatStore] fs ON fs.[StoreID] = fb.[StoreID] AND fs.[IsOwnedByTenant] = 1
		WHERE ui.[UserID] = fb.[UserID] AND fb.POSDateTime >= @TransactionsTimeLimit
		GROUP BY fb.[UserID], fs.CountryIsoCode, fs.StateCode
		ORDER BY COUNT(*) DESC
	) loc
	WHERE (@UserID IS NOT NULL OR ui.[CountryUpdatedOn] IS NULL OR ui.[CountryUpdatedOn] <= @UpdateTimeLimit)
		AND (@UserID IS NULL OR ui.[UserID] = @UserID)
END

