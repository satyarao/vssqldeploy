﻿CREATE PROCEDURE [dbo].[IUDMCXLocationMapping]
	@StoreID UNIQUEIDENTIFIER,
	@MCXFuelLocationId NVARCHAR(50) = NULL,
	@MCXPOSLocationId NVARCHAR(50) = NULL,
	@Delete BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION
		IF(@Delete = 1)
		BEGIN
			DELETE FROM [dbo].[MCX_LocationMapping] WHERE StoreID = @StoreID
		END
		ELSE
		BEGIN
			IF(EXISTS(SELECT TOP 1 * FROM [dbo].[MCX_LocationMapping] WHERE StoreID = @StoreID))
			BEGIN
				UPDATE [dbo].[MCX_LocationMapping]
				SET MCXFuelLocationId = @MCXFuelLocationId,
					MCXPOSLocationId = @MCXPOSLocationId
				WHERE StoreID = @StoreID
			END
			ELSE
			BEGIN
				IF (NOT (@MCXFuelLocationId IS NULL OR @MCXPOSLocationId IS NULL))
				BEGIN
					INSERT INTO [dbo].[MCX_LocationMapping]
					   ([StoreID]
					   ,[MCXFuelLocationId]
					   ,[MCXPOSLocationId])
				VALUES
					   (@StoreID
					   ,@MCXFuelLocationId
					   ,@MCXPOSLocationId)
				END
			END
		END
		COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(4000) = ERROR_MESSAGE(),
		@ErrorNumber int = ERROR_NUMBER(),
		@ErrorSeverity int = ERROR_SEVERITY(),
		@ErrorState int = ERROR_STATE(),
		@ErrorLine int = ERROR_LINE(),
		@ErrorProcedure nvarchar(200) = ISNULL(ERROR_PROCEDURE(), '-');
		SET @ErrorMessage = N'Error: %d, Level: %d, State: %d, Procedure: %s, Line: %d, ' + 'Message: ' + @ErrorMessage;
		ROLLBACK
		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine)
	END CATCH
END
