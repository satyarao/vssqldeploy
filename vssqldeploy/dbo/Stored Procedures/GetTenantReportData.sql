﻿
-- =============================================
-- Author: Igor Gaidukov   
-- Create date: 05/13/2018
-- Update date: 07/17/2018 Igor Kutesnka - TenantObjectID
-- Update date: 09/9/2019 Andrei Ramanovich - use UserIdentity instead of ObjectID
-- Description:   Get tenant billing report data
-- =============================================
CREATE PROCEDURE [dbo].[GetTenantReportData]
	  @TenantID UNIQUEIDENTIFIER = NULL
	, @FromDate DATETIME2
	, @ToDate DATETIME2
	, @Offset INT
	, @Limit INT
	AS 
	BEGIN
		WITH BillableStoresCnt AS
		(
			SELECT TenantID, 
				COUNT(*) AS [Count]
			FROM dbo.Store s
			WHERE (@TenantID IS NULL OR s.TenantID = @TenantID) AND  IsSiteBillable = 1
			GROUP BY TenantID	
		)
		,AuthenticationsCnt AS
		(
			SELECT TenantID, 
				SUM(RefreshTokenCnt) AS [RefreshTokenCnt],
				SUM(AuthorizationCodeCnt) AS [AuthorizationCodeCnt]
			FROM [AccessControl].[B2CAuthentication] b2ca
			WHERE (@TenantID IS NULL OR b2ca.TenantID = @TenantID) AND b2ca.[Date] >= @FromDate AND b2ca.[Date] <= @ToDate
			GROUP BY TenantID
		
		), TempTenantReport AS 
		( 
			SELECT t.[TenantID] AS TenantID,
				t.[Name] as TenantName,
				ISNULL((
						SELECT COUNT(*) / bsc.Count 
						FROM dbo.UserInfo ui JOIN AccessControl.UserIdentity aui ON aui.UserID = ui.UserID AND (aui.ProviderID = 1 OR aui.ProviderID = 2)
						WHERE ui.TenantID = t.TenantID
						), 0) AS AvgUsersPerStore,
				ISNULL( b2ca.RefreshTokenCnt / bsc.Count, 0) AS AvgRefreshTokenPerStore,
				ISNULL( b2ca.AuthorizationCodeCnt / bsc.Count, 0) AS AvgAuthorizationCodePerStore
			FROM [dbo].[Tenant] t
			LEFT JOIN AuthenticationsCnt b2ca ON b2ca.TenantID = t.TenantID
			JOIN BillableStoresCnt bsc ON bsc.TenantID = t.TenantID
			WHERE @TenantID IS NULL OR t.TenantID = @TenantID
		), TempCount AS (SELECT COUNT(*) AS Total FROM TempTenantReport)

		SELECT TenantId,
			TenantName,
			AvgUsersPerStore,
			AvgAuthorizationCodePerStore,
			AvgRefreshTokenPerStore,
			(SELECT Total FROM TempCount) AS Total
		FROM TempTenantReport tr
		ORDER BY TenantName ASC
		OFFSET @Offset ROWS 
		FETCH NEXT @Limit ROWS ONLY
	END
