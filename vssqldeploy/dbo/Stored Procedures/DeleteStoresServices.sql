﻿
CREATE PROC [dbo].[DeleteStoresServices]
	@StoresIds ListOfGuid READONLY
AS
SET NOCOUNT ON
UPDATE dbo.StoreService
SET  IsActive  = 0
	,UpdatedOn = GETUTCDATE()
WHERE StoreID IN (SELECT ID FROM @StoresIds)
