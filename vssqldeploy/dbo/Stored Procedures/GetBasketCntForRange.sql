﻿-- =============================================
-- Update date: 02/21/2017 Igor gaidukov - SELECT from FlatStore (is needed for partners tenant), select only billable stores/transactions
-- Update date: 05/29/2017 Igor Gaidukov - use IsSiteBillable instead of IsPaymentBillable (for Basket table)
-- Update date: 08/16/2017 Jamy Ryals - Fix TenantId Issue
-- Update date: 12/13/2017 Igor Gaidukov - Transaction Report respect DisplayOnMobile instead of IsSiteBillable
-- Update date: 1/10/2018  Alex Goroshko - using outer apply instead of cross apply 
-- Update date: 3/07/2019  Davydovich Raman - try to improve perfomance
-- =============================================
CREATE PROC [dbo].[GetBasketCntForRange]
		@TenantIDs ListOfGuid READONLY
		, @FromDate DATETIME2
		, @ToDate DATETIME2
		, @Offset INT
	    , @Limit INT
AS 

SELECT t.TenantID
	, t.Name AS TenantName
	, lksit.StoreInstallTypeName
	, s.StoreInstallDate
	, fs.StoreID
	, fs.Name AS StoreName
	, fs.StreetAddress
	, fs.City
	, fs.StateCode
	, fs.PostalCode AS ZipCode 
	, ISNULL((SELECT COUNT(*) 
	FROM dbo.Basket b
	OUTER APPLY
	(
		SELECT pt.PartnerTenantId
		FROM Reports.FlatBasket_VisibleByTenants pt
		WHERE pt.TransactionID = b.BasketID

	) AS VisibleByTenants
	WHERE b.storeid = fs.storeid
	AND ( b.StoreTenantID = fs.TenantID OR  b.AppTenantID = fs.TenantID OR VisibleByTenants.PartnerTenantId = fs.TenantID)
	AND b.CreatedOn >= @FromDate
	AND b.CreatedOn <=  @ToDate
	AND b.POSOperatorID != 'PZOutside' 
	And b.POSOperatorID != 'Outside' 
	AND BasketStateID = 3),0) AS InsideCnt
, ISNULL((SELECT SUM(b.Total) 
	FROM dbo.Basket b
	OUTER APPLY
	(
		SELECT pt.PartnerTenantId
		FROM Reports.FlatBasket_VisibleByTenants pt
		WHERE pt.TransactionID = b.BasketID

	) AS VisibleByTenants
	WHERE b.storeid = fs.storeid
	AND ( b.StoreTenantID = fs.TenantID OR  b.AppTenantID = fs.TenantID OR VisibleByTenants.PartnerTenantId = fs.TenantID)
	AND b.CreatedOn >= @FromDate
	AND b.CreatedOn <=  @ToDate
	AND b.POSOperatorID != 'PZOutside' 
	And b.POSOperatorID != 'Outside' 
	AND BasketStateID = 3),0) AS InsideTotal
, ISNULL((SELECT COUNT(*) 
	FROM dbo.Basket b
	OUTER APPLY
	(
		SELECT pt.PartnerTenantId
		FROM Reports.FlatBasket_VisibleByTenants pt
		WHERE pt.TransactionID = b.BasketID

	) AS VisibleByTenants
	WHERE b.storeid = fs.storeid
	AND ( b.StoreTenantID = fs.TenantID OR  b.AppTenantID = fs.TenantID OR VisibleByTenants.PartnerTenantId = fs.TenantID)
	AND b.CreatedOn >= @FromDate
	AND b.CreatedOn <=  @ToDate
	AND (b.POSOperatorID = 'PZOutside' OR b.POSOperatorID = 'Outside')
	AND BasketStateID = 3),0) AS OutsideCnt
, ISNULL((SELECT SUM(b.Total) 
	FROM dbo.Basket b
	OUTER APPLY
	(
		SELECT pt.PartnerTenantId
		FROM Reports.FlatBasket_VisibleByTenants pt
		WHERE pt.TransactionID = b.BasketID

	) AS VisibleByTenants
	WHERE b.storeid = fs.storeid
	AND ( b.StoreTenantID = fs.TenantID OR  b.AppTenantID = fs.TenantID OR VisibleByTenants.PartnerTenantId = fs.TenantID)
	AND b.CreatedOn >= @FromDate
	AND b.CreatedOn <=  @ToDate
	AND (b.POSOperatorID = 'PZOutside' OR b.POSOperatorID = 'Outside')
	AND BasketStateID = 3),0) AS OutsideTotal
, ISNULL((SELECT COUNT(*) 
	FROM dbo.Basket b
	OUTER APPLY
	(
		SELECT pt.PartnerTenantId
		FROM Reports.FlatBasket_VisibleByTenants pt
		WHERE pt.TransactionID = b.BasketID

	) AS VisibleByTenants
	WHERE b.storeid = fs.storeid
	AND ( b.StoreTenantID = fs.TenantID OR  b.AppTenantID = fs.TenantID OR VisibleByTenants.PartnerTenantId = fs.TenantID)
	AND b.CreatedOn >= @FromDate
	AND b.CreatedOn <=  @ToDate
	AND BasketStateID = 3),0) AS Cnt
, ISNULL((SELECT SUM(b.Total) 
	FROM dbo.Basket b
	OUTER APPLY
	(
		SELECT pt.PartnerTenantId
		FROM Reports.FlatBasket_VisibleByTenants pt
		WHERE pt.TransactionID = b.BasketID

	) AS VisibleByTenants
	WHERE b.storeid = fs.storeid
		AND ( b.StoreTenantID = fs.TenantID OR  b.AppTenantID = fs.TenantID OR VisibleByTenants.PartnerTenantId = fs.TenantID)
		AND b.CreatedOn >= @FromDate
		AND b.CreatedOn <=  @ToDate
		AND BasketStateID = 3),0) AS SumBasketTotal
FROM FlatStore fs
JOIN dbo.Tenant t ON t.TenantID = fs.TenantID
JOIN dbo.Store s ON fs.StoreID = s.StoreID
LEFT JOIN dbo.LKStoreInstallType lksit ON lksit.StoreInstallTypeID = fs.StoreInstallTypeID
WHERE (NOT EXISTS (SELECT 1 FROM @TenantIDs) OR fs.TenantID IN (SELECT ID FROM @TenantIDs)) AND fs.DisplayOnMobile = 1
ORDER BY t.Name
OFFSET @Offset ROWS 
FETCH NEXT @Limit ROWS ONLY

SELECT COUNT(*) as Total
FROM FlatStore fs
JOIN dbo.Tenant t ON t.TenantID = fs.TenantID
JOIN dbo.Store s ON fs.StoreID = s.StoreID
LEFT JOIN dbo.LKStoreInstallType lksit ON lksit.StoreInstallTypeID = fs.StoreInstallTypeID
WHERE (NOT EXISTS (SELECT 1 FROM @TenantIDs) OR fs.TenantID IN (SELECT ID FROM @TenantIDs)) AND fs.DisplayOnMobile = 1


