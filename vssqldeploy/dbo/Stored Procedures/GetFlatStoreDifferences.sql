﻿
-- =============================================
-- Author:      Igor Gaidukov
-- Create Date: 11/29/2017
-- Description: Get difference between FlatStore table and StoreService, Store, HoursOFOperations, FuelPrice tables
-- =============================================
CREATE PROCEDURE GetFlatStoreDifferences
(
    @StoreIDs ListOfGuid  READONLY
)
AS
BEGIN
    SET NOCOUNT ON

DECLARE @TempFlatStore table(
	[TenantID] [uniqueidentifier] NOT NULL,
	[StoreID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[StoreNumber] [nvarchar](64) NULL,
	[MerchantId] [nvarchar](50) NULL,
	[Phone] [nvarchar](16) NULL,
	[NumberOfPumps] [tinyint] NOT NULL,
	[Latitude] [decimal](10, 7) NOT NULL,
	[Longitude] [decimal](10, 7) NOT NULL,
	[StreetAddress] [nvarchar](128) NOT NULL,
	[City] [nvarchar](64) NOT NULL,
	[StateCode] [char](2) NULL,
	[PostalCode] [nvarchar](16) NULL,
	[CountryIsoCode] [char](2) NULL,
	[FuelBrand] [nvarchar](128) NOT NULL,
	[Services] [nvarchar](max) NULL,
	[DisplayOnMobile] [bit] NOT NULL,
	[County] [nvarchar](100) NULL,
	[MppaID] [nvarchar](50) NULL,
	[StoreInstallTypeID] [int] NULL,
	[OwnerID] [uniqueidentifier] NOT NULL,
	[IsOwnedByTenant]  AS (CONVERT([bit],case when [TenantID]=[OwnerID] then (1) else (0) end)),
	[StationImageUrl] [nvarchar](256) NULL,
	[ActiveAlertsMaintenanceMode] [bit] NOT NULL,
	[UnavailablePumps] [nvarchar](max) NULL,
	[IsSiteBillable] [bit] NOT NULL,
	[FuelProducts] [nvarchar](max) NULL,
	[OperatingHours] [nvarchar](max) NULL,
	[ActiveAlertsIgnored] [bit] NOT NULL,
	[AllowInsidePayment] [bit] NOT NULL,
	[AllowOutsidePayment] [bit] NOT NULL,
	[IsBootstrapped] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[EposIds] [nvarchar](max) NULL,
	[StateName] [nvarchar](64) NULL,
	[SiteUnavailablePumps] [nvarchar](max) NULL);

DECLARE @OriginalFlatStore table(
	[TenantID] [uniqueidentifier] NOT NULL,
	[StoreID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[StoreNumber] [nvarchar](64) NULL,
	[MerchantId] [nvarchar](50) NULL,
	[Phone] [nvarchar](16) NULL,
	[NumberOfPumps] [tinyint] NOT NULL,
	[Latitude] [decimal](10, 7) NOT NULL,
	[Longitude] [decimal](10, 7) NOT NULL,
	[StreetAddress] [nvarchar](128) NOT NULL,
	[City] [nvarchar](64) NOT NULL,
	[StateCode] [char](2) NULL,
	[PostalCode] [nvarchar](16) NULL,
	[CountryIsoCode] [char](2) NULL,
	[FuelBrand] [nvarchar](128) NOT NULL,
	[Services] [nvarchar](max) NULL,
	[DisplayOnMobile] [bit] NOT NULL,
	[County] [nvarchar](100) NULL,
	[MppaID] [nvarchar](50) NULL,
	[StoreInstallTypeID] [int] NULL,
	[OwnerID] [uniqueidentifier] NOT NULL,
	[IsOwnedByTenant]  AS (CONVERT([bit],case when [TenantID]=[OwnerID] then (1) else (0) end)),
	[StationImageUrl] [nvarchar](256) NULL,
	[ActiveAlertsMaintenanceMode] [bit] NOT NULL,
	[UnavailablePumps] [nvarchar](max) NULL,
	[IsSiteBillable] [bit] NOT NULL,
	[FuelProducts] [nvarchar](max) NULL,
	[OperatingHours] [nvarchar](max) NULL,
	[ActiveAlertsIgnored] [bit] NOT NULL,
	[AllowInsidePayment] [bit] NOT NULL,
	[AllowOutsidePayment] [bit] NOT NULL,
	[IsBootstrapped] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[EposIds] [nvarchar](max) NULL,
	[StateName] [nvarchar](64) NULL,
	[SiteUnavailablePumps] [nvarchar](max) NULL);

WITH TempResult AS (SELECT 
				StoreOverrides.ConsumerTenantID														AS TenantID, 
				store.StoreID																		AS StoreID,
				COALESCE(StoreOverrides.Name, store.Name)											AS Name, 
				COALESCE(StoreOverrides.StoreNumber, store.StoreNumber)								AS StoreNumber, 
				store.MerchantId																	AS MerchantID,
				store.Phone																			AS Phone,
				store.NumberOfPumps																	AS NumberOfPumps,
				store.Latitude																		AS Latitude,
				store.Longitude																		AS Longitude,
				store.StreetAddress																	AS StreetAddress,
				store.City																			AS City,
				store.StateCode																		AS StateCode,
				store.ZipCode																		AS PostalCode,
				store.CountryIsoCode																AS CountryIsoCode,
				FuelBrand.Name																		AS FuelBrand,
				COALESCE(StoreOverrides.DisplayOnMobile, store.DisplayOnMobile)						AS DisplayOnMobile, 
				store.County																		AS County,
				(SELECT FORMAT(store.MppaID, '0000-####'))											AS MppaID,
				store.StoreInstallTypeID															AS StoreInstallTypeID,
				store.TenantID																		AS OwnerID,
				si.ImageUrl																			AS StationImageUrl,
				COALESCE(StoreOverrides.AllowInsidePayment, store.AllowInsidePayment)				AS AllowInsidePayment, 
				COALESCE(StoreOverrides.AllowOutsidePayment, store.AllowOutsidePayment)				AS AllowOutsidePayment, 
				COALESCE(StoreOverrides.ActiveAlertsMaintenanceMode, store.ActiveAlertsMaintenanceMode)	AS ActiveAlertsMaintenanceMode, 
				store.UnavailablePumps																AS UnavailablePumps,
				COALESCE(StoreOverrides.IsSiteBillable, store.IsSiteBillable)						AS IsSiteBillable, 
				COALESCE(StoreOverrides.ActiveAlertsIgnored, store.ActiveAlertsIgnored)				AS ActiveAlertsIgnored,
				store.IsDeleted																		AS IsDeleted,
				store.IsBootstrapped																AS IsBootstrapped,
				store.EposIds																		AS EposIds,
				store.SiteUnavailablePumps															AS SiteUnavailablePumps
			FROM Store store
			CROSS APPLY OPENJSON(store.StoreTenants) 
						WITH (  
								ConsumerTenantID UNIQUEIDENTIFIER N'$.TenantID',   
								StoreNumber NVARCHAR(16) N'$.StoreNumber',  
								Name NVARCHAR(64) N'$.Name',  
								DisplayOnMobile BIT N'$.DisplayOnMobile',  
								StationImageID INT N'$.StationImageID',  
								AllowInsidePayment BIT N'$.AllowInsidePayment',  
								AllowOutsidePayment BIT N'$.AllowOutsidePayment',  
								ActiveAlertsMaintenanceMode BIT N'$.ActiveAlertsMaintenanceMode',  
								IsSiteBillable     BIT N'$.IsSiteBillable',  
								ActiveAlertsIgnored BIT N'$.ActiveAlertsIgnored'
							) 
			AS StoreOverrides
			JOIN FuelBrand ON FuelBrand.FuelBrandID = store.FuelBrandID
			LEFT JOIN AppFeatures.Stationimage si ON COALESCE(StoreOverrides.StationImageID, store.StationImageID) = si.StationImageID
			WHERE store.StoreTenants IS NOT NULL AND ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR store.StoreID IN (SELECT ID FROM @StoreIDs) ) 
			UNION
			SELECT store.TenantID, 
				store.StoreID, 
				store.Name, 
				StoreNumber, 
				MerchantID, 
				Phone, 
				NumberOfPumps, 
				Latitude, 
				Longitude, 
				StreetAddress, 
				City, 
				StateCode, 
				ZipCode AS PostalCode, 
				CountryIsoCode, 
				FuelBrand.Name AS FuelBrand, 
				store.DisplayOnMobile, 
				County, 
				(SELECT FORMAT(store.MppaID, '0000-####')) AS MppaID, 
				StoreInstallTypeID, 
				store.TenantID AS OwnerID, 
				si.ImageUrl AS StationImageUrl, 
				AllowInsidePayment, 
				AllowOutsidePayment, 
				ActiveAlertsMaintenanceMode, 
				UnavailablePumps, 
				IsSiteBillable, 
				ActiveAlertsIgnored, 
				store.IsDeleted, 
				store.IsBootstrapped, 
				store.EposIds, 
				SiteUnavailablePumps
			FROM Store store
			JOIN FuelBrand ON FuelBrand.FuelBrandID = store.FuelBrandID
			LEFT JOIN AppFeatures.Stationimage si ON store.StationImageID = si.StationImageID
			WHERE ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR store.StoreID IN (SELECT ID FROM @StoreIDs) ) )

INSERT INTO @TempFlatStore
           ([TenantID]
           ,[StoreID]
           ,[Name]
           ,[StoreNumber]
           ,[MerchantId]
           ,[Phone]
           ,[NumberOfPumps]
           ,[Latitude]
           ,[Longitude]
           ,[StreetAddress]
           ,[City]
           ,[StateCode]
           ,[PostalCode]
           ,[CountryIsoCode]
           ,[FuelBrand]
           ,[Services]
           ,[DisplayOnMobile]
           ,[County]
           ,[MppaID]
           ,[StoreInstallTypeID]
           ,[OwnerID]
           ,[StationImageUrl]
           ,[ActiveAlertsMaintenanceMode]
           ,[UnavailablePumps]
           ,[IsSiteBillable]
           ,[FuelProducts]
           ,[OperatingHours]
           ,[ActiveAlertsIgnored]
           ,[AllowInsidePayment]
           ,[AllowOutsidePayment]
           ,[IsBootstrapped]
           ,[IsDeleted]
           ,[EposIds]
           ,[StateName]
           ,[SiteUnavailablePumps])
SELECT TenantID
        ,StoreID
        ,Name
        ,StoreNumber
        ,MerchantId
        ,Phone
        ,NumberOfPumps
        ,Latitude
        ,Longitude
        ,StreetAddress
        ,City
        ,StateCode
        ,PostalCode
        ,CountryIsoCode
        ,FuelBrand
        ,dbo.GetStoreServiceArray(StoreID)
        ,DisplayOnMobile
        ,County
        ,MppaID
        ,StoreInstallTypeID
        ,OwnerID
        ,StationImageUrl
        ,ActiveAlertsMaintenanceMode
        ,UnavailablePumps
        ,IsSiteBillable
        ,dbo.GetFuelPricesJsonByStoreID(StoreID)
        ,dbo.GetOperatingHoursJsonByStoreID(StoreID)
        ,ActiveAlertsIgnored
        ,AllowInsidePayment
        ,AllowOutsidePayment
        ,IsBootstrapped
        ,IsDeleted
        ,EposIds
        ,[dbo].[GetStateNameByCode] ( StateCode )
        ,SiteUnavailablePumps
FROM TempResult


INSERT INTO @OriginalFlatStore
           ([TenantID]
           ,[StoreID]
           ,[Name]
           ,[StoreNumber]
           ,[MerchantId]
           ,[Phone]
           ,[NumberOfPumps]
           ,[Latitude]
           ,[Longitude]
           ,[StreetAddress]
           ,[City]
           ,[StateCode]
           ,[PostalCode]
           ,[CountryIsoCode]
           ,[FuelBrand]
           ,[Services]
           ,[DisplayOnMobile]
           ,[County]
           ,[MppaID]
           ,[StoreInstallTypeID]
           ,[OwnerID]
           ,[StationImageUrl]
           ,[ActiveAlertsMaintenanceMode]
           ,[UnavailablePumps]
           ,[IsSiteBillable]
           ,[FuelProducts]
           ,[OperatingHours]
           ,[ActiveAlertsIgnored]
           ,[AllowInsidePayment]
           ,[AllowOutsidePayment]
           ,[IsBootstrapped]
           ,[IsDeleted]
           ,[EposIds]
           ,[StateName]
           ,[SiteUnavailablePumps])
SELECT [TenantID]
           ,[StoreID]
           ,[Name]
           ,[StoreNumber]
           ,[MerchantId]
           ,[Phone]
           ,[NumberOfPumps]
           ,[Latitude]
           ,[Longitude]
           ,[StreetAddress]
           ,[City]
           ,[StateCode]
           ,[PostalCode]
           ,[CountryIsoCode]
           ,[FuelBrand]
           ,[Services]
           ,[DisplayOnMobile]
           ,[County]
           ,[MppaID]
           ,[StoreInstallTypeID]
           ,[OwnerID]
           ,[StationImageUrl]
           ,[ActiveAlertsMaintenanceMode]
           ,[UnavailablePumps]
           ,[IsSiteBillable]
           ,[FuelProducts]
           ,[OperatingHours]
           ,[ActiveAlertsIgnored]
           ,[AllowInsidePayment]
           ,[AllowOutsidePayment]
           ,[IsBootstrapped]
           ,[IsDeleted]
           ,[EposIds]
           ,[StateName]
           ,[SiteUnavailablePumps] FROM FlatStore
		 WHERE  ( NOT EXISTS (SELECT 1 FROM @StoreIDs) OR StoreID IN (SELECT ID FROM @StoreIDs) )

	( SELECT * FROM @TempFlatStore EXCEPT SELECT * FROM @OriginalFlatStore )
	UNION ALL
	( SELECT * FROM @OriginalFlatStore EXCEPT SELECT * FROM @TempFlatStore )
	END
