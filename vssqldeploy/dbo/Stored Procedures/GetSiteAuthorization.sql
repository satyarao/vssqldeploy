﻿CREATE PROC [dbo].[GetSiteAuthorization]
	@StoreID UNIQUEIDENTIFIER
  , @HostName NVARCHAR(50)
  , @MacAddress VARCHAR(150)
  , @IPAddress VARCHAR(50) = NULL
AS /*EXEC dbo.GetSiteAuthorization @StoreID = '00000000-0000-0000-0000-000000000000',
	@HostName = N'Raks0001SS',
	@MacAddress = '406C8F4B91FA|7CD1C3853F3E|7CD1C3853F3F',
	@IPAddress = '192.168.1.12'
*/
DECLARE	@ConfigID INT
DECLARE	@OldHostName NVARCHAR(50)
DECLARE	@OldIPAddress VARCHAR(50)
DECLARE	@UpdateHostName BIT
DECLARE	@UpdateIPAddress BIT

DECLARE	@Response BIT
DECLARE	@ResponseReason NVARCHAR(100)

IF NOT EXISTS ( SELECT	StoreID
				FROM	dbo.SiteInfo
				WHERE	StoreID = @StoreID )
	BEGIN
		INSERT	INTO dbo.SiteInfo
				(StoreID
			   , MacAddress
			   , IPAddress
			   , HostName
			   , InstallDateTime
			   , RestartDateTime)
		VALUES	(@StoreID
			   , @MacAddress
			   , @IPAddress
			   , @HostName
			   , GETUTCDATE()
			   , GETUTCDATE())

		SET @Response = 1
		SET @ResponseReason = 'New Install'
	END
ELSE
	BEGIN
		SELECT	@ConfigID = ConfigID
			  , @OldHostName = HostName
			  , @OldIPAddress = IPAddress
		FROM	dbo.SiteInfo
		WHERE	StoreID = @StoreID
				AND EXISTS ( SELECT	s.ListValue
							 FROM	Common.FnSplit(@MacAddress, '|', 0) s
									JOIN Common.FnSplit(MacAddress, '|', 0) d ON d.ListValue = s.ListValue )

		IF @ConfigID IS NOT NULL
			BEGIN
				IF @HostName <> @OldHostName
					SET @UpdateHostName = 1
				IF @IPAddress <> @OldIPAddress
					SET @UpdateIPAddress = 1

				UPDATE	dbo.SiteInfo
				SET		RestartDateTime = GETUTCDATE()
					  , IPAddress = CASE WHEN @UpdateIPAddress = 1 THEN @IPAddress
										 ELSE IPAddress
									END
					  , HostName = CASE	WHEN @UpdateHostName = 1 THEN @HostName
										ELSE HostName
								   END
				WHERE	ConfigID = @ConfigID
				SET @Response = 1
				SET @ResponseReason = 'Normal Restart' + CASE WHEN @UpdateIPAddress = 1 THEN ', IP Address Changed'
															  ELSE ''
														 END + CASE	WHEN @UpdateHostName = 1 THEN ', Host Name Changed'
																	ELSE ''
															   END
			END
		ELSE
			BEGIN
				SET @Response = 0
				SET @ResponseReason = 'Same StoreID with different MACAddress'
			END
	END
SELECT	@Response AS Response
	  , @ResponseReason AS ResponseReason
