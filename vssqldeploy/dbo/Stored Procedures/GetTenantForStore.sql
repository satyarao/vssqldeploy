﻿-- =============================================
-- Author:		Jamy, Ryals
-- Create date: 5/3/2016
-- Description:	Returns tenantId for a store
-- =============================================
CREATE PROCEDURE [dbo].[GetTenantForStore]
	@StoreID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TenantID
	FROM dbo.Store
	WHERE StoreID = @StoreID
END
