﻿



-- =============================================
-- Author:		Jamy Ryals
-- Create date: 01/26/2015
-- Description:	Retrieves the most recent Message Descriptions for a store.
-- =============================================
CREATE PROCEDURE [dbo].[GetStoreMessageDescriptions] 
	-- Add the parameters for the stored procedure here
	@StoreID UNIQUEIDENTIFIER,
	@Action NVARCHAR(256) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @Action IS NULL 
	BEGIN
		SELECT	[StoreID], 
				[Action], 
				[Version], 
				[IsEncrypted], 
				[IsActive] 				
		FROM (	SELECT *, ROW_NUMBER() OVER (PARTITION BY [Action] ORDER BY [Version] DESC) AS rn
				FROM [dbo].[MessageDescription]
				WHERE [IsActive] = 1
				AND [StoreID] = @StoreID) AS V
		WHERE rn = 1	
	END
	ELSE
	BEGIN
		SELECT	[StoreID], 
				[Action], 
				[Version], 
				[IsEncrypted], 
				[IsActive]
		FROM (	SELECT *, ROW_NUMBER() OVER (PARTITION BY [Action] ORDER BY [Version] DESC) AS rn
				FROM [dbo].[MessageDescription]
				WHERE [IsActive] = 1
				AND [StoreID] = @StoreID
				AND [Action] = @Action) AS V
		WHERE rn = 1
	END
END
