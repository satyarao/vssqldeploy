﻿
CREATE PROCEDURE [dbo].[GetChildsTenantID]
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE	@child HIERARCHYID

	SELECT	@child = OrgID
	FROM	dbo.Tenant
	WHERE	TenantID = @TenantID

	BEGIN
		SELECT	Tenantid,Name, OrgID.GetLevel()  as OrgLevel
		FROM	dbo.Tenant
		WHERE OrgID.IsDescendantOf(@child)=1
		and TenantID<>@TenantID
		ORDER BY OrgLevel
	END
END

