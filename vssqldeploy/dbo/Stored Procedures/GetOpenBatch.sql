﻿
CREATE PROCEDURE [dbo].[GetOpenBatch]
	@StoreID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	  	
	IF NOT EXISTS (SELECT * FROM [dbo].[MppaBatch] WHERE StoreID = @StoreID AND IsClosed = 0)
	begin
		INSERT INTO [dbo].[MppaBatch]
			([StoreID]
			,[IsClosed]
			,[CreatedOn])
		VALUES
			(@StoreID
			,0
			,GETUTCDATE())
	end
	SELECT StoreID as StoreId
	       ,BatchID as BatchId
	       ,IsClosed
	       ,IsMisMatch
	       ,TotalTransactions
	       ,TotalAmount
	       ,RefData
	       ,SettlementPeriodID as SettlementPeriodId
	       ,BusinessDate
	       ,UMTI,
		   BatchGuid
	FROM [dbo].[MppaBatch]
	WHERE StoreID = @StoreID AND IsClosed = 0
END
