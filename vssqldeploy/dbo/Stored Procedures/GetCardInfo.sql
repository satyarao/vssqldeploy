﻿CREATE PROC [dbo].[GetCardInfo]
       @CardNumber NVARCHAR(50) = NULL,
       @CardTypeID SMALLINT = NULL OUTPUT,
       @CardType NVARCHAR(50) = NULL OUTPUT,
       @CardIssuer NVARCHAR(50) = NULL OUTPUT
AS
--Set the cardtypeid from the cardNumber passed in
DECLARE @RowNum INT, @regex NVARCHAR(50)
SELECT @CardTypeID = MIN(CardTypeID) FROM LKCardType WHERE IsActive = 1  --start with the lowest ID
SELECT @RowNum = Count(*) FROM LKCardType WHERE IsActive=1      --get total number of records
WHILE @RowNum > 0                         --loop until no more records
BEGIN
    SELECT @regex = RegEx, @CardType = CardType, @CardIssuer = CardIssuer FROM LKCardType WHERE CardTypeID = @CardTypeID --get other info from that row
       IF PATINDEX(@regex, @CardNumber) > 0  --check if regex matches card number
       BEGIN
              break --if regex matches then we found our card type
       END
       
    SELECT TOP 1 @CardTypeID=CardTypeID FROM LKCardType WHERE IsActive=1 AND CardTypeID > @CardTypeID ORDER BY CardTypeID ASC --get the next one
    SET @RowNum = @RowNum - 1 --decrease count
END
IF @RowNum = 0 --if we never match on any row then set cardtypeid to null
       BEGIN
              SET @CardTypeID = NULL
              SET @CardType = NULL
              SET @CardIssuer = NULL
       END

SELECT @CardTypeID AS CardTypeID
         , @CardType   AS CardType
         , @CardIssuer AS CardIssuer
