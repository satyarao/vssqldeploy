﻿

-- =============================================
-- Author:  Andrei Ramanovich
-- Create date: 06/21/2018 
-- Description:	Delete all customer data in accordance with GDPR rules
-- 10/26/2018: Jamy Ryals Remove Verification Delete
-- 12/05/2018: Igor Kutesnka - TenantObjectID
-- 02/08/2019: Roman Lavreniuk - update [EmailAddressHash] column
-- 08/06/2019: Sergei Buday - PLAT-4004 [Identity] Clean up membership reboot stuff that we don't need
-- 08/20/2019: Andrei Ramanovich - delete old tables and partner user ids
-- 09/23/2019: Jamy Ryals - remove payment accounts from deleted users
-- =============================================
CREATE PROCEDURE [dbo].[DeleteAllCustomerData]
	@UserID UNIQUEIDENTIFIER,
	@TenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRAN

	DECLARE @UserEmail nvarchar(255);
	DECLARE @NewUserEmail nvarchar(255) = CONVERT(CHAR(36), @UserID) + '@DELETED.petrozone.com';

	SELECT @UserEmail = [EmailAddress]
	FROM [dbo].[UserInfo]
	WHERE [UserID] = @UserID

	UPDATE [dbo].[UserInfo]
	SET [EmailAddress] = @NewUserEmail
        ,[EmailAddressHash] = [Common].[BinaryToBase64](HASHBYTES('SHA2_256', @NewUserEmail))
		,[MobileNumber] = NULL
		,[FirstName] = NULL
		,[LastName] = NULL
		,[ImageUri] = 'https://p97dev.blob.core.windows.net/customerphotos/PetroZoneUser.jpg'
		,[IsActive] = 0
		,[UpdatedBy] = 'GDPR Delete'
		,[UpdatedOn] = GETUTCDATE()
		,[Name] = NULL
		,[IsDeleted] = 1
		--,[TenantObjectId] = NULL --tbd
	WHERE [UserID] = @UserID

	DELETE FROM [AccessControl].[UserIdentity]
	WHERE [UserID] = @UserID

	DELETE FROM [AccessControl].[UserGroupAssignment]
	WHERE [UserID] = @UserID

	DELETE FROM [AccessControl].[UserRestrictedTenantAssignment]
	WHERE [UserID] = @UserID

	UPDATE [Payment].[LKUserPaymentSource]
	SET [IsActive] = 0
	WHERE [UserID] = @UserID
	
	UPDATE [PaymentsEx].[LKUserPaymentSource]
	SET [IsActive] = 0
	WHERE [UserID] = @UserID

	UPDATE [dbo].[Store]
	SET [CreatedBy] = @NewUserEmail
	WHERE [TenantID] = @TenantID AND [CreatedBy] = @UserEmail

	UPDATE [dbo].[Store]
	SET [UpdatedBy] = @NewUserEmail
	WHERE [TenantID] = @TenantID AND [UpdatedBy] = @UserEmail

	UPDATE [dbo].[FlatBasket]
	SET [UserEmail] = @NewUserEmail
	WHERE [UserID] = @UserID

	UPDATE [AccessControl].[PowerBiConfig]
	SET [CreatedBy] = @NewUserEmail
	WHERE [TenantId] = @TenantID AND [CreatedBy] = @UserEmail

	UPDATE [AccessControl].[PowerBiConfig]
	SET [UpdatedBy] = @NewUserEmail
	WHERE [TenantId] = @TenantID AND [UpdatedBy] = @UserEmail
	
	DELETE FROM [Loyalty].[UserLoyaltyCard]
	WHERE [UserID] = @UserID

	DELETE FROM [dbo].[EmailSubscription]
	WHERE [UserID] = @UserID

	DELETE FROM [dbo].[JanrainAccessCode]
	WHERE [UserID] = @UserID

	DELETE FROM [dbo].[JanrainUserAccess]
	WHERE [UserID] = @UserID

	DELETE FROM [dbo].[ExcentusMemberTokens]
	WHERE [UserID] = @UserID

	DELETE FROM [PNS].[NotificationToken]
	WHERE [UserID] = @UserID

	DELETE FROM [dbo].[UserTrackLocation]
	WHERE [UserID] = @UserID

	DELETE FROM [UserInput].[Entry]
	WHERE [UserID] = @UserID

	DELETE FROM [dbo].[ConfigNPCA]
	WHERE [TenantID] = @TenantID AND [AuthUserName] = @UserEmail

	DELETE FROM [MSP].[UserWalletMap]
	WHERE [UserID] = @UserID

	COMMIT TRAN
END

