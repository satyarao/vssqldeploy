﻿CREATE PROC [dbo].[IUDCarWashDetail]
	@StoreID UNIQUEIDENTIFIER
  , @POSCode NVARCHAR(50)
  , @Modifier NVARCHAR(10) = NULL
  , @POSCodeFormat NVARCHAR(10) = NULL
  , @Description NVARCHAR(max)
  , @PaymentSystemProductCode NVARCHAR(50)
  , @Amount MONEY
  , @CurrencyCode NVARCHAR(10)
  , @UnitOfMeasure NVARCHAR(20)
  , @UnitPrice MONEY
  , @TaxCode NVARCHAR(20) = NULL
  , @IsActive BIT = 1
AS
BEGIN
	SET NOCOUNT ON
	IF @Amount > 0
		BEGIN
			MERGE dbo.CarWash AS target
			USING
				( SELECT	@StoreID
						  , @POSCode
						  , @Modifier
						  , @POSCodeFormat
						  , @Description
						  , @PaymentSystemProductCode
						  , @Amount
						  , @CurrencyCode
						  , @UnitOfMeasure
						  , @UnitPrice
						  , @TaxCode
						  , @IsActive
				) AS source ( StoreID, POSCode, Modifier, POSCodeFormat, Description, PaymentSystemProductCode, Amount
						  , CurrencyCode, UnitOfMeasure, UnitPrice, TaxCode, IsActive )
			ON ( target.StoreID = source.StoreID
				 AND target.POSCode = source.POSCode
			   )
			WHEN MATCHED THEN
				UPDATE SET
						 Modifier = source.Modifier
					   , POSCodeFormat = source.POSCodeFormat
					   , Description = source.Description
					   , PaymentSystemProductCode = source.PaymentSystemProductCode
					   , Amount = source.Amount
					   , CurrencyCode = source.CurrencyCode
					   , UnitOfMeasure = source.UnitOfMeasure
					   , UnitPrice = source.UnitPrice
					   , TaxCode = source.TaxCode
					   , IsActive = source.IsActive
					   , UpdatedOn = GETUTCDATE()
			WHEN NOT MATCHED THEN
				INSERT ( StoreID
					   , POSCode
					   , Modifier
					   , POSCodeFormat
					   , Description
					   , PaymentSystemProductCode
					   , Amount
					   , CurrencyCode
					   , UnitOfMeasure
					   , UnitPrice
					   , TaxCode
					   , IsActive
					   )
				VALUES ( source.StoreID
					   , source.POSCode
					   , source.Modifier
					   , source.POSCodeFormat
					   , source.Description
					   , source.PaymentSystemProductCode
					   , source.Amount
					   , source.CurrencyCode
					   , source.UnitOfMeasure
					   , source.UnitPrice
					   , source.TaxCode
					   , source.IsActive
					   );
		END
END
