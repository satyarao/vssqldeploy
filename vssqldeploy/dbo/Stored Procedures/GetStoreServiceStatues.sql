﻿
CREATE   PROCEDURE GetStoreServiceStatues
	@StoreId uniqueidentifier
AS
BEGIN
	SELECT log.*
	FROM StoreServiceStatusLog log
	CROSS APPLY 
	(
		SELECT TOP 1 [ID] FROM StoreServiceStatusLog
		WHERE [ServiceType] = log.[ServiceType] 
			AND (([ServiceName] IS NULL AND log.[ServiceName] IS NULL) OR ([ServiceName] = log.[ServiceName])) 
			AND [StoreId] = log.[StoreId]
		ORDER BY [ActualDate] DESC
	) AS [group]
	WHERE [group].ID = log.ID AND log.[StoreId] = @StoreId
END
