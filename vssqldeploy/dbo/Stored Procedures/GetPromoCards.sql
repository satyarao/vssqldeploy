﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 1/16/2017
-- Update date: 1/17/2017 Andrei Ramanovich - Add multiple tenants filter
-- Update date: 2/16/2017 Andrei Ramanovich - Add IsExpired filter, changed [PromoID] order to ASC
-- Update date: 4/05/2017 Andrei Ramanovich - Add [StoreRulesPayload] column
-- Update date: 10/20/2017 Andrei Ramanovich - Add [LinksPayload] column
-- Description:	Returns promo cards for a tenant
-- =============================================
CREATE PROCEDURE [dbo].[GetPromoCards]
	@TenantIDs ListOfGuid READONLY
	,@PromoID UNIQUEIDENTIFIER = NULL
	,@IsExpired bit = NULL
	,@LoggedIn bit = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [PromoID]
      ,[TenantID]
      ,[Target]
      ,[Title]
      ,[Subtitle]
      ,[Description]
      ,[ImageURL]
      ,[TemplateImagePosition]
      ,[ExpirationDate]
      ,[Order] 
      ,[StoreRulesPayload]
      ,[LinksPayload]
	FROM [dbo].[PromoCards] pr
	WHERE (pr.[TenantID] IN (SELECT ID FROM @TenantIDs) OR (NOT EXISTS (SELECT * FROM @TenantIDs)))    
	AND pr.[PromoID] = ISNULL(@PromoID, pr.[PromoID])
	AND 
	(@IsExpired IS NULL OR 
	(@IsExpired = 1 AND GETUTCDATE() > pr.[ExpirationDate]) OR 
	(@IsExpired = 0 AND (pr.[ExpirationDate] IS NULL OR GETUTCDATE() < pr.[ExpirationDate])))
	AND 
	(@LoggedIn IS NULL OR 
	(@LoggedIn = 1 AND pr.Target IN ('authorized', 'all')) OR 
	(@LoggedIn = 0 AND pr.Target IN ('anonymous', 'all'))
	)
	ORDER BY [Order] ASC, [PromoID] ASC
END
