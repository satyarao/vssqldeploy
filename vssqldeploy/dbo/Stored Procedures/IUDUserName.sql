﻿-- =============================================
-- Author:		<Author: Alexandr Goroshko>
-- Create date: <Create Date: 8/18/2015>
-- Description:	<Description: Insert/Update Name into dbo.UserInfo table>
-- =============================================

CREATE PROCEDURE [dbo].[IUDUserName] 
	-- Add the parameters for the stored procedure here
	@UserID UNIQUEIDENTIFIER
  , @Name NVARCHAR(255) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRAN
			UPDATE	dbo.UserInfo
					SET	    Name = @Name
						  , UpdatedBy = EmailAddress
						  , UpdatedOn = GETUTCDATE()
					WHERE	UserID = @UserID
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
		DECLARE @ErrorMessage nvarchar(4000) = ERROR_MESSAGE(),
			@ErrorNumber int = ERROR_NUMBER(),
			@ErrorSeverity int = ERROR_SEVERITY(),
			@ErrorState int = ERROR_STATE(),
			@ErrorLine int = ERROR_LINE(),
			@ErrorProcedure nvarchar(200) = ISNULL(ERROR_PROCEDURE(), '-');
		SET @ErrorMessage = N'Error: %d, Level: %d, State: %d, Procedure: %s, Line: %d, ' + 'Message: ' + @ErrorMessage;
		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine)
	END CATCH
END
