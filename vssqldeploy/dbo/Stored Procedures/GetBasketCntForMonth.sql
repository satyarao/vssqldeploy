﻿	CREATE PROC [dbo].[GetBasketCntForMonth]
		@TenantID UNIQUEIDENTIFIER = NULL
	, @ForMonth SMALLINT
	, @ForYear INT
	AS /*
	EXEC dbo.GetBasketCntForMonth
		@ForMonth = 11
	, @ForYear = 2014
	*/
	
	DECLARE	@FromDate DATE
	DECLARE	@ToDate DATE
	
	SELECT	@FromDate = CONVERT(SMALLDATETIME, (CONVERT(VARCHAR(4), @ForYear) + '-' + CONVERT(VARCHAR(2), @ForMonth) + '-01'))
	SELECT	@ToDate = DATEADD(month,1,@FromDate)
	
	IF @TenantID IS NULL
		BEGIN
			SELECT t.TenantID
				, t.Name AS TenantName
				, s.StoreID
				, s.Name AS StoreName, s.StreetAddress, s.City, s.StateCode, s.ZipCode 
				, ISNULL((SELECT COUNT(*) FROM dbo.Basket b
					WHERE b.storeid = s.storeid
					AND b.CreatedOn >= @FromDate
					AND b.CreatedOn <  @ToDate
					AND BasketStateID = 3),0) AS Cnt
				, ISNULL((SELECT SUM(b.Total) FROM dbo.Basket b
					WHERE b.storeid = s.storeid
					AND b.CreatedOn >= @FromDate
					AND b.CreatedOn <  @ToDate
					AND BasketStateID = 3),0) AS SumBasketTotal
			FROM store s
			JOIN dbo.Tenant t ON t.TenantID = s.TenantID
			WHERE s.IsSiteBillable = 1
			ORDER BY t.Name, s.[Name]
	end
	ELSE
		BEGIN
			SELECT t.TenantID
				, t.Name AS TenantName
				, s.StoreID
				, s.Name AS StoreName, s.StreetAddress, s.City, s.StateCode, s.ZipCode 
				, ISNULL((SELECT COUNT(*) FROM dbo.Basket b
					WHERE b.storeid = s.storeid
					AND b.CreatedOn >= @FromDate
					AND b.CreatedOn <  @ToDate
					AND BasketStateID = 3),0) AS Cnt
				, ISNULL((SELECT SUM(b.Total) FROM dbo.Basket b
					WHERE b.storeid = s.storeid
					AND b.CreatedOn >= @FromDate
					AND b.CreatedOn <  @ToDate
					AND BasketStateID = 3),0) AS SumBasketTotal
			FROM store s
			JOIN dbo.Tenant t ON t.TenantID = s.TenantID
			WHERE s.TenantID = @TenantID AND s.IsSiteBillable = 1
			ORDER BY t.Name, s.[Name]
		END
		
