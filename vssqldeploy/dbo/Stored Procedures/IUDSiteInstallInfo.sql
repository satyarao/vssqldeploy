﻿
CREATE PROC [dbo].[IUDSiteInstallInfo]
	@TenantID [uniqueidentifier],
	@StoreID [uniqueidentifier],
	@IsSelfServe [bit] = 1,
	@IsInStoreEnabled [bit] = 0,
	@ForecourtIP [nvarchar](64) = NULL,
	@SecondNetworkCom [nvarchar](16) = NULL,
	@VirtualPrinterPort [nvarchar](16) = NULL,
	@DispenserTypeID [tinyint] = NULL,
	@POSTypeID [tinyint] = NULL,
	@FCTypeID [tinyint]  = NULL,
    @IsActive BIT = 1
AS 

MERGE [dbo].[SiteInstallInfo] AS target
USING
	(SELECT @TenantID AS TenantID
		  , @StoreID AS StoreID
		  , @IsSelfServe AS IsSelfServe
		  , @IsInStoreEnabled AS IsInStoreEnabled
		  , @ForecourtIP AS ForecourtIP
		  , @SecondNetworkCom AS SecondNetworkCom
		  , @VirtualPrinterPort AS VirtualPrinterPort
		  , @DispenserTypeID AS DispenserTypeID
		  , @POSTypeID AS POSTypeID
		  , @FCTypeID AS FCTypeID
		  , @IsActive AS IsActive)
		  AS source (TenantID, StoreID, IsSelfServe, IsInStoreEnabled, ForecourtIP, SecondNetworkCom, 
		  VirtualPrinterPort, DispenserTypeID, POSTypeID, FCTypeID, IsActive)
ON (target.TenantID = source.TenantID AND target.StoreID = source.StoreID)
WHEN MATCHED THEN
	UPDATE SET IsSelfServe = ISNULL(source.IsSelfServe, target.IsSelfServe)
			 , IsInStoreEnabled = ISNULL(source.IsInStoreEnabled, target.IsInStoreEnabled)
			 , ForecourtIP = source.ForecourtIP
			 , SecondNetworkCom = source.SecondNetworkCom
			 , VirtualPrinterPort = source.VirtualPrinterPort
			 , DispenserTypeID = source.DispenserTypeID
			 , POSTypeID = source.POSTypeID
			 , FCTypeID = source.FCTypeID
			 , IsActive = ISNULL(source.IsActive, target.IsActive)
			 , UpdatedBy = N'SP'
			 , UpdatedOn = GETUTCDATE()
WHEN NOT MATCHED THEN
		INSERT (
			TenantID
		  , StoreID
		  , IsSelfServe
		  , IsInStoreEnabled
		  , ForecourtIP
		  , SecondNetworkCom
		  , VirtualPrinterPort
		  , DispenserTypeID
		  , POSTypeID
		  , FCTypeID
		  , IsActive
		  , CreatedBy
		  , CreatedOn
		   )
	VALUES (
			@TenantID
		  , @StoreID
		  , ISNULL(@IsSelfServe, 1)
		  , ISNULL(@IsInStoreEnabled, 0)
		  , @ForecourtIP
		  , @SecondNetworkCom
		  , @VirtualPrinterPort
		  , @DispenserTypeID
		  , @POSTypeID
		  , @FCTypeID
		  , ISNULL(@IsActive, 1)
		  , N'SP'
		  , GETUTCDATE()
		   );

--EXEC  [dbo].[IUDSiteInstallInfo]
--@TenantID = '00000000-0000-0000-0000-000000000000',
--@StoreID = '08513F16-6788-4895-9771-0003F65E73C1',
--@IsSelfServe = 1,
--@IsInStoreEnabled = 1,
--@ForecourtIP = '192.168.1.150',
--@SecondNetworkCom = 'COM11',
--@VirtualPrinterPort = 'COM13',
--@DispenserTypeID = 1,
--@POSTypeID = 1,
--@FCTypeID = 1,
--@IsActive = 1

--EXEC  [dbo].[IUDSiteInstallInfo]
--@TenantID = '91AB977C-6784-44AD-B670-AE4B039F8ED5',
--@StoreID = 'C5F073EC-3799-4695-82B8-008A49FEACF3',
--@IsSelfServe = NULL,
--@IsInStoreEnabled = NULL,
--@ForecourtIP = NULL,
--@SecondNetworkCom = NULL,
--@VirtualPrinterPort = NULL,
--@DispenserTypeID = NULL,
--@POSTypeID = NULL,
--@FCTypeID = NULL,
--@IsActive = NULL
