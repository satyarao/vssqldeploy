﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 2018-04-05
-- Description:	Get janrain access token by one time access code
-- =============================================
CREATE PROCEDURE [dbo].[GetJanrainTokensByAccessCode]
	@AccessCode nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT code.[AccessCode], code.[UserID], tokens.[AccessToken], tokens.[RefreshToken], tokens.[ExpiresOn]
	FROM [dbo].[JanrainAccessCode] code
	CROSS APPLY 
	(
		SELECT TOP(1) access.[AccessToken], access.[RefreshToken], access.[ExpiresOn]
		FROM [dbo].[JanrainUserAccess] access
		WHERE access.[UserID] = code.[UserID]
		ORDER BY [ExpiresOn] DESC
	) tokens
	WHERE code.[AccessCode] = @AccessCode AND code.[IsActive] = 1 AND code.[ExpiresOn] > GETUTCDATE()

	UPDATE [dbo].[JanrainAccessCode]
	SET [IsActive] = 0
	WHERE [AccessCode] = @AccessCode
END
