﻿CREATE PROCEDURE [dbo].[UpdateMPPA_XSLT] 
	@MPPAID INT,
    @PropertyValue NVARCHAR(max) = NULL,
    @Delete BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @StoreID UNIQUEIDENTIFIER = NULL;
	SELECT @StoreID = StoreID
	FROM [dbo].[Store]
	WHERE MPPAID = @MPPAID

	IF(@StoreID IS NOT NULL AND @PropertyValue IS NOT NULL AND DATALENGTH(@PropertyValue) != 0)
	BEGIN
		IF @Delete = 1
		BEGIN
			DELETE FROM [dbo].[TenantProperty]
			WHERE TenantID = @StoreID AND PropertyName = 'mppa_xslt'
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT 1 FROM [dbo].[TenantProperty] WHERE TenantID = @StoreID AND PropertyName = 'mppa_xslt')
			BEGIN
				UPDATE [dbo].[TenantProperty]
				SET PropertyValue = @PropertyValue
				WHERE TenantID = @StoreID AND PropertyName = 'mppa_xslt'
			END
			ELSE
			BEGIN
				INSERT INTO [dbo].[TenantProperty] VALUES(@StoreID, 'mppa_xslt', @PropertyValue)
			END
		END
	END
END
