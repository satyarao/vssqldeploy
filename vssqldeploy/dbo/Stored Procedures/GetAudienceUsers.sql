﻿-- =============================================
-- Author:  Davydovich Raman
-- Description: Get user data for Audience page
-- Create date: 21/08/2018  
-- =============================================
CREATE PROCEDURE [dbo].[GetAudienceUsers]
    @UserIDs ListOfGuid READONLY
AS
BEGIN
    SET NOCOUNT ON;

    SELECT ui.UserID
          , ui.TenantID
          , t.Name AS TenantName
          , ui.FirstName
          , ui.EmailAddress AS Email
          , ui.LastName
          , ui.MobileNumber AS PhoneNumber
          , ui.CreatedOn AS CreatedOn
          , ui.IsActive
          , ls.NuDetectScore
          , ls.NuDetectScoreBand
          , ls.ScoreSignals
          , ul.BlockTransactions AS BlockTransactions
          , ul.BlockCardAddition AS BlockCardAddition
    FROM dbo.UserInfo ui
        JOIN dbo.Tenant t ON t.TenantID = ui.TenantID
        LEFT JOIN NuDetect.UserLastScore ls ON ui.UserID = ls.UserId
        LEFT JOIN dbo.UserLockouts ul ON ui.UserID = ul.UserId
        INNER JOIN @UserIDs uids on uids.ID = ui.UserID
END


