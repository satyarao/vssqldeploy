﻿

/*
	EXEC dbo.SearchEVSiteByGeoCoordinates 
		@Latitude			= 52.494853,
		@Longitude			= 13.452201,
		@SearchDistance		= 10000.00,
		@SearchMetric		= 'Meter',
		@Offset				= 1,
		@Limit				= 10

*/

CREATE   PROC dbo.SearchEVSiteByGeoCoordinates 
	@Latitude			DECIMAL(10,7),
	@Longitude			DECIMAL(10,7),
	@SearchDistance		DECIMAL(12,6),
	@SearchMetric		VARCHAR(10), -- Accepted Values: 'Mile, KM, Meter'
	@Offset				INT = 1,
	@Limit				INT = 25000
AS 
BEGIN

	DECLARE @StartGeoPoint	GEOGRAPHY
	DECLARE @MetricFactor	DECIMAL(20, 15)
	DECLARE @SRID			INT -- Spatial Reference ID for Geography data type computations

	IF (@SearchMetric NOT IN ('KM', 'Meter', 'Mile'))
	BEGIN
		THROW 100004, 'Invalid search distance Metric. Accepted values are KM, Meter & Mile', 1;
		RETURN -1
	END;

	SET @SRID = 4326
	SET @StartGeoPoint = GEOGRAPHY::Point(@Latitude, @Longitude, @SRID);


	SET @MetricFactor = 
		CASE @SearchMetric
			WHEN 'KM' THEN 0.001
			WHEN 'Meter' THEN 1
			WHEN 'Mile' THEN 0.000621371
	END;

	SELECT	sd.SiteIdentifier
			,sd.Name
			,sd.DisplayName
			,sd.StreetName
			,sd.City
			,sd.ZipCode
			,sd.Longitude
			,sd.Latitude
			,sd.Phone
			,ROUND(@StartGeoPoint.STDistance(sd.GeoPosition)*@MetricFactor, 4) as Distance
			--GEOGRAPHY::Point(sd.Latitude, sd.Longitude, 4326) Geo
			,DataProviderName ServiceProvider
	FROM	Common.SiteDetail sd
	WHERE	@StartGeoPoint.STDistance(sd.GeoPosition)*@MetricFactor <= @SearchDistance
	ORDER	BY Distance
	OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY;

END
