﻿CREATE PROCEDURE [dbo].[IUDTenantProperty_Trim]
	@TenantID UNIQUEIDENTIFIER,
	@PropertyName NVARCHAR(50),
	@PropertyValue NVARCHAR(250)
AS
BEGIN
	--If property value is null then enable delete
	SET NOCOUNT ON;
	DECLARE @Delete BIT
	Set @Delete = CAST(CASE WHEN ISNULL(@PropertyValue, '') = '' THEN 1 ELSE 0 END AS BIT);
	EXEC [dbo].[IUDTenantProperty] @TenantID = @TenantID, @PropertyName = @PropertyName, @PropertyValue = @PropertyValue, @Delete = @Delete;
END
