﻿CREATE   PROC [dbo].[IUDFuelPrice]
	@StoreID UNIQUEIDENTIFIER
  , @PCATSCode NVARCHAR(20)
  , @CashPrice SMALLMONEY
  , @CreditPrice SMALLMONEY
  , @UOM NVARCHAR(50)
  , @OctaneRating TINYINT
  , @IsActive BIT = 1
  , @StandardProductCode NVARCHAR(150)
AS /*
EXEC dbo.IUDFuelPrice @StoreID = 'AA913650-785D-42A1-A6E3-00014AC3EA2D', @PCATSCode = N'003', @CashPrice = 3.695,
	@CreditPrice = 3.699, @UOM = N'Gallon', @OctaneRating = 89, @IsActive = 1
*/
BEGIN
	SET NOCOUNT ON
	IF @CreditPrice > 0
		OR @CashPrice > 0
		BEGIN
			MERGE dbo.FuelPrice AS target
			USING
				( SELECT	@StoreID
						  , @PCATSCode
						  , @CashPrice
						  , @CreditPrice
						  , @UOM
						  , @OctaneRating
						  , @IsActive
                          , @StandardProductCode
				) AS source ( StoreID, PCATSCode, CashPrice, CreditPrice, UOM, OctaneRating, IsActive, StandardProductCode )
			ON ( target.StoreID = source.StoreID
				 AND target.PCATSCode = source.PCATSCode
			   )
			WHEN MATCHED THEN
				UPDATE SET
						 CashPrice = source.CashPrice
					   , CreditPrice = source.CreditPrice
					   , UOM = source.UOM
					   , OctaneRating = (CASE WHEN source.OctaneRating > 0 THEN source.OctaneRating ELSE target.OctaneRating END)
					   , IsActive = source.IsActive
					   , UpdatedBy = N'SP'
					   , UpdatedOn = GETUTCDATE()
                       , StandardProductCode = source.StandardProductCode
			WHEN NOT MATCHED THEN
				INSERT ( StoreID
					   , PCATSCode
					   , CashPrice
					   , CreditPrice
					   , UOM
					   , OctaneRating
					   , IsActive
					   , CreatedBy 
					   , StandardProductCode)
				VALUES ( source.StoreID
					   , source.PCATSCode
					   , source.CashPrice
					   , source.CreditPrice
					   , source.UOM
					   , source.OctaneRating
					   , source.IsActive
					   , N'SP'
					   , source.StandardProductCode);
		END
END
