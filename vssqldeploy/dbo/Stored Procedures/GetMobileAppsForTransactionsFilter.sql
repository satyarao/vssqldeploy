﻿-- =============================================
-- Author:		Alex Goroshko
-- Create date: 9/13/2017
-- Update date: 9/23/2017 Alex Goroshko - bug fix to return partners tenants even if they are not set IsOwnersTransactionsShown = 1
-- Update date: 2/2/2018 Alex Goroshko - bug fix to return a tenanId itself
-- Description:	Retrieve mobile app tenants list which tenant has access to see transactions
-- =============================================
CREATE PROCEDURE [dbo].[GetMobileAppsForTransactionsFilter] 
	@TenantId UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	WITH MobileAppsTenantIds (TenantId)
	AS
	(
	    SELECT DISTINCT StorePartners.PartnerTenantID AS TenantId FROM store s
		CROSS APPLY OPENJSON(s.StoreTenants) 
								WITH (  
										PartnerTenantID UNIQUEIDENTIFIER N'$.TenantID',   
										IsOwnersTransactionsShown BIT N'$.IsOwnersTransactionsShown'
									 )
									 AS StorePartners
		WHERE s.IsDeleted = 0 AND TenantId = @TenantId --AND StorePartners.IsOwnersTransactionsShown = 1
		UNION
		SELECT DISTINCT s.TenantID AS TenantId FROM store s
		CROSS APPLY OPENJSON(s.StoreTenants) 
								WITH (  
										PartnerTenantID UNIQUEIDENTIFIER N'$.TenantID',   
										IsOwnersTransactionsShown BIT N'$.IsOwnersTransactionsShown'
									 )
									 AS StorePartners
		WHERE s.IsDeleted = 0 AND StorePartners.PartnerTenantID = @TenantId and StorePartners.IsOwnersTransactionsShown = 1
		UNION
		SELECT @TenantId AS TenantId --FROM Store WHERE IsDeleted = 0 AND TenantId = @TenantId
	)
	
	SELECT mat.TenantId, (SELECT Name from tenant t where t.TenantId = mat.TenantId ) AS TenantName FROM MobileAppsTenantIds mat
END

