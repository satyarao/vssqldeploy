﻿CREATE PROC [dbo].[GetStoresServices]
	@StoresIds ListOfGuid READONLY
AS 
SET NOCOUNT ON
SELECT	ss.StoreID
	  , ss.ServiceID
	  , lks.Name as ServiceName
FROM	dbo.StoreService ss
JOIN dbo.LKService lks ON ss.ServiceID = lks.ServiceID
WHERE	ss.StoreID IN (SELECT ID FROM @StoresIds) AND ss.IsActive = 1
