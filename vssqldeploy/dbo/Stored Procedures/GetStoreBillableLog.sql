﻿-- =============================================
-- Author:      
-- Create date: 01/17/2018 - Igor Gaidukov 
-- Update date: 04/26/2018 - Igor Gaidukov - added StoreName filter, use TenantID instead of list of TenantIDs
-- Description:   Get site billing changes
-- =============================================
CREATE PROC [dbo].[GetStoreBillableLog]
	@TenantID UNIQUEIDENTIFIER = NULL
	, @FromDate DATETIME2 = NULL
	, @ToDate DATETIME2 = NULL
	, @IsSiteBillable BIT = NULL
	, @StoreName NVARCHAR(256) = NULL
	, @TimeOffset INT = 0
	, @Offset INT
	, @Limit INT
	AS 
BEGIN 
	WITH TempResult AS (SELECT s.StoreID
		, s.TenantID
		, s.Name AS StoreName
		, t.Name AS TenantName
		, s.MppaID
		, s.StreetAddress
		, s.City
		, s.StateCode
		, s.ZipCode 
		, s.CountryIsoCode AS Country
		, s.IsSiteBillable
		, s.StoreInstallDate
		, sbl.LastActivationDate
		, sbl.LastDeactivationDate
	FROM dbo.Store s
	JOIN dbo.Tenant t ON t.TenantID = s.TenantID
	LEFT JOIN StoreBillableLog sbl ON sbl.StoreID = s.StoreID AND sbl.TenantID = s.TenantID
	WHERE ( @TenantID IS NULL OR s.TenantID = @TenantID)
		AND s.DisplayOnMobile = 1
		AND ( @IsSiteBillable IS NULL OR s.IsSiteBillable = @IsSiteBillable )
		AND ( @StoreName IS NULL OR s.Name LIKE '%' + @StoreName + '%' )
		AND ( (@FromDate IS NULL AND @ToDate IS NULL) OR (sbl.LastActivationDate BETWEEN @FromDate AND @ToDate) OR (sbl.LastDeactivationDate BETWEEN @FromDate AND @ToDate) )
		AND s.IsDeleted = 0
	),
	TempCount AS (SELECT COUNT(*) AS Total FROM TempResult)

	SELECT StoreID
		, TenantID
		, StoreName
		, TenantName
		, (SELECT FORMAT(MppaID, '0000-####')) AS MppaId
		, StreetAddress
		, City
		, StateCode
		, ZipCode 
		, Country
		, IsSiteBillable
		, DATEADD(mi, -(@TimeOffset), StoreInstallDate) AS StoreInstallDate
		, DATEADD(mi, -(@TimeOffset), LastActivationDate) AS LastActivationDate
		, DATEADD(mi, -(@TimeOffset), LastDeactivationDate) AS LastDeactivationDate
		, Total
	FROM TempResult, TempCount
	
	ORDER BY TenantName, StoreName ASC
	OFFSET @Offset ROWS 
	FETCH NEXT @Limit ROWS ONLY
END
