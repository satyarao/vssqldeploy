﻿
-- =============================================
-- Author:  Igor Gaidukov  (V3)
-- Create date: 18/01/2018  
-- Udpate date: 9/20/2017 Alex Goroshko - Remove P97 tenant ignorance   
-- Udpate date: 1/25/2018 Igor Gaidukov - Added output parameters   
-- Udpate date: 2/21/2018 Igor Gaidukov - 
-- Description:	Get users which were registered in user portal (mobile app)
-- =============================================
CREATE PROCEDURE [dbo].[GetMobileUsersV3]  
	  @TenantIDs ListOfGuid READONLY
	, @StartDate DATETIME2 = NULL 
	, @EndDate DATETIME2 = NULL
	, @UserID UNIQUEIDENTIFIER = NULL
	, @UserEmail NVARCHAR(MAX) = NULL
	, @SearchTerm NVARCHAR(MAX) = NULL
	, @Offset INT
	, @Limit INT
AS
BEGIN
	SET NOCOUNT ON;

	WITH TempResult AS ( SELECT ui.UserID
		, ui.TenantID
		, t.Name AS TenantName
		, ui.EmailAddress AS Email
		, ui.FirstName
		, ui.LastName
		, ui.MobileNumber AS PhoneNumber
		, ui.CreatedOn AS CreatedOn
		, ui.IsActive
		FROM dbo.UserInfo ui
			JOIN dbo.Tenant t ON t.TenantID = ui.TenantID
		WHERE (( @StartDate IS NULL AND @EndDate IS NULL) OR (@StartDate IS NOT NULL AND @EndDate IS NOT NULL AND ui.CreatedOn >= @StartDate AND ui.CreatedOn <= @EndDate ))
			AND (ui.TenantID IN (SELECT ID FROM @TenantIDs) OR (NOT EXISTS (SELECT * FROM @TenantIDs)))
			AND (@UserID IS NULL OR @UserID = ui.UserID)
			AND (@UserEmail IS NULL OR ui.EmailAddress LIKE '%' + @UserEmail + '%' )
			AND (@SearchTerm IS NULL 
				OR ui.EmailAddress LIKE '%' + @SearchTerm + '%' 
				OR ui.UserID LIKE '%' + @SearchTerm + '%' )
			) -- only user of user portal
		, TempCount AS (SELECT COUNT(*) AS Total FROM TempResult)


   SELECT UserID, TenantID,  TenantName, FirstName, Email, LastName, PhoneNumber, CreatedOn, IsActive, Total
		FROM TempResult, TempCount
		ORDER BY CreatedOn ASC  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY
END
