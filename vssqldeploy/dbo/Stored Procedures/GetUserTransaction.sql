﻿-- =============================================
-- Create date: <8/31/2015>
-- Update date: 10/21/2016
-- Update date: 1/3/2017 - Alex Goroshko - Renamed AddressLine1 to StreetAddress
-- Update date: 5/24/2017 - Igor Gaidukov - Added TotalDiscountAmount
-- Update date: 5/26/2017 - Igor Gaidukov - Adjust subtotal amount
-- Update date: 8/28/2017 - Igor Gaidukov - delete JOIN with BasketPayment, consider StoreTenantID
-- Description:	<Get transaction details>
-- =============================================
CREATE PROCEDURE [dbo].[GetUserTransaction]
	@UserID UNIQUEIDENTIFIER,
	@TransactionID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TenantID UNIQUEIDENTIFIER;
	SELECT @TenantID = TenantID
	FROM dbo.UserInfo
	WHERE UserID = @UserID

	DECLARE @CardType  NVARCHAR(MAX), @LastFour NCHAR(4);
	SELECT TOP 1
		@LastFour = (SELECT LastFour FROM [Payment].[UserPrecidiaPaymentSource] upps where upps.userpaymentsourceid = bp.userpaymentsourceid)
	, @CardType = CASE WHEN lkups.PaymentProcessorID = (SELECT paymentprocessorid from payment.lkpaymentprocessor where paymentprocessorname = 'ZipLine') THEN dbo.GetTenantPropertyValue(@TenantID, 'Payment.Zipline.Name', DEFAULT) ELSE lkups.CardName END
	FROM dbo.BasketPayment bp
	LEFT JOIN [Payment].[LKUserPaymentSource] lkups on lkups.UserPaymentSourceID = bp.UserPaymentSourceID
	WHERE bp.BasketID = @TransactionID

	DECLARE @AuthCode NVARCHAR(50);
	SET @AuthCode = (SELECT TOP 1 AuthCode FROM dbo.BasketPayment 
							where BasketID = @TransactionID Order by BasketPaymentID desc)

	SELECT	DISTINCT
	    b.BasketID AS TransactionID
	  , b.UserID
	  , b.POSDateTimeLocal
	  , fs.Name AS StoreName
	  , fs.Phone AS StorePhone
	  , fs.StreetAddress AS AddressLine1
	  , fs.City
	  , fs.StateCode
	  , fs.PostalCode
	  , @AuthCode as AuthCode
	  , @CardType AS CardType
	  , @LastFour AS LastFourCardDigits
	  , ISNULL(b.PreRewardSubTotal, b.SubTotal) AS SubTotal
	  , ISNULL((b.FuelRewardTotal + b.ItemRewardTotal), 0) AS TotalDiscountAmount
	  , b.TaxAmount 
	  , b.Total
	  
FROM	dbo.Basket b
		JOIN dbo.FlatStore fs ON fs.StoreID = b.StoreID AND ( fs.TenantID = b.AppTenantID OR fs.OwnerID = b.StoreTenantID )
		
WHERE	b.BasketID = @TransactionID AND b.UserID = @UserID

END


