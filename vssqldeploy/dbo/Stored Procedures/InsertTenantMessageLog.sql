﻿CREATE PROC [dbo].[InsertTenantMessageLog]
	@TenantID UNIQUEIDENTIFIER
  , @CorrelationID UNIQUEIDENTIFIER = NULL
  , @SequenceNumber INT = NULL
  , @SeverityID INT
  , @CategoryID INT = NULL
  , @Message NVARCHAR(MAX)
  , @CreatedOn DATETIME
AS /*
EXECUTE [dbo].[InsertTenantMessageLog] 
   @TenantID = '00000000-0000-0000-0000-000000000000'
  ,@CorrelationID = 'DA29DE0D-1CDA-4BF7-8A58-32BBAC852DC8'
  ,@SequenceNumber = 100
  ,@SeverityID = 1
  ,@CategoryID = 1
  ,@Message = 'This is a test'
  ,@CreatedOn = '2014-03-11 17:11:24.743'
*/
INSERT	INTO [dbo].[TenantMessageLog]
		( TenantID
		, CorrelationID
		, SequenceNumber
		, SeverityID
		, CategoryID
		, MessageText
		, CreatedOn
		)
VALUES	( @TenantID
		, @CorrelationID
		, @SequenceNumber
		, @SeverityID
		, @CategoryID
		, @Message
		, @CreatedOn
		)
