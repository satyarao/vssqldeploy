﻿CREATE PROCEDURE [dbo].[GetMppaBatchDetails]
	@StoreID UNIQUEIDENTIFIER,
	@FundingProvider NVARCHAR(255),
	@BatchId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT DISTINCT b.BasketID as BasketId
	, b.Total
	, ct.CardType
	FROM dbo.Basket b 
		 JOIN dbo.BasketPayment bp ON b.BasketId = bp.BasketId
		 JOIN Payment.LKUserPaymentSource ups ON ups.UserPaymentSourceID = bp.UserPaymentSourceID
		 JOIN Payment.LKPaymentProcessor ct ON ct.PaymentProcessorID = ups.PaymentProcessorID
		 JOIN Payment.FundingProvider fp ON fp.FundingProviderID = ct.FundingProviderID
	WHERE b.BatchId = @BatchId
	AND b.StoreId = @StoreID
	AND fp.FundingProviderName = @FundingProvider
	AND b.BasketStateID = 3 -- Only completed transactions
END
