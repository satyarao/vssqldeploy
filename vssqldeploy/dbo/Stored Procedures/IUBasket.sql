﻿

-- =============================================
-- Update date: 01/28/2017 Igor Gaidukov - added TenantID as parameter
-- Update date: 02/03/2017 Igor Gaidukov - added IsPaymentBillable to Basket
-- Update date: 03/21/2017 - Fixed Basket's IsPaymentBillable
-- Update date: 05/29/2017 - use IsSiteBillable instead of IsPaymentBillable (for Basket table)
-- Update date: 04/13/2017 Andrei Ramanovich - added loyalty columns to BasketLineItem
-- Update date: 08/06/2017 Jamy Ryals - Added StoreTenantID & AppTenantID
-- Update date: 10/23/2017 Igor Gaidukov - Added BasketLoyaltyStateID
-- Update date: 01/04/2018 Igor Gaidukov - Added BasketLoyaltyPrograms
-- Update date: 04/12/2018 Andrei Ramanovich - Added AppDisplayName, AppChannel
-- Update date: 07/23/2018 Jamy Ryals - Added carwashtext
-- Update date: 07/30/2018 Roman Lavreniuk - added FleetTenantID as parameter
-- Update date: 08/20/2018 Dzmitry Mikhailau - added OtherRewardTotal as parameter
-- Update date: 12/27/2018 Roman Lavreniuk - added CurrencyIsoCode as parameter
-- Update date: 10/22/2019 Samad Ahmed - added OtherLoyaltyRewardAmount, P97RewardDiscount, ExternalRewardDiscount as parameters
-- Update date: 11/15/2019 Jack Swink - Changed ItemDescription length to nvarchar(255)
-- =============================================
--Latest DbUpdate Script Number: 390
-- =============================================
CREATE   PROC [dbo].[IUBasket]
	@BasketID UNIQUEIDENTIFIER
  , @StoreTenantID UNIQUEIDENTIFIER
  , @AppTenantID UNIQUEIDENTIFIER
  , @UserID UNIQUEIDENTIFIER = NULL
  , @StoreID UNIQUEIDENTIFIER
  , @POSStoreID NVARCHAR(50)
  , @POSOperatorID NVARCHAR(50)
  , @POSTransactionID NVARCHAR(50)
  , @POSTerminalID NVARCHAR(50)
  , @POSTillID NVARCHAR(50)
  , @POSDateTime DATETIME2(7)
  , @SubTotal DECIMAL(9, 2) = NULL
  , @TaxAmount DECIMAL(9, 2) = NULL
  , @Total DECIMAL(9, 2) = NULL
  , @BasketStateID TINYINT
  , @BasketLoyaltyStateID tinyint
  , @BasketLoyaltyPrograms NVARCHAR(MAX) = NULL
  , @QRCodeText NVARCHAR(1024) = NULL
  , @RefBasketID1 UNIQUEIDENTIFIER = NULL
  , @BasketLineItem BasketLineItemType READONLY
  , @BasketPayment BasketPaymentType READONLY
  , @BatchID int = NULL
  , @PartnerTransactionID NVARCHAR(50) = NULL
  , @StacString NVARCHAR(50) = NULL
  , @AppDisplayName NVARCHAR(255)
  , @AppChannel NVARCHAR(100)
  , @FleetTenantID UNIQUEIDENTIFIER = NULL
  , @OtherRewardTotal [decimal](9, 2) = NULL
  , @CurrencyIsoCode CHAR(3)
AS
BEGIN
SET NOCOUNT ON

DECLARE	@TimeZoneID INT
SELECT	@TimeZoneID = ISNULL(TimeZoneID, 36)
FROM	dbo.Store
WHERE	StoreID = @StoreID

DECLARE	@POSDateTimeLocal DATETIME2
SELECT	@POSDateTimeLocal = Common.fnConvertUtcToLocalByTimezoneID(@TimeZoneID, @POSDateTime)

MERGE dbo.Basket AS target
USING
	(SELECT	@BasketID AS BasketID
		  , @StoreTenantID AS StoreTenantID
		  , @AppTenantID AS AppTenantID
		  , @UserID AS UserID
		  , @StoreID AS StoreID
		  , @POSStoreID AS POSStoreID
		  , @POSOperatorID AS POSOperatorID
		  , @POSTransactionID AS POSTransactionID
		  , @POSTerminalID AS POSTerminalID
		  , @POSTillID AS POSTillID
		  , @POSDateTime AS POSDateTime
		  , @SubTotal AS SubTotal
		  , @TaxAmount AS TaxAmount
		  , @Total AS Total
		  , @BasketStateID AS BasketStateID
		  , @BasketLoyaltyStateID AS BasketLoyaltyStateID
		  , @BasketLoyaltyPrograms AS BasketLoyaltyPrograms
		  , @QRCodeText AS QRCodeText
		  , @BatchID as BatchID
		  , @PartnerTransactionID as PartnerTransactionID
		  , @StacString as StacString
		  , @AppDisplayName as AppDisplayName
		  , @AppChannel as AppChannel
          , @FleetTenantID AS FleetTenantID
		  , @OtherRewardTotal AS OtherRewardTotal
          , @CurrencyIsoCode AS CurrencyIsoCode)  AS source
ON (target.BasketID = source.BasketID)
WHEN MATCHED THEN
	UPDATE SET UserID = ISNULL(source.UserID, target.UserID)
			 , POSStoreID = source.POSStoreID
			 , POSOperatorID = source.POSOperatorID
			 , POSTransactionID = source.POSTransactionID
			 , POSTerminalID = source.POSTerminalID
			 , POSTillID = source.POSTillID
			 , POSDateTime = source.POSDateTime
			 , POSDateTimeLocal = @POSDateTimeLocal
			 , SubTotal = ISNULL(source.SubTotal, target.SubTotal)
			 , TaxAmount = ISNULL(source.TaxAmount, target.TaxAmount)
			 , Total = ISNULL(source.Total, target.Total)
			 , BasketStateID = source.BasketStateID
			 , BasketLoyaltyStateID = source.BasketLoyaltyStateID
			 , BasketLoyaltyPrograms = source.BasketLoyaltyPrograms
			 , QRCodeText = ISNULL(source.QRCodeText, target.QRCodeText)
			 , UpdatedOn = GETUTCDATE()
			 , BatchID = source.BatchID
			 , PartnerTransactionID = source.PartnerTransactionID
			 , StacString = source.StacString
			 , AppDisplayName = source.AppDisplayName
			 , AppChannel = source.AppChannel
             , FleetTenantID = ISNULL(source.FleetTenantID, target.FleetTenantID)
			 , OtherRewardTotal = ISNULL(source.OtherRewardTotal, target.OtherRewardTotal)
             , CurrencyIsoCode = source.CurrencyIsoCode
WHEN NOT MATCHED THEN
	INSERT (
			BasketID
		  , StoreTenantID
		  , AppTenantID
		  , UserID
		  , StoreID
		  , POSStoreID
		  , POSOperatorID
		  , POSTransactionID
		  , POSTerminalID
		  , POSTillID
		  , POSDateTime
		  , POSDateTimeLocal
		  , SubTotal
		  , TaxAmount
		  , Total
		  , BasketStateID
		  , BasketLoyaltyStateID
		  , BasketLoyaltyPrograms
		  , QRCodeText
		  , BatchID
		  , PartnerTransactionID
		  , StacString
		  , IsSiteBillable
		  , AppDisplayName
		  , AppChannel
          , FleetTenantID
		  , OtherRewardTotal
          , CurrencyIsoCode
		   )
	VALUES (
			@BasketID
		  , @StoreTenantID
		  , @AppTenantID
		  , @UserID
		  , @StoreID
		  , @POSStoreID
		  , @POSOperatorID
		  , @POSTransactionID
		  , @POSTerminalID
		  , @POSTillID
		  , @POSDateTime
		  , @POSDateTimeLocal
		  , @SubTotal
		  , @TaxAmount
		  , @Total
		  , @BasketStateID
		  , @BasketLoyaltyStateID
		  , @BasketLoyaltyPrograms
		  , @QRCodeText
		  , @BatchID
		  , @PartnerTransactionID
		  , @StacString
		  , [dbo].[IsSiteBillable] (@StoreTenantID, @StoreID)
		  , @AppDisplayName
		  , @AppChannel
          , @FleetTenantID
		  , @OtherRewardTotal
          , @CurrencyIsoCode
		   );


MERGE dbo.BasketLineItem AS target
USING
	(SELECT	*
	 FROM	@BasketLineItem) AS source
ON (target.BasketLineItemID = source.BasketLineItemID)
WHEN MATCHED THEN
	UPDATE SET POSLineItemID = source.POSLineItemID
			 , UPC = ISNULL(source.UPC, target.UPC)
			 , PCATSCode = ISNULL(source.PCATSCode, target.PCATSCode)
			 , UnitOfMeasure = ISNULL(source.UnitOfMeasure, target.UnitOfMeasure)
			 , ItemDescription = source.ItemDescription
			 , SellingUnit = ISNULL(source.SellingUnit, target.SellingUnit)
			 , RegularPrice = ISNULL(source.RegularPrice, target.RegularPrice)
			 , SalesPrice = ISNULL(source.SalesPrice, target.SalesPrice)
			 , Quantity = source.Quantity
			 , ExtendedPrice = source.ExtendedPrice
			 , TaxAmount = ISNULL(source.TaxAmount, target.TaxAmount)
			 , TaxRate = ISNULL(source.TaxRate, target.TaxRate)
			 , ExtendedAmount = source.ExtendedAmount
			 , LoyaltyRewardID = ISNULL(source.LoyaltyRewardID, target.LoyaltyRewardID)
			 , LoyaltyRewardAmount = ISNULL(source.LoyaltyRewardAmount, target.LoyaltyRewardAmount)
			 , BasketLineItemStateID = source.BasketLineItemStateID
			 , CarWashCode = ISNULL(source.CarWashCode, target.CarWashCode)
			 , CarWashCodeExpiration = ISNULL(source.CarWashCodeExpiration, target.CarWashCodeExpiration)
			 , CarWashText = ISNULL(source.CarWashText, target.CarWashText)
			 , LoyaltyDescription = ISNULL(source.LoyaltyDescription, target.LoyaltyDescription)
			 , LoyaltyRewardUnitAmount = ISNULL(source.LoyaltyRewardUnitAmount, target.LoyaltyRewardUnitAmount)
			 , LoyaltyRewardQuantity = ISNULL(source.LoyaltyRewardQuantity, target.LoyaltyRewardQuantity)
			 , LoyaltyRewardSellingUnit = ISNULL(source.LoyaltyRewardSellingUnit, target.LoyaltyRewardSellingUnit)
			 , OtherLoyaltyRewardAmount = ISNULL(source.OtherLoyaltyRewardAmount, target.OtherLoyaltyRewardAmount)
			 , P97RewardDiscount = ISNULL(source.P97RewardDiscount, target.P97RewardDiscount)
			 , ExternalRewardDiscount = ISNULL(source.ExternalRewardDiscount, target.ExternalRewardDiscount)
			 , UpdatedOn = GETUTCDATE()
WHEN NOT MATCHED THEN
	INSERT (
			BasketID
		  , BasketLineItemID
		  , POSLineItemID
		  , UPC
		  , PCATSCode
		  , UnitOfMeasure
		  , ItemDescription
		  , SellingUnit
		  , RegularPrice
		  , SalesPrice
		  , Quantity
		  , ExtendedPrice
		  , TaxAmount
		  , TaxRate
		  , ExtendedAmount
		  , LoyaltyRewardID
		  , LoyaltyRewardAmount
		  , BasketLineItemStateID
		  , CarWashCode
		  , CarWashCodeExpiration
		  , CarWashText
		  , LoyaltyDescription
		  , LoyaltyRewardUnitAmount
		  , LoyaltyRewardQuantity
		  , LoyaltyRewardSellingUnit
		  , OtherLoyaltyRewardAmount
		  , P97RewardDiscount
		  , ExternalRewardDiscount
		   )
	VALUES (
			source.BasketID
		  , source.BasketLineItemID
		  , source.POSLineItemID
		  , source.UPC
		  , source.PCATSCode
		  , source.UnitOfMeasure
		  , source.ItemDescription
		  , source.SellingUnit
		  , source.RegularPrice
		  , source.SalesPrice
		  , source.Quantity
		  , source.ExtendedPrice
		  , source.TaxAmount
		  , source.TaxRate
		  , source.ExtendedAmount
		  , source.LoyaltyRewardID
		  , source.LoyaltyRewardAmount
		  , source.BasketLineItemStateID
		  , source.CarWashCode
		  , source.CarWashCodeExpiration
		  , source.CarWashText
		  , source.LoyaltyDescription
		  , source.LoyaltyRewardUnitAmount
		  , source.LoyaltyRewardQuantity
		  , source.LoyaltyRewardSellingUnit
		  , source.OtherLoyaltyRewardAmount
		  , source.P97RewardDiscount
		  , source.ExternalRewardDiscount
		   );

IF @RefBasketID1 IS NOT NULL
	INSERT	INTO dbo.BasketLinkBasket
			(BasketID1, BasketID2)
	VALUES	(@RefBasketID1, @BasketID)

IF EXISTS ( SELECT	*
			FROM	@BasketPayment )
	EXEC dbo.InsertBasketPayment @BasketPayment
END

