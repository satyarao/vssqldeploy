﻿-- =============================================
-- Author:		Daniel DiPaolo
-- Create date: 2015/06/15
-- Description:	Creates a STAC (Single Transaction Auth Code) for a POS to be paid for by a PetroZone mobile customer
-- =============================================
CREATE PROCEDURE [dbo].[InsertPosStac] 
	-- Add the parameters for the stored procedure here
	@StoreId uniqueidentifier,
	@Amount decimal(9,2),
	@Token uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @Token = ISNULL(@Token, NEWID())

	DECLARE @Expiration DATETIME2(7)
	SET @Expiration =  DATEADD(minute, 30, GETUTCDATE())

	INSERT INTO STAC(
		Token,
		StoreID,
		Amount,
		Expiration)
	VALUES(
		@Token,
		@StoreId,
		@Amount,
		@Expiration)

	DECLARE @StoreName NVARCHAR(256)
	SELECT
		@StoreName = Name
	FROM
		Store
	WHERE
		StoreID = @StoreId
			
	IF @StoreName IS NULL
		BEGIN; --RAISERROR(14043, 16, 1, 'StoreName', '[InsertPosStac]');
			THROW 50001, 'StoreName not found', 16
		END	

	SELECT
		@Token as Token
		,@Expiration as Expiration
		,@StoreId as StoreID
		,@StoreName as StoreName
		,@Amount as Amount
END
