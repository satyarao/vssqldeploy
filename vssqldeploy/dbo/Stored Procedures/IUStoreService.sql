﻿--drop PROC dbo.IUStoreService
/*
--delete FROM dbo.StoreService WHERE StoreID = '00000000-0000-0000-0000-000000000000'
DECLARE @new bit = 1
DECLARE	@StoreService AS StoreServiceType

INSERT	INTO @StoreService (StoreID, ServiceID, IsActive)
VALUES	('00000000-0000-0000-0000-000000000000',1,1)
INSERT	INTO @StoreService (StoreID, ServiceID, IsActive)
VALUES	('00000000-0000-0000-0000-000000000000',2,1)
INSERT	INTO @StoreService (StoreID, ServiceID, IsActive)
VALUES	('00000000-0000-0000-0000-000000000000',3,0)

SELECT * FROM dbo.StoreService WHERE StoreID = '00000000-0000-0000-0000-000000000000'
EXEC dbo.IUStoreService @StoreService
SELECT * FROM dbo.StoreService WHERE StoreID = '00000000-0000-0000-0000-000000000000'
*/

CREATE PROC [dbo].[IUStoreService]
	@StoreService StoreServiceType READONLY
	,@StoreID UNIQUEIDENTIFIER = NULL
AS
SET NOCOUNT ON

DECLARE	@UpdateStoreId UNIQUEIDENTIFIER
SET @UpdateStoreId = ISNULL(@StoreID, (SELECT TOP 1 StoreID FROM @StoreService))

MERGE dbo.StoreService AS target
USING
	(SELECT	*
	 FROM	@StoreService) AS source
ON (target.StoreID = @UpdateStoreId
	AND target.ServiceID = source.ServiceID)
WHEN MATCHED THEN
	UPDATE SET IsActive = source.IsActive
			 , UpdatedOn = GETUTCDATE()
WHEN NOT MATCHED THEN
	INSERT (StoreID
		  , ServiceID
		  , IsActive)
	VALUES (@UpdateStoreId
		  , source.ServiceID
		  , source.IsActive)
WHEN NOT MATCHED BY SOURCE AND target.StoreID = @UpdateStoreId THEN
UPDATE SET IsActive = 0
		 , UpdatedOn = GETUTCDATE();


