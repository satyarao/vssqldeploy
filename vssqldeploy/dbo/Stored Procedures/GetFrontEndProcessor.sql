﻿CREATE PROCEDURE [dbo].[GetFrontEndProcessor]
	@TenantID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	IF @TenantID IS NULL
	BEGIN
		SELECT [FrontEndID],
		[FrontEndTypeName],
		[IsActive]
		FROM [dbo].[FrontEndProcessor]
		WHERE IsActive = 1
	END
	ELSE
	BEGIN
		--If TenantID was passed as a StoreID, look for its TenantID
		IF NOT EXISTS(SELECT 1 FROM [dbo].[Tenant] WHERE TenantID = @TenantID)
			SET @TenantID = (SELECT TOP 1 TenantID FROM [dbo].[Store]
							WHERE StoreID = @TenantID)
		DECLARE @FrontEndName NVARCHAR(50);
		SET @FrontEndName = [dbo].[GetTenantPropertyValue] (@TenantID, 'OutsidePaymentProvider', DEFAULT);
		
		IF @FrontEndName IS NOT NULL
		BEGIN
			SELECT TOP 1
				[FrontEndID],
				[FrontEndTypeName],
				[IsActive]
			FROM [dbo].[FrontEndProcessor]
			WHERE IsActive = 1
			AND FrontEndTypeName = @FrontEndName;
		END
	END
END

--EXEC dbo.GetFrontEndProcessor @TenantID = 'f12b5631-5001-44c0-98ca-00badea037b5'
