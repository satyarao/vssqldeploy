﻿
CREATE PROCEDURE [dbo].[InsertUserTrackLocations]
	@UserId UNIQUEIDENTIFIER,
	@Coordinates [dbo].[CoordinatesType] READONLY
AS
BEGIN

	INSERT INTO [dbo].[UserTrackLocation] (UserId, Longitude, Latitude, GeoCoordinates, Timestamp)
	SELECT
		  @UserId
		, c.Longitude
		, c.Latitude
		, GEOGRAPHY::STGeomFromText('POINT(' + CONVERT(varchar(12),c.Longitude) + ' ' + CONVERT(varchar(12),c.Latitude) + ')',4326)
		, c.Timestamp
	FROM @Coordinates c
END
