﻿-- =============================================
-- Author:		Ingram Leonards
-- Create date: 5/29/2015
-- Description:	Retrieve user favorites
-- =============================================
CREATE PROCEDURE [dbo].[GetFavorites] 
	@DeviceRegistrationId UNIQUEIDENTIFIER = NULL, 
	@InstallationId NVARCHAR(128) = NULL,
	@UserId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @UserId = '00000000-0000-0000-0000-000000000000'
		BEGIN
			SET @UserId = NULL
		END
	IF @DeviceRegistrationId = '00000000-0000-0000-0000-000000000000'
		BEGIN
			SET @DeviceRegistrationId = NULL
		END

	IF @UserId IS NULL
		BEGIN
			IF @DeviceRegistrationId IS NOT NULL
				BEGIN
					SELECT @UserId = ui.UserID
					FROM dbo.UserInfo ui
					INNER JOIN dbo.DeviceRegistration dr ON dr.UserID = ui.UserID
					WHERE dr.DeviceRegistrationID = @DeviceRegistrationId
				END
			ELSE 
				BEGIN
					SELECT @UserId = ui.UserID
					FROM dbo.UserInfo ui
					INNER JOIN dbo.DeviceRegistration dr ON dr.UserID = ui.UserID
					WHERE dr.InstallationID = @InstallationId
				END
		END

	IF @UserId IS NULL
		BEGIN
			SELECT NULL
		END
	ELSE
		BEGIN
			SELECT FavID as FavId, StoreID as StoreId, UserID as UserId
			FROM dbo.Favorite
			WHERE UserID = @UserId
		END
END
