﻿
-- =============================================
-- Author:		Roman Davydovich
-- Create date: 2019-04-09
-- Description:	Get changes from flatbasket table to apply it to azure search index
-- Update date: 04/25/2019 - Alex Goroshko - added [AppDisplayName], [AppChannel] columns
-- =============================================

CREATE PROCEDURE [dbo].[GetTransactionChanges]
AS
BEGIN
	DECLARE @CurrentVersion INT = (SELECT CHANGE_TRACKING_CURRENT_VERSION());
	DECLARE @BaseVersion INT = (SELECT [Version] FROM ChangeTrackerVersion WHERE [ChangeTracker] = 'Transaction')
	
	SELECT LOWER(CONVERT(nvarchar(50), CT.[FlatBasketID])) AS [FlatBasketID], [TransactionID], [StoreTenantID], [AppTenantID], [BasketStateName], [BasketStateID], [BasketLoyaltyStateID], [BasketLoyaltyStateName], [UserID], [StoreMppaID], [UserEmail], [POSOperatorID], [POSTerminalID], [POSTransactionID], [PartnerTransactionID], [StoreName], [StoreAddress], [StoreCity], [StoreID], [StoreNumber], [StoreMerchantID], [POSDateTimeLocal], [POSDateTime], CONVERT(nvarchar(50), [OriginalPrice]) AS OriginalPrice, CONVERT(nvarchar(50),[ProductDiscount]) AS ProductDiscount, CONVERT(nvarchar(50),[FuelDiscount]) AS FuelDiscount, CONVERT(nvarchar(50),[TotalDiscount]) AS TotalDiscount, CONVERT(nvarchar(50),[SubTotal]) AS SubTotal, CONVERT(nvarchar(50),[TaxAmount]) AS TaxAmount, CONVERT(nvarchar(50),[TotalAmount]) AS TotalAmount,  [FundingProviderID], [FundingProviderName], [CardType], [BatchID], [SettlementPeriodID], [Stac], [IsSiteBillable], [AuthCode], [IsSiteLevelPayment], [VisibleByTenants], [PaymentInfo], [OfferName], [OfferID], [BasketLoyaltyPrograms], [AppDisplayName], [AppChannel], [Odometer], [LicensePlate], [LicenseNumber], [CustomerDriverID], [FleetTenantID], [NuDetectScore], [NuDetectScoreBand], [ScoreSignals], [CurrencyIsoCode], [NumberOfGallons] 
	FROM[dbo].[FlatBasket] AS fb RIGHT OUTER JOIN CHANGETABLE(CHANGES[dbo].[FlatBasket], @BaseVersion) AS CT ON fb.FlatBasketID = CT.FlatBasketID AND (CT.SYS_CHANGE_OPERATION = 'I' OR CT.SYS_CHANGE_OPERATION = 'U')
	
	UPDATE ChangeTrackerVersion SET [Version] = @CurrentVersion  WHERE [ChangeTracker] = 'Transaction'
END
