﻿CREATE PROCEDURE [dbo].[IUBatch]
  @BatchID INT = NULL
, @StoreID UNIQUEIDENTIFIER
, @IsClosed BIT = NULL
, @IsMisMatch BIT = NULL
, @TotalTransactions INT = NULL
, @TotalAmount DECIMAL(9,2) = NULL
, @RefData NVARCHAR(MAX) = NULL
, @SettlementPeriodID NVARCHAR(50) = NULL
, @BusinessDate DATETIME2(7) = NULL
, @UMTI NVARCHAR(50) = NULL
AS
BEGIN
MERGE [dbo].[MppaBatch] AS target
USING
	(SELECT	@BatchID
			, @StoreID
			, @IsClosed
			, @IsMisMatch
			, @TotalTransactions
			, @TotalAmount
			, @RefData 
			, @SettlementPeriodID
			, @BusinessDate
			, @UMTI)
    AS source (BatchID
			, StoreID
			, IsClosed
			, IsMisMatch
			, TotalTransactions
			, TotalAmount
			, RefData 
			, SettlementPeriodID
			, BusinessDate
			, UMTI)
ON (target.BatchID = source.BatchID)
WHEN MATCHED THEN
	UPDATE SET IsClosed = ISNULL(source.IsClosed, target.IsClosed)
			, IsMisMatch = ISNULL(source.IsMisMatch, target.IsMisMatch)
			, TotalTransactions = ISNULL(source.TotalTransactions, target.TotalTransactions)
			, TotalAmount = ISNULL(source.TotalAmount, target.TotalAmount)
			, RefData = ISNULL(source.RefData, target.RefData)
			, SettlementPeriodID = ISNULL(source.SettlementPeriodID, target.SettlementPeriodID)
			, BusinessDate = ISNULL(source.BusinessDate, target.BusinessDate)
			, UMTI = ISNULL(source.UMTI, target.UMTI)
			, UpdatedOn = GETUTCDATE()
WHEN NOT MATCHED THEN
	INSERT
           ([StoreID]
			,[IsClosed]
			,[IsMisMatch]
			,[TotalTransactions]
			,[TotalAmount]
			,[RefData]
			,[SettlementPeriodID]
			,[BusinessDate]
			,[UMTI]
            ,[CreatedOn])
     VALUES
           (@StoreID
		    , ISNULL(@IsClosed, 0)
			, ISNULL(@IsMisMatch, 0)
			, @TotalTransactions
			, @TotalAmount
			, @RefData 
			, @SettlementPeriodID
			, @BusinessDate
			, @UMTI
            , GETUTCDATE());
END
