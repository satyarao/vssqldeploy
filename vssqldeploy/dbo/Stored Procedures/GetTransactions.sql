﻿-- =============================================
-- Create date: 1/27/2016
-- Update date: 03/23/2017
-- Description:	Gets user's recent transactions
-- =============================================
CREATE PROCEDURE [dbo].[GetTransactions] 
	@UserID UNIQUEIDENTIFIER
	,@StartDate DATE
	,@EndDate DATE
AS
BEGIN
	SET NOCOUNT ON;
	IF (@UserID IS NOT NULL)
	BEGIN
		SELECT 
			b.BasketID as BasketId
			, b.StoreId
			, store.StreetAddress as Address
			, store.Name
			, b.POSDateTime
			, b.POSDateTimeLocal
			, '$' + CAST(b.Total as VARCHAR(10)) as FormattedTotal
			, CASE WHEN b.POSOperatorID IN ('PZOutside', 'Outside') THEN CAST(0 AS bit)
				ELSE CAST(1 AS bit)
				END as InStorePurchase
		FROM dbo.Basket b 
		LEFT JOIN [dbo].[Store] store ON b.StoreID = store.StoreID
		LEFT JOIN [dbo].[LKBasketState] lkbs on lkbs.BasketStateID = b.BasketStateID
		WHERE b.UserID = @UserID
		AND CONVERT(DATE, b.POSDateTime) BETWEEN ISNULL(@StartDate, CONVERT(DATE, '1/1/1753')) AND ISNULL(@EndDate, CONVERT(DATE, GETUTCDATE()))
		AND lkbs.Name IN ('Complete')
		ORDER BY POSDateTime DESC
	END
	ELSE
	BEGIN
		SELECT NULL
	END
END
