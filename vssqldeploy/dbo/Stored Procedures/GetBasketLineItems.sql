﻿CREATE PROCEDURE [dbo].[GetBasketLineItems]
	@BasketID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

    SELECT BasketLineItemID ,
          BasketID ,
          POSLineItemID ,
          UPC ,
          PCATSCode ,
          UnitOfMeasure ,
          ItemDescription ,
          SellingUnit ,
          RegularPrice ,
          SalesPrice ,
          Quantity ,
          ExtendedPrice ,
          TaxAmount ,
          TaxRate ,
          ExtendedAmount ,
          LoyaltyRewardID ,
          LoyaltyRewardAmount ,
          BasketLineItemStateID ,
		  CarWashCode ,
		  CarWashCodeExpiration
	FROM dbo.BasketLineItem
	WHERE BasketID = @BasketID
END


