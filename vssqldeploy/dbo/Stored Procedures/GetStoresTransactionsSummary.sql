﻿-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 4/27/2017
-- Update date: 4/27/2017 - add fields for sorting
-- Update date: 5/11/2017 - only for completed transactions
-- Update date: 5/24/2017 - fixed tenant filter (now for transactions, not for stores)
-- Update date: 08/07/2017 - Update for TenantId change
-- Update date: 12/08/2017 - Igor Gaidukov - added ability to show offers to partners
-- Update date: 01/11/2018 - Igor Gaidukov - added OfferId as a parameter
-- Update date: 07/09/2018 - Vadim Prikich - changed OfferId data type from uniqueidentifier to nvarchar
-- Update date: 10/03/2018 - Dzmitry Mikhailau - improve timeout.
-- Update date: 01/28/2019 - Vadim Prikich - updated sp to get offer report for partner tenant
-- Update date: 03/07/2019 - Davydovich Raman - try to improve perfomance
-- Update date: 03/18/2019 - Davydovich Raman - try to improve perfomance
-- Description:	Returns summary for stores transactions
-- =============================================

CREATE PROCEDURE [dbo].[GetStoresTransactionsSummary]
	@TenantID UNIQUEIDENTIFIER = NULL
	,@StoreName NVARCHAR(256) = NULL
	,@SortBy nvarchar(100) = NULL
	,@Offset INT
	,@Limit INT
	,@FromDate DATETIME2(7)
	,@ToDate DATETIME2(7)
	,@OfferId NVARCHAR(256) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @TempTable table(
		StoreID UNIQUEIDENTIFIER NOT NULL,
		StoreName NVARCHAR(256),
		TenantID UNIQUEIDENTIFIER NOT NULL,
		TenantName NVARCHAR(255),
		TotalTransactions INT,
		RedeemedOffers INT,
		TotalAmount DECIMAL(9,2),
		TotalDiscount DECIMAL(9,2)
	);

	WITH RedeemedOffersResults AS
	(
		SELECT	b.[BasketID],
				COUNT(DISTINCT( TempOfferItems.OfferId  )) AS RedeemedOffers, 
				SUM(ISNULL(TempOfferItems.OfferAmount, 0)) as TotalDiscount
		FROM BasketLineItem b 
		LEFT JOIN Reports.BasketLineItem_LoyaltyDescription TempOfferItems ON TempOfferItems.BasketLineItemID = b.BasketLineItemID
		LEFT JOIN Reports.FlatBasket_VisibleByTenants VisibleByTenants ON VisibleByTenants.TransactionID = b.BasketID AND VisibleByTenants.PartnerTenantId = @TenantID
		WHERE b.[CreatedOn] BETWEEN @FromDate AND @ToDate AND ( @OfferId IS NULL OR TempOfferItems.OfferId = @OfferId )			
		GROUP BY b.[BasketID]
	)
	, TransactionsResults AS
	(
		SELECT b.[StoreID],
			   COUNT(*) as TotalTransactions, 
			   SUM(ro.RedeemedOffers) as RedeemedOffers, 
			   SUM(ISNULL(Total, 0)) as TotalAmount, 
			   SUM(ro.TotalDiscount) as TotalDiscount
		FROM RedeemedOffersResults ro, [dbo].[Basket] b
		LEFT JOIN Reports.FlatBasket_VisibleByTenants VisibleByTenants ON VisibleByTenants.TransactionID = b.BasketID AND VisibleByTenants.PartnerTenantId = @TenantID
		WHERE (b.[POSDateTime] BETWEEN @FromDate AND @ToDate) 
		AND ro.BasketID = b.[BasketID] 
		AND b.[BasketStateID] = 3
		AND (@TenantID IS NULL
						OR
					( b.StoreTenantID = @TenantID ) 
						OR 
					( b.StoreTenantID != @TenantID AND b.AppTenantId = @TenantID)
						OR
					( b.StoreTenantID != @TenantID AND b.AppTenantId != @TenantID AND VisibleByTenants.PartnerTenantID = @TenantID))
		GROUP BY b.[StoreID]
	)

	INSERT INTO @TempTable(StoreID, StoreName, TenantID, TenantName, TotalTransactions, RedeemedOffers, TotalAmount, TotalDiscount)
	SELECT s.[StoreID], s.[Name] AS StoreName, s.[TenantID], t.[Name] AS TenantName, r.TotalTransactions, r.RedeemedOffers, r.TotalAmount, r.TotalDiscount
	FROM TransactionsResults r 
	JOIN [dbo].[Store] s ON r.[StoreID] = s.[StoreID]
	JOIN [dbo].[Tenant] t ON s.[TenantID] = t.[TenantID]
	WHERE (@StoreName IS NULL OR s.[Name] LIKE '%' + @StoreName + '%')
		
	SELECT s.StoreID, s.StoreName, s.TenantID, s.TenantName, s.TotalTransactions, s.RedeemedOffers, s.TotalAmount, s.TotalDiscount
	FROM @TempTable s
	ORDER BY
		    CASE WHEN @SortBy = 'storeName' THEN s.StoreName END ASC,
			CASE WHEN @SortBy = '-storeName' THEN s.StoreName END DESC,
			CASE WHEN @SortBy = 'transactionsCnt' THEN s.TotalTransactions END ASC,
			CASE WHEN @SortBy = '-transactionsCnt' THEN s.TotalTransactions END DESC,
			CASE WHEN @SortBy = 'redeemedCnt' THEN s.RedeemedOffers END ASC,
			CASE WHEN @SortBy = '-redeemedCnt' THEN s.RedeemedOffers END DESC,
			CASE WHEN @SortBy = 'totalAmount' THEN s.TotalAmount END ASC,
			CASE WHEN @SortBy = '-totalAmount' THEN s.TotalAmount END DESC,
			CASE WHEN @SortBy = 'totalDiscount' THEN s.TotalDiscount END ASC,
			CASE WHEN @SortBy = '-totalDiscount' THEN s.TotalDiscount END DESC

	OFFSET @Offset ROWS 
	FETCH NEXT @Limit ROWS ONLY;
	
	SELECT 
		COUNT(*) as TotalRows
		, SUM(TotalTransactions) as SumTotalTransactions
		, SUM(RedeemedOffers) as SumTotalRedeemedOffers
		, SUM(TotalAmount) as SumTotalAmount
		, SUM(TotalDiscount) as SumDiscountAmount
	FROM @TempTable
END
