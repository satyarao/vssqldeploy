﻿CREATE PROC [dbo].[GetSRPInfo]
	@installationID NVARCHAR(128)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT	InstallationID
	  , Salt
	  , SafePrime
	  , Generator
	  , Verifier
	  , SessionTokenVerifier
	  , EphemeralSessionTokenVerifier
	  , PrivateServerEphemeral1
	  , PrivateServerEphemeral2
	  , PublicServerEphemeral1
	  , PublicServerEphemeral2
	  , RemainingAttempts
--*, InstallationID AS InstallId
FROM	[dbo].[SRP]
WHERE	[InstallationID] = @installationID
