﻿
CREATE PROCEDURE [dbo].[GetSiteInstallInfo]
	@StoreID UNIQUEIDENTIFIER = NULL
  , @TenantID UNIQUEIDENTIFIER = NULL
  , @DispenserTypeName NVARCHAR(64) = NULL
  , @POSTypeName NVARCHAR(64) = NULL
  , @FCTypeName NVARCHAR(64) = NULL
  , @OnlyActive BIT = 1
AS
BEGIN
	SET NOCOUNT ON;

	IF (@StoreID IS NOT NULL AND @TenantID IS NOT NULL
	AND NOT EXISTS(SELECT 1 FROM DBO.SiteInstallInfo 
		WHERE StoreID = @StoreID AND TenantID = @TenantID))
	BEGIN
		SELECT @StoreID AS StoreID
		  , @TenantID AS TenantID
		  , [dbo].[GetGeneratedStoreKey] (@StoreID) AS AesKey
		  , MPPAID
		  FROM [dbo].[Store]
		  WHERE StoreID = @StoreID AND
				TenantID = @TenantID
	END 
	ELSE
	BEGIN
		SELECT SII.TenantID
		  ,SII.StoreID
		  ,S.MPPAID
		  ,[dbo].[GetGeneratedStoreKey] (SII.StoreID) AS AesKey
		  ,SII.IsSelfServe
		  ,SII.IsInStoreEnabled
		  ,SII.ForecourtIP
		  ,SII.SecondNetworkCom
		  ,SII.VirtualPrinterPort
		  ,SII.DispenserTypeID
		  ,DIS.DispenserTypeName
		  ,DIS.IsActive AS IsDispenserActive
		  ,SII.POSTypeID
		  ,POS.POSTypeName
		  ,POS.IsActive AS IsPOSActive
		  ,SII.FCTypeID
		  ,FC.FCTypeName
		  ,FC.IsActive AS IsFCActive
		  ,SII.IsActive
		FROM DBO.SiteInstallInfo AS SII
		LEFT JOIN DBO.LKDispenserType AS DIS
		ON SII.DispenserTypeID = DIS.DispenserTypeID 
		LEFT JOIN DBO.LKPOSType AS POS
		ON SII.POSTypeID = POS.POSTypeID 
		LEFT JOIN DBO.LKFCType AS FC
		ON SII.FCTypeID = FC.FCTypeID 
		INNER JOIN Store AS S ON 
		S.StoreID = SII.StoreID
		WHERE 	SII.StoreID = ISNULL(@StoreID, SII.StoreID)
			AND SII.TenantID = ISNULL(@TenantID, SII.TenantID)
			AND (DIS.DispenserTypeName = @DispenserTypeName OR @DispenserTypeName IS NULL)
			AND (POS.POSTypeName = @POSTypeName OR @POSTypeName IS NULL)
			AND (FC.FCTypeName = @FCTypeName OR @FCTypeName IS NULL)
			AND SII.IsActive = ISNULL(@OnlyActive, SII.IsActive);
		--Note: (FC.FCTypeName = @FCTypeName OR @FCTypeName IS NULL) is the same as FC.FCTypeName = ISNULL(@FCTypeName, FC.FCTypeName) 
		--but doesn't work for where clause because FC.FCTypeName might be a null value.
	END
END

--[dbo].[GetSiteInstallInfo] @StoreID = 'C5F073EC-3799-4695-82B8-008A49FEACF3'
--, @TenantID = '91AB977C-6784-44AD-B670-AE4B039F8ED5'
--, @DispenserTypeName = NULL
--, @POSTypeName = NULL
--, @FCTypeName = NULL
--, @OnlyActive = NULL
