﻿-- =============================================
-- Updated by:      Roman Lavreniuk
-- Update date:     03/15/2018
-- Description:     https://p97networks.atlassian.net/browse/PLAT-2091
-- =============================================

CREATE PROC [dbo].[IUDUserPreference]
	@UserID UNIQUEIDENTIFIER
  , @PaperReceipt BIT = NULL
  , @EmailReceipt BIT = NULL
  , @NotificationReceipt BIT = NULL
  , @FuelGrade NVARCHAR(50) = NULL
  , @UserPreferenceIsActive BIT = 1
  , @OptInMarketingEmail BIT = NULL
  , @Sms BIT = NULL
  , @OptInMarketingSms BIT = NULL
  , @Push BIT = NULL
  , @OptInMarketingPush BIT = NULL

AS
BEGIN
	SET NOCOUNT ON

	IF EXISTS ( SELECT	UserID
				FROM	dbo.UserPreference
				WHERE	UserID = @UserID )
		BEGIN
			UPDATE	dbo.UserPreference
			SET		UserID = @UserID
					, PaperReceipt =  ISNULL(@PaperReceipt, PaperReceipt)
					, EmailReceipt = ISNULL(@EmailReceipt, EmailReceipt)
                    , NotificationReceipt = ISNULL(@NotificationReceipt, NotificationReceipt)
					, FuelGrade = ISNULL(@FuelGrade, FuelGrade)
					, IsActive = @UserPreferenceIsActive
					, UpdatedOn = GETUTCDATE()
					, OptInMarketingEmail = ISNULL(@OptInMarketingEmail, OptInMarketingEmail)
					, Sms = ISNULL(@Sms, Sms)
					, OptInMarketingSms = ISNULL(@OptInMarketingSms, OptInMarketingSms)
					, Push = ISNULL(@Push, Push)
					, OptInMarketingPush = ISNULL(@OptInMarketingPush, OptInMarketingPush)
			WHERE	UserID = @UserID
		END 
	ELSE
		BEGIN

			INSERT	INTO dbo.UserPreference
					( UserID
					, PaperReceipt
					, EmailReceipt
                    , NotificationReceipt
					, FuelGrade
					, IsActive
					, CreatedOn
					, OptInMarketingEmail
					, Sms
					, OptInMarketingSms
					, Push
					, OptInMarketingPush
					)
			VALUES	( @UserID
					, ISNULL(@PaperReceipt, 0)
					, ISNULL(@EmailReceipt, 1)
                    , ISNULL(@NotificationReceipt, 0)
					, ISNULL(@FuelGrade, 'R')
					, @UserPreferenceIsActive
					, GETUTCDATE()
					, ISNULL(@OptInMarketingEmail, 1)
					, ISNULL(@Sms, 0)
					, ISNULL(@OptInMarketingSms, 0)
					, ISNULL(@Push, 1)
					, ISNULL(@OptInMarketingPush, 1)
					)
		END
END


