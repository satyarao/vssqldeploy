﻿
CREATE   PROCEDURE [dbo].[InsertUserGeoLocation]
	@UserID    uniqueidentifier,
	@ChannelID uniqueidentifier,
	@Latitude  decimal(9,6),
	@Longitude decimal(9,6),
	@EventType nvarchar(100) = NULL
AS
BEGIN TRY
	BEGIN TRANSACTION
		IF @EventType IS NULL
		BEGIN
			SET @EventType = 'App Home'
		END
	
		INSERT INTO [dbo].[UserGeoLocationLog]
			([UserID], [ChannelID], [Latitude], [Longitude], [CreatedOn], [EventType])
		VALUES
			(@UserID, @ChannelID, @Latitude, @Longitude, getutcdate(), @EventType)
	COMMIT TRANSACTION
	RETURN;
END TRY
BEGIN CATCH
	DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @UserID                  [UserID],
	        @Latitude                [Latitude],
			@Longitude               [Longitude],
			@EventType               [EventType]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
