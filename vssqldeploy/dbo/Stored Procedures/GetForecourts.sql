﻿
CREATE PROCEDURE [dbo].[GetForecourts]
	@POSTypeID tinyint = NULL
AS
SET NOCOUNT ON;	

IF @POSTypeID IS NULL
	BEGIN
		SELECT *
		FROM dbo.LKFCType
		WHERE IsActive = 1;
	END
ELSE
	BEGIN
		SELECT *
		FROM dbo.LKFCType
		WHERE IsActive = 1 
			AND	FCTypeID in
				(SELECT FCTypeID
				FROM dbo.LKPOSType_FCType
				WHERE POSTypeID = @POSTypeID);
	END


