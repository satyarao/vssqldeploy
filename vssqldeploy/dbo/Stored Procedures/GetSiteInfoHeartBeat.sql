﻿-- =============================================
-- Author:		Jamy Ryals
-- Create date: 5/19/2015
-- Description:	Returns store status
-- =============================================
CREATE PROCEDURE GetSiteInfoHeartBeat
	-- Add the parameters for the stored procedure here
	@StoreID UNIQUEIDENTIFIER = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF @StoreID IS NULL
		BEGIN	
		SELECT [StoreID], [HeartBeatStatus] FROM [dbo].[SiteInfo]
		END
	ELSE	
		BEGIN
		SELECT [StoreID], [HeartBeatStatus] FROM [dbo].[SiteInfo] WHERE StoreID = @StoreID
		END
END
