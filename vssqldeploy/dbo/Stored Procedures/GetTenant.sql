﻿CREATE PROCEDURE [dbo].[GetTenant]
	@IsActive BIT = NULL
  , @TenantTypeID INT = NULL
  , @Report BIT = 0
AS /*
EXEC dbo.GetTenant
	@IsActive = 1
  , @Report = 1
*/

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN
	SELECT	NULL AS TenantID
		, 'All' AS Name
		FROM	dbo.Tenant
		WHERE @Report = 1
	UNION
	SELECT	TenantID
			, Name
		FROM	dbo.Tenant
		WHERE	@IsActive IS NULL
				OR IsActive = @IsActive
				AND (
						(
						@TenantTypeID IS NULL
						--AND TenantTypeID NOT IN (1, 5)
						)
						OR TenantTypeID = @TenantTypeID
					)
END
