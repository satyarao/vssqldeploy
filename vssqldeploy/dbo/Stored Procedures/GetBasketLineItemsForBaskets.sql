﻿
Create PROCEDURE [dbo].[GetBasketLineItemsForBaskets]
	@BasketIDs BasketListType READONLY 
AS
BEGIN
	SET NOCOUNT ON;

    SELECT BasketLineItemID ,
          BasketID ,
          POSLineItemID ,
          UPC ,
          PCATSCode ,
          UnitOfMeasure ,
          ItemDescription ,
          SellingUnit ,
          RegularPrice ,
          SalesPrice ,
          Quantity ,
          ExtendedPrice ,
          TaxAmount ,
          TaxRate ,
          ExtendedAmount ,
          LoyaltyRewardID ,
          LoyaltyRewardAmount ,
          BasketLineItemStateID
	FROM dbo.BasketLineItem
	WHERE BasketID IN (SELECT BasketID FROM @BasketIDs)
END
