﻿CREATE PROC [dbo].[GetStoreService]
	@StoreID UNIQUEIDENTIFIER
AS /*
EXEC dbo.GetStoreService @StoreID = '00000000-0000-0000-0000-000000000000'
*/
SET NOCOUNT ON
SELECT	StoreID
	  , ServiceID
	  , IsActive
FROM	dbo.StoreService
WHERE	StoreID = @StoreID AND IsActive = 1
