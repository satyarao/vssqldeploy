﻿-- =============================================
-- Author:     Tom Vuong
-- Description: Insert p97TransactionId and return report transaction id
-- =============================================
CREATE PROCEDURE [dbo].[GetReportTransaction]
(
    @P97TransactionId uniqueidentifier,
	@ReportTransactionIdLength Int
)
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @Error as int
	BEGIN TRY
		INSERT INTO [dbo].[ReportTransactionMap]
			   ([P97TransactionId]
			   ,[ReportTransactionIdLength])
		 VALUES
			   (@P97TransactionId, @ReportTransactionIdLength)
		SELECT [ID]
		  ,[P97TransactionId]
		  ,[ReportTransactionIdLength]
		  ,[ReportTransactionId]
		FROM [dbo].[ReportTransactionMap]
		WHERE [P97TransactionId] = @P97TransactionId
	END TRY
	BEGIN CATCH
		SET @Error = ERROR_NUMBER()
		IF @Error = 2627
		SELECT [ID]
		  ,[P97TransactionId]
		  ,[ReportTransactionIdLength]
		  ,[ReportTransactionId]
		FROM [dbo].[ReportTransactionMap]
		WHERE [P97TransactionId] = @P97TransactionId
	END CATCH
END
