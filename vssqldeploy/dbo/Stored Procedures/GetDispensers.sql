﻿
CREATE PROCEDURE [dbo].[GetDispensers]
	@FCTypeID tinyint = NULL
AS
SET NOCOUNT ON;	

IF @FCTypeID IS NULL
	BEGIN
		SELECT *
		FROM dbo.LKDispenserType
		WHERE IsActive = 1;
	END
ELSE
	BEGIN
		SELECT *
		FROM dbo.LKDispenserType
		WHERE IsActive = 1 
			AND	DispenserTypeID in
				(SELECT DispenserTypeID
				FROM dbo.LKFCType_DispenserType
				WHERE FCTypeID = @FCTypeID);
	END


