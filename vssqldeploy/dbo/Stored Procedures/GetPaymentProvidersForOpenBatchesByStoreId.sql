﻿CREATE PROCEDURE [dbo].[GetPaymentProvidersForOpenBatchesByStoreId]	
	@StoreID uniqueidentifier
AS
BEGIN		
	SET NOCOUNT ON;
    
	with Baskets as (	
		select Basket.BasketID, Basket.BatchId
		from dbo.Basket join dbo.MppaBatch on Basket.BatchId = MppaBatch.BatchID
		where Basket.StoreID = @StoreId and Basket.BasketStateID = 3 and MppaBatch.IsClosed = 0
	),

	FundingSources as (
		select distinct UserPaymentSourceID
		from dbo.BasketPayment bp join Baskets b on bp.BasketID = b.BasketID
		where bp.PaymentCommand = 'Finalize'
	),

	PaymentProcessors as (
		select distinct PaymentProcessorID
		from Payment.LKUserPaymentSource ups join FundingSources fs on ups.UserPaymentSourceID = fs.UserPaymentSourceId		
	),

	FundingProviders as (
		select distinct FundingProviderID
		from Payment.LKPaymentProcessor lkpp join PaymentProcessors pp on lkpp.PaymentProcessorID = pp.PaymentProcessorID
	),

	ProviderNames as (
		select distinct FundingProviderName
		from Payment.FundingProvider pfp join FundingProviders fp on pfp.FundingProviderID = fp.FundingProviderID
	)

	select distinct FundingProviderName
	from ProviderNames
	
END





