﻿CREATE PROCEDURE [dbo].[IUDPrecidiaPaymentConfig]
	@TenantID [uniqueidentifier],
	@StoreName [nvarchar](30) = NULL,
	@MerchantIP [nvarchar](15) = NULL,
	@URI [nvarchar](250) = NULL,
	@Lane [nvarchar](50),
	@Port [int] = NULL,
	@ClientMAC [nvarchar](50),
	@IsActive [bit] = NULL,
	@UseTokenService [bit],
	@UseTCP [bit],
	@BuyPassPumpLane [tinyint] = NULL
AS 
BEGIN
	SET NOCOUNT ON;
	IF (EXISTS(
		SELECT 1
		FROM [dbo].[ConfigPRECIDIAPOSLYNX]
		WHERE TenantID = @TenantID AND Lane = @Lane
	))
	BEGIN
		UPDATE [dbo].[ConfigPRECIDIAPOSLYNX]
		SET StoreName = @StoreName
			, MerchantIP = @MerchantIP
			, URI = ISNULL(@URI, URI)
			, Port = @Port
			, ClientMAC = @ClientMAC
			, IsActive = ISNULL(@IsActive, IsActive)
			, UpdatedOn = GETUTCDATE()
			, UseTokenService = ISNULL(@UseTokenService, UseTokenService)
			, UseTCP = ISNULL(@UseTCP, UseTCP)
			, BuyPassPumpLane = ISNULL(@BuyPassPumpLane, BuyPassPumpLane)
		WHERE TenantID = @TenantID AND Lane = @Lane
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[ConfigPRECIDIAPOSLYNX]
			( TenantID
			, StoreName
			, MerchantIP
			, URI
			, Lane
			, Port
			, ClientMAC
			, IsActive
			, CreatedOn
			, UseTokenService
			, UseTCP
			, BuyPassPumpLane
		   ) 
		VALUES (@TenantID
			, @StoreName
			, @MerchantIP
			, ISNULL(@URI, 'https://p97.mlnetservices.com')
			, @Lane
			, @Port
			, @ClientMAC
		    , ISNULL(@IsActive, 1)
		    , GETUTCDATE()
		    , ISNULL(@UseTokenService, 1)
			, ISNULL(@UseTCP, 0)
			, ISNULL(@BuyPassPumpLane, 1)
		   );
	END
END
