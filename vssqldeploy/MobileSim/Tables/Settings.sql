﻿CREATE TABLE [MobileSim].[Settings] (
    [SettingsID] INT              IDENTITY (1, 1) NOT NULL,
    [UserID]     UNIQUEIDENTIFIER NOT NULL,
    [Name]       NVARCHAR (50)    NOT NULL,
    [Request]    NVARCHAR (20)    NOT NULL,
    [Body]       NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_MobileSim_Settings] PRIMARY KEY CLUSTERED ([SettingsID] ASC),
    CONSTRAINT [FK_MobileSim_Settings_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [IX_MobileSim_Settings_UniqNames] UNIQUE NONCLUSTERED ([UserID] ASC, [Name] ASC)
);

