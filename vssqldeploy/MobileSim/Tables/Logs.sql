﻿CREATE TABLE [MobileSim].[Logs] (
    [LogID]        INT            IDENTITY (1, 1) NOT NULL,
    [SimulationID] NVARCHAR (50)  NOT NULL,
    [DateTime]     DATETIME2 (7)  NOT NULL,
    [Severity]     NVARCHAR (10)  NOT NULL,
    [Message]      NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_MobileSim_Logs] PRIMARY KEY CLUSTERED ([LogID] ASC),
    CONSTRAINT [FK_Logs_SimulationID] FOREIGN KEY ([SimulationID]) REFERENCES [MobileSim].[History] ([SimulationID]) ON DELETE CASCADE
);

