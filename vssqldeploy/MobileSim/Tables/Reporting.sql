﻿CREATE TABLE [MobileSim].[Reporting] (
    [ReportID]     INT              IDENTITY (1, 1) NOT NULL,
    [ReportTimeID] DATETIME2 (7)    NOT NULL,
    [SimulationID] NVARCHAR (50)    NOT NULL,
    [Category]     NVARCHAR (30)    NOT NULL,
    [SubCategory]  NVARCHAR (30)    NOT NULL,
    [Flow]         NVARCHAR (8)     NOT NULL,
    [Status]       NVARCHAR (16)    NOT NULL,
    [Message]      NVARCHAR (MAX)   NOT NULL,
    [RequestID]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MobileSim_Reporting] PRIMARY KEY CLUSTERED ([ReportID] ASC),
    CONSTRAINT [FK_Reporting_SimulationID] FOREIGN KEY ([SimulationID]) REFERENCES [MobileSim].[History] ([SimulationID]) ON DELETE CASCADE
);

