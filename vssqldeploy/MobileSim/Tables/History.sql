﻿CREATE TABLE [MobileSim].[History] (
    [SimulationID] NVARCHAR (50)    NOT NULL,
    [CreatedOn]    DATETIME2 (7)    CONSTRAINT [DF_MobileSim_History_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UserID]       UNIQUEIDENTIFIER NULL,
    [Request]      NVARCHAR (20)    NOT NULL,
    [Status]       NVARCHAR (16)    NOT NULL,
    [UpdatedOn]    DATETIME2 (7)    NULL,
    CONSTRAINT [PK_MobileSim_History] PRIMARY KEY CLUSTERED ([SimulationID] ASC),
    CONSTRAINT [FK_MobileSim_History_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [UQ__History__85DBAE7539F1AD06] UNIQUE NONCLUSTERED ([SimulationID] ASC)
);

