﻿
-- =============================================
-- Author:      Andrei Ramanovich
-- Create date: 10/02/2017
-- Description:   Get logs for mobile simulation
-- =============================================
CREATE PROC [MobileSim].[GetLogs]
	 @Id nvarchar(50)
	,@Offset datetime2(7) = NULL
	,@Limit INT
	,@Count INT OUT

AS
BEGIN
SET NOCOUNT ON

			SELECT @Count = COUNT(*) FROM [MobileSim].[Logs] WHERE [SimulationID] = @Id

			SELECT TOP(@Limit) [LogID], [SimulationID], [DateTime], [Severity], [Message] 
			FROM [MobileSim].[Logs]
			WHERE ([SimulationID] = @Id)
			AND (@Offset IS NULL OR [DateTime] < @Offset)
			ORDER BY [DateTime] DESC

END
