﻿-- =============================================
-- Author:      Andrei Ramanovich
-- Create date: 02/09/2017
-- Update date: 02/21/2017 Andrei Ramanovich - add count as output parameter
-- Update date: 06/19/2017 Andrei Ramanovich - add filters SimulationId, FromDate, ToDate
-- Description: Get simulation history
-- =============================================
CREATE PROC [MobileSim].[GetSimulations]
	 @UserID UNIQUEIDENTIFIER = NULL
	,@SimulationId nvarchar(50) = NULL
	,@Statuses ListOfString READONLY
	,@FromDate DATETIME2 = NULL
	,@ToDate DATETIME2 = NULL
	,@Offset INT
	,@Limit INT
	,@Count INT OUT

AS
BEGIN
SET NOCOUNT ON;

		DECLARE @TempTable TABLE
		(
			[SimulationID] [nvarchar](50) NOT NULL,
			[CreatedOn] [datetime2](7) NOT NULL,
			[UserID] [uniqueidentifier] NULL,
			[Request] [nvarchar](20) NOT NULL,
			[Status] [nvarchar](16) NOT NULL,
			[UpdatedOn] [datetime2](7) NULL
		);

		INSERT INTO @TempTable ([SimulationID], [CreatedOn], [UserID], [Request], [Status], [UpdatedOn])
		SELECT [SimulationID], [CreatedOn], [UserID], [Request], [Status], [UpdatedOn]
		FROM [MobileSim].[History]
		WHERE (@UserID IS NULL OR [UserID] = @UserID)
			AND ([Status] IN (SELECT * FROM @Statuses) OR (NOT EXISTS (SELECT * FROM @Statuses)))
			AND (@SimulationId IS NULL OR [SimulationID] LIKE '%' + @SimulationId + '%')
			AND (@FromDate IS NULL OR @FromDate <= ISNULL([UpdatedOn], [CreatedOn]))
			AND (@ToDate IS NULL OR @ToDate >= [CreatedOn])

		SELECT @Count = COUNT(*) FROM @TempTable

		SELECT [SimulationID], [CreatedOn], [UpdatedOn], [UserID], [Request], [Status] 
		FROM @TempTable
		ORDER BY [CreatedOn] DESC
		OFFSET @Offset ROWS
		FETCH NEXT @Limit ROWS ONLY

END

