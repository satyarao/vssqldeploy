﻿
-- =============================================
-- Author:	Andrei Ramanovich
-- Create date: 5/31/2017
-- Description:	Returns collected data for mobile sim reporting
-- =============================================
CREATE PROCEDURE [MobileSim].[GetReportingData]
	@FromDate datetime2
	,@ToDate datetime2 
	,@SimulationID nvarchar(50) = NULL
	,@Status nvarchar(16) = NULL
	,@Categories ListOfString READONLY
AS
BEGIN
	SET NOCOUNT ON;
	SELECT r.[RequestID]
		  ,r.[ReportTimeID] as StartDateTime
		  ,resp.[ReportTimeID] as EndDateTime
		  ,r.[SimulationID]
		  ,r.[Category]
		  ,r.[SubCategory]
		  ,resp.[Status]
		  ,resp.[Message]
	FROM [MobileSim].[Reporting] r
	JOIN [MobileSim].[Reporting] resp ON (r.[RequestID] = resp.[RequestID] AND resp.[Flow] = 'Response')
	WHERE r.[RequestID] IS NOT NULL AND r.[Flow] = 'Request' 
	AND (r.[SimulationID] = ISNULL(@SimulationID, r.[SimulationID])) 
	AND (r.[ReportTimeID] BETWEEN @FromDate AND @ToDate) 
	AND (resp.[Status] = ISNULL(@Status, resp.[Status]))
	AND (NOT EXISTS (SELECT StringValue FROM @Categories) OR ((r.[Category] + ':' + r.[SubCategory]) IN (SELECT StringValue FROM @Categories)))
	ORDER BY r.[ReportID] ASC
END



