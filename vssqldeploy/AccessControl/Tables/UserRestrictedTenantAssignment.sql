﻿CREATE TABLE [AccessControl].[UserRestrictedTenantAssignment] (
    [UserID]             UNIQUEIDENTIFIER NOT NULL,
    [RestrictedTenantID] UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_AccessControlUserRestrictedTenantAssignment_CreatedOn] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_AccessControlUserRestrictedTenantAssignment] PRIMARY KEY CLUSTERED ([UserID] ASC, [RestrictedTenantID] ASC),
    CONSTRAINT [FK_AccessControlUserRestrictedTenantAssignment_Tenant] FOREIGN KEY ([RestrictedTenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_AccessControlUserRestrictedTenantAssignment_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

