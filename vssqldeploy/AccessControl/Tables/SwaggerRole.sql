﻿CREATE TABLE [AccessControl].[SwaggerRole] (
    [SwaggerRoleId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (127) NOT NULL,
    [IsActive]      BIT            CONSTRAINT [DF_SwaggerRole_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_SwaggerRole] PRIMARY KEY CLUSTERED ([SwaggerRoleId] ASC),
    CONSTRAINT [UC_SwaggerRole] UNIQUE NONCLUSTERED ([Name] ASC)
);

