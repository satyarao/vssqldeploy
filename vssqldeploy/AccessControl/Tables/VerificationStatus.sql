﻿CREATE TABLE [AccessControl].[VerificationStatus] (
    [Id]        BIGINT           IDENTITY (1, 1) NOT NULL,
    [UserID]    UNIQUEIDENTIFIER NOT NULL,
    [Channel]   INT              NOT NULL,
    [Status]    INT              NOT NULL,
    [CreatedOn] DATETIME2 (7)    NOT NULL
);

