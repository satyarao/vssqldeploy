﻿CREATE TABLE [AccessControl].[PowerBiConfig] (
    [PowerBiConfigId]     INT              IDENTITY (1, 1) NOT NULL,
    [TenantId]            UNIQUEIDENTIFIER NOT NULL,
    [WorkspaceCollection] NVARCHAR (50)    NOT NULL,
    [WorkspaceId]         UNIQUEIDENTIFIER NOT NULL,
    [AccessKey]           NVARCHAR (MAX)   NOT NULL,
    [ApiUrl]              NVARCHAR (50)    NOT NULL,
    [CreatedOn]           DATETIME2 (7)    NULL,
    [UpdatedOn]           DATETIME2 (7)    NULL,
    [CreatedBy]           NVARCHAR (255)   NULL,
    [UpdatedBy]           NVARCHAR (255)   NULL,
    [IsActive]            BIT              NULL,
    CONSTRAINT [PK_PowerBiConfig] PRIMARY KEY CLUSTERED ([PowerBiConfigId] ASC)
);

