﻿CREATE TABLE [AccessControl].[TokenIssuer] (
    [TenantID]                UNIQUEIDENTIFIER NOT NULL,
    [ApplicationID]           UNIQUEIDENTIFIER NOT NULL,
    [RefreshTokenLength]      INT              NOT NULL,
    [RefreshTokenExp]         INT              NOT NULL,
    [MaxRefreshTokenLifetime] INT              NULL,
    [SigningKeySecretName]    NVARCHAR (255)   NOT NULL,
    [PublicKeySecretName]     NVARCHAR (255)   NOT NULL,
    [AccessTokenExp]          INT              DEFAULT ((1440)) NOT NULL
);

