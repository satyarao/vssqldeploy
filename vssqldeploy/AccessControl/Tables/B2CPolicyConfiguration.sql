﻿CREATE TABLE [AccessControl].[B2CPolicyConfiguration] (
    [PolicyConfigID]          INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]                UNIQUEIDENTIFIER NOT NULL,
    [Name]                    NVARCHAR (255)   NULL,
    [ApplicationID]           UNIQUEIDENTIFIER NOT NULL,
    [PolicyConfigJsonPayload] NVARCHAR (MAX)   NULL,
    [ClientSecret]            NVARCHAR (128)   NULL,
    [ApplicationType]         SMALLINT         NOT NULL,
    [IsTenantSpecific]        BIT              CONSTRAINT [DF_B2CPolicyConfiguration_IsTenantSpecific] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_B2CPolicyConfiguration] PRIMARY KEY CLUSTERED ([PolicyConfigID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_B2CPolicyConfiguration_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [UC_B2CPolicyConfiguration_ApplicationType] UNIQUE NONCLUSTERED ([TenantID] ASC, [ApplicationType] ASC) WITH (FILLFACTOR = 80)
);

