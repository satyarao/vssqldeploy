﻿CREATE TABLE [AccessControl].[UserGroupModule] (
    [UserGroupModuleID]   INT            IDENTITY (1, 1) NOT NULL,
    [UserGroupModuleName] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_UserGroupModule] PRIMARY KEY CLUSTERED ([UserGroupModuleID] ASC)
);

