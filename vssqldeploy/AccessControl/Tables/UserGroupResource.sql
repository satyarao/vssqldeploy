﻿CREATE TABLE [AccessControl].[UserGroupResource] (
    [UserGroupResourceID] INT           IDENTITY (1, 1) NOT NULL,
    [UserGroupID]         INT           NOT NULL,
    [ApiResourceID]       INT           NOT NULL,
    [ReadPermission]      BIT           NOT NULL,
    [WritePermission]     BIT           NOT NULL,
    [DeletePermission]    BIT           NOT NULL,
    [IsActive]            BIT           CONSTRAINT [DF__UserGroup__IsAct__617C500E] DEFAULT ((1)) NULL,
    [CreatedOn]           DATETIME2 (7) CONSTRAINT [DF__UserGroup__Creat__62707447] DEFAULT (getdate()) NULL,
    [UpdatedOn]           DATETIME2 (7) NULL,
    CONSTRAINT [PK__UserGrou__80BE132BAABACC4F] PRIMARY KEY CLUSTERED ([UserGroupResourceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserGroupResource_ApiResource] FOREIGN KEY ([ApiResourceID]) REFERENCES [AccessControl].[ApiResource] ([ApiResourceID]),
    CONSTRAINT [FK_UserGroupResource_UserGroup] FOREIGN KEY ([UserGroupID]) REFERENCES [AccessControl].[UserGroup] ([UserGroupID]) ON DELETE CASCADE
);

