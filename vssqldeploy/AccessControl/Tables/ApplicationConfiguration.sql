﻿CREATE TABLE [AccessControl].[ApplicationConfiguration] (
    [ConfigurationID] INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID]   UNIQUEIDENTIFIER NOT NULL,
    [ProviderID]      INT              NOT NULL,
    [Version]         NVARCHAR (128)   NULL,
    [ClientID]        NVARCHAR (128)   NOT NULL,
    [ClientSecret]    NVARCHAR (128)   NULL,
    [ConfigJson]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_AccessControl.ApplicationConfiguration] PRIMARY KEY CLUSTERED ([ConfigurationID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_AccessControl.ApplicationConfiguration_ConfigJson_IsJson] CHECK (isjson([ConfigJson])<>(0)),
    CONSTRAINT [FK_AccessControl.ApplicationConfiguration_AccessControl.Application_ApplicationID] FOREIGN KEY ([ApplicationID]) REFERENCES [AccessControl].[Application] ([ApplicationID]),
    CONSTRAINT [FK_AccessControl.ApplicationConfiguration_AccessControl.IdentityProvider_ProviderID] FOREIGN KEY ([ProviderID]) REFERENCES [AccessControl].[IdentityProvider] ([ProviderID]),
    CONSTRAINT [UC_AccessControl.ApplicationConfiguration_ClientID] UNIQUE NONCLUSTERED ([ApplicationID] ASC, [ClientID] ASC, [Version] ASC) WITH (FILLFACTOR = 80)
);

