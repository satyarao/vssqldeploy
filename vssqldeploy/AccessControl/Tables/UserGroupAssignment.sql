﻿CREATE TABLE [AccessControl].[UserGroupAssignment] (
    [UserID]         UNIQUEIDENTIFIER NOT NULL,
    [UserGroupID]    INT              NOT NULL,
    [CreatedOn]      DATETIME2 (7)    CONSTRAINT [DF_AccessControlUserGroupAssignment_CreatedOn] DEFAULT (getdate()) NULL,
    [ExpirationDate] DATETIME2 (7)    NULL,
    CONSTRAINT [PK_AccessControlUserGroupAssignment] PRIMARY KEY CLUSTERED ([UserID] ASC, [UserGroupID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_AccessControlUserGroupAssignment_UserGroup] FOREIGN KEY ([UserGroupID]) REFERENCES [AccessControl].[UserGroup] ([UserGroupID]),
    CONSTRAINT [FK_AccessControlUserGroupAssignment_UserInfo] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

