﻿CREATE TABLE [AccessControl].[TokenBlacklist] (
    [ID]         UNIQUEIDENTIFIER NOT NULL,
    [TokenValue] NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]  DATETIME2 (7)    NOT NULL,
    [ExpiresOn]  DATETIME2 (7)    NOT NULL
);

