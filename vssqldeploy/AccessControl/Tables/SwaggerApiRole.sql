﻿CREATE TABLE [AccessControl].[SwaggerApiRole] (
    [SwaggerApiRoleId]       INT IDENTITY (1, 1) NOT NULL,
    [SwaggerRoleId]          INT NOT NULL,
    [SwaggerApiDefinitionId] INT NOT NULL,
    [IsActive]               BIT CONSTRAINT [DF_SwaggerApiRole_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_SwaggerApiRole] PRIMARY KEY CLUSTERED ([SwaggerApiRoleId] ASC),
    CONSTRAINT [FK_SwaggerApiRole_SwaggerApiDefinition] FOREIGN KEY ([SwaggerApiDefinitionId]) REFERENCES [AccessControl].[SwaggerApiDefinition] ([SwaggerApiDefinitionId]),
    CONSTRAINT [FK_SwaggerApiRole_SwaggerRole] FOREIGN KEY ([SwaggerRoleId]) REFERENCES [AccessControl].[SwaggerRole] ([SwaggerRoleId]),
    CONSTRAINT [UC_SwaggerApiRole] UNIQUE NONCLUSTERED ([SwaggerRoleId] ASC, [SwaggerApiDefinitionId] ASC)
);

