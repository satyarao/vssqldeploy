﻿CREATE TABLE [AccessControl].[UserGroup] (
    [UserGroupID]       INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]          UNIQUEIDENTIFIER NOT NULL,
    [UserGroupName]     NVARCHAR (150)   NOT NULL,
    [IsActive]          BIT              CONSTRAINT [DF_UserGroup_IsActive] DEFAULT ((1)) NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_UserGroup_CreatedOn] DEFAULT (getdate()) NULL,
    [UpdatedOn]         DATETIME2 (7)    NULL,
    [UserGroupModuleID] INT              CONSTRAINT [DF_UserGroup_UserGroupModuleID] DEFAULT ((5)) NOT NULL,
    CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED ([UserGroupID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UserGroup_Tenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_UserGroup_UserGroupModule] FOREIGN KEY ([UserGroupModuleID]) REFERENCES [AccessControl].[UserGroupModule] ([UserGroupModuleID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Tenant_UserGroupName]
    ON [AccessControl].[UserGroup]([TenantID] ASC, [UserGroupName] ASC) WHERE ([IsActive]=(1)) WITH (FILLFACTOR = 80);

