﻿CREATE TABLE [AccessControl].[UserIdentity] (
    [UserID]         UNIQUEIDENTIFIER NOT NULL,
    [ProviderID]     INT              NOT NULL,
    [ProviderUserID] NVARCHAR (255)   NOT NULL,
    [CreatedOn]      DATETIME2 (7)    CONSTRAINT [DF_AccessControl.UserIdentity_CreatedOn] DEFAULT (getdate()) NULL,
    [UpdatedOn]      DATETIME2 (7)    NULL,
    CONSTRAINT [PK_AccessControl.UserIdentity] PRIMARY KEY CLUSTERED ([UserID] ASC, [ProviderID] ASC),
    CONSTRAINT [FK_AccessControl.UserIdentity_dbo.UserInfo_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

