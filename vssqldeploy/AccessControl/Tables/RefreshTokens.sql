﻿CREATE TABLE [AccessControl].[RefreshTokens] (
    [TokenID]                    BIGINT           IDENTITY (1, 1) NOT NULL,
    [RefreshTokenHash]           VARBINARY (2000) NOT NULL,
    [RefreshTokenIssuedAt]       DATETIME         NOT NULL,
    [RefreshTokenExpiresAt]      DATETIME         NOT NULL,
    [RefreshTokenChainExpiresAt] DATETIME         NULL,
    [ApplicationID]              UNIQUEIDENTIFIER NOT NULL,
    [TenantID]                   UNIQUEIDENTIFIER NOT NULL,
    [IsActive]                   BIT              NOT NULL,
    [UserID]                     UNIQUEIDENTIFIER DEFAULT (CONVERT([uniqueidentifier],'00000000-0000-0000-0000-000000000000')) NOT NULL
);

