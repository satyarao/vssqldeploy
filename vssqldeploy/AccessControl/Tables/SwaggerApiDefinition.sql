﻿CREATE TABLE [AccessControl].[SwaggerApiDefinition] (
    [SwaggerApiDefinitionId] INT            IDENTITY (1, 1) NOT NULL,
    [ApiType]                INT            CONSTRAINT [DF_SwaggerApiDefinition_ApiType] DEFAULT ((0)) NOT NULL,
    [Controller]             NVARCHAR (255) NOT NULL,
    [Action]                 NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_SwaggerApiDefinition] PRIMARY KEY CLUSTERED ([SwaggerApiDefinitionId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UC_SwaggerApiDefinition] UNIQUE NONCLUSTERED ([ApiType] ASC, [Controller] ASC, [Action] ASC) WITH (FILLFACTOR = 80)
);

