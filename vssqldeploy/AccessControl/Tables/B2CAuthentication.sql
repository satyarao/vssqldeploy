﻿CREATE TABLE [AccessControl].[B2CAuthentication] (
    [ID]                   INT              IDENTITY (1, 1) NOT NULL,
    [TenantID]             UNIQUEIDENTIFIER NULL,
    [RefreshTokenCnt]      INT              NOT NULL,
    [AuthorizationCodeCnt] INT              NOT NULL,
    [Date]                 DATE             NOT NULL,
    CONSTRAINT [PK_B2CAuthentication] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_B2CAuthentication_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [UC_B2CAuthentication_Tenant_Date] UNIQUE NONCLUSTERED ([TenantID] ASC, [Date] ASC) WITH (FILLFACTOR = 80)
);

