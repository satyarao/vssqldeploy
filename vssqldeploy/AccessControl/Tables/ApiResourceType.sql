﻿CREATE TABLE [AccessControl].[ApiResourceType] (
    [ApiResourceTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [ApiResourceType]   NVARCHAR (100) NOT NULL,
    [IsActive]          BIT            CONSTRAINT [DF__ApiResour__IsAct__274FAE79] DEFAULT ((1)) NULL,
    CONSTRAINT [PK__ApiResou__F430FE7AB6E08B8A] PRIMARY KEY CLUSTERED ([ApiResourceTypeID] ASC) WITH (FILLFACTOR = 80)
);

