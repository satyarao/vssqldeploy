﻿CREATE TABLE [AccessControl].[Application] (
    [ApplicationID]   UNIQUEIDENTIFIER CONSTRAINT [DF_AccessControl.Application_ApplicationID] DEFAULT (newid()) NOT NULL,
    [TenantID]        UNIQUEIDENTIFIER NOT NULL,
    [ApplicationName] NVARCHAR (255)   NOT NULL,
    [ApplicationType] SMALLINT         NOT NULL,
    CONSTRAINT [PK_AccessControl.Application] PRIMARY KEY CLUSTERED ([ApplicationID] ASC),
    CONSTRAINT [FK_AccessControl.Application_dbo.Tenant_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [UC_AccessControl.Application_ApplicationType] UNIQUE NONCLUSTERED ([TenantID] ASC, [ApplicationType] ASC)
);

