﻿CREATE TABLE [AccessControl].[ApiType] (
    [ApiTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [ApiType]   NVARCHAR (100) NOT NULL,
    [IsActive]  BIT            CONSTRAINT [DF__ApiType__IsActiv__2A2C1B24] DEFAULT ((1)) NULL,
    CONSTRAINT [PK__ApiType__562C3D6391FD2A60] PRIMARY KEY CLUSTERED ([ApiTypeID] ASC) WITH (FILLFACTOR = 80)
);

