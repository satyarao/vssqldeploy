﻿CREATE TABLE [AccessControl].[ApplicationDefaultIdentity] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [ApplicationID] UNIQUEIDENTIFIER NOT NULL,
    [ProviderID]    INT              NOT NULL,
    [Version]       NVARCHAR (128)   NULL,
    CONSTRAINT [PK_AccessControl.ApplicationDefaultIdentity] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_AccessControl.ApplicationDefaultIdentity_AccessControl.Application_ApplicationID] FOREIGN KEY ([ApplicationID]) REFERENCES [AccessControl].[Application] ([ApplicationID]),
    CONSTRAINT [FK_AccessControl.ApplicationDefaultIdentity_AccessControl.IdentityProvider_ProviderID] FOREIGN KEY ([ProviderID]) REFERENCES [AccessControl].[IdentityProvider] ([ProviderID]),
    CONSTRAINT [UC_AccessControl.ApplicationDefaultIdentity_ApplicationID_Version] UNIQUE NONCLUSTERED ([ApplicationID] ASC, [Version] ASC)
);

