﻿CREATE TABLE [AccessControl].[ModuleUsersLimit] (
    [UserGroupModuleID] INT              NOT NULL,
    [TenantID]          UNIQUEIDENTIFIER NOT NULL,
    [UsersLimit]        INT              NULL,
    CONSTRAINT [PK_ModuleUsersLimit] PRIMARY KEY CLUSTERED ([UserGroupModuleID] ASC, [TenantID] ASC)
);

