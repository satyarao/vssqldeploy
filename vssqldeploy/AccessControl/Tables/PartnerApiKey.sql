﻿CREATE TABLE [AccessControl].[PartnerApiKey] (
    [PartnerApiKeyId] INT              IDENTITY (1, 1) NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [ApiKey]          NVARCHAR (150)   NULL,
    [Channel]         NVARCHAR (100)   DEFAULT ('WhiteLabelMobile') NOT NULL,
    [DisplayName]     NVARCHAR (255)   DEFAULT ('Not Set') NOT NULL,
    [IsActive]        BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__PartnerA__F628337C7348DC50] PRIMARY KEY CLUSTERED ([PartnerApiKeyId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UC_AccessControl_PartnerApiKey_Tenant_ApiKey] UNIQUE NONCLUSTERED ([TenantId] ASC, [ApiKey] ASC) WITH (FILLFACTOR = 80)
);

