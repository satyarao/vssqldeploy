﻿CREATE TABLE [AccessControl].[VerificationCode] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [VerificationCode] NVARCHAR (255)   NOT NULL,
    [Channel]          INT              NOT NULL,
    [ExpirationDate]   DATETIME2 (7)    NOT NULL,
    [AttemptCount]     INT              NOT NULL,
    [UserID]           UNIQUEIDENTIFIER NOT NULL,
    [IsActive]         BIT              NOT NULL,
    [ChannelData]      NVARCHAR (255)   DEFAULT (NULL) NULL
);

