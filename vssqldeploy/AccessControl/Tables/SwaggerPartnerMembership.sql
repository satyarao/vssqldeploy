﻿CREATE TABLE [AccessControl].[SwaggerPartnerMembership] (
    [SwaggerPartnerMembershipId] INT IDENTITY (1, 1) NOT NULL,
    [PartnerApiKeyId]            INT NOT NULL,
    [SwaggerRoleId]              INT NOT NULL,
    [IsActive]                   BIT CONSTRAINT [DF_SwaggerPartnerMembership_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_SwaggerPartnerMembership] PRIMARY KEY CLUSTERED ([SwaggerPartnerMembershipId] ASC),
    CONSTRAINT [FK_SwaggerPartnerMembership_PartnerApiKey] FOREIGN KEY ([PartnerApiKeyId]) REFERENCES [AccessControl].[PartnerApiKey] ([PartnerApiKeyId]),
    CONSTRAINT [FK_SwaggerPartnerMembership_SwaggerRole] FOREIGN KEY ([SwaggerRoleId]) REFERENCES [AccessControl].[SwaggerRole] ([SwaggerRoleId]),
    CONSTRAINT [UC_SwaggerPartnerMembership] UNIQUE NONCLUSTERED ([PartnerApiKeyId] ASC, [SwaggerRoleId] ASC)
);

