﻿CREATE TABLE [AccessControl].[ApiResource] (
    [ApiResourceID]       INT            IDENTITY (1, 1) NOT NULL,
    [ApiResourceName]     NVARCHAR (100) NOT NULL,
    [ApiResourceTypeID]   INT            NOT NULL,
    [ApiTypeID]           INT            NOT NULL,
    [IsActive]            BIT            CONSTRAINT [DF__ApiResour__IsAct__247341CE] DEFAULT ((1)) NULL,
    [ApiResourceCode]     NVARCHAR (100) CONSTRAINT [DF_ApiResource_ApiResourceCode] DEFAULT (N'default') NOT NULL,
    [ApiResourceCategory] NVARCHAR (100) NULL,
    CONSTRAINT [PK__ApiResou__B29F5B985E45BA9C] PRIMARY KEY CLUSTERED ([ApiResourceID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ApiResource_ApiResourceType] FOREIGN KEY ([ApiResourceTypeID]) REFERENCES [AccessControl].[ApiResourceType] ([ApiResourceTypeID]),
    CONSTRAINT [FK_ApiResource_ApiType] FOREIGN KEY ([ApiTypeID]) REFERENCES [AccessControl].[ApiType] ([ApiTypeID]),
    CONSTRAINT [UK_Category_Name] UNIQUE NONCLUSTERED ([ApiResourceCategory] ASC, [ApiResourceName] ASC) WITH (FILLFACTOR = 80)
);

