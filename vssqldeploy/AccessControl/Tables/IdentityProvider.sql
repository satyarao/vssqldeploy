﻿CREATE TABLE [AccessControl].[IdentityProvider] (
    [ProviderID]   INT           NOT NULL,
    [ProviderName] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_AccessControl.IdentityProvider] PRIMARY KEY CLUSTERED ([ProviderID] ASC)
);

