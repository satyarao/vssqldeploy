﻿CREATE TYPE [AccessControl].[ApiResourceList] AS TABLE (
    [ApiResourceID]    INT NOT NULL,
    [ReadPermission]   BIT NOT NULL,
    [WritePermission]  BIT NOT NULL,
    [DeletePermission] BIT NOT NULL);

