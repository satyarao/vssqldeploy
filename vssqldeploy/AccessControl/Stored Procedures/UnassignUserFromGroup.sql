﻿-- =============================================
-- Author:		Justin Blau
-- Create date: 8/12/2016
-- Description:	Unassigns a user from a group
-- =============================================
CREATE PROCEDURE [AccessControl].[UnassignUserFromGroup]
	@UserId UNIQUEIDENTIFIER, 
	@GroupId NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [AccessControl].[UserClaims]
      WHERE [UserID] = @UserId AND 
			[Value] = @GroupId AND (
				[Type] = 'UserGroupID' OR
				[Type] = 'RestrictedTenantID' OR
				[Type] = 'RestrictedStoreID')
END
