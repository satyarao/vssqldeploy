﻿
-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 8/20/2019
-- Description:	IUD User ProviderID
-- =============================================
CREATE PROCEDURE [AccessControl].[GetUserProviderID]
	 @TenantID UNIQUEIDENTIFIER
	,@ProviderID INT
	,@ProviderUserID nvarchar(255)
AS
BEGIN
    SELECT ui.UserID
    FROM dbo.UserInfo ui
    JOIN AccessControl.UserIdentity aui ON aui.UserID = ui.UserID AND aui.ProviderID = @ProviderID AND aui.ProviderUserID = @ProviderUserID
    WHERE ui.TenantID = @TenantID 
    ORDER BY ui.CreatedOn ASC
END
