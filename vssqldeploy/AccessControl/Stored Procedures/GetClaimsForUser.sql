﻿-- =============================================
-- Author:		Justin Blau	
-- Create date: 8/12/2016
-- Description:	Returns claims for a user
-- =============================================
CREATE PROCEDURE [AccessControl].[GetClaimsForUser]
	@UserID UNIQUEIDENTIFIER, 
	@Type NVARCHAR(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @Type IS NULL
		SELECT [Type], [Value]
		FROM [AccessControl].[UserClaims]
		WHERE [UserID] = @UserID
	ELSE 
		SELECT [Type], [Value]
		FROM [AccessControl].[UserClaims]
		WHERE [UserID] = @UserID AND
			[Type] = @Type
END
