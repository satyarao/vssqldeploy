﻿
-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 8/20/2019
-- Description:	IUD User ProviderID
-- =============================================
CREATE PROCEDURE [AccessControl].[IUDUserProviderID]
	 @UserID UNIQUEIDENTIFIER
	,@ProviderID INT
	,@ProviderUserID nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM AccessControl.UserIdentity WHERE UserID = @UserID AND ProviderID = @ProviderID)
		BEGIN
			UPDATE AccessControl.UserIdentity 
			SET ProviderUserID = @ProviderUserID
				, UpdatedOn = GETUTCDATE()
			WHERE UserID = @UserID AND ProviderID = @ProviderID
		END
	ELSE
		BEGIN
			INSERT INTO AccessControl.UserIdentity (UserID, ProviderID, ProviderUserID, UpdatedOn)
			VALUES (@UserID, @ProviderID, @ProviderUserID, GETDATE())
		END
END
