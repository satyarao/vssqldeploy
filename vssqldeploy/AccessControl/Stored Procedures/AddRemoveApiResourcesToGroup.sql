﻿-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 10/8/2015
-- Description:	Add or remove(based on IsActive income param) ApiResources to group
-- =============================================
CREATE PROCEDURE [AccessControl].[AddRemoveApiResourcesToGroup]
	 @UserGroupID INT
	,@ApiResourceList [AccessControl].[ApiResourceList] READONLY
	,@IsActive BIT = 1
AS
BEGIN
	SET NOCOUNT ON;

	IF(@IsActive = 1)
		BEGIN
			UPDATE [AccessControl].[UserGroupResource]
			SET 
				 IsActive = 1
				,ReadPermission = (SELECT ReadPermission FROM @ApiResourceList arl WHERE ApiResourceID = arl.ApiResourceID)
				,WritePermission = (SELECT WritePermission FROM @ApiResourceList arl WHERE ApiResourceID = arl.ApiResourceID)
				,DeletePermission = (SELECT DeletePermission FROM @ApiResourceList arl WHERE ApiResourceID = arl.ApiResourceID)
			WHERE UserGroupID = @UserGroupID AND (ApiResourceID IN (SELECT ApiResourceID FROM @ApiResourceList))

			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			SELECT @UserGroupID, ugp.ApiResourceID, arl.ReadPermission, arl.WritePermission, arl.DeletePermission
			FROM @ApiResourceList arl
			JOIN  [AccessControl].[UserGroupResource] ugp ON arl.ApiResourceID = ugp.ApiResourceID
			WHERE arl.ApiResourceID NOT IN (SELECT ugp.ApiResourceID FROM [AccessControl].[UserGroupResource] as ugp)
		END
	ELSE
		BEGIN
			UPDATE [AccessControl].[UserGroupResource]
			SET IsActive = 0
			WHERE UserGroupID = @UserGroupID AND (ApiResourceID IN (SELECT ApiResourceID FROM @ApiResourceList))
		END
END
