﻿-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 8/16/2018
-- Description:	Create Fleet Admin and Fleet Manager user groups
-- Update: 11/13/2018 - Alex Goroshko - Added fleet:payment_report
-- Update: 01/02/2019 - Davydovich Raman - Update 'Fleet Manager' permissions
-- Update: 01/03/2019 - Darya Batalava - Added fleet:management_fee
-- Update: 02/14/2019 - Davydovich Raman - Added fleet:company:rci:restrictions
-- Update: 03/19/2019 - Darya Batalava - Added service_transactions
-- =============================================
CREATE PROCEDURE [AccessControl].[CreateDefaultFleetUsergroups]
	 @FleetTenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @UserGroupID INT;
	IF(NOT EXISTS(SELECT 1 FROM [AccessControl].[UserGroup] WHERE UserGroupName = 'Fleet Admin' AND TenantID = @FleetTenantID))
		BEGIN
			INSERT INTO AccessControl.UserGroup (TenantID, UserGroupName, UserGroupModuleID)
			VALUES (@FleetTenantID, 'Fleet Admin', 5) -- 5 - "means Other"
			SET @UserGroupID = (SELECT SCOPE_IDENTITY())

			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:vehicle_management'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:transactions'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_management'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:user_management'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:payment_report'), 1, 0, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:management_fee'), 1, 0, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company:rci:restrictions'), 1, 0, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_category:rci:restrictions'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'service_transactions'), 1, 0, 0 )
		END

	IF(NOT EXISTS(SELECT 1 FROM [AccessControl].[UserGroup] WHERE UserGroupName = 'Fleet Manager' AND TenantID = @FleetTenantID))
		BEGIN
			INSERT INTO AccessControl.UserGroup (TenantID, UserGroupName, UserGroupModuleID)
			VALUES (@FleetTenantID, 'Fleet Manager', 5) -- 5 - means "Other"
			SET @UserGroupID = (SELECT SCOPE_IDENTITY())
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:vehicle_management'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:transactions'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_management'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company'), 1, 0, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:user_management'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:payment_report'), 1, 0, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:management_fee'), 1, 0, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company:rci:restrictions'), 1, 0, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_category:rci:restrictions'), 1, 1, 0 )
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
			VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'service_transactions'), 1, 0, 0 )
		END
END
	





