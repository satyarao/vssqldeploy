﻿
-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 6/29/2017
-- Description:	Get users which was assigned to module
-- 08/06/2019:  Sergei Buday - PLAT-4004 [Identity] Clean up membership reboot stuff that we don't need
-- =============================================
CREATE PROCEDURE [AccessControl].[GetUsersAssignedToModule]
	@TenantID UNIQUEIDENTIFIER,
	@UserGroupModuleID INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT uga.UserID AS UserID
	FROM [AccessControl].[UserGroupAssignment] uga 
		JOIN [AccessControl].[UserGroup] ug ON uga.UserGroupID = ug.UserGroupID
	WHERE ug.IsActive = 1 
		AND ug.TenantID = @TenantID
		AND ug.UserGroupModuleID = @UserGroupModuleID
END

