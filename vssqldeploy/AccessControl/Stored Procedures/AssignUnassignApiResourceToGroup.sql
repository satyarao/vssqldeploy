﻿-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 10/8/2015
-- Description:	Assign or unassign(based on IsActive income param) single ApiResource to group
-- =============================================
CREATE PROCEDURE [AccessControl].[AssignUnassignApiResourceToGroup]
	  @UserGroupID INT
	 ,@ApiResourceID INT
	 ,@ReadPermission BIT 
	 ,@WritePermission BIT 
	 ,@DeletePermission BIT 
	 ,@IsActive BIT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF(EXISTS(SELECT * FROM [AccessControl].[UserGroupResource] WHERE UserGroupID=@UserGroupID AND ApiResourceID =  @ApiResourceID))
		BEGIN
			UPDATE [AccessControl].[UserGroupResource]
			SET IsActive = @IsActive, ReadPermission = @ReadPermission, WritePermission = @WritePermission, DeletePermission = @DeletePermission
			WHERE UserGroupID=@UserGroupID AND ApiResourceID =  @ApiResourceID
		END
	ELSE
		BEGIN
			INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission, IsActive)
			VALUES (@UserGroupID, @ApiResourceID, @ReadPermission, @WritePermission, @DeletePermission, @IsActive)
		END
END
