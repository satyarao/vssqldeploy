﻿-- =============================================
-- Author:		Justin Blau	
-- Create date: 8/12/2016
-- Description:	Removes a claim for a user
-- =============================================
CREATE PROCEDURE [AccessControl].[RemoveClaimForUser]
	@UserID UNIQUEIDENTIFIER, 
	@Type NVARCHAR(150),
	@Value NVARCHAR(150) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	IF @Value IS NULL
		DELETE FROM [AccessControl].[UserClaims]
		WHERE [UserID] = @UserID AND
			[Type] = @Type
	ELSE
		DELETE FROM [AccessControl].[UserClaims]
		WHERE [UserID] = @UserID AND
			[Type] = @Type AND
			[Value] = @Value
END
