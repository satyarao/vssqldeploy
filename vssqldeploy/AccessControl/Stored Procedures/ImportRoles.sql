﻿-- =============================================
-- Author:		Andrei Ramanovich
-- Create date: 06/22/2017
-- Description:	Import User Groups with resources
-- =============================================
CREATE PROCEDURE [AccessControl].[ImportRoles]
	 @TenantID UNIQUEIDENTIFIER
	,@Groups NVARCHAR(max) -- JSON with format UserGroupName, UserGroupModuleId, ApiResources: [ ApiResourceCode, ReadPermission, WritePermission, DeletePermission ]
AS
BEGIN
	SET NOCOUNT ON;
	
	SET XACT_ABORT ON
	
	BEGIN TRAN

	-- deactivate groups with the same name
	UPDATE [AccessControl].[UserGroup]
				SET IsActive = 0, UpdatedOn = GETUTCDATE()
				WHERE [TenantID] = @TenantID AND [IsActive] = 1 AND [UserGroupName] IN (SELECT UserGroupName FROM OPENJSON( (SELECT @Groups) ) WITH (UserGroupName nvarchar(max)) )

	DECLARE @CurrentGroupID INT, @CurrentUserGroupName nvarchar(max), @CurrentUserGroupModuleId INT, @CurrentResourcesJson nvarchar(max);

	DECLARE cur CURSOR READ_ONLY
	FOR SELECT JSON_VALUE(value, '$.UserGroupName')  as UserGroupName
				, JSON_VALUE(value, '$.UserGroupModuleId') as UserGroupModuleId
				, JSON_QUERY(value, '$.ApiResources') as ResourcesJson
	FROM OPENJSON(@Groups)
	OPEN cur 

	FETCH NEXT FROM cur INTO @CurrentUserGroupName, @CurrentUserGroupModuleId, @CurrentResourcesJson
	WHILE @@FETCH_STATUS = 0 
	BEGIN 

	-- create group
	INSERT INTO [AccessControl].[UserGroup] (TenantID, UserGroupName, UserGroupModuleID)
				VALUES (@TenantID, @CurrentUserGroupName, @CurrentUserGroupModuleId)
				SET @CurrentGroupID = (SELECT SCOPE_IDENTITY())

	-- assign resources
	INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission, IsActive)
				SELECT 	@CurrentGroupID as UserGroupID,
						ar.[ApiResourceID] as ApiResourceID,
						IIF(JSON_Value (res.value, '$.ReadPermission') = 'true', 1, 0) as ReadPermission,
						IIF(JSON_Value (res.value, '$.WritePermission') = 'true', 1, 0) as WritePermission,
						IIF(JSON_Value (res.value, '$.DeletePermission') = 'true', 1, 0) as DeletePermission,
						1 as IsActive
	FROM OPENJSON ( ( @CurrentResourcesJson ) ) as res
	JOIN [AccessControl].[ApiResource] ar ON ar.ApiResourceCode = JSON_Value (res.value, '$.ApiResourceCode')

	FETCH NEXT FROM cur INTO @CurrentUserGroupName, @CurrentUserGroupModuleId, @CurrentResourcesJson
	END 

	CLOSE cur 
	DEALLOCATE cur

	COMMIT TRAN

END
