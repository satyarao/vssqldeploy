﻿
-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 10/8/2015
-- Update date: 1/14/2017 Andrei Ramanovich - removed @UserGroupTypeID
-- Update date: 6/20/2017 Igor Gaidukov - added @UserGroupModuleID
-- Description:	IUD User Group (returns UserGroupID)
-- =============================================
CREATE PROCEDURE [AccessControl].[IUDUserGroup]
	 @TenantID UNIQUEIDENTIFIER
	,@UserGroupName NVARCHAR(150) = NULL
	,@UserGroupID INT = NULL
	,@IsActive BIT = NULL
	,@UserGroupModuleID INT = NULL 
AS
BEGIN
	SET NOCOUNT ON;

    SET @IsActive = ISNULL(@IsActive, 1);

	-- if it is deleting operation so do soft delete
	IF (@IsActive = 0)
		BEGIN
			UPDATE AccessControl.UserGroup 
			SET IsActive = 0, UpdatedOn = GETUTCDATE()
			WHERE TenantID = @TenantID AND UserGroupID = @UserGroupID
		END
	--if it is not deleting operation so insert new record or update existing one
	ELSE
		BEGIN 
			IF(@UserGroupID IS NOT NULL) 
				BEGIN
					UPDATE AccessControl.UserGroup 
					SET UserGroupName = @UserGroupName
						, UpdatedOn = GETUTCDATE()
						, IsActive = 1
						, UserGroupModuleID = ISNULL(@UserGroupModuleID, UserGroupModuleID)
					WHERE TenantID = @TenantID AND UserGroupID = @UserGroupID
				END
			ELSE
				BEGIN
					INSERT INTO AccessControl.UserGroup (TenantID, UserGroupName, UserGroupModuleID) 
					VALUES (@TenantID, @UserGroupName, @UserGroupModuleID)
					SET @UserGroupID = (SELECT SCOPE_IDENTITY())
				END
		END
	SELECT @UserGroupID
END
