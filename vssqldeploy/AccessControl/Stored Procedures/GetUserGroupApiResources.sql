﻿
-- =============================================
-- Author:      Alexandr Goroshko
-- Create date: 10/9/2015
-- Update date: 1/14/2017 Andrei Ramanovich - removed UserGroupTypeID
-- Description: Get api resources list which are available for user group
-- =============================================
CREATE PROCEDURE [AccessControl].[GetUserGroupApiResources]
                @UserGroupID INT = NULL
                ,@TenantID UNIQUEIDENTIFIER = NULL
AS
BEGIN
                SET NOCOUNT ON;
                SELECT ugp.UserGroupID, ugp.ApiResourceID, ar.ApiResourceName, ar.ApiResourceCode, ugp.ReadPermission, ugp.WritePermission, ugp.DeletePermission, art.ApiResourceTypeID, art.ApiResourceType, at.ApiTypeID, at.ApiType, ug.TenantID
                FROM AccessControl.UserGroupResource AS ugp
                JOIN AccessControl.UserGroup ug ON ug.UserGroupID = ugp.UserGroupID AND ug.TenantID = ISNULL(@TenantID, ug.TenantID)
                JOIN AccessControl.ApiResource AS ar ON ar.ApiResourceID = ugp.ApiResourceID
                JOIN AccessControl.ApiResourceType AS art ON art.ApiResourceTypeID = ar.ApiResourceTypeID
                JOIN AccessControl.ApiType AS at ON at.ApiTypeID = ar.ApiTypeID
                WHERE ugp.UserGroupID = ISNULL(@UserGroupID, ugp.UserGroupID) AND ugp.IsActive = 1
END

