﻿
-- =============================================
-- Author:  Andrei Ramanovich
-- Create date: 01/11/2018
-- Update date: 01/19/2018 Performance improved
-- Update date: 09/18/2018 by Igor Kutsenka : TenantSpecific support
-- Update date: 9/05/2019 Andrei Ramanovich - remove B2CPolicyConfiguration table usage
-- Description: Returns AadInstance and DirectoryTenant tenant values for b2c configurations
-- =============================================
CREATE PROCEDURE [AccessControl].[GetB2CConfigurationsFromTenantProperties] 
AS
BEGIN
	SET NOCOUNT ON;
	--Provider ID 2 is AzureB2CExternal which means tenant specific B2C
	
	SELECT a.TenantID, ac.ProviderID, ac.ClientID, ac.ConfigJson
		,COALESCE(ai.[PropertyValue], ai.[DefaultPropertyValue]) AS AadInstance
		,COALESCE(dt.[PropertyValue], dt.[DefaultPropertyValue]) AS DirectoryTenant
		FROM [AccessControl].[Application] a
		JOIN [AccessControl].[ApplicationConfiguration] ac ON a.ApplicationID = ac.ApplicationID
		CROSS APPLY dbo.GetTenantPropertyInfo(IIF(ac.ProviderID = 2, a.TenantID, '00000000-0000-0000-0000-000000000000'), 'B2C.AADInstance', DEFAULT) AS ai
		CROSS APPLY dbo.GetTenantPropertyInfo(IIF(ac.ProviderID = 2, a.TenantID, '00000000-0000-0000-0000-000000000000'), 'B2C.DirectoryTenant', DEFAULT) AS dt
		WHERE (ac.ProviderID = 1 OR ac.ProviderID = 2)
END
