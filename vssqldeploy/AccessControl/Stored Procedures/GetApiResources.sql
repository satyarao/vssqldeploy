﻿
-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 10/9/2015
-- Update date: 11/08/2017 Igor Gaidukov - added ApiResourceCategory to ApiResource table
-- Description:	Get available API resources to be able to add them to a user group
-- 
-- =============================================
CREATE PROCEDURE [AccessControl].[GetApiResources]
	  @ApiTypeID INT = NULL
	 ,@ApiResourceTypeID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SELECT ar.ApiResourceID, ar.ApiResourceName, ar.ApiResourceCategory, ar.ApiResourceCode, art.ApiResourceTypeID, art.ApiResourceType, at.ApiTypeID, at.ApiType
	FROM AccessControl.ApiResource AS ar
	JOIN AccessControl.ApiResourceType AS art ON art.ApiResourceTypeID = ar.ApiResourceTypeID
	JOIN AccessControl.ApiType AS at ON at.ApiTypeID = ar.ApiTypeID
	WHERE ar.ApiTypeID = ISNULL(@ApiTypeID, ar.ApiTypeID) AND ar.ApiResourceTypeID = ISNULL(@ApiResourceTypeID, ar.ApiResourceTypeID) AND ar.IsActive = 1
END
