﻿
-- =============================================
-- Author:		Alexandr Goroshko
-- Create date: 10/8/2015
-- Description:	Assign user to user group
-- 8/6/2019:    Sergei Buday - PLAT-4004 [Identity] Clean up membership reboot stuff that we don't need
-- =============================================
CREATE PROCEDURE [AccessControl].[AssignGroupToUser]
	 @TenantID UNIQUEIDENTIFIER
	,@UserID UNIQUEIDENTIFIER
	,@UserGroupID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF (EXISTS(SELECT * 
			   FROM AccessControl.UserGroup
			   WHERE UserGroupID = @UserGroupID AND TenantID = @TenantID))
	BEGIN
		INSERT INTO [AccessControl].[UserGroupAssignment] ([UserID], [UserGroupID])
		VALUES(@UserID, @UserGroupID)
	END
END

