﻿-- =============================================
-- Author:		Justin Blau
-- Create date: 8/12/2016
-- Description:	Add User Claim
-- =============================================
CREATE PROCEDURE [AccessControl].[AddClaimForUser]
	@UserID UNIQUEIDENTIFIER,
	@Type NVARCHAR(150),
	@Value NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (
		SELECT [Key] 
		FROM [AccessControl].[UserClaims]
		WHERE [UserID] = @UserID AND 
			[Type] = @Type AND 
			[Value] = @Value)
	BEGIN
	INSERT INTO [AccessControl].[UserClaims]
           ([UserID]
           ,[Type]
           ,[Value])
     VALUES
           (@UserID
           ,@Type
           ,@Value)
	END
END
