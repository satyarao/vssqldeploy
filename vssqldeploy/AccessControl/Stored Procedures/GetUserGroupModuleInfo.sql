﻿
-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 6/22/2017
-- Description:	Get usergroup module info
-- 08/06/2019: Sergei Buday - PLAT-4004 [Identity] Clean up membership reboot stuff that we don't need
-- =============================================
CREATE PROCEDURE [AccessControl].[GetUserGroupModuleInfo]
	@TenantID UNIQUEIDENTIFIER,
	@UserGroupModuleID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ugm.UserGroupModuleID
		, UserGroupModuleName
		, UsersLimit
		, (SELECT COUNT (*) AS a 
			FROM (SELECT DISTINCT uga.UserGroupID 
								, ug.[TenantID] AS GroupTenantID
								, ug.UserGroupModuleID 
								FROM [AccessControl].[UserGroupAssignment] uga 
								RIGHT JOIN [AccessControl].[UserGroup] ug ON uga.UserGroupID = ug.UserGroupID
								WHERE ug.IsActive = 1 
									AND ug.TenantID = @TenantID 
									AND ug.UserGroupModuleID = ugm.[UserGroupModuleID]) AS tmp) AS AssignedUsersCnt
		, CAST( ISNULL( ( SELECT TOP 1 1 FROM [AccessControl].[UserGroup] ug WHERE ug.UserGroupModuleID = ugm.UserGroupModuleID AND ug.TenantID = @TenantID AND ug.IsActive = 1), 0) AS BIT) AS HasGroups
		FROM [AccessControl].[UserGroupModule] ugm
		LEFT JOIN ( SELECT * FROM [AccessControl].[ModuleUsersLimit] mul WHERE mul.TenantID = @TenantID) t2 ON t2.[UserGroupModuleID] = ugm.[UserGroupModuleID]
		WHERE @UserGroupModuleID IS NULL OR @UserGroupModuleID = ugm.UserGroupModuleID
END


