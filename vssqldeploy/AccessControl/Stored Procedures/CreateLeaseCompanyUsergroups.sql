﻿-- =============================================
-- Author:		Davydovich Raman
-- Create date: 12/13/2018
-- Description:	Create Lease Admin and Lease Manager user groups
-- Update by Batalava Darya 01/03/2019   - Added fleet:management_fee permissions
-- Update by Davydovich Raman 12/18/2018 - Permissions were updated
-- Update by Davydovich Raman 12/29/2018 - Added fleet:vehicle_management permissions
-- Update by Davydovich Raman 01/02/2019 - Update 'Lease Company Manager' permissions
-- Update by Vadim Prikich 2/13/2019 - Added service_transactions permissions
-- =============================================
CREATE PROCEDURE [AccessControl].[CreateLeaseCompanyUsergroups]
	 @LeaseTenantID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @UserGroupID INT;
	
	IF(NOT EXISTS(SELECT 1 FROM [AccessControl].[UserGroup] WHERE UserGroupName = 'Lease Company Manager' AND TenantID = @LeaseTenantID))
	BEGIN
		INSERT INTO AccessControl.UserGroup (TenantID, UserGroupName, UserGroupModuleID)
		VALUES (@LeaseTenantID, 'Lease Company Manager', 5) -- 5 - "means Other"
		SET @UserGroupID = (SELECT SCOPE_IDENTITY())

		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'lease:company'), 1, 0, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'lease:company:user_management'), 1, 0, 0 )

		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:user_management'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_management'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:vehicle_management'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:management_fee'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'service_transactions'), 1, 1, 1 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company:rci:restrictions'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'lease:company:rci:restrictions'), 1, 0, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_category:rci:restrictions'), 1, 1, 0 )

	END

	IF(NOT EXISTS(SELECT 1 FROM [AccessControl].[UserGroup] WHERE UserGroupName = 'Lease Company Admin' AND TenantID = @LeaseTenantID))
	BEGIN
		INSERT INTO AccessControl.UserGroup (TenantID, UserGroupName, UserGroupModuleID)
		VALUES (@LeaseTenantID, 'Lease Company Admin', 5) -- 5 - "means Other"
		SET @UserGroupID = (SELECT SCOPE_IDENTITY())

		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'lease:company'), 1, 0, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'lease:company:user_management'), 1, 1, 0 )

		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:user_management'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_management'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:vehicle_management'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:management_fee'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'service_transactions'), 1, 1, 1 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:company:rci:restrictions'), 1, 1, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'lease:company:rci:restrictions'), 1, 0, 0 )
		INSERT INTO [AccessControl].[UserGroupResource] (UserGroupID, ApiResourceID, ReadPermission, WritePermission, DeletePermission)
		VALUES(@UserGroupID, (select TOP(1) ApiResourceID FROM AccessControl.ApiResource WHERE ApiResourceCode = 'fleet:driver_category:rci:restrictions'), 1, 1, 0 )
	END
END	



