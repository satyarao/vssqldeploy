﻿
-- =============================================
-- Author: Andrei Ramanovich
-- Create date: 9/6/2019
-- Description:	Add or update Application Configuration
-- =============================================
CREATE PROCEDURE [AccessControl].[IUApplicationConfiguration]
	@TenantID UNIQUEIDENTIFIER,
	@ApplicationType smallint,
    @ProviderID int,
    @Version NVARCHAR(128) = NULL,
	@ClientID NVARCHAR(128),
    @ClientSecret NVARCHAR(128) = NULL,
	@ConfigJson NVARCHAR(max) = NULL
AS
BEGIN
	DECLARE @ApplicationID UNIQUEIDENTIFIER;
	DECLARE @InsertedApplicationIds table([ApplicationID] UNIQUEIDENTIFIER);
	
	IF EXISTS (SELECT * FROM [AccessControl].[Application] WHERE TenantID = @TenantID AND ApplicationType = @ApplicationType)
	BEGIN
		SELECT @ApplicationID = [ApplicationID] FROM [AccessControl].[Application] WHERE TenantID = @TenantID AND ApplicationType = @ApplicationType
	END
	ELSE
	BEGIN
		INSERT INTO [AccessControl].[Application] (TenantID, ApplicationName, ApplicationType)
		OUTPUT INSERTED.ApplicationID INTO @InsertedApplicationIds
		VALUES (@TenantID, LOWER(REPLACE( (SELECT TOP 1 Name FROM dbo.Tenant t WHERE t.TenantID = @TenantID), ' ', '')), @ApplicationType)

		SELECT @ApplicationID = [ApplicationID] FROM @InsertedApplicationIds
	END

	IF NOT EXISTS (SELECT * FROM [AccessControl].[ApplicationConfiguration] WHERE ApplicationID = @ApplicationID AND ProviderID = @ProviderID AND ([Version] = @Version OR ([Version] IS NULL AND @Version IS NULL)) )
	BEGIN
		INSERT INTO AccessControl.ApplicationConfiguration (ApplicationID, ProviderID, [Version], ClientID, ClientSecret, ConfigJson)
		VALUES (@ApplicationID, @ProviderID, @Version, @ClientID, @ClientSecret, @ConfigJson)
	END
	ELSE
	BEGIN
		UPDATE [AccessControl].[ApplicationConfiguration]
		SET 
			ClientID = @ClientID, 
			ClientSecret = @ClientSecret, 
			ConfigJson = @ConfigJson
		WHERE ApplicationID = @ApplicationID AND ProviderID = @ProviderID AND ([Version] = @Version OR ([Version] IS NULL AND @Version IS NULL))
	END
END
