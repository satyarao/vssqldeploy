﻿
-- =============================================
-- Create date: 9/26/2016
-- Update date: 8/20/2019 Andrei Ramanovich - add new application configuration tables support
-- Update date: 9/05/2019 Andrei Ramanovich - remove B2CPolicyConfiguration table usage
-- Description:	Returns tenantId for an applicationid
-- =============================================
CREATE FUNCTION [AccessControl].[GetTenantIdForApplicationId]
(
	@ClientID nvarchar(128)
)
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	DECLARE @TenantID UNIQUEIDENTIFIER

	;WITH cte(TenantID)
	AS
	(	
		SELECT a.TenantID
		FROM [AccessControl].[Application] a
		JOIN [AccessControl].[ApplicationConfiguration] ac ON a.ApplicationID = ac.ApplicationID
		WHERE ac.ClientID = @ClientID
	 )
	 SELECT @TenantID = MIN(TenantID) FROM cte

	 RETURN @TenantID;
END
