﻿CREATE PROCEDURE [KM].[DeleteStoreKey]
	@StoreID UNIQUEIDENTIFIER
AS /*
DECLARE	@StoreID UNIQUEIDENTIFIER
SELECT TOP 1
		@StoreID = storeid
FROM	KM.StoreKey

EXEC [KM].[DeleteStoreKey] @StoreID = @StoreID
*/
SET NOCOUNT ON;

DELETE	[KM].[StoreKey]
WHERE	StoreID = @StoreID
