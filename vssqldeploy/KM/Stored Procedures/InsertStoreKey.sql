﻿CREATE PROCEDURE [KM].[InsertStoreKey]
	@StoreID UNIQUEIDENTIFIER
  , @EncKey VARCHAR(128)
AS /*
DECLARE @StoreID UNIQUEIDENTIFIER
SELECT @StoreID = NEWID()
DECLARE @EncKey VARCHAR(128)
SELECT @EncKey = 'asdasdasdasdasdasdasdasdasdasdasdasd'

exec [KM].[InsertStoreKey]
	@StoreID = @StoreID
  , @EncKey = @EncKey
*/
SET NOCOUNT ON;

DELETE	FROM [KM].[StoreKey]
WHERE	StoreID = @StoreID

INSERT	INTO [KM].[StoreKey]
		( StoreID, EncKey )
VALUES	( @StoreID, @EncKey )
