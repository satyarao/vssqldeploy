﻿CREATE TABLE [KM].[StoreKeyLog] (
    [EncKeyID]    INT           NOT NULL,
    [Comment]     NVARCHAR (50) NOT NULL,
    [LogDateTime] DATETIME2 (7) CONSTRAINT [DF_StoreKeyLog_ActionDateTime] DEFAULT (getutcdate()) NOT NULL
);

