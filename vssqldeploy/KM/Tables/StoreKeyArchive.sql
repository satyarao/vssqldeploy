﻿CREATE TABLE [KM].[StoreKeyArchive] (
    [EncKeyID] INT              NOT NULL,
    [StoreID]  UNIQUEIDENTIFIER NOT NULL,
    [EncKey]   VARCHAR (128)    NOT NULL
);

