﻿CREATE TABLE [KM].[StoreKey] (
    [EncKeyID] INT              IDENTITY (1, 1) NOT NULL,
    [StoreID]  UNIQUEIDENTIFIER NOT NULL,
    [EncKey]   VARCHAR (128)    NOT NULL,
    CONSTRAINT [PK_StoreKey_1] PRIMARY KEY CLUSTERED ([StoreID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE TRIGGER [KM].[TrDeleteStoreKey] ON [KM].[StoreKey]
	AFTER DELETE
AS
BEGIN
	SET NOCOUNT ON;

	INSERT	INTO [KM].[StoreKeyArchive]
			( EncKeyID
			, StoreID
			, EncKey
			)
			SELECT	EncKeyID
				  , StoreID
				  , EncKey
			FROM	Deleted

	INSERT	INTO [KM].[StoreKeyLog]
			( EncKeyID
			, Comment
			)
			SELECT	EncKeyID
				  , 'Deleted'
			FROM	Deleted
END

GO
CREATE TRIGGER [KM].[TrInsertStoreKey] ON [KM].[StoreKey]
	AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT	INTO [KM].[StoreKeyLog]
			( EncKeyID
			, Comment
			)
			SELECT	EncKeyID
				  , 'Inserted'
			FROM	Inserted
END
