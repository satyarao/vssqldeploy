﻿
CREATE FUNCTION [Compatible].[VerifiedUserPaymentSource]
	(
	  @UserPaymentSourceID INT
	, @UserID UNIQUEIDENTIFIER = NULL
	, @InstallationID NVARCHAR(128) = NULL
	)
RETURNS BIT
AS
BEGIN
	DECLARE	@VerifiedFunding BIT = 0

	IF @UserID IS NULL AND @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationID)
	END

	--Sanatize Input
	IF @UserPaymentSourceID = 0
	BEGIN
		SET @UserPaymentSourceID = NULL
	END

	--If @UserPaymentSourceID is null, check if any active, default exist.
	--If @UserPaymentSourceID is not null, check if that source is active.
	IF @UserID IS NOT NULL AND EXISTS ( SELECT	*
				FROM	[Payment].[LKUserPaymentSource]
				WHERE	UserID = @UserID
						AND IsActive = 1
						AND (@UserPaymentSourceID IS NOT NULL) 
						AND UserPaymentSourceID = ISNULL(@UserPaymentSourceID, UserPaymentSourceID) )
	BEGIN
		SET @VerifiedFunding = 1
	END

	RETURN @VerifiedFunding
END
