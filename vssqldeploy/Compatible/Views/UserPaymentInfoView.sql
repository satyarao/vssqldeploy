﻿----------------------------------------------------------------------------------------

-- =============================================
-- Create date: 2016/03/02
-- Description:    [Compatible Version] View of the payment 
-- source information used during transaction purchase
-- =============================================
CREATE   VIEW [Compatible].[UserPaymentInfoView] AS
Select  --Precidia
                UserPaymentSourceID                  AS UserPaymentSourceID,
                PaymentProviderToken AS PaymentProviderToken, 
                FirstSix                                                  AS FirstSix,
                LastFour                                                               AS LastFour, 
                ExpDate                                                                               AS ExpDate,
                NULL                                                                     AS EmailAddress
From [Payment].[UserPrecidiaPaymentSource]
UNION
SELECT  --Zipline
                UserPaymentSourceID                  AS UserPaymentSourceID,
                'NPCA'                                                                  AS PaymentProviderToken, 
                'npca_9'                                                               AS FirstSix,
                '9999'                                                                     AS LastFour, 
                '1299'                                                                     AS ExpDate,
                ZiplineEmailAddress                       AS EmailAddress
FROM [Payment].[UserZiplinePaymentSource]
UNION
SELECT  --BIM
                UserPaymentSourceID                  AS UserPaymentSourceID,
                ConnectionCode                                              AS PaymentProviderToken, 
                'bim__9'                                                               AS FirstSix,
                '9999'                                                                     AS LastFour, 
                '1299'                                                                     AS ExpDate,
                EmailAddress                                     AS EmailAddress
FROM [Payment].[UserBIMPaymentSource]
UNION
SELECT  --Mock
                UserPaymentSourceID                  AS UserPaymentSourceID,
                'Auto'                                                                    AS PaymentProviderToken, 
                '979797'                                                                AS FirstSix,
                '9797'                                                                     AS LastFour, 
                '1299'                                                                     AS ExpDate,
                NULL                                                                     AS EmailAddress
FROM [Payment].[UserMockPaymentSource]
UNION
SELECT  --LoyaltyOnly
                UserPaymentSourceID                  AS UserPaymentSourceID,
                'LoyaltyOnly'                                      AS PaymentProviderToken, 
                '979797'                                                                AS FirstSix,
                '9797'                                                                     AS LastFour, 
                '1299'                                                                     AS ExpDate,
                NULL                                                                     AS EmailAddress
FROM [Payment].[UserMockPaymentSource]
UNION   
SELECT  --Token ex
                UserPaymentSourceID                  AS UserPaymentSourceID,
                [PaymentProviderToken]             AS PaymentProviderToken, 
                [FirstSix]                                                              AS FirstSix,
                [LastFour]                                                           AS LastFour, 
                [ExpDate]                                                            AS ExpDate,
                NULL                                                                     AS EmailAddress
FROM [Payment].[UserTokenExPaymentSource]
UNION
SELECT  --Synchrony
                [UserPaymentSourceId] AS UserPaymentSourceID,
                [PaymentProviderToken]             AS PaymentProviderToken, 
                [FirstSix]                                                              AS FirstSix,
                [LastFour]                                                           AS LastFour, 
                NULL                                                                     AS ExpDate,
                NULL                                                                     AS EmailAddress
FROM [Payment].[UserSynchronyPaymentSource] syn
UNION
SELECT  --TNS Card On File
                [UserPaymentSourceId] AS UserPaymentSourceID,
                [PaymentProviderToken]             AS PaymentProviderToken, 
                [FirstSix]                                                              AS FirstSix,
                [LastFour]                                                           AS LastFour, 
                [ExpDate]                                                            AS ExpDate,
                NULL                                                                     AS EmailAddress
FROM [Payment].[UserTNSCardOnFilePaymentSource] tns

