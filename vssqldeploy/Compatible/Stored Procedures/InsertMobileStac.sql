﻿
-- =============================================
-- Create date: 2016/03/02
-- Description:	[Compatible Version ]Creates a STAC (Single Txn Auth Code)
-- =============================================
CREATE PROCEDURE [Compatible].[InsertMobileStac] 
	@InstallationId nvarchar(128),
	@UserPaymentSourceID int = null,
	@Token uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PaymentProviderToken NVARCHAR(100) = NULL
	DECLARE @Expiration DATETIME2(7)
	DECLARE @UserID UNIQUEIDENTIFIER
	DECLARE @PaymentProviderName NVARCHAR(MAX) = NULL

	SET @Expiration = DATEADD(minute, 30, GETUTCDATE()) --Add 30 minutes from Now

	IF @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationId)
	END

	IF @UserID IS NOT NULL AND @UserPaymentSourceID != 0
	BEGIN
		IF @UserPaymentSourceID IS NULL
		BEGIN
			SELECT TOP 1
				@UserPaymentSourceID = UserPaymentSourceID
			FROM [Payment].[LKUserPaymentSource]
			WHERE UserID = @UserID AND IsActive = 1
			ORDER BY UpdatedOn desc, CreatedOn desc
		END

		IF EXISTS(SELECT TOP 1 * FROM [Payment].[LKUserPaymentSource] WHERE @UserPaymentSourceID = UserPaymentSourceID AND IsActive = 1)
		BEGIN
			SELECT 
				  @PaymentProviderToken = info.PaymentProviderToken
				, @PaymentProviderName = FP.[FundingProviderName]
			FROM [Payment].[LKUserPaymentSource] LK
			LEFT JOIN [Compatible].[UserPaymentInfoView] info on info.UserPaymentSourceID = LK.UserPaymentSourceID
			LEFT JOIN [Payment].[LKPaymentProcessor] pp on pp.[PaymentProcessorID] = LK.[PaymentProcessorID]
			LEFT JOIN [Payment].[FundingProvider] FP on FP.[FundingProviderID] = pp.[FundingProviderID]
			WHERE LK.UserPaymentSourceID = @UserPaymentSourceID AND LK.IsActive = 1

			SET @Token = ISNULL(@Token, NEWID())

			INSERT INTO STAC(Token, UserPaymentSourceID, Expiration ,TokenStatusID)
			VALUES
				(@Token
				,@UserPaymentSourceID
				,@Expiration
				,1 -- New
				)
			SELECT 
				 @Token					as Token
				,@InstallationID		as InstallationID
				,@UserPaymentSourceID	as UserPaymentSourceID
				,@PaymentProviderName	as PaymentProviderName
				,@PaymentProviderToken	as PaymentProviderToken
				,@Expiration			as Expiration
		END
		ELSE
		BEGIN
			SELECT NULL
		END
	END
	ELSE
	BEGIN
		SELECT NULL
	END
END
