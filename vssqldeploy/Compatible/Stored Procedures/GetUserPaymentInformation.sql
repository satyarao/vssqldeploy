﻿
-- =============================================
-- Create date: 2016/03/01
-- Update date: 05/31/2017 - Igor Gaidukov - delete IsDefault functionality
-- Description:	Retrieves a user's payment information, optionally from their payment source of choice
-- =============================================
CREATE PROCEDURE [Compatible].[GetUserPaymentInformation] 
	@InstallationID nvarchar(128), 
	@UserPaymentSourceID int = NULL -- optional, allow users to specify a payment source
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @UserID UNIQUEIDENTIFIER

	IF @UserID IS NULL AND @InstallationID IS NOT NULL
	BEGIN
		SET @UserID = [dbo].[GetUserIDByInstallID] (@InstallationID)
	END

	IF @UserID IS NOT NULL --AND @UserPaymentSourceID != 0
	BEGIN
		SELECT TOP 1
			LK.UserPaymentSourceID	as UserPaymentSourceID
			,info.FirstSix				as FirstSix
			,info.LastFour				as LastFour
			,pp.CardType				as CardType
			,pp.CardIssuer				as CardIssuer
			,CASE WHEN info.ExpDate IS NOT NULL AND LEN(info.ExpDate) = 4 THEN info.ExpDate ELSE '1299' END as ExpDate
		FROM [Payment].[LKUserPaymentSource] LK
		LEFT JOIN [Compatible].[UserPaymentInfoView] info on info.UserPaymentSourceID = LK.UserPaymentSourceID
		LEFT JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = LK.PaymentProcessorID
		JOIN [dbo].[UserInfo] ui on ui.UserID = @UserID
		WHERE LK.UserID = @UserID AND LK.IsActive = 1
		AND LK.UserPaymentSourceID = ISNULL(@UserPaymentSourceID, LK.UserPaymentSourceID) 
		AND ( @UserPaymentSourceID IS NOT NULL )
		AND [Payment].[IsFundingProviderAllowedByTenant](pp.FundingProviderID, ui.TenantID) = 1
	END
END
