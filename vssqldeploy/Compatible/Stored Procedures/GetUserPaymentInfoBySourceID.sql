﻿-- =============================================
-- Create date: 12/15/2016
-- Description:	Retrieves Payment Details for a source id
-- =============================================
CREATE PROCEDURE [Compatible].[GetUserPaymentInfoBySourceID]
	@UserPaymentSourceID int
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT TOP 1 * FROM [Payment].[LKUserPaymentSource] WHERE @UserPaymentSourceID = UserPaymentSourceID AND IsActive = 1)
	BEGIN
		SELECT
			info.[PaymentProviderToken] as PaymentProviderToken,
			FP.[FundingProviderName] as PaymentProviderName
		FROM [Payment].[LKUserPaymentSource] LK
		LEFT JOIN [Compatible].[UserPaymentInfoView] info on info.UserPaymentSourceID = LK.UserPaymentSourceID
		LEFT JOIN [Payment].[LKPaymentProcessor] pp on pp.[PaymentProcessorID] = LK.[PaymentProcessorID]
		LEFT JOIN [Payment].[FundingProvider] FP on FP.[FundingProviderID] = pp.[FundingProviderID]
		WHERE LK.UserPaymentSourceID = @UserPaymentSourceID AND LK.IsActive = 1
	END
	ELSE
	BEGIN
		SELECT NULL
	END
END
