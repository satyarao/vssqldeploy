﻿
CREATE PROCEDURE [Compatible].[GetUserPaymentSource]
	@UserID UNIQUEIDENTIFIER
AS
BEGIN
	SELECT
		LK.UserPaymentSourceID,
		Uni.LastFour,
		Uni.ExpDate,
		pp.PaymentProcessorName as CardType
	FROM [Payment].[LKUserPaymentSource] LK
	LEFT JOIN (
		SELECT 
			UserPaymentSourceID,
			LastFour,
			ExpDate
		FROM [Payment].[UserPrecidiaPaymentSource]
		UNION
		SELECT 
			UserPaymentSourceID,
			'9797' as LastFour,
			'1299' as ExpDate
		FROM [Payment].[UserMockPaymentSource]
	) Uni ON LK.UserPaymentSourceID = Uni.UserPaymentSourceID
	LEFT JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = LK.PaymentProcessorID
	WHERE LK.UserID = @UserID And LK.IsActive = 1
	ORDER BY LK.CreatedOn desc
END
