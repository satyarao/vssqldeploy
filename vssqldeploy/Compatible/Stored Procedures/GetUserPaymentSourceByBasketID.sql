﻿
-- =============================================
-- Create date: 3/02/2016
-- Description:	[Compatible Version] Gets the UserPaymentSource From a BasketID
-- =============================================
/*
EXEC Compatible.GetUserPaymentSourceByBasketID @BasketID = 'C968D743-4657-4FBF-AE86-FDD750232BBB'
*/
CREATE PROCEDURE [Compatible].[GetUserPaymentSourceByBasketID]
	@BasketID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		  LK.UserPaymentSourceID
		, LK.UserID
		, info.PaymentProviderToken
		, info.LastFour
		, info.ExpDate
		, LK.PaymentProcessorID AS CardTypeID
		, pp.PaymentProcessorName AS CardType
	FROM [Payment].[LKUserPaymentSource] LK
	LEFT JOIN [Compatible].[UserPaymentInfoView] info on info.UserPaymentSourceID = LK.UserPaymentSourceID
	LEFT JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = LK.PaymentProcessorID
	WHERE LK.UserPaymentSourceID = 
		(SELECT TOP 1 UserPaymentSourceID FROM dbo.BasketPayment 
		where BasketID = @BasketID order by BasketPaymentID desc )
END
