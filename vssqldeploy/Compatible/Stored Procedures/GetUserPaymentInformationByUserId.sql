﻿CREATE PROCEDURE [Compatible].[GetUserPaymentInformationByUserId] 
	@UserID UNIQUEIDENTIFIER, 
	@UserPaymentSourceID int = NULL -- optional, allow users to specify a payment source
AS
BEGIN
	SET NOCOUNT ON;

	IF @UserID IS NOT NULL
	BEGIN
		SELECT TOP 1
			LK.UserPaymentSourceID	as UserPaymentSourceID
			,info.FirstSix				as FirstSix
			,info.LastFour				as LastFour
			,pp.CardType				as CardType
			,pp.CardIssuer				as CardIssuer
			,info.ExpDate				as ExpDate
			,pp.PaymentType				as PaymentType
			,pp.PaymentProcessorID      as PaymentProcessorID
			,pp.FundingProviderID       as FundingProviderID
		FROM [Payment].[LKUserPaymentSource] LK
		LEFT JOIN [Compatible].[UserPaymentInfoView] info on info.UserPaymentSourceID = LK.UserPaymentSourceID
		LEFT JOIN [Payment].[LKPaymentProcessor] pp on pp.PaymentProcessorID = LK.PaymentProcessorID
		JOIN [dbo].[UserInfo] ui on ui.UserID = @UserID
		WHERE LK.UserID = @UserID AND LK.IsActive = 1
		AND LK.UserPaymentSourceID = ISNULL(@UserPaymentSourceID, LK.UserPaymentSourceID) 
		AND ( @UserPaymentSourceID IS NOT NULL)
		AND [Payment].[IsFundingProviderAllowedByTenant](pp.FundingProviderID, ui.TenantID) = 1
	END
END

