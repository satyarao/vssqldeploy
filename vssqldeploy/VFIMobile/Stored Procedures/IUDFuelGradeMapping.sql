﻿
CREATE PROCEDURE [VFIMobile].[IUDFuelGradeMapping] 
	@StoreID UNIQUEIDENTIFIER,
	@FuelGradeID int,
	@ProductCode NVARCHAR(20),
	@OctaneRating tinyint,
	@UOM NVARCHAR(50),
	@Delete BIT = 0
AS 
BEGIN
	SET NOCOUNT ON;
	IF @Delete = 1
	BEGIN
		DELETE FROM [VFIMobile].[LKFuelGradeMapping] WHERE StoreID = @StoreID AND FuelGradeID = @FuelGradeID
	END
	ELSE
	BEGIN
		MERGE [VFIMobile].[LKFuelGradeMapping] AS target
		USING
			(SELECT
				    @StoreID AS StoreID
				  , @FuelGradeID AS FuelGradeID
				  , @ProductCode AS ProductCode
				  , @OctaneRating AS OctaneRating
				  , @UOM AS UOM)
				  AS source (StoreID, FuelGradeID, ProductCode, OctaneRating, UOM)
		ON (target.StoreID = source.StoreID AND target.FuelGradeID = source.FuelGradeID)
		WHEN MATCHED THEN
			UPDATE SET ProductCode = ISNULL(source.ProductCode, target.ProductCode)
					 , OctaneRating = ISNULL(source.OctaneRating, target.OctaneRating)
					 , UOM = ISNULL(source.UOM, target.UOM)
		WHEN NOT MATCHED THEN
				INSERT (
					StoreID
				  , FuelGradeID
				  , ProductCode
				  , OctaneRating
				  , UOM
				   )
			VALUES (
					@StoreID
				  , @FuelGradeID
				  , @ProductCode
				  , @OctaneRating
				  , @UOM);
	END
END
