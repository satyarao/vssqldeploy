﻿
CREATE PROCEDURE [VFIMobile].[GetFuelGradeMapping] 
	@StoreID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		 [StoreID]
		,[FuelGradeID]
		,[ProductCode]
		,[OctaneRating]
		,[UOM]
	FROM [VFIMobile].[LKFuelGradeMapping] 
	WHERE StoreID = @StoreID
END
