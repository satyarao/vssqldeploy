﻿CREATE TABLE [VFIMobile].[LKFuelGradeMapping] (
    [StoreID]      UNIQUEIDENTIFIER NOT NULL,
    [FuelGradeID]  INT              NOT NULL,
    [ProductCode]  NVARCHAR (20)    NOT NULL,
    [OctaneRating] TINYINT          NOT NULL,
    [UOM]          NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_LKFuelGradeMapping] PRIMARY KEY CLUSTERED ([StoreID] ASC, [FuelGradeID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_LKFuelGradeMapping_Store] FOREIGN KEY ([StoreID]) REFERENCES [dbo].[Store] ([StoreID])
);

