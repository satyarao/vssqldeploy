﻿CREATE TABLE [Offer].[DiscountLimit] (
    [ProductDiscountID] UNIQUEIDENTIFIER NOT NULL,
    [DiscountLimitType] NVARCHAR (20)    NOT NULL,
    [MinimumQuantity]   DECIMAL (6, 3)   NULL,
    [MaximumQuantity]   DECIMAL (6, 3)   NULL,
    [IsActive]          BIT              CONSTRAINT [DF_DiscountLimit_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_DiscountLimit_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_DiscountLimit] PRIMARY KEY CLUSTERED ([ProductDiscountID] ASC, [DiscountLimitType] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [CHK_DiscountLimit_MaximumQuantity] CHECK ([MaximumQuantity]>=(0)),
    CONSTRAINT [CHK_DiscountLimit_MinimumQuantity] CHECK ([MinimumQuantity]>=(0)),
    CONSTRAINT [FK_DiscountLimit_DiscountLimitType] FOREIGN KEY ([DiscountLimitType]) REFERENCES [Offer].[DiscountLimitType] ([DiscountLimitType]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_DiscountLimit_ProductDiscountID] FOREIGN KEY ([ProductDiscountID]) REFERENCES [Offer].[ProductDiscount] ([ProductDiscountID]) ON DELETE CASCADE ON UPDATE CASCADE
);

