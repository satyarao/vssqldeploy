﻿CREATE TABLE [Offer].[LkOffer_ProductDiscount] (
    [OfferID]           UNIQUEIDENTIFIER NOT NULL,
    [ProductDiscountID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]          BIT              CONSTRAINT [DF_LkOfferProductDiscount_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_LkOfferProductDiscount_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkOfferProductDiscount] PRIMARY KEY CLUSTERED ([OfferID] ASC, [ProductDiscountID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOfferProductDiscount_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkOfferProductDiscount_ProductDiscountID] FOREIGN KEY ([ProductDiscountID]) REFERENCES [Offer].[ProductDiscount] ([ProductDiscountID]) ON DELETE CASCADE ON UPDATE CASCADE
);

