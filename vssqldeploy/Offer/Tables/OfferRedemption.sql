﻿CREATE TABLE [Offer].[OfferRedemption] (
    [TxnID]                  UNIQUEIDENTIFIER NOT NULL,
    [OfferID]                UNIQUEIDENTIFIER NOT NULL,
    [UserID]                 UNIQUEIDENTIFIER NOT NULL,
    [CurrentRedemptionCount] INT              NOT NULL,
    [CreatedOn]              DATETIME2 (7)    CONSTRAINT [DF_OfferRedemption_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_OfferRedepmtion] PRIMARY KEY CLUSTERED ([OfferID] ASC, [UserID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_OfferRedemption_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_OfferRedemption_TxnID] FOREIGN KEY ([TxnID]) REFERENCES [Txn].[Txn] ([TxnID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_OfferRedemption_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);

