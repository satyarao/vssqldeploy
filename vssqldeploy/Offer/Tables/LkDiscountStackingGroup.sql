﻿CREATE TABLE [Offer].[LkDiscountStackingGroup] (
    [ProductDiscountID]       UNIQUEIDENTIFIER NOT NULL,
    [DiscountStackingGroupID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LkDiscountStackingGroup] PRIMARY KEY CLUSTERED ([ProductDiscountID] ASC, [DiscountStackingGroupID] ASC),
    CONSTRAINT [FK_LkDiscountStackingGroup_DiscountStackingGroupID] FOREIGN KEY ([DiscountStackingGroupID]) REFERENCES [Offer].[DiscountStackingGroup] ([DiscountStackingGroupID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkDiscountStackingGroup_ProductDiscountID] FOREIGN KEY ([ProductDiscountID]) REFERENCES [Offer].[ProductDiscount] ([ProductDiscountID]) ON DELETE CASCADE ON UPDATE CASCADE
);

