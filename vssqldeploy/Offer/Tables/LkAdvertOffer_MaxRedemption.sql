﻿CREATE TABLE [Offer].[LkAdvertOffer_MaxRedemption] (
    [AdvertOfferID]   UNIQUEIDENTIFIER NOT NULL,
    [MaxRedemptionID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              CONSTRAINT [DF_LkAdvertOfferMaxRedemption_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_LkAdvertOfferMaxRedemption_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkAdvertOfferMaxRedemption] PRIMARY KEY CLUSTERED ([AdvertOfferID] ASC, [MaxRedemptionID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkAdvertOfferMaxRedemption_AdvertOfferID] FOREIGN KEY ([AdvertOfferID]) REFERENCES [Offer].[AdvertOffer] ([AdvertOfferID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkAdvertOfferMaxRedemption_MaxRedemptionID] FOREIGN KEY ([MaxRedemptionID]) REFERENCES [Offer].[MaxRedemption] ([MaxRedemptionID]) ON DELETE CASCADE ON UPDATE CASCADE
);

