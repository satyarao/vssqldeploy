﻿CREATE TABLE [Offer].[ProductReward] (
    [ProductRewardID] UNIQUEIDENTIFIER NOT NULL,
    [OfferID]         UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_ProductReward_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_ProductReward] PRIMARY KEY CLUSTERED ([ProductRewardID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_ProductReward_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IDX_ProductReward_OfferID]
    ON [Offer].[ProductReward]([OfferID] ASC);

