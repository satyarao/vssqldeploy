﻿CREATE TABLE [Offer].[LkAdvertOffer_RuleFilter] (
    [AdvertOfferID] UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID]  UNIQUEIDENTIFIER NOT NULL,
    [IsActive]      BIT              CONSTRAINT [DF_LkAdvertOfferRuleFilter_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME2 (7)    CONSTRAINT [DF_LkAdvertOfferRuleFilter_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkAdvertOfferRuleFilter] PRIMARY KEY CLUSTERED ([AdvertOfferID] ASC, [RuleFilterID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkAdvertOfferRuleFilter_OfferID] FOREIGN KEY ([AdvertOfferID]) REFERENCES [Offer].[AdvertOffer] ([AdvertOfferID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkAdvertOfferRuleFilter_RuleFilterID] FOREIGN KEY ([RuleFilterID]) REFERENCES [dbo].[RuleFilter] ([RuleFilterID]) ON DELETE CASCADE ON UPDATE CASCADE
);

