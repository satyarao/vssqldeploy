﻿CREATE TABLE [Offer].[Offer] (
    [OfferID]                      UNIQUEIDENTIFIER NOT NULL,
    [AdvertOfferID]                UNIQUEIDENTIFIER NULL,
    [AutoUpdateAdvert]             BIT              NOT NULL,
    [OfferTemplateID]              UNIQUEIDENTIFIER NULL,
    [TenantID]                     UNIQUEIDENTIFIER NOT NULL,
    [OfferType]                    NVARCHAR (255)   NOT NULL,
    [RewardType]                   NVARCHAR (255)   NOT NULL,
    [TargetUsersType]              NVARCHAR (255)   CONSTRAINT [DF_Offer_TargetUsersType] DEFAULT ('authorized') NOT NULL,
    [EditorTitle]                  NVARCHAR (255)   NULL,
    [EditorDescription]            NVARCHAR (MAX)   NULL,
    [Title]                        NVARCHAR (255)   NOT NULL,
    [Subtitle]                     NVARCHAR (255)   CONSTRAINT [DF_Offer_Subtitle] DEFAULT ('') NOT NULL,
    [Description]                  NVARCHAR (MAX)   CONSTRAINT [DF_Offer_Description] DEFAULT ('') NOT NULL,
    [ImageURL]                     NVARCHAR (255)   NULL,
    [ImagePos]                     NVARCHAR (50)    CONSTRAINT [DF_Offer_ImagePos] DEFAULT ('top') NOT NULL,
    [Order]                        INT              NOT NULL,
    [IsFeaturedOffer]              BIT              NULL,
    [IsDeleted]                    BIT              CONSTRAINT [DF_Offer_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]                     BIT              NULL,
    [IsDraft]                      BIT              CONSTRAINT [DF_Offer_IsDraft] DEFAULT ((0)) NOT NULL,
    [IsRestricted]                 BIT              CONSTRAINT [DF_Offer_IsRestricted] DEFAULT ((0)) NOT NULL,
    [IsDisplay]                    BIT              NULL,
    [IsBrandedOffer]               BIT              CONSTRAINT [DF_Offer_BrandedOffer] DEFAULT ((0)) NOT NULL,
    [Display]                      NVARCHAR (50)    CONSTRAINT [DF_Offer_Display] DEFAULT ('deal') NOT NULL,
    [StartDate]                    DATETIME2 (7)    NULL,
    [EndDate]                      DATETIME2 (7)    NULL,
    [CreatedOn]                    DATETIME2 (7)    CONSTRAINT [DF_Offer_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                    NVARCHAR (255)   CONSTRAINT [DF_Offer_CreatedBy] DEFAULT ('SQL') NOT NULL,
    [CreatedByUserID]              UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]                    DATETIME2 (7)    CONSTRAINT [DF_Offer_UpdatedOn] DEFAULT (getutcdate()) NULL,
    [UpdatedBy]                    NVARCHAR (255)   CONSTRAINT [DF_Offer_UpdatedBy] DEFAULT ('SQL') NULL,
    [UpdatedByUserID]              UNIQUEIDENTIFIER NULL,
    [RewardID]                     NVARCHAR (50)    NULL,
    [IsDisplayTransactionProgress] BIT              CONSTRAINT [DF_Offer_IsDisplayTransactionProgress] DEFAULT ((0)) NOT NULL,
    [BarCode]                      NVARCHAR (255)   NULL,
    [BarCodeType]                  NVARCHAR (50)    NULL,
    [PunchCardDisplayType]         NVARCHAR (50)    NULL,
    [MaxPunchAmount]               INT              NULL,
    CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED ([OfferID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Offer_AdvertOfferID] FOREIGN KEY ([AdvertOfferID]) REFERENCES [Offer].[AdvertOffer] ([AdvertOfferID]),
    CONSTRAINT [FK_Offer_CreatedByUserID] FOREIGN KEY ([CreatedByUserID]) REFERENCES [dbo].[UserInfo] ([UserID]),
    CONSTRAINT [FK_Offer_OfferTemplateID] FOREIGN KEY ([OfferTemplateID]) REFERENCES [Offer].[OfferTemplate] ([OfferTemplateID]),
    CONSTRAINT [FK_Offer_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Offer_UpdatedByUserID] FOREIGN KEY ([UpdatedByUserID]) REFERENCES [dbo].[UserInfo] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IDX_Offer_AdvertOfferID]
    ON [Offer].[Offer]([AdvertOfferID] ASC) WHERE ([AdvertOfferID] IS NOT NULL) WITH (FILLFACTOR = 80);

