﻿CREATE TABLE [Offer].[Link] (
    [LinkID]    UNIQUEIDENTIFIER CONSTRAINT [DF_Link_LinkID] DEFAULT (newid()) NOT NULL,
    [LinkText]  NVARCHAR (255)   NULL,
    [LinkType]  NVARCHAR (255)   NOT NULL,
    [Url]       NVARCHAR (255)   NULL,
    [IsActive]  BIT              CONSTRAINT [DF_Link_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_Link_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_Link] PRIMARY KEY CLUSTERED ([LinkID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON)
);

