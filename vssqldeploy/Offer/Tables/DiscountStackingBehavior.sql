﻿CREATE TABLE [Offer].[DiscountStackingBehavior] (
    [DiscountStackingBehavior] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DiscountStackingBehavior] PRIMARY KEY CLUSTERED ([DiscountStackingBehavior] ASC)
);

