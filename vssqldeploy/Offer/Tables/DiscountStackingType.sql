﻿CREATE TABLE [Offer].[DiscountStackingType] (
    [DiscountStackingType] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DiscountStackingType] PRIMARY KEY CLUSTERED ([DiscountStackingType] ASC)
);

