﻿CREATE TABLE [Offer].[ExternalOffer] (
    [ExternalOfferID] UNIQUEIDENTIFIER CONSTRAINT [DF_ExternalOffer_ExternalOfferID] DEFAULT (newid()) NOT NULL,
    [ReferenceID]     NVARCHAR (50)    NOT NULL,
    [TenantID]        UNIQUEIDENTIFIER NOT NULL,
    [Description]     NVARCHAR (MAX)   NULL,
    [AuthorID]        NVARCHAR (50)    NULL,
    [StartDate]       DATETIME2 (7)    NULL,
    [EndDate]         DATETIME2 (7)    NULL,
    [IsConfigured]    BIT              CONSTRAINT [DF_ExternalOffer_IsConfigured] DEFAULT ((0)) NOT NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_ExternalOffer_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_ExternalOffer_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [DeletedOn]       DATETIME2 (7)    NULL,
    [ConfiguredOn]    DATETIME2 (7)    NULL,
    CONSTRAINT [PK_ExternalOffer] PRIMARY KEY CLUSTERED ([ExternalOfferID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_ExternalOffer_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

