﻿CREATE TABLE [Offer].[ProductDiscount] (
    [ProductDiscountID]      UNIQUEIDENTIFIER NOT NULL,
    [ProductRewardID]        UNIQUEIDENTIFIER CONSTRAINT [DF_ProductDiscount_ProductRewardID] DEFAULT (newid()) NULL,
    [StandardProductGroupID] UNIQUEIDENTIFIER NOT NULL,
    [DiscountType]           NVARCHAR (50)    NULL,
    [DiscountValue]          DECIMAL (7, 3)   CONSTRAINT [DF_ProductDiscount_DiscountValue] DEFAULT ((0)) NOT NULL,
    [DiscountCurrency]       NVARCHAR (50)    CONSTRAINT [DF_ProductDiscount_DiscountCurrency] DEFAULT ('USD') NOT NULL,
    [NewUnitPrice]           DECIMAL (7, 3)   NULL,
    [IsActive]               BIT              CONSTRAINT [DF_ProductDiscount_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]              DATETIME2 (7)    CONSTRAINT [DF_ProductDiscount_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [CouponPosId]            NVARCHAR (20)    NULL,
    [IsStackable]            BIT              NOT NULL,
    CONSTRAINT [PK_ProductDiscount] PRIMARY KEY CLUSTERED ([ProductDiscountID] ASC) WITH (FILLFACTOR = 80, IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_ProductDiscount_StandardProductGroupID] FOREIGN KEY ([StandardProductGroupID]) REFERENCES [Product].[StandardProductGroup] ([StandardProductGroupID])
);


GO
CREATE NONCLUSTERED INDEX [IDX_ProductDiscount_OfferD]
    ON [Offer].[ProductDiscount]([StandardProductGroupID] ASC) WITH (FILLFACTOR = 80);

