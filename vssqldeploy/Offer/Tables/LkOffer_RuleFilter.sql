﻿CREATE TABLE [Offer].[LkOffer_RuleFilter] (
    [OfferID]      UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]     BIT              CONSTRAINT [DF_LkOfferRuleFilter_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME2 (7)    CONSTRAINT [DF_LkOfferRuleFilter_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkOfferRuleFilter] PRIMARY KEY CLUSTERED ([OfferID] ASC, [RuleFilterID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOfferRuleFilter_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkOfferRuleFilter_RuleFilterID] FOREIGN KEY ([RuleFilterID]) REFERENCES [dbo].[RuleFilter] ([RuleFilterID]) ON DELETE CASCADE ON UPDATE CASCADE
);

