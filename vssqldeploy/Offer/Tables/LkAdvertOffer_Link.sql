﻿CREATE TABLE [Offer].[LkAdvertOffer_Link] (
    [LinkID]        UNIQUEIDENTIFIER NOT NULL,
    [AdvertOfferID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]      BIT              CONSTRAINT [DF_LkAdvertOfferLink_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME2 (7)    CONSTRAINT [DF_LkAdvertOfferLink_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKAdvertOfferLink] PRIMARY KEY CLUSTERED ([LinkID] ASC, [AdvertOfferID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkAdvertOfferLink_LinkID] FOREIGN KEY ([LinkID]) REFERENCES [Offer].[Link] ([LinkID]),
    CONSTRAINT [FK_LkAdvertOfferLink_OfferID] FOREIGN KEY ([AdvertOfferID]) REFERENCES [Offer].[AdvertOffer] ([AdvertOfferID])
);

