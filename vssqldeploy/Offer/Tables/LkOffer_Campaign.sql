﻿CREATE TABLE [Offer].[LkOffer_Campaign] (
    [OfferID]    UNIQUEIDENTIFIER NOT NULL,
    [CampaignID] INT              NOT NULL,
    [IsActive]   BIT              CONSTRAINT [DF_LkOfferCampaign_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME2 (7)    CONSTRAINT [DF_LkOfferCampaign_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkOfferCampaign] PRIMARY KEY CLUSTERED ([OfferID] ASC, [CampaignID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOfferCampaign_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE
);

