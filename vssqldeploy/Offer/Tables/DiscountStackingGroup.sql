﻿CREATE TABLE [Offer].[DiscountStackingGroup] (
    [DiscountStackingGroupID]  UNIQUEIDENTIFIER NOT NULL,
    [Name]                     NVARCHAR (50)    NULL,
    [DiscountStackingType]     NVARCHAR (20)    NULL,
    [DiscountStackingBehavior] NVARCHAR (20)    NULL,
    CONSTRAINT [PK_DiscountStackingGroup] PRIMARY KEY CLUSTERED ([DiscountStackingGroupID] ASC),
    CONSTRAINT [FK_DiscountStackingGroup_DiscountStackingBehavior] FOREIGN KEY ([DiscountStackingBehavior]) REFERENCES [Offer].[DiscountStackingBehavior] ([DiscountStackingBehavior]),
    CONSTRAINT [FK_DiscountStackingGroup_DiscountStackingType] FOREIGN KEY ([DiscountStackingType]) REFERENCES [Offer].[DiscountStackingType] ([DiscountStackingType])
);

