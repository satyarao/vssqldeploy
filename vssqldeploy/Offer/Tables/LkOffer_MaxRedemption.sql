﻿CREATE TABLE [Offer].[LkOffer_MaxRedemption] (
    [OfferID]         UNIQUEIDENTIFIER NOT NULL,
    [MaxRedemptionID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              CONSTRAINT [DF_LkOfferMaxRedemption_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_LkOfferMaxRedemption_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkOfferMaxRedemption] PRIMARY KEY CLUSTERED ([OfferID] ASC, [MaxRedemptionID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOfferMaxRedemption_MaxRedemptionID] FOREIGN KEY ([MaxRedemptionID]) REFERENCES [Offer].[MaxRedemption] ([MaxRedemptionID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkOfferMaxRedemption_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE
);

