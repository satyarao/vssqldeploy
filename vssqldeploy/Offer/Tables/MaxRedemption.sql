﻿CREATE TABLE [Offer].[MaxRedemption] (
    [MaxRedemptionID]     UNIQUEIDENTIFIER CONSTRAINT [DF_MaxRedemption_MaxRedemptionID] DEFAULT (newid()) NOT NULL,
    [RedemptionType]      NVARCHAR (10)    NOT NULL,
    [MaxRedemptions]      DECIMAL (10, 3)  CONSTRAINT [DF_MaxRedemption_MaxRedemptions] DEFAULT ((0)) NOT NULL,
    [RollingWindowDays]   TINYINT          CONSTRAINT [DF_MaxRedemption_RollingWindowDays] DEFAULT ((0)) NOT NULL,
    [RollingWindowWeeks]  TINYINT          CONSTRAINT [DF_MaxRedemption_RollingWindowWeeks] DEFAULT ((0)) NOT NULL,
    [RollingWindowMonths] TINYINT          CONSTRAINT [DF_MaxRedemption_RollingWindowMonths] DEFAULT ((0)) NOT NULL,
    [RollingWindowYears]  TINYINT          CONSTRAINT [DF_MaxRedemption_RollingWindowYears] DEFAULT ((0)) NOT NULL,
    [IsActive]            BIT              CONSTRAINT [DF_MaxRedemption_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]           DATETIME2 (7)    CONSTRAINT [DF_MaxRedemption_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_MaxRedemption] PRIMARY KEY CLUSTERED ([MaxRedemptionID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [CHK_MaxRedemption_MaxRedemptions] CHECK ([MaxRedemptions]>=(0)),
    CONSTRAINT [FK_MaxRedemption_RedemptionType] FOREIGN KEY ([RedemptionType]) REFERENCES [Offer].[RedemptionType] ([RedemptionType])
);

