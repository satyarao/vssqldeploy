﻿CREATE TABLE [Offer].[OfferCategory] (
    [OfferCategoryID] UNIQUEIDENTIFIER NOT NULL,
    [CategoryName]    NVARCHAR (255)   NOT NULL,
    [TenantID]        UNIQUEIDENTIFIER NULL,
    [IsActive]        BIT              CONSTRAINT [DF_OfferCategory_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_OfferCategory_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_OfferCategory] PRIMARY KEY CLUSTERED ([OfferCategoryID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_OfferCategory_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IDX_OfferCategory_TenantID]
    ON [Offer].[OfferCategory]([TenantID] ASC) WHERE ([TenantID] IS NOT NULL);

