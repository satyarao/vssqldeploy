﻿CREATE TABLE [Offer].[LkOffer_Link] (
    [LinkID]    UNIQUEIDENTIFIER NOT NULL,
    [OfferID]   UNIQUEIDENTIFIER NOT NULL,
    [IsActive]  BIT              CONSTRAINT [DF_LkOfferLink_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn] DATETIME2 (7)    CONSTRAINT [DF_LkOfferLink_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKOfferLink] PRIMARY KEY CLUSTERED ([LinkID] ASC, [OfferID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOfferLink_LinkID] FOREIGN KEY ([LinkID]) REFERENCES [Offer].[Link] ([LinkID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkOfferLink_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID])
);

