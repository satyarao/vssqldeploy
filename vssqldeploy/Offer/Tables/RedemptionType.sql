﻿CREATE TABLE [Offer].[RedemptionType] (
    [RedemptionType] NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK_RedemptionType] PRIMARY KEY CLUSTERED ([RedemptionType] ASC) WITH (IGNORE_DUP_KEY = ON)
);

