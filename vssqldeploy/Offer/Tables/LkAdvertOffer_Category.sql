﻿CREATE TABLE [Offer].[LkAdvertOffer_Category] (
    [OfferCategoryID] UNIQUEIDENTIFIER NOT NULL,
    [AdvertOfferID]   UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              CONSTRAINT [DF_LkAdvertOfferCategory_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_LkAdvertOfferCategory_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKAdvertOfferCategory] PRIMARY KEY CLUSTERED ([OfferCategoryID] ASC, [AdvertOfferID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkAdvertOfferCategory_OfferID] FOREIGN KEY ([AdvertOfferID]) REFERENCES [Offer].[AdvertOffer] ([AdvertOfferID]),
    CONSTRAINT [FK_LkAdvertOfferrCategory_OfferCategoryID] FOREIGN KEY ([OfferCategoryID]) REFERENCES [Offer].[OfferCategory] ([OfferCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE
);

