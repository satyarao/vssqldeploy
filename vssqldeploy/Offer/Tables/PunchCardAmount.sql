﻿CREATE TABLE [Offer].[PunchCardAmount] (
    [UserID]             UNIQUEIDENTIFIER NOT NULL,
    [OfferID]            UNIQUEIDENTIFIER NOT NULL,
    [CurrentPunchAmount] INT              NULL,
    [CreatedOn]          DATETIME2 (7)    DEFAULT (getutcdate()) NULL,
    [IsActivated]        BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Offer_PunchCardAmount] PRIMARY KEY CLUSTERED ([UserID] ASC, [OfferID] ASC),
    CONSTRAINT [FK_Offer_PunchCardAmount_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Offer_PunchCardAmount_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[UserInfo] ([UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

