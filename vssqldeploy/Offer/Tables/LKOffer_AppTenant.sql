﻿CREATE TABLE [Offer].[LKOffer_AppTenant] (
    [OfferID]     UNIQUEIDENTIFIER NOT NULL,
    [AppTenantID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]    BIT              CONSTRAINT [DF_LKOfferAppTenant_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME2 (7)    CONSTRAINT [DF_LKOfferAppTenant_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKOfferAppTenant] PRIMARY KEY CLUSTERED ([OfferID] ASC, [AppTenantID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKOfferAppTenant_AppTenantID] FOREIGN KEY ([AppTenantID]) REFERENCES [dbo].[Tenant] ([TenantID]),
    CONSTRAINT [FK_LKOfferAppTenant_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE
);

