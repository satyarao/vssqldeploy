﻿CREATE TABLE [Offer].[LkOffer_Category] (
    [OfferCategoryID] UNIQUEIDENTIFIER NOT NULL,
    [OfferID]         UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              CONSTRAINT [DF_LkOfferCategory_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_LkOfferCategory_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKOfferCategory] PRIMARY KEY CLUSTERED ([OfferCategoryID] ASC, [OfferID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOfferCategory_OfferCategoryID] FOREIGN KEY ([OfferCategoryID]) REFERENCES [Offer].[OfferCategory] ([OfferCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkOfferCategory_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID])
);

