﻿CREATE TABLE [Offer].[LKOffer_InterstitialZone] (
    [InterstitialZoneID] UNIQUEIDENTIFIER NOT NULL,
    [OfferID]            UNIQUEIDENTIFIER NOT NULL,
    [IsActive]           BIT              CONSTRAINT [DF_LKOfferInterstitialZone_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_LKOfferInterstitialZone_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LKOfferInterstitialZone] PRIMARY KEY CLUSTERED ([OfferID] ASC, [InterstitialZoneID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LKOfferInterstitialZone_InterstitialZoneID] FOREIGN KEY ([InterstitialZoneID]) REFERENCES [dbo].[InterstitialZoneInfo] ([ZoneID]),
    CONSTRAINT [FK_LKOfferInterstitialZone_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID]) ON DELETE CASCADE ON UPDATE CASCADE
);

