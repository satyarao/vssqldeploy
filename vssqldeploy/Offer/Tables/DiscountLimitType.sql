﻿CREATE TABLE [Offer].[DiscountLimitType] (
    [DiscountLimitType] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DiscountLimitType] PRIMARY KEY CLUSTERED ([DiscountLimitType] ASC) WITH (IGNORE_DUP_KEY = ON)
);

