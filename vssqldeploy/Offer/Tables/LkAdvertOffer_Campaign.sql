﻿CREATE TABLE [Offer].[LkAdvertOffer_Campaign] (
    [AdvertOfferID] UNIQUEIDENTIFIER NOT NULL,
    [CampaignID]    INT              NOT NULL,
    [IsActive]      BIT              CONSTRAINT [DF_LkAdvertOfferCampaign_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME2 (7)    CONSTRAINT [DF_LkAdvertOfferCampaign_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkAdvertOfferCampaign] PRIMARY KEY CLUSTERED ([AdvertOfferID] ASC, [CampaignID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkAdvertOfferCampaign_AdvertOfferID] FOREIGN KEY ([AdvertOfferID]) REFERENCES [Offer].[AdvertOffer] ([AdvertOfferID]) ON DELETE CASCADE ON UPDATE CASCADE
);

