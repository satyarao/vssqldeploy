﻿CREATE TABLE [Offer].[LkOfferTemplate_RuleFilter] (
    [OfferTemplateID] UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID]    UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              CONSTRAINT [DF_LkOfferTemplateRuleFilter_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    CONSTRAINT [DF_LkOfferTemplateRuleFilter_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkOfferTemplateRuleFilter] PRIMARY KEY CLUSTERED ([OfferTemplateID] ASC, [RuleFilterID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOfferTemplateRuleFilter_OfferID] FOREIGN KEY ([OfferTemplateID]) REFERENCES [Offer].[OfferTemplate] ([OfferTemplateID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkOfferTemplateRuleFilter_RuleFilterID] FOREIGN KEY ([RuleFilterID]) REFERENCES [dbo].[RuleFilter] ([RuleFilterID]) ON DELETE CASCADE ON UPDATE CASCADE
);

