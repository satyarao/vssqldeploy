﻿CREATE TABLE [Offer].[LkAdvertOffer_ProductDiscount] (
    [AdvertOfferID]     UNIQUEIDENTIFIER NOT NULL,
    [ProductDiscountID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]          BIT              CONSTRAINT [DF_LkAdvertOfferProductDiscount_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_LkAdvertOfferProductDiscount_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_LkAdvertOfferProductDiscount] PRIMARY KEY CLUSTERED ([AdvertOfferID] ASC, [ProductDiscountID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkAdvertOfferProductDiscount_OfferID] FOREIGN KEY ([AdvertOfferID]) REFERENCES [Offer].[AdvertOffer] ([AdvertOfferID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LkAdvertOfferProductDiscount_ProductDiscountID] FOREIGN KEY ([ProductDiscountID]) REFERENCES [Offer].[ProductDiscount] ([ProductDiscountID]) ON DELETE CASCADE ON UPDATE CASCADE
);

