﻿CREATE TYPE [Offer].[StoreOfferMatchingType] AS TABLE (
    [OfferID]          UNIQUEIDENTIFIER NOT NULL,
    [StoreID]          UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID]     UNIQUEIDENTIFIER NOT NULL,
    [NeedsConfirmed]   BIT              NOT NULL,
    [EvaluationResult] BIT              NULL);

