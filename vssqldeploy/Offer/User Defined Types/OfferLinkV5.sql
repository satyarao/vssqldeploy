﻿CREATE TYPE [Offer].[OfferLinkV5] AS TABLE (
    [OfferID]  UNIQUEIDENTIFIER NULL,
    [LinkID]   UNIQUEIDENTIFIER NULL,
    [LinkText] NVARCHAR (255)   NULL,
    [LinkType] NVARCHAR (255)   NULL,
    [Url]      NVARCHAR (255)   NULL);

