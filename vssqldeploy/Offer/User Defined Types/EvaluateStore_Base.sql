﻿CREATE TYPE [Offer].[EvaluateStore_Base] AS TABLE (
    [OfferID]      UNIQUEIDENTIFIER NOT NULL,
    [StoreID]      UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID] UNIQUEIDENTIFIER NOT NULL,
    [ZipCode]      NVARCHAR (16)    NULL,
    [StateCode]    CHAR (2)         NULL,
    [City]         NVARCHAR (54)    NULL,
    [MppaID]       NVARCHAR (9)     NULL);

