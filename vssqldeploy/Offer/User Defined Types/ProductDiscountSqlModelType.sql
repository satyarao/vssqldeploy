﻿CREATE TYPE [Offer].[ProductDiscountSqlModelType] AS TABLE (
    [OfferID]                UNIQUEIDENTIFIER NOT NULL,
    [ProductDiscountID]      UNIQUEIDENTIFIER NOT NULL,
    [ProductRewardID]        UNIQUEIDENTIFIER NULL,
    [StandardProductGroupID] UNIQUEIDENTIFIER NOT NULL,
    [DiscountType]           NVARCHAR (50)    NULL,
    [DiscountLimitType]      NVARCHAR (20)    NOT NULL,
    [DiscountValue]          DECIMAL (7, 3)   NOT NULL,
    [DiscountCurrency]       NVARCHAR (50)    NOT NULL,
    [NewUnitPrice]           DECIMAL (7, 3)   NULL,
    [MinimumQuantity]        DECIMAL (6, 3)   NULL,
    [MaximumQuantity]        DECIMAL (6, 3)   NULL,
    [IsActive]               BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]              DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

