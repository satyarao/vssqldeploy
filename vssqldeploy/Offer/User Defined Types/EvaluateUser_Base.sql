﻿CREATE TYPE [Offer].[EvaluateUser_Base] AS TABLE (
    [OfferID]      UNIQUEIDENTIFIER NOT NULL,
    [UserID]       UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID] UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]    DATETIME2 (7)    NULL);

