﻿CREATE TYPE [Offer].[OfferModelV4ProductRewardType] AS TABLE (
    [OfferID]         UNIQUEIDENTIFIER NOT NULL,
    [ProductRewardID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

