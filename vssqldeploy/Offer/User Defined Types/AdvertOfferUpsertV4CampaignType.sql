﻿CREATE TYPE [Offer].[AdvertOfferUpsertV4CampaignType] AS TABLE (
    [AdvertOfferID] UNIQUEIDENTIFIER NOT NULL,
    [CampaignID]    INT              NOT NULL,
    [IsActive]      BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

