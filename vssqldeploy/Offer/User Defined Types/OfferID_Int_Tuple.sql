﻿CREATE TYPE [Offer].[OfferID_Int_Tuple] AS TABLE (
    [OfferID]  UNIQUEIDENTIFIER NULL,
    [IntValue] INT              NULL);

