﻿CREATE TYPE [Offer].[MaxRedemptionSqlModelType] AS TABLE (
    [OfferID]             UNIQUEIDENTIFIER NOT NULL,
    [MaxRedemptionID]     UNIQUEIDENTIFIER NOT NULL,
    [RedemptionType]      NVARCHAR (10)    NOT NULL,
    [MaxRedemptions]      DECIMAL (10, 3)  NOT NULL,
    [RollingWindowDays]   TINYINT          NOT NULL,
    [RollingWindowWeeks]  TINYINT          NOT NULL,
    [RollingWindowMonths] TINYINT          NOT NULL,
    [RollingWindowYears]  TINYINT          NOT NULL,
    [IsActive]            BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]           DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

