﻿CREATE TYPE [Offer].[OfferModelV4AppTenantType] AS TABLE (
    [OfferID]     UNIQUEIDENTIFIER NOT NULL,
    [AppTenantID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]    BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

