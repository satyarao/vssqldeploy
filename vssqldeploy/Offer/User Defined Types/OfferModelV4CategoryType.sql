﻿CREATE TYPE [Offer].[OfferModelV4CategoryType] AS TABLE (
    [OfferID]         UNIQUEIDENTIFIER NOT NULL,
    [OfferCategoryID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

