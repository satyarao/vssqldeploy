﻿CREATE TYPE [Offer].[OfferModelV4InterstitialZoneType] AS TABLE (
    [OfferID]            UNIQUEIDENTIFIER NOT NULL,
    [InterstitialZoneID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]           BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]          DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

