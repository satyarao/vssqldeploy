﻿CREATE TYPE [Offer].[AdvertOfferUpsertV4CategoryType] AS TABLE (
    [AdvertOfferID]   UNIQUEIDENTIFIER NOT NULL,
    [OfferCategoryID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

