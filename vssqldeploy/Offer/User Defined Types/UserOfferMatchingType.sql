﻿CREATE TYPE [Offer].[UserOfferMatchingType] AS TABLE (
    [OfferID]          UNIQUEIDENTIFIER NOT NULL,
    [UserID]           UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID]     UNIQUEIDENTIFIER NOT NULL,
    [NeedsConfirmed]   BIT              NOT NULL,
    [EvaluationResult] BIT              NULL);

