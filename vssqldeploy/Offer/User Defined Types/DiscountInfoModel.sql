﻿CREATE TYPE [Offer].[DiscountInfoModel] AS TABLE (
    [discountAmount]     DECIMAL (7, 3) NULL,
    [productCode]        NVARCHAR (50)  NULL,
    [productCodeType]    NVARCHAR (50)  NULL,
    [discountAmountType] NVARCHAR (50)  NULL);

