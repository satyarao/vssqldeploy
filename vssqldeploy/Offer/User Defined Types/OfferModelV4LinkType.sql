﻿CREATE TYPE [Offer].[OfferModelV4LinkType] AS TABLE (
    [OfferID]   UNIQUEIDENTIFIER NOT NULL,
    [LinkID]    UNIQUEIDENTIFIER NOT NULL,
    [LinkText]  NVARCHAR (255)   NULL,
    [LinkType]  NVARCHAR (255)   NOT NULL,
    [Url]       NVARCHAR (255)   NULL,
    [IsActive]  BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn] DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

