﻿CREATE TYPE [Offer].[OfferModelV4RuleFilterType] AS TABLE (
    [OfferID]      UNIQUEIDENTIFIER NOT NULL,
    [RuleFilterID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]     BIT              DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME2 (7)    DEFAULT (getutcdate()) NOT NULL);

