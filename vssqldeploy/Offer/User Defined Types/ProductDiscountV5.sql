﻿CREATE TYPE [Offer].[ProductDiscountV5] AS TABLE (
    [OfferID]                UNIQUEIDENTIFIER NULL,
    [ProductDiscountID]      UNIQUEIDENTIFIER NULL,
    [StandardProductGroupID] UNIQUEIDENTIFIER NULL,
    [DiscountType]           NVARCHAR (50)    NULL,
    [DiscountValue]          DECIMAL (7, 3)   NULL,
    [DiscountCurrency]       NVARCHAR (50)    NULL,
    [NewUnitPrice]           DECIMAL (7, 3)   NULL,
    [CouponPosId]            NVARCHAR (20)    NULL,
    [IsStackable]            BIT              NULL,
    [DiscountLimitType]      NVARCHAR (20)    NULL,
    [MinimumQuantity]        DECIMAL (6, 3)   NULL,
    [MaximumQuantity]        DECIMAL (6, 3)   NULL);

