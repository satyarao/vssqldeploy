﻿
CREATE   VIEW [Offer].[RF_StoreZipCode] WITH SCHEMABINDING
AS

	SELECT	RF.[RuleFilterID],
			items.[ZipCode]
	FROM	(
				SELECT	[RuleFilterID],
						[ListString]
				FROM	[Offer].[RuleFilterV2] WITH(NOLOCK)
				WHERE	[RuleFilterTypeID] = 23
			) rf
	CROSS APPLY (
		SELECT	idx.value AS [ZipCode]
		FROM	OPENJSON ((RF.[ListString]), 'lax $') main 
		CROSS APPLY OPENJSON(main.value) idx
		WHERE	ISJSON(RF.[ListString]) > 0
	) items

