﻿
CREATE   VIEW [Offer].[RF_PaymentMethod] WITH SCHEMABINDING
AS

	SELECT	RF.[RuleFilterID], 
			items.[FundingProviderId], 
			items.[FundingProviderName],
			items.[PaymentProcessorId],
			items.[PaymentProcessorName],
			items.[CardIssuer],
			items.[CardType]
	FROM	(
				SELECT	[RuleFilterID],
						[PaymentMethod]
				FROM	[Offer].[RuleFilterV2] WITH(NOLOCK)
				WHERE	[RuleFilterTypeID] = 13
			) rf
	OUTER APPLY 
	(
		SELECT	JSON_VALUE (c.value, '$.FundingProviderId') AS [FundingProviderId], 
				JSON_VALUE (c.value, '$.FundingProviderName') AS [FundingProviderName], 
				JSON_VALUE (p.value, '$.PaymentProcessorId') AS [PaymentProcessorId],
				JSON_VALUE (p.value, '$.PaymentProcessorName') AS [PaymentProcessorName],
				JSON_VALUE (p.value, '$.CardIssuer') AS [CardIssuer],
				JSON_VALUE (p.value, '$.CardType') AS [CardType]
		FROM	OPENJSON (RF.[PaymentMethod], '$') AS c
		CROSS APPLY OPENJSON (c.value, '$.PaymentProcessors') AS p
		WHERE	ISJSON(RF.[PaymentMethod]) > 0
	) items

