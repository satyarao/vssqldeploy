﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   VIEW [Offer].[vGET_OfferView]
WITH SCHEMABINDING
AS
SELECT
    ofr.[OfferID],
    ofr.[TenantID],
    [Name],
    [OfferType],
    [Title],
    [Subtitle],
    [Description],
    [EditorTitle],
    [EditorDescription],
    [TargetUsersType] = ISNULL(ofr.[TargetUsersType], 'authorized'),
    [ImageURL],
    [ImagePos],
    [StartDate],
    [EndDate],
    [Order],
    [MaxRedemptionAmount]	= [Offer].[GetOfferMaxRedemptionAmount]( ofr.[OfferID] ),
    [MaxRedemptions] 		= [Offer].[GetUserMaxRedemption]( ofr.[OfferID] ),
    [RuleFilters] 			= JSON_QUERY([Offer].[GetOfferRuleFilters]( ofr.[OfferID] )),
    [ProductRewards] 		= JSON_QUERY([Offer].[GetOfferProductRewards]( ofr.[OfferID], ofr.[OfferType] )),
    [Links] = (
                SELECT      lnk.[LinkText]  AS [linkText],
                            lnk.[LinkType]  AS [linkType],
                            lnk.[Url]       AS [url]
                FROM        [Offer].[Link] lnk WITH(NOLOCK)
                INNER JOIN  [Offer].[LkOffer_Link] lnklk WITH(NOLOCK)
                ON          lnk.[LinkID] = lnklk.[LinkID]
                WHERE       lnklk.[OfferID] = ofr.[OfferID]
                FOR JSON PATH
            ),
   [IsFeaturedOffer],
   ofr.[IsActive],
   [IsBrandedOffer],
   [Display],
   [IsDisplay],
   [RedemptionsCount] = (
                SELECT  COUNT([TxnLineItemDiscountID])
                FROM    [Txn].[TxnLineItemDiscount] tlid WITH(NOLOCK)
                WHERE   tlid.[OfferID] = ofr.[OfferID] AND [RewardApplied] = 1
            ),
            --offerStatuses
    [IntestitialZones] = (
                SELECT      JSON_QUERY('[' + STRING_AGG('"' + LOWER(CONVERT(nvarchar(36),[InterstitialZoneID])) +  '"', ',') + ']')
                FROM        [ContentPush].[InterstitialZone] WITH(NOLOCK)
                WHERE       [OfferID] = ofr.OfferID
                GROUP BY    [OfferID]
            ),
    [IsDraft],
    [IsRestricted],
    [IsDeleted],
    [Categories] = (
                SELECT      JSON_QUERY('[' + STRING_AGG('"' + LOWER(CONVERT(nvarchar(36),[OfferCategoryID])) +  '"', ',') + ']')
                FROM        [Offer].[LkOffer_Category] WITH(NOLOCK)
                WHERE       [OfferID] = ofr.OfferID
                GROUP BY    [OfferID]
            ),
    [IsNonDisplay] = CASE
                WHEN ofr.[IsDisplay] = 1
                    THEN 0
                WHEN ofr.[IsDisplay] = 0
                    THEN 1
                ELSE NULL
            END,
            --canBeDeleted
    [Campaigns] = (
                SELECT      JSON_QUERY('[' + STRING_AGG('"' + LOWER(CONVERT(nvarchar(36),[CampaignID])) +  '"', ',') + ']')
                FROM        [Offer].[LkOffer_Campaign] WITH(NOLOCK)
                WHERE       [OfferID] = ofr.OfferID
                GROUP BY    [OfferID]
            ),
    [MessagesCount] = ISNULL((
                SELECT  COUNT([MessageID])
                FROM    [ContentPush].[Message] msg WITH(NOLOCK)
                WHERE   msg.[OfferID] = ofr.[OfferID] AND
                        msg.[IsDeleted] = 0
            ), 0),
    [InterstitialZonesCount] = ISNULL((
                SELECT  COUNT([InterstitialZoneID])
                FROM    [ContentPush].[InterstitialZone] msg WITH(NOLOCK)
                WHERE   msg.[OfferID] = ofr.[OfferID] AND
                        msg.[IsDeleted] = 0
            ), 0),
    [AppTenants] = JSON_QUERY( [Offer].[GetOfferAppTenants]( ofr.OfferID ) ),
    [IsDisplayTransactionProgress],
    [BarCode],
    [BarCodeType],
    [PunchCardDisplayType],
    [MaxPunchAmount],
    [SitesCoveredCount] 	= [Offer].[GetSitesCoveredCount]( ofr.[OfferID] ),
    [AudienceCoveredCount] 	= [Offer].[GetAudienceCoveredCount]( ofr.[OfferID] ),
    ofr.[CreatedOn]
FROM
    [Offer].[Offer] ofr WITH(NOLOCK)
    INNER JOIN  [dbo].[Tenant] tnt WITH(NOLOCK) ON tnt.[TenantID] = ofr.[TenantID]

