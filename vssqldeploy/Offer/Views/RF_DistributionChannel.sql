﻿
CREATE   VIEW [Offer].[RF_DistributionChannel] WITH SCHEMABINDING
AS

	SELECT	RF.[RuleFilterID], 
			TRY_CONVERT(uniqueidentifier, [AppTenantID]) AS [AppTenantID]
	FROM	(
				SELECT	[RuleFilterID],
						[ListGuid]
				FROM	[Offer].[RuleFilterV2] WITH(NOLOCK)
				WHERE	[RuleFilterTypeID] = 21
			) rf
	CROSS APPLY (
		SELECT	idx.value AS [AppTenantID]
		FROM	OPENJSON (RF.[ListGuid]) main 
		CROSS APPLY OPENJSON(main.value) idx
		WHERE	ISJSON(RF.[ListGuid]) > 0
	) items

