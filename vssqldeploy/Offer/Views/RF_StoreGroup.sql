﻿

CREATE   VIEW [Offer].[RF_StoreGroup] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID],
			TRY_CONVERT(INT, [NumberOf]) AS StoreGroupID
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 10

