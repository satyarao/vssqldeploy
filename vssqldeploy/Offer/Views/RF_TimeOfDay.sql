﻿

CREATE   VIEW [Offer].[RF_TimeOfDay] WITH SCHEMABINDING
AS
	SELECT	[RuleFilterID], 
			[Bool]			AS [IsLocalTime],
			[StartTime]		AS [TimeOfDayStart],
			[EndTime]		AS [TimeOfDayEnd]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 2

