﻿
CREATE   VIEW [Offer].[RF_ExactStore] WITH SCHEMABINDING
AS

	SELECT	rf.[RuleFilterID], 
			items.[MppaId]		AS [MppaId]
	FROM	(
				SELECT	[RuleFilterID],
						[ListString]
				FROM	[Offer].[RuleFilterV2] WITH(NOLOCK)
				WHERE	[RuleFilterTypeID] = 9
			) rf
	CROSS APPLY (
		SELECT	idx.value AS [MppaId]
		FROM	OPENJSON (RF.[ListString]) main 
		CROSS APPLY OPENJSON(main.value) idx
		WHERE	ISJSON(RF.[ListString]) > 0
	) items

