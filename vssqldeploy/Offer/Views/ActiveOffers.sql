﻿CREATE   VIEW [Offer].[ActiveOffers] 
AS
	SELECT * 
	FROM [Offer].[Offer]
	WHERE [StartDate] < GETUTCDATE() 
		AND ([EndDate] IS NULL OR [EndDate] > GETUTCDATE()) 
		AND [IsDeleted] = 0 
		AND [IsActive] = 1 
		AND [IsDraft] = 0
