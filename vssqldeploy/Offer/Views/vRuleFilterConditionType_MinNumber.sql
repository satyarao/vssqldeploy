﻿
CREATE   VIEW [Offer].[vRuleFilterConditionType_MinNumber] WITH SCHEMABINDING
AS
SELECT   [ConditionID]
		,[RuleFilterID]
		,[Name]
		,[Description]
		,[RuleFilterConditionTypeID]
		,[Value]
		,[Discriminator] 
FROM [dbo].[RuleFilterCondition] 
WHERE [RuleFilterConditionTypeID] = 7
