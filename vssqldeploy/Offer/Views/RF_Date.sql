﻿
CREATE   VIEW [Offer].[RF_Date] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID], 
			[StartDate]		AS [StartDate],
			[EndDate]		AS [EndDate]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 3

