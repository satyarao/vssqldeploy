﻿
CREATE   VIEW [Offer].[RF_VisitCountToStore] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID], 
			[Bool]			AS [IsSameStoreOnly],
			[NumberOf]		AS [NumberOfTransactions],
			[StartDate]		AS [TransactionStartDate]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 8

