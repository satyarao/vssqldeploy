﻿
CREATE   VIEW [Offer].[RF_StoreCityState] 
WITH SCHEMABINDING
AS

	SELECT	RF.[RuleFilterID], 
			items.[CityName], 
			items.[StateName]
	FROM	(
				SELECT	[RuleFilterID],
						[CityStateValue]
				FROM	[Offer].[RuleFilterV2] WITH(NOLOCK)
				WHERE	[RuleFilterTypeID] = 25
			) rf
	CROSS APPLY (
		SELECT	d.[CityName],
				d.[StateName]
		FROM	OPENJSON ((SELECT RF.[CityStateValue]))
				WITH
				(
					[CityName]	sys.nvarchar(20) '$.CityName',
					[StateName] sys.nvarchar(20) '$.StateName'
				) d
		WHERE	ISJSON(RF.[CityStateValue]) > 0
	) items