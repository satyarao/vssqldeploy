﻿
CREATE   VIEW [Offer].[vGET_AdvertOffers]
AS
SELECT
      AdvOFR.AdvertOfferID
    , OfferTemplateID
    , AdvOFR.TenantID
    , TenantName = Tenant.[Name]
    , OfferType
    , RewardType
    , TargetUsersType
    , EditorTitle
    , EditorDescription
    , Title
    , Subtitle
    , [Description]
    , ImageURL
    , ImagePos
    , [Order]
    , Display
    , StartDate
    , EndDate
    , IsFeaturedOffer
    , IsDeleted
    , AdvOFR.IsActive
    , IsDraft
    , IsRestricted
    , IsDisplay
    , IsBrandedOffer
    , RedemptionsCount = COALESCE((SELECT SUM([MaxRedemptions]) FROM [Offer].[MaxRedemption] WHERE [RedemptionType] = 'user' AND [MaxRedemptionID] IN
                            (SELECT [MaxRedemptionID] FROM [Offer].[LkOffer_MaxRedemption] WHERE [OfferID] IN
                            (SELECT [OfferID] FROM [offer].[Offer] OO WHERE OO.AdvertOfferID = AdvOFR.AdvertOfferID))),0)
    , DiscountValue = (SELECT [DiscountValue] FROM [Offer].[ProductDiscount] PD
                WHERE PD.[ProductDiscountID] IN (SELECT [ProductDiscountID] FROM [Offer].[LkAdvertOffer_ProductDiscount] WHERE [AdvertOfferID] = AdvOFR.AdvertOfferID
                    AND DiscountValue = [DiscountValue]))
    , [Links] = (
            SELECT
                [LinkText]  AS [linkText],
                [LinkType]  AS [linkType],
                [Url]       AS [url]
            FROM    [Offer].[Link]
            WHERE   [IsActive] = 1 AND [LinkID] IN (SELECT [LinkID] FROM [Offer].[LkAdvertOffer_Link] WHERE [AdvertOfferID] = AdvOFR.AdvertOfferID)
            ORDER BY [LinkID]
            FOR JSON PATH, INCLUDE_NULL_VALUES
    )
    , [Categories] = (
        -- Following "weird" query returns JSON array of GUIDs (OfferCategoryID)
        SELECT JSON_QUERY('[' + STUFF((
            SELECT ',"' + LOWER(TRY_CONVERT(nvarchar(50), [OfferCategoryID])) + '"'
            FROM [Offer].[LkAdvertOffer_Category]
            WHERE [IsActive] = 1 AND [AdvertOfferID] = AdvOFR.AdvertOfferID
            ORDER BY [OfferCategoryID]
            FOR XML PATH('')
        ),1,1,'') + ']')
    )
    , [RuleFilters] = (
        -- Following "weird" query returns JSON array of GUIDs (RuleFilterID)
        SELECT JSON_QUERY('[' + STUFF((
            SELECT ',"' + LOWER(TRY_CONVERT(nvarchar(50), [RuleFilterID])) + '"'
            FROM [Offer].[LkAdvertOffer_RuleFilter]
            WHERE [IsActive] = 1 AND [AdvertOfferID] = AdvOFR.AdvertOfferID
            ORDER BY [RuleFilterID]
            FOR XML PATH('')
        ),1,1,'') + ']')
    )
    , [Campaigns] = (
        -- Following "weird" query returns JSON array of ints (CampaignID)
        SELECT JSON_QUERY('[' + STUFF((
            SELECT ',"' + LOWER(TRY_CONVERT(nvarchar(50), [CampaignID])) + '"'
            FROM [Offer].[LkAdvertOffer_Campaign]
            WHERE [IsActive] = 1 AND [AdvertOfferID] = AdvOFR.AdvertOfferID
            ORDER BY [CampaignID]
            FOR XML PATH('')
        ),1,1,'') + ']')
    )
    , [MaxRedemptions] = (
        SELECT
            --[MaxRedemptionID],
            [RedemptionType],
            [MaxRedemptions],
            [RollingWindowDays] = CASE WHEN [RollingWindowDays] > 0 THEN [RollingWindowDays] ELSE NULL END,
            [RollingWindowWeeks] = CASE WHEN [RollingWindowWeeks] > 0 THEN [RollingWindowWeeks] ELSE NULL END,
            [RollingWindowMonths] = CASE WHEN [RollingWindowMonths] > 0 THEN [RollingWindowMonths] ELSE NULL END,
            [RollingWindowYears] = CASE WHEN [RollingWindowYears] > 0 THEN [RollingWindowYears] ELSE NULL END
        FROM
            [Offer].[MaxRedemption]
        WHERE
            [MaxRedemptionID] IN (
                SELECT Lk.[MaxRedemptionID]
                FROM [Offer].[LkAdvertOffer_MaxRedemption] Lk
                JOIN [Offer].[MaxRedemption] MR ON MR.[MaxRedemptionID] = Lk.[MaxRedemptionID]
                WHERE [AdvertOfferID] = AdvOFR.AdvertOfferID AND MR.IsActive = 1
            )
        FOR JSON PATH
    )
    , ProductDiscounts = (
        SELECT
            PD.[ProductDiscountID]      AS ProductDiscountId
            , [ProductRewardID]         AS ProductRewardId
            , [StandardProductGroupID]  AS StandardProductGroupId
            , [DiscountType]
            , DL.[DiscountLimitType]
            , [DiscountValue]
            , [DiscountCurrency]
            , [NewUnitPrice]
            , DL.[MinimumQuantity]
            , DL.[MaximumQuantity]
        FROM
            [Offer].[ProductDiscount] PD
            LEFT JOIN [Offer].[DiscountLimit] DL ON PD.ProductDiscountID = DL.ProductDiscountID AND DL.IsActive=1
        WHERE
            PD.[IsActive] = 1
            AND PD.[ProductDiscountID] IN (SELECT [ProductDiscountID] FROM [Offer].[LkAdvertOffer_ProductDiscount] WHERE [AdvertOfferID] = AdvOFR.AdvertOfferID)
        ORDER BY [ProductDiscountID]
        FOR JSON PATH
    )
    , ProductCode = (
        SELECT TOP(1) [PosCode]
        FROM        [Product].[StandardProduct] sp
        INNER JOIN  [Product].[LKStandardProductGroup] lkspg
        ON          sp.[StandardProductID] = lkspg.[StandardProductCodeID]
        INNER JOIN  [Product].[StandardProductGroup] spg
        ON          spg.[StandardProductGroupID] = lkspg.[StandardProductGroupID]
        INNER JOIN  [Offer].[ProductDiscount] pd
        ON          pd.[StandardProductGroupID] = spg.[StandardProductGroupID]
        WHERE       pd.[ProductDiscountID] IN (SELECT [ProductDiscountID] FROM [Offer].[LkAdvertOffer_ProductDiscount] WHERE [AdvertOfferID] = AdvOFR.AdvertOfferID)
    )
    , AdvOFR.CreatedOn
    , AdvOFR.CreatedBy
    , AdvOFR.CreatedByUserID
    , AdvOFR.UpdatedOn
    , AdvOFR.UpdatedBy
    , AdvOFR.UpdatedByUserID
FROM
    [Offer].[AdvertOffer] AdvOFR
    JOIN [dbo].[Tenant] Tenant ON Tenant.TenantID = AdvOFR.TenantID

