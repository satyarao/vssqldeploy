﻿
CREATE   VIEW [Offer].[RuleFilterV2] WITH SCHEMABINDING AS

	WITH cityState AS (
		SELECT  [RuleFilterID],
				[Value] = '[' + STRING_AGG([Value], ',') + ']'
		FROM    [dbo].[RuleFilterCondition] t2 WITH(NOLOCK)
		WHERE   [RuleFilterConditionTypeID] = 15
		GROUP BY [RuleFilterID]
	),
	paymentMethod AS (
		SELECT  t2.[RuleFilterID], 
				[Value] = '[' + STRING_AGG(rfcPm.[Value], ',') + ']'
		FROM    [dbo].[RuleFilterCondition] t2 WITH(NOLOCK)
		INNER JOIN [Offer].[vRuleFilterConditionType_PaymentMethod] rfcPm
		ON      t2.[ConditionID] = rfcPm.[ConditionID]
		WHERE   t2.[RuleFilterConditionTypeID] = 14
		GROUP BY t2.[RuleFilterID]
	),
	stringKeyValue AS (
		 SELECT     t2.[RuleFilterID],
					[Value] = '[' + STRING_AGG(rfcSkv.[Value], ',') + ']'
		FROM        [dbo].[RuleFilterCondition] t2 WITH(NOLOCK)
		INNER JOIN [Offer].[vRuleFilterConditionType_StringKeyValue] rfcSkv
		ON      rfcSkv.[ConditionID] = t2.[ConditionID]
		WHERE   t2.[RuleFilterConditionTypeID] = 13
		GROUP BY t2.[RuleFilterID]
	),
	numberOf AS (
	  SELECT [RuleFilterID], [Value]  
	  FROM [Offer].[vRuleFilterConditionType_NumberOf]    
	)

	SELECT 	 RF.[RuleFilterID]
			,RF.[TenantID]
			,RF.[RuleFilterTypeID]
			,RF.[Name]
			,RF.[Description]
			,RF.[CreatedOn]
			,RF.[UpdatedOn]
			,TRY_CONVERT(DATETIME2(7), startDate.[Value]) AS [StartDate]
			,TRY_CONVERT(DATETIME2(7), endDate.[Value]) AS [EndDate]
			,TRY_CONVERT(TIME, startTime.[Value]) AS [StartTime]
			,TRY_CONVERT(TIME, endTime.[Value]) AS [EndTime]
			,[dayOfWeek].[Value] AS [DayOfWeek]
			,CASE
				WHEN LOWER(bool.[Value]) = 'false' 
					THEN 0
				WHEN LOWER(bool.[Value]) = 'true' 
					THEN 1
				ELSE NULL
			 END AS [Bool]
			,minNumber.[Value] AS [MinNumber]
			,maxNumber.[Value] AS [MaxNumber]
			,numberOf.[Value] AS [NumberOf]
			,CASE
				WHEN listInt.[Value] = '[]' 
					THEN NULL
				WHEN listInt.[Value] IS NULL 
					THEN NULL
				ELSE '{"ListInt": ' + listInt.[Value] + '}'
			END AS [ListInt]
			,CASE
				WHEN listString.[Value] = '[]' 
					THEN NULL
				WHEN listString.[Value] IS NULL 
					THEN NULL
				ELSE '{"ListString": ' + listString.[Value] + '}'
			 END [ListString]
			,CASE
				WHEN listGuid.[Value] = '[]' 
					THEN NULL
				WHEN listGuid.[Value] IS NULL 
					THEN NULL
				ELSE '{"ListGuid": ' + listGuid.[Value] + '}'
			 END AS [ListGuid]
			,stringKeyValue.[Value] AS [StringKeyValue]
			,paymentMethod.[Value] AS [PaymentMethod]
			,cityStateValue.[Value] AS [CityStateValue]
	FROM [Offer].[vRuleFilterSubquery_From] RF 
	LEFT JOIN [Offer].[vRuleFilterConditionType_StartDate] startDate
	  ON startDate.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_EndDate] endDate
	  ON endDate.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_StartTime] startTime
	  ON startTime.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_EndTime] endTime
	  ON endTime.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_DayOfWeek] [dayOfWeek]
	  ON [dayOfWeek].[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_Bool] bool
	  ON bool.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_MinNumber] minNumber
	  ON minNumber.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_MaxNumber] maxNumber
	  ON maxNumber.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN numberOf AS numberOf
	  ON numberOf.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_ListInt] listInt
	  ON listInt.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_ListString] listString
	  ON listString.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN [Offer].[vRuleFilterConditionType_ListGuid]  listGuid
	  ON listGuid.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN stringKeyValue AS stringKeyValue
	  ON stringKeyValue.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN paymentMethod AS paymentMethod
	  ON paymentMethod.[RuleFilterID] = RF.[RuleFilterID]
	LEFT JOIN cityState AS cityStateValue
	  ON cityStateValue.[RuleFilterID] = RF.[RuleFilterID]

