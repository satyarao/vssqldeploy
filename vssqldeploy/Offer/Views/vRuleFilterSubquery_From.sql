﻿
CREATE   VIEW [Offer].[vRuleFilterSubquery_From] WITH SCHEMABINDING
AS
SELECT		 [RuleFilterID]
			,[TenantID]
			,[RuleFilterTypeID]
			,[Name]
			,[Description]
			,[CreatedOn]
			,[UpdatedOn]
			,[Discriminator] 
	  FROM [dbo].[RuleFilter]
