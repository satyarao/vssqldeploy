﻿
CREATE   VIEW [Offer].[RF_PreviousPurchase] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID], 
			TRY_CONVERT(INT, [NumberOf])	AS [NumberOfTransactions],
			[StartDate]						AS [TransactionStartDate]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 18

