﻿
--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   VIEW [Offer].[vProductRewardModel]
WITH SCHEMABINDING
AS
	SELECT		 [StartDate] 			= ofr.[StartDate]
				,[EndDate] 				= ofr.[EndDate]
				,[OfferType] 			= ofr.[OfferType]
				,[ProductCode] 			= [Offer].[GetOfferProductCode]( ofr.[OfferID], ofr.[OfferType] )
				,[ProductCodeType] 		= [Offer].[GetOfferProductCodeType]( ofr.[OfferID], ofr.[OfferType] )
				,[MaxRedemptions] 		= [Offer].[GetOfferMaxRedemption]( ofr.OfferID )
				,[MaxUserRedemptions] 	= [Offer].[GetUserMaxRedemption]( ofr.OfferID )
				,[Value] 				= 0
				,[IsActive] 			= ofr.[IsActive]
				,[IsDeleted] 			= ofr.[IsDeleted]
				,[TenantId] 			= ofr.[TenantId]
				,[Id] 					= ofr.[OfferID]
				,[DisplayType] 			= ofr.[Display]
				,[RuleFilters] 			= JSON_QUERY( [Offer].[GetOfferRuleFilters]( ofr.OfferID ) )
				,[AppTenants] 			= JSON_QUERY( [Offer].[GetOfferAppTenants]( ofr.OfferID ) )
				,[DiscountInfo] 		= JSON_QUERY([Offer].[GetProductRewardModelDiscountInfo]( ofr.OfferID, ofr.[TenantId] ))
	FROM		[Offer].[Offer] ofr WITH(NOLOCK)