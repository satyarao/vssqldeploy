﻿
CREATE   VIEW [Offer].[RF_FirstVisitToStore] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 15

