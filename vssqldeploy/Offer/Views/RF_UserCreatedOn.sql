﻿
CREATE   VIEW [Offer].[RF_UserCreatedOn] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID], 
			TRY_CONVERT(INT,[MinNumber])	AS [MinAccountAgeDays],
			TRY_CONVERT(INT,[MaxNumber])	AS [MaxAccountAgeDays]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 5

