﻿
CREATE   VIEW [Offer].[RF_LastVisitDate] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID], 
			TRY_CONVERT(INT,[NumberOf])	AS [MinDaysSinceLastVisit],
			[StartDate]					AS [TransactionStartDate],
			[EndDate]					AS [TransactionEndDate]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 7

