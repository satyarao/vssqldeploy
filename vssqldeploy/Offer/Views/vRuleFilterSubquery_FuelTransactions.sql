﻿
CREATE   VIEW [Offer].[vRuleFilterSubquery_FuelTransactions] WITH SCHEMABINDING
AS
SELECT T.[TxnID] AS TxnID, 
	   T.[UserID] AS UserID,
	   T.[StoreID] AS StoreID, 
	   T.[AppTenantID] AS AppTenantID,
	   TLI.[Quantity] AS Quantity, 
	   TLI.[UnitOfMeasure] AS UnitOfMeasure, 
	   TLI.[ProductCode] AS ProductCode, 
	   TLI.[ProductDescription] AS ProductDescription, 
	   T.[CreatedOn] AS CreatedOn
FROM [Txn].[Txn] T
INNER JOIN Txn.TxnLineItem TLI ON T.TxnID = TLI.TxnID
WHERE LOWER(ProductDescription) = 'fuel'
