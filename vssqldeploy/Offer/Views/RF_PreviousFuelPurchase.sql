﻿
CREATE   VIEW [Offer].[RF_PreviousFuelPurchase] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID],
			[StartDate] AS [TransactionStartDate],
			TRY_CONVERT(INT, [NumberOf]) AS [NumberOfGallons]
	FROM	[Offer].[RuleFilterV2] RF
	WHERE	[RuleFilterTypeID] = 19

