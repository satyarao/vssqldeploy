﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   VIEW [Offer].[vGET_Offers]
WITH SCHEMABINDING
AS
SELECT
    ofr.[OfferID],
    ofr.[AdvertOfferID],
    [OfferTemplateID],
    ofr.[TenantID],
    [TenantName]          = Tenant.[Name],
    [SourceTenantName]    = [Offer].[GetOfferSourcTenantName]( ofr.[AdvertOfferID] ),
    [OfferType],
    [RewardType],
    [TargetUsersType],
    [EditorTitle],
    [EditorDescription],
    [Title],
    [Subtitle],
    [Description],
    [ImageURL],
    [ImagePos],
    [Order],
    [Display],
    [StartDate],
    [EndDate],
    [IsFeaturedOffer],
    [IsDeleted],
    ofr.[IsActive],
    [IsDraft],
    [IsRestricted],
    [IsDisplay],
    [IsBrandedOffer],
    [BarCode],
    [BarCodeType],
    [PunchCardDisplayType],
    [MaxPunchAmount],
    [MaxRedemptionAmount] 	= [Offer].[GetOfferMaxRedemptionAmount]( ofr.[OfferID] ),
    [MaxRedemption]       	= [Offer].[GetUserMaxRedemption]( ofr.[OfferID] ),
    [SitesCoveredCount]		= [Offer].[GetSitesCoveredCount]( ofr.[OfferID] ),
    [AudienceCoveredCount] 	= [Offer].[GetAudienceCoveredCount]( ofr.[OfferID] ),
    [RuleFilters] 			= JSON_QUERY([Offer].[GetOfferRuleFilters]( ofr.[OfferID] )),
    [ProductRewards] = (
                SELECT      JSON_QUERY('[' + STRING_AGG('"' + LOWER(TRY_CONVERT(nvarchar(36),[ProductDiscountID])) +  '"', ',') + ']')
                FROM        [Offer].[LkOffer_ProductDiscount]
                WHERE       [OfferID] = ofr.OfferID
                GROUP BY    [OfferID]
            ),
    [InterstitialZones] = (
                SELECT      JSON_QUERY('[' + STRING_AGG('"' + LOWER(TRY_CONVERT(nvarchar(36),[InterstitialZoneID])) +  '"', ',') + ']')
                FROM        [ContentPush].[InterstitialZone] WITH(NOLOCK)
                WHERE       [OfferID] = ofr.OfferID
                GROUP BY    [OfferID]
            ),
    [Links] = (
                SELECT
                    [LinkText] AS [linkText]
                    , [LinkType] AS [linkType]
                    , [Url] AS [url]
                FROM [Offer].[Link]
                WHERE [IsActive] = 1 AND [LinkID] IN (SELECT [LinkID] FROM [Offer].[LkOffer_Link] WHERE [OfferID] = ofr.OfferID)
                ORDER BY [LinkID]
                FOR JSON PATH, INCLUDE_NULL_VALUES
            ),
    [Categories] = (
                SELECT      JSON_QUERY('[' + STRING_AGG('"' + LOWER(TRY_CONVERT(nvarchar(36),[OfferCategoryID])) +  '"', ',') + ']')
                FROM        [Offer].[LkOffer_Category] WITH(NOLOCK)
                WHERE       [OfferID] = ofr.OfferID
                GROUP BY    [OfferID]
            ),
    [Campaigns] = (
                SELECT      JSON_QUERY('[' + STRING_AGG('"' + LOWER(TRY_CONVERT(nvarchar(36),[CampaignID])) +  '"', ',') + ']')
                FROM        [Offer].[LkOffer_Campaign] WITH(NOLOCK)
                WHERE       [OfferID] = ofr.OfferID
                GROUP BY    [OfferID]
            ),
    [AppTenants] 		= JSON_QUERY( [Offer].[GetOfferAppTenants]( ofr.OfferID ) ),
    [ProductDiscounts] 	= JSON_QUERY([Offer].[GetOfferProductDiscounts]( ofr.[OfferID] )),
    [MaxRedemptions] = (
                SELECT
                    [MaxRedemptions],
                    [RollingWindowDays] = CASE WHEN [RollingWindowDays] > 0 THEN [RollingWindowDays] ELSE NULL END,
                    [RollingWindowWeeks] = CASE WHEN [RollingWindowWeeks] > 0 THEN [RollingWindowWeeks] ELSE NULL END,
                    [RollingWindowMonths] = CASE WHEN [RollingWindowMonths] > 0 THEN [RollingWindowMonths] ELSE NULL END,
                    [RollingWindowYears] = CASE WHEN [RollingWindowYears] > 0 THEN [RollingWindowYears] ELSE NULL END
                FROM
                    [Offer].[MaxRedemption]
                WHERE
                    [MaxRedemptionID] IN (
                        SELECT Lk.[MaxRedemptionID]
                        FROM [Offer].[LkOffer_MaxRedemption] Lk
                        JOIN [Offer].[MaxRedemption] MR ON MR.[MaxRedemptionID] = Lk.[MaxRedemptionID]
                        WHERE [OfferID] = ofr.OfferID AND MR.IsActive = 1
                    )
                FOR JSON PATH
            ),
    ofr.[CreatedOn],
    ofr.[CreatedBy],
    ofr.[CreatedByUserID],
    ofr.[UpdatedOn],
    ofr.[UpdatedBy],
    ofr.[UpdatedByUserID]
FROM
    [Offer].[Offer] ofr
    JOIN [dbo].[Tenant] Tenant ON Tenant.TenantID = ofr.TenantID
