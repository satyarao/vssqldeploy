﻿
CREATE   VIEW [Offer].[RF_TimeDayOfWeek] WITH SCHEMABINDING
AS

	SELECT	rf.[RuleFilterID], 
			rf.[Bool]			AS [IsLocalTime], 
			items.[DayOfWeek]	AS [DaysOfWeek],
			rf.[StartTime]		AS [TimeOfWeekStart],
			rf.[EndTime]		AS [TimeOfWeekEnd]
	FROM	(
				SELECT	[RuleFilterID],
						[Bool],
						[StartTime],
						[EndTime],
						[DayOfWeek]
				FROM	[Offer].[RuleFilterV2] WITH(NOLOCK)
				WHERE	[RuleFilterTypeID] = 1
			) rf
	CROSS APPLY (
		SELECT	main.value AS [DayOfWeek]
		FROM	OPENJSON (RF.[DayOfWeek]) main 
		WHERE	ISJSON(RF.[DayOfWeek]) > 0
	) items

