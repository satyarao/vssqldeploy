﻿
CREATE   VIEW [Offer].[RF_StoreState] WITH SCHEMABINDING
AS

	SELECT	RF.[RuleFilterID],
			items.[State]
	FROM	(
				SELECT	[RuleFilterID],
						[ListString]
				FROM	[Offer].[RuleFilterV2] WITH(NOLOCK)
				WHERE	[RuleFilterTypeID] = 26
			) rf
	CROSS APPLY (
		SELECT	idx.value AS [State]
		FROM	OPENJSON (RF.[ListString]) main 
		CROSS APPLY OPENJSON(main.value) idx
		WHERE	ISJSON(RF.[ListString]) > 0
	) items

