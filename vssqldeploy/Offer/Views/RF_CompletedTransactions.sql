﻿
CREATE   VIEW [Offer].[RF_CompletedTransactions] WITH SCHEMABINDING
AS

	SELECT	[RuleFilterID], 
			[NumberOf]		AS [NumberOfTransactions],
			[StartDate]		AS [CompletedTransactionStartDate]
	FROM	[Offer].[RuleFilterV2]
	WHERE	[RuleFilterTypeID] = 6

