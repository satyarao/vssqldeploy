﻿
--====================================================================
--UpdatedBy Trace
--11/19/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   FUNCTION [Offer].[GetOfferProductRewards]( @OfferId uniqueidentifier, @OfferType nvarchar(50) )
RETURNS nvarchar(max)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		JSON_QUERY((
			SELECT
				CASE
				--Have to handle Fuel Rewards and Product Rewards seperately.
				--Fuel Rewards are granular to Product Group, where Product is granular to Product Code
					WHEN @OfferType = 'Fuel Rewards'
					THEN
					(
						SELECT      pd.[DiscountValue]  AS [discountAmount],
									ISNULL(TRY_CONVERT(int, spg.[GroupCode]), LOWER(TRY_CONVERT(nvarchar(50), pd.[StandardProductGroupID])) )
														AS [productCode],
									'PaymentSystemsProductCode'
														AS [productCodeType],
									[DiscountType]      AS [discountAmountType]
						FROM        [Offer].[ProductDiscount] pd WITH(NOLOCK)
						INNER JOIN  [Offer].[LkOffer_ProductDiscount] lkpd WITH(NOLOCK)
						ON          pd.[ProductDiscountID] = lkpd.[ProductDiscountID]
						LEFT JOIN   [Product].[StandardProductGroup] spg WITH(NOLOCK)
						ON          spg.[StandardProductGroupID] = pd.[StandardProductGroupID]
						WHERE       lkpd.[OfferID] = @OfferId
						FOR JSON PATH
					)
					ELSE
					(
						SELECT      pd.[DiscountValue]  AS [discountAmount],
									CASE
										WHEN spc.[UpcA] IS NOT NULL
											THEN spc.[UpcA]
										WHEN spc.[UpcE] IS NOT NULL
											THEN spc.[UpcE]
										WHEN spc.[Ean8] IS NOT NULL
											THEN spc.[Ean8]
										WHEN spc.[PosCode] IS NOT NULL
											THEN spc.[PosCode]
										WHEN spc.[PaymentSystemProductCode] IS NOT NULL
											THEN spc.[PaymentSystemProductCode]
									END
														AS [productCode],
									spc.[ProductCodeType]
														AS [productCodeType],
									[DiscountType]      AS [discountAmountType]
						FROM        [Offer].[ProductDiscount] pd WITH(NOLOCK)
						INNER JOIN  [Offer].[LkOffer_ProductDiscount] lkpd WITH(NOLOCK)
						ON          pd.[ProductDiscountID] = lkpd.[ProductDiscountID]
						LEFT JOIN   [Product].[StandardProductGroup] spg WITH(NOLOCK)
						ON          spg.[StandardProductGroupID] = pd.[StandardProductGroupID]
						LEFT JOIN   [Product].[LKStandardProductGroup] lkspg WITH(NOLOCK)
						ON          spg.[StandardProductGroupID] = lkspg.[StandardProductGroupID]
						LEFT JOIN   [Product].[StandardProduct] spc WITH(NOLOCK)
						ON          spc.[StandardProductID] = lkspg.[StandardProductCodeID]
						WHERE       lkpd.[OfferID] = @OfferId
						FOR JSON PATH
					)
				END
		))
	)
END

