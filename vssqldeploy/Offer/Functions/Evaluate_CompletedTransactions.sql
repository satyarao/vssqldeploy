﻿
CREATE   FUNCTION [Offer].[Evaluate_CompletedTransactions] ( @EvaluationUserBase [Offer].[EvaluateUser_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		eub.[OfferID],
					eub.[UserID],
					RF.[RuleFilterID],
					CASE
						WHEN ISNULL(RF.[NumberOfTransactions], 0) <= [dbo].[UserTransactionCount]( eub.[UserID], RF.[CompletedTransactionStartDate] )
							 THEN 1
						ELSE 0
					END AS [EvaluationResult]
		FROM		@EvaluationUserBase eub
		INNER JOIN	[Offer].[RF_CompletedTransactions] RF WITH(NOLOCK)
		ON			RF.[RuleFilterID] = eub.[RuleFilterID]
