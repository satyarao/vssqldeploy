﻿

CREATE   FUNCTION [Offer].[EvaluateUser_BaseQuery] ( @OfferId uniqueidentifier = NULL, @UserId uniqueidentifier = NULL )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		ofr.[OfferID],
					usr.[UserID],
					lkRF.[RuleFilterID],
					usr.[CreatedOn]
		FROM		[Offer].[LkOffer_Tenant]( @OfferId, [dbo].[GetUsersTenant]( @UserId ) ) ofr
		INNER JOIN	[Offer].[LkOffer_RuleFilter] lkRF WITH(NOLOCK)
		ON			lkRF.[OfferID] = ofr.[OfferID]
		INNER JOIN	[dbo].[UserInfo] usr WITH(NOLOCK)
		ON	usr.[TenantID] = ofr.[TenantID]
		WHERE usr.[UserID] = ISNULL(@UserId, usr.[UserID])
