﻿

CREATE   FUNCTION [Offer].[OffersWithoutStoreFilters] ( @OfferID uniqueidentifier )
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GET Offers that either don't have any rule filters, or don't contain store rule filters
	RETURN
		SELECT	[OfferID],
				[TenantID]
		FROM	(
					SELECT		[OfferID],
								[TenantID],
								SUM([IsStoreRuleFilter]) AS [NumStoreRuleFilters]
					FROM		(
									SELECT		ofr.[OfferID],
												ofr.[TenantID],
												lkRF.[RuleFilterID],
												CASE
													WHEN	rfes.[RuleFilterID] IS NULL AND
															rfscs.[RuleFilterID] IS NULL AND
															rfss.[RuleFilterID] IS NULL AND
															rfsz.[RuleFilterID] IS NULL
														THEN 0
													ELSE 1
												END AS [IsStoreRuleFilter]
									FROM		[Offer].[Offer] ofr WITH(NOLOCK)
									INNER JOIN	[Offer].[LkOffer_RuleFilter] lkRF WITH(NOLOCK)
									ON			lkRF.[OfferID] = ofr.[OfferID]
									--Does Offer have any Store Rule Filters?
									LEFT JOIN	[Offer].[RF_ExactStore] rfes WITH(NOLOCK)
									ON			rfes.[RuleFilterID] = lkRF.[RuleFilterID]
									LEFT JOIN	[Offer].[RF_StoreCityState] rfscs WITH(NOLOCK)
									ON			rfscs.[RuleFilterID] = lkRF.[RuleFilterID]
									LEFT JOIN	[Offer].[RF_StoreState] rfss WITH(NOLOCK)
									ON			rfss.[RuleFilterID] = lkRF.[RuleFilterID]
									LEFT JOIN	[Offer].[RF_StoreZipcode] rfsz WITH(NOLOCK)
									ON			rfsz.[RuleFilterID] = lkRF.[RuleFilterID]
									WHERE		ofr.[OfferID] = ISNULL(@OfferID, ofr.[OfferID])
								) tbl

					GROUP BY	[TenantID], [OfferID]
				) otrtbl

		WHERE	[NumStoreRuleFilters] = 0

		UNION

		SELECT	[OfferID],
				[TenantID]
		FROM	[Offer].[OffersWithoutRuleFilters]( @OfferID )