﻿
CREATE   FUNCTION [Offer].[GetAudienceCoveredCount]( @OfferId uniqueidentifier )
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		ISNULL
		(
			(
                SELECT	COUNT([UserID])
                FROM	[Offer].[UserOfferMatching] som WITH(NOLOCK)
                WHERE	som.[OfferID] = @OfferId
            ), 
			0
		)
	)
END
