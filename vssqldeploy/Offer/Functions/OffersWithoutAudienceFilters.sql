﻿
CREATE   FUNCTION [Offer].[OffersWithoutAudienceFilters] ( @OfferID uniqueidentifier )
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GET Offers that either don't have any rule filters, or don't contain store rule filters
	RETURN
		SELECT	[OfferID],
				[TenantID]
		FROM	(
					SELECT		[OfferID],
								[TenantID],
								SUM([IsStoreRuleFilter]) AS [NumStoreRuleFilters]
					FROM		(
									SELECT		ofr.[OfferID],
												ofr.[TenantID],
												lkRF.[RuleFilterID],
												CASE
													WHEN	rfnu.[RuleFilterID] IS NULL AND
															rfuco.[RuleFilterID] IS NULL AND
															rfct.[RuleFilterID] IS NULL AND
															rflvd.[RuleFilterID] IS NULL AND
															rfvtsa.[RuleFilterID] IS NULL
														THEN 0
													ELSE 1
												END AS [IsStoreRuleFilter]
									FROM		[Offer].[Offer] ofr WITH(NOLOCK)
									INNER JOIN	[Offer].[LkOffer_RuleFilter] lkRF WITH(NOLOCK)
									ON			lkRF.[OfferID] = ofr.[OfferID]
									--Does Offer have any Store Rule Filters?
									LEFT JOIN	[Offer].[RF_NewUser] rfnu WITH(NOLOCK)
									ON			rfnu.[RuleFilterID] = lkRF.[RuleFilterID]
									LEFT JOIN	[Offer].[RF_UserCreatedOn] rfuco WITH(NOLOCK)
									ON			rfuco.[RuleFilterID] = lkRF.[RuleFilterID]
									LEFT JOIN	[Offer].[RF_CompletedTransactions] rfct WITH(NOLOCK)
									ON			rfct.[RuleFilterID] = lkRF.[RuleFilterID]
									LEFT JOIN	[Offer].[RF_LastVisitDate] rflvd WITH(NOLOCK)
									ON			rflvd.[RuleFilterID] = lkRF.[RuleFilterID]
									LEFT JOIN	[Offer].[RF_VisitCountToStoreActivity] rfvtsa WITH(NOLOCK)
									ON			rfvtsa.[RuleFilterID] = lkRF.[RuleFilterID]
									WHERE		ofr.[OfferID] = ISNULL(@OfferID, ofr.[OfferID])
								) tbl

					GROUP BY	[TenantID], [OfferID]
				) otrtbl
		WHERE	[NumStoreRuleFilters] = 0

		UNION

		SELECT	[OfferID],
				[TenantID]
		FROM	[Offer].[OffersWithoutRuleFilters]( @OfferID )
