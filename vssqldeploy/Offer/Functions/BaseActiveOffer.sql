﻿
CREATE   FUNCTION [Offer].[BaseActiveOffer] ( @TenantId uniqueidentifier, @OfferId uniqueidentifier )
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GETs Offers that are globally active
	RETURN
		SELECT	 [OfferID]
				,[TenantID]
				,[TargetUsersType]
		FROM	[Offer].[Offer] WITH(NOLOCK)
		WHERE	[StartDate] < GETUTCDATE() AND 
				([EndDate] IS NULL OR [EndDate] > GETUTCDATE())  AND 
				[IsDeleted] = 0 AND 
				[IsActive] = 1 AND 
				[IsDraft] = 0 AND
				--[TenantID] = ISNULL(@TenantId, [TenantID]) AND
				[OfferID] = ISNULL(@OfferId, [OfferID])
