﻿
CREATE   FUNCTION [Offer].[GetOfferMaxRedemptionAmount]( @OfferId uniqueidentifier )
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		ISNULL
		(
			(
                SELECT      TOP(1)  dl.[MaximumQuantity]
                FROM        [Offer].[DiscountLimit] dl WITH(NOLOCK)
                INNER JOIN  [Offer].[ProductDiscount] pd WITH(NOLOCK)
                ON          dl.[ProductDiscountID] = pd.[ProductDiscountID]
                INNER JOIN  [Offer].[LkOffer_ProductDiscount] lkpd WITH(NOLOCK)
                ON          lkpd.[ProductDiscountID] = pd.[ProductDiscountID]
                WHERE       lkpd.[OfferID] = @OfferId AND
                            dl.[DiscountLimitType] = 'volume' AND
                            dl.[MaximumQuantity] IS NOT NULL
            ), 
			0
		)
	)
END
