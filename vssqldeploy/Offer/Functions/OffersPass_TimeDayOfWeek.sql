﻿
CREATE   FUNCTION [Offer].[OffersPass_TimeDayOfWeek] (@TimeZoneId int)
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GET All Offers that have at least one rule filter, and do not fail [Time Day Of Week] Rule Filter
	RETURN
		SELECT		lkrf.[OfferID]
		FROM		[Offer].[LkOffer_RuleFilter] lkrf WITH(NOLOCK)
		LEFT JOIN	[Offer].[TimeZoned_TimeDayOfWeek](@TimeZoneId) tod
		ON			lkrf.[RuleFilterID] = tod.[RuleFilterID]
		WHERE		(tod.[RuleFilterID] IS NULL) OR
					(tod.[TimeOfWeekStart] < TRY_CONVERT(TIME, GETUTCDATE()) AND tod.[TimeOfWeekEnd] >  TRY_CONVERT(TIME, GETUTCDATE()))
		GROUP BY	lkrf.[OfferID]
