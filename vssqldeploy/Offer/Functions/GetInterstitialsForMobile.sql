﻿

CREATE   FUNCTION [Offer].[GetInterstitialsForMobile]( @OfferId uniqueidentifier )
RETURNS nvarchar(max)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT		ISNULL(mz.[MobileZoneID], iz.InterstitialZoneID) AS ID,
					[ZoneInfoID] AS [ZoneInfoId], 
					[ZoneName], 
					[MaxOffersDisplayCount], 
					[ImageUrl], 
					[CardDisplayType],
					[Headline], 
					[Description], 
					[ContentPosition], 
					[LinkType], 
					[LinkType2], 
					[CtaText], 
					[CtaText2], 
					[Url], 
					[Url2],
					[BimLinkType], 
					[ExpirationDate]
		FROM 		[ContentPush].[InterstitialZone] iz
		LEFT JOIN	[ContentPush].[LKInterstitial_MobileZones] mz
		ON			iz.[InterstitialZoneID] = mz.[InterstitialZoneID]
		WHERE		iz.OfferID = @OfferId
					AND (
						iz.[ExpirationDate] IS NULL 
						OR iz.[ExpirationDate] > GETUTCDATE()
						)
					AND iz.[IsDeleted] = 0
					AND iz.[IsActive] = 1
		FOR JSON PATH
	)
END
