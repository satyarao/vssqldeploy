﻿
CREATE   FUNCTION [Offer].[LkOffer_Tenant] ( @TenantId uniqueidentifier = NULL, @OfferId uniqueidentifier = NULL )
RETURNS TABLE
WITH SCHEMABINDING
AS
	--Returns all Offers for a given Tenant, or all tenants for a given offer
	RETURN
		--GET The Offers Tenant if it is not a Distributed Channel Offer
		SELECT		ofr.[OfferID],
					ofr.[TenantID]
		FROM		[Offer].[Offer] ofr WITH(NOLOCK)
		LEFT JOIN 	[Offer].[DistributionChannelOffers](NULL) dc
		ON			ofr.[OfferID] = dc.[OfferID]
		WHERE		dc.[OfferID] IS NULL AND
					ofr.[TenantID] = ISNULL(@TenantId, ofr.[TenantID]) AND
					ofr.[OfferID] = ISNULL(@OfferId, ofr.[OfferID])
		UNION
		--GET The Offer App Tenants if it is not a Distributed Channel Offer
		SELECT		lkat.[OfferID],
					lkat.[AppTenantID]
		FROM		[Offer].[LkOffer_AppTenant] lkat WITH(NOLOCK)
		LEFT JOIN 	[Offer].[DistributionChannelOffers](NULL) dc
		ON			lkat.[OfferID] = dc.[OfferID]
		WHERE		dc.[OfferID] IS NULL AND
					lkat.[IsActive] = 1 AND
					lkat.[AppTenantID] = ISNULL(@TenantId, lkat.[AppTenantID]) AND
					lkat.[OfferID] = ISNULL(@OfferId, lkat.[OfferID])
		UNION
		--GET Distributed Channel Tenants
		SELECT		lkrf.[OfferID],
					rfdc.[AppTenantID]
		FROM		[Offer].[LkOffer_RuleFilter] lkrf WITH(NOLOCK)
		INNER JOIN	[Offer].[RF_DistributionChannel] rfdc WITH(NOLOCK)
		ON			lkrf.[RuleFilterID] = rfdc.[RuleFilterID]
		WHERE		lkrf.[IsActive] = 1 AND
					rfdc.[AppTenantID] = ISNULL(@TenantId, rfdc.[AppTenantID]) AND
					lkrf.[OfferID] = ISNULL(@OfferId, lkrf.[OfferID])
