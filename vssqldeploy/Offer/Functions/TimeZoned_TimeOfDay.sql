﻿

CREATE   FUNCTION [Offer].[TimeZoned_TimeOfDay] (@TimeZoneId int)
RETURNS TABLE
WITH SCHEMABINDING
AS
	--Converts time of days to be the correct UTC time for the given timezone
	RETURN 
		SELECT	a.[RuleFilterID],
				a.[IsLocalTime],
				CASE
					WHEN a.[IsLocalTime] = 1  AND a.[TimeOfDayStart] IS NOT NULL
						THEN TRY_CONVERT(TIME, TRY_CONVERT(DATETIME2(7), a.[TimeOfDayStart]) AT TIME ZONE b.[Identifier])
					ELSE a.[TimeOfDayStart]
				END [TimeOfDayStart],
				CASE
					WHEN a.[IsLocalTime] = 1 AND a.[TimeOfDayEnd] IS NOT NULL
						THEN TRY_CONVERT(TIME, TRY_CONVERT(DATETIME2(7), a.[TimeOfDayEnd]) AT TIME ZONE b.[Identifier])
					ELSE a.[TimeOfDayEnd]
				END [TimeOfDayEnd]
		FROM	[Offer].[RF_TimeOfDay] a WITH(NOLOCK)
		CROSS JOIN [Common].[LKTimezone] b WITH(NOLOCK)
		WHERE	b.[TimeZoneID] = @TimeZoneId
