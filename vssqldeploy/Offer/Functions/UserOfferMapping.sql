﻿
CREATE   FUNCTION [Offer].[UserOfferMapping] ( @OfferId uniqueidentifier = NULL, @UserId uniqueidentifier = NULL )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		[OfferID],
					[UserID]
		FROM		[Offer].[FilteredOfferUsers]( @OfferId, @UserId )

		UNION

		SELECT		ofr.[OfferID],
					usr.[UserID]
		FROM		[Offer].[OffersWithoutAudienceFilters]( @OfferId ) ofr
		INNER JOIN	[Offer].[LkOffer_Tenant]( [dbo].[GetUsersTenant]( @UserId ), @OfferId ) lkt
		ON			lkt.[OfferID] = ofr.[OfferID]
		INNER JOIN	[dbo].[UserInfo] usr WITH(NOLOCK)
		ON			lkt.[TenantID] = usr.[TenantID]
		WHERE		usr.[UserID] = ISNULL(@UserId, usr.[UserID])

