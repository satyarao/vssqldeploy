﻿
CREATE   FUNCTION [Offer].[OffersWithoutRuleFilters] ( @OfferID uniqueidentifier )
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GET Offers that have no rule filters
	RETURN
		SELECT		ofr.[OfferID],
					ofr.[TenantID]
		FROM		[Offer].[Offer] ofr WITH(NOLOCK)
		LEFT JOIN	[Offer].[LkOffer_RuleFilter] lkRF WITH(NOLOCK)
		ON			lkRF.[OfferID] = ofr.[OfferID]
		WHERE		lkRF.[OfferID] IS NULL AND
					ofr.[OfferID] = ISNULL(@OfferID, ofr.[OfferID])
		GROUP BY	ofr.[TenantID], ofr.[OfferID]
