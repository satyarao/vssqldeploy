﻿
CREATE   FUNCTION [Offer].[TimeZoned_TimeDayOfWeek] (@TimeZoneId int)
RETURNS TABLE
WITH SCHEMABINDING
AS
	--Converts time of days to be the correct UTC time for the given timezone
	--Filters days of week that are not today (at the given timezone)
	RETURN 
		SELECT	a.[RuleFilterID],
				a.[IsLocalTime],
				CASE
					WHEN a.[IsLocalTime] = 1  AND a.[TimeOfWeekStart] IS NOT NULL
						THEN TRY_CONVERT(TIME, TRY_CONVERT(DATETIME2(7), a.[TimeOfWeekStart]) AT TIME ZONE b.[Identifier])
					ELSE a.[TimeOfWeekStart]
				END [TimeOfWeekStart],
				CASE
					WHEN a.[IsLocalTime] = 1 AND a.[TimeOfWeekEnd] IS NOT NULL
						THEN TRY_CONVERT(TIME, TRY_CONVERT(DATETIME2(7), a.[TimeOfWeekEnd]) AT TIME ZONE b.[Identifier])
					ELSE a.[TimeOfWeekEnd]
				END [TimeOfWeekEnd]
		FROM	[Offer].[RF_TimeDayOfWeek] a WITH(NOLOCK)
		CROSS JOIN [Common].[LKTimezone] b WITH(NOLOCK)
		WHERE	b.[TimeZoneID] = @TimeZoneId AND
				LOWER(a.[DaysOfWeek]) = [Common].[TimeZone_DayOfWeek](@TimeZoneId, a.[IsLocalTime])
