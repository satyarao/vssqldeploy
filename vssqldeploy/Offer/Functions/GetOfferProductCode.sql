﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   FUNCTION [Offer].[GetOfferProductCode]( @OfferId uniqueidentifier, @OfferType nvarchar(50) )
RETURNS nvarchar(50)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		CASE
			WHEN @OfferType = 'Product Rewards'
				THEN
				(
					SELECT TOP(1) [ProductCode]
					FROM
					(
						SELECT 
							CASE
								WHEN sp.[UpcA] IS NOT NULL
									THEN sp.[UpcA]
								WHEN sp.[UpcE] IS NOT NULL
									THEN sp.[UpcE]
								WHEN sp.[Ean8] IS NOT NULL
									THEN sp.[Ean8]
								WHEN sp.[PosCode] IS NOT NULL
									THEN sp.[PosCode]
								WHEN sp.[PaymentSystemProductCode] IS NOT NULL
									THEN sp.[PaymentSystemProductCode]
							END AS [ProductCode],

							sp.[ProductCodeType] AS [ProductCodeType]
						FROM [Product].[StandardProduct] sp WITH(NOLOCK)
						INNER JOIN [Product].[LKStandardProductGroup] lkspg WITH(NOLOCK)
						ON lkspg.[StandardProductCodeID] = sp.[StandardProductID]
						INNER JOIN [Offer].[ProductDiscount] pd WITH(NOLOCK)
						ON pd.[StandardProductGroupID] = lkspg.[StandardProductGroupID]
						INNER JOIN [Offer].[LkOffer_ProductDiscount] lkpd WITH(NOLOCK)
						ON lkpd.[ProductDiscountID] = pd.[ProductDiscountID]
						WHERE lkpd.[OfferID] = @OfferId
					) tbl
				)
			ELSE NULL
		END
	)
END
