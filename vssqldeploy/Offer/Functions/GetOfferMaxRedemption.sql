﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   FUNCTION [Offer].[GetOfferMaxRedemption]( @OfferId uniqueidentifier )
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT      TOP(1) TRY_CONVERT(INT, [MaxRedemptions])
		FROM        [Offer].[MaxRedemption] WITH(NOLOCK)
		WHERE       [RedemptionType] = 'offer'  AND 
					[MaxRedemptionID] IN (
						SELECT  [MaxRedemptionID] 
						FROM    [Offer].[LkOffer_MaxRedemption] WITH(NOLOCK)
						WHERE   [OfferID] = @OfferId
					)
	)
END
