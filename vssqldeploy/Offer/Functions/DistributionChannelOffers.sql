﻿

CREATE   FUNCTION [Offer].[DistributionChannelOffers] (@OfferId uniqueidentifier = NULL)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
	--RETURNS All Offers that have a DistributionChannel Rule Filter
	--This is to exclude the Offers TenantID in TenantOfferMatching
	SELECT		lkrf.[OfferID]
	FROM		[Offer].[RF_DistributionChannel] rfdc WITH(NOLOCK)
	INNER JOIN 	[Offer].[LkOffer_RuleFilter] lkrf WITH(NOLOCK)
	ON			lkrf.[RuleFilterID] = rfdc.[RuleFilterID]
	WHERE		lkrf.[OfferID] = ISNULL(@OfferId, lkrf.[OfferID])
	GROUP BY 	lkrf.[OfferID]
