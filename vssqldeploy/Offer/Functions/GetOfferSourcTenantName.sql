﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   FUNCTION [Offer].[GetOfferSourcTenantName]( @AdvertOfferId uniqueidentifier )
RETURNS nvarchar(255)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT	[Name]
		FROM	[dbo].[Tenant]
		WHERE	[TenantID] = (	SELECT 	TOP(1) [TenantID] 
								FROM 	[Offer].[AdvertOffer] 
								WHERE 	[AdvertOfferID] = @AdvertOfferId )
	)
END
