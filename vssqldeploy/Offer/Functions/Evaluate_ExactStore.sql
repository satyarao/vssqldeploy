﻿

CREATE   FUNCTION [Offer].[Evaluate_ExactStore] ( @EvaluationStoreBase [Offer].[EvaluateStore_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		esb.[OfferID],
					esb.[StoreID],
					esb.[RuleFilterID],
					CASE
						WHEN esb.[MppaID] = RF.[MppaId]
							THEN 1
						ELSE 0
					END AS [EvaluationResult]
		FROM		@EvaluationStoreBase esb
		INNER JOIN	[Offer].[RF_ExactStore] RF WITH(NOLOCK)
		ON			RF.[RuleFilterID] = esb.[RuleFilterID]
