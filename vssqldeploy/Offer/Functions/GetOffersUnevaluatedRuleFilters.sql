﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   FUNCTION [Offer].[GetOffersUnevaluatedRuleFilters]( @OfferId uniqueidentifier )
RETURNS nvarchar(max)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT      JSON_QUERY('[' + STRING_AGG('"' + TRY_CONVERT(nvarchar(36),lkRf.[RuleFilterID]) +  '"', ',') + ']')
		FROM        [Offer].[LkOffer_RuleFilter] lkRf WITH(NOLOCK)
		INNER JOIN	[dbo].[RuleFilter] rf
		ON			lkRf.[RuleFilterID] = rf.[RuleFilterID]
		WHERE       [OfferID] = @OfferId AND rf.RuleFilterTypeID IN 
						(
							 --1,		--TimeOfWeekFilter
							 --2,		--TimeOfDayFilter
							 --3,		--ByDateFilter
							 --4,		--NewUserFilter
							 --5,		--UserCreatedOnFilter
							 --6,		--CompletedTransactionsFilter
							 7,		--LastVisitDateFilter
							 8,		--VisitCountToStoreFilter
							 --9,		--ExactStoreFilter
							 10,	--StoreGroupFilter
							 --11,	--StoreGeolocationFilter
							 --12,	--TenantFilter[DEPRECATED]
							 13,	--PaymentMethodFilter
							 14,	--VisitCountToStoreActivityFilter
							 15,	--FirstVisitToStore
							 --16,	--ImpressionCountFilter[DEPRECATED]
							 --17,	--OfferClicksFilter[DEPRECATED]
							 18,	--PreviousPurchaseFilter
							 19,	--PurchasedFuelFilter
							 --20,	--ZoneRuleFilter[DEPRECATED]
							 --21,	--DistributionChannelFilter
							 22,	--UserGroupFilter
							 --23,	--StoreZipCodesFilter
							 24		--UserInputFilter
							 --25,	--StoreCityStateFilter
							 --26	--StoreStatesFilter
						 )
		GROUP BY    lkRf.[OfferID]
	)
END
