﻿


CREATE   FUNCTION [Offer].[Evaluate_VisitCountToStoreActivity] ( @EvaluationUserBase [Offer].[EvaluateUser_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		eub.[OfferID],
					eub.[UserID],
					eub.[RuleFilterID],
					CASE
						WHEN RF.[NumberOfTransactions] <= [dbo].[UserTransactionCount]( eub.[UserID], RF.[TransactionStartDate] )
							THEN 1
						ELSE 0
					END as [EvaluationResult]
		FROM		@EvaluationUserBase eub
		INNER JOIN	[Offer].[RF_VisitCountToStoreActivity] RF WITH(NOLOCK)
		ON			RF.[RuleFilterID] = eub.[RuleFilterID]
