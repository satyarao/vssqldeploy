﻿
CREATE   FUNCTION [Offer].[GetActiveOffers] (@TenantId uniqueidentifier, @TimeZoneId int)
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GET All Offers for a Tenant where the Offer is Active, and if the offer
	--has a [Date], [Time Of Day] or [Time Day Of Week] Rule Filter it passes
	RETURN
		SELECT		tom.[OfferID],
					ofr.[TargetUsersType]
		FROM		[Offer].[Offer] ofr WITH(NOLOCK)
		INNER JOIN	[Offer].[TenantOfferMatching](@TenantId) tom
		ON			tom.[OfferID] = ofr.[OfferID]
		LEFT JOIN	[Offer].[LkOffer_RuleFilter] lkrf WITH(NOLOCK)
		ON			lkrf.[OfferID] = tom.[OfferID]
		LEFT JOIN	[Offer].[OffersPass_Date]() dte
		ON			tom.[OfferID] = dte.[OfferID]
		LEFT JOIN	[Offer].[OffersPass_TimeOfDay](@TimeZoneId) tod
		ON			tom.[OfferID] = tod.[OfferID]
		LEFT JOIN	[Offer].[OffersPass_TimeDayOfWeek](@TimeZoneId) tdow
		ON			tom.[OfferID] = tdow.[OfferID]
		WHERE		(
						(lkrf.[OfferID] IS NULL) 
						OR
						(
							dte.[OfferID] IS NOT NULL AND
							tod.[OfferID] IS NOT NULL AND
							tdow.[OfferID] IS NOT NULL
						)
					)

