﻿

CREATE   FUNCTION [Offer].[OffersPass_Date] ()
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GET All Offers that have at least one rule filter, and do not fail [Date] Rule Filter
	RETURN
		SELECT		lkrf.[OfferID]
		FROM		[Offer].[LkOffer_RuleFilter] lkrf WITH(NOLOCK)
		LEFT JOIN	[Offer].[RF_Date] dte WITH(NOLOCK)
		ON			lkrf.[RuleFilterID] = dte.[RuleFilterID]
		WHERE		(dte.[RuleFilterID] IS NULL) OR
					(dte.[StartDate] < GETUTCDATE() AND dte.[EndDate] >  GETUTCDATE())
		GROUP BY	lkrf.[OfferID]
