﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   FUNCTION [Offer].[GetOfferAppTenants]( @OfferId uniqueidentifier )
RETURNS nvarchar(max)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT		JSON_QUERY('[' + STRING_AGG('"' + LOWER(CONVERT(nvarchar(36),[AppTenantID])) +  '"', ',') + ']')
		FROM		[Offer].[LKOffer_AppTenant] WITH(NOLOCK)
		WHERE		[OfferID] = @OfferId
		GROUP BY	[OfferID]
	)
END
