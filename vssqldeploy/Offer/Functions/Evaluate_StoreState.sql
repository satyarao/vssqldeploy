﻿
CREATE   FUNCTION [Offer].[Evaluate_StoreState] ( @EvaluationStoreBase [Offer].[EvaluateStore_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		esb.[OfferID],
					esb.[StoreID],
					esb.[RuleFilterID],
					CASE
						WHEN esb.[StateCode] = RF.[State]
							THEN 1
						ELSE 0
					END AS [EvaluationResult]
		FROM		@EvaluationStoreBase esb
		INNER JOIN	[Offer].[RF_StoreState] RF WITH(NOLOCK)
		ON			RF.[RuleFilterID] = esb.[RuleFilterID]
