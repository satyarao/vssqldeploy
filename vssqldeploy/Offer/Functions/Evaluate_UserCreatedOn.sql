﻿

CREATE   FUNCTION [Offer].[Evaluate_UserCreatedOn] (  @EvaluationUserBase [Offer].[EvaluateUser_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		eub.[OfferID],
					eub.[UserID],
					eub.[RuleFilterID],
					CASE
						WHEN	DATEDIFF(DAY, eub.[CreatedOn], GETUTCDATE()) > RF.[MinAccountAgeDays] AND 
								DATEDIFF(DAY, eub.[CreatedOn], GETUTCDATE()) < RF.[MaxAccountAgeDays]
							THEN 1
						ELSE 0
					END
					AS [EvaluationResult]
		FROM		@EvaluationUserBase eub
		INNER JOIN	[Offer].[RF_UserCreatedOn] RF WITH(NOLOCK)
		ON			RF.[RuleFilterID] = eub.[RuleFilterID]
