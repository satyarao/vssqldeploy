﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE FUNCTION [Offer].[GetProductRewardModelDiscountInfo]( @OfferId uniqueidentifier, @TenantId uniqueidentifier )
RETURNS NVARCHAR(MAX)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT		[DiscountType]
					,[DiscountValue] AS [DiscountAmount.Value]
					,[DiscountCurrency] AS [DiscountAmount.Currency]
					,(
						SELECT TOP(1) [MinimumQuantity]
						FROM [Offer].[DiscountLimit] dl WITH(NOLOCK)
						WHERE dl.[ProductDiscountID] = pd.[ProductDiscountID] 
								AND (dl.[DiscountLimitType] = 'quantity' OR dl.[DiscountLimitType] = 'volume')
					)
					AS [MinimumQuantity]
					,(
						SELECT TOP(1) [MaximumQuantity]
						FROM [Offer].[DiscountLimit] dl WITH(NOLOCK)
						WHERE dl.[ProductDiscountID] = pd.[ProductDiscountID]
								AND (dl.[DiscountLimitType] = 'quantity' OR dl.[DiscountLimitType] = 'volume')
					)
					AS [MaximumQuantity]
					,[NewUnitPrice] AS [ItemUnitPrice]
					,(
						SELECT spg.[Name] AS [fuelGroupName]
							,spg.[GroupCode] AS [fuelGroupProductCode]
							,TRY_CONVERT(int, spg.[GroupCode]) AS [fuelGroupId]
							,@TenantId AS [tenantId]
							,(
								SELECT spc.[PaymentSystemProductCode] as PCATSCode
									,spc.[Description] as FuleGradeDescription
								FROM [Product].[LKStandardProductGroup]lkSpg
								INNER JOIN [Product].[StandardProduct] spc WITH(NOLOCK)
								ON lkSpg.[StandardProductCodeID] = spc.[StandardProductID]
								WHERE lkSpg.[StandardProductGroupID] = spg.[StandardProductGroupID]
								FOR JSON PATH
							)
							AS [paymentSystemProductCodes]
						FROM [Product].[StandardProductGroup] spg WITH(NOLOCK)
						WHERE spg.[StandardProductGroupID] = pd.[StandardProductGroupID]
						FOR JSON PATH
					)
					AS [FuelGroups]
					,(
						SELECT	[POSCodeFormat] = [ProductCodeType],
								[POSCode] = [PosCode]
						FROM	[Product].[GetStandardProductGroupItemCodes]( pd.[StandardProductGroupID] )
						FOR JSON PATH
					)
					AS [ItemCodes]
		FROM [Offer].[ProductDiscount] pd WITH(NOLOCK)
		INNER JOIN [Offer].[LkOffer_ProductDiscount] lkPd WITH(NOLOCK)
			ON pd.[ProductDiscountID] = lkPd.[ProductDiscountID]
		WHERE lkPd.[OfferID] = @OfferId
		FOR JSON PATH
	)
END