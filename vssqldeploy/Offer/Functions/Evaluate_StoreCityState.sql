﻿
CREATE   FUNCTION [Offer].[Evaluate_StoreCityState] ( @EvaluationStoreBase [Offer].[EvaluateStore_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		esb.[OfferID],
					esb.[StoreID],
					esb.[RuleFilterID],
					CASE
						WHEN	esb.[StateCode] = RF.[StateName] AND 
								esb.[City] = RF.[CityName]
							THEN 1
						ELSE 0
					END AS [EvaluationResult]
		FROM		@EvaluationStoreBase esb
		INNER JOIN	[Offer].[RF_StoreCityState] RF WITH(NOLOCK) 
		ON			RF.[RuleFilterID] = esb.[RuleFilterID]