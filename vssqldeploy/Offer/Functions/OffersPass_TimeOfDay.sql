﻿
CREATE   FUNCTION [Offer].[OffersPass_TimeOfDay] (@TimeZoneId int)
RETURNS TABLE
WITH SCHEMABINDING
AS
	--GET All Offers that have at least one rule filter, and do not fail [Time Of Day] Rule Filter
	RETURN
		SELECT		lkrf.[OfferID]
		FROM		[Offer].[LkOffer_RuleFilter] lkrf WITH(NOLOCK)
		LEFT JOIN	[Offer].[TimeZoned_TimeOfDay](@TimeZoneId) tod
		ON			lkrf.[RuleFilterID] = tod.[RuleFilterID]
		WHERE		(tod.[RuleFilterID] IS NULL) OR
					(tod.[TimeOfDayStart] < TRY_CONVERT(TIME, GETUTCDATE()) AND tod.[TimeOfDayEnd] >  TRY_CONVERT(TIME, GETUTCDATE()))
		GROUP BY	lkrf.[OfferID]
