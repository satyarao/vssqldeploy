﻿
CREATE   FUNCTION [Offer].[GetSitesCoveredCount]( @OfferId uniqueidentifier )
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		ISNULL
		(
			(
                SELECT	COUNT([StoreID])
                FROM	[Offer].[StoreOfferMatching] som WITH(NOLOCK)
                WHERE	som.[OfferID] = @OfferId
            ), 
			0
		)
	)
END
