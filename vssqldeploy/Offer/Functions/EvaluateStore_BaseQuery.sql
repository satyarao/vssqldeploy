﻿



CREATE   FUNCTION [Offer].[EvaluateStore_BaseQuery] ( @OfferId uniqueidentifier = NULL, @StoreId uniqueidentifier = NULL )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		ofr.[OfferID],
					stre.[StoreID],
					lkRF.[RuleFilterID],
					stre.[ZipCode],
					stre.[StateCode],
					stre.[City],
					[Common].[MPPAID_INT_TO_STRING](stre.[MppaID]) AS [MppaID]
		FROM		[Offer].[Offer] ofr WITH(NOLOCK)
		INNER JOIN	[Offer].[LkOffer_RuleFilter] lkRF WITH(NOLOCK)
		ON			lkRF.[OfferID] = ofr.[OfferID]
		INNER JOIN	[dbo].[StorePartnerTenantMapping]( NULL, @StoreID ) stre 
		ON			stre.[TenantID] = ofr.[TenantID]
		WHERE		ofr.[OfferID] = ISNULL(@OfferId, ofr.[OfferID]) AND
					stre.[StoreID] = ISNULL(@StoreId, stre.[StoreID])