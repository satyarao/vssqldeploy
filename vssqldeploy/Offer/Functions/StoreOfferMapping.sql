﻿
CREATE   FUNCTION [Offer].[StoreOfferMapping] ( @OfferId uniqueidentifier = NULL, @StoreId uniqueidentifier = NULL )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		[OfferID],
					[StoreID]
		FROM		[Offer].[FilteredOfferStores]( @OfferId, @StoreId )
		
		UNION

		SELECT		[OfferID],
					[StoreID]
		FROM		[Offer].[OffersWithoutStoreFilters]( @OfferId ) ofr
		CROSS JOIN	[dbo].[StorePartnerTenantMapping] ( (SELECT TOP(1) [TenantID] FROM [Offer].[Offer] WHERE [OfferID] = @OfferId), @StoreId ) stre
		--ON			ofr.[StoreID] = stre.[StoreID]
		WHERE		stre.[StoreID] = ISNULL(@StoreId, stre.[StoreID])