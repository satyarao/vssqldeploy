﻿
CREATE   FUNCTION [Offer].[Evaluate_StoreZipcode] ( @EvaluationStoreBase [Offer].[EvaluateStore_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		esb.[OfferID],
					esb.[StoreID],
					esb.[RuleFilterID],
					CASE
						WHEN esb.[ZipCode] = RF.[ZipCode]
							THEN 1
						ELSE 0
					END AS [EvaluationResult]
		FROM		@EvaluationStoreBase esb
		INNER JOIN	[Offer].[RF_StoreZipcode] RF WITH(NOLOCK)
		ON			RF.[RuleFilterID] = esb.[RuleFilterID]
