﻿

CREATE   FUNCTION [Offer].[FilteredOfferStores] ( @OfferId uniqueidentifier = NULL, @StoreId uniqueidentifier = NULL )
RETURNS @filteredOfferStores TABLE (
	[OfferID]	uniqueidentifier	NOT NULL,
	[StoreID]	uniqueidentifier	NOT NULL
)
WITH SCHEMABINDING
AS
BEGIN
	
	DECLARE @EvaluationBase [Offer].[EvaluateStore_Base];
	INSERT INTO @EvaluationBase
		([OfferID],[StoreID],[RuleFilterID],[ZipCode],[StateCode],[City],[MppaID])
	SELECT	[OfferID],
			[StoreID],
			[RuleFilterID],
			[ZipCode],
			[StateCode],
			[City],
			[MppaID]
	FROM	[Offer].[EvaluateStore_BaseQuery]( @OfferId, @StoreId )

	INSERT INTO @filteredOfferStores
		([OfferID], [StoreID])
	SELECT	[OfferID],
			[StoreID]
	FROM
	(
		SELECT	[OfferID],
				[StoreID],
				COUNT([RuleFilterID])	AS [NumFilter],
				SUM([EvaluationResult])	AS [NumFiltersPassed]
		FROM
		(
			/****************************************ExactSites****************************************/
			SELECT		[OfferID],
						[StoreID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_ExactStore]( @EvaluationBase )
			UNION
			/****************************************StoreCityState****************************************/
			SELECT		[OfferID],
						[StoreID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_StoreCityState]( @EvaluationBase )
			UNION
			/****************************************StoreState****************************************/
			SELECT		[OfferID],
						[StoreID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_StoreState]( @EvaluationBase )
			UNION
			/****************************************StoreZipcode****************************************/
			SELECT		[OfferID],
						[StoreID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_StoreZipcode]( @EvaluationBase )
		) tbl
		GROUP BY [OfferID], [StoreID]
	) otrTbl
	WHERE	[NumFilter] = [NumFiltersPassed]

	RETURN;
END