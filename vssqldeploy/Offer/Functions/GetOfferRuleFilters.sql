﻿
CREATE   FUNCTION [Offer].[GetOfferRuleFilters]( @OfferId uniqueidentifier )
RETURNS nvarchar(max)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
		SELECT      JSON_QUERY('[' + STRING_AGG('"' + TRY_CONVERT(nvarchar(36),[RuleFilterID]) +  '"', ',') + ']')
		FROM        [Offer].[LkOffer_RuleFilter] WITH(NOLOCK)
		WHERE       [OfferID] = @OfferId
		GROUP BY    [OfferID]
	)
END
