﻿
CREATE   FUNCTION [Offer].[TenantOfferMatching] (@TenantId uniqueidentifier = NULL)
RETURNS TABLE
WITH SCHEMABINDING
AS
	--Returns all BaseActiveOffer's for a given Tenant
	RETURN
		SELECT		ofr.[OfferID],
					ofr.[TenantID],
					bao.[TargetUsersType]
		FROM		[Offer].[LkOffer_Tenant]( @TenantId, NULL ) ofr
		INNER JOIN	[Offer].[BaseActiveOffer]( @TenantId, NULL ) bao
		ON			ofr.[OfferID] = bao.[OfferID]
