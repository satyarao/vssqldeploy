﻿CREATE   FUNCTION [Offer].[GetCurrentRedemptionCount]
(
	@OfferID	uniqueidentifier,
	@UserID		uniqueidentifier
)
RETURNS INT
AS
BEGIN

	RETURN (
		SELECT TOP(1) [RedemptionCount]
		FROM
		(
			SELECT	ISNULL(MAX([CurrentRedemptionCount]), 0) AS [RedemptionCount]
			FROM	[Offer].[OfferRedemption]
			WHERE	[OfferID] = @OfferID AND
					[UserID] = @UserID
			GROUP BY [OfferID], [UserID]
		) tbl
	);

END
