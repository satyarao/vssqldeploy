﻿
CREATE   FUNCTION [Offer].[GetOfferProductDiscounts]( @OfferId uniqueidentifier )
RETURNS nvarchar(max)
WITH SCHEMABINDING
AS
BEGIN
	RETURN
	(
        SELECT		PD.[ProductDiscountID]    AS ProductDiscountId,
					[ProductRewardID]         AS ProductRewardId,
					[StandardProductGroupID]  AS StandardProductGroupId,
					[DiscountType],
					DL.[DiscountLimitType],
					[DiscountValue],
					[DiscountCurrency],
					[NewUnitPrice],
					DL.[MinimumQuantity],
					DL.[MaximumQuantity]
        FROM 		[Offer].[ProductDiscount] PD WITH(NOLOCK)
        LEFT JOIN 	[Offer].[DiscountLimit] DL WITH(NOLOCK) 
		ON 			PD.ProductDiscountID = DL.ProductDiscountID
        WHERE		PD.[IsActive] = 1 AND
					DL.IsActive=1 AND
					PD.[ProductDiscountID] IN 
					(
						SELECT 	[ProductDiscountID] 
						FROM 	[Offer].[LkOffer_ProductDiscount] WITH(NOLOCK) 
						WHERE 	[OfferID] = @OfferId
					)
        ORDER BY 	[ProductDiscountID]
        FOR JSON PATH
    )
END
