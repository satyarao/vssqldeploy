﻿

CREATE   FUNCTION [Offer].[FilteredOfferUsers] ( @OfferId uniqueidentifier = NULL, @UserId uniqueidentifier = NULL )
RETURNS @filteredOfferUsers TABLE (
	[OfferID]	uniqueidentifier	NOT NULL,
	[UserID]	uniqueidentifier	NOT NULL
)
WITH SCHEMABINDING
AS
BEGIN
	DECLARE @EvaluationBase [Offer].[EvaluateUser_Base];
	INSERT INTO @EvaluationBase
		([OfferID],[UserID],[RuleFilterID],[CreatedOn])
	SELECT	[OfferID],
			[UserID],
			[RuleFilterID],
			[CreatedOn]
	FROM	[Offer].[EvaluateUser_BaseQuery]( @OfferId, @UserId )

	INSERT INTO @filteredOfferUsers
		([OfferID], [UserID])
	SELECT	[OfferID],
			[UserID]
	FROM
	(
		SELECT	[OfferID],
				[UserID],
				COUNT([RuleFilterID])	AS [NumFilter],
				SUM([EvaluationResult])	AS [NumFiltersPassed]
		FROM
		(
			/****************************************UserCreatedOn****************************************/
			SELECT		[OfferID],
						[UserID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_UserCreatedOn]( @EvaluationBase )
			UNION
			/****************************************NewUser****************************************/
			SELECT		[OfferID],
						[UserID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_NewUser]( @EvaluationBase )
			UNION
			/****************************************CompletedTransactions****************************************/
			SELECT		[OfferID],
						[UserID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_CompletedTransactions]( @EvaluationBase )
				
			UNION
			/****************************************LastVisitDate****************************************/
			SELECT		[OfferID],
						[UserID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_LastVisitDate]( @EvaluationBase )
			UNION
			/****************************************VisitCountToStoreActivity****************************************/
			SELECT		[OfferID],
						[UserID],
						[RuleFilterID],
						[EvaluationResult]
			FROM		[Offer].[Evaluate_VisitCountToStoreActivity]( @EvaluationBase )
		) tbl
		GROUP BY [OfferID], [UserID]
	) otrTbl
	WHERE	[NumFilter] = [NumFiltersPassed]

	RETURN;
END
