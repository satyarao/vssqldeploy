﻿
CREATE   FUNCTION [Offer].[Evaluate_LastVisitDate] ( @EvaluationUserBase [Offer].[EvaluateUser_Base] READONLY )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT		eub.[OfferID],
					eub.[UserID],
					eub.[RuleFilterID],
					CASE
						WHEN RF.MinDaysSinceLastVisit <= [dbo].[UserDaysSinceLatestTransaction]( eub.[UserID], RF.[TransactionStartDate], RF.[TransactionEndDate] )
							THEN 1
						ELSE 0
					END AS [EvaluationResult]
		FROM		@EvaluationUserBase eub
		INNER JOIN	[Offer].[RF_LastVisitDate] RF WITH(NOLOCK)
		ON			RF.[RuleFilterID] = eub.[RuleFilterID]
