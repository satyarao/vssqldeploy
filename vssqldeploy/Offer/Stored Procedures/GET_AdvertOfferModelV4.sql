﻿
CREATE   PROCEDURE [Offer].[GET_AdvertOfferModelV4] (
    @AdvertOfferID  uniqueidentifier,
    @TenantID       uniqueidentifier = NULL,
    @IsDeleted      bit = NULL
) AS
BEGIN
BEGIN TRY
    SELECT
        [AdvertOfferID]     AS [AdvertOfferID],
        [OfferTemplateID]   AS [OfferTemplateID],
        AdvOFR.[TenantID]   AS [TenantId],
        Tenant.[Name]       AS [TenantName],
        [OfferType]         AS [OfferType],
        [RewardType]        AS [RewardType],
        [TargetUsersType]   AS [TargetUsersType],
        [Title]             AS [Title],
        [Subtitle]          AS [Subtitle],
        [Description]       AS [Description],
        [EditorTitle]       AS [EditorTitle],
        [EditorDescription] AS [EditorDescription],
        [ImageURL]          AS [ImageUrl],
        [ImagePos]          AS [ImagePos],
        [Order]             AS [Order],
        [IsFeaturedOffer]   AS [IsFeaturedOffer],
        [IsDeleted]         AS [IsDeleted],
        AdvOFR.[IsActive]   AS [IsActive],
        [IsDraft]           AS [IsDraft],
        [IsRestricted]      AS [IsRestricted],
        [IsDisplay]         AS [IsDisplay],
        [IsBrandedOffer]    AS [IsBrandedOffer],
        [Display]           AS [Display],
        [StartDate]         AS [StartDate],
        [EndDate]           AS [EndDate],
        AdvOFR.[CreatedOn]  AS [Meta.createdOn],
        AdvOFR.[CreatedBy]  AS [Meta.createdBy],
        [CreatedByUserID]   AS [Meta.createdByUserID],
        AdvOFR.[UpdatedOn]  AS [Meta.updatedOn],
        AdvOFR.[UpdatedBy]  AS [Meta.updatedBy],
        [UpdatedByUserID]   AS [Meta.UpdatedByUserID],
        [Links]             AS [Links],
        [Categories]        AS [Categories],
        [RuleFilters]       AS [RuleFilters],
        [Campaigns]         AS [Campaigns],
        [MaxRedemptions]    AS [MaxRedemptions],
        [ProductDiscounts]  AS [ProductDiscounts],
        [ProductCode]       AS [ProductCode],
        [RedemptionsCount]  AS RedemptionsCount
    FROM
         [Offer].[vGET_AdvertOffers] AdvOFR
        JOIN [dbo].[Tenant] Tenant ON Tenant.TenantID = AdvOFR.TenantID
    WHERE
        [AdvertOfferID] = @AdvertOfferID
        AND (AdvOFR.[TenantID] = @TenantID OR @TenantID IS NULL)
        AND (AdvOFR.[IsDeleted] = @IsDeleted OR @IsDeleted IS NULL)

    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), CONVERT(nvarchar(36),@AdvertOfferID) AS [StoredProcedureInput];

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
