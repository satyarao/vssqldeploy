﻿CREATE   PROCEDURE [Offer].[GET_PunchCardAmount]
    @UserID     uniqueidentifier,
    @OfferID    uniqueidentifier
AS
SELECT
    PCA.[UserID]
    , PCA.[OfferID]
    , PCA.[CurrentPunchAmount]
    , OFR.[MaxPunchAmount]
FROM
    [Offer].[PunchCardAmount] AS PCA
    JOIN [Offer].[Offer] AS OFR ON OFR.OfferID = PCA.OfferID
