﻿
CREATE   PROCEDURE [Offer].[DELETE_OfferModelV4] (
    @OfferID    uniqueidentifier,
    @TenantID   uniqueidentifier
) AS
BEGIN
    IF NOT EXISTS (SELECT * FROM [Offer].[Offer] WHERE [OfferID] = @OfferID) RETURN
    IF NOT EXISTS (SELECT * FROM [dbo].[Tenant] WHERE [TenantID] = @TenantID) RETURN
    IF NOT EXISTS (SELECT * FROM [Offer].[Offer] WHERE [OfferID] = @OfferID AND TenantID = @TenantID)
    BEGIN
        ;THROW 50004, '@OfferID and @TenantID do not match', 1;
    END

BEGIN TRY
    BEGIN TRANSACTION

    UPDATE [Offer].[Offer]
    SET [IsDeleted] = 1
    WHERE [OfferID] = @OfferID

    IF
        (NOT EXISTS (SELECT * FROM [Offer].[LkOffer_Link] WHERE [LinkID] IN (SELECT LinkID FROM [Offer].[Link] WHERE [LinkID] IN (SELECT [LinkID] FROM [Offer].[LkOffer_Link] WHERE [OfferID] = @OfferID)) AND [OfferID] != @OfferID))
        AND
        (NOT EXISTS (SELECT * FROM [Offer].[LkAdvertOffer_Link] WHERE [LinkID] IN (SELECT [LinkID] FROM [Offer].[LkOffer_Link] WHERE [OfferID] = @OfferID)))
    BEGIN
        UPDATE [Offer].[Link]
        SET [IsActive] = 0
        WHERE [LinkID] IN (SELECT [LinkID] FROM [Offer].[LkOffer_Link] WHERE [OfferID] = @OfferID)
    END

    IF
        (NOT EXISTS (SELECT * FROM [Offer].[LkOffer_MaxRedemption] WHERE [MaxRedemptionID] IN (SELECT MaxRedemptionID FROM [Offer].[MaxRedemption] WHERE [MaxRedemptionID] IN (SELECT [MaxRedemptionID] FROM [Offer].[LkOffer_MaxRedemption] WHERE [OfferID] = @OfferID)) AND [OfferID] != @OfferID))
        AND
        (NOT EXISTS (SELECT * FROM [Offer].[LkAdvertOffer_MaxRedemption] WHERE [MaxRedemptionID] IN (SELECT [MaxRedemptionID] FROM [Offer].[LkOffer_MaxRedemption] WHERE [OfferID] = @OfferID)))
    BEGIN
        UPDATE [Offer].[MaxRedemption]
        SET [IsActive] = 0
        WHERE [MaxRedemptionID] IN (SELECT [MaxRedemptionID] FROM [Offer].[LkOffer_MaxRedemption] WHERE [OfferID] = @OfferID)
    END

    --UPDATE [Offer].[ProductReward]
    --SET [IsActive] = 0
    --WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_AppTenant]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_Campaign]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_Category]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    UPDATE [ContentPush].[InterstitialZone]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_Link]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_MaxRedemption]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_ProductDiscount]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_RuleFilter]
    SET [IsActive] = 0
    WHERE [OfferID] = @OfferID

    COMMIT TRANSACTION;
    RETURN 1;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), CONVERT(nvarchar(36),@OfferID) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
