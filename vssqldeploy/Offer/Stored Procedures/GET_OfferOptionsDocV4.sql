﻿
CREATE   PROCEDURE [Offer].[GET_OfferOptionsDocV4] (
    @TenantID uniqueidentifier
)
AS
BEGIN
    SELECT
        'offer_options_doc_v4'              AS [documentType],
        'CCE'                               AS [rawPartitionKey],
        'CCE'                               AS [partitionKey],
        [TenantID]                          AS [tenantId],
        [OfferTypeFuel]                     AS [offerTypeFuel],
        [OfferTypeProduct]                  AS [offerTypeProduct],
        [TimeOfWeekFilter]                  AS [timeOfWeekFilter],
        [TimeOfDayFilter]                   AS [timeOfDayFilter],
        [ByDateFilter]                      AS [byDateFilter],
        [UserCreatedOnFilter]               AS [userCreatedOnFilter],
        [NewUserFilter]                     AS [newUserFilter],
        [CompletedTransactionsFilter]       AS [completedTransactionsFilter],
        [LastVisitDateFilter]               AS [lastVisitDateFilter],
        [VisitCountToStoreFilter]           AS [visitCountToStoreFilter],
        [ExactStoreFilter]                  AS [exactStoreFilter],
        [StoreGroupFilter]                  AS [storeGroupFilter],
        [DistributionChannelFilter]         AS [distributionChannelFilter],
        [StoreGeolocationFilter]            AS [storeGeolocationFilter],
        [StoreCityStateFilter]              AS [storeCityStateFilter],
        [StoreStatesFilter]                 AS [storeStatesFilter],
        [PaymentMethodFilter]               AS [paymentMethodFilter],
        [FirstVisitToStore]                 AS [firstVisitToStore],
        [PreviousPurchaseFilter]            AS [previousPurchaseFilter],
        [PurchasedFuelFilter]               AS [purchasedFuelFilter],
        [UnauthenticatedFilter]             AS [unauthenticatedFilter],
        [DisplayTypeDiscount]               AS [displayTypeDiscount],
        [DisplayTypePromo]                  AS [displayTypePromo],
        [DisplayTypeCoupon]                 AS [displayTypeCoupon],
        [TenantFilter]                      AS [tenantFilter],
        [VisitCountToStoreActivityFilter]   AS [visitCountToStoreActivityFilter],
        [ImpressionCountFilter]             AS [impressionCountFilter],
        [OfferClicksFilter]                 AS [offerClicksFilter],
        [Unauthenticated]                   AS [unauthenticated],
        [OmniChannelPush]                   AS [omniChannelPush],
        [OmniChannelSms]                    AS [omniChannelSms],
        [OmniChannelEmail]                  AS [omniChannelEmail],
        [UserGroupFilter]                   AS [userGroupFilter],
        [StoreZipCodesFilter]               AS [storeZipCodesFilter],
        [ZoneRuleFilter]                    AS [zoneRuleFilter],
        [UserInputFilter]                   AS [userInputFilter],
        [BarCodeTypeUpcA]                   AS [barCodeTypeUpcA],
        [BarCodeType128]                    AS [barCodeType128],
        [BarCodeTypeLoyaltyId]              AS [barCodeTypeLoyaltyId],
        [BarCodeTypeQRCode]                 AS [barCodeTypeQRCode]
    FROM
        [Offer].[OfferOptions]
    WHERE
        TenantID = @TenantID
    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
END
