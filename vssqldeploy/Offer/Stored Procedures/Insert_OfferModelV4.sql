﻿
CREATE   PROCEDURE [Offer].[Insert_OfferModelV4] (
    @Offer              [Offer].[OfferUpsertSqlModelV4Type]         READONLY,
    @MaxRedemption      [Offer].[MaxRedemptionSqlModelType]         READONLY,
    @Link               [Offer].[OfferModelV4LinkType]              READONLY,
    @RuleFilter         [Offer].[OfferModelV4RuleFilterType]        READONLY,
    @ProductReward      [Offer].[OfferModelV4ProductRewardType]     READONLY,
--  @InterstitialZone   [Offer].[OfferModelV4InterstitialZoneType]  READONLY,
    @AppTenant          [Offer].[OfferModelV4AppTenantType]         READONLY,
    @Category           [Offer].[OfferModelV4CategoryType]          READONLY,
    @Campaign           [Offer].[OfferModelV4CampaignType]          READONLY,
    @ProductDiscount    [Offer].[ProductDiscountSqlModelType]       READONLY
) AS
BEGIN
    SET NOCOUNT ON
    --SET XACT_ABORT ON

    IF EXISTS (SELECT * FROM [Offer].[Offer] db INNER JOIN @Offer v ON db.[OfferID] = v.[OfferID])
    BEGIN
        ;THROW 50003, 'Attempting to insert duplicate record', 1;
    END

BEGIN TRY
    DECLARE @MaxUserRedemptionID uniqueidentifier, @MaxOfferRedemptionID uniqueidentifier;

    BEGIN TRANSACTION

    INSERT INTO [Offer].[Offer]
        ([OfferID], [AdvertOfferID], [AutoUpdateAdvert], [OfferTemplateID], [TenantID], [OfferType], [RewardType], [TargetUsersType], [EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order], [IsFeaturedOffer], [IsDeleted], [IsActive], [IsDraft], [IsRestricted], [IsDisplay], [IsDisplayTransactionProgress], [IsBrandedOffer], [Display], [StartDate], [EndDate], [BarCode], [BarCodeType], [PunchCardDisplayType], [MaxPunchAmount], [CreatedOn], [CreatedBy], [CreatedByUserID], [UpdatedOn], [UpdatedBy], [UpdatedByUserID])
    SELECT
         [OfferID], [AdvertOfferID], [AutoUpdateAdvert], [OfferTemplateID], [TenantID], [OfferType], [RewardType], [TargetUsersType], [EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order], [IsFeaturedOffer], [IsDeleted], [IsActive], [IsDraft], [IsRestricted], [IsDisplay], [IsDisplayTransactionProgress], [IsBrandedOffer], [Display], [StartDate], [EndDate], [BarCode], [BarCodeType], [PunchCardDisplayType], [MaxPunchAmount], [CreatedOn], [CreatedBy], [CreatedByUserID], [UpdatedOn], [UpdatedBy], [UpdatedByUserID]
    FROM @Offer

    IF EXISTS (SELECT * FROM @MaxRedemption)
    BEGIN
        INSERT INTO [Offer].[MaxRedemption]
            ([MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears], [IsActive])
        SELECT
             [MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears], 1 AS [IsActive]
        FROM @MaxRedemption

        INSERT INTO [Offer].[LkOffer_MaxRedemption]
            (OfferID, MaxRedemptionID, IsActive)
        SELECT
            [OfferID], [MaxRedemptionID], 1 AS [IsActive]
        FROM @MaxRedemption
    END

    IF EXISTS (SELECT * FROM @Link)
    BEGIN
        INSERT INTO [Offer].[Link]
            (LinkID, LinkText, LinkType, [Url], IsActive)
        SELECT
            [LinkID], [LinkText], [LinkType], [Url], 1 AS [IsActive]
        FROM @Link AS L

        INSERT INTO [Offer].[LkOffer_Link] (OfferID, LinkID, IsActive)
            SELECT OfferID, LinkID, IsActive FROM @Link
    END

    IF EXISTS (SELECT * FROM @RuleFilter)
    BEGIN
        INSERT INTO [Offer].[LkOffer_RuleFilter]
            ([OfferID], [RuleFilterID], [IsActive], [CreatedOn])
        SELECT
            [OfferID], [RuleFilterID], 1 AS [IsActive], [CreatedOn]
        FROM @RuleFilter
    END

    --IF EXISTS (SELECT * FROM @ProductReward)
    --BEGIN
    --    INSERT INTO [Offer].[ProductReward]
    --        ([OfferID], [ProductRewardID], [IsActive], [CreatedOn])
    --    SELECT
    --        [OfferID], [ProductRewardID], 1 AS [IsActive], [CreatedOn]
    --    FROM @ProductReward
    --END

    --IF EXISTS (SELECT * FROM @InterstitialZone)
    --BEGIN
    --    INSERT INTO [Offer].[LkOffer_InterstitialZone]
    --        ([OfferID], [InterstitialZoneID], [IsActive], [CreatedOn])
    --    SELECT
    --        [OfferID], [InterstitialZoneID], 1 AS [IsActive], [CreatedOn]
    --    FROM @InterstitialZone
    --END

    IF EXISTS (SELECT * FROM @AppTenant)
    BEGIN
        INSERT INTO [Offer].[LkOffer_AppTenant]
            ([OfferID], [AppTenantID], [IsActive], [CreatedOn])
        SELECT
            [OfferID], [AppTenantID], 1 AS [IsActive], [CreatedOn]
        FROM @AppTenant
    END

    IF EXISTS (SELECT * FROM @Category)
    BEGIN
        INSERT INTO [Offer].[LkOffer_Category]
            ([OfferID], [OfferCategoryID], [IsActive], [CreatedOn])
        SELECT
            [OfferID], [OfferCategoryID], 1 AS [IsActive], [CreatedOn]
        FROM @Category
    END

    IF EXISTS (SELECT * FROM @Campaign)
    BEGIN
        INSERT INTO [Offer].[LkOffer_Campaign]
            ([OfferID], [CampaignID], [IsActive], [CreatedOn])
        SELECT
            [OfferID], [CampaignID], 1 AS [IsActive], [CreatedOn]
        FROM @Campaign
    END

    IF EXISTS (SELECT * FROM @ProductDiscount)
    BEGIN
        INSERT INTO [Offer].[ProductDiscount] ([ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], [IsActive], IsStackable)
            SELECT                             [ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], 1         , 0
            FROM @ProductDiscount
        INSERT INTO [Offer].[DiscountLimit] ([ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], [IsActive], [CreatedOn])
            SELECT                           [ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], 1         , [CreatedOn]
            FROM @ProductDiscount
        INSERT INTO [Offer].[LkOffer_ProductDiscount] ([ProductDiscountID], [OfferID])
            SELECT [ProductDiscountID], [OfferID] FROM @ProductDiscount
    END

    COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT
        (SELECT * FROM @Offer FOR JSON PATH)[Offer]
        ,(SELECT * FROM @MaxRedemption FOR JSON PATH )[MaxRedemption]
        ,(SELECT * FROM @Link FOR JSON PATH )[Link]
        ,(SELECT * FROM @RuleFilter FOR JSON PATH)[RuleFilter]
        ,(SELECT * FROM @ProductReward FOR JSON PATH)[ProductReward]
      --,(SELECT * FROM @InterstitialZone FOR JSON PATH)[InterstitialZone]
        ,(SELECT * FROM @AppTenant FOR JSON PATH)[AppTenant]
        ,(SELECT * FROM @Category FOR JSON PATH)[Category]
        ,(SELECT * FROM @Campaign FOR JSON PATH)[Campaign]
    FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
