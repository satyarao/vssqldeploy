﻿CREATE   PROCEDURE [Offer].[GET_TenantPromos] @TenantID uniqueidentifier
AS
BEGIN
    SELECT
            ISNULL((SELECT TOP(1) extO.[ExternalOfferId] FROM [Offer].[ExternalOffer] extO WHERE extO.[ReferenceId] = ofr.RewardID), ofr.[OfferID]) AS [OfferID],
            ofr.[TenantID] AS [TenantID],
            ofr.[OfferType] AS [OfferType],
            ofr.[TargetUsersType] AS [TargetUsersType],
            ofr.[Title] AS [Title],
            ofr.[Subtitle] AS [Subtitle],
            ofr.[Description] AS [Description],
            ofr.[ImageUrl] AS [ImageURL],
            ofr.[ImagePos] AS [ImagePos],
            ofr.[StartDate] AS [StartDate],
            ofr.[EndDate] AS [EndDate],
            ofr.[Order] AS [Order],
            (
                SELECT [RuleFilterID] AS [RuleFilterId]
                FROM [Offer].[LkOffer_RuleFilter] WHERE [OfferID] = ofr.[OfferID]
                FOR JSON PATH
            ) AS [RuleFilters],
            (
                SELECT lkPd.[ProductDiscountID] AS [ProductDiscountId]
                    ,pd.[CouponPosId] AS [couponPosId]
                FROM [Offer].[LkOffer_ProductDiscount] AS lkPd
                INNER JOIN [Offer].[ProductDiscount] pd
                    ON lkPd.[ProductDiscountID] = pd.[ProductDiscountID]
                WHERE lkPd.[OfferID] = ofr.[OfferID]
                FOR JSON PATH
            ) AS [ProductRewards],
            (
                SELECT [LinkText] AS [linkText]
                    ,[LinkType] AS [linkType]
                    ,[Url] AS [url]
                FROM (SELECT * FROM [Offer].[LkOffer_Link] WHERE [OfferID] = ofr.[OfferID]) lkLink
                INNER JOIN [Offer].[Link] link ON lkLink.[LinkID] = link.[LinkID]
                FOR JSON PATH
            ) AS [Links],
            (
                SELECT TOP(1) pd.[IsStackable] AS [isStackable]
                FROM [Offer].[LkOffer_ProductDiscount] AS lkPd
                INNER JOIN [Offer].[ProductDiscount] pd
                    ON lkPd.[ProductDiscountID] = pd.[ProductDiscountID]
                WHERE lkPd.[OfferID] = ofr.[OfferID]
                FOR JSON PATH
            ) AS [IsStackable],
            --CASE
            --  WHEN EXISTS(SELECT * FROM [Offer].[DiscountStackingGroup] dsg WITH(NOLOCK) INNER JOIN [Offer].[LkOffer_ProductDiscount] lkPD on ofr.[OfferID] = lkPD.[OfferID] INNER JOIN [Offer].[ProductDiscount] pd ON pd.[ProductDiscountID] = lkPD.[ProductDiscountID] INNER JOIN [Offer].[LkDiscountStackingGroup] lkDsg ON lkDsg.[DiscountStackingGroupID] = dsg.[DiscountStackingGroupID] WHERE lkDsg.[ProductDiscountID] = pd.[ProductDiscountID] AND LOWER([DiscountStackingType]) = 'stack')
            --      THEN CAST(1 AS bit)
            --  ELSE
            --      CAST(0 AS bit)
            --END AS [IsStackable],
            ofr.[RewardID] AS [ExternalReferenceId],
            ofr.[IsFeaturedOffer] AS [IsFeaturedOffer],
            ofr.[IsActive] AS [IsActive],
            ofr.[IsRestricted] AS [IsRestricted],
            (
                SELECT [CategoryName] AS [CategoryName]
                FROM (SELECT * FROM [Offer].[LkOffer_Category] WHERE [OfferID] = ofr.[OfferID]) lkCat
                INNER JOIN [Offer].[OfferCategory] cat ON lkCat.[OfferCategoryID] = cat.[OfferCategoryID]
                FOR JSON PATH
            ) AS [Categories],
            ofr.[IsDisplay] AS [IsDisplay],
            (
                SELECT [InterstitialZoneID] as ID, [ZoneInfoID] as [ZoneInfoId], [ZoneName], [MaxOffersDisplayCount], [ImageUrl], [CardDisplayType],
                [Headline], [Description], [ContentPosition], [LinkType], [LinkType2], [CtaText], [CtaText2], [Url], [Url2],
                [BimLinkType], [ExpirationDate]
                FROM [ContentPush].[InterstitialZone]
                WHERE OfferID = ofr.OfferID
                AND (ExpirationDate IS NULL OR [ExpirationDate] > GETUTCDATE())
                AND [IsDeleted] = 0
                AND [IsActive] = 1
                FOR JSON PATH
            ) AS [InterstitialZoneContents]
        FROM [Offer].[ActiveOffers] AS orf WITH(NOLOCK)
        INNER JOIN [Offer].[Offer] ofr WITH(NOLOCK)
            ON ofr.[OfferID] = orf.[OfferID]
        LEFT JOIN [Offer].[ExternalOffer] eo
            ON eo.[ExternalOfferId] = ofr.[OfferID]
        WHERE
            ofr.[Display] = 'promotion' AND ofr.[TenantID] = @TenantID
        FOR JSON PATH
END

