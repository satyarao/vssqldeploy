﻿

/****************************************REGISTER EXTERNAL OFFER****************************************/
CREATE   PROCEDURE [Offer].[RegisterExternalOffer]
(
	@ReferenceId nvarchar(50),
	@TenantId uniqueidentifier,
    @Description nvarchar(max),
    @AuthorId nvarchar(50),
    @StartDate datetime2(7),
    @EndDate datetime2(7),
    @IsConfigured bit,
    @IsDeleted bit,
    @CreatedOn datetime2(7),
    @DeletedOn datetime2(7),
    @ConfiguredOn datetime2(7)
)
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY
	BEGIN TRANSACTION
	
		IF NOT EXISTS(SELECT [ExternalOfferId] FROM [Offer].[ExternalOffer] WHERE ([ReferenceID] = @ReferenceId AND [TenantID] = @TenantId))
			BEGIN
	
				INSERT INTO [Offer].[ExternalOffer]
				   ([ExternalOfferID],[ReferenceID],[TenantID],[Description],[AuthorID],[StartDate],[EndDate],[IsConfigured],[IsDeleted],[CreatedOn],[DeletedOn],[ConfiguredOn])
				   OUTPUT INSERTED.[ExternalOfferId]
				VALUES
				   (NEWID(),@ReferenceId,@TenantId,@Description,@AuthorId,@StartDate,@EndDate,@IsConfigured,@IsDeleted,@CreatedOn,@DeletedOn,@ConfiguredOn)
				   
			END
		ELSE
			BEGIN
				;THROW 50001, 'Offer already exists', 1;
			END

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT @ReferenceId AS [ReferenceID],
		@TenantId AS [TenantID],
		@Description AS [Description],
		@AuthorId AS [AuthorID],
		@StartDate AS [StartDate],
		@EndDate AS [EndDate],
		@IsConfigured AS [IsConfigured],
		@IsDeleted AS [IsDeleted],
		@CreatedOn AS [CreatedOn],
		@DeletedOn AS [DeletedOn],
		@ConfiguredOn AS [ConfiguredOn]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
