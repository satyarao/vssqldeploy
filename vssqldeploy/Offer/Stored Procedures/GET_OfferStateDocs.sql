﻿
CREATE   PROCEDURE [Offer].[GET_OfferStateDocs]
    @OfferIds nvarchar(max)
AS
SELECT
    OfferId     AS [offer_id]
    --, [redemption_count] = (SELECT COUNT(*) FROM [Offer].[OfferRedemption] WHERE [OfferID] = OFR.OfferID)
    , [redemption_count] = 0
    , CreatedOn AS [created_on]
FROM
    Offer.Offer AS OFR
WHERE
    OfferID IN (SELECT CAST([value] AS uniqueidentifier) FROM OPENJSON(@OfferIds))
FOR JSON PATH
