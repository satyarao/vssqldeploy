﻿
CREATE   PROCEDURE [Offer].[GET_Offers] (
    @OfferIDs               varchar(max)        = NULL, -- JSON array of GUIDs
    @TenantID               uniqueidentifier    = NULL,
    @SearchTerm             nvarchar(255)       = NULL,
    @OfferType              nvarchar(255)       = NULL,
    @CampaignID             int                 = NULL,
    @StartDate              datetime2(7)        = NULL,
    @EndDate                datetime2(7)        = NULL,
    @CategoryID             uniqueidentifier    = NULL,
    @IsDraft                bit                 = NULL,
    @IsDisplay              bit                 = NULL,
    @IsDeleted              bit                 = NULL,
    @PageNumber             int                 = NULL,
    @PageSize               int                 = NULL,
    @IncludePartnerOffers   bit                 = 0,
    @AudienceCoveredCount   int                 = NULL,
    @SitesCoveredCount      int                 = NULL
) AS
BEGIN
    IF @SearchTerm IS NOT NULL SET @SearchTerm = '%' + @SearchTerm + '%'
    IF @PageNumber IS NULL SET @PageNumber = 1
    IF @PageSize IS NULL OR @PageSize < 1
    BEGIN
        SET @PageSize = (SELECT COUNT(*) FROM [Offer].[Offer])
        IF @PageSize = 0 SET @PageSize = 1
    END

    SELECT
        OfferID                 AS [id]
        , AdvertOfferID         AS [AdvertOfferID]
        , OfferTemplateID       AS [OfferTemplateID]
        , TenantID              AS [tenantID]
        , TenantName            AS [tenantName]
        , SourceTenantName      AS [sourceTenantName]
        , OfferType             AS [OfferType]
        , RewardType            AS [RewardType]
        , TargetUsersType       AS [TargetUsersType]
        , EditorTitle           AS [EditorTitle]
        , EditorDescription     AS [EditorDescription]
        , Title                 AS [Title]
        , Subtitle              AS [Subtitle]
        , [Description]         AS [Description]
        , ImageURL              AS [ImageURL]
        , ImagePos              AS [ImagePos]
        , [Order]               AS [Order]
        , Display               AS [Display]
        , StartDate             AS [StartDate]
        , EndDate               AS [EndDate]
        , IsFeaturedOffer       AS [IsFeaturedOffer]
        , IsDeleted             AS [IsDeleted]
        , IsActive              AS [IsActive]
        , IsDraft               AS [IsDraft]
        , IsRestricted          AS [IsRestricted]
        , IsDisplay             AS [IsDisplay]
        , IsBrandedOffer        AS [IsBrandedOffer]
        , BarCode               AS [BarCode]
        , BarCodeType           AS [BarCodeType]
        , PunchCardDisplayType  AS [PunchCardDisplayType]
        , MaxPunchAmount        AS [MaxPunchAmount]
        , MaxRedemptionAmount   AS [MaxRedemptionAmount]
        , MaxRedemption         AS [MaxRedemption]
        , [MaxRedemptions]        AS [MaxRedemptions]
        , AudienceCoveredCount  AS [AudienceCoveredCount]
        , SitesCoveredCount     AS [SitesCoveredCount]
        , RuleFilters           AS [RuleFilters]
        , ProductRewards        AS [ProductRewards]
        , InterstitialZones     AS [InterstitialZones]
        , Links                 AS [Links]
        , Categories            AS [Categories]
        , Campaigns             AS [Campaigns]
        , AppTenants            AS [AppTenants]
        , ProductDiscounts      AS [ProductDiscounts]
        , CreatedOn             AS [Meta.createdOn]
        , CreatedBy             AS [Meta.createdBy]
        , CreatedByUserID       AS [Meta.createdByUserID]
        , UpdatedOn             AS [Meta.updatedOn]
        , UpdatedBy             AS [Meta.updatedBy]
        , UpdatedByUserID       AS [Meta.updatedByUserID]
    FROM
        [Offer].[vGET_Offers] OFR
    WHERE
        (IsDeleted = @IsDeleted OR @IsDeleted IS NULL)
        AND (IsDraft = @IsDraft OR @IsDraft IS NULL)
        AND (IsDisplay = @IsDisplay OR @IsDisplay IS NULL)
        AND
        (
            (@IncludePartnerOffers = 1 AND @TenantId IS NOT NULL AND TenantID IN (
                -- Select partner tenant IDs
                SELECT t.[TenantID]
                FROM [dbo].[Tenant] t
                WHERE t.[TenantID] IN
                (
                    SELECT [TenantID] FROM [dbo].[FlatStore] WHERE [OwnerID] = @TenantID
                    UNION
                    SELECT [OwnerID] FROM [dbo].[FlatStore] WHERE [TenantID] = @TenantID
                    UNION
                    SELECT @TenantID
                )
            ))
            OR (@IncludePartnerOffers = 1 AND @TenantId IS NULL)
            OR (@IncludePartnerOffers = 0 AND @TenantId IS NOT NULL AND TenantID = @TenantId)
            OR (@IncludePartnerOffers = 0 AND @TenantId IS NULL)
        )
        AND (@CampaignID IS NULL OR EXISTS (SELECT * FROM [Offer].[LkOffer_Campaign] WHERE OfferID = OFR.OfferID AND CampaignID = @CampaignID))
        AND (OfferType = @OfferType COLLATE Latin1_General_CI_AI OR @OfferType IS NULL)
        AND (@OfferIds IS NULL OR (@OfferIds IS NOT NULL AND OFR.OfferID IN (SELECT CAST(value AS uniqueidentifier) FROM OPENJSON(@OfferIDs))))
        AND (@CategoryID IS NULL OR EXISTS (SELECT * FROM [Offer].[LkOffer_Category] WHERE OfferID = OFR.OfferID AND OfferCategoryID = @CategoryID))
        AND ((Title LIKE @SearchTerm OR Subtitle LIKE @SearchTerm OR EditorTitle LIKE @SearchTerm OR EditorDescription LIKE @SearchTerm) OR @SearchTerm IS NULL)
        AND (
            @StartDate IS NULL
            --OR (StartDate IS NOT NULL AND EndDate IS NOT NULL AND StartDate >= @StartDate AND EndDate > @StartDate)
            --OR (StartDate IS NULL AND EndDate IS NOT NULL AND EndDate > @StartDate)
            --OR (StartDate IS NOT NULL AND EndDate IS NULL AND EndDate > @StartDate)
            OR (ISNULL(StartDate, '0001-01-01') >= @StartDate AND ISNULL(EndDate, '9999-12-31') > @StartDate)
        )
        AND (
            @EndDate IS NULL
            OR (@EndDate IS NOT NULL AND EndDate IS NULL)
            --OR (StartDate IS NOT NULL AND EndDate IS NOT NULL AND EndDate <= @EndDate AND StartDate < @EndDate)
            --OR (StartDate IS NULL AND EndDate IS NOT NULL AND EndDate <= @EndDate)
            --OR (StartDate IS NOT NULL AND EndDate IS NULL AND StartDate < @EndDate)
            OR (ISNULL(EndDate, '9999-12-31') <= @EndDate AND ISNULL(StartDate, '0001-01-01') < @EndDate)
        )
         AND (AudienceCoveredCount = @AudienceCoveredCount OR @AudienceCoveredCount IS NULL)
         AND (SitesCoveredCount = @SitesCoveredCount OR @SitesCoveredCount IS NULL)
    ORDER BY [Order]
    OFFSET (@PageNumber-1)*@PageSize ROWS
    FETCH NEXT @PageSize ROWS ONLY
    FOR JSON PATH
END
