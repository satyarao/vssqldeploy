﻿
CREATE   PROCEDURE [Offer].[DraftOffer]
    @AdvertOfferID      uniqueidentifier,
    @TenantID           uniqueidentifier, -- TenantId for new offer
    @AutoUpdateAdvert   bit,
    @OfferID_out        uniqueidentifier = NULL OUTPUT
AS
    IF NOT EXISTS (SELECT * FROM [Offer].[AdvertOffer] WHERE [AdvertOfferID] = @AdvertOfferID AND [IsDeleted] = 0)
    BEGIN
        ;THROW 50002, 'Must include a valid AdvertOfferID', 1;
    END

    IF EXISTS (SELECT * FROM [Offer].[Offer] WHERE [AdvertOfferID] = @AdvertOfferID AND [TenantID] = @TenantID AND [IsDeleted] = 0)
    BEGIN
        -- Offer cannot be drafted twice for same tenant
        ;THROW 50008, 'It is not allowed to draft one AdvertOffer twice for same tenant', 1;
        RETURN
    END

    --SET NOCOUNT ON
BEGIN TRY
    BEGIN TRANSACTION

    DECLARE @Ids TABLE (Id uniqueidentifier PRIMARY KEY)
    DECLARE @OfferID uniqueidentifier
    SET @OfferID = NEWID()

    INSERT INTO [Offer].[Offer]
        ([OfferID], [AdvertOfferID], [AutoUpdateAdvert], [OfferTemplateID], [TenantID], [OfferType], [RewardType], [TargetUsersType], [EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order], [IsFeaturedOffer], [IsDeleted], [IsActive], [IsDraft], [IsRestricted], [IsDisplay], [IsBrandedOffer], [Display], [StartDate], [EndDate], [CreatedOn], [CreatedBy], [CreatedByUserID])
    SELECT
        @OfferID  , @AdvertOfferID , @AutoUpdateAdvert , [OfferTemplateID], @TenantID , [OfferType], [RewardType], [TargetUsersType], [EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order], [IsFeaturedOffer],           0,          0,         1, [IsRestricted], [IsDisplay], [IsBrandedOffer], [Display], [StartDate], [EndDate], [CreatedOn], [CreatedBy], [CreatedByUserID]
    FROM
        [Offer].[AdvertOffer]
    WHERE
        [AdvertOfferID] = @AdvertOfferID
        AND [IsDeleted] = 0

    BEGIN
        INSERT INTO [Offer].[LkOffer_Campaign]
            ([CampaignID], [OfferID])
        SELECT
            [CampaignID], @OfferID
        FROM
            [Offer].[LkAdvertOffer_Campaign]
        WHERE
            [AdvertOfferID] = @AdvertOfferID
            AND IsActive=1
    END

    -- We don't need to copy category???
    --BEGIN
        --INSERT INTO [Offer].[OfferCategory]
        --    ([OfferCategoryID], [CategoryName], [TenantID])
        --OUTPUT inserted.[OfferCategoryID] INTO @Ids
        --SELECT
        --    NEWID() AS [OfferCategoryID], [CategoryName], [TenantID]
        --FROM [Offer].[OfferCategory]

        --INSERT INTO [Offer].[LkOffer_Category]
        --    ([OfferCategoryID], [OfferID])
        --SELECT
        --    Id AS [OfferCategoryID], @OfferID
        --FROM
        --    @Ids

        --DELETE @Ids

        --INSERT INTO [Offer].[LkOffer_Category]
        --    ([OfferCategoryID], [OfferID])
        --SELECT
        --    [OfferCategoryID], @OfferID
        --FROM
        --    [Offer].[LkAdvertOffer_Category]
        --WHERE
        --    [AdvertOfferID] = @AdvertOfferID
        --    AND IsActive = 1
    --END

    BEGIN
        --INSERT INTO [Offer].[Link]
        --    ([LinkID], [LinkText], [LinkType], [Url])
        --OUTPUT inserted.[LinkID] INTO @Ids
        --SELECT
        --    NEWID() AS [LinkID], [LinkText], [LinkType], [Url]
        --FROM [Offer].[Link]

        --INSERT INTO [Offer].[LkOffer_Link]
        --    ([LinkID], [OfferID])
        --SELECT
        --    Id AS [LinkID], @OfferID
        --FROM @Ids

        --DELETE @Ids

        INSERT INTO [Offer].[LkOffer_Link]
            ([LinkID], [OfferID])
        SELECT
            [LinkID], @OfferID
        FROM
            [Offer].[LkAdvertOffer_Link]
        WHERE
            [AdvertOfferID] = @AdvertOfferID
            AND IsActive=1
    END

    BEGIN
        --INSERT INTO [Offer].[MaxRedemption]
        --    ([MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears])
        --OUTPUT inserted.[MaxRedemptionID] INTO @Ids
        --SELECT
        --    NEWID() AS [MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears]
        --FROM [Offer].[MaxRedemption]

        --INSERT INTO [Offer].[LkOffer_MaxRedemption]
        --    ([MaxRedemptionID], [OfferID])
        --SELECT
        --    Id AS [MaxRedemptionID], @OfferID
        --FROM @Ids

        --DELETE @Ids

        INSERT INTO [Offer].[LkOffer_MaxRedemption]
            ([MaxRedemptionID], [OfferID])
        SELECT
            [MaxRedemptionID], @OfferID
        FROM
            [Offer].[LkAdvertOffer_MaxRedemption]
        WHERE
            [AdvertOfferID] = @AdvertOfferID
            AND IsActive = 1
    END

    BEGIN
        --DECLARE @ProductDiscount_n_DiscountLimit TABLE
        --(
        --    [OldProductDiscountID]      [uniqueidentifier]  NOT NULL,
        --    [NewProductDiscountID]      [uniqueidentifier]  NOT NULL,

        --    [ProductRewardID]           [uniqueidentifier]  NULL,
        --    [StandardProductGroupID]    [uniqueidentifier]  NOT NULL,
        --    [DiscountType]              [nvarchar](50)      NULL,
        --    [DiscountValue]             [decimal](7, 3)     NOT NULL,
        --    [DiscountCurrency]          [nvarchar](50)      NOT NULL,
        --    [NewUnitPrice]              [decimal](7, 3)     NULL,
        --    [DiscountLimitType]         [nvarchar](20)      NOT NULL,
        --    [MinimumQuantity]           [decimal](6, 3)     NULL,
        --    [MaximumQuantity]           [decimal](6, 3)     NULL
        --)

        --INSERT INTO @ProductDiscount_n_DiscountLimit (
        --    [OldProductDiscountID],
        --    [NewProductDiscountID],
        --    [ProductRewardID],
        --    [StandardProductGroupID],
        --    [DiscountType],
        --    [DiscountValue],
        --    [DiscountCurrency],
        --    [NewUnitPrice],
        --    [DiscountLimitType],
        --    [MinimumQuantity],
        --    [MaximumQuantity]
        --)
        --SELECT
        --    PD.[ProductDiscountID],
        --    NEWID() AS [NewProductDiscountID],
        --    [ProductRewardID],
        --    [StandardProductGroupID],
        --    [DiscountType],
        --    [DiscountValue],
        --    [DiscountCurrency],
        --    [NewUnitPrice],
        --    [DiscountLimitType],
        --    [MinimumQuantity],
        --    [MaximumQuantity]
        --FROM
        --    [Offer].[ProductDiscount] PD
        --    JOIN [Offer].[DiscountLimit] DL ON PD.[ProductDiscountID] = DL.[ProductDiscountID]

        --INSERT INTO [Offer].[ProductDiscount]
        --    ([ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], IsStackable)
        --SELECT
        --    [NewProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], 0
        --FROM @ProductDiscount_n_DiscountLimit

        --INSERT INTO [Offer].[DiscountLimit]
        --    ([ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity])
        --SELECT
        --    [NewProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity]
        --FROM @ProductDiscount_n_DiscountLimit

        --INSERT INTO [Offer].[LkOffer_ProductDiscount]
        --    ([ProductDiscountID], [OfferID])
        --SELECT
        --    [NewProductDiscountID], @OfferID
        --FROM @ProductDiscount_n_DiscountLimit

        --DELETE @ProductDiscount_n_DiscountLimit

        INSERT INTO [Offer].[LkOffer_ProductDiscount]
            ([ProductDiscountID], [OfferID])
        SELECT
            [ProductDiscountID], @OfferID
        FROM
            [Offer].[LkAdvertOffer_ProductDiscount]
        WHERE
            [AdvertOfferID] = @AdvertOfferID
            AND IsActive = 1
    END

    BEGIN
        INSERT INTO [Offer].[LkOffer_RuleFilter]
            ([RuleFilterID], [OfferID])
        SELECT
            [RuleFilterID], @OfferID
        FROM
            [Offer].[LkAdvertOffer_RuleFilter]
        WHERE
            [AdvertOfferID] = @AdvertOfferID
            AND IsActive = 1
    END

    COMMIT TRANSACTION

    SET @OfferID_out = @OfferID
    RETURN;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION;

    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT
        AdvertOfferID = @AdvertOfferID,
        TenantID = @TenantID,
        AutoUpdateAdvert = @AutoUpdateAdvert
    FOR JSON PATH) AS [StoredProcedureInput];
    EXEC [dbo].[PostStoredProcedureException] @Exceptions;

    THROW
END CATCH
