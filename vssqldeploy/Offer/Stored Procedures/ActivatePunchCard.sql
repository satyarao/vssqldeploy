﻿CREATE   PROCEDURE [Offer].[ActivatePunchCard]
    @UserID     uniqueidentifier,
    @OfferID    uniqueidentifier,
    @Activate   bit
AS
IF EXISTS (SELECT * FROM [Offer].[ActiveOffers] WHERE [OfferID] = @OfferID)
BEGIN
    DECLARE @CurrentPunchAmount int = (SELECT CurrentPunchAmount FROM [Offer].[PunchCardAmount] WHERE UserID = @UserID AND OfferID = @OfferID)
    DECLARE @MaxPunchAmount int = (SELECT MaxPunchAmount FROM [Offer].[Offer] WHERE OfferID = @OfferID)
    
    IF @CurrentPunchAmount = @MaxPunchAmount
    BEGIN
        UPDATE [Offer].[PunchCardAmount]
        SET IsActivated = @Activate
        WHERE
            UserID = @UserID
            AND OfferID = @OfferID
    END
END
