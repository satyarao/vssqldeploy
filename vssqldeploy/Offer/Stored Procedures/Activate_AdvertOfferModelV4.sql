﻿
CREATE   PROCEDURE [Offer].[Activate_AdvertOfferModelV4] (
    @AdvertOfferID  uniqueidentifier,
    @TenantID       uniqueidentifier,
    @IsActive       bit
)
AS
BEGIN
    IF NOT EXISTS (SELECT * FROM [Offer].[AdvertOffer] WHERE [AdvertOfferID] = @AdvertOfferID AND TenantID = @TenantID)
    BEGIN
        ;THROW 50004, '@AdvertOfferID and @TenantID do not match', 1;
    END

BEGIN TRY

    BEGIN TRANSACTION

    UPDATE [Offer].[AdvertOffer]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[Offer]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_Campaign]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_Category]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_Link]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_MaxRedemption]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_ProductDiscount]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_RuleFilter]
    SET [IsActive] = @IsActive
    WHERE [AdvertOfferID] = @AdvertOfferID

    COMMIT TRANSACTION;
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
        SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), CONVERT(nvarchar(36),@AdvertOfferID) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
