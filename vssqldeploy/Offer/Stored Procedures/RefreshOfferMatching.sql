﻿CREATE   PROCEDURE [Offer].[RefreshOfferMatching] ( @RefreshStore bit = 1, @RefreshUser bit = 1 )
AS
BEGIN

	DECLARE @OfferIds AS TABLE (
		[OfferID]	uniqueidentifier NOT NULL
	);

	INSERT INTO @OfferIds
		([OfferID])
	SELECT		[OfferID] 
	FROM		[Offer].[Offer]
	WHERE		[IsDeleted] = 0
	ORDER BY	[TenantId]


	WHILE (SELECT COUNT([OfferID]) FROM @OfferIds) > 0
	BEGIN
		DECLARE @localOfferId uniqueidentifier = (SELECT TOP(1) [OfferID] FROM @OfferIds);

		BEGIN TRY
			IF (@RefreshStore = 1)
			BEGIN
				EXEC [Offer].[Refresh_StoreOfferMatching] NULL, @localOfferId
			END
			IF (@RefreshUser = 1)
			BEGIN
				EXEC [Offer].[Refresh_UserOfferMatching] NULL, @localOfferId
			END
		END TRY
		BEGIN CATCH
			
		END CATCH

		DELETE FROM @OfferIds WHERE [OfferID] = @localOfferId;
	END;

END
