﻿


/****************************************UPDATE EXTERNAL OFFER****************************************/
CREATE   PROCEDURE [Offer].[UpdateExternalOffer]
(
	@ExternalOfferId uniqueidentifier,
	@ReferenceId nvarchar(50),
	@TenantId uniqueidentifier,
    @Description nvarchar(max),
    @AuthorId nvarchar(50),
    @StartDate datetime2(7),
    @EndDate datetime2(7),
    @IsConfigured bit,
    @IsDeleted bit,
    @CreatedOn datetime2(7),
    @DeletedOn datetime2(7),
    @ConfiguredOn datetime2(7)
)
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY
	BEGIN TRANSACTION

		IF EXISTS(SELECT [ExternalOfferId] FROM [Offer].[ExternalOffer] WHERE ([ExternalOfferID] = @ExternalOfferId AND [ReferenceID] = @ReferenceId AND [TenantID] = @TenantId))
			BEGIN
				UPDATE [Offer].[ExternalOffer]
				   SET [ReferenceID] = @ReferenceId
					  ,[Description] = @Description
					  ,[AuthorID] = @AuthorId
					  ,[StartDate] = @StartDate
					  ,[EndDate] = @EndDate
					  ,[IsConfigured] = @IsConfigured
					  ,[IsDeleted] = @IsDeleted
					  ,[CreatedOn] = @CreatedOn
					  ,[DeletedOn] = @DeletedOn
					  ,[ConfiguredOn] = @ConfiguredOn
					WHERE ([ExternalOfferID] = @ExternalOfferId AND [ReferenceID] = @ReferenceId AND [TenantID] = @TenantId)
				
			END
		ELSE
			BEGIN
				;THROW 50001, 'Offer not found', 1;
			END

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT @ExternalOfferId AS [ExternalOfferID],
		@ReferenceId AS [ReferenceID],
		@TenantId AS [TenantID],
		@Description AS [Description],
		@AuthorId AS [AuthorID],
		@StartDate AS [StartDate],
		@EndDate AS [EndDate],
		@IsConfigured AS [IsConfigured],
		@IsDeleted AS [IsDeleted],
		@CreatedOn AS [CreatedOn],
		@DeletedOn AS [DeletedOn],
		@ConfiguredOn AS [ConfiguredOn]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
