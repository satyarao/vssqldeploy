﻿
CREATE   PROCEDURE [Offer].[Upsert_ItemProductDiscount] (
	@ProductDiscountID uniqueidentifier,
	@StandardProductGroupID uniqueidentifier,
	@DiscountType nvarchar(50),
	@DiscountValue decimal(7,3),
	@DiscountCurrency nvarchar(10),
	@IsStackable bit,
	@MaxFuelAmount decimal(6,3) = null
) AS
BEGIN
	BEGIN TRY
		INSERT INTO [Offer].[ProductDiscount]
			([ProductDiscountID],[StandardProductGroupID],[DiscountType],[DiscountValue],[DiscountCurrency],[IsStackable])
		VALUES
			(@ProductDiscountID,@StandardProductGroupID,@DiscountType,@DiscountValue,@DiscountCurrency,@IsStackable)

		IF(@MaxFuelAmount IS NOT NULL AND @MaxFuelAmount > 0)
		BEGIN
			INSERT INTO [Offer].[DiscountLimit]
				([ProductDiscountID],[DiscountLimitType],[MaximumQuantity])
			VALUES
				(@ProductDiscountID, 'volume', @MaxFuelAmount)
		END

		SELECT (SELECT @ProductDiscountID AS [ProductDiscountID]
				FOR JSON PATH) AS [ProductDiscountIDJson]
		FOR JSON PATH
	END TRY
	BEGIN CATCH
		DECLARE @Exceptions [StoredProcedureExceptionType];
		INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
		SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
			(
				SELECT @ProductDiscountID 		  AS ProductDiscountID,
					   @StandardProductGroupID    AS StandardProductGroupID,
					   @DiscountType 	          AS DiscountType,
					   @DiscountValue 		      AS DiscountValue,
					   @DiscountCurrency          AS DiscountCurrency,
					   @IsStackable 	          AS IsStackable,
					   @MaxFuelAmount 	          AS MaxFuelAmount
				FOR JSON PATH
			)			AS [StoredProcedureInput];

		--ROLLBACK TRANSACTION;

		EXEC [dbo].[PostStoredProcedureException] @Exceptions;
	END CATCH
END
