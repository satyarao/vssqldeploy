﻿
CREATE   PROCEDURE [Offer].[PUT_OfferOptionsDocV4] (
    @json nvarchar(max)
)
AS
BEGIN
    SET NOCOUNT ON
BEGIN TRY
    BEGIN TRANSACTION

    DECLARE @OfferOptions AS [Offer].[OfferOptionsDocV4SqlModelType]
    INSERT INTO @OfferOptions
    SELECT * FROM OPENJSON(@json)
    WITH
    (
        [TenantID]                          [uniqueidentifier]  '$.tenantId',
        [OfferTypeFuel]                     [bit]               '$.offerTypeFuel',
        [OfferTypeProduct]                  [bit]               '$.offerTypeProduct',
        [TimeOfWeekFilter]                  [bit]               '$.timeOfWeekFilter',
        [TimeOfDayFilter]                   [bit]               '$.timeOfDayFilter',
        [ByDateFilter]                      [bit]               '$.byDateFilter',
        [UserCreatedOnFilter]               [bit]               '$.userCreatedOnFilter',
        [NewUserFilter]                     [bit]               '$.newUserFilter',
        [CompletedTransactionsFilter]       [bit]               '$.completedTransactionsFilter',
        [LastVisitDateFilter]               [bit]               '$.lastVisitDateFilter',
        [VisitCountToStoreFilter]           [bit]               '$.visitCountToStoreFilter',
        [ExactStoreFilter]                  [bit]               '$.exactStoreFilter',
        [StoreGroupFilter]                  [bit]               '$.storeGroupFilter',
        [DistributionChannelFilter]         [bit]               '$.distributionChannelFilter',
        [StoreGeolocationFilter]            [bit]               '$.storeGeolocationFilter',
        [StoreCityStateFilter]              [bit]               '$.storeCityStateFilter',
        [StoreStatesFilter]                 [bit]               '$.storeStatesFilter',
        [PaymentMethodFilter]               [bit]               '$.paymentMethodFilter',
        [FirstVisitToStore]                 [bit]               '$.firstVisitToStore',
        [PreviousPurchaseFilter]            [bit]               '$.previousPurchaseFilter',
        [PurchasedFuelFilter]               [bit]               '$.purchasedFuelFilter',
        [UnauthenticatedFilter]             [bit]               '$.unauthenticatedFilter',
        [DisplayTypeDiscount]               [bit]               '$.displayTypeDiscount',
        [DisplayTypePromo]                  [bit]               '$.displayTypePromo',
        [DisplayTypeCoupon]                 [bit]               '$.displayTypeCoupon',
        [TenantFilter]                      [bit]               '$.tenantFilter',
        [VisitCountToStoreActivityFilter]   [bit]               '$.visitCountToStoreActivityFilter',
        [ImpressionCountFilter]             [bit]               '$.impressionCountFilter',
        [OfferClicksFilter]                 [bit]               '$.offerClicksFilter',
        [Unauthenticated]                   [bit]               '$.unauthenticated',
        [OmniChannelPush]                   [bit]               '$.omniChannelPush',
        [OmniChannelSms]                    [bit]               '$.omniChannelSms',
        [OmniChannelEmail]                  [bit]               '$.omniChannelEmail',
        [UserGroupFilter]                   [bit]               '$.userGroupFilter',
        [StoreZipCodesFilter]               [bit]               '$.storeZipCodesFilter',
        [ZoneRuleFilter]                    [bit]               '$.zoneRuleFilter',
        [UserInputFilter]                   [bit]               '$.userInputFilter',
        [BarCodeTypeUpcA]                   [bit]               '$.barCodeTypeUpcA',
        [BarCodeType128]                    [bit]               '$.barCodeType128',
        [BarCodeTypeLoyaltyId]              [bit]               '$.barCodeTypeLoyaltyId',
        [BarCodeTypeQRCode]                 [bit]               '$.barCodeTypeQRCode'
    );

    MERGE INTO [Offer].[OfferOptions] AS target
    USING (
        SELECT * FROM @OfferOptions
    ) AS source
    ON target.TenantID = source.TenantID
    WHEN MATCHED THEN
        UPDATE SET
            target.[OfferTypeFuel]                   = source.[OfferTypeFuel],
            target.[OfferTypeProduct]                = source.[OfferTypeProduct],
            target.[TimeOfWeekFilter]                = source.[TimeOfWeekFilter],
            target.[TimeOfDayFilter]                 = source.[TimeOfDayFilter],
            target.[ByDateFilter]                    = source.[ByDateFilter],
            target.[UserCreatedOnFilter]             = source.[UserCreatedOnFilter],
            target.[NewUserFilter]                   = source.[NewUserFilter],
            target.[CompletedTransactionsFilter]     = source.[CompletedTransactionsFilter],
            target.[LastVisitDateFilter]             = source.[LastVisitDateFilter],
            target.[VisitCountToStoreFilter]         = source.[VisitCountToStoreFilter],
            target.[ExactStoreFilter]                = source.[ExactStoreFilter],
            target.[StoreGroupFilter]                = source.[StoreGroupFilter],
            target.[DistributionChannelFilter]       = source.[DistributionChannelFilter],
            target.[StoreGeolocationFilter]          = source.[StoreGeolocationFilter],
            target.[StoreCityStateFilter]            = source.[StoreCityStateFilter],
            target.[StoreStatesFilter]               = source.[StoreStatesFilter],
            target.[PaymentMethodFilter]             = source.[PaymentMethodFilter],
            target.[FirstVisitToStore]               = source.[FirstVisitToStore],
            target.[PreviousPurchaseFilter]          = source.[PreviousPurchaseFilter],
            target.[PurchasedFuelFilter]             = source.[PurchasedFuelFilter],
            target.[UnauthenticatedFilter]           = source.[UnauthenticatedFilter],
            target.[DisplayTypeDiscount]             = source.[DisplayTypeDiscount],
            target.[DisplayTypePromo]                = source.[DisplayTypePromo],
            target.[DisplayTypeCoupon]               = source.[DisplayTypeCoupon],
            target.[TenantFilter]                    = source.[TenantFilter],
            target.[VisitCountToStoreActivityFilter] = source.[VisitCountToStoreActivityFilter],
            target.[ImpressionCountFilter]           = source.[ImpressionCountFilter],
            target.[OfferClicksFilter]               = source.[OfferClicksFilter],
            target.[Unauthenticated]                 = source.[Unauthenticated],
            target.[OmniChannelPush]                 = source.[OmniChannelPush],
            target.[OmniChannelSms]                  = source.[OmniChannelSms],
            target.[OmniChannelEmail]                = source.[OmniChannelEmail],
            target.[UserGroupFilter]                 = source.[UserGroupFilter],
            target.[StoreZipCodesFilter]             = source.[StoreZipCodesFilter],
            target.[ZoneRuleFilter]                  = source.[ZoneRuleFilter],
            target.[UserInputFilter]                 = source.[UserInputFilter],
            target.[BarCodeTypeUpcA]                 = source.[BarCodeTypeUpcA],
            target.[BarCodeType128]                  = source.[BarCodeType128],
            target.[BarCodeTypeLoyaltyId]            = source.[BarCodeTypeLoyaltyId],
            target.[BarCodeTypeQRCode]               = source.[BarCodeTypeQRCode]
    WHEN NOT MATCHED BY target THEN
        INSERT ([TenantID], [OfferTypeFuel], [OfferTypeProduct], [TimeOfWeekFilter], [TimeOfDayFilter], [ByDateFilter], [UserCreatedOnFilter], [NewUserFilter], [CompletedTransactionsFilter], [LastVisitDateFilter], [VisitCountToStoreFilter], [ExactStoreFilter], [StoreGroupFilter], [DistributionChannelFilter], [StoreGeolocationFilter], [StoreCityStateFilter], [StoreStatesFilter], [PaymentMethodFilter], [FirstVisitToStore], [PreviousPurchaseFilter], [PurchasedFuelFilter], [UnauthenticatedFilter], [DisplayTypeDiscount], [DisplayTypePromo], [DisplayTypeCoupon], [TenantFilter], [VisitCountToStoreActivityFilter], [ImpressionCountFilter], [OfferClicksFilter], [Unauthenticated], [OmniChannelPush], [OmniChannelSms], [OmniChannelEmail], [UserGroupFilter], [StoreZipCodesFilter], [ZoneRuleFilter], [UserInputFilter], [BarCodeTypeUpcA], [BarCodeType128], [BarCodeTypeLoyaltyId], [BarCodeTypeQRCode])
        VALUES ([TenantID], [OfferTypeFuel], [OfferTypeProduct], [TimeOfWeekFilter], [TimeOfDayFilter], [ByDateFilter], [UserCreatedOnFilter], [NewUserFilter], [CompletedTransactionsFilter], [LastVisitDateFilter], [VisitCountToStoreFilter], [ExactStoreFilter], [StoreGroupFilter], [DistributionChannelFilter], [StoreGeolocationFilter], [StoreCityStateFilter], [StoreStatesFilter], [PaymentMethodFilter], [FirstVisitToStore], [PreviousPurchaseFilter], [PurchasedFuelFilter], [UnauthenticatedFilter], [DisplayTypeDiscount], [DisplayTypePromo], [DisplayTypeCoupon], [TenantFilter], [VisitCountToStoreActivityFilter], [ImpressionCountFilter], [OfferClicksFilter], [Unauthenticated], [OmniChannelPush], [OmniChannelSms], [OmniChannelEmail], [UserGroupFilter], [StoreZipCodesFilter], [ZoneRuleFilter], [UserInputFilter], [BarCodeTypeUpcA], [BarCodeType128], [BarCodeTypeLoyaltyId], [BarCodeTypeQRCode])
    ;

    COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT
        (SELECT * FROM @OfferOptions FOR JSON PATH)[OfferOptions]
    FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
