﻿CREATE   PROCEDURE [Offer].[GET_OfferView] (
    @OfferIDs               varchar(max)        = NULL, -- JSON array of GUIDs
    @TenantID               uniqueidentifier    = NULL,
    @SearchTerm             nvarchar(255)       = NULL,
    @OfferType              nvarchar(255)       = NULL,
    @CampaignID             int                 = NULL,
    @StartDate              datetime2(7)        = NULL,
    @EndDate                datetime2(7)        = NULL,
    @CategoryID             uniqueidentifier    = NULL,
    @IsDraft                bit                 = NULL,
    @IsDisplay              bit                 = NULL,
    @IsDeleted              bit                 = NULL,
    @PageNumber             int                 = NULL,
    @PageSize               int                 = NULL,
    @IncludePartnerOffers   bit                 = 0,
    @AudienceCoveredCount   int                 = NULL,
    @SitesCoveredCount      int                 = NULL
) AS
BEGIN
    IF @SearchTerm IS NOT NULL SET @SearchTerm = '%' + @SearchTerm + '%'
    IF @PageNumber IS NULL SET @PageNumber = 1
    IF @PageSize IS NULL OR @PageSize < 1
    BEGIN
        SET @PageSize = (SELECT COUNT(*) FROM [Offer].[Offer])
        IF @PageSize = 0 SET @PageSize = 1
    END

    SELECT
        [OfferID]                   AS [id],
        [TenantID]                  AS [tenantId],
        [Name]                      AS [source],
        [OfferType]                 AS [offerType],
        [Title]                     AS [title],
        [Subtitle]                  AS [subtitle],
        [Description]               AS [description],
        [EditorTitle]               AS [editorTitle],
        [EditorDescription]         AS [editorDescription],
        [TargetUsersType]           AS [target],
        [ImageURL]                  AS [imageUrl],
        [ImagePos]                  AS [imagePos],
        [StartDate]                 AS [startDate],
        [EndDate]                   AS [endDate],
        [Order]                     AS [order],
        [MaxRedemptionAmount]       AS [maxRedemptionAmount],
        [MaxRedemptions]            AS [maxRedemptions],
        [RuleFilters]               AS [ruleFilters],
        [ProductRewards]            AS [productRewards],
        [Links]                     AS [links],
        [IsFeaturedOffer]           AS [isFeaturedOffer],
        [IsActive]                  AS [isActive],
        [IsBrandedOffer]            AS [brandedOffer],
        [Display]                   AS [display],
        [RedemptionsCount]          AS [redemptionsCount],
        [IntestitialZones]          AS [intestitialZones],
        [IsDraft]                   AS [isDraft],
        [IsRestricted]              AS [isRestricted],
        [Categories]                AS [categories],
        [SitesCoveredCount]         AS [sitesCoveredCount],
        [IsNonDisplay]              AS [isNonDisplay],
        [Campaigns]                 AS [campaigns],
        [CreatedOn]                 AS [createdOn],
        [MessagesCount]             AS [messagesCount],
        [InterstitialZonesCount]    AS [interstitialZonesCount],
        [AppTenants]                AS [appTenants],
        [AudienceCoveredCount]      AS [audienceCoveredCount],
        [IsDisplayTransactionProgress] AS [isDisplayTransactionProgress],
        [BarCode]                   AS [barCode],
        [BarCodeType]               AS [barCodeType],
        [PunchCardDisplayType]      AS [punchCardDisplayType],
        [MaxPunchAmount]            AS [maxPunchAmount]
    FROM
        [Offer].[vGET_OfferView]
    WHERE
        ([IsDeleted] = @IsDeleted OR @IsDeleted IS NULL)
        AND ([IsDraft] = @IsDraft OR @IsDraft IS NULL)
        AND ([IsDisplay] = @IsDisplay OR @IsDisplay IS NULL)
        AND
        (
            (@TenantId IS NULL) OR
            (@IncludePartnerOffers = 1 AND [TenantID] IN
                (
                    SELECT [DistributionChannel]
                    FROM [dbo].[Tenant] tnt
                    CROSS APPLY (
                        SELECT TRY_CONVERT(uniqueidentifier, main.VALUE) AS [DistributionChannel]
                        FROM OPENJSON([DistributionChannels], 'lax $') main
                    ) itms
                    WHERE tnt.[TenantID] = @TenantId
                    UNION
                    SELECT @TenantID
                    -- Select partner tenant IDs
                    --SELECT t.[TenantID]
                    --FROM [dbo].[Tenant] t
                    --WHERE t.[TenantID] IN
                    --(
                    --  SELECT [TenantID]
                    --  FROM [dbo].[FlatStore] WITH(NOLOCK)
                    --  WHERE [OwnerID] = @TenantID
                    --  UNION
                    --  SELECT [OwnerID]
                    --  FROM [dbo].[FlatStore] WITH(NOLOCK)
                    --  WHERE [TenantID] = @TenantID
                    --  UNION
                    --  SELECT @TenantID
                    --)
                )
            )
            OR (@IncludePartnerOffers = 0 AND @TenantId =  [TenantID])
        )
        AND (@CampaignID IS NULL OR EXISTS (SELECT * FROM [Offer].[LkOffer_Campaign] WITH(NOLOCK) WHERE OfferID = OfferID AND CampaignID = @CampaignID))
        AND (OfferType = @OfferType COLLATE Latin1_General_CI_AI OR @OfferType IS NULL)
        AND (@OfferIds IS NULL OR (@OfferIds IS NOT NULL AND OfferID IN (SELECT CAST(value AS uniqueidentifier) FROM OPENJSON(@OfferIDs))))
        AND (@CategoryID IS NULL OR EXISTS (SELECT * FROM [Offer].[LkOffer_Category] WITH(NOLOCK) WHERE OfferID = OfferID AND OfferCategoryID = @CategoryID))
        AND (@AudienceCoveredCount = AudienceCoveredCount OR @AudienceCoveredCount IS NULL)
        AND (@SitesCoveredCount = SitesCoveredCount OR @SitesCoveredCount IS NULL)
        AND ((Title LIKE @SearchTerm OR Subtitle LIKE @SearchTerm OR EditorTitle LIKE @SearchTerm OR EditorDescription LIKE @SearchTerm) OR @SearchTerm IS NULL)
        AND (
            @StartDate IS NULL
            --OR (StartDate IS NOT NULL AND EndDate IS NOT NULL AND StartDate >= @StartDate AND EndDate > @StartDate)
            --OR (StartDate IS NULL AND EndDate IS NOT NULL AND EndDate > @StartDate)
            --OR (StartDate IS NOT NULL AND EndDate IS NULL AND EndDate > @StartDate)
            OR (ISNULL(StartDate, '0001-01-01') >= @StartDate AND ISNULL(EndDate, '9999-12-31') > @StartDate)
        )
        AND (
            @EndDate IS NULL
            OR (@EndDate IS NOT NULL AND EndDate IS NULL)
            --OR (StartDate IS NOT NULL AND EndDate IS NOT NULL AND EndDate <= @EndDate AND StartDate < @EndDate)
            --OR (StartDate IS NULL AND EndDate IS NOT NULL AND EndDate <= @EndDate)
            --OR (StartDate IS NOT NULL AND EndDate IS NULL AND StartDate < @EndDate)
            OR (ISNULL(EndDate, '9999-12-31') <= @EndDate AND ISNULL(StartDate, '0001-01-01') < @EndDate)
        )
    ORDER BY [CreatedOn] DESC
    OFFSET (@PageNumber-1)*@PageSize ROWS
    FETCH NEXT @PageSize ROWS ONLY
    FOR JSON PATH
END

