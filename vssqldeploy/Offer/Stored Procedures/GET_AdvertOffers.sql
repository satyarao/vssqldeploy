﻿
CREATE   PROCEDURE [Offer].[GET_AdvertOffers] (
    @AdvertOfferIDs     varchar(max)        = NULL, -- JSON array of GUIDs
    @TenantID           uniqueidentifier    = NULL,
    @SearchTerm         nvarchar(255)       = NULL,
    @OfferType          nvarchar(255)       = NULL,
    @Title              nvarchar(255)       = NULL,
    @Display            nvarchar(255)       = NULL,
    @CampaignID         int                 = NULL,
    @StartDate          datetime2(7)        = NULL,
    @EndDate            datetime2(7)        = NULL,
    @CreatedOn          datetime2(7)        = NULL,
    @Status             nvarchar(255)       = NULL,
    @State              nvarchar(255)       = NULL,
    @CategoryID         uniqueidentifier    = NULL,
    @IsActive           bit                 = NULL,
    @IsDeleted          bit                 = NULL,
    @PageNumber         int                 = NULL,
    @PageSize           int                 = NULL,
    @DiscountValue      decimal             = NULL,
    @RedemptionsCount   int                 = NULL,
    @TenantName         nvarchar(255)       = NULL
) AS
BEGIN
    IF @SearchTerm IS NOT NULL SET @SearchTerm = '%' + @SearchTerm + '%'
    IF @Title IS NOT NULL SET @Title = '%' + @Title + '%'
    IF @TenantName IS NOT NULL SET @TenantName = '%' + @TenantName + '%'
    IF @PageNumber IS NULL SET @PageNumber = 1
    IF @PageSize IS NULL OR @PageSize < 1
    BEGIN
        SET @PageSize = (SELECT COUNT(*) FROM [Offer].[AdvertOffer])
        IF @PageSize = 0 SET @PageSize = 1
    END

    SELECT
          AdvertOfferID             AS [AdvertOfferID]
        , OfferTemplateID           AS [OfferTemplateID]
        , TenantID                  AS [TenantID]
        , TenantName                AS [TenantName]
        , OfferType                 AS [OfferType]
        , RewardType                AS [RewardType]
        , TargetUsersType           AS [TargetUsersType]
        , EditorTitle               AS [EditorTitle]
        , EditorDescription         AS [EditorDescription]
        , Title                     AS [Title]
        , Subtitle                  AS [Subtitle]
        , [Description]             AS [Description]
        , ImageURL                  AS [ImageURL]
        , ImagePos                  AS [ImagePos]
        , [Order]                   AS [Order]
        , Display                   AS [Display]
        , StartDate                 AS [StartDate]
        , EndDate                   AS [EndDate]
        , IsFeaturedOffer           AS [IsFeaturedOffer]
        , IsDeleted                 AS [IsDeleted]
        , IsActive                  AS [IsActive]
        , IsDraft                   AS [IsDraft]
        , IsRestricted              AS [IsRestricted]
        , IsDisplay                 AS [IsDisplay]
        , IsBrandedOffer            AS [IsBrandedOffer]
        , RedemptionsCount          AS [RedemptionsCount]
        , Links                     AS [Links]
        , Categories                AS [Categories]
        , RuleFilters               AS [RuleFilters]
        , Campaigns                 AS [Campaigns]
        , MaxRedemptions            AS [MaxRedemptions]
        , ProductDiscounts          AS ProductDiscounts
        , ProductCode               AS [ProductCode]
        , CreatedOn                 AS [Meta.createdOn]
        , CreatedBy                 AS [Meta.createdBy]
        , CreatedByUserID           AS [Meta.createdByUserID]
        , UpdatedOn                 AS [Meta.updatedOn]
        , UpdatedBy                 AS [Meta.updatedBy]
        , UpdatedByUserID           AS [Meta.updatedByUserID]
    FROM
        [Offer].[vGET_AdvertOffers] AdvOFR
    WHERE
        (IsDeleted = @IsDeleted OR @IsDeleted IS NULL)
        AND (AdvOFR.TenantID = @TenantId OR @TenantId IS NULL)
        AND (@AdvertOfferIds IS NULL OR (@AdvertOfferIds IS NOT NULL AND AdvOFR.AdvertOfferID IN (SELECT TRY_CONVERT(uniqueidentifier, value) FROM OPENJSON(@AdvertOfferIDs))))
        AND ((Title LIKE @SearchTerm OR Subtitle LIKE @SearchTerm OR EditorTitle LIKE @SearchTerm OR EditorDescription LIKE @SearchTerm) OR @SearchTerm IS NULL)
        AND (AdvOFR.IsActive = @IsActive OR @IsActive IS NULL)
        AND (OfferType = @OfferType OR @OfferType IS NULL)
        AND (Title LIKE @Title OR @Title IS NULL)
        AND (Display = @Display OR @Display IS NULL)
        AND (TenantName LIKE @TenantName OR @TenantName IS NULL)
        AND ((@Status = 'Draft' AND isDraft = 1) OR (@Status = 'Published' AND isDraft = 0) OR @Status IS NULL)
        AND (@CampaignID IS NULL OR EXISTS (SELECT * FROM [Offer].[LkAdvertOffer_Campaign] WHERE AdvertOfferID = AdvOFR.AdvertOfferID AND CampaignID = @CampaignID))
        AND (@CategoryID IS NULL OR EXISTS (SELECT * FROM [Offer].[LkAdvertOffer_Category] WHERE AdvertOfferID = AdvOFR.AdvertOfferID AND OfferCategoryID = @CategoryID))
        AND (
            @StartDate IS NULL
            --OR (StartDate IS NOT NULL AND EndDate IS NOT NULL AND StartDate >= @StartDate AND EndDate > @StartDate)
            --OR (StartDate IS NULL AND EndDate IS NOT NULL AND EndDate > @StartDate)
            --OR (StartDate IS NOT NULL AND EndDate IS NULL AND EndDate > @StartDate)
            OR (ISNULL(StartDate, '0001-01-01') >= @StartDate AND ISNULL(EndDate, '9999-12-31') > @StartDate)
        )
        AND (
            @EndDate IS NULL
            OR (@EndDate IS NOT NULL AND EndDate IS NULL)
            --OR (StartDate IS NOT NULL AND EndDate IS NOT NULL AND EndDate <= @EndDate AND StartDate < @EndDate)
            --OR (StartDate IS NULL AND EndDate IS NOT NULL AND EndDate <= @EndDate)
            --OR (StartDate IS NOT NULL AND EndDate IS NULL AND StartDate < @EndDate)
            OR (ISNULL(EndDate, '9999-12-31') <= @EndDate AND ISNULL(StartDate, '0001-01-01') < @EndDate)
        )
        AND (
            (YEAR(@CreatedOn) = YEAR(AdvOFR.CreatedOn)
            AND MONTH(@CreatedOn) = MONTH(AdvOFR.CreatedOn)
            AND DAY(@CreatedOn) = DAY(AdvOFR.CreatedOn))
            OR @CreatedOn IS NULL
        )
        AND (
            (@State = 0 AND AdvOFR.IsActive = 1)
            OR (@State = 1 AND AdvOFR.isActive = 0 AND @EndDate > EndDate)
            OR (@State = 2 AND AdvOFR.isActive = 0)
            OR  @State IS NULL
        )
        AND (
            @DiscountValue = DiscountValue OR @DiscountValue IS NULL
        )
        AND (
            @RedemptionsCount = RedemptionsCount
            OR @RedemptionsCount IS NULL
        )
    ORDER BY [Order]
    OFFSET (@PageNumber-1)*@PageSize ROWS
    FETCH NEXT @PageSize ROWS ONLY
    FOR JSON PATH
END

