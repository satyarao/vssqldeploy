﻿
/****************************************PUT OFFER MODEL****************************************/
CREATE   PROCEDURE [Offer].[PUT_OfferModelV5] @json nvarchar(max)
AS BEGIN
	EXEC [Offer].[Serialize_OfferModelV5] @json, 1, 0;
END
