﻿

CREATE   PROCEDURE [Offer].[Refresh_StoreOfferMatching] ( @StoreId uniqueidentifier = NULL, @OfferId uniqueidentifier = NULL )
AS
BEGIN
	MERGE [Offer].[StoreOfferMatching] AS target
	USING [Offer].[StoreOfferMapping]( @OfferId, @StoreId ) AS source
	ON target.[OfferID] = source.[OfferID] AND target.[StoreID] = source.[StoreID]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([StoreID],[OfferID])
		VALUES
			([StoreID],[OfferID])
	WHEN	NOT MATCHED BY source AND 
			(@StoreId IS NULL OR target.[StoreID] = @StoreId)  AND
			(@OfferId IS NULL OR target.[OfferID] = @OfferId)
	THEN	DELETE;
END
