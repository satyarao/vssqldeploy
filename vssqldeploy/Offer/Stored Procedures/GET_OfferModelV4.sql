﻿
CREATE   PROCEDURE [Offer].[GET_OfferModelV4] (
    @OfferID            uniqueidentifier,
    @TenantID           uniqueidentifier    = NULL,
    @IsDeleted          bit                 = NULL
    --, @jsonText nvarchar(max) = null OUTPUT
) AS
BEGIN
BEGIN TRY
    SELECT
        'CCE'                   AS [rawPartitionKey],
        'cce_offer_doc_v4'      AS [documentType],
        'CCE'                   AS [partitionKey],
        LOWER(CAST(OFR.[OfferID] AS varchar(36))) AS [id],
        [OfferType]             AS [OfferType],
        [RewardType]            AS [RewardType],
        [TargetUsersType]       AS [TargetUsersType],
        [EditorTitle]           AS [EditorTitle],
        [EditorDescription]     AS [EditorDescription],
        [Title]                 AS [Title],
        [Subtitle]              AS [Subtitle],
        [Description]           AS [Description],
        [ImageURL]              AS [ImageURL],
        [ImagePos]              AS [ImagePos],
        [StartDate]             AS [StartDate],
        [EndDate]               AS [EndDate],
        [Order]                 AS [Order],
        [AudienceCoveredCount]  AS [AudienceCoveredCount],
        [SitesCoveredCount]     AS [SitesCoveredCount],
        [RuleFilters]           AS [RuleFilters],
        [ProductRewards]        AS [ProductRewards],
        [InterstitialZones]     AS [InterstitialZones],
        [Links]                 AS [Links],
        [ProductDiscounts]      AS [ProductDiscounts],
        [IsFeaturedOffer]       AS [IsFeaturedOffer],
        [IsDeleted]             AS [IsDeleted],
        IsActive                AS [IsActive],
        [MaxRedemptionAmount]   AS [MaxRedemptionAmount],
        [MaxRedemption]         AS [MaxRedemption],
        [MaxRedemptions]        AS [MaxRedemptions],
        [IsBrandedOffer]        AS [BrandedOffer],
        [BarCode]               AS [BarCode],
        [BarCodeType]           AS [BarCodeType],
        [PunchCardDisplayType]  AS [PunchCardDisplayType],
        [MaxPunchAmount]        AS [MaxPunchAmount],
        [Display]               AS [Display],
        [IsDraft]               AS [IsDraft],
        [IsRestricted]          AS [IsRestricted],
        [Categories]            AS [Categories],
        [IsDisplay]             AS [IsDisplay],
        [Campaigns]             AS [Campaigns],
        [AppTenants]            AS [AppTenants],
        OFR.[TenantID]          AS [tenantId],
        TenantName              AS [tenantName],
        [SourceTenantName]      AS [sourceTenantName],
        [CreatedOn]             AS [Meta.createdOn],
        [CreatedBy]             AS [Meta.createdBy],
        [CreatedByUserId]       AS [Meta.createdByUserId],
        [UpdatedOn]             AS [Meta.updatedOn],
        [UpdatedBy]             AS [Meta.updatedBy],
        [UpdatedByUserId]       AS [Meta.updatedByUserId]
    FROM
        [Offer].[vGET_Offers] OFR
    WHERE
        OFR.[OfferID] = @OfferID
        AND (OFR.[TenantID] = @TenantID OR @TenantID IS NULL)
        AND (OFR.[IsDeleted] = @IsDeleted OR @IsDeleted IS NULL)
    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), CONVERT(nvarchar(36),@OfferID) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
