﻿
CREATE   PROCEDURE [Offer].[Serialize_AdvertOfferModelV4] (
    @json nvarchar(max),
    @isUpdate bit,
    @isInsert bit
) AS
BEGIN
    IF @json IS NULL
    BEGIN
        ;THROW 50005, 'Parameter @json is required and cannot be NULL', 1;
    END
    IF ISNULL(@isUpdate,0) = ISNULL(@isInsert,0)
    BEGIN
        ;THROW 50006, 'You must pass either @isUpdate=1 OR @isInsert=1 but not both at the same time', 1;
    END

    SET NOCOUNT ON;
    --SET XACT_ABORT ON;
BEGIN TRY
    --BEGIN TRANSACTION

    -- Supply default value if property is missing
    IF JSON_VALUE(@json, '$.TargetUsersType')   IS NULL SET @json = JSON_MODIFY(@json, '$.TargetUsersType', 'authorized')
    IF JSON_VALUE(@json, '$.Subtitle')          IS NULL SET @json = JSON_MODIFY(@json, '$.Subtitle', '')
    IF JSON_VALUE(@json, '$.Description')       IS NULL SET @json = JSON_MODIFY(@json, '$.Description', '')
    IF JSON_VALUE(@json, '$.ImagePos')          IS NULL SET @json = JSON_MODIFY(@json, '$.ImagePos', 'top')
    IF JSON_VALUE(@json, '$.Display')           IS NULL SET @json = JSON_MODIFY(@json, '$.Display', 'deal')
    IF JSON_VALUE(@json, '$.IsDeleted')         IS NULL SET @json = JSON_MODIFY(@json, '$.IsDeleted', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.IsDraft')           IS NULL SET @json = JSON_MODIFY(@json, '$.IsDraft', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.IsRestricted')      IS NULL SET @json = JSON_MODIFY(@json, '$.IsRestricted', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.IsBrandedOffer')    IS NULL SET @json = JSON_MODIFY(@json, '$.IsBrandedOffer', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.RewardType')        IS NULL SET @json = JSON_MODIFY(@json, '$.RewardType', '')

    DECLARE
        @AdvertOffer        [Offer].[AdvertOfferUpsertSqlModelV4Type],
        @MaxRedemption      [Offer].[MaxRedemptionSqlModelType],
        @Link               [Offer].[OfferModelV4LinkType],
        @RuleFilter         [Offer].[AdvertOfferUpsertV4RuleFilterType],
        @ProductDiscount    [Offer].[ProductDiscountSqlModelType],
        @Category           [Offer].[AdvertOfferUpsertV4CategoryType],
        @Campaign           [Offer].[AdvertOfferUpsertV4CampaignType]

    INSERT INTO @AdvertOffer
    SELECT * FROM OPENJSON(@json)
    WITH
    (
        [AdvertOfferID]         uniqueidentifier    '$.AdvertOfferId',
        [OfferTemplateID]       uniqueidentifier    '$.OfferTemplateId',
        [TenantID]              uniqueidentifier    '$.TenantId',
        [OfferType]             nvarchar(255)       '$.OfferType',
        [RewardType]            nvarchar(255)       '$.RewardType',
        [TargetUsersType]       nvarchar(255)       '$.TargetUsersType',
        [EditorTitle]           nvarchar(255)       '$.EditorTitle',
        [EditorDescription]     nvarchar(max)       '$.EditorDescription',
        [Title]                 nvarchar(255)       '$.Title',
        [Subtitle]              nvarchar(255)       '$.Subtitle',
        [Description]           nvarchar(max)       '$.Description',
        [ImageURL]              nvarchar(255)       '$.ImageURL',
        [ImagePos]              nvarchar(50)        '$.ImagePos',
        [StartDate]             datetime2(7)        '$.StartDate',
        [EndDate]               datetime2(7)        '$.EndDate',
        [Order]                 int                 '$.Order',
        [IsFeaturedOffer]       bit                 '$.IsFeaturedOffer',
        [IsDeleted]             bit                 '$.IsDeleted',
        [IsActive]              bit                 '$.IsActive',
        [IsDraft]               bit                 '$.IsDraft',
        [IsRestricted]          bit                 '$.IsRestricted',
        [IsDisplay]             bit                 '$.IsDisplay',
        [IsBrandedOffer]        bit                 '$.IsBrandedOffer',
        [Display]               nvarchar(50)        '$.Display',
        [CreatedOn]             datetime2(7)        '$.Meta.createdOn',
        [CreatedBy]             nvarchar(255)       '$.Meta.createdBy',
        [CreatedByUserID]       uniqueidentifier    '$.Meta.createdByUserId',
        [UpdatedOn]             datetime2(7)        '$.Meta.updatedOn',
        [UpdatedBy]             nvarchar(255)       '$.Meta.updatedBy',
        [UpdatedByUserID]       uniqueidentifier    '$.Meta.updatedByUserId'
    );
    IF ((SELECT COUNT(*) FROM @AdvertOffer) != 1)
    BEGIN
        ;THROW 50001, 'Must submit exactly one advert offer', 1;
    END
    IF /*NOT*/ EXISTS (SELECT * FROM @AdvertOffer WHERE [AdvertOfferID] IS NULL)
    BEGIN
        ;THROW 50002, 'Must include a valid AdvertOfferID', 1;
    END

    INSERT INTO @MaxRedemption
    SELECT
        [AdvertOfferID]
        , MaxRedemptionID = ISNULL(MaxRedemptions.MaxRedemptionID, NEWID())
        , MaxRedemptions.RedemptionType
        , MaxRedemptions.[MaxRedemptions]
        , MaxRedemptions.[RollingWindowDays]
        , MaxRedemptions.[RollingWindowWeeks]
        , MaxRedemptions.[RollingWindowMonths]
        , MaxRedemptions.[RollingWindowYears]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [AdvertOfferID]         [uniqueidentifier]  '$.AdvertOfferId',
        [MaxRedemptions] [nvarchar](max) AS json,
        [IsActive]              [bit]               '$.IsActive',
        [CreatedOn]             [datetime2]         '$.Meta.createdOn'
    ) AS AdvertOffer
    CROSS APPLY OPENJSON (AdvertOffer.MaxRedemptions)
    WITH
    (
        [MaxRedemptionID]       [uniqueidentifier]  '$.MaxRedemptionId',
        [RedemptionType]        [nvarchar](10)      '$.RedemptionType',
        [MaxRedemptions]        [decimal](10,3)     '$.MaxRedemptions',
        [RollingWindowDays]     [tinyint]           '$.RollingWindowDays',
        [RollingWindowWeeks]    [tinyint]           '$.RollingWindowWeeks',
        [RollingWindowMonths]   [tinyint]           '$.RollingWindowMonths',
        [RollingWindowYears]    [tinyint]           '$.RollingWindowYears'
    ) AS MaxRedemptions
    WHERE [AdvertOfferID] IS NOT NULL;

    INSERT INTO @Link
    SELECT
        [AdvertOfferID]
        , LinkID = ISNULL(Links.[LinkID], NEWID())
        , Links.[LinkText]
        , Links.[LinkType]
        , Links.[Url]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [AdvertOfferID]     [uniqueidentifier]  '$.AdvertOfferId',
        [Links] [nvarchar](max) AS json,
        [IsActive]          [bit]               '$.IsActive',
        [CreatedOn]         [datetime2](7)      '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.Links)
    WITH
    (
        [LinkID]    [uniqueidentifier]  '$.linkId',
        [LinkText]  [nvarchar](255)     '$.linkText',
        [LinkType]  [nvarchar](255)     '$.linkType',
        [Url]       [nvarchar](255)     '$.url'
    ) AS Links
    WHERE [AdvertOfferID] IS NOT NULL AND ([LinkText] IS NOT NULL OR [LinkType] IS NOT NULL OR [Url] IS NOT NULL);

    INSERT INTO @RuleFilter
    SELECT
        [AdvertOfferID]
        , RF.value AS [RuleFilterID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [AdvertOfferID]           uniqueidentifier    '$.AdvertOfferId',
        RuleFilters nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.RuleFilters) AS RF
    WHERE RF.value IS NOT NULL AND [AdvertOfferID] IS NOT NULL;

    INSERT INTO @ProductDiscount
    SELECT
        [AdvertOfferID]
        , ProductDiscountID = ISNULL(ProductDiscounts.ProductDiscountID, NEWID())
        , ProductDiscounts.[ProductRewardID]
        , ProductDiscounts.[StandardProductGroupID]
        , ProductDiscounts.[DiscountType]
        , ProductDiscounts.[DiscountLimitType]
        , ProductDiscounts.[DiscountValue]
        , ProductDiscounts.[DiscountCurrency]
        , ProductDiscounts.[NewUnitPrice]
        , ProductDiscounts.[MinimumQuantity]
        , ProductDiscounts.[MaximumQuantity]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [AdvertOfferID]     [uniqueidentifier]  '$.AdvertOfferId',
        [ProductDiscounts]  [nvarchar](max) AS json,
        [IsActive]          [bit]               '$.IsActive',
        [CreatedOn]         [datetime2]         '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.ProductDiscounts)
    WITH
    (
        [ProductDiscountID]         uniqueidentifier    '$.ProductDiscountId',
        [ProductRewardID]           uniqueidentifier    '$.ProductRewardId',
        [StandardProductGroupID]    uniqueidentifier    '$.StandardProductGroupId',
        [DiscountType]              nvarchar(50)        '$.DiscountType',
        [DiscountLimitType]         nvarchar(20)        '$.DiscountLimitType',
        [DiscountValue]             decimal(7,3)        '$.DiscountValue',
        [DiscountCurrency]          nvarchar(50)        '$.DiscountCurrency',
        [NewUnitPrice]              decimal(7,3)        '$.NewUnitPrice',
        [MinimumQuantity]           decimal(6,3)        '$.MinimumQuantity',
        [MaximumQuantity]           decimal(6,3)        '$.MaximumQuantity'
        --[QuantityType]      nvarchar(50)        '$.QuantityType',
        --[MinimumPurchase]   decimal(7,3)        '$.MinimumPurchase',
        --[ProductCodetType]  nvarchar(50)        '$.ProductCodetType',
        --[ProductCode]       nvarchar(14)        '$.ProductCode',
    ) AS ProductDiscounts
    WHERE [AdvertOfferID] IS NOT NULL;

    INSERT INTO @Category
    SELECT
        [AdvertOfferId]
        , Ctg.value AS [OfferCategoryID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [AdvertOfferId]     uniqueidentifier    '$.AdvertOfferId',
        Categories nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.Categories) AS Ctg
    WHERE Ctg.value IS NOT NULL AND [AdvertOfferID] IS NOT NULL;

    INSERT INTO @Campaign
    SELECT
        [AdvertOfferID]
        , Cmp.value AS [CampaignID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [AdvertOfferID]     uniqueidentifier    '$.AdvertOfferId',
        Campaigns nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.Campaigns) AS Cmp
    WHERE Cmp.value IS NOT NULL AND [AdvertOfferID] IS NOT NULL;

    IF (@isUpdate = 1)
    BEGIN
        EXEC [Offer].[Upsert_AdvertOfferModelV4] @AdvertOffer, @MaxRedemption, @Link, @RuleFilter, @ProductDiscount, @Category, @Campaign
    END
    ELSE IF (@isInsert = 1)
    BEGIN
        EXEC [Offer].[Insert_AdvertOfferModelV4] @AdvertOffer, @MaxRedemption, @Link, @RuleFilter, @ProductDiscount, @Category, @Campaign
    END

    --COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), @json AS [StoredProcedureInput];

    --ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
