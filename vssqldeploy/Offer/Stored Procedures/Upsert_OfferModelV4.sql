﻿
CREATE   PROCEDURE [Offer].[Upsert_OfferModelV4] (
    @Offer              [Offer].[OfferUpsertSqlModelV4Type]         READONLY,
    @MaxRedemption      [Offer].[MaxRedemptionSqlModelType]         READONLY,
    @Link               [Offer].[OfferModelV4LinkType]              READONLY,
    @RuleFilter         [Offer].[OfferModelV4RuleFilterType]        READONLY,
    @ProductReward      [Offer].[OfferModelV4ProductRewardType]     READONLY,
  --@InterstitialZone   [Offer].[OfferModelV4InterstitialZoneType]  READONLY,
    @AppTenant          [Offer].[OfferModelV4AppTenantType]         READONLY,
    @Category           [Offer].[OfferModelV4CategoryType]          READONLY,
    @Campaign           [Offer].[OfferModelV4CampaignType]          READONLY,
    @ProductDiscount    [Offer].[ProductDiscountSqlModelType]       READONLY
)
AS
BEGIN
    SET NOCOUNT ON
    --SET XACT_ABORT ON

    IF EXISTS (
        SELECT * FROM
            [Offer].[Offer] OFR
            INNER JOIN @Offer OFR_new ON OFR.OfferID = OFR_new.OfferID
        WHERE OFR.TenantID <> OFR_new.TenantID)
    BEGIN
        ;THROW 50009, 'It is not allowed to change TenantId for existing Offer', 1;
    END

    IF EXISTS (
        SELECT * FROM
            [Offer].[Offer] OFR
            INNER JOIN @Offer OFR_new ON OFR.OfferID = OFR_new.OfferID
        WHERE OFR.IsDraft = 0)
    BEGIN
        ;THROW 50007, 'It is not allowed to update Offer with IsDraft=0', 1;
    END

BEGIN TRY
    BEGIN TRANSACTION

    BEGIN
        DECLARE @Ids TABLE (ID uniqueidentifier PRIMARY KEY)

        DELETE [Offer].[LKOffer_AppTenant]          WHERE OfferID IN (SELECT OfferID FROM @Offer)
        DELETE [Offer].[LkOffer_Campaign]           WHERE OfferID IN (SELECT OfferID FROM @Offer)
        DELETE [Offer].[LkOffer_Category]           WHERE OfferID IN (SELECT OfferID FROM @Offer)
      --DELETE [Offer].[LKOffer_InterstitialZone]   WHERE OfferID IN (SELECT OfferID FROM @Offer)
        DELETE [Offer].[LkOffer_RuleFilter]         WHERE OfferID IN (SELECT OfferID FROM @Offer)

        INSERT INTO @Ids SELECT LinkID FROM [Offer].[LKOffer_Link] WHERE OfferID IN (SELECT OfferID FROM @Offer)
        DELETE [Offer].[LKOffer_Link] WHERE LinkID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[Link] WHERE LinkID IN (SELECT ID FROM @Ids)

        INSERT INTO @Ids SELECT MaxRedemptionID FROM [Offer].[LkOffer_MaxRedemption] WHERE OfferID IN (SELECT OfferID FROM @Offer)
        DELETE [Offer].[LkOffer_MaxRedemption] WHERE MaxRedemptionID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[MaxRedemption] WHERE MaxRedemptionID IN (SELECT ID FROM @Ids)

        INSERT INTO @Ids SELECT ProductDiscountID FROM [Offer].[LkOffer_ProductDiscount] WHERE OfferID IN (SELECT OfferID FROM @Offer)
        DELETE [Offer].[DiscountLimit] WHERE ProductDiscountID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[LkOffer_ProductDiscount] WHERE ProductDiscountID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[ProductDiscount] WHERE ProductDiscountID IN (SELECT ID FROM @Ids)
    END

    MERGE INTO [Offer].[Offer] AS target
    USING (
        SELECT * FROM @Offer
    ) AS source
    ON target.OfferID = source.OfferID
    WHEN MATCHED THEN
        UPDATE SET
            target.[AdvertOfferID]        = source.[AdvertOfferID],
            target.[AutoUpdateAdvert]     = source.[AutoUpdateAdvert],
            target.[OfferTemplateID]      = source.[OfferTemplateID],
            --target.[TenantID]             = source.[TenantID],
            target.[OfferType]            = source.[OfferType],
            target.[RewardType]           = source.[RewardType],
            target.[TargetUsersType]      = source.[TargetUsersType],
            target.[EditorTitle]          = source.[EditorTitle],
            target.[EditorDescription]    = source.[EditorDescription],
            target.[Title]                = source.[Title],
            target.[Subtitle]             = source.[Subtitle],
            target.[Description]          = source.[Description],
            target.[ImageURL]             = source.[ImageURL],
            target.[ImagePos]             = source.[ImagePos],
            target.[Order]                = source.[Order],
            target.[IsFeaturedOffer]      = source.[IsFeaturedOffer],
            target.[IsDeleted]            = source.[IsDeleted],
            target.[IsActive]             = source.[IsActive],
            target.[IsDraft]              = source.[IsDraft],
            target.[IsRestricted]         = source.[IsRestricted],
            target.[IsDisplay]            = source.[IsDisplay],
            target.[IsBrandedOffer]       = source.[IsBrandedOffer],
            target.[Display]              = source.[Display],
            target.[StartDate]            = source.[StartDate],
            target.[EndDate]              = source.[EndDate],
            target.[BarCode]              = source.[BarCode],
            target.[BarCodeType]          = source.[BarCodeType],
            target.[PunchCardDisplayType] = source.[PunchCardDisplayType],
            target.[MaxPunchAmount]       = source.[MaxPunchAmount],
            target.[IsDisplayTransactionProgress] = source.[IsDisplayTransactionProgress]
    WHEN NOT MATCHED BY target THEN
        INSERT ([OfferID], [AdvertOfferID], [AutoUpdateAdvert], [OfferTemplateID], [TenantID], [OfferType], [RewardType], [TargetUsersType], [EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order], [IsFeaturedOffer], [IsDeleted], [IsActive], [IsDraft], [IsRestricted], [IsDisplay], [IsDisplayTransactionProgress], [IsBrandedOffer], [Display], [StartDate], [EndDate], [BarCode], [BarCodeType], [PunchCardDisplayType], [MaxPunchAmount], [CreatedOn], [CreatedBy], [CreatedByUserID], [UpdatedOn], [UpdatedBy], [UpdatedByUserID])
        VALUES ([OfferID], [AdvertOfferID], [AutoUpdateAdvert], [OfferTemplateID], [TenantID], [OfferType], [RewardType], [TargetUsersType], [EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order], [IsFeaturedOffer], [IsDeleted], [IsActive], [IsDraft], [IsRestricted], [IsDisplay], [IsDisplayTransactionProgress], [IsBrandedOffer], [Display], [StartDate], [EndDate], [BarCode], [BarCodeType], [PunchCardDisplayType], [MaxPunchAmount], [CreatedOn], [CreatedBy], [CreatedByUserID], [UpdatedOn], [UpdatedBy], [UpdatedByUserID]);
    --WHEN NOT MATCHED BY source THEN
    --    UPDATE SET target.[IsActive] = 0;

    IF EXISTS (SELECT * FROM @MaxRedemption)
    BEGIN
        MERGE INTO [Offer].[MaxRedemption] AS target
        USING (
            SELECT * FROM @MaxRedemption
        ) AS source
        ON target.MaxRedemptionID = source.MaxRedemptionID AND target.RedemptionType = source.RedemptionType
        WHEN MATCHED THEN
            UPDATE SET
                target.[MaxRedemptions]         = source.[MaxRedemptions],
                target.[RollingWindowDays]      = source.[RollingWindowDays],
                target.[RollingWindowWeeks]     = source.[RollingWindowWeeks],
                target.[RollingWindowMonths]    = source.[RollingWindowMonths],
                target.[RollingWindowYears]     = source.[RollingWindowYears],
                target.[IsActive]               = source.[IsActive]
        WHEN NOT MATCHED BY target THEN
            INSERT ([MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears], [IsActive])
            VALUES ([MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears], 1         );
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;

        MERGE INTO [Offer].[LkOffer_MaxRedemption] AS target
        USING (
            SELECT [OfferID], [MaxRedemptionID] FROM @MaxRedemption
        ) AS source
        ON target.MaxRedemptionID = source.MaxRedemptionID AND target.OfferID = source.OfferID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([MaxRedemptionID], [OfferID])
            VALUES ([MaxRedemptionID], [OfferID]);
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @Link)
    BEGIN
        MERGE INTO [Offer].[Link] AS target
        USING (
            SELECT * FROM @Link
        ) AS source
        ON target.[LinkID] = source.[LinkID]
            AND target.[LinkText] = source.[LinkText]
            AND target.[LinkType] = source.[LinkType]
            AND target.[Url] = source.[Url]
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([LinkID], [LinkText], [LinkType], [Url], [IsActive], [CreatedOn] )
            VALUES ([LinkID], [LinkText], [LinkType], [Url], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;

        MERGE INTO [Offer].[LkOffer_Link] AS target
        USING (
            SELECT [OfferID], [LinkID] FROM @Link
        ) AS source
        ON target.[LinkID] = source.[LinkID] AND target.[OfferID] = source.[OfferID]
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([LinkID],[OfferID])
            VALUES ([LinkID],[OfferID]);
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @RuleFilter)
    BEGIN
        MERGE INTO [Offer].[LkOffer_RuleFilter] AS target
        USING (
            SELECT * FROM @RuleFilter
            WHERE [OfferID] IS NOT NULL AND [RuleFilterID] IS NOT NULL
        ) AS source
        ON target.OfferID = source.OfferID AND target.RuleFilterID = source.RuleFilterID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([OfferID], [RuleFilterID], [IsActive], [CreatedOn] )
            VALUES ([OfferID], [RuleFilterID], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    --IF EXISTS (SELECT * FROM @ProductReward)
    --BEGIN
    --    MERGE INTO [Offer].[ProductReward] AS target
    --    USING (
    --        SELECT * FROM @ProductReward
    --        WHERE [OfferID] IS NOT NULL AND [ProductRewardID] IS NOT NULL
    --    ) AS source
    --    ON target.OfferID = source.OfferID AND target.ProductRewardID = source.ProductRewardID
    --    WHEN MATCHED THEN
    --        UPDATE SET target.[IsActive] = 1
    --    WHEN NOT MATCHED BY target THEN
    --        INSERT ([OfferID], [ProductRewardID], [IsActive], [CreatedOn] )
    --        VALUES ([OfferID], [ProductRewardID], 1         , GETUTCDATE());
    --    --WHEN NOT MATCHED BY source THEN
    --    --    UPDATE SET target.[IsActive] = 0;
    --END

    --IF EXISTS (SELECT * FROM @InterstitialZone)
    --BEGIN
    --    MERGE INTO [Offer].[LkOffer_InterstitialZone] AS target
    --    USING (
    --        SELECT * FROM @InterstitialZone
    --        WHERE [OfferID] IS NOT NULL AND [InterstitialZoneID] IS NOT NULL
    --    ) AS source
    --    ON target.OfferID = source.OfferID AND target.InterstitialZoneID = source.InterstitialZoneID
    --    WHEN MATCHED THEN
    --        UPDATE SET target.[IsActive] = 1
    --    WHEN NOT MATCHED BY target THEN
    --        INSERT ([OfferID], [InterstitialZoneID], [IsActive], [CreatedOn] )
    --        VALUES ([OfferID], [InterstitialZoneID], 1         , GETUTCDATE());
    --    --WHEN NOT MATCHED BY source THEN
    --    --    UPDATE SET target.[IsActive] = 0;
    --END

    IF EXISTS (SELECT * FROM @AppTenant)
    BEGIN
        MERGE INTO [Offer].[LkOffer_AppTenant] AS target
        USING (
            SELECT * FROM @AppTenant
            WHERE [OfferID] IS NOT NULL AND [AppTenantID] IS NOT NULL
        ) AS source
        ON target.OfferID = source.OfferID AND target.AppTenantID = source.AppTenantID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([OfferID], [AppTenantID], [IsActive], [CreatedOn] )
            VALUES ([OfferID], [AppTenantID], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @Category)
    BEGIN
        MERGE INTO [Offer].[LkOffer_Category] AS target
        USING (
            SELECT * FROM @Category
            WHERE [OfferID] IS NOT NULL AND [OfferCategoryID] IS NOT NULL
        ) AS source
        ON target.OfferID = source.OfferID AND target.OfferCategoryID = source.OfferCategoryID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive]  = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([OfferID], [OfferCategoryID], [IsActive], [CreatedOn] )
            VALUES ([OfferID], [OfferCategoryID], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @Campaign)
    BEGIN
        MERGE INTO [Offer].[LkOffer_Campaign] AS target
        USING (
            SELECT * FROM @Campaign
            WHERE [OfferID] IS NOT NULL AND [CampaignID] IS NOT NULL
        ) AS source
        ON target.OfferID = source.OfferID AND target.CampaignID = source.CampaignID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive]  = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([OfferID], [CampaignID], [IsActive], [CreatedOn] )
            VALUES ([OfferID], [CampaignID], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @ProductDiscount)
    BEGIN
        MERGE INTO [Offer].[ProductDiscount] AS target
        USING (
            SELECT * FROM @ProductDiscount
        ) AS source
        ON target.ProductDiscountID = source.ProductDiscountID
            AND target.ProductRewardID = source.ProductRewardID
            AND target.StandardProductGroupID = source.StandardProductGroupID
        WHEN MATCHED THEN
            UPDATE SET
                target.[DiscountType]     = source.[DiscountType],
                target.[DiscountValue]    = source.[DiscountValue],
                target.[DiscountCurrency] = source.[DiscountCurrency],
                target.[NewUnitPrice]     = source.[NewUnitPrice],
                target.[IsActive]         = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], [IsActive], IsStackable, [CreatedOn] )
            VALUES ([ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], 1         , 0          , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;

        MERGE INTO [Offer].[DiscountLimit] AS target
        USING (
            SELECT * FROM @ProductDiscount
        ) AS source
        ON target.ProductDiscountID = source.ProductDiscountID
        WHEN MATCHED THEN
            UPDATE SET
                target.[DiscountLimitType]  = source.[DiscountLimitType],
                target.[MinimumQuantity]    = source.[MinimumQuantity],
                target.[MaximumQuantity]    = source.[MaximumQuantity],
                target.[IsActive]           = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], [IsActive], [CreatedOn] )
            VALUES ([ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], 1         , GETUTCDATE());

        MERGE INTO [Offer].[LkAdvertOffer_ProductDiscount] AS target
        USING (
            SELECT [OfferID], [ProductDiscountID] FROM @ProductDiscount
        ) AS source
        ON target.[ProductDiscountID] = source.[ProductDiscountID] AND target.[AdvertOfferID] = source.[OfferID]
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([ProductDiscountID], [AdvertOfferID])
            VALUES ([ProductDiscountID], [OfferID]);
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    COMMIT TRANSACTION
    RETURN
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType]
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT
        (SELECT * FROM @Offer FOR JSON PATH)[Offer]
        ,(SELECT * FROM @MaxRedemption FOR JSON PATH )[MaxRedemption]
        ,(SELECT * FROM @Link FOR JSON PATH )[Link]
        ,(SELECT * FROM @RuleFilter FOR JSON PATH)[RuleFilter]
        ,(SELECT * FROM @ProductReward FOR JSON PATH)[ProductReward]
      --,(SELECT * FROM @InterstitialZone FOR JSON PATH)[InterstitialZone]
        ,(SELECT * FROM @AppTenant FOR JSON PATH)[AppTenant]
        ,(SELECT * FROM @Category FOR JSON PATH)[Category]
        ,(SELECT * FROM @Campaign FOR JSON PATH)[Campaign]
    FOR JSON PATH) AS [StoredProcedureInput]

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW;
END CATCH
END
