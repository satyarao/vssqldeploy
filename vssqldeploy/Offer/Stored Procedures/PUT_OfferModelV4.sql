﻿
CREATE   PROCEDURE [Offer].[PUT_OfferModelV4] (
    @json nvarchar(max)
) AS
BEGIN
    EXEC [Offer].[Serialize_OfferModelV4] @json, @isUpdate=1, @isInsert=0
END
