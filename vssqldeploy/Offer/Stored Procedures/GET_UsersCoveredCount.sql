﻿
CREATE   PROCEDURE [Offer].[GET_UsersCoveredCount]
(
   @UserGroupId             int = NULL,
   @NewUserAccountAge_Min   int = NULL,
   @NewUserAccountAge_Max   int = NULL,
   @AccountAge_Min          int = NULL,
   @AccountAge_Max          int = NULL
)
AS
SELECT
    COUNT(*)
FROM
    dbo.UserInfo ui
WHERE
    (
        @UserGroupId IS NULL
        OR EXISTS (SELECT * FROM AccessControl.UserGroupAssignment WHERE UserID = ui.UserID AND UserGroupId = @UserGroupId)
    )
    AND
    (
        (@AccountAge_Min IS NULL AND @AccountAge_Max IS NULL)
        OR (@AccountAge_Min IS NOT NULL AND @AccountAge_Max IS NOT NULL
            AND DATEDIFF(dd, CreatedOn, GETUTCDATE()) BETWEEN @AccountAge_Min AND @AccountAge_Max
        )
        OR (@AccountAge_Min IS NOT NULL AND @AccountAge_Max IS NULL
            AND DATEDIFF(dd, CreatedOn, GETUTCDATE()) >= @AccountAge_Min
        )
        OR (@AccountAge_Min IS NULL AND @AccountAge_Max IS NOT NULL
            AND DATEDIFF(dd, CreatedOn, GETUTCDATE()) <= @AccountAge_Max
        )
    )
    AND
    (
        (@NewUserAccountAge_Min IS NULL AND @NewUserAccountAge_Max IS NULL)
        OR (@NewUserAccountAge_Min IS NOT NULL AND @NewUserAccountAge_Max IS NOT NULL
            AND DATEDIFF(dd, CreatedOn, GETUTCDATE()) BETWEEN @NewUserAccountAge_Min AND @NewUserAccountAge_Max
        )
        OR (@NewUserAccountAge_Min IS NOT NULL AND @NewUserAccountAge_Max IS NULL
            AND DATEDIFF(dd, CreatedOn, GETUTCDATE()) >= @NewUserAccountAge_Min
        )
        OR (@NewUserAccountAge_Min IS NULL AND @NewUserAccountAge_Max IS NOT NULL
            AND DATEDIFF(dd, CreatedOn, GETUTCDATE()) <= @NewUserAccountAge_Max
        )
    )


