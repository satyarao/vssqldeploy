﻿
CREATE   PROCEDURE [Offer].[Upsert_AdvertOfferModelV4] (
    @AdvertOffer        [Offer].[AdvertOfferUpsertSqlModelV4Type]   READONLY,
    @MaxRedemption      [Offer].[MaxRedemptionSqlModelType]         READONLY,
    @Link               [Offer].[OfferModelV4LinkType]              READONLY,
    @RuleFilter         [Offer].[AdvertOfferUpsertV4RuleFilterType] READONLY,
    @ProductDiscount    [Offer].[ProductDiscountSqlModelType]       READONLY,
    @Category           [Offer].[AdvertOfferUpsertV4CategoryType]   READONLY,
    @Campaign           [Offer].[AdvertOfferUpsertV4CampaignType]   READONLY
) AS
BEGIN
    SET NOCOUNT ON
    --SET XACT_ABORT ON

    IF EXISTS (
        SELECT * FROM
            [Offer].[AdvertOffer] AdvOFR
            INNER JOIN @AdvertOffer AdvOFR_new ON AdvOFR.AdvertOfferID = AdvOFR_new.AdvertOfferID
        WHERE AdvOFR.TenantID <> AdvOFR_new.TenantID)
    BEGIN
        ;THROW 50009, 'It is not allowed to change TenantId for existing AdvertOffer', 1;
    END

BEGIN TRY
    BEGIN TRANSACTION

    IF EXISTS (
        SELECT * FROM
            [Offer].[AdvertOffer] AdvOFR
            INNER JOIN @AdvertOffer AdvOFR_new ON AdvOFR.AdvertOfferID = AdvOFR_new.AdvertOfferID
        WHERE AdvOFR.IsDraft = 0)
    BEGIN
        UPDATE AdvOFR
        SET isActive = ao.isActive
        FROM [Offer].[AdvertOffer] AS AdvOFR
        JOIN @AdvertOffer AS ao ON AdvOFR.AdvertOfferID = ao.AdvertOfferID

        COMMIT TRANSACTION
        RETURN
    END

    BEGIN
        DECLARE @Ids TABLE (ID uniqueidentifier PRIMARY KEY)

        DELETE [Offer].[LkAdvertOffer_Campaign]     WHERE AdvertOfferID IN (SELECT AdvertOfferID FROM @AdvertOffer)
        DELETE [Offer].[LkAdvertOffer_Category]     WHERE AdvertOfferID IN (SELECT AdvertOfferID FROM @AdvertOffer)
        DELETE [Offer].[LkAdvertOffer_RuleFilter]   WHERE AdvertOfferID IN (SELECT AdvertOfferID FROM @AdvertOffer)

        INSERT INTO @Ids SELECT LinkID FROM [Offer].[LKAdvertOffer_Link] WHERE AdvertOfferID IN (SELECT AdvertOfferID FROM @AdvertOffer)
        DELETE [Offer].[LKAdvertOffer_Link] WHERE LinkID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[Link] WHERE LinkID IN (SELECT ID FROM @Ids)

        INSERT INTO @Ids SELECT MaxRedemptionID FROM [Offer].[LkAdvertOffer_MaxRedemption] WHERE AdvertOfferID IN (SELECT AdvertOfferID FROM @AdvertOffer)
        DELETE [Offer].[LkAdvertOffer_MaxRedemption] WHERE MaxRedemptionID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[MaxRedemption] WHERE MaxRedemptionID IN (SELECT ID FROM @Ids)

        INSERT INTO @Ids SELECT ProductDiscountID FROM [Offer].[LkAdvertOffer_ProductDiscount] WHERE AdvertOfferID IN (SELECT AdvertOfferID FROM @AdvertOffer)
        DELETE [Offer].[DiscountLimit] WHERE ProductDiscountID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[LkAdvertOffer_ProductDiscount] WHERE ProductDiscountID IN (SELECT ID FROM @Ids)
        DELETE [Offer].[ProductDiscount] WHERE ProductDiscountID IN (SELECT ID FROM @Ids)
    END

    MERGE INTO [Offer].[AdvertOffer] AS target
    USING (
        SELECT * FROM @AdvertOffer
    ) AS source
    ON target.AdvertOfferID = source.AdvertOfferID
    WHEN MATCHED THEN
        UPDATE SET
            target.[OfferTemplateID]    = source.[OfferTemplateID],
            --target.[TenantID]           = source.[TenantID],
            target.[OfferType]          = source.[OfferType],
            target.[RewardType]         = source.[RewardType],
            target.[TargetUsersType]    = source.[TargetUsersType],
            target.[EditorTitle]        = source.[EditorTitle],
            target.[EditorDescription]  = source.[EditorDescription],
            target.[Title]              = source.[Title],
            target.[Subtitle]           = source.[Subtitle],
            target.[Description]        = source.[Description],
            target.[ImageURL]           = source.[ImageURL],
            target.[ImagePos]           = source.[ImagePos],
            target.[Order]              = source.[Order],
            target.[IsFeaturedOffer]    = source.[IsFeaturedOffer],
            target.[IsDeleted]          = source.[IsDeleted],
            target.[IsActive]           = source.[IsActive],
            target.[IsDraft]            = source.[IsDraft],
            target.[IsRestricted]       = source.[IsRestricted],
            target.[IsDisplay]          = source.[IsDisplay],
            target.[IsBrandedOffer]     = source.[IsBrandedOffer],
            target.[Display]            = source.[Display],
            target.[StartDate]          = source.[StartDate],
            target.[EndDate]            = source.[EndDate]
    WHEN NOT MATCHED BY target THEN
        INSERT
            ([AdvertOfferID], [OfferTemplateID], [TenantID], [OfferType], [RewardType], [TargetUsersType]
            ,[EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order]
            , [IsFeaturedOffer], [IsDeleted], [IsActive], [IsDraft], [IsRestricted], [IsDisplay], [IsBrandedOffer]
            , [Display], [StartDate], [EndDate])
        VALUES
            ([AdvertOfferID], [OfferTemplateID], [TenantID], [OfferType], [RewardType], [TargetUsersType]
            ,[EditorTitle], [EditorDescription], [Title], [Subtitle], [Description], [ImageURL], [ImagePos], [Order]
            , [IsFeaturedOffer], [IsDeleted], [IsActive], [IsDraft], [IsRestricted], [IsDisplay], [IsBrandedOffer]
            , [Display], [StartDate], [EndDate]);
    --WHEN NOT MATCHED BY source THEN
    --    UPDATE SET target.[IsActive] = 0;

    IF EXISTS (SELECT * FROM @MaxRedemption)
    BEGIN
        MERGE INTO [Offer].[MaxRedemption] AS target
        USING (
            SELECT * FROM @MaxRedemption
        ) AS source
        ON target.MaxRedemptionID = source.MaxRedemptionID AND target.RedemptionType = source.RedemptionType
        WHEN MATCHED THEN
            UPDATE SET
                target.[MaxRedemptions]         = source.[MaxRedemptions],
                target.[RollingWindowDays]      = source.[RollingWindowDays],
                target.[RollingWindowWeeks]     = source.[RollingWindowWeeks],
                target.[RollingWindowMonths]    = source.[RollingWindowMonths],
                target.[RollingWindowYears]     = source.[RollingWindowYears],
                target.[IsActive]               = source.[IsActive]
        WHEN NOT MATCHED BY target THEN
            INSERT ([MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears], [IsActive])
            VALUES ([MaxRedemptionID], [RedemptionType], [MaxRedemptions], [RollingWindowDays], [RollingWindowWeeks], [RollingWindowMonths], [RollingWindowYears], 1);
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;

        MERGE INTO [Offer].[LkAdvertOffer_MaxRedemption] AS target
        USING (
            SELECT [OfferID], [MaxRedemptionID] FROM @MaxRedemption
        ) AS source
        ON target.MaxRedemptionID = source.MaxRedemptionID AND target.AdvertOfferID = source.OfferID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([MaxRedemptionID], [AdvertOfferID])
            VALUES ([MaxRedemptionID], [OfferID]);
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @Link)
    BEGIN
        MERGE INTO [Offer].[Link] AS target
        USING (
            SELECT * FROM @Link
        ) AS source
        ON target.[LinkID] = source.[LinkID]
            AND target.[LinkText] = source.[LinkText]
            AND target.[LinkType] = source.[LinkType]
            AND target.[Url] = source.[Url]
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([LinkID], [LinkText], [LinkType], [Url], [IsActive], [CreatedOn] )
            VALUES ([LinkID], [LinkText], [LinkType], [Url], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;

        MERGE INTO [Offer].[LkAdvertOffer_Link] AS target
        USING (
            SELECT [OfferID], [LinkID] FROM @Link
        ) AS source
        ON target.[LinkID] = source.[LinkID] AND target.[AdvertOfferID] = source.[OfferID]
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([LinkID], [AdvertOfferID])
            VALUES ([LinkID], [OfferID]);
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @RuleFilter)
    BEGIN
        MERGE INTO [Offer].[LkAdvertOffer_RuleFilter] AS target
        USING (
            SELECT * FROM @RuleFilter
            WHERE [AdvertOfferID] IS NOT NULL AND [RuleFilterID] IS NOT NULL
        ) AS source
        ON target.AdvertOfferID = source.AdvertOfferID
            AND target.RuleFilterID = source.RuleFilterID
        WHEN MATCHED THEN UPDATE SET target.[IsActive]  = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([AdvertOfferID], [RuleFilterID], [IsActive], [CreatedOn])
            VALUES ([AdvertOfferID], [RuleFilterID], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @ProductDiscount)
    BEGIN
        MERGE INTO [Offer].[ProductDiscount] AS target
        USING (
            SELECT * FROM @ProductDiscount
        ) AS source
        ON target.ProductDiscountID = source.ProductDiscountID
            AND target.ProductRewardID = source.ProductRewardID
            AND target.StandardProductGroupID = source.StandardProductGroupID
        WHEN MATCHED THEN
            UPDATE SET
                target.[DiscountType]     = source.[DiscountType],
                target.[DiscountValue]    = source.[DiscountValue],
                target.[DiscountCurrency] = source.[DiscountCurrency],
                target.[NewUnitPrice]     = source.[NewUnitPrice],
                target.[IsActive]         = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], [IsActive], IsStackable, [CreatedOn] )
            VALUES ([ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], 1         , 0          , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;

        MERGE INTO [Offer].[DiscountLimit] AS target
        USING (
            SELECT * FROM @ProductDiscount
        ) AS source
        ON target.ProductDiscountID = source.ProductDiscountID
        WHEN MATCHED THEN
            UPDATE SET
                target.[DiscountLimitType]  = source.[DiscountLimitType],
                target.[MinimumQuantity]    = source.[MinimumQuantity],
                target.[MaximumQuantity]    = source.[MaximumQuantity],
                target.[IsActive]           = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], [IsActive], [CreatedOn] )
            VALUES ([ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], 1         , GETUTCDATE());

        MERGE INTO [Offer].[LkAdvertOffer_ProductDiscount] AS target
        USING (
            SELECT [OfferID], [ProductDiscountID] FROM @ProductDiscount
        ) AS source
        ON target.[ProductDiscountID] = source.[ProductDiscountID] AND target.[AdvertOfferID] = source.[OfferID]
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive] = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([ProductDiscountID], [AdvertOfferID])
            VALUES ([ProductDiscountID], [OfferID]);
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @Category)
    BEGIN
        MERGE INTO [Offer].[LkAdvertOffer_Category] AS target
        USING (
            SELECT * FROM @Category
        ) AS source
        ON target.AdvertOfferID = source.AdvertOfferID AND target.OfferCategoryID = source.OfferCategoryID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive]  = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([AdvertOfferID], [OfferCategoryID], [IsActive], [CreatedOn] )
            VALUES ([AdvertOfferID], [OfferCategoryID], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    IF EXISTS (SELECT * FROM @Campaign)
    BEGIN
        MERGE INTO [Offer].[LkAdvertOffer_Campaign] AS target
        USING (
            SELECT * FROM @Campaign
        ) AS source
        ON target.AdvertOfferID = source.AdvertOfferID AND target.CampaignID = source.CampaignID
        WHEN MATCHED THEN
            UPDATE SET target.[IsActive]  = 1
        WHEN NOT MATCHED BY target THEN
            INSERT ([AdvertOfferID], [CampaignID], [IsActive], [CreatedOn] )
            VALUES ([AdvertOfferID], [CampaignID], 1         , GETUTCDATE());
        --WHEN NOT MATCHED BY source THEN
        --    UPDATE SET target.[IsActive] = 0;
    END

    COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT
        (SELECT * FROM @AdvertOffer FOR JSON PATH)[Offer]
        ,(SELECT * FROM @MaxRedemption FOR JSON PATH)[MaxRedemption]
        ,(SELECT * FROM @Link FOR JSON PATH )[Link]
        ,(SELECT * FROM @RuleFilter FOR JSON PATH)[RuleFilter]
        ,(SELECT * FROM @ProductDiscount FOR JSON PATH)[ProductDiscount]
        ,(SELECT * FROM @Category FOR JSON PATH)[Category]
        ,(SELECT * FROM @Campaign FOR JSON PATH)[Campaign]
    FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
