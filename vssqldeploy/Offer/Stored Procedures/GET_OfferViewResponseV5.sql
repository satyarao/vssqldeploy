﻿


/****************************************GET OFFER VIEW****************************************/
CREATE   PROCEDURE [Offer].[GET_OfferViewResponseV5]
(
	@TenantId uniqueidentifier,
	@IsActive bit = NULL,
	@OfferType nvarchar(255) = NULL
) AS
BEGIN
	SET NOCOUNT ON;
    SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	SELECT ofr.[OfferID]	[offerId],
		ofr.[TenantID]		[tenantId],
		tnt.[Name]			[tenantName],
		ofr.[Title]			[title],
		ofr.[Subtitle]		[subtitle],
		ofr.[Description]	[description],
		ofr.[CreatedOn]		[createdOn],
		ofr.[StartDate]		[startDate],
		ofr.[EndDate]		[endDate],
		(
			SELECT pd.[DiscountValue]	[discountAmount],
				pd.[IsStackable]		[isStackable]
			FROM [Offer].[ProductDiscount] pd
			INNER JOIN [Offer].[LkOffer_ProductDiscount] lkPd
				ON lkPd.[ProductDiscountID] = pd.[ProductDiscountID]
			WHERE lkPd.[OfferID] = ofr.[OfferID]
			FOR JSON PATH
		) AS				[productRewards],
		ofr.[IsActive]		[isActive],
		CASE
			WHEN ofr.[IsDraft] = 1
				THEN 'draft'
			ELSE 'published'
		END AS				[offerStatus],
		CASE
			WHEN ofr.[IsActive] = 0
				THEN 'disabled'
			ELSE
				CASE
					WHEN ofr.[EndDate] IS NOT NULL AND ofr.[EndDate] < GETUTCDATE()
						THEN 'expired'
					ELSE 'enabled'
				END
		END AS				[offerState],
		ofr.[RewardType]	[rewardType]
	FROM [Offer].[Offer] ofr
	INNER JOIN [dbo].[Tenant] tnt
		ON tnt.[TenantID] = ofr.[TenantID]
	WHERE ofr.[TenantID] = @TenantId AND
		ofr.[IsActive] = ISNULL(@IsActive, ofr.[IsActive]) AND
		ofr.[OfferType] = ISNULL(@OfferType, ofr.[OfferType])
	FOR JSON PATH


	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @TenantId [TenantId], @IsActive [IsActive], @OfferType [OfferType] FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
