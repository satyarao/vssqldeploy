﻿


/****************************************SERIALIZE OFFER MODEL****************************************/
CREATE   PROCEDURE [Offer].[Serialize_OfferModelV5]
(
	@json nvarchar(max),
    @isUpdate bit,
    @isInsert bit
) AS
BEGIN
	IF @json IS NULL
    BEGIN
        ;THROW 50005, 'Parameter @json is required and cannot be NULL', 1;
    END
    IF ISNULL(@isUpdate,0) = ISNULL(@isInsert,0)
    BEGIN
        ;THROW 50006, 'You must pass either @isUpdate=1 OR @isInsert=1 but not both at the same time', 1;
    END

	SET NOCOUNT ON;
    SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	DECLARE @OfferBase [Offer].[OfferModelV5],
		@ProductDiscounts [Offer].[ProductDiscountV5],
		@Links [Offer].[OfferLinkV5],
		@AppTenants [Offer].[OfferID_Guid_Tuple],
		@RuleFilters [Offer].[OfferID_Guid_Tuple],
		@InterstitialZones [Offer].[OfferID_Guid_Tuple],
		@Categories [Offer].[OfferID_Guid_Tuple],
		@Campaigns [Offer].[OfferID_Int_Tuple];

	INSERT INTO @OfferBase
		([OfferID],[AdvertOfferID],[AutoUpdateAdvert],[OfferTemplateID],[TenantID],[RewardID],[OfferType]
		,[RewardType],[TargetUsersType],[EditorTitle],[EditorDescription],[Title],[Subtitle],[Description]
		,[ImageURL],[ImagePos],[Order],[IsFeaturedOffer],[IsDeleted],[IsActive],[IsDraft],[IsRestricted]
		,[IsDisplay],[IsBrandedOffer],[Display],[StartDate],[EndDate],[CreatedOn],[CreatedBy],[CreatedByUserID]
		,[UpdatedOn],[UpdatedBy],[UpdatedByUserID],[UserMaxRedemptions],[UserMaxRedemptionRollingWindowDays]
		,[UserMaxRedemptionRollingWindowWeeks],[UserMaxRedemptionRollingWindowMonths],[UserMaxRedemptionRollingWindowYears]
		,[OfferMaxRedemptions],[OfferMaxRedemptionRollingWindowDays],[OfferMaxRedemptionRollingWindowWeeks]
		,[OfferMaxRedemptionRollingWindowMonths],[OfferMaxRedemptionRollingWindowYears])
	SELECT * FROM OPENJSON(@json)
	WITH
	(
		[OfferID]								uniqueidentifier    '$.offerId',
		[AdvertOfferID]							uniqueidentifier    '$.advertOfferId',
		[AutoUpdateAdvert]						bit                 '$.autoUpdateAdvert',
		[OfferTemplateID]						uniqueidentifier    '$.offerTemplateId',
		[TenantID]								uniqueidentifier    '$.tenantId',

		[RewardID]								nvarchar(50)		'$.rewardId',

		[OfferType]								nvarchar(255)       '$.offerType',
		[RewardType]							nvarchar(255)       '$.rewardType',
		[TargetUsersType]						nvarchar(255)       '$.targetUsersType',
		[EditorTitle]							nvarchar(255)       '$.editorTitle',
		[EditorDescription]						nvarchar(max)       '$.editorDescription',
		[Title]									nvarchar(255)       '$.title',
		[Subtitle]								nvarchar(255)       '$.subtitle',
		[Description]							nvarchar(max)       '$.description',
		[ImageURL]								nvarchar(255)       '$.imageUrl',
		[ImagePos]								nvarchar(50)        '$.imagePos',
		[Order]									int                 '$.order',
		[IsFeaturedOffer]						bit                 '$.isFeaturedOffer',
		[IsDeleted]								bit                 '$.isDeleted',
		[IsActive]								bit                 '$.isActive',
		[IsDraft]								bit                 '$.isDraft',
		[IsRestricted]							bit                 '$.isRestricted',
		[IsDisplay]								bit                 '$.isDisplay',
		[IsBrandedOffer]						bit                 '$.isBrandedOffer',
		[Display]								nvarchar(50)        '$.display',
		[StartDate]								datetime2(7)        '$.startDate',
		[EndDate]								datetime2(7)        '$.endDate',
		[CreatedOn]								datetime2(7)        '$.metaData.createdOn',
		[CreatedBy]								nvarchar(255)       '$.metaData.createdBy',
		[CreatedByUserID]						uniqueidentifier    '$.metaData.createdByUserId',
		[UpdatedOn]								datetime2(7)        '$.metaData.updatedOn',
		[UpdatedBy]								nvarchar(255)       '$.metaData.updatedBy',
		[UpdatedByUserID]						uniqueidentifier    '$.metaData.updatedByUserId',

		[UserMaxRedemptions]					decimal(10,3)		'$.userMaxRedemption.maxRedemptions',
		[UserMaxRedemptionRollingWindowDays]	int					'$.userMaxRedemption.rollingWindowDays',
		[UserMaxRedemptionRollingWindowWeeks]	int					'$.userMaxRedemption.rollingWindowWeeks',
		[UserMaxRedemptionRollingWindowMonths]	int					'$.userMaxRedemption.rollingWindowMonths',
		[UserMaxRedemptionRollingWindowYears]	int					'$.userMaxRedemption.rollingWindowYears',

		[OfferMaxRedemptions]					decimal(10,3)		'$.offerMaxRedemption.maxRedemptions',
		[OfferMaxRedemptionRollingWindowDays]	int					'$.offerMaxRedemption.rollingWindowDays',
		[OfferMaxRedemptionRollingWindowWeeks]	int					'$.offerMaxRedemption.rollingWindowWeeks',
		[OfferMaxRedemptionRollingWindowMonths]	int					'$.offerMaxRedemption.rollingWindowMonths',
		[OfferMaxRedemptionRollingWindowYears]	int					'$.offerMaxRedemption.rollingWindowYears'
	)

	INSERT INTO @ProductDiscounts
		([OfferID],[ProductDiscountID],[StandardProductGroupID],[DiscountType],[DiscountValue],[DiscountCurrency],[NewUnitPrice]
		,[CouponPosId],[IsStackable],[DiscountLimitType],[MinimumQuantity],[MaximumQuantity])
	SELECT
        [OfferID]
		,NEWID() AS [ProductDiscountID]
        ,ProductDiscount.[StandardProductGroupID]
		,ProductDiscount.[DiscountType]
		,ProductDiscount.[DiscountValue]
		,ProductDiscount.[DiscountCurrency]
		,ProductDiscount.[NewUnitPrice]
		,ProductDiscount.[CouponPosId]
		,ProductDiscount.[IsStackable]
		,ProductDiscount.[DiscountLimitType]
		,ProductDiscount.[MinimumQuantity]
		,ProductDiscount.[MaximumQuantity]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.offerId',
        [productDiscounts]	nvarchar(max) AS json
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.productDiscounts)
    WITH
    (
        [StandardProductGroupID]        uniqueidentifier    '$.standardProductGroupId',
        [DiscountType]					nvarchar(50)		'$.discountType',
        [DiscountValue]					decimal(7,3)		'$.discountValue',
		[DiscountCurrency]				nvarchar(50)		'$.discountCurrency',
		[NewUnitPrice]					decimal(7,3)		'$.newUnitPrice',
		[CouponPosId]					nvarchar(20)		'$.couponPosId',
		[IsStackable]					bit					'$.isStackable',
		[DiscountLimitType]				nvarchar(20)		'$.discountLimit.discountLimitType',
		[MinimumQuantity]				decimal(6,3)		'$.discountLimit.minimumQuantity',
		[MaximumQuantity]				decimal(6,3)		'$.discountLimit.maximumQuantity'
    ) AS ProductDiscount
    WHERE [StandardProductGroupID] IS NOT NULL AND [DiscountValue] IS NOT NULL AND [OfferID] IS NOT NULL;

	INSERT INTO @Links
		([OfferID],[LinkID],[LinkText],[LinkType],[Url])
	SELECT
        [OfferID]
		,NEWID() AS [LinkID]
        , Links.[LinkText]
        , Links.[LinkType]
        , Links.[Url]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.offerId',
        [links] nvarchar(max) AS json
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.links)
    WITH
    (
        [LinkText]          nvarchar(255)       '$.linkText',
        [LinkType]          nvarchar(255)       '$.linkType',
        [Url]               nvarchar(255)       '$.url'
    ) AS Links
    WHERE ([LinkText] IS NOT NULL OR [LinkType] IS NOT NULL OR [Url] IS NOT NULL) AND [OfferID] IS NOT NULL;


	INSERT INTO @AppTenants
		([OfferID],[GuidValue])
	SELECT
        [OfferID]
        ,CAST(AppT.value AS uniqueidentifier) AS [GuidValue]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.offerId',
        [appTenants] nvarchar(max) AS json
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.appTenants) AS AppT
    WHERE AppT.value IS NOT NULL AND [OfferID] IS NOT NULL;

	INSERT INTO @RuleFilters
		([OfferID],[GuidValue])
	SELECT
        [OfferID]
        ,CAST(RF.value AS uniqueidentifier) AS [GuidValue]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.offerId',
        [ruleFilters] nvarchar(max) AS json
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.ruleFilters) AS RF
    WHERE RF.value IS NOT NULL AND [OfferID] IS NOT NULL;

	INSERT INTO @InterstitialZones
		([OfferID],[GuidValue])
	SELECT
        [OfferID]
        ,CAST(IZ.value AS uniqueidentifier) AS [GuidValue]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.offerId',
        [interstitialZones] nvarchar(max) AS json
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.interstitialZones) AS IZ
    WHERE IZ.value IS NOT NULL AND [OfferID] IS NOT NULL;

	INSERT INTO @Categories
		([OfferID],[GuidValue])
	SELECT
        [OfferID]
        ,CAST(CAT.value AS uniqueidentifier) AS [GuidValue]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.offerId',
        [categories]		nvarchar(max) AS json
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.categories) AS CAT
    WHERE CAT.value IS NOT NULL AND [OfferID] IS NOT NULL;

	INSERT INTO @Campaigns
		([OfferID],[IntValue])
	SELECT
        [OfferID]
        ,CAST(CAMP.value AS int) AS [IntValue]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.offerId',
        [campaigns]			nvarchar(max) AS json
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.campaigns) AS CAMP
    WHERE CAMP.value IS NOT NULL AND [OfferID] IS NOT NULL;

	IF @isUpdate = 1
	BEGIN
		EXEC [Offer].[Upsert_OfferModelV5] @OfferBase, @ProductDiscounts, @Links, @AppTenants, @RuleFilters, @InterstitialZones, @Categories, @Campaigns;
	END

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @json [json], @isUpdate [isUpdate], @isInsert [isInsert] FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
