﻿
CREATE   PROCEDURE [Offer].[RuleFilterEngine] @TenantID uniqueidentifier, @UserID uniqueidentifier, @StoreIds nvarchar(max), @OfferType nvarchar(255) = NULL
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	DECLARE @StoreIDTable TABLE ( [StoreID] uniqueidentifier );
	INSERT INTO @StoreIDTable ([StoreID])
	SELECT [StoreId]
	FROM OPENJSON(@StoreIds)
	WITH ( [StoreId] uniqueidentifier '$.StoreId' )

	DECLARE @AppTenant uniqueidentifier = @TenantID;
    DECLARE @TimeZone INT	= (SELECT TOP(1) [TimeZoneID] FROM [dbo].[Store] WHERE [StoreID] = (SELECT TOP(1) [StoreID] FROM @StoreIDTable));

	WITH SetReduction AS (
		SELECT orf.[OfferID]
		FROM		[Offer].[GetActiveOffers]( @AppTenant, @TimeZone ) orf
		INNER JOIN	[Offer].[UserOfferMatching] uom WITH(NOLOCK)
		ON			uom.[OfferID] = orf.[OfferID] AND
					uom.[UserID] = @UserID
		INNER JOIN	[Offer].[StoreOfferMatching] som WITH(NOLOCK)
		ON			som.[OfferID] = orf.[OfferID] AND
					som.[StoreID] IN (SELECT [StoreID] FROM @StoreIDTable)
		GROUP BY orf.[OfferID]
	)


	SELECT 
		ISNULL((SELECT TOP(1) extO.[ExternalOfferId] FROM [Offer].[ExternalOffer] extO WHERE extO.[ReferenceId] = ofr.RewardID), ofr.[OfferID]) AS [OfferID],
		ofr.[TenantID] AS [TenantID],
		ofr.[OfferType] AS [OfferType],
		ofr.[TargetUsersType] AS [TargetUsersType],
		ofr.[Title] AS [Title],
		ofr.[Subtitle] AS [Subtitle],
		ofr.[Description] AS [Description],
		ofr.[ImageUrl] AS [ImageURL],
		ofr.[Display] AS [Display],
		ofr.[ImagePos] AS [ImagePos],
		ofr.[StartDate] AS [StartDate],
		ofr.[EndDate] AS [EndDate],
		ofr.[Order] AS [Order],
		(
			SELECT [RuleFilterID] AS [RuleFilterId]
			FROM [Offer].[LkOffer_RuleFilter] WHERE [OfferID] = ofr.[OfferID]
			FOR JSON PATH
		) AS [RuleFilters],
		(
			SELECT lkPd.[ProductDiscountID] AS [ProductDiscountId]
				,pd.[CouponPosId] AS [couponPosId]
			FROM [Offer].[LkOffer_ProductDiscount] AS lkPd 
			INNER JOIN [Offer].[ProductDiscount] pd 
				ON lkPd.[ProductDiscountID] = pd.[ProductDiscountID]
			WHERE lkPd.[OfferID] = ofr.[OfferID]
			FOR JSON PATH
		) AS [ProductRewards],
		(
			SELECT [LinkText] AS [linkText]
				,[LinkType] AS [linkType]
				,[Url] AS [url]
			FROM (SELECT * FROM [Offer].[LkOffer_Link] WHERE [OfferID] = ofr.[OfferID]) lkLink 
			INNER JOIN [Offer].[Link] link ON lkLink.[LinkID] = link.[LinkID]
			FOR JSON PATH
		) AS [Links],
		(
			SELECT TOP(1) pd.[IsStackable] AS [isStackable]
			FROM [Offer].[LkOffer_ProductDiscount] AS lkPd 
			INNER JOIN [Offer].[ProductDiscount] pd 
				ON lkPd.[ProductDiscountID] = pd.[ProductDiscountID]
			WHERE lkPd.[OfferID] = ofr.[OfferID]
			FOR JSON PATH
		) AS [IsStackable],
		ofr.[RewardID] AS [ExternalReferenceId],
		ofr.[IsFeaturedOffer] AS [IsFeaturedOffer],
		ofr.[IsActive] AS [IsActive],
		ofr.[IsRestricted] AS [IsRestricted],
		(
			SELECT [CategoryName] AS [CategoryName]
			FROM (SELECT * FROM [Offer].[LkOffer_Category] WHERE [OfferID] = ofr.[OfferID]) lkCat
			INNER JOIN [Offer].[OfferCategory] cat ON lkCat.[OfferCategoryID] = cat.[OfferCategoryID]
			FOR JSON PATH
		) AS [Categories],
		ofr.[IsDisplay] AS [IsDisplay],
		[InterstitialZoneContents] = JSON_QUERY( [Offer].[GetInterstitialsForMobile]( ofr.[OfferID] ) )

	FROM		SetReduction orf
	INNER JOIN [Offer].[Offer] ofr WITH(NOLOCK) 
	ON			ofr.[OfferID] = orf.[OfferID]
	FOR JSON PATH

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(255) = ERROR_MESSAGE(), @ErrorSeverity int = ERROR_SEVERITY(), @ErrorState int = ERROR_STATE();
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), @ErrorMessage, @ErrorSeverity, 
	(SELECT @TenantID AS [TenantID], @UserID AS [UserID], @StoreIds AS [StoreIds], @OfferType AS [OfferType] FOR JSON PATH) AS [StoredProcedureInput];
    EXEC [dbo].[PostStoredProcedureException] @Exceptions;

	ROLLBACK TRANSACTION;
	RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
END CATCH
END

