﻿
CREATE   PROCEDURE [Offer].[GET_OffersForCache] (
    @IsDisplayPromotion bit,
    @TargetUsersType    nvarchar(255)
) AS
BEGIN
    SELECT
        OfferID                 AS [id]
        , AdvertOfferID         AS [AdvertOfferID]
        , OfferTemplateID       AS [OfferTemplateID]
        , TenantID              AS [tenantID]
        , TenantName            AS [tenantName]
        , SourceTenantName      AS [sourceTenantName]
        , OfferType             AS [OfferType]
        , RewardType            AS [RewardType]
        , TargetUsersType       AS [TargetUsersType]
        , EditorTitle           AS [EditorTitle]
        , EditorDescription     AS [EditorDescription]
        , Title                 AS [Title]
        , Subtitle              AS [Subtitle]
        , [Description]         AS [Description]
        , ImageURL              AS [ImageURL]
        , ImagePos              AS [ImagePos]
        , [Order]               AS [Order]
        , Display               AS [Display]
        , StartDate             AS [StartDate]
        , EndDate               AS [EndDate]
        , IsFeaturedOffer       AS [IsFeaturedOffer]
        , IsDeleted             AS [IsDeleted]
        , IsActive              AS [IsActive]
        , IsDraft               AS [IsDraft]
        , IsRestricted          AS [IsRestricted]
        , IsDisplay             AS [IsDisplay]
        , IsBrandedOffer        AS [IsBrandedOffer]
        , MaxRedemptionAmount   AS [MaxRedemptionAmount]
        , MaxRedemption         AS [MaxRedemption]
        , AudienceCoveredCount  AS [AudienceCoveredCount]
        , RuleFilters           AS [RuleFilters]
        , InterstitialZones     AS [InterstitialZones]
        , Links                 AS [Links]
        , Categories            AS [Categories]
        , Campaigns             AS [Campaigns]
        , AppTenants            AS [AppTenants]
        , ProductDiscounts      AS [ProductDiscounts]
        , CreatedOn             AS [Meta.createdOn]
        , CreatedBy             AS [Meta.createdBy]
        , CreatedByUserID       AS [Meta.createdByUserID]
        , UpdatedOn             AS [Meta.updatedOn]
        , UpdatedBy             AS [Meta.updatedBy]
        , UpdatedByUserID       AS [Meta.updatedByUserID]
    FROM
        [Offer].[vGET_Offers]
    WHERE
        IsDeleted = 0
        AND IsActive = 1
        AND IsDraft = 0
        AND (
            (@IsDisplayPromotion = 0 AND Display <> 'promotion' COLLATE Latin1_General_CI_AI)
            OR (@IsDisplayPromotion = 1 AND Display = 'promotion' COLLATE Latin1_General_CI_AI)
        )
        AND (TargetUsersType = 'all' OR TargetUsersType = @TargetUsersType)
    FOR JSON PATH
END
