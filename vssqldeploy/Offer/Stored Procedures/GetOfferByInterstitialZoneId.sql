﻿
CREATE   PROCEDURE [Offer].[GetOfferByInterstitialZoneId] (
    @ZoneID     uniqueidentifier    = NULL,
    @TenantID   uniqueidentifier    = NULL,
    @IsDeleted  bit                 = NULL
) AS
BEGIN
    DECLARE @OfferID uniqueidentifier
    SET @OfferID = (SELECT OfferID FROM [ContentPush].[InterstitialZone] WHERE InterstitialZoneID = @ZoneID AND ((IsActive = 0 AND @IsDeleted IS NULL) OR IsActive = ~@IsDeleted))
    IF @OfferID IS NULL
        SELECT NULL
    ELSE
        EXEC [Offer].[GET_OfferModelV4] @OfferID = @OfferID, @TenantID = @TenantID, @IsDeleted = @IsDeleted
END
