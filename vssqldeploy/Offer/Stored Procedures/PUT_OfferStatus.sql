﻿CREATE   PROCEDURE [Offer].[PUT_OfferStatus] @OfferId uniqueidentifier, @IsActive bit, @UserId uniqueidentifier
AS
BEGIN
BEGIN TRY
    BEGIN TRANSACTION

	IF EXISTS(SELECT * FROM [Offer].[Offer] WHERE [OfferID] = @OfferId) AND
		NOT EXISTS (SELECT * FROM [Offer].[Offer] WHERE [OfferID] = @OfferId AND [IsActive] = @IsActive)
	BEGIN
		UPDATE	[Offer].[Offer]
		SET		[IsActive] = @IsActive,
				[UpdatedBy] = 'PUT_OfferStatus',
				[UpdatedOn] = GETUTCDATE(),
				[UpdatedByUserId] = @UserId
		WHERE	[OfferID] = @OfferId
	END

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(255) = ERROR_MESSAGE(), @ErrorSeverity int = ERROR_SEVERITY(), @ErrorState int = ERROR_STATE();
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), @ErrorMessage, @ErrorSeverity, 
	(SELECT @OfferId AS [OfferId], @IsActive AS [IsActive], @UserId AS [UserId] FOR JSON PATH)  AS [StoredProcedureInput]; 
    ROLLBACK TRANSACTION;
    EXEC [dbo].[PostStoredProcedureException] @Exceptions;

	RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
END CATCH
END
