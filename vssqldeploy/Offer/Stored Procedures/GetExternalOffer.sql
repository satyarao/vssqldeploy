﻿/****************************************GET EXTERNAL OFFER****************************************/
CREATE   PROCEDURE [Offer].[GetExternalOffer]
(
	@ExternalOfferId uniqueidentifier = null,
	@ReferenceId nvarchar(50) = null,
	@TenantId uniqueidentifier = null,
	@AuthorId nvarchar(50) = null
)
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY

	SELECT [ExternalOfferID]
		,[ReferenceID]
		,[TenantID]
		,[Description]
		,[AuthorID]
		,[StartDate]
		,[EndDate]
		,[IsConfigured]
		,[IsDeleted]
		,[CreatedOn]
		,[DeletedOn]
		,[ConfiguredOn]
	FROM [Offer].[ExternalOffer] 
	WHERE [ExternalOfferID] = ISNULL(@ExternalOfferId, [ExternalOfferID]) AND
		[ReferenceID] = ISNULL(@ReferenceId, [ReferenceID]) AND
		[TenantID] = ISNULL(@TenantId, [TenantID]) AND
		(@AuthorId IS NULL OR [AuthorID] = @AuthorId)
  
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT @ExternalOfferId AS [ExternalOfferID], @ReferenceId AS [ReferenceID], @TenantId AS [TenantID], @AuthorId AS [AuthorID] FOR JSON PATH) AS [StoredProcedureInput];
    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
