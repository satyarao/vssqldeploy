﻿



/****************************************UPSERT OFFER MODEL****************************************/
CREATE   PROCEDURE [Offer].[Upsert_OfferModelV5] 
(
	@OfferBase			[Offer].[OfferModelV5]			READONLY,
	@ProductDiscounts	[Offer].[ProductDiscountV5]		READONLY,
	@Links				[Offer].[OfferLinkV5]			READONLY,
	@AppTenants			[Offer].[OfferID_Guid_Tuple]	READONLY,
	@RuleFilters		[Offer].[OfferID_Guid_Tuple]	READONLY,
	@InterstitialZones	[Offer].[OfferID_Guid_Tuple]	READONLY,
	@Categories			[Offer].[OfferID_Guid_Tuple]	READONLY,
	@Campaigns			[Offer].[OfferID_Int_Tuple]		READONLY
) AS
BEGIN
	IF (SELECT COUNT(*) FROM @OfferBase) != 1
    BEGIN
        ;THROW 50005, 'There must be only one Offer for this procedure to proceed', 1;
    END

	SET NOCOUNT ON;
    SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	DECLARE @offerID uniqueidentifier = (SELECT TOP(1) [OfferID] FROM @OfferBase);
	DECLARE @TenantID uniqueidentifier = (SELECT TOP(1) [TenantID] FROM @OfferBase);

	MERGE INTO [Offer].[Offer] AS target
	USING @OfferBase AS source
	ON source.[OfferID] = target.[OfferID]
	WHEN MATCHED THEN
		UPDATE SET
			target.[OfferType]			= source.[OfferType],
			target.[RewardType]			= source.[RewardType],
			target.[TargetUsersType]	= source.[TargetUsersType],
			target.[EditorTitle]		= source.[EditorTitle],
			target.[EditorDescription]	= source.[EditorDescription],
			target.[Title]				= source.[Title],
			target.[Subtitle]			= source.[Subtitle],
			target.[Description]		= source.[Description],
			target.[ImageURL]			= source.[ImageURL],
			target.[ImagePos]			= source.[ImagePos],
			target.[Order]				= source.[Order],
			target.[IsFeaturedOffer]	= source.[IsFeaturedOffer],
			target.[IsDeleted]			= source.[IsDeleted],
			target.[IsActive]			= source.[IsActive],
			target.[IsDraft]			= source.[IsDraft],
			target.[IsRestricted]		= source.[IsRestricted],
			target.[IsDisplay]			= source.[IsDisplay],
			target.[IsBrandedOffer]		= source.[IsBrandedOffer],
			target.[Display]			= source.[Display],
			target.[StartDate]			= source.[StartDate],
			target.[EndDate]			= source.[EndDate],
			target.[UpdatedOn]			= GETUTCDATE(),
			target.[UpdatedBy]			= ISNULL(source.[UpdatedBy], 'SQL'),
			target.[UpdatedByUserID]	= source.[UpdatedByUserID],
			target.[RewardID]			= source.[RewardID]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([OfferID],[AdvertOfferID],[AutoUpdateAdvert],[OfferTemplateID],[TenantID],[RewardID],[OfferType]
			,[RewardType],[TargetUsersType],[EditorTitle],[EditorDescription],[Title],[Subtitle],[Description]
			,[ImageURL],[ImagePos],[Order],[IsFeaturedOffer],[IsDeleted],[IsActive],[IsDraft],[IsRestricted]
			,[IsDisplay],[IsBrandedOffer],[Display],[StartDate],[EndDate],[CreatedOn],[CreatedBy],[CreatedByUserID])
		VALUES
			([OfferID],[AdvertOfferID],[AutoUpdateAdvert],[OfferTemplateID],[TenantID],[RewardID],[OfferType]
			,[RewardType],[TargetUsersType],[EditorTitle],[EditorDescription],[Title],[Subtitle],[Description]
			,[ImageURL],[ImagePos],[Order],[IsFeaturedOffer],[IsDeleted],[IsActive],[IsDraft],[IsRestricted]
			,[IsDisplay],[IsBrandedOffer],[Display],[StartDate],[EndDate],[CreatedOn],[CreatedBy],[CreatedByUserID]);

	--****************************************MAX REDEMPTION****************************************

	DELETE FROM [Offer].[LkOffer_ProductDiscount] WHERE [OfferID] = @OfferID

	INSERT INTO [Offer].[ProductDiscount]
		([ProductDiscountID],[StandardProductGroupID],[DiscountType],[DiscountValue]
		,[DiscountCurrency],[NewUnitPrice],[CouponPosId],[IsStackable])
	SELECT [ProductDiscountID],
		[StandardProductGroupID],
		[DiscountType],
		[DiscountValue],
		[DiscountCurrency],
		[NewUnitPrice],
		[CouponPosId],
		[IsStackable]
	FROM @ProductDiscounts

	INSERT INTO [Offer].[LkOffer_ProductDiscount]
		([OfferID],[ProductDiscountID])
	SELECT [OfferID],
		[ProductDiscountID]
	FROM @ProductDiscounts

	

	DELETE FROM [Offer].[LkOffer_Link] WHERE [OfferID] = @OfferID

	INSERT INTO [Offer].[Link]
		([LinkID],[LinkText],[LinkType],[Url])
	SELECT [LinkID],
		[LinkText],
		[LinkType],
		[Url]
	FROM @Links

	INSERT INTO [Offer].[LkOffer_Link]
		([OfferID],[LinkID])
	SELECT [OfferID],
		[LinkID]
	FROM @Links

	DELETE FROM [Offer].[DiscountLimit] WHERE [ProductDiscountID] IN (SELECT [ProductDiscountID] FROM @ProductDiscounts);
	INSERT INTO [Offer].[DiscountLimit]
		([ProductDiscountID],[DiscountLimitType],[MinimumQuantity],[MaximumQuantity],[IsActive],[CreatedOn])
	SELECT [ProductDiscountID],
		[DiscountLimitType],
		[MinimumQuantity],
		[MaximumQuantity],
		1 AS [IsActive],
		GETUTCDATE() AS [CreatedOn]
	FROM @ProductDiscounts
	WHERE [DiscountLimitType] IS NOT NULL

	DELETE FROM [Offer].[LkOffer_MaxRedemption] WHERE [OfferID] = @OfferID
	IF (SELECT TOP(1) [UserMaxRedemptions] FROM @OfferBase) IS NOT NULL
	BEGIN
		DECLARE @userMaxRedemptionID uniqueidentifier = NEWID();

		INSERT INTO [Offer].[MaxRedemption]
			([MaxRedemptionID],[RedemptionType],[MaxRedemptions],[RollingWindowDays],[RollingWindowWeeks],[RollingWindowMonths],[RollingWindowYears],[IsActive],[CreatedOn])
		SELECT @userMaxRedemptionID						[MaxRedemptionID], 
			'user'										[RedemptionType], 
			[UserMaxRedemptions]						[MaxRedemptions],
			[UserMaxRedemptionRollingWindowDays]		[RollingWindowDays],
			[UserMaxRedemptionRollingWindowWeeks]		[RollingWindowWeeks],
			[UserMaxRedemptionRollingWindowMonths]		[RollingWindowMonths],
			[UserMaxRedemptionRollingWindowYears]		[RollingWindowYears],
			1											[IsActive],
			GETUTCDATE()								[CreatedOn]
		FROM @OfferBase

		INSERT INTO [Offer].[LkOffer_MaxRedemption]
			([OfferID],[MaxRedemptionID],[IsActive],[CreatedOn])
		VALUES
			(@offerId, @userMaxRedemptionID, 1, GETUTCDATE())
	END
	IF (SELECT TOP(1) [OfferMaxRedemptions] FROM @OfferBase) IS NOT NULL
	BEGIN
		DECLARE @offerMaxRedemptionID uniqueidentifier = NEWID();

		INSERT INTO [Offer].[MaxRedemption]
			([MaxRedemptionID],[RedemptionType],[MaxRedemptions],[RollingWindowDays],[RollingWindowWeeks],[RollingWindowMonths],[RollingWindowYears],[IsActive],[CreatedOn])
		SELECT @offerMaxRedemptionID					[MaxRedemptionID], 
			'offer'										[RedemptionType], 
			[OfferMaxRedemptions]						[MaxRedemptions],
			[OfferMaxRedemptionRollingWindowDays]		[RollingWindowDays],
			[OfferMaxRedemptionRollingWindowWeeks]		[RollingWindowWeeks],
			[OfferMaxRedemptionRollingWindowMonths]		[RollingWindowMonths],
			[OfferMaxRedemptionRollingWindowYears]		[RollingWindowYears],
			1											[IsActive],
			GETUTCDATE()								[CreatedOn]
		FROM @OfferBase

		INSERT INTO [Offer].[LkOffer_MaxRedemption]
			([OfferID],[MaxRedemptionID],[IsActive],[CreatedOn])
		VALUES
			(@offerId, @offerMaxRedemptionID, 1, GETUTCDATE())
	END

	DELETE FROM [Offer].[LkOffer_AppTenant] WHERE [OfferID] = @OfferID
	INSERT INTO [Offer].[LkOffer_AppTenant]
		([OfferID],[AppTenantID])
	SELECT [OfferID],
		[GuidValue] AS [AppTenantID]
	FROM @AppTenants

	DELETE FROM [Offer].[LkOffer_RuleFIlter] WHERE [OfferID] = @OfferID
	INSERT INTO [Offer].[LkOffer_RuleFIlter]
		([OfferID],[RuleFilterID])
	SELECT [OfferID], 
		[GuidValue] AS [RuleFilterID]
	FROM @RuleFilters

	DELETE FROM [Offer].[LkOffer_InterstitialZone] WHERE [OfferID] = @OfferID
	INSERT INTO [Offer].[LkOffer_InterstitialZone]
		([OfferID],[InterstitialZoneID])
	SELECT [OfferID], 
		[GuidValue] AS [RuleFilterID]
	FROM @InterstitialZones

	DELETE FROM [Offer].[LkOffer_Category] WHERE [OfferID] = @OfferID
	INSERT INTO [Offer].[LkOffer_Category]
		([OfferID],[OfferCategoryID])
	SELECT [OfferID], 
		[GuidValue] AS [OfferCategoryID]
	FROM @Categories

	DELETE FROM [Offer].[LkOffer_Campaign] WHERE [OfferID] = @OfferID
	INSERT INTO [Offer].[LkOffer_Campaign]
		([OfferID],[CampaignID])
	SELECT [OfferID], 
		[IntValue] AS [CampaignID]
	FROM @Campaigns

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT 
		(SELECT * FROM @OfferBase FOR JSON PATH) [OfferBase],
		(SELECT * FROM @ProductDiscounts FOR JSON PATH) [ProductDiscounts],
		(SELECT * FROM @Links FOR JSON PATH) [Links],
		(SELECT * FROM @AppTenants FOR JSON PATH) [AppTenants],
		(SELECT * FROM @RuleFilters FOR JSON PATH) [RuleFilters],
		(SELECT * FROM @InterstitialZones FOR JSON PATH) [InterstitialZones],
		(SELECT * FROM @Categories FOR JSON PATH) [Categories],
		(SELECT * FROM @Campaigns FOR JSON PATH) [Campaigns]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
