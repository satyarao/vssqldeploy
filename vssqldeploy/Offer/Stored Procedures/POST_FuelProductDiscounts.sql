﻿
CREATE   PROCEDURE [Offer].[POST_FuelProductDiscounts]
    @OfferId uniqueidentifier,
	@ProductRewardJson nvarchar(max), 
	@MaxFuelAmount decimal(6,3)= null
AS
BEGIN
	BEGIN TRY
		DECLARE @ProductRewards [Offer].[DiscountInfoModel];
		DECLARE @ProductDiscounts AS TABLE (
				[ProductDiscountID]         uniqueidentifier,
				[StandardProductGroupID]    uniqueidentifier,
				[DiscountType]              nvarchar(50),
				[DiscountValue]             decimal(7, 3),
				[IsStackable]               bit
			);

		INSERT INTO @ProductRewards
		SELECT [discountAmount],[productCode],[productCodeType],[discountAmountType]
		FROM OPENJSON(@ProductRewardJson)
		WITH
		(
			[discountAmount]        decimal(7,3),
			[productCode]           nvarchar(50),
			[productCodeType]       nvarchar(50),
			[discountAmountType]    nvarchar(50)
		);

		INSERT INTO @ProductDiscounts
		SELECT      NEWID() [ProductDiscountID],
					spg.[StandardProductGroupID],
					pr.[discountAmountType] [DiscountType],
					pr.[discountAmount] [DiscountValue],
					1 [IsStackable]
		FROM        @ProductRewards pr
		INNER JOIN  [Product].[StandardProductGroup] spg
		ON          spg.[GroupCode] = pr.[ProductCode] OR
					TRY_CONVERT(int, spg.[GroupCode]) = pr.[ProductCode]

		INSERT INTO [Offer].[ProductDiscount]
			([ProductDiscountID],[StandardProductGroupID],[DiscountType],[DiscountValue],[IsStackable])
		SELECT
			[ProductDiscountID],[StandardProductGroupID],[DiscountType],[DiscountValue],[IsStackable]
		FROM @ProductDiscounts

		IF(@MaxFuelAmount IS NOT NULL AND @MaxFuelAmount > 0)
		BEGIN
			INSERT INTO [Offer].[DiscountLimit]
				([ProductDiscountID],[DiscountLimitType],[MaximumQuantity])
			SELECT
				[ProductDiscountID], 'volume', @MaxFuelAmount
			FROM @ProductDiscounts
		END
		
		SELECT (SELECT ProductDiscountID
				FROM @ProductDiscounts 
				FOR JSON PATH) AS [ProductDiscountIDJson]
		FOR JSON PATH
	END TRY
	BEGIN CATCH
		DECLARE @Exceptions [StoredProcedureExceptionType];
		INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
		SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
			(
				SELECT @OfferId 		  AS OfferID,
					   @ProductRewardJson AS ProductRewardJson,
					   @MaxFuelAmount 	  AS MaxFuelAmount
				FOR JSON PATH
			)			AS [StoredProcedureInput];

		--ROLLBACK TRANSACTION;

		EXEC [dbo].[PostStoredProcedureException] @Exceptions;
	END CATCH
END
