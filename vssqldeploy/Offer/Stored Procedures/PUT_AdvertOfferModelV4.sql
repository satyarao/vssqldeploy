﻿
CREATE   PROCEDURE [Offer].[PUT_AdvertOfferModelV4] (
    @json nvarchar(max)
) AS
BEGIN
    EXEC [Offer].[Serialize_AdvertOfferModelV4] @json, @isUpdate=1, @isInsert=0
END
