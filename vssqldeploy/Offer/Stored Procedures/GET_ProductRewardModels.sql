﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   PROCEDURE [Offer].[GET_ProductRewardModels] @TenantId uniqueidentifier, @StoreId uniqueidentifier, @UserId uniqueidentifier
AS
BEGIN
	
	DECLARE @TimeZone INT	= (SELECT TOP(1) [TimeZoneID] FROM [dbo].[Store] WHERE [StoreID] = @StoreId);

	WITH [SetReduction] AS (
		SELECT orf.[OfferID]
		FROM		[Offer].[GetActiveOffers]( @TenantId, @TimeZone ) orf
		INNER JOIN	[Offer].[UserOfferMatching] uom WITH(NOLOCK)
		ON			uom.[OfferID] = orf.[OfferID] AND
					uom.[UserID] = @UserID
		INNER JOIN	[Offer].[StoreOfferMatching] som WITH(NOLOCK)
		ON			som.[OfferID] = orf.[OfferID] AND
					som.[StoreID] = @StoreId
		GROUP BY orf.[OfferID]
	)

	SELECT		[Id],
				[TenantId],
				[StartDate],
				[EndDate],
				[OfferType],
				[ProductCode],
				[ProductCodeType],
				[MaxRedemptions],
				[MaxUserRedemptions],
				[Value],
				[IsActive],
				[IsDeleted],
				[DisplayType],
				[RuleFilters],
				[AppTenants],
				[DiscountInfo]
	FROM		[SetReduction] s
	INNER JOIN [Offer].[vProductRewardModel] prm
	ON			prm.[Id] = s.[OfferID]
	WHERE		[DiscountInfo] IS NOT NULL
	FOR JSON PATH
	
END
