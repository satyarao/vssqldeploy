﻿CREATE   PROCEDURE [Offer].[ProcessAsdRequest] @json nvarchar(max) AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	DECLARE @ExceptionMessage nvarchar(max);

    DECLARE @AsdRequest [Txn].[AboveSiteDiscountRequest],
			@SaleItem [Txn].[V1SaleItemType];
    
    INSERT INTO @AsdRequest
        ([StoreID],[TransactionID],[UserID],[RequestTime])        
    SELECT * FROM OPENJSON(@json)
    WITH
    (
        [StoreID]			uniqueidentifier,
        [TransactionID]		uniqueidentifier,
        [UserID]			uniqueidentifier,
        [RequestTime]		datetime2(7)
    );

    IF ((SELECT COUNT(*) FROM @AsdRequest) != 1)
		BEGIN
			SET @ExceptionMessage = CONCAT('Recieved [',(SELECT COUNT(*) FROM @AsdRequest),'] Request(s) instead of 1');
			THROW 5001, @ExceptionMessage, 0
		END

    IF NOT EXISTS(
        SELECT	[StoreID]
		FROM	@AsdRequest 
        WHERE	[StoreID] IS NOT NULL AND 
				[TransactionID] IS NOT NULL AND 
				[UserID] IS NOT NULL
	)
		BEGIN
			SET @ExceptionMessage = 'Cannot have null StoreID, TransactionID or UserID in this request';
			THROW 5001, @ExceptionMessage, 0
		END

    INSERT INTO @SaleItem
		([StandardProductCodeID],[PosCode],[EvaluateOnly])      
	SELECT	[StandardProductCodeID],
			[PosCode],
			[EvaluateOnly]
	FROM	OPENJSON(@json, 'lax $.SaleItems')
	WITH
	(
		[StandardProductCodeID]     nvarchar(14),
		[PosCode]                   nvarchar(20),
		[EvaluateOnly]              bit
	);

	/*******************************************************************************************************************************************/

    DECLARE @AppTenant uniqueidentifier = (SELECT TOP(1) [TenantID] FROM [dbo].[UserInfo] u INNER JOIN @AsdRequest r ON u.UserID = r.UserID);
    DECLARE @StoreID uniqueidentifier   = (SELECT TOP(1) [StoreID] FROM @AsdRequest);
    DECLARE @UserID uniqueidentifier    = (SELECT TOP(1) [UserID] FROM @AsdRequest);
    DECLARE @TimeZone INT				= (SELECT TOP(1) [TimeZoneID] FROM [dbo].[Store] WHERE [StoreID] = @StoreID);

	

	SELECT		ofr.[OfferType] AS [offerType],
				ofr.[RewardType] AS [rewardType],
				ofr.[OfferID] AS[offerId],
				ofr.[Title] AS [title],
				ofr.[RewardID] AS [RewardID],
				TRY_CONVERT(NVARCHAR(200), (SELECT TOP(1) [ReferenceId] FROM [Offer].[ExternalOffer] eo WITH(NOLOCK) WHERE eo.[ExternalOfferId] = ofr.[OfferID])) AS [externalOfferId],
				(SELECT TOP(1) [Description] FROM [Offer].[ExternalOffer] eo WITH(NOLOCK) WHERE eo.[ExternalOfferId] = ofr.[OfferID]) AS [externalDescription],
				pd.[DiscountType] AS [discountType],
				pd.[NewUnitPrice] AS [unitPrice],
				pd.[DiscountValue] AS [amount],
				(
					SELECT spg.[StandardProductCodeID] AS [standardProductCode],
						si.[PosCode] AS [posCode]
					FROM [Product].[LKStandardProductGroup] spg WITH(NOLOCK) 
					INNER JOIN @SaleItem si ON si.[StandardProductCodeID] = spg.[StandardProductCodeID]
					WHERE spg.StandardProductGroupID = pd.StandardProductGroupID
					FOR JSON PATH
				) [targetCodes],
				pd.[IsStackable] AS [isStackable],
				(
					SELECT dl.[DiscountLimitType] AS [discountLimitType], dl.[MinimumQuantity] AS [minimumQuantity], dl.[MaximumQuantity] AS [maximumQuantity]
					FROM [Offer].[DiscountLimit] dl WITH(NOLOCK) 
					WHERE dl.[ProductDiscountID] = pd.[ProductDiscountID] AND dl.[IsActive] = 1
					FOR JSON PATH
				)	[discountLimits],
				pd.[CouponPosId] AS [couponPosId]
	FROM		[Offer].[GetActiveOffers]( @AppTenant, @TimeZone ) orf
	INNER JOIN	[Offer].[UserOfferMatching] uom WITH(NOLOCK)
	ON			uom.[OfferID] = orf.[OfferID] AND
				uom.[UserID] = @UserID
	INNER JOIN	[Offer].[StoreOfferMatching] som WITH(NOLOCK)
	ON			som.[OfferID] = orf.[OfferID] AND
				som.[StoreID] = @StoreID
	INNER JOIN	[Offer].[Offer] ofr WITH(NOLOCK)
	ON			ofr.[OfferID] = orf.[OfferID]
	INNER JOIN [Offer].[LkOffer_ProductDiscount] lkPd WITH(NOLOCK)
	ON			orf.[OfferID] = lkPd.[OfferID]
	INNER JOIN [Offer].[ProductDiscount] pd WITH(NOLOCK)
	ON			pd.[ProductDiscountID] = lkPd.[ProductDiscountID]
	FOR JSON PATH
	

    COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage nvarchar(255) = ERROR_MESSAGE(), @ErrorSeverity int = ERROR_SEVERITY(), @ErrorState int = ERROR_STATE();
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), @ErrorMessage, @ErrorSeverity, @json AS [StoredProcedureInput]; 
    ROLLBACK TRANSACTION;
    EXEC [dbo].[PostStoredProcedureException] @Exceptions;

	RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
END CATCH
END


