﻿





/****************************************GET OFFER****************************************/
CREATE   PROCEDURE [Offer].[GET_ReadOfferResponseV5]
(
	@TenantId uniqueidentifier,
	@OfferId uniqueidentifier
) AS
BEGIN
	SET NOCOUNT ON;
    SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	SELECT ofr.[OfferID]		[offerId],
		ofr.[AdvertOfferID]		[advertOfferId],
		ofr.[AutoUpdateAdvert]	[autoUpdateAdvert],
		ofr.[OfferTemplateID]	[offerTemplateId],
		ofr.[RewardID]			[rewardId],
		ofr.[OfferType]			[offerType],
		ofr.[RewardType]		[rewardType],
		ofr.[TargetUsersType]	[targetUsersType],
		ofr.[EditorTitle]		[editorTitle],
		ofr.[EditorDescription]	[editorDescription],
		ofr.[Title]				[title],
		ofr.[Subtitle]			[subtitle],
		ofr.[Description]		[description],
		ofr.[ImagePos]			[imagePos],
		ofr.[Order]				[order],
		ofr.[IsFeaturedOffer]	[isFeaturedOffer],
		ofr.[IsDraft]			[isDraft],
		ofr.[IsRestricted]		[isRestricted],
		ofr.[IsDisplay]			[isDisplay],
		ofr.[IsBrandedOffer]	[isBrandedOffer],
		ofr.[Display]			[display],
		ofr.[StartDate]			[startDate],
		ofr.[EndDate]			[endDate],
		(
			SELECT  
			JSON_QUERY('[' + STUFF(( SELECT ',' + '"' + CAST(apt.[AppTenantID] AS nvarchar(50)) + '"' 
			FROM [Offer].[LkOffer_AppTenant]  apt
			WHERE apt.[OfferID] = ofr.[OfferID]
			FOR XML PATH('')),1,1,'') + ']' )
		) 
		AS [appTenants],
		JSON_QUERY((
			SELECT mr.[MaxRedemptions]		[maxRedemptions],
				mr.[RollingWindowDays]		[rollingWindowDays],
				mr.[RollingWindowWeeks]		[rollingWindowWeeks],
				mr.[RollingWindowMonths]	[rollingWindowMonths],
				mr.[RollingWindowYears]		[rollingWindowYears]
			FROM [Offer].[MaxRedemption] mr
			INNER JOIN [Offer].[LkOffer_MaxRedemption] lkMr
				ON lkMr.[MaxRedemptionID] = mr.[MaxRedemptionID]
			WHERE lkMr.[OfferID] = ofr.[OfferID] AND mr.[RedemptionType] = 'user'
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		))
		AS [userMaxRedemption],
		JSON_QUERY((
			SELECT mr.[MaxRedemptions]		[maxRedemptions],
				mr.[RollingWindowDays]		[rollingWindowDays],
				mr.[RollingWindowWeeks]		[rollingWindowWeeks],
				mr.[RollingWindowMonths]	[rollingWindowMonths],
				mr.[RollingWindowYears]		[rollingWindowYears]
			FROM [Offer].[MaxRedemption] mr
			INNER JOIN [Offer].[LkOffer_MaxRedemption] lkMr
				ON lkMr.[MaxRedemptionID] = mr.[MaxRedemptionID]
			WHERE lkMr.[OfferID] = ofr.[OfferID] AND mr.[RedemptionType] = 'offer'
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		))
		AS [offerMaxRedemption],
		(
			SELECT pd.[StandardProductGroupID]	[standardProductGroupId],
				pd.[DiscountType]				[discountType],
				pd.[DiscountValue]				[discountValue],
				pd.[DiscountCurrency]			[discountCurrency],
				pd.[NewUnitPrice]				[newUnitPrice],
				pd.[CouponPosId]				[couponPosId],
				JSON_QUERY((
					SELECT TOP(1) dl.[DiscountLimitType],
						dl.[MinimumQuantity],
						dl.[MaximumQuantity]
					FROM [Offer].[DiscountLimit] dl
					WHERE dl.[ProductDiscountID] = pd.[ProductDiscountID]
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
				))
				AS [discountLimit],
				[IsStackable]					[isStackable]
			FROM [Offer].[ProductDiscount] pd
			INNER JOIN [Offer].[LkOffer_ProductDiscount] lkPd
				ON lkPd.[ProductDiscountID] = pd.[ProductDiscountID]
			WHERE lkPd.[OfferID] = ofr.[OfferID]
			FOR JSON PATH
		)
		AS [productDiscounts],
		(
			SELECT lnk.[LinkText]	[linkText],
				lnk.[LinkType]		[linkType],
				lnk.[Url]			[url]
			FROM [Offer].[Link] lnk
			INNER JOIN [Offer].[LkOffer_Link] lKlnk
				ON lkLnk.[LinkID] = lnk.[LinkID]
			WHERE lkLnk.[OfferID] = ofr.[OfferID]
			FOR JSON PATH
		)
		AS [links],
		(
			SELECT  
			JSON_QUERY('[' + STUFF(( SELECT ',' + '"' + CAST(rf.[RuleFilterID] AS nvarchar(50)) + '"' 
			FROM [Offer].[LkOffer_RuleFilter]  rf
			WHERE rf.[OfferID] = ofr.[OfferID]
			FOR XML PATH('')),1,1,'') + ']' )
		) 
		AS [ruleFilters],
		(
			SELECT  
			JSON_QUERY('[' + STUFF(( SELECT ',' + '"' + CAST(iz.[InterstitialZoneID] AS nvarchar(50)) + '"' 
			FROM [Offer].[LkOffer_InterstitialZone]  iz
			WHERE iz.[OfferID] = ofr.[OfferID]
			FOR XML PATH('')),1,1,'') + ']' )
		) 
		AS [interstitialZones],
		(
			SELECT  
			JSON_QUERY('[' + STUFF(( SELECT ',' + '"' + CAST(cat.[OfferCategoryID] AS nvarchar(50)) + '"' 
			FROM [Offer].[LkOffer_Category]  cat
			WHERE cat.[OfferID] = ofr.[OfferID]
			FOR XML PATH('')),1,1,'') + ']' )
		) 
		AS [categories],
		(
			SELECT  
			JSON_QUERY('[' + STUFF(( SELECT ',' + CAST(cmp.[CampaignID] AS nvarchar(50))
			FROM [Offer].[LkOffer_Campaign]  cmp
			WHERE cmp.[OfferID] = ofr.[OfferID]
			FOR XML PATH('')),1,1,'') + ']' )
		) 
		AS [campaigns],
		ofr.[ImageURL]		[imageUrl]
	FROM [Offer].[Offer] ofr
	WHERE ofr.[TenantID] = @TenantId AND ofr.[OfferID] = @OfferId
	FOR JSON PATH

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @TenantId [TenantId], @OfferId [OfferId] FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
