﻿


CREATE   PROCEDURE [Offer].[Refresh_UserOfferMatching] ( @UserID uniqueidentifier = NULL, @OfferId uniqueidentifier = NULL )
AS
BEGIN
	MERGE [Offer].[UserOfferMatching] AS target
	USING [Offer].[UserOfferMapping]( @OfferId, @UserID ) AS source
	ON target.[OfferID] = source.[OfferID] AND target.[UserID] = source.[UserID]
	WHEN NOT MATCHED BY target THEN
		INSERT
			([UserID],[OfferID])
		VALUES
			([UserID],[OfferID])
	WHEN	NOT MATCHED BY source AND 
			(@UserID IS NULL OR target.[UserID] = @UserID) AND
			(@OfferId IS NULL OR target.[OfferID] = @OfferId)
	THEN	DELETE;
END
