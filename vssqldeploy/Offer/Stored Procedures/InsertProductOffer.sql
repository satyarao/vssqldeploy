﻿CREATE   PROCEDURE [Offer].[InsertProductOffer]
	@OfferID 		   uniqueidentifier,
	@ProductDiscountIDJson nvarchar(max)
AS
BEGIN
	BEGIN TRY
		IF IsJson(@ProductDiscountIDJson) = 0
		BEGIN
			;THROW 50001, 'Must provide ProductDiscountID JSON', 1;
		END
		
		DECLARE @PreviousDiscountIds AS TABLE (
			[ProductDiscountID]			uniqueidentifier
		);

		INSERT INTO @PreviousDiscountIds
		SELECT [ProductDiscountID]
		FROM [Offer].[LkOffer_ProductDiscount] WHERE [OfferID] = @OfferId;

		WITH DiscountIDToInsert (OfferID, ProductDiscountID) AS
		(
			SELECT @OfferID AS OfferID, ProductDiscountID
			FROM OPENJSON(@ProductDiscountIDJson)
			WITH
			(
				[ProductDiscountIDJson] nvarchar(max) AS json
			) AS Discounts
			CROSS APPLY OPENJSON(Discounts.ProductDiscountIDJson)
			WITH
			(
				[ProductDiscountID]       [uniqueidentifier]  '$.ProductDiscountID'
			) AS DiscountID
		)
		
		INSERT INTO [Offer].[LkOffer_ProductDiscount] 
		(OfferID, ProductDiscountID)
		(SELECT OfferID, ProductDiscountID FROM DiscountIDToInsert)

		DELETE FROM [Offer].[DiscountLimit]	WHERE [ProductDiscountID] IN (SELECT [ProductDiscountID] FROM @PreviousDiscountIds);
		DELETE FROM [Offer].[LkOffer_ProductDiscount] WHERE [ProductDiscountID] IN (SELECT [ProductDiscountID] FROM @PreviousDiscountIds);
		DELETE FROM [Offer].[ProductDiscount] WHERE [ProductDiscountID] IN (SELECT [ProductDiscountID] FROM @PreviousDiscountIds);
	END TRY
	BEGIN CATCH
	    DECLARE @Exceptions [StoredProcedureExceptionType];
		INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
		SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
			(
				SELECT @OfferID 			  AS OfferID,
					   @ProductDiscountIDJson AS ProductDiscountIDJson
				FOR JSON PATH
			)			AS [StoredProcedureInput];

		--ROLLBACK TRANSACTION;

		EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
	END CATCH
END
