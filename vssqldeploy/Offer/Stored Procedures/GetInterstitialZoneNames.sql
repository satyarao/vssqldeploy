﻿CREATE   PROCEDURE [Offer].[GetInterstitialZoneNames]
	@TenantID uniqueidentifier
AS
BEGIN
	SELECT ZoneID, ZoneName FROM [dbo].[InterstitialZoneInfo] WHERE TenantID = @TenantID
END
