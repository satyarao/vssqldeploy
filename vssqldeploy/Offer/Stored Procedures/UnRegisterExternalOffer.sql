﻿

/****************************************UN-REGISTER EXTERNAL OFFER****************************************/
CREATE   PROCEDURE [Offer].[UnRegisterExternalOffer]
(
	@ExternalOfferId uniqueidentifier,
	@ReferenceId nvarchar(50),
	@TenantId uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY
	BEGIN TRANSACTION

		IF EXISTS(SELECT [ExternalOfferId] FROM [Offer].[ExternalOffer] WHERE ([ExternalOfferID] = @ExternalOfferId AND [ReferenceID] = @ReferenceId AND [TenantID] = @TenantId))
			BEGIN
				UPDATE [Offer].[ExternalOffer]
				SET [IsDeleted] = 1
					,[DeletedOn] = GETUTCDATE()
				WHERE ([ExternalOfferID] = @ExternalOfferId AND [ReferenceID] = @ReferenceId AND [TenantID] = @TenantId)
			END
		ELSE
			BEGIN
				;THROW 50001, 'Offer not found', 1;
			END

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT @ExternalOfferId AS [ExternalOfferID],@ReferenceId AS [ReferenceID], @TenantId AS [TenantID] FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
