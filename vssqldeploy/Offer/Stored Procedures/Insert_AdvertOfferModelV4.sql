﻿
CREATE   PROCEDURE [Offer].[Insert_AdvertOfferModelV4] (
    @AdvertOffer        [Offer].[AdvertOfferUpsertSqlModelV4Type]   READONLY,
    @MaxRedemption      [Offer].[MaxRedemptionSqlModelType]         READONLY,
    @Link               [Offer].[OfferModelV4LinkType]              READONLY,
    @RuleFilter         [Offer].[AdvertOfferUpsertV4RuleFilterType] READONLY,
    @ProductDiscount    [Offer].[ProductDiscountSqlModelType]       READONLY,
    @Category           [Offer].[AdvertOfferUpsertV4CategoryType]   READONLY,
    @Campaign           [Offer].[AdvertOfferUpsertV4CampaignType]   READONLY
) AS
BEGIN
    SET NOCOUNT ON
    --SET XACT_ABORT ON

    IF EXISTS (SELECT * FROM [Offer].[AdvertOffer] db INNER JOIN @AdvertOffer v ON db.[AdvertOfferID] = v.[AdvertOfferID])
    BEGIN
        ;THROW 50003, 'Attempting to insert duplicate record', 1;
    END

BEGIN TRY
    BEGIN TRANSACTION

    INSERT INTO [Offer].[AdvertOffer] (
        [AdvertOfferID]
        , [OfferTemplateID]
        , [TenantID]
        , [OfferType]
        , [RewardType]
        , [TargetUsersType]
        , [EditorTitle]
        , [EditorDescription]
        , [Title]
        , [Subtitle]
        , [Description]
        , [ImageURL]
        , [ImagePos]
        , [Order]
        , [IsFeaturedOffer]
        , [IsDeleted]
        , [IsActive]
        , [IsDraft]
        , [IsRestricted]
        , [IsDisplay]
        , [IsBrandedOffer]
        , [Display]
        , [StartDate]
        , [EndDate]
        , [CreatedBy]
        , [CreatedOn]
        , [CreatedByUserID]
    )
    SELECT
        [AdvertOfferID]
        , [OfferTemplateID]
        , [TenantID]
        , [OfferType]
        , [RewardType]
        , [TargetUsersType]
        , [EditorTitle]
        , [EditorDescription]
        , [Title]
        , [Subtitle]
        , [Description]
        , [ImageURL]
        , [ImagePos]
        , [Order]
        , [IsFeaturedOffer]
        , [IsDeleted]
        , [IsActive]
        , [IsDraft]
        , [IsRestricted]
        , [IsDisplay]
        , [IsBrandedOffer]
        , [Display]
        , [StartDate]
        , [EndDate]
        , [CreatedBy]
        , [CreatedOn]
        , [CreatedByUserID]
    FROM @AdvertOffer

    IF EXISTS (SELECT * FROM @MaxRedemption)
    BEGIN
        INSERT INTO [Offer].[MaxRedemption]
        (
            [MaxRedemptionID],
            [RedemptionType],
            [MaxRedemptions],
            [RollingWindowDays],
            [RollingWindowWeeks],
            [RollingWindowMonths],
            [RollingWindowYears],
            [IsActive],
            [CreatedOn]
        )
        SELECT
            [MaxRedemptionID],
            [RedemptionType],
            [MaxRedemptions],
            [RollingWindowDays],
            [RollingWindowWeeks],
            [RollingWindowMonths],
            [RollingWindowYears],
            1 AS IsActive,
            [CreatedOn]
        FROM @MaxRedemption
        INSERT INTO [Offer].[LkAdvertOffer_MaxRedemption] ([MaxRedemptionID], [AdvertOfferID])
            SELECT [MaxRedemptionID], [OfferID] FROM @MaxRedemption
    END

    IF EXISTS (SELECT * FROM @Link)
    BEGIN
        INSERT INTO [Offer].[Link]
        (
            [LinkID],
            [LinkText],
            [LinkType],
            [Url]
        )
        SELECT
            [LinkID],
            [LinkText],
            [LinkType],
            [Url]
        FROM @Link
        INSERT INTO [Offer].[LkAdvertOffer_Link] ([LinkID], [AdvertOfferID])
            SELECT [LinkID], [OfferID] FROM @Link
    END

    IF EXISTS (SELECT * FROM @RuleFilter)
    BEGIN
        INSERT INTO [Offer].[LkAdvertOffer_RuleFilter] ([RuleFilterID], [AdvertOfferID])
        SELECT [RuleFilterId], [AdvertOfferID] FROM @RuleFilter
    END

    IF EXISTS (SELECT * FROM @ProductDiscount)
    BEGIN
        INSERT INTO [Offer].[ProductDiscount] ([ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], [IsActive], IsStackable)
            SELECT                             [ProductDiscountID], [ProductRewardID], [StandardProductGroupID], [DiscountType], [DiscountValue], [DiscountCurrency], [NewUnitPrice], 1         , 0
            FROM @ProductDiscount
        INSERT INTO [Offer].[DiscountLimit] ([ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], [IsActive], [CreatedOn])
            SELECT                           [ProductDiscountID], [DiscountLimitType], [MinimumQuantity], [MaximumQuantity], 1         , [CreatedOn]
            FROM @ProductDiscount
        INSERT INTO [Offer].[LkAdvertOffer_ProductDiscount] ([ProductDiscountID], [AdvertOfferID])
            SELECT [ProductDiscountID], [OfferID] FROM @ProductDiscount
    END

    IF EXISTS (SELECT * FROM @Category)
    BEGIN
        INSERT INTO [Offer].[LkAdvertOffer_Category]
            (AdvertOfferID, OfferCategoryID, IsActive, CreatedOn)
        SELECT
            AdvertOfferID, OfferCategoryID, 1, CreatedOn
        FROM @Category
    END

    INSERT INTO [Offer].[LkAdvertOffer_Campaign] ([CampaignID], [AdvertOfferID])
        SELECT [CampaignID], [AdvertOfferID] FROM @Campaign

    COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
    (SELECT
        (SELECT * FROM @AdvertOffer FOR JSON PATH)[Offer]
        ,(SELECT * FROM @MaxRedemption FOR JSON PATH)[MaxRedemption]
        ,(SELECT * FROM @Link FOR JSON PATH )[Link]
        ,(SELECT * FROM @RuleFilter FOR JSON PATH)[RuleFilter]
        ,(SELECT * FROM @ProductDiscount FOR JSON PATH)[ProductDiscount]
        ,(SELECT * FROM @Category FOR JSON PATH)[Category]
        ,(SELECT * FROM @Campaign FOR JSON PATH)[Campaign]
    FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
