﻿
CREATE   PROCEDURE [Offer].[Activate_OfferModelV4] (
    @OfferID    uniqueidentifier,
    @TenantID   uniqueidentifier,
    @IsActive   bit
) AS
BEGIN
    IF NOT EXISTS (SELECT * FROM [Offer].[Offer] WHERE [OfferID] = @OfferID AND TenantID = @TenantID)
    BEGIN
        ;THROW 50004, '@OfferID and @TenantID do not match', 1;
    END

BEGIN TRY
    BEGIN TRANSACTION

    UPDATE [Offer].[Offer]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[Link]
    SET [IsActive] = @IsActive
    WHERE [LinkID] IN (SELECT [LinkID] FROM [Offer].[LkOffer_Link] WHERE [OfferID] = @OfferID)

    UPDATE [Offer].[MaxRedemption]
    SET [IsActive] = @IsActive
    WHERE [MaxRedemptionID] IN (SELECT [MaxRedemptionID] FROM [Offer].[LkOffer_MaxRedemption] WHERE [OfferID] = @OfferID)

    --UPDATE [Offer].[ProductReward]
    --SET [IsActive] = @IsActive
    --WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_AppTenant]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_Campaign]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_Category]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [ContentPush].[InterstitialZone]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_Link]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_MaxRedemption]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_ProductDiscount]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    UPDATE [Offer].[LkOffer_RuleFilter]
    SET [IsActive] = @IsActive
    WHERE [OfferID] = @OfferID

    COMMIT TRANSACTION;
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), CONVERT(nvarchar(36),@OfferID) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
