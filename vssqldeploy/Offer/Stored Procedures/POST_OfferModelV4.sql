﻿
CREATE   PROCEDURE [Offer].[POST_OfferModelV4] (
    @json nvarchar(max)
) AS
BEGIN
    EXEC [Offer].[Serialize_OfferModelV4] @json, @isUpdate=0, @isInsert=1
END
