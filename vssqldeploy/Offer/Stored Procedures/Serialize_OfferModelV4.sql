﻿CREATE    PROCEDURE [Offer].[Serialize_OfferModelV4] (
    @json nvarchar(max),
    @isUpdate bit,
    @isInsert bit
) AS
BEGIN
    IF @json IS NULL
    BEGIN
        ;THROW 50005, 'Parameter @json is required and cannot be NULL', 1;
    END
    IF ISNULL(@isUpdate,0) = ISNULL(@isInsert,0)
    BEGIN
        ;THROW 50006, 'You must pass either @isUpdate=1 OR @isInsert=1 but not both at the same time', 1;
    END

    SET NOCOUNT ON;
    --SET XACT_ABORT ON;
BEGIN TRY
    --BEGIN TRANSACTION

    -- Supply default value if property is missing
    IF JSON_VALUE(@json, '$.AutoUpdateAdvert')              IS NULL SET @json = JSON_MODIFY(@json, '$.AutoUpdateAdvert', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.TargetUsersType')               IS NULL SET @json = JSON_MODIFY(@json, '$.TargetUsersType', 'authorized')
    IF JSON_VALUE(@json, '$.Subtitle')                      IS NULL SET @json = JSON_MODIFY(@json, '$.Subtitle', '')
    IF JSON_VALUE(@json, '$.Description')                   IS NULL SET @json = JSON_MODIFY(@json, '$.Description', '')
    IF JSON_VALUE(@json, '$.ImagePos')                      IS NULL SET @json = JSON_MODIFY(@json, '$.ImagePos', 'top')
    IF JSON_VALUE(@json, '$.Display')                       IS NULL SET @json = JSON_MODIFY(@json, '$.Display', 'deal')
    IF JSON_VALUE(@json, '$.IsDeleted')                     IS NULL SET @json = JSON_MODIFY(@json, '$.IsDeleted', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.IsDraft')                       IS NULL SET @json = JSON_MODIFY(@json, '$.IsDraft', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.IsRestricted')                  IS NULL SET @json = JSON_MODIFY(@json, '$.IsRestricted', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.BrandedOffer')                  IS NULL SET @json = JSON_MODIFY(@json, '$.BrandedOffer', CAST(0 as BIT))
    IF JSON_VALUE(@json, '$.RewardType')                    IS NULL SET @json = JSON_MODIFY(@json, '$.RewardType', '')
    IF JSON_VALUE(@json, '$.IsDisplayTransactionProgress')  IS NULL SET @json = JSON_MODIFY(@json, '$.IsDisplayTransactionProgress', CAST(0 as BIT))

    DECLARE @Offer [Offer].[OfferUpsertSqlModelV4Type],
        @MaxRedemption [Offer].[MaxRedemptionSqlModelType],
        @Link [Offer].[OfferModelV4LinkType],
        @RuleFilter [Offer].[OfferModelV4RuleFilterType],
        @ProductReward [Offer].[OfferModelV4ProductRewardType],
      --@InterstitialZone [Offer].[OfferModelV4InterstitialZoneType],
        @AppTenant [Offer].[OfferModelV4AppTenantType],
        @Category [Offer].[OfferModelV4CategoryType],
        @Campaign [Offer].[OfferModelV4CampaignType],
        @ProductDiscount [Offer].[ProductDiscountSqlModelType];

    INSERT INTO @Offer
    SELECT * FROM OPENJSON(@json)
    WITH
    (
        [OfferID]               uniqueidentifier    '$.id',
        [AdvertOfferID]         uniqueidentifier    '$.AdvertOfferId', -- ??
        [AutoUpdateAdvert]      bit                 '$.AutoUpdateAdvert', -- ??
        [OfferTemplateID]       uniqueidentifier    '$.offerTemplateId', -- ??
        [TenantID]              uniqueidentifier    '$.tenantId',
        [OfferType]             nvarchar(255)       '$.OfferType',
        [RewardType]            nvarchar(255)       '$.RewardType',
        [TargetUsersType]       nvarchar(255)       '$.TargetUsersType',
        [EditorTitle]           nvarchar(255)       '$.EditorTitle',
        [EditorDescription]     nvarchar(max)       '$.EditorDescription',
        [Title]                 nvarchar(255)       '$.Title',
        [Subtitle]              nvarchar(255)       '$.Subtitle',
        [Description]           nvarchar(max)       '$.Description',
        [ImageURL]              nvarchar(255)       '$.ImageURL',
        [ImagePos]              nvarchar(50)        '$.ImagePos',
        [Order]                 int                 '$.Order',
        [IsFeaturedOffer]       bit                 '$.IsFeaturedOffer',
        [IsDeleted]             bit                 '$.IsDeleted',
        [IsActive]              bit                 '$.IsActive',
        [IsDraft]               bit                 '$.IsDraft',
        [IsRestricted]          bit                 '$.IsRestricted',
        [IsDisplay]             bit                 '$.IsDisplay',
        [IsDisplayTransactionProgress] bit          '$.IsDisplayTransactionProgress',
        [IsBrandedOffer]        bit                 '$.BrandedOffer',
        [Display]               nvarchar(50)        '$.Display',
        [StartDate]             datetime2(7)        '$.StartDate',
        [EndDate]               datetime2(7)        '$.EndDate',
        [BarCode]               nvarchar(255)       '$.BarCode',
        [BarCodeType]           nvarchar(50)        '$.BarCodeType',
        [PunchCardDisplayType]  nvarchar(50)        '$.PunchCardDisplayType',
        [MaxPunchAmount]        int                 '$.MaxPunchAmount',
        [CreatedOn]             datetime2(7)        '$.Meta.createdOn',
        [CreatedBy]             nvarchar(255)       '$.Meta.createdBy',
        [CreatedByUserID]       uniqueidentifier    '$.Meta.createdByUserId',
        [UpdatedOn]             datetime2(7)        '$.Meta.updatedOn',
        [UpdatedBy]             nvarchar(255)       '$.Meta.updatedBy',
        [UpdatedByUserID]       uniqueidentifier    '$.Meta.updatedByUserId'
    );
    IF ((SELECT COUNT(*) FROM @Offer) != 1)
    BEGIN
        ;THROW 50001, 'Must submit exactly one offer', 1;
    END
    IF EXISTS (SELECT * FROM @Offer WHERE [OfferID] IS NULL)
    BEGIN
        ;THROW 50002, 'Must include a valid OfferID', 1;
    END

    BEGIN
        INSERT INTO @MaxRedemption
        SELECT
            [OfferID],
            NEWID()         AS MaxRedemptionID,
            'user'          AS RedemptionType,
            [MaxRedemption] AS [MaxRedemptions],
            0               AS [RollingWindowDays],
            0               AS [RollingWindowWeeks],
            0               AS [RollingWindowMonths],
            0               AS [RollingWindowYears],
            [IsActive],
            [CreatedOn]
        FROM OPENJSON(@json)
        WITH
        (
            [OfferID]               [uniqueidentifier]  '$.id',
            [MaxRedemption]         [decimal](10,3)     '$.MaxRedemption',
            [IsActive]              [bit]               '$.IsActive',
            [CreatedOn]             [datetime2]         '$.Meta.createdOn'
        ) WHERE [OfferID] IS NOT NULL AND [MaxRedemption] IS NOT NULL;
    END

    INSERT INTO @Link
    SELECT
        [OfferID]
        , LinkID = ISNULL(Links.[LinkID], NEWID())
        , Links.[LinkText]
        , Links.[LinkType]
        , Links.[Url]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.id',
        [Links] nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.Links)
    WITH
    (
        [LinkID]            uniqueidentifier    '$.linkId',
        [LinkText]          nvarchar(255)       '$.linkText',
        [LinkType]          nvarchar(255)       '$.linkType',
        [Url]               nvarchar(255)       '$.url'
    ) AS Links
    WHERE ([LinkText] IS NOT NULL OR [LinkType] IS NOT NULL OR [Url] IS NOT NULL) AND [OfferID] IS NOT NULL;

    INSERT INTO @RuleFilter
    SELECT
        [OfferID]
        , RF.value AS [RuleFilterID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.id',
        RuleFilters nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.RuleFilters) AS RF
    WHERE RF.value IS NOT NULL AND [OfferID] IS NOT NULL;

    INSERT INTO @ProductReward
    SELECT
        [OfferID]
        , PR.value AS [ProductRewardID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.id',
        ProductRewards nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.ProductRewards) AS PR
    WHERE PR.value IS NOT NULL AND [OfferID] IS NOT NULL;

    --INSERT INTO @InterstitialZone
    --SELECT
    --    [OfferID]
    --    , IZ.value AS [InterstitialZoneID]
    --    , [IsActive]
    --    , [CreatedOn]
    --FROM OPENJSON(@json)
    --WITH
    --(
    --    [OfferID]               uniqueidentifier    '$.id',
    --    [InterstitialZones] nvarchar(max) AS json,
    --    [IsActive]              bit                 '$.IsActive',
    --    [CreatedOn]             datetime2(7)        '$.Meta.createdOn'
    --) AS Offer
    --CROSS APPLY OPENJSON (Offer.InterstitialZones) AS IZ
    --WHERE IZ.value IS NOT NULL AND [OfferID] IS NOT NULL;

    INSERT INTO @AppTenant
    SELECT
        [OfferID]
        , AppT.value AS [AppTenantID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.id',
        [AppTenants] nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.AppTenants) AS AppT
    WHERE AppT.value IS NOT NULL AND [OfferID] IS NOT NULL;

    INSERT INTO @Category
    SELECT
        [OfferID]
        , Ctg.value AS [OfferCategoryID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.id',
        Categories nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.Categories) AS Ctg
    WHERE Ctg.value IS NOT NULL AND [OfferID] IS NOT NULL;

    INSERT INTO @Campaign
    SELECT
        [OfferID]
        , Cmp.value AS [CampaignID]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           uniqueidentifier    '$.id',
        Campaigns nvarchar(max) AS json,
        [IsActive]          bit                 '$.IsActive',
        [CreatedOn]         datetime2(7)        '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.Campaigns) AS Cmp
    WHERE Cmp.value IS NOT NULL AND [OfferID] IS NOT NULL;

    INSERT INTO @ProductDiscount
    SELECT
        [OfferID]
        , ProductDiscountID = ISNULL(ProductDiscounts.ProductDiscountID, NEWID())
        , ProductDiscounts.[ProductRewardID]
        , ProductDiscounts.[StandardProductGroupID]
        , ProductDiscounts.[DiscountType]
        , ProductDiscounts.[DiscountLimitType]
        , ProductDiscounts.[DiscountValue]
        , ProductDiscounts.[DiscountCurrency]
        , ProductDiscounts.[NewUnitPrice]
        , ProductDiscounts.[MinimumQuantity]
        , ProductDiscounts.[MaximumQuantity]
        , [IsActive]
        , [CreatedOn]
    FROM OPENJSON(@json)
    WITH
    (
        [OfferID]           [uniqueidentifier]  '$.id',
        [ProductDiscounts]  [nvarchar](max) AS json,
        [IsActive]          [bit]               '$.IsActive',
        [CreatedOn]         [datetime2]         '$.Meta.createdOn'
    ) AS Offer
    CROSS APPLY OPENJSON (Offer.ProductDiscounts)
    WITH
    (
        [ProductDiscountID]         uniqueidentifier    '$.ProductDiscountId',
        [ProductRewardID]           uniqueidentifier    '$.ProductRewardId',
        [StandardProductGroupID]    uniqueidentifier    '$.StandardProductGroupId',
        [DiscountType]              nvarchar(50)        '$.DiscountType',
        [DiscountLimitType]         nvarchar(20)        '$.DiscountLimitType',
        [DiscountValue]             decimal(7,3)        '$.DiscountValue',
        [DiscountCurrency]          nvarchar(50)        '$.DiscountCurrency',
        [NewUnitPrice]              decimal(7,3)        '$.NewUnitPrice',
        [MinimumQuantity]           decimal(6,3)        '$.MinimumQuantity',
        [MaximumQuantity]           decimal(6,3)        '$.MaximumQuantity'
        --[QuantityType]      nvarchar(50)        '$.QuantityType',
        --[MinimumQuantity]   decimal(7,3)        '$.MinimumQuantity',
        --[MaximumQuantity]   decimal(7,3)        '$.MaximumQuantity',
        --[MinimumPurchase]   decimal(7,3)        '$.MinimumPurchase',
        --[ProductCodetType]  nvarchar(50)        '$.ProductCodetType',
        --[ProductCode]       nvarchar(14)        '$.ProductCode',
    ) AS ProductDiscounts
    WHERE [OfferID] IS NOT NULL;

    IF (@isUpdate = 1)
    BEGIN
        EXEC [Offer].[Upsert_OfferModelV4] @Offer, @MaxRedemption, @Link, @RuleFilter, @ProductReward, @AppTenant, @Category, @Campaign, @ProductDiscount;
    END
    ELSE IF (@isInsert = 1)
    BEGIN
        EXEC [Offer].[Insert_OfferModelV4] @Offer, @MaxRedemption, @Link, @RuleFilter, @ProductReward, @AppTenant, @Category, @Campaign, @ProductDiscount;
    END

    --COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), @json AS [StoredProcedureInput];

    --ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END

