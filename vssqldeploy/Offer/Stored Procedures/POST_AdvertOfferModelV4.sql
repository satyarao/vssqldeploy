﻿
CREATE   PROCEDURE [Offer].[POST_AdvertOfferModelV4] (
    @json nvarchar(max)
) AS
BEGIN
    EXEC [Offer].[Serialize_AdvertOfferModelV4] @json, @isUpdate=0, @isInsert=1
END
