﻿
CREATE   PROCEDURE [Offer].[INSERT_RuleFilters_ForAdvertOffers]
    @AdvertOfferID  uniqueidentifier,
    @TenantID       uniqueidentifier,
    @RuleFilterIds  nvarchar(max)
AS
BEGIN
BEGIN TRY
    DECLARE @IsDraft bit
    SET @IsDraft = (SELECT IsDraft FROM [Offer].[AdvertOffer] WHERE AdvertOfferID = @AdvertOfferID AND TenantID = @TenantID)
    IF @IsDraft IS NULL
    BEGIN
        ;THROW 50004, '@AdvertOfferID and @TenantID do not match', 1;
    END

    IF @IsDraft = 0
    BEGIN
        ;THROW 50007, 'It is not allowed to update Offer with IsDraft=0', 1;
    END

    DECLARE @Ids TABLE (ID uniqueidentifier)
    INSERT INTO @Ids SELECT DISTINCT CAST([value] AS uniqueidentifier) FROM OPENJSON(@RuleFilterIds)
    DELETE FROM @Ids WHERE ID IN (SELECT [RuleFilterID] FROM [Offer].[LkAdvertOffer_RuleFilter] WHERE AdvertOfferID = @AdvertOfferID)

    INSERT INTO [Offer].[LkAdvertOffer_RuleFilter] ([AdvertOfferID], [RuleFilterID]) SELECT @AdvertOfferID, ID FROM @Ids
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
        SELECT
            ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(),
            (SELECT
                AdvertOfferID = @AdvertOfferID,
                TenantID = @TenantID,
                RuleFilterIds = @RuleFilterIds
            FOR JSON PATH) AS [StoredProcedureInput];

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
