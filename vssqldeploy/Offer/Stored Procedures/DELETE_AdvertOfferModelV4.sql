﻿
CREATE   PROCEDURE [Offer].[DELETE_AdvertOfferModelV4] (
    @AdvertOfferID  uniqueidentifier,
    @TenantID       uniqueidentifier
)
AS
BEGIN
    IF NOT EXISTS (SELECT * FROM [Offer].[AdvertOffer] WHERE [AdvertOfferID] = @AdvertOfferID) RETURN
    IF NOT EXISTS (SELECT * FROM [dbo].[Tenant] WHERE [TenantID] = @TenantID) RETURN

    IF NOT EXISTS (SELECT * FROM [Offer].[AdvertOffer] WHERE [AdvertOfferID] = @AdvertOfferID AND TenantID = @TenantID)
    BEGIN
        ;THROW 50004, '@AdvertOfferID and @TenantID do not match', 1;
    END

BEGIN TRY

    BEGIN TRANSACTION

    UPDATE [Offer].[AdvertOffer]
    SET [IsDeleted] = 1
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[Offer]
    SET [IsDeleted] = 1
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_Campaign]
    SET [IsActive] = 0
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_Category]
    SET [IsActive] = 0
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_Link]
    SET [IsActive] = 0
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_MaxRedemption]
    SET [IsActive] = 0
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_ProductDiscount]
    SET [IsActive] = 0
    WHERE [AdvertOfferID] = @AdvertOfferID

    UPDATE [Offer].[LkAdvertOffer_RuleFilter]
    SET [IsActive] = 0
    WHERE [AdvertOfferID] = @AdvertOfferID

    COMMIT TRANSACTION;
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
        SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), CONVERT(nvarchar(36),@AdvertOfferID) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
