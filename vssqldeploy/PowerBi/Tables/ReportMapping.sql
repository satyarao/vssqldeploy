﻿CREATE TABLE [PowerBi].[ReportMapping] (
    [ReportMappingID] INT              IDENTITY (1, 1) NOT NULL,
    [ReportID]        UNIQUEIDENTIFIER NOT NULL,
    [TenantID]        UNIQUEIDENTIFIER NOT NULL,
    [IsActive]        BIT              NOT NULL,
    CONSTRAINT [PK_ReportMappingID] PRIMARY KEY CLUSTERED ([ReportMappingID] ASC),
    CONSTRAINT [FK_ReportID] FOREIGN KEY ([ReportID]) REFERENCES [PowerBi].[PowerBiConfig] ([ReportId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReportToTenant] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE,
    CONSTRAINT [UC_ReportIdTenantId] UNIQUE NONCLUSTERED ([ReportID] ASC, [TenantID] ASC)
);

