﻿CREATE TABLE [PowerBi].[PowerBiConfig] (
    [PowerBiConfigId]  INT              IDENTITY (1, 1) NOT NULL,
    [WorkspaceGroupId] UNIQUEIDENTIFIER NOT NULL,
    [ReportId]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [IsActive]         BIT              NOT NULL,
    [ReportName]       VARCHAR (50)     NOT NULL,
    [TenantId]         UNIQUEIDENTIFIER NULL,
    [Comment]          NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_PowerBiConfig] PRIMARY KEY CLUSTERED ([PowerBiConfigId] ASC),
    CONSTRAINT [UC_ReportId] UNIQUE NONCLUSTERED ([ReportId] ASC)
);

