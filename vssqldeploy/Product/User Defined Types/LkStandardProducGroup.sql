﻿CREATE TYPE [Product].[LkStandardProducGroup] AS TABLE (
    [StandardProductCodeID]  NVARCHAR (14)    NULL,
    [StandardProductGroupID] UNIQUEIDENTIFIER NULL);

