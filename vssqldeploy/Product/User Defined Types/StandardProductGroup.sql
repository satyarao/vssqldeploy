﻿CREATE TYPE [Product].[StandardProductGroup] AS TABLE (
    [StandardProductGroupID] UNIQUEIDENTIFIER NULL,
    [Name]                   NVARCHAR (50)    NULL,
    [Description]            NVARCHAR (255)   NULL,
    [GroupType]              NVARCHAR (50)    NULL,
    [SystemID]               UNIQUEIDENTIFIER NULL,
    [TenantID]               UNIQUEIDENTIFIER NULL,
    [LoyaltyProgramID]       UNIQUEIDENTIFIER NULL);

