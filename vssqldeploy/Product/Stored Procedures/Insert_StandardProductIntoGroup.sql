﻿
CREATE   PROCEDURE [Product].[Insert_StandardProductIntoGroup]
	@StandardProductCodeID nvarchar(14),
	@StandardProductGroupID uniqueidentifier
AS
BEGIN
	IF NOT EXISTS (SELECT TOP 1 * FROM [Product].[LKStandardProductGroup] WHERE StandardProductCodeID = @StandardProductCodeID AND StandardProductGroupID = @StandardProductGroupID)
	BEGIN
		INSERT INTO [Product].[LKStandardProductGroup]
			([StandardProductCodeID],[StandardProductGroupID],[IsActive],[CreatedOn])
		VALUES
			(@StandardProductCodeID, @StandardProductGroupID, 1, GETUTCDATE());
	END
END
