﻿

CREATE   PROCEDURE [Product].[GetProductCode]
	@ProductCode uniqueidentifier
AS
BEGIN
SELECT SP.StandardProductID
FROM [Product].[Product.StandardProduct] 
 WHERE [UpcA] = @ProductCode
    OR [UpcE] = @ProductCode
	OR [Ean13] = @ProductCode
	OR [PosCode] = @ProductCode
END
