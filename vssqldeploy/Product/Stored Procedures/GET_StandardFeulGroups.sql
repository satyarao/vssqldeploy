﻿
CREATE   PROCEDURE [Product].[GET_StandardFeulGroups]
AS
BEGIN

	SELECT	[StandardProductGroupID]		[StandardProductGroupId],
			TRY_CONVERT(int, [GroupCode])	[ProductGroupId],
			[GroupCode],
			[Name]							[GradeName],
			[IsDeleted]						[Deprecated]
	FROM	[Product].[StandardProductGroup] 
	WHERE	[TenantID] IS NULL AND 
			[GroupCode] IS NOT NULL AND
			[IsDeleted] = 0

END
