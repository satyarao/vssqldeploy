﻿
--============================================PRODUCT============================================--


CREATE   PROCEDURE [Product].[GET_FuelGroupTypes] @FuelGroups nvarchar(max) = NULL
AS
BEGIN
	
	IF(@FuelGroups IS NOT NULL AND LEN(@FuelGroups) > 1)
	BEGIN

		SELECT		sp.[StandardProductID]			[StandardProductCode],
					sp.[PaymentSystemProductCode]	[PaymentCode],
					sp.[Name]						[Description],
					spg.[IsDeleted]					[Deprecated],
					sp.[Description]				[Grade],
					TRY_CONVERT(int,spg.[GroupCode])[ProductGroupId],
					(
						SELECT	
							TRY_CONVERT(int,spg.[GroupCode])	[ProductGroupId],
							spg.[GroupCode],
							spg.[Name]							[GradeName],
							spg.[IsDeleted]						[Deprecated]
						FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
					)
													AS [PaymentSystemProductGroup]
		FROM		[Product].[StandardProductGroup] spg WITH(NOLOCK)
		INNER JOIN	[Product].[LKStandardProductGroup] lkspg WITH(NOLOCK)
		ON			lkspg.[StandardProductGroupID] = spg.[StandardProductGroupID]
		INNER JOIN	[Product].[StandardProduct] sp WITH(NOLOCK)
		ON			lkspg.[StandardProductCodeID] = sp.[StandardProductID]
		WHERE		spg.[GroupCode] IN (SELECT value FROM STRING_SPLIT(@FuelGroups, ',')) 
		FOR JSON PATH

	END

END
