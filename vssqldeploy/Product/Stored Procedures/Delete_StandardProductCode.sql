﻿
CREATE   PROCEDURE [Product].[Delete_StandardProductCode]
	@StandardProductCodeID nvarchar(14)
AS
BEGIN
	UPDATE [Product].[LKStandardProductGroup]
	SET [IsActive] = 0
	WHERE [StandardProductCodeID] = @StandardProductCodeID
END
