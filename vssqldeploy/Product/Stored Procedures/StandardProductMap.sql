﻿CREATE   PROCEDURE [Product].[StandardProductMap] @json nvarchar(max) AS
BEGIN

	DECLARE @ProductCode TABLE ( [ProductCode] nvarchar(255) );

	INSERT INTO @ProductCode
	SELECT * FROM OPENJSON (@json) WITH
	(
		[ProductCode] nvarchar(255)
	);

	SELECT [StandardProductCodeID]
	FROM [Product].[StandardProductCode] spc
	INNER JOIN @ProductCode pc
		ON spc.[ProductCode] = pc.[ProductCode] OR spc.[PosCode] = pc.[ProductCode] OR 
			spc.[GenericCode] = pc.[ProductCode] OR spc.[UPC] = pc.[ProductCode] OR  
			spc.[NacsmCode] = pc.[ProductCode]


END
