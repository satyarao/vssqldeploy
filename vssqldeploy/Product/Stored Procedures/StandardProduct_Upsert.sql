﻿
CREATE   PROCEDURE [Product].[StandardProduct_Upsert] 
(
	@StandardProductID			nvarchar(14)	,
	@ProductCodeType			nvarchar(50)	,
	@Name						nvarchar(255)	,
	@Description				nvarchar(255)	,
	@UpcA						nvarchar(12)	,
	@UpcE						nvarchar(8)		,
	@Ean8						nvarchar(8)		,
	@Ean13						nvarchar(13)	,
	@PaymentSystemProductCode	nvarchar(50)	,
	@PosCode					nvarchar(50)	,
	@PosCodeModifier			nvarchar(50)	,
	@ProductType				nvarchar(20)	
) AS
BEGIN
	SET NOCOUNT ON;
    SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION

	IF @StandardProductID IS NULL OR @ProductCodeType IS NULL
	BEGIN
		;THROW 5100, 'Cannot have a null Standard Product Code or Product Code Type', 1;
	END
	SET @Name = ISNULL(@Name, '-');

	IF LEN(@StandardProductId) != 14
	BEGIN
		;THROW 5100, 'Invalid StandardProductCode', 1;
	END


	IF NOT EXISTS(SELECT * FROM [Product].[StandardProduct] WHERE [StandardProductID] = @StandardProductID)
	BEGIN
		INSERT INTO [Product].[StandardProduct]
			([StandardProductID],[ProductCodeType],[Name],[Description],[UpcA],[UpcE],[Ean8],[Ean13]
			,[PaymentSystemProductCode],[PosCode],[PosCodeModifier],[ProductType],[CreatedOn])
		VALUES
			(@StandardProductID, @ProductCodeType, @Name, @Description, @UpcA, @UpcE, @Ean8, @Ean13
			,@PaymentSystemProductCode, @PosCode, @PosCodeModifier, @ProductType, GETUTCDATE())
	END

	COMMIT TRANSACTION
    RETURN;
END TRY
BEGIN CATCH
    DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @StandardProductID		[StandardProductID], 
		@ProductCodeType			[ProductCodeType], 
		@Name						[Name],
		@Description				[Description],
		@UpcA						[UpcA],
		@UpcE						[UpcE],
		@Ean8						[Ean8],
		@Ean13						[Ean13],
		@PaymentSystemProductCode	[PaymentSystemProductCode],
		@PosCode					[PosCode],
		@PosCodeModifier			[PosCodeModifier],
		@ProductType				[ProductType]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
END
