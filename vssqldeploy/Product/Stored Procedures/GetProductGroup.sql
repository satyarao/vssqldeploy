﻿
CREATE   PROCEDURE [Product].[GetProductGroup]
	@StandardProductGroupID uniqueidentifier
AS
BEGIN

SELECT TOP 1 SPG.[StandardProductGroupID] AS [StandardProductGroupID],
	   SPG.[Name] AS [Name],
	   SPG.[Description] AS [Description],
	   SPG.[GroupType] AS GroupType,
	   SPG.[SystemID] AS [SystemID],
	   SPG.[TenantID] AS [TenantID],
	   LK.[StandardProductCodeID] AS [StandardProductCodeID]
FROM [Product].[LKStandardProductGroup] AS LK
 INNER JOIN
     [Product].[StandardProductGroup] SPG
 ON LK.[StandardProductGroupID] = SPG.[StandardProductGroupID]
 WHERE LK.[StandardProductGroupID] = @StandardProductGroupID
   AND SPG.[IsDeleted] = 0
END
