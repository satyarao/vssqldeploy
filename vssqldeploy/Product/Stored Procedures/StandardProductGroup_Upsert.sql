﻿



CREATE   PROCEDURE [Product].[StandardProductGroup_Upsert]
(
	@Json nvarchar(max), @IsTenantFuel bit = null
) AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
BEGIN TRY
    BEGIN TRANSACTION
		
		DECLARE @StandardProductGroup [Product].[StandardProductGroup],
			@LkStandardProducGroup [Product].[LkStandardProducGroup];

		INSERT INTO @StandardProductGroup
			([StandardProductGroupID],[Name],[Description],[GroupType],[SystemID],[TenantID],[LoyaltyProgramID])
		SELECT ISNULL([StandardProductGroupID], NEWID()) AS [StandardProductGroupID],
			[Name],
			[Description],
			[GroupType],
			[SystemID],
			[TenantID],
			[LoyaltyProgramID]
		FROM OPENJSON(@Json)
		WITH
		(
			[StandardProductGroupID]		uniqueidentifier	'$.StandardProductGroupID',
			[Name]      					nvarchar(50)		'$.Name',
			[Description]    				nvarchar(255)		'$.Description',
			[GroupType]     				nvarchar(50)		'$.GroupType',
			[SystemID]     					uniqueidentifier	'$.SystemID',
			[TenantID]     					uniqueidentifier	'$.TenantID',
			[LoyaltyProgramID]   			uniqueidentifier    '$.LoyaltyProgramID'
		);
		
		INSERT INTO @LkStandardProducGroup
			([StandardProductCodeID],[StandardProductGroupID])
		SELECT * FROM OPENJSON(@Json)
		WITH
		(
			[StandardProductCodeID]  		nvarchar(14)  		'$.StandardProductCodes.StandardProductCodeID',
			[StandardProductGroupID]		uniqueidentifier	'$.StandardProductCodes.StandardProductGroupID'
		)
		WHERE [StandardProductCodeID] IS NOT NULL AND [StandardProductGroupID] IS NOT NULL;
		
		IF(@IsTenantFuel = 1)
			BEGIN
				MERGE INTO [Product].[StandardProductGroup] AS target
				USING	@StandardProductGroup AS source
				ON		target.[TenantID] = source.[TenantID] AND 
						target.[GroupType] = source.[GroupType] AND
						target.[Name] = source.[Name] AND
						target.[Description] = source.[Description]
				WHEN NOT MATCHED BY target THEN
				INSERT	([StandardProductGroupID],[Name],[Description],[GroupType],[SystemID],[TenantID],[LoyaltyProgramID])
				VALUES	([StandardProductGroupID],[Name],[Description],[GroupType],[SystemID],[TenantID],[LoyaltyProgramID]);
			END
		ELSE
			BEGIN
				MERGE INTO [Product].[StandardProductGroup] AS target
				USING @StandardProductGroup AS source
				ON target.[StandardProductGroupID] = source.[StandardProductGroupID]
				WHEN MATCHED THEN
					UPDATE SET
						target.Name = source.Name,
						target.Description = source.Description,
						target.SystemID = source.SystemID,
						target.LoyaltyProgramID = source.LoyaltyProgramID
				WHEN NOT MATCHED BY target THEN
					INSERT
						([StandardProductGroupID],[Name],[Description],[GroupType],[SystemID],[TenantID],[LoyaltyProgramID])
					VALUES
						([StandardProductGroupID],[Name],[Description],[GroupType],[SystemID],[TenantID],[LoyaltyProgramID]);
			END
				
		MERGE INTO [Product].[LKStandardProductGroup] AS target
		USING @LkStandardProducGroup AS source
		ON target.[StandardProductCodeID] = source.[StandardProductCodeID] AND target.[StandardProductGroupID] = source.[StandardProductGroupID]
		WHEN NOT MATCHED BY target THEN
			INSERT
				([StandardProductCodeID],[StandardProductGroupID],[IsActive],[CreatedOn])
			VALUES
				([StandardProductCodeID],[StandardProductGroupID], 1, GETUTCDATE());
		
		IF(@IsTenantFuel = 1)
			BEGIN
				SELECT TOP(1)	target.[StandardProductGroupID] 
				FROM			@StandardProductGroup source
				INNER JOIN		[Product].[StandardProductGroup] target
				ON				target.[TenantID] = source.[TenantID] AND 
								target.[GroupType] = source.[GroupType] AND
								target.[Name] = source.[Name] AND
								target.[Description] = source.[Description]
			END
		ELSE
			BEGIN
				SELECT TOP(1) [StandardProductGroupID] FROM @StandardProductGroup
			END

	COMMIT TRANSACTION
	RETURN;
END TRY
BEGIN CATCH
	DECLARE @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), @Json AS [StoredProcedureInput];
	ROLLBACK TRANSACTION;
	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
	THROW
END CATCH
END

