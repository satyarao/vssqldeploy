﻿
CREATE   PROCEDURE [Product].[Insert_StandardProduct_PluCode]
	@StandardIDPrefix           nvarchar(2) NULL,
	@TenantID          			uniqueidentifier,
	@CountryCode				nvarchar(3) NULL,
	@ProductCodeType            nvarchar(50),
	@Name                       nvarchar(255) NULL,
	@Description                nvarchar(255),
	@UpcA                       nvarchar(12),
	@UpcE                       nvarchar(8),
	@Ean8                       nvarchar(8),
	@Ean13                      nvarchar(13),
	@PaymentSystemProductCode   nvarchar(50),
	@PosCode                    nvarchar(50),
	@PosCodeModifier            nvarchar(50),
	@ProductType                nvarchar(20)
AS
BEGIN TRY
	BEGIN TRANSACTION
		SET @Name = ISNULL(@Name, '-');

		IF @TenantID IS NULL
		BEGIN
			;THROW 5100, 'Missing TenantID', 1;
		END
		
		IF @PosCode IS NULL
		BEGIN
			;THROW 5100, 'Missing PLU Code', 1;
		END
		
		IF @StandardIDPrefix IS NULL
		BEGIN
			SET @StandardIDPrefix = '20'
		END
		
		IF @CountryCode IS NULL
		BEGIN
			SET @CountryCode = '000'
		END
		
		IF NOT EXISTS (SELECT * FROM [Product].[LKTenantNamespace] WHERE [TenantID] = @TenantID)
		BEGIN
			INSERT INTO [Product].[LKTenantNamespace]
				([TenantID])
			VALUES
				(@TenantID)
		END
		
		DECLARE @PluCountString nvarchar(5)
		SELECT @PluCountString = (SELECT [PluCountString]
								  FROM [Product].[LKTenantNamespace]
							      WITH (TABLOCKX, HOLDLOCK)
							      WHERE TenantID = @TenantID)
		
		DECLARE @TenantNamespace nvarchar(4)
		SELECT  @TenantNamespace = (SELECT [TenantNamespaceString] FROM [Product].[LKTenantNamespace] WHERE [TenantID] = @TenantID)
		DECLARE @StandardProductId nvarchar(14)
		SET @StandardProductID = @StandardIDPrefix + @CountryCode + @TenantNamespace + @PluCountString
		
		IF NOT EXISTS (SELECT * FROM [Product].[StandardProduct] WHERE [PosCode] = @PosCode AND Substring([StandardProductID], 5, 9) <> @TenantNamespace)
		BEGIN
			INSERT INTO [Product].[StandardProduct]
				([StandardProductID],[ProductCodeType],[Name],[Description],[UpcA],[UpcE],[Ean8],[Ean13]
				,[PaymentSystemProductCode],[PosCode],[PosCodeModifier],[ProductType],[CreatedOn])
			VALUES
				(@StandardProductID, @ProductCodeType, @Name, @Description, @UpcA, @UpcE, @Ean8, @Ean13
				,@PaymentSystemProductCode, @PosCode, @PosCodeModifier, @ProductType, GETUTCDATE())
				
			UPDATE [Product].[LKTenantNamespace]
			SET
				[PluCount] = [PluCount] + 1
		END
		
	COMMIT TRANSACTION
	
	SELECT [StandardProductID] FROM [Product].[StandardProduct] WHERE [PosCode] = @PosCode
	
	RETURN;
END TRY
BEGIN CATCH
	DECLARE @Exceptions [StoredProcedureExceptionType];
    INSERT INTO @Exceptions ([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
    SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @StandardIdPrefix		    [StandardIdPrefix], 
		    @TenantID					[TenantID], 
		    @CountryCode				[CountryCode], 
		    @ProductCodeType			[ProductCodeType], 
		    @Name						[Name],
		    @Description				[Description],
		    @UpcA						[UpcA],
		    @UpcE						[UpcE],
		    @Ean8						[Ean8],
		    @Ean13						[Ean13],
		    @PaymentSystemProductCode	[PaymentSystemProductCode],
		    @PosCode					[PosCode],
		    @PosCodeModifier			[PosCodeModifier],
		    @ProductType				[ProductType]
	FOR JSON PATH) AS [StoredProcedureInput];

    ROLLBACK TRANSACTION;

    EXEC [dbo].[PostStoredProcedureException] @Exceptions;
    THROW
END CATCH
