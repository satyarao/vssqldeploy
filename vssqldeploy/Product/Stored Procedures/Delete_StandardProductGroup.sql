﻿
CREATE   PROCEDURE [Product].[Delete_StandardProductGroup]
	@StandardProductGroupID uniqueidentifier
AS
BEGIN
	UPDATE [Product].[LKStandardProductGroup]
	SET [IsActive] = 0
	WHERE [StandardProductCodeID] in (SELECT StandardProductCodeID FROM [Product].[LKStandardProductGroup] WHERE [StandardProductGroupID] = @StandardProductGroupID)
	
	UPDATE [Product].[StandardProductGroup]
	SET [IsDeleted] = 1
	WHERE [StandardProductGroupID] = @StandardProductGroupID
END
