﻿CREATE TABLE [Product].[LKTenantNamespace] (
    [TenantID]              UNIQUEIDENTIFIER NOT NULL,
    [TenantNamespaceID]     INT              IDENTITY (1, 1) NOT NULL,
    [TenantNamespaceString] AS               (right('0000'+CONVERT([nvarchar](4),[TenantNamespaceID]),(4))) PERSISTED,
    [PluCount]              INT              CONSTRAINT [DF_LKTenantNamespace_PluCount] DEFAULT ((1)) NOT NULL,
    [PluCountString]        AS               (right('00000'+CONVERT([nvarchar](5),[PluCount]),(5))) PERSISTED,
    CONSTRAINT [PK_LKTenantNamespace] PRIMARY KEY CLUSTERED ([TenantID] ASC),
    CONSTRAINT [C_LKTenantNamespace_PluCount] CHECK ([PluCount]>(0)),
    CONSTRAINT [FK_LKTenantNamespace_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

