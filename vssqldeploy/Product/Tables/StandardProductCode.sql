﻿CREATE TABLE [Product].[StandardProductCode] (
    [StandardProductCodeID] NVARCHAR (14)  NOT NULL,
    [ProductCodeType]       NVARCHAR (50)  NOT NULL,
    [Name]                  NVARCHAR (255) NOT NULL,
    [Description]           NVARCHAR (255) NULL,
    [ProductCode]           NVARCHAR (14)  NULL,
    [PosCode]               NVARCHAR (20)  NULL,
    [PosCodeModifier]       NVARCHAR (10)  NULL,
    [PosCodeFormat]         NVARCHAR (50)  NULL,
    [GenericCode]           NVARCHAR (20)  NULL,
    [UPC]                   NVARCHAR (14)  NULL,
    [NacsmCode]             NVARCHAR (20)  NULL,
    [ProductType]           NVARCHAR (20)  NULL,
    [CreatedOn]             DATETIME2 (7)  CONSTRAINT [DF_StandardProductCode_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]             DATETIME2 (7)  NULL,
    CONSTRAINT [PK_StandardProductCode] PRIMARY KEY CLUSTERED ([StandardProductCodeID] ASC),
    CONSTRAINT [FK_StandardProductCode_ProductCodeType] FOREIGN KEY ([ProductCodeType]) REFERENCES [Product].[ProductCodeType] ([ProductCodeType]) ON DELETE CASCADE ON UPDATE CASCADE
);

