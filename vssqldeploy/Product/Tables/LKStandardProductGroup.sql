﻿CREATE TABLE [Product].[LKStandardProductGroup] (
    [StandardProductCodeID]  NVARCHAR (14)    NOT NULL,
    [StandardProductGroupID] UNIQUEIDENTIFIER NOT NULL,
    [IsActive]               BIT              CONSTRAINT [DF_LKStandardProductGroup_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]              DATETIME2 (7)    CONSTRAINT [DF_LKStandardProductGroup_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]              DATETIME2 (7)    NULL,
    CONSTRAINT [PK_LKStandardProductGroup] PRIMARY KEY CLUSTERED ([StandardProductCodeID] ASC, [StandardProductGroupID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_LKStandardProductGroup_StandardProductGroupID] FOREIGN KEY ([StandardProductGroupID]) REFERENCES [Product].[StandardProductGroup] ([StandardProductGroupID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_LKStandardProductGroup_StandardProductID] FOREIGN KEY ([StandardProductCodeID]) REFERENCES [Product].[StandardProduct] ([StandardProductID])
);

