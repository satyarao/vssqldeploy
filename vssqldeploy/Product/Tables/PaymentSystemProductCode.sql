﻿CREATE TABLE [Product].[PaymentSystemProductCode] (
    [PaymentCode] NVARCHAR (3)   NOT NULL,
    [Description] NVARCHAR (MAX) NOT NULL,
    [Deprecated]  BIT            NOT NULL,
    [CreatedOn]   DATETIME2 (7)  CONSTRAINT [DF_PaymentSystemProductCode_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]   DATETIME2 (7)  NULL,
    CONSTRAINT [PK_PaymentSystemProductCode] PRIMARY KEY CLUSTERED ([PaymentCode] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UK_PaymentCode] UNIQUE NONCLUSTERED ([PaymentCode] ASC) WITH (FILLFACTOR = 80)
);

