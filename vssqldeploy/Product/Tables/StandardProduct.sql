﻿CREATE TABLE [Product].[StandardProduct] (
    [StandardProductID]        NVARCHAR (14)  NOT NULL,
    [ProductCodeType]          NVARCHAR (50)  NOT NULL,
    [Name]                     NVARCHAR (255) NOT NULL,
    [Description]              NVARCHAR (255) NULL,
    [UpcA]                     NVARCHAR (12)  NULL,
    [UpcE]                     NVARCHAR (8)   NULL,
    [Ean8]                     NVARCHAR (8)   NULL,
    [Ean13]                    NVARCHAR (13)  NULL,
    [PaymentSystemProductCode] NVARCHAR (50)  NULL,
    [PosCode]                  NVARCHAR (50)  NULL,
    [PosCodeModifier]          NVARCHAR (50)  NULL,
    [ProductType]              NVARCHAR (20)  NULL,
    [CreatedOn]                DATETIME2 (7)  CONSTRAINT [DF_StandardProduct_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]                DATETIME2 (7)  NULL,
    CONSTRAINT [PK_StandardProduct] PRIMARY KEY CLUSTERED ([StandardProductID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_StandardProduct_ProductCodeType] FOREIGN KEY ([ProductCodeType]) REFERENCES [Product].[ProductCodeType] ([ProductCodeType]) ON DELETE CASCADE ON UPDATE CASCADE
);

