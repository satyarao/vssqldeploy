﻿CREATE TABLE [Product].[ProductCodeType] (
    [ProductCodeType] NVARCHAR (50) NOT NULL,
    [IsActive]        BIT           CONSTRAINT [DF_ProductCodeType_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME2 (7) CONSTRAINT [DF_ProductCodeType_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]       DATETIME2 (7) NULL,
    CONSTRAINT [PK_ProductCodeType] PRIMARY KEY CLUSTERED ([ProductCodeType] ASC)
);

