﻿CREATE TABLE [Product].[StandardProductGroup] (
    [StandardProductGroupID] UNIQUEIDENTIFIER NOT NULL,
    [Name]                   NVARCHAR (50)    NOT NULL,
    [Description]            NVARCHAR (255)   NULL,
    [GroupType]              NVARCHAR (50)    NULL,
    [SystemID]               UNIQUEIDENTIFIER NULL,
    [TenantID]               UNIQUEIDENTIFIER NULL,
    [LoyaltyProgramID]       UNIQUEIDENTIFIER NULL,
    [IsDeleted]              BIT              CONSTRAINT [DF_StandardProductGroup_IsDeleted] DEFAULT ((0)) NOT NULL,
    [GroupCode]              NVARCHAR (50)    NULL,
    CONSTRAINT [PK_StandardProductGroup] PRIMARY KEY CLUSTERED ([StandardProductGroupID] ASC),
    CONSTRAINT [FK_StandardProductGroup_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [Loyalty].[LoyaltyProgram] ([LoyaltyProgramID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_StandardProductGroup_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID]) ON DELETE CASCADE ON UPDATE CASCADE
);

