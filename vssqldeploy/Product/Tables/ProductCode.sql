﻿CREATE TABLE [Product].[ProductCode] (
    [ProductCodeTypeID] INT             NOT NULL,
    [ProductCode]       NVARCHAR (14)   NOT NULL,
    [ProductIndex]      INT             IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (MAX)  NOT NULL,
    [Description]       NVARCHAR (MAX)  NOT NULL,
    [PaymentCode]       NCHAR (3)       NULL,
    [ModifierName]      NVARCHAR (10)   NULL,
    [ModifierValue]     DECIMAL (9, 2)  NULL,
    [UnitPrice]         DECIMAL (18, 3) NULL,
    [UnitOfMeasure]     NVARCHAR (20)   NULL,
    [CreatedOn]         DATETIME2 (7)   CONSTRAINT [DF_ProductCode_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]         DATETIME2 (7)   NULL,
    CONSTRAINT [PK_ProductCode] PRIMARY KEY CLUSTERED ([ProductCodeTypeID] ASC, [ProductCode] ASC, [ProductIndex] ASC),
    CONSTRAINT [FK_ProductCode_ProductCodeType] FOREIGN KEY ([ProductCodeTypeID]) REFERENCES [dbo].[ProductCodeType] ([ProductCodeTypeID])
);

