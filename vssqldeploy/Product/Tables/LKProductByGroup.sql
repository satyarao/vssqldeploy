﻿CREATE TABLE [Product].[LKProductByGroup] (
    [GroupId]           UNIQUEIDENTIFIER NOT NULL,
    [ProductCodeTypeId] INT              NOT NULL,
    [ProductCode]       NVARCHAR (14)    NOT NULL,
    [ProductIndex]      INT              NOT NULL,
    [IsActive]          BIT              NOT NULL,
    [CreatedOn]         DATETIME2 (7)    CONSTRAINT [DF_LKProductByGroup_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]         DATETIME2 (7)    NULL,
    CONSTRAINT [PK_LKProductByGroup] PRIMARY KEY CLUSTERED ([GroupId] ASC, [ProductCodeTypeId] ASC, [ProductCode] ASC, [ProductIndex] ASC),
    CONSTRAINT [FK_LKProductByGroup_PaymentSystemProductGroupId] FOREIGN KEY ([GroupId]) REFERENCES [Product].[PaymentSystemProductGroup] ([GroupId]),
    CONSTRAINT [FK_LKProductByGroup_ProductCodeId] FOREIGN KEY ([ProductCodeTypeId], [ProductCode], [ProductIndex]) REFERENCES [Product].[ProductCode] ([ProductCodeTypeID], [ProductCode], [ProductIndex])
);

