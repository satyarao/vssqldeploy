﻿CREATE TABLE [Product].[PaymentSystemProductGroup] (
    [GroupId]    UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [GroupName]  NVARCHAR (MAX)   NOT NULL,
    [Deprecated] BIT              NOT NULL,
    [CreatedOn]  DATETIME2 (7)    CONSTRAINT [DF_PaymentSystemProductGroup_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]  DATETIME2 (7)    NULL,
    CONSTRAINT [PK_PaymentSystemProductCodes] PRIMARY KEY CLUSTERED ([GroupId] ASC) WITH (FILLFACTOR = 80)
);

