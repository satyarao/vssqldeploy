﻿

--====================================================================
--UpdatedBy Trace
--11/15/2019 Jack Swink
--====================================================================
--Latest DbUpdate Script: 317
--====================================================================
CREATE   FUNCTION [Product].[GetStandardProductGroupItemCodes]( @StandardProductGroupID uniqueidentifier )
RETURNS TABLE
WITH SCHEMABINDING
AS
	RETURN
		SELECT	sp.[ProductCodeType],
				CASE
					WHEN sp.[ProductCodeType] = 'POS-UPCA'
						THEN sp.[UpcA]
					WHEN sp.[ProductCodeType] = 'POS-UPCE'
						THEN sp.[UpcE]
					WHEN sp.[ProductCodeType] = 'EAN13'
						THEN sp.[Ean13]
					WHEN sp.[ProductCodeType] = 'EAN8'
						THEN sp.[EAN8]
					WHEN sp.[ProductCodeType] = 'PaymentSystemsProductCode'
						THEN sp.[PaymentSystemProductCode]
					ELSE sp.[PosCode]
				END
				AS [PosCode]
		FROM	[Product].[StandardProductGroup] spg WITH(NOLOCK)
		INNER JOIN	[Product].[LKStandardProductGroup] lksp WITH(NOLOCK)
		ON			spg.[StandardProductGroupID] = lksp.[StandardProductGroupID]
		INNER JOIN	[Product].[StandardProduct] sp WITH(NOLOCK)
		ON			sp.[StandardProductID] = lksp.[StandardProductCodeID]
		WHERE		spg.[StandardProductGroupID] = @StandardProductGroupID
