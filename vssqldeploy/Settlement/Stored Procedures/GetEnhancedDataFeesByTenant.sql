﻿


CREATE   PROCEDURE [Settlement].[GetEnhancedDataFeesByTenant] @TenantId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
BEGIN TRY
	BEGIN TRANSACTION

	SELECT [BaseRedemptionCost] AS [baseRedemptionCost],
		(
			SELECT lkEdft.[EnhancedDataFeeTypeID] AS [enhancedDataFeeTypeId],
				edft.[Name] AS [name],
				CASE
					WHEN lkEdft.[CustomAmount] IS NOT NULL
						THEN lkEdft.[CustomAmount]
					ELSE
						edft.[Amount]
				END AS [amount],
				edft.[Enabled] AS [enabled]
			FROM [Settlement].[LkTenant_EnhancedDataFeeTypes] lkEdft
			INNER JOIN [Settlement].[EnhancedDataFeeType] edft
				ON lkEdft.[EnhancedDataFeeTypeID] = edft.[EnhancedDataFeeTypeID]
			WHERE lkEdft.[TenantID] = @TenantId
			FOR JSON PATH
		) AS [enhancedDataFees]

	FROM [Settlement].[EnhancedDataFees] edf
	WHERE edf.[TenantID] = @TenantId
	FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	DECLARE @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), (SELECT @TenantId AS [TenantId] FOR JSON PATH) AS [StoredProcedureInput];
	
	ROLLBACK TRANSACTION

	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
END CATCH
END
