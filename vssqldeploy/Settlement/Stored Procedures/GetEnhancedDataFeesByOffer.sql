﻿



CREATE   PROCEDURE [Settlement].[GetEnhancedDataFeesByOffer] @TenantId uniqueidentifier, @OfferId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
BEGIN TRY
	BEGIN TRANSACTION

	SELECT
		(
			SELECT lkEdf.[EnhancedDataFeeTypeID] AS [enhancedDataFeeTypeId],
				lkEdf.[Enabled] AS [enabled]
			FROM [Settlement].[LkOffer_EnhancedDataFees] lkEdf
			INNER JOIN [Offer].[Offer] ofr
				ON ofr.[OfferID] = lkEdf.[OfferID]
			WHERE ofr.[TenantID] = @TenantId AND lkEdf.[OfferID] = @OfferId
			FOR JSON PATH
		) AS [enhancedDataOptions]

	FROM [Settlement].[EnhancedDataFees] edf
	WHERE edf.[TenantID] = @TenantId
	FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	DECLARE @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), (SELECT @TenantId AS [TenantId], @OfferId AS [OfferId] FOR JSON PATH) AS [StoredProcedureInput];
	
	ROLLBACK TRANSACTION

	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
END CATCH
END
