﻿

CREATE   PROCEDURE [Settlement].[SetEnhancedDataFeesByOffer] @OfferId uniqueidentifier, @EnhancedDataFeeTypeId uniqueidentifier, @Enabled bit
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
BEGIN TRY
	BEGIN TRANSACTION

	IF EXISTS(SELECT * FROM [Settlement].[LkOffer_EnhancedDataFees] WHERE [OfferID] = @OfferId AND [EnhancedDataFeeTypeID] = @EnhancedDataFeeTypeId)
		BEGIN
			UPDATE [Settlement].[LkOffer_EnhancedDataFees]
			SET [Enabled] = @Enabled
			WHERE [OfferID] = @OfferId AND [EnhancedDataFeeTypeID] = @EnhancedDataFeeTypeId
		END
	ELSE
		BEGIN
			INSERT INTO [Settlement].[LkOffer_EnhancedDataFees]
				([OfferId], [EnhancedDataFeeTypeID], [Enabled])
			VALUES
				(@OfferId, @EnhancedDataFeeTypeId, @Enabled)
		END

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	DECLARE @Exceptions [StoredProcedureExceptionType];
	INSERT INTO @Exceptions([ErrorProcedure], [ErrorLine], [ErrorNumber], [ErrorMessage], [ErrorSeverity], [StoredProcedureInput])
	SELECT ERROR_PROCEDURE(), ERROR_LINE(), ERROR_NUMBER(), ERROR_MESSAGE(), ERROR_SEVERITY(), 
	(SELECT @OfferId AS [OfferId], @EnhancedDataFeeTypeId AS [EnhancedDataFeeTypeId], @Enabled AS [Enabled] FOR JSON PATH) AS [StoredProcedureInput];
	
	ROLLBACK TRANSACTION

	EXEC [dbo].[PostStoredProcedureException] @Exceptions;
END CATCH
END
