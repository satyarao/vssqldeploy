﻿CREATE TABLE [Settlement].[EnhancedDataFees] (
    [TenantID]           UNIQUEIDENTIFIER NOT NULL,
    [BaseRedemptionCost] DECIMAL (7, 2)   NOT NULL,
    [IsActive]           BIT              CONSTRAINT [DF_EnhancedDataFees_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_EnhancedDataFees] PRIMARY KEY CLUSTERED ([TenantID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_EnhancedDataFees_StoreTenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

