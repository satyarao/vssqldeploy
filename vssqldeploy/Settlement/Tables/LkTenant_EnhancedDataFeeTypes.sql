﻿CREATE TABLE [Settlement].[LkTenant_EnhancedDataFeeTypes] (
    [TenantID]              UNIQUEIDENTIFIER NOT NULL,
    [EnhancedDataFeeTypeID] UNIQUEIDENTIFIER NOT NULL,
    [CustomAmount]          DECIMAL (7, 2)   NULL,
    CONSTRAINT [PK_LkTenant_EnhancedDataFeeTypes] PRIMARY KEY CLUSTERED ([TenantID] ASC, [EnhancedDataFeeTypeID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkTenant_EnhancedDataFeeTypes_EnhancedDataFeeTypeID] FOREIGN KEY ([EnhancedDataFeeTypeID]) REFERENCES [Settlement].[EnhancedDataFeeType] ([EnhancedDataFeeTypeID]),
    CONSTRAINT [FK_LkTenant_EnhancedDataFeeTypes_TenantID] FOREIGN KEY ([TenantID]) REFERENCES [dbo].[Tenant] ([TenantID])
);

