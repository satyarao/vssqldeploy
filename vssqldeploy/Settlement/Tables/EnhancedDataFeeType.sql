﻿CREATE TABLE [Settlement].[EnhancedDataFeeType] (
    [EnhancedDataFeeTypeID] UNIQUEIDENTIFIER NOT NULL,
    [Name]                  NVARCHAR (255)   NOT NULL,
    [Amount]                DECIMAL (7, 2)   NOT NULL,
    [Enabled]               BIT              CONSTRAINT [DF_EnhancedDataFeeType_Enabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_EnhancedDataFeeType] PRIMARY KEY CLUSTERED ([EnhancedDataFeeTypeID] ASC) WITH (IGNORE_DUP_KEY = ON)
);

