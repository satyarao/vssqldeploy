﻿CREATE TABLE [Settlement].[LkOffer_EnhancedDataFees] (
    [OfferID]               UNIQUEIDENTIFIER NOT NULL,
    [EnhancedDataFeeTypeID] UNIQUEIDENTIFIER NOT NULL,
    [Enabled]               BIT              NOT NULL,
    CONSTRAINT [PK_LkOffer_EnhancedDataFees] PRIMARY KEY CLUSTERED ([OfferID] ASC, [EnhancedDataFeeTypeID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_LkOffer_EnhancedDataFees_EnhancedDataFeeTypeID] FOREIGN KEY ([EnhancedDataFeeTypeID]) REFERENCES [Settlement].[EnhancedDataFeeType] ([EnhancedDataFeeTypeID]),
    CONSTRAINT [FK_LkOffer_EnhancedDataFees_OfferID] FOREIGN KEY ([OfferID]) REFERENCES [Offer].[Offer] ([OfferID])
);

