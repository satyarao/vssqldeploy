﻿CREATE TABLE [VFleet].[UserVFleetPaymentSource] (
    [UserPaymentSourceID]  UNIQUEIDENTIFIER NOT NULL,
    [PaymentProviderToken] UNIQUEIDENTIFIER NOT NULL,
    [UserId]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]            DATETIME2 (7)    CONSTRAINT [DF_UserVFleetPaymentSource_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]             BIT              NOT NULL,
    CONSTRAINT [PK_UserVFleetPaymentSource] PRIMARY KEY CLUSTERED ([UserPaymentSourceID] ASC),
    CONSTRAINT [FK_UserVFleetPaymentSource_account] FOREIGN KEY ([PaymentProviderToken]) REFERENCES [VFleet].[account] ([PAN])
);

