﻿CREATE TABLE [VFleet].[account_activity] (
    [refnumber]    UNIQUEIDENTIFIER NOT NULL,
    [p97txid]      UNIQUEIDENTIFIER NULL,
    [pan]          UNIQUEIDENTIFIER NOT NULL,
    [merchantid]   NVARCHAR (50)    NULL,
    [sequenceId]   NVARCHAR (50)    NULL,
    [period]       NVARCHAR (50)    NULL,
    [amount]       DECIMAL (19, 2)  NOT NULL,
    [activitytype] INT              NOT NULL,
    [timestamp]    DATETIME         CONSTRAINT [DF_Account_Activity_TimeStamp] DEFAULT (getdate()) NOT NULL,
    [driverid]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [FK_Account_Activity_Account] FOREIGN KEY ([pan]) REFERENCES [VFleet].[account] ([PAN]),
    CONSTRAINT [FK_Account_Activity_Account_Activity_Type] FOREIGN KEY ([activitytype]) REFERENCES [VFleet].[account_activity_type] ([id])
);

