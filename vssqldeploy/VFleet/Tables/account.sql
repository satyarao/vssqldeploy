﻿CREATE TABLE [VFleet].[account] (
    [PAN]                           UNIQUEIDENTIFIER NOT NULL,
    [OwnerId]                       UNIQUEIDENTIFIER NOT NULL,
    [Limit]                         DECIMAL (19, 2)  NOT NULL,
    [Balance]                       DECIMAL (19, 2)  NOT NULL,
    [TotalProcessingFees]           DECIMAL (19, 2)  NOT NULL,
    [PaymentDeckId]                 INT              NOT NULL,
    [PeriodCloseType]               INT              NULL,
    [IsAutoPay]                     INT              NULL,
    [IsActive]                      BIT              NOT NULL,
    [CreatedOn]                     DATETIME2 (7)    CONSTRAINT [DF_ACCOUNT_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [AccountType]                   INT              DEFAULT ((0)) NOT NULL,
    [ParentId]                      UNIQUEIDENTIFIER NULL,
    [LimitModifiedOn]               DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [BalanceModifiedOn]             DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [TotalProcessingFeesModifiedOn] DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [PeriodCloseTypeModifiedOn]     DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [LimitModifiedBy]               UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [BalanceModifiedBy]             UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [TotalProcessingFeesModifiedBy] UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    [PeriodCloseTypeModifiedBy]     UNIQUEIDENTIFIER DEFAULT (NULL) NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED ([PAN] ASC),
    CONSTRAINT [FK_Account_Account_Type] FOREIGN KEY ([PaymentDeckId]) REFERENCES [PaymentsEx].[PaymentDeck] ([ID]),
    CONSTRAINT [FK_Account_Type_Id] FOREIGN KEY ([AccountType]) REFERENCES [VFleet].[account_type] ([id]),
    CONSTRAINT [FK_Period_Close_Type] FOREIGN KEY ([PeriodCloseType]) REFERENCES [VFleet].[period_close_type] ([id])
);

