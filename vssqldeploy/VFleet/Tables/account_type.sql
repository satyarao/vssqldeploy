﻿CREATE TABLE [VFleet].[account_type] (
    [id]         INT           NOT NULL,
    [decription] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Account_Type_Id] PRIMARY KEY CLUSTERED ([id] ASC)
);

