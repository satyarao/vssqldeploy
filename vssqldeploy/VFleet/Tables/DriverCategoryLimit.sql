﻿CREATE TABLE [VFleet].[DriverCategoryLimit] (
    [DriverCategoryId]     INT              NOT NULL,
    [FleetTenantId]        UNIQUEIDENTIFIER NOT NULL,
    [SpendingLimit]        DECIMAL (9, 2)   NOT NULL,
    [ModifiedBy]           UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]           DATETIME2 (7)    CONSTRAINT [DF_VFleetDriverCategoryLimit_ModifiedOn] DEFAULT (getutcdate()) NOT NULL,
    [FuelUsageLimit]       INT              NULL,
    [ParkingOnUsageLimit]  INT              NULL,
    [ParkingOffUsageLimit] INT              NULL,
    [CarWashUsageLimit]    INT              NULL,
    [EvChargingUsageLimit] INT              NULL,
    CONSTRAINT [PK_DriverCategoryLimit] PRIMARY KEY CLUSTERED ([DriverCategoryId] ASC, [FleetTenantId] ASC)
);

