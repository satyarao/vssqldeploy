﻿

CREATE PROCEDURE [VFleet].[sp_retrieve_user_details]
@USER_ID uniqueidentifier,
@USER_PAYMENT_SOURCE_ID uniqueidentifier

AS
	BEGIN
		SELECT acct.[OwnerId], acct.[Limit], usr.[UserPaymentSourceID]
		FROM [VFleet].[UserVFleetPaymentSource] usr
			INNER JOIN [VFleet].[account] acct 
			ON usr.[PaymentProviderToken] = acct.[PAN]
		WHERE 
		    usr.[UserId] = @USER_ID AND 
			usr.[UserPaymentSourceID] = @USER_PAYMENT_SOURCE_ID AND
			acct.[AccountType] = 1
	END
