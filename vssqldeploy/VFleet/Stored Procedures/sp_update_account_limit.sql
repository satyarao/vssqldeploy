﻿
--******(TH) New Procedure (NOT IN REPO YET).
CREATE PROCEDURE [VFleet].[sp_update_account_limit]
@LIMIT_TYPE      INT,
@LIMIT           [DECIMAL](19,2),
@TENANT_ID       [UNIQUEIDENTIFIER],
@DRIVER_CATEGORY INT,
@MODIFIED_BY     [UNIQUEIDENTIFIER]

AS
	BEGIN
		DECLARE @STATUS     INT = -1; 
		DECLARE @PAN        UNIQUEIDENTIFIER		
		DECLARE @DRIVER_IDS TABLE ( driverId UNIQUEIDENTIFIER )

		DECLARE @DRIVER_ID_CURSOR CURSOR;
		DECLARE @DRIVER_ID        UNIQUEIDENTIFIER
		
		BEGIN TRY
			IF( @LIMIT_TYPE = 0 )
				BEGIN
					SELECT @PAN = [PAN]
					FROM   [VFleet].[account] 
					WHERE  [OwnerId] = @TENANT_ID AND [ParentId] IS NULL

					IF @PAN IS NOT NULL
						BEGIN
							BEGIN TRAN update_fleet_limit
							BEGIN TRY
								INSERT [VFleet].[account_activity]([refnumber],[pan],[amount],[activitytype])
								VALUES( NEWID(), @PAN, @LIMIT, 8 )
					
								UPDATE [VFleet].[account] 
								SET    [Limit] = @LIMIT,
								       [LimitModifiedOn] = GETDATE(), 
			                           [LimitModifiedBy] = @MODIFIED_BY
								WHERE  [PAN] = @PAN
							
								IF( @@ROWCOUNT < 1 )
									BEGIN
										SET @STATUS = -10
										RAISERROR('No rows were updated.',1,1);
									END

								COMMIT TRAN update_fleet_limit					
								SET @STATUS = 1		
							END TRY
							BEGIN CATCH
								ROLLBACK TRANSACTION update_fleet_limit
								RAISERROR('Update fleet account limit failed.',1,1);
							END CATCH
						END
				END

			ELSE IF( @LIMIT_TYPE = 1 )
				BEGIN
					INSERT INTO @DRIVER_IDS
					SELECT [DriverID] 
					FROM [RCI].[FleetDriver]
					WHERE [DriverCategoryID] = @DRIVER_CATEGORY AND [TenantID] = @TENANT_ID

					BEGIN TRAN update_driver_limit
					BEGIN TRY							
						IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY )
							UPDATE [VFleet].[DriverCategoryLimit]
							SET [SpendingLimit] = @LIMIT, [ModifiedBy] = @MODIFIED_BY, [ModifiedOn] = getutcdate() 
							WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY
						ELSE
							INSERT INTO [VFleet].[DriverCategoryLimit]([DriverCategoryId],[FleetTenantId],[SpendingLimit],[ModifiedBy],[FuelUsageLimit],[ParkingOnUsageLimit],[ParkingOffUsageLimit],[CarWashUsageLimit],[EvChargingUsageLimit])
							VALUES( @DRIVER_CATEGORY, @TENANT_ID, @LIMIT, @MODIFIED_BY, NULL, NULL, NULL, NULL, NULL )

						SET @DRIVER_ID_CURSOR = CURSOR FOR
						select driverId from @DRIVER_IDS

						OPEN @DRIVER_ID_CURSOR

						FETCH NEXT FROM @DRIVER_ID_CURSOR INTO @DRIVER_ID
						WHILE @@FETCH_STATUS = 0
							BEGIN
								SELECT @PAN = [PAN]
								FROM   [VFleet].[account] 
								WHERE  [OwnerId] = @DRIVER_ID AND [ParentId] = @TENANT_ID AND [Limit] != @LIMIT

								IF @PAN IS NOT NULL
								BEGIN									
									INSERT [VFleet].[account_activity]([refnumber],[pan],[amount],[activitytype])
									VALUES( NEWID(), @PAN, @LIMIT, 8 )
					
									UPDATE [VFleet].[account] 
									SET    [Limit] = @LIMIT,
									       [LimitModifiedOn] = GETDATE(), 
										   [LimitModifiedBy] = @MODIFIED_BY
									WHERE  [PAN] = @PAN					
									
									IF( @@ROWCOUNT < 1 )
										BEGIN
											SET @STATUS = -11
											RAISERROR('No rows were updated.',1,1);
										END
								END									

								FETCH NEXT FROM @DRIVER_ID_CURSOR INTO @DRIVER_ID
							END

							CLOSE      @DRIVER_ID_CURSOR;
							DEALLOCATE @DRIVER_ID_CURSOR;
						
							COMMIT TRAN update_driver_limit	
							SET @STATUS = 1
					END TRY
					BEGIN CATCH
						ROLLBACK TRANSACTION update_driver_limit
						RAISERROR('Update driver account limit failed.',1,1);
					END CATCH
				END

			ELSE IF( @LIMIT_TYPE = 2 )
				BEGIN
					IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY )
						UPDATE [VFleet].[DriverCategoryLimit]
						SET [FuelUsageLimit] = @LIMIT, [ModifiedBy] = @MODIFIED_BY, [ModifiedOn] = getutcdate() 
						WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY
					ELSE
						INSERT INTO [VFleet].[DriverCategoryLimit]([DriverCategoryId],[FleetTenantId],[SpendingLimit],[ModifiedBy],[FuelUsageLimit],[ParkingOnUsageLimit],[ParkingOffUsageLimit],[CarWashUsageLimit],[EvChargingUsageLimit])
						VALUES( @DRIVER_CATEGORY, @TENANT_ID, 0, @MODIFIED_BY, @LIMIT, NULL, NULL, NULL, NULL )

					IF( @@ROWCOUNT < 1 )
						BEGIN
							SET @STATUS = -12
							RAISERROR('No rows were updated.',1,1);
						END

					SET @STATUS = 1
				END

			ELSE IF( @LIMIT_TYPE = 3 )
				BEGIN
					IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY )
						UPDATE [VFleet].[DriverCategoryLimit]
						SET [ParkingOnUsageLimit] = @LIMIT, [ModifiedBy] = @MODIFIED_BY, [ModifiedOn] = getutcdate() 
						WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY
					ELSE
						INSERT INTO [VFleet].[DriverCategoryLimit]([DriverCategoryId],[FleetTenantId],[SpendingLimit],[ModifiedBy],[FuelUsageLimit],[ParkingOnUsageLimit],[ParkingOffUsageLimit],[CarWashUsageLimit],[EvChargingUsageLimit])
						VALUES( @DRIVER_CATEGORY, @TENANT_ID, 0, @MODIFIED_BY, NULL, @LIMIT, NULL, NULL, NULL )

					IF( @@ROWCOUNT < 1 )
						BEGIN
							SET @STATUS = -13
							RAISERROR('No rows were updated.',1,1);
						END

					SET @STATUS = 1
				END

			ELSE IF( @LIMIT_TYPE = 4 )
				BEGIN
					IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY )
						UPDATE [VFleet].[DriverCategoryLimit]
						SET [ParkingOffUsageLimit] = @LIMIT, [ModifiedBy] = @MODIFIED_BY, [ModifiedOn] = getutcdate() 
						WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY
					ELSE
						INSERT INTO [VFleet].[DriverCategoryLimit]([DriverCategoryId],[FleetTenantId],[SpendingLimit],[ModifiedBy],[FuelUsageLimit],[ParkingOnUsageLimit],[ParkingOffUsageLimit],[CarWashUsageLimit],[EvChargingUsageLimit])
						VALUES( @DRIVER_CATEGORY, @TENANT_ID, 0, @MODIFIED_BY, NULL, NULL, @LIMIT, NULL, NULL )

					IF( @@ROWCOUNT < 1 )
						BEGIN
							SET @STATUS = -14
							RAISERROR('No rows were updated.',1,1);
						END

					SET @STATUS = 1
				END

			ELSE IF( @LIMIT_TYPE = 5 )
				BEGIN
					IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY )
						UPDATE [VFleet].[DriverCategoryLimit]
						SET [CarWashUsageLimit] = @LIMIT, [ModifiedBy] = @MODIFIED_BY, [ModifiedOn] = getutcdate() 
						WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY
					ELSE
						INSERT INTO [VFleet].[DriverCategoryLimit]([DriverCategoryId],[FleetTenantId],[SpendingLimit],[ModifiedBy],[FuelUsageLimit],[ParkingOnUsageLimit],[ParkingOffUsageLimit],[CarWashUsageLimit],[EvChargingUsageLimit])
						VALUES( @DRIVER_CATEGORY, @TENANT_ID, 0, @MODIFIED_BY, NULL, NULL, NULL, @LIMIT, NULL )

					IF( @@ROWCOUNT < 1 )
						BEGIN
							SET @STATUS = -15
							RAISERROR('No rows were updated.',1,1);
						END

					SET @STATUS = 1
				END

			ELSE IF( @LIMIT_TYPE = 6 )
				BEGIN
					IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY )
						UPDATE [VFleet].[DriverCategoryLimit]
						SET [EvChargingUsageLimit] = @LIMIT, [ModifiedBy] = @MODIFIED_BY, [ModifiedOn] = getutcdate() 
						WHERE [FleetTenantId] = @TENANT_ID AND DriverCategoryId = @DRIVER_CATEGORY
					ELSE
						INSERT INTO [VFleet].[DriverCategoryLimit]([DriverCategoryId],[FleetTenantId],[SpendingLimit],[ModifiedBy],[FuelUsageLimit],[ParkingOnUsageLimit],[ParkingOffUsageLimit],[CarWashUsageLimit],[EvChargingUsageLimit])
						VALUES( @DRIVER_CATEGORY, @TENANT_ID, 0, @MODIFIED_BY, NULL, NULL, NULL, NULL, @LIMIT )

					IF( @@ROWCOUNT < 1 )
						BEGIN
							SET @STATUS = -16
							RAISERROR('No rows were updated.',1,1);
						END

					SET @STATUS = 1
				END

		END TRY
		BEGIN CATCH						
		END CATCH

		SELECT @STATUS
	END
