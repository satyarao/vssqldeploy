﻿






 CREATE PROCEDURE [VFleet].[sp_auth] 
   @DRIVER_PAN    UNIQUEIDENTIFIER, 
   @AMOUNT        DECIMAL(19, 2), 
   @PERIOD        NVARCHAR(50), 
   @MERCHANTID    NVARCHAR(50), 
   @P97TXID       UNIQUEIDENTIFIER,
   @SEQUENCEID    NVARCHAR(50),
   @PARTIAL_AUTH  BIT = 0,
   @MODIFIED_BY   UNIQUEIDENTIFIER
AS 
   BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from  
      -- interfering with SELECT statements.  
      SET nocount ON; 

      IF @DRIVER_PAN IS NULL 
        RAISERROR('PAN CANNOT BE NULL',1,1); 

      IF @AMOUNT IS NULL 
        RAISERROR('AMOUNT CANNOT BE NULL',1,1); 

      IF @MERCHANTID IS NULL 
        RAISERROR('MERCHANTID CANNOT BE NULL',1,1); 

      DECLARE @AUTH_AMOUNT DECIMAL(19, 2) = 0
	  
	  DECLARE @DRIVER_ID UNIQUEIDENTIFIER
      DECLARE @DRIVER_BALANCE DECIMAL(19, 2) 
	  DECLARE @DRIVER_LIMIT DECIMAL(19,2)
	  DECLARE @DRIVER_AVAIL_BALANCE DECIMAL(19,2)
	  
	  DECLARE @FLEET_PAN UNIQUEIDENTIFIER
	  DECLARE @FLEET_BALANCE DECIMAL(19, 2) 
	  DECLARE @FLEET_LIMIT DECIMAL(19,2)
	  DECLARE @FLEET_AVAIL_BALANCE DECIMAL(19,2)

      DECLARE @REF_NUM UNIQUEIDENTIFIER 
      DECLARE @STATUS INT = -1 -- Assume it failed  
	  DECLARE @PARENT_ID UNIQUEIDENTIFIER      

      BEGIN try 
          SELECT @DRIVER_BALANCE = x.[balance], @DRIVER_LIMIT = x.[limit], @PARENT_ID = x.[ParentId], @DRIVER_ID = OwnerId
          FROM   [VFleet].[account] x 
          WHERE  @DRIVER_PAN = [x].[pan] AND [x].[IsActive] = 1
                          
	      IF EXISTS ( SELECT * FROM [VFleet].[account_activity] WHERE [p97txid] = @P97TXID 
			AND [activitytype] >= 1 AND [activitytype] <= 5 )
			BEGIN
				 SET @STATUS = 6	   --already processed auth for this txnId.
			     SET @AMOUNT = 0	
			END
		  ELSE IF @DRIVER_ID IS NULL
		      BEGIN
			     SET @STATUS = 5	   --couldn't find driver account.
			     SET @AMOUNT = 0
			  END
		  ELSE IF @PARENT_ID IS NULL
			 BEGIN
				 SET @STATUS = 4	   --couldn't find parent owner id.
			     SET @AMOUNT = 0
			 END
		  ELSE IF NOT EXISTS ( SELECT * FROM [RCI].[FleetDriver] WHERE [DriverID] = @DRIVER_ID AND IsActive = 1 )
			 BEGIN
			 	 SET @STATUS = 7	   --driver is deactivated.
			     SET @AMOUNT = 0
			 END
	      ELSE IF NOT EXISTS ( SELECT * FROM [VFleet].[account] WHERE OwnerId = @PARENT_ID AND ParentId IS NULL AND IsActive = 1 )
			 BEGIN 
			     SET @STATUS = 8	   --fleet account is deactivated.
			     SET @AMOUNT = 0
			 END
		  ELSE
			  BEGIN							     
				  SELECT @FLEET_BALANCE = x.[balance], @FLEET_LIMIT = x.[limit], @FLEET_PAN = [PAN]
				  FROM   [VFleet].[account] x 
				  WHERE  @PARENT_ID = [x].[OwnerId] AND [x].[IsActive] = 1

				  SET @DRIVER_AVAIL_BALANCE = @DRIVER_LIMIT - @DRIVER_BALANCE
				  SET @FLEET_AVAIL_BALANCE  = @FLEET_LIMIT  - @FLEET_BALANCE

				  IF( @DRIVER_AVAIL_BALANCE = 0 OR @FLEET_AVAIL_BALANCE = 0 )
					 BEGIN
						 SET @STATUS = 99	   --zero balance status
						 SET @AMOUNT = 0
					 END
				  ELSE IF( @AMOUNT > @DRIVER_AVAIL_BALANCE OR @AMOUNT > @FLEET_AVAIL_BALANCE ) 
					 IF( @PARTIAL_AUTH = 1 ) 
						 BEGIN
							IF( @DRIVER_AVAIL_BALANCE < @FLEET_AVAIL_BALANCE )
								SET @AUTH_AMOUNT = @DRIVER_AVAIL_BALANCE;
							ELSE
								SET @AUTH_AMOUNT = @FLEET_AVAIL_BALANCE							 
							 SET @STATUS = 2;  --partial authorized
						 END
					 ELSE 
						 SET @STATUS = 3;      --No Partial Auth Allowed  
				  ELSE 
					 BEGIN
						 SET @AUTH_AMOUNT = @AMOUNT; 
						 SET @STATUS = 1	   --fully authorized
					 END
			  END

          IF ( @AUTH_AMOUNT > 0 ) 
             BEGIN 
				BEGIN TRAN auth; 
				BEGIN TRY
					 SET @REF_NUM = Newid();                              				               
					 -- Create Fleet Activity Record  
					 INSERT INTO [VFleet].[account_activity] ([refnumber],[pan],[driverid],[period],[amount], [p97txid],[merchantid],[sequenceid],[activitytype]) 
						  VALUES (@REF_NUM, 
								  @FLEET_PAN, 
								  @DRIVER_ID,
								  @PERIOD, 
								  @AUTH_AMOUNT,
								  @P97TXID,
								  @MERCHANTID, 
				 				  @SEQUENCEID,
								  1); -- AUTH_HOLD                               
				 
					 -- Update Account Balance  
					 EXEC [VFleet].[sp_update_balance] @DRIVER_PAN, @AUTH_AMOUNT, @MODIFIED_BY, 1 -- DEBIT 
					 EXEC [VFleet].[sp_update_balance] @FLEET_PAN,  @AUTH_AMOUNT, @MODIFIED_BY, 1 -- DEBIT 

					 COMMIT TRAN auth; 
				 END TRY
				 BEGIN CATCH
					ROLLBACK TRANSACTION auth
					RAISERROR('Auth failed',1,1);
				 END CATCH
            END           
      END try 

      BEGIN catch           
		  SET @STATUS =-1;
		  SET @AUTH_AMOUNT = 0;
		  SET @REF_NUM = NULL;
      END catch 
	  
      SELECT @STATUS, 
             @REF_NUM, 
             @AUTH_AMOUNT 
  END 

