﻿

CREATE PROCEDURE [VFleet].[sp_get_payment_info]
@PaymentSourceId uniqueidentifier
AS
	BEGIN
		SELECT [UserPaymentSourceID], [PaymentProviderToken], [UserId] 
		FROM [VFleet].[UserVFleetPaymentSource] 
		WHERE [UserPaymentSourceID] = @PaymentSourceId
	END
