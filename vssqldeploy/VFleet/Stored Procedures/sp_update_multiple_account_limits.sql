﻿
CREATE PROCEDURE [VFleet].[sp_update_multiple_account_limits]
@LIMITS          [VFleet].[LimitsMap] readonly,
@TENANT_ID       [UNIQUEIDENTIFIER],
@DRIVER_CATEGORY INT,
@MODIFIED_BY     [UNIQUEIDENTIFIER]
AS
	BEGIN
		DECLARE @LIMIT_CURSOR   CURSOR;
		DECLARE @LIMIT_TYPE     INT;
		DECLARE @LIMIT          [DECIMAL](19,2);
		DECLARE @STATUS         INT; 
		DECLARE @OVERALL_STATUS INT = -1;
		
		SET @LIMIT_CURSOR = CURSOR FOR
		SELECT [LimitType], [Limit] FROM @LIMITS

		OPEN @LIMIT_CURSOR

		BEGIN TRY
			BEGIN TRAN update_multiple_limits
			FETCH NEXT FROM @LIMIT_CURSOR INTO @LIMIT_TYPE, @LIMIT
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXEC @STATUS = [VFleet].[sp_update_account_limit] @LIMIT_TYPE, @LIMIT, @TENANT_ID, @DRIVER_CATEGORY, @MODIFIED_BY
					
					IF ( @STATUS IS NULL )											
						RAISERROR('Secondary Update Limit Call returned null.',1,1);					
					IF ( @STATUS != 1 )
						BEGIN
							SET @OVERALL_STATUS = @STATUS
							RAISERROR('Secondary Update Limit Call did not return successful result code.',1,1);		
						END

					FETCH NEXT FROM @LIMIT_CURSOR INTO @LIMIT_TYPE, @LIMIT
				END

			SET @OVERALL_STATUS = 1
			COMMIT TRAN update_multiple_limits
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION update_multiple_limits
		END CATCH

		SELECT @OVERALL_STATUS
	END
