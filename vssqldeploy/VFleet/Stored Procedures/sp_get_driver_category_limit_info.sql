﻿

CREATE PROCEDURE [VFleet].[sp_get_driver_category_limit_info]
	@DRIVER_CATEGORY INT,
	@FLEET_ID uniqueidentifier
AS
	BEGIN
		SELECT 
            [SpendingLimit],
            [FuelUsageLimit],
            [ParkingOnUsageLimit],
            [ParkingOffUsageLimit],
            [CarWashUsageLimit],
            [EvChargingUsageLimit],
            [ModifiedBy],
            [ModifiedOn]
        FROM [VFleet].[DriverCategoryLimit]
        WHERE DriverCategoryId = @DRIVER_CATEGORY AND FleetTenantId = @FLEET_ID
	END
