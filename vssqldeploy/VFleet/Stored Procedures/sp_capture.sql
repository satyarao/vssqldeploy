﻿





CREATE PROCEDURE [VFleet].[sp_capture] -- direct capture (no auth beforehand).
    @DRIVER_PAN			   UNIQUEIDENTIFIER,
 	@AMOUNT				   DECIMAL(19, 2), 
	@PROCESSING_FEE_AMOUNT DECIMAL(19, 2), 
    @PERIOD				   NVARCHAR(50), 
	@MERCHANTID			   NVARCHAR(50), 
	@P97TXID			   UNIQUEIDENTIFIER,
	@SEQUENCEID			   NVARCHAR(50),
	@MODIFIED_BY           UNIQUEIDENTIFIER
AS 
BEGIN
   --if insufficient, return -2
   --begin transaction
   --create capture record
   --update balance
   --on exception
   -- status = -1
   --return status 1
   DECLARE @STATUS INT = -1 -- Assume it failed  

   DECLARE @DRIVER_ID UNIQUEIDENTIFIER
   DECLARE @FLEET_ID UNIQUEIDENTIFIER
   DECLARE @FLEET_PAN UNIQUEIDENTIFIER
   DECLARE @DRIVER_AVAIL_BALANCE DECIMAL(19,2)
   DECLARE @FLEET_AVAIL_BALANCE DECIMAL(19,2)
   DECLARE @REF_NUM UNIQUEIDENTIFIER = NEWID()
  
   SELECT @DRIVER_AVAIL_BALANCE = x.[limit] - x.[balance], @DRIVER_ID = x.[OwnerId], @FLEET_ID = x.[ParentId]
      FROM [VFleet].[account] x 
      WHERE @DRIVER_PAN = [x].[pan] AND [x].[IsActive] = 1

	IF EXISTS ( SELECT * FROM [VFleet].[account_activity] WHERE [p97txid] = @P97TXID AND [activitytype] >= 1 AND [activitytype] <= 5 )
		SET @STATUS = 6 --already processed sale for this txnId.

   ELSE IF @DRIVER_ID IS NULL
       SET @STATUS = 5     --couldn't find driver account.

   ELSE IF @FLEET_ID IS NULL
	   SET @STATUS = 4	   --couldn't find parent owner id.

   ELSE IF NOT EXISTS ( SELECT * FROM [RCI].[FleetDriver] WHERE [DriverID] = @DRIVER_ID AND IsActive = 1 )
	   SET @STATUS = 7	   --driver is deactivated.
   ELSE IF NOT EXISTS ( SELECT * FROM [VFleet].[account] WHERE OwnerId = @FLEET_ID AND ParentId IS NULL AND IsActive = 1 )
		SET @STATUS = 8	   --fleet account is deactivated.
   ELSE
       BEGIN
		  SELECT @FLEET_AVAIL_BALANCE = x.[limit] - x.[balance], @FLEET_PAN = x.[PAN]
		  FROM [VFleet].[account] x 
		  WHERE @FLEET_ID = [x].[OwnerId] AND [x].[IsActive] = 1

		  IF @FLEET_PAN IS NULL
		      SET @STATUS = 4	   --couldn't find parent owner id.  		  
		  --check available balance
		  IF @DRIVER_AVAIL_BALANCE < @AMOUNT OR @FLEET_AVAIL_BALANCE < @AMOUNT
				SET @STATUS = 3    -- declined

		  ELSE
			 BEGIN
				BEGIN TRY
				   BEGIN TRAN vcard_capture
				   INSERT INTO [VFleet].[account_activity] ([refnumber],[pan],[driverid],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype]) 
						VALUES (@REF_NUM, 
								@FLEET_PAN, 
								@DRIVER_ID,
								@P97TXID,
								@PERIOD, 
								@AMOUNT,
								@MERCHANTID, 
								@SEQUENCEID,
								3); -- Capture
        
				  -- DEBIT the account
				  EXEC [VFleet].[sp_update_balance] @FLEET_PAN,  @AMOUNT, @MODIFIED_BY, 1
				  EXEC [VFleet].[sp_update_balance] @DRIVER_PAN, @AMOUNT, @MODIFIED_BY, 1 

				  -- Create/Update Processing Fee Records
					INSERT INTO [VFleet].[account_activity] ([refnumber],[pan],[driverid],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype])
						VALUES (NEWID(),
								@FLEET_PAN,
								@DRIVER_ID,
								@P97TXID,
								@PERIOD,
								@PROCESSING_FEE_AMOUNT,
								@MERCHANTID, 
		  						@SEQUENCEID,
								5);
			  
				  UPDATE [VFleet].[account]
			  		 SET [TotalProcessingFees] = [TotalProcessingFees] + @PROCESSING_FEE_AMOUNT,
					     [TotalProcessingFeesModifiedOn] = GETDATE(), 
					     [TotalProcessingFeesModifiedBy] = @MODIFIED_BY
				   WHERE [pan] = @FLEET_PAN

				   UPDATE [VFleet].[account]
			  		 SET [TotalProcessingFees] = [TotalProcessingFees] + @PROCESSING_FEE_AMOUNT,
					     [TotalProcessingFeesModifiedOn] = GETDATE(), 
			             [TotalProcessingFeesModifiedBy] = @MODIFIED_BY
				   WHERE [pan] = @DRIVER_PAN

				  SET @STATUS = 1
				  COMMIT TRAN vcard_capture
			   END TRY

			   BEGIN CATCH
				  ROLLBACK TRAN vcard_capture
				  SET @STATUS = -1
				  SET @REF_NUM = NULL
			   END CATCH
			END
		END

   SELECT @STATUS, @REF_NUM
END
