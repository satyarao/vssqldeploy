﻿
--INTERNAL
--THIS SPROC RETURNS THE PANS USED IN THE PERIOD CLOSE SPROC BELOW:
CREATE PROCEDURE [VFleet].[sp_period_close_get_valid_accounts]
@PERIOD_CLOSE_TYPE INT

AS
	BEGIN
		SELECT [OwnerId]    
		FROM [VFleet].[account]
		WHERE 
			[PeriodCloseType] = @PERIOD_CLOSE_TYPE AND			
			[IsActive]        = 1 AND
			[AccountType]     = 0
	END
