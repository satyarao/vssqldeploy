﻿
--******(TH) New Procedure (NOT IN REPO YET). 
CREATE PROCEDURE [VFleet].[sp_get_account_activity_entries]
@TENANT_ID uniqueidentifier,
@PERIOD_START datetime2,
@PERIOD_END datetime2
AS
	BEGIN
		DECLARE @FLEET_PAN uniqueidentifier

		SELECT @FLEET_PAN = [PAN]
		FROM   [VFleet].[account] 
		WHERE  [OwnerId] = @TENANT_ID

		SELECT [refnumber],[p97txid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp]
		FROM   [VFleet].[account_activity]
		WHERE 
			[pan] = @FLEET_PAN AND 
			[timestamp] >= @PERIOD_START AND 
			[timestamp] < @PERIOD_END			
	END
