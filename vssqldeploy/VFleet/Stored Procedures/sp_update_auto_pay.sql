﻿ 


CREATE PROCEDURE [VFleet].[sp_update_auto_pay]
@TENANT_ID [uniqueidentifier],
@IS_AUTO_PAY [BIT]

AS
	BEGIN
		DECLARE @STATUS INT = 1

		BEGIN TRY
			IF NOT EXISTS ( SELECT * FROM [VFleet].[account] WHERE [OwnerId] = @TENANT_ID )				
				RAISERROR('Could not find fleet account.',1,1); 

			UPDATE [VFleet].[account]
			SET    [IsAutoPay] = @IS_AUTO_PAY
			WHERE  [OwnerId]   = @TENANT_ID			
		END TRY
		BEGIN CATCH
			SET @STATUS = -1
		END CATCH

		SELECT @STATUS
	END
