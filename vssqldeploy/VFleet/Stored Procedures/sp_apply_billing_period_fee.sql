﻿

CREATE PROCEDURE [VFleet].[sp_apply_billing_period_fee]
@TENANT_ID uniqueidentifier,
@AMOUNT DECIMAL(19, 2),
@TIME_STAMP datetime2
AS
	BEGIN
		DECLARE @STATUS INT = -1
		DECLARE @FLEET_PAN uniqueidentifier

		SELECT @FLEET_PAN = [PAN]
		FROM   [VFleet].[account] 
		WHERE  [OwnerId] = @TENANT_ID

		IF @FLEET_PAN IS NULL
			RAISERROR('Could not find fleet account.',1,1); 

		BEGIN TRAN billing_period_fee
		BEGIN TRY
			INSERT INTO [VFleet].[account_activity] ([refnumber],[pan],[amount],[activitytype],[timestamp])
				VALUES (NEWID(),
						@FLEET_PAN,													
						@AMOUNT,						  			
						11,
						@TIME_STAMP);
			  
			UPDATE [VFleet].[account]
			SET    [TotalProcessingFees] = [TotalProcessingFees] + @AMOUNT,
				   [TotalProcessingFeesModifiedOn] = GETDATE(), 
			       [TotalProcessingFeesModifiedBy] = NULL
			WHERE [pan] = @FLEET_PAN

			COMMIT TRAN billing_period_fee
			SET @STATUS = 1
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION billing_period_fee
		END CATCH

		SELECT @STATUS
	END
