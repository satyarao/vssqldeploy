﻿
--VFLEET SERVICE LAYER LOOKUP DRIVER CATEGORY:
CREATE PROCEDURE [VFleet].[sp_lookup_driver_category]
@UserID uniqueidentifier,
@TenantID uniqueidentifier

AS
	BEGIN
		SELECT DriverCategoryID
		FROM [RCI].[FleetDriver]
		WHERE UserID = @UserID AND TenantID = @TenantID AND IsActive = 1
	END
