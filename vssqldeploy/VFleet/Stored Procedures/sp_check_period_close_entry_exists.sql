﻿
--INTERNAL
CREATE PROCEDURE [VFleet].[sp_check_period_close_entry_exists]
@TENANT_ID			   [uniqueidentifier],
@PERIOD_START          [datetime2],
@PERIOD_END            [datetime2]

AS
	BEGIN
		DECLARE @FLEET_PAN UNIQUEIDENTIFIER		
		DECLARE @exists BIT = 0

		SELECT @FLEET_PAN = [PAN]
		FROM   [VFleet].[account] 
		WHERE  [OwnerId] = @TENANT_ID

		IF @FLEET_PAN IS NOT NULL
			IF EXISTS ( SELECT * FROM [VFleet].[payment_period_summary] WHERE [PAN] = @FLEET_PAN AND [PeriodStart] = @PERIOD_START AND [PeriodEnd] = @PERIOD_END )
				SET @exists = 1
		SELECT @exists
	END
