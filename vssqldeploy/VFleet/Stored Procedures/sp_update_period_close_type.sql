﻿

CREATE PROCEDURE [VFleet].[sp_update_period_close_type]
@TENANT_ID [uniqueidentifier],
@NEW_PERIOD_CLOSE_TYPE [INT],
@DAY_OF_THE_MONTH [INT],
@MODIFIED_BY [uniqueidentifier]

AS
	BEGIN
		DECLARE @STATUS INT = 1
		DECLARE @CURRENT_PERIOD_CLOSE_TYPE INT

		IF NOT EXISTS ( SELECT * FROM [VFleet].[account] WHERE [OwnerId] = @TENANT_ID )				
			RAISERROR('Could not find fleet account.',1,1);
		
		SELECT @CURRENT_PERIOD_CLOSE_TYPE = [PeriodCloseType]
		FROM  [VFleet].[account]
		WHERE [OwnerId] = @TENANT_ID
		
		IF( @CURRENT_PERIOD_CLOSE_TYPE = 1 AND @NEW_PERIOD_CLOSE_TYPE = 2 AND @DAY_OF_THE_MONTH >= 16 )			
			IF EXISTS ( SELECT * FROM [VFleet].[account] WHERE [OwnerId] = @TENANT_ID AND [IsActive] = 1 AND [AccountType] = 0 )
				SET @STATUS = 2  --status code: tells calling entity to create a Bi-Monthly Period Summary for first half of month with this PAN.
			
		BEGIN TRY
			UPDATE [VFleet].[account]
			SET [PeriodCloseType] = @NEW_PERIOD_CLOSE_TYPE,
				[PeriodCloseTypeModifiedOn] = GETDATE(), 
			    [PeriodCloseTypeModifiedBy] = @MODIFIED_BY
			WHERE [OwnerId] = @TENANT_ID
		END TRY
		BEGIN CATCH
			SET @STATUS = -1
		END CATCH

		SELECT @STATUS
	END
