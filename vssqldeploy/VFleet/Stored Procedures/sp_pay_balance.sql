﻿
--******(TH) New Procedure (NOT IN REPO YET). 
CREATE PROCEDURE [VFleet].[sp_pay_balance]
@TENANT_ID [uniqueidentifier]

AS
	BEGIN
		BEGIN TRY			
			DECLARE @STATUS			       INT = 100
			DECLARE @LATEST_PERIOD_END     datetime2
			DECLARE @FLEET_PAN			   uniqueidentifier
			DECLARE @FINAL_BALANCE         decimal(19,2)
			DECLARE @FINAL_CONV_BALANCE    decimal(19,2)
			DECLARE @TOTAL_FINAL_BALANCE   decimal(19,2)

			DECLARE @PAYMENT_BALANCE       decimal(19,2)
			DECLARE @PAYMENT_CONV_BALANCE  decimal(19,2)
			DECLARE @TOTAL_PAYMENT_BALANCE decimal(19,2)
				    
			SELECT @FLEET_PAN = [PAN]
			FROM   [VFleet].[account] 
			WHERE  [OwnerId] = @TENANT_ID

			IF @FLEET_PAN IS NULL
				RAISERROR('Could not find fleet account.',1,1); 

			SELECT @LATEST_PERIOD_END = MAX(PeriodEnd)
			FROM [VFleet].[payment_period_summary]
			WHERE [PAN] = @FLEET_PAN

			SELECT @FINAL_BALANCE = [FinalBalance], @PAYMENT_BALANCE = [PaymentBalance], @FINAL_CONV_BALANCE = [FinalProcessingFeeBalance], @PAYMENT_CONV_BALANCE = [PaymentProcessingFeeBalance]
			FROM [VFleet].[payment_period_summary]
			WHERE [PAN] = @FLEET_PAN AND [PeriodEnd] = @LATEST_PERIOD_END

			SET @TOTAL_FINAL_BALANCE = @FINAL_BALANCE + @FINAL_CONV_BALANCE
			SET @TOTAL_PAYMENT_BALANCE = @PAYMENT_BALANCE + @PAYMENT_CONV_BALANCE

			IF( ( @TOTAL_FINAL_BALANCE - @TOTAL_PAYMENT_BALANCE ) != 0 )
				BEGIN		
					BEGIN TRAN vcard_pay_balance
					BEGIN TRY
						INSERT INTO [VFleet].[account_activity]([refnumber],[p97txid],[pan],[amount],[activitytype])
						VALUES( NEWID(), NEWID(), @FLEET_PAN, @FINAL_BALANCE, 9 )

						UPDATE [VFleet].[payment_period_summary]
						SET [PaymentBalance] = @FINAL_BALANCE
						WHERE [PAN] = @FLEET_PAN AND [PeriodEnd] = @LATEST_PERIOD_END
				
						INSERT INTO [VFleet].[account_activity]([refnumber],[p97txid],[pan],[amount],[activitytype])
						VALUES( NEWID(), NEWID(), @FLEET_PAN, @FINAL_CONV_BALANCE, 10 )

						UPDATE [VFleet].[payment_period_summary]
						SET [PaymentProcessingFeeBalance] = @FINAL_CONV_BALANCE
						WHERE [PAN] = @FLEET_PAN AND [PeriodEnd] = @LATEST_PERIOD_END

						COMMIT TRAN vcard_pay_balance
					END TRY
					BEGIN CATCH
						ROLLBACK TRANSACTION vcard_pay_balance
						RAISERROR('Pay Balance Failed.',1,1); 
					END CATCH
				END
			ELSE
				SET @STATUS = 102 --Balance is already paid.
			
		END TRY
		BEGIN CATCH			
			SET @STATUS = 101
		END CATCH
			

		SELECT @STATUS
	END
