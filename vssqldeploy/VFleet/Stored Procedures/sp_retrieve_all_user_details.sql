﻿
CREATE PROCEDURE [VFleet].[sp_retrieve_all_user_details]
@USER_ID uniqueidentifier

AS
	BEGIN
		SELECT acct.[OwnerId], acct.[Limit], usr.[UserPaymentSourceID]
		FROM [VFleet].[UserVFleetPaymentSource] usr
			INNER JOIN [VFleet].[account] acct 
			ON usr.[PaymentProviderToken] = acct.[PAN]
		WHERE usr.[UserId] = @USER_ID AND
			  acct.[AccountType] = 1
	END
