﻿
CREATE PROCEDURE [VFleet].[sp_create_account]
 @ACCOUNT_TYPE INT,
 @TENANT_ID UNIQUEIDENTIFIER,
 @DRIVER_ID UNIQUEIDENTIFIER,
 @MODIFIED_BY UNIQUEIDENTIFIER,
 @LIMIT DECIMAL(19,2),
 @MOP INT,
 @PERIOD_CLOSE_ID INT,
 @IS_AUTO_PAY BIT,
 @DEFAULT_PERIOD_START datetime2,
 @DEFAULT_PERIOD_END datetime2,
 @IS_ACTIVE BIT

 AS
 BEGIN
   DECLARE @PAN             UNIQUEIDENTIFIER
   DECLARE @OWNER_ID        UNIQUEIDENTIFIER
   DECLARE @PARENT_ID       UNIQUEIDENTIFIER
   DECLARE @DRIVER_CATEGORY INT
   DECLARE @RESULT          INT = -1
   DECLARE @DRIVER_LIMIT    DECIMAL(19,2)
   DECLARE @FINAL_LIMIT     DECIMAL(19,2) = @LIMIT

   BEGIN TRANSACTION create_acct
   BEGIN TRY	   
		SET @PAN = NEWID()

		IF @ACCOUNT_TYPE = 0 --FLEET
			BEGIN
				SET @OWNER_ID = @TENANT_ID				
				IF NOT EXISTS(SELECT * FROM [VFleet].[account] WHERE [OWNERID] = @TENANT_ID)
					BEGIN
						INSERT INTO [VFleet].[account](
							[PAN],
							[ACCOUNTTYPE],
							[OWNERID],
							[LIMIT],
							[BALANCE],
							[TotalProcessingFees],
							[PaymentDeckId],
							[PeriodCloseType],
							[IsAutoPay],
							[IsActive],
							[CreatedOn],
							[LimitModifiedOn],
							[LimitModifiedBy],
							[BalanceModifiedOn],
							[BalanceModifiedBy],
							[TotalProcessingFeesModifiedOn],
							[TotalProcessingFeesModifiedBy],
							[PeriodCloseTypeModifiedOn],
							[PeriodCloseTypeModifiedBy])
						VALUES( @PAN, @ACCOUNT_TYPE, @TENANT_ID, @LIMIT, 0, 0, @MOP, @PERIOD_CLOSE_ID, @IS_AUTO_PAY, @IS_ACTIVE, GETDATE(), GETDATE(), @MODIFIED_BY, GETDATE(), @MODIFIED_BY, GETDATE(), @MODIFIED_BY, GETDATE(), @MODIFIED_BY )   

						INSERT INTO [VFleet].[payment_period_summary]([Id],[PAN],[StartBalance],[StartProcessingFeeBalance],[FinalBalance],[FinalProcessingFeeBalance],[PaymentBalance],[PaymentProcessingFeeBalance],[PeriodStart],[PeriodEnd])
						VALUES( NEWID(), @PAN, 0, 0, 0, 0, 0, 0, @DEFAULT_PERIOD_START, @DEFAULT_PERIOD_END )

						COMMIT TRANSACTION create_acct
						
						SET @RESULT   = 1
					END
			END
		ELSE IF @ACCOUNT_TYPE = 1 --DRIVER
			BEGIN
				SET @OWNER_ID  = @DRIVER_ID
				SET @PARENT_ID = @TENANT_ID
				
				IF EXISTS ( SELECT * FROM [VFleet].[account] WHERE [OWNERID] = @TENANT_ID )
					IF NOT EXISTS(SELECT * FROM [VFleet].[account] WHERE [OWNERID] = @DRIVER_ID AND [ParentId] = @TENANT_ID)						
						BEGIN
							SELECT @DRIVER_CATEGORY = [DriverCategoryID]
							FROM [RCI].[FleetDriver]
							WHERE [DriverID] = @DRIVER_ID							

							IF @DRIVER_CATEGORY IS NOT NULL
								BEGIN
									SELECT @DRIVER_LIMIT = [SpendingLimit]
									FROM [VFleet].[DriverCategoryLimit]
									WHERE [DriverCategoryId] = @DRIVER_CATEGORY AND [FleetTenantId] = @TENANT_ID

									IF @DRIVER_LIMIT IS NULL
									BEGIN
										SET @DRIVER_LIMIT = 0
										INSERT INTO [VFleet].[DriverCategoryLimit](
											[DriverCategoryId],
										    [FleetTenantId],
										    [SpendingLimit],
										    [ModifiedBy],
										    [ModifiedOn],
										    [FuelUsageLimit],
										    [ParkingOnUsageLimit],
										    [ParkingOffUsageLimit],
										    [CarWashUsageLimit],
										    [EvChargingUsageLimit]
										) VALUES( @DRIVER_CATEGORY, @TENANT_ID, 0, @MODIFIED_BY, GETDATE(), 0, 0, 0, 0, 0 )
									END
									SET @FINAL_LIMIT = @DRIVER_LIMIT
																																				
									INSERT INTO [VFleet].[account](
										[PAN],
										[ACCOUNTTYPE],
										[OWNERID],
										[PARENTID],
										[LIMIT],
										[BALANCE],
										[TotalProcessingFees],
										[PaymentDeckId],
										[IsActive],
										[CreatedOn],
										[LimitModifiedOn],
										[LimitModifiedBy],
										[BalanceModifiedOn],
										[BalanceModifiedBy],
										[TotalProcessingFeesModifiedOn],
										[TotalProcessingFeesModifiedBy])
									VALUES( @PAN, @ACCOUNT_TYPE, @DRIVER_ID, @TENANT_ID, @DRIVER_LIMIT, 0, 0, @MOP, @IS_ACTIVE, GETDATE(), GETDATE(), @MODIFIED_BY, GETDATE(), @MODIFIED_BY, GETDATE(), @MODIFIED_BY ) 			   				    
									COMMIT TRANSACTION create_acct													

									SET @RESULT   = 1
										
								END
						END		
			END
   END TRY

   BEGIN CATCH
    ROLLBACK TRANSACTION create_acct
   END CATCH
   
   SELECT @RESULT, @OWNER_ID, @PARENT_ID, @DRIVER_CATEGORY, @FINAL_LIMIT
 END
