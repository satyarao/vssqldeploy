﻿
/************************************************************************* 
 TEST SPROCS 
*************************************************************************/ 
CREATE PROCEDURE [VFleet].[sp_test_populate_activity_tables]
@TENANT_ID uniqueidentifier,
@MERCHANT_ID uniqueidentifier,
@SEQUENCE_ID NVARCHAR(50),
@PERIOD NVARCHAR(50),
@AUTH_AMT [DECIMAL](19,2),
@CAPTURE_AMT [DECIMAL](19,2),
@CONV_FEE_AMT [DECIMAL](19,2),
@TIME_STAMP datetime2,
@DRIVER_ID uniqueidentifier

AS
	BEGIN
		DECLARE @PAN uniqueidentifier
		DECLARE @TXN_ID uniqueidentifier
		DECLARE @REF_NUM uniqueidentifier

		SELECT @PAN = [PAN]
		FROM   [VFleet].[account] 
		WHERE  [OwnerId] = @TENANT_ID
		
		IF @PAN IS NULL
			RAISERROR('Could not find fleet account with pan.',1,1);

		-- Auth -> Capture ( auth_amt, conv_fee_amt )
		SET @TXN_ID  = NEWID()
		SET @REF_NUM = NEWID()
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 2, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CAPTURE_AMT, 3, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
            VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CONV_FEE_AMT, 5, @TIME_STAMP ); 

		-- Auth -> Reversal ($0)
		SET @TXN_ID  = NEWID()
		SET @REF_NUM = NEWID()
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 2, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, 0.0, 4, @TIME_STAMP );		

		-- Auth-Hold Only
		SET @TXN_ID  = NEWID()
		SET @REF_NUM = NEWID()
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );

		--Auth-Hold and Auth-Release Only
		SET @TXN_ID  = NEWID()
		SET @REF_NUM = NEWID()
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 2, @TIME_STAMP );

		-- Auth -> Capture ( auth_amt, conv_fee_amt )
		SET @TXN_ID  = NEWID()
		SET @REF_NUM = NEWID()
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 2, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CAPTURE_AMT, 3, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
            VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @CONV_FEE_AMT, 5, @TIME_STAMP ); 

		-- Auth -> Reversal (without Auth-Release)
		SET @TXN_ID  = NEWID()
		SET @REF_NUM = NEWID()
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, @AUTH_AMT, 1, @TIME_STAMP );
		INSERT INTO [VFleet].[account_activity] ([refnumber],[p97txid],[pan],[driverid],[merchantid],[sequenceid],[period],[amount],[activitytype],[timestamp])
			VALUES( @REF_NUM, @TXN_ID, @PAN, @DRIVER_ID, @MERCHANT_ID, @SEQUENCE_ID, @PERIOD, 0.0, 4, @TIME_STAMP );
	END
