﻿ 


 --INTERNAL
CREATE PROCEDURE [VFleet].[sp_update_balance] @PAN         UNIQUEIDENTIFIER, 
                                              @AMOUNT      DECIMAL(19, 2), 
											  @MODIFIED_BY UNIQUEIDENTIFIER, 
                                              @UPDATE_TYPE BIT 
-- 0 = Credit (-balance), 1 = Debit (+balance) 
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from  
      -- interfering with SELECT statements.  
      SET nocount ON; 

      DECLARE @CURRENT_BAL DECIMAL(19, 2) 
      DECLARE @BALANCE_ADJUST DECIMAL(19, 2) 

      IF @UPDATE_TYPE = 0 
        SET @BALANCE_ADJUST = @AMOUNT  * -1; 
      ELSE 
        SET @BALANCE_ADJUST = @AMOUNT; 

      SELECT @CURRENT_BAL = [balance] 
      FROM   [VFleet].[account] 
      WHERE  [pan] = @PAN 

      UPDATE [VFleet].[account] 
      SET    [balance] = @CURRENT_BAL + @BALANCE_ADJUST,
			 [BalanceModifiedOn] = GETDATE(), 
			 [BalanceModifiedBy] = @MODIFIED_BY
      WHERE  [pan] = @PAN 
    
  END 

