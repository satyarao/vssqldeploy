﻿
CREATE PROCEDURE [VFleet].[sp_decrement_service_usage_balance]
	@SERVICE_TYPE INT,
	@USER_ID uniqueidentifier,
	@FLEET_ID uniqueidentifier
AS
	BEGIN
		DECLARE @STATUS	   INT = -1
		DECLARE @DRIVER_ID uniqueidentifier

		SELECT @DRIVER_ID = DriverID
		FROM [RCI].[FleetDriver]
		WHERE UserID = @USER_ID AND TenantID = @FLEET_ID AND IsActive = 1

		IF @DRIVER_ID IS NULL
			RAISERROR('COULD NOT FIND DRIVER_ID',1,1);  

		IF ( @SERVICE_TYPE = 0 )
			UPDATE [RCi].[FleetDriver]
			SET FuelUsageBalance = FuelUsageBalance - 1
			WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID AND FuelUsageBalance > 0
		
		ELSE IF ( @SERVICE_TYPE = 1 )
			UPDATE [RCi].[FleetDriver]
			SET EvChargingUsageBalance = EvChargingUsageBalance - 1
			WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID AND EvChargingUsageBalance > 0

		ELSE IF ( @SERVICE_TYPE = 2 )						  																				
			UPDATE [RCi].[FleetDriver]
			SET ParkingOffUsageBalance = ParkingOffUsageBalance - 1
			WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID AND ParkingOffUsageBalance > 0		
			
		ELSE IF ( @SERVICE_TYPE = 3 )						  																				
			UPDATE [RCi].[FleetDriver]
			SET ParkingOnUsageBalance = ParkingOnUsageBalance - 1
			WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID AND ParkingOnUsageBalance > 0	

		ELSE IF ( @SERVICE_TYPE = 4 )						  																			
			UPDATE [RCi].[FleetDriver]
			SET CarWashUsageBalance = CarWashUsageBalance - 1
			WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID AND CarWashUsageBalance > 0

		IF ( @@ROWCOUNT < 1 )
			SET @STATUS = -3
		ELSE
			SET @STATUS = 1

		SELECT @STATUS
	END
