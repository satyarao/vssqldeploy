﻿


--******(TH) New Procedure (NOT IN REPO YET). 
CREATE PROCEDURE [VFleet].[sp_get_payment_period_summary]
@TENANT_ID uniqueidentifier,
@TIME_RANGE_START datetime2,
@TIME_RANGE_END datetime2
AS
	BEGIN
		DECLARE @FLEET_PAN uniqueidentifier

		SELECT @FLEET_PAN = [PAN]
		FROM   [VFleet].[account] 
		WHERE  [OwnerId] = @TENANT_ID

		SELECT   [StartBalance],[StartProcessingFeeBalance],[FinalBalance],[FinalProcessingFeeBalance],[PaymentBalance],[PaymentProcessingFeeBalance],[PeriodStart],[PeriodEnd]
		FROM     [VFleet].[payment_period_summary]
		WHERE    [PAN] = @FLEET_PAN AND [PeriodStart] >= @TIME_RANGE_START AND [PeriodEnd] <= @TIME_RANGE_END
		ORDER BY [PeriodEnd] DESC
	END
