﻿
--VL - UPDATED 12/19 NOT IN REPO YET
CREATE PROCEDURE [VFleet].[sp_get_wallet]
@UserID uniqueidentifier

AS
	DECLARE @results TABLE (USER_PAYMENT_SOURCE_ID uniqueidentifier, LS_USER_PAYMENT_SOURCE_ID int, PROVIDER_ID int, PAYMENT_PROVIDER_TOKEN uniqueidentifier, OWNER_ID uniqueidentifier )

	BEGIN
	INSERT INTO @results ( USER_PAYMENT_SOURCE_ID, LS_USER_PAYMENT_SOURCE_ID, PROVIDER_ID, PAYMENT_PROVIDER_TOKEN, OWNER_ID )
	SELECT usr.[ID], usr.[LS_UserPaymentSourceID], usr.[ProviderID], vf.[PaymentProviderToken], act.[PAN]
	FROM [VFleet].[UserVFleetPaymentSource] as vf INNER JOIN [PaymentsEx].[LKUserPaymentSource] as usr ON vf.[UserPaymentSourceID] = usr.[ID] 
	INNER JOIN [VFleet].[account] as act ON vf.[PaymentProviderToken] = act.[PAN]
	WHERE usr.UserID = @UserID AND usr.ProviderId = 901

	SELECT USER_PAYMENT_SOURCE_ID, LS_USER_PAYMENT_SOURCE_ID, PROVIDER_ID, OWNER_ID
			FROM @results
	END
