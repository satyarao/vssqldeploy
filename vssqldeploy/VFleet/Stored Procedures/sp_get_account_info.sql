﻿		


--******(TH) New Procedure (NOT IN REPO YET).
CREATE PROCEDURE [VFleet].[sp_get_account_info]
@TenantId    [UNIQUEIDENTIFIER]

AS
	BEGIN
		SELECT [OwnerId],
		[ParentId],
		[Limit],
		[Balance],
		[TotalProcessingFees],
		[PaymentDeckID],
		[PeriodCloseType],
		[IsAutoPay],
		[IsActive],
		[CreatedOn],
		[LimitModifiedOn],			    
	    [LimitModifiedBy],			    								     
	    [BalanceModifiedOn],		       
	    [BalanceModifiedBy],		        
	    [TotalProcessingFeesModifiedOn], 
	    [TotalProcessingFeesModifiedBy],
		[PeriodCloseTypeModifiedOn],
	    [PeriodCloseTypeModifiedBy]
		FROM   [VFleet].[account] 
		WHERE  [OwnerId] = @TenantId AND [ParentId] IS NULL
	END
