﻿

--******(TH) Updated Procedure (NOT IN REPO YET).
CREATE PROCEDURE [VFleet].[sp_capture_auth] -- called after the auth.
   @REF_NUM			      UNIQUEIDENTIFIER, 
   @AMOUNT			      DECIMAL(19, 2),
   @PROCESSING_FEE_AMOUNT DECIMAL(19, 2),
   @PERIOD			      NVARCHAR(50),
   @MODIFIED_BY           UNIQUEIDENTIFIER
AS 
BEGIN
   --look for auth_hold
     -- if no auth_hold, set @STATUS = -2
   --look for auth_release
     --if auth_release,  set @STATUS = -3

    DECLARE @FLEET_PAN				UNIQUEIDENTIFIER
	DECLARE @DRIVER_PAN             UNIQUEIDENTIFIER
	DECLARE @DRIVER_ID			    UNIQUEIDENTIFIER
	DECLARE @STATUS					INT = -1  --assume error
	DECLARE @AUTH_HOLD_AMOUNT		DECIMAL(19, 2)
	DECLARE @BALANCE_ADJUST			DECIMAL(19, 2)
	DECLARE @MERCHANTID				NVARCHAR(50)
	DECLARE @SEQUENCEID				NVARCHAR(50)
	DECLARE @P97TXID				UNIQUEIDENTIFIER

	SELECT TOP 1 @FLEET_PAN = [pan], @DRIVER_ID = [driverid] 
	   FROM  [VFleet].[account_activity] 
	   WHERE [refnumber] = @REF_NUM;

	SELECT TOP 1 @DRIVER_PAN = [PAN]
		FROM [VFleet].[account]
		WHERE [OwnerId] = @DRIVER_ID
	

	-- Find AUTH Record  
	-- IF NOT FOUND, ERROR -1 
	IF EXISTS ( SELECT * FROM [VFleet].[account_activity] WHERE [p97txid] = @P97TXID AND [activitytype] >= 2 AND [activitytype] <= 5 )
		SET @STATUS = 6 --already processed capture for this txnId.

	ELSE IF @FLEET_PAN IS NULL	
		SET @STATUS = -1

	ELSE IF @DRIVER_PAN IS NULL
		SET @STATUS = -1

    --is it already captured?
    ELSE IF EXISTS (SELECT 1 FROM [VFleet].[account_activity] x WHERE x.[pan] = @FLEET_PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 3)
		   SET @STATUS = -2;

	ELSE
	   BEGIN

		 -- FIND AUTH_RELEASE, if Exists :(
		  IF EXISTS (SELECT 1 FROM [VFleet].[account_activity] x 
		  			 WHERE x.[pan] = @FLEET_PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 2)
		     BEGIN
		        SET @STATUS = -3;
		     END

		  -- Determine if no auth record exists
		  ELSE IF NOT EXISTS (SELECT 1 FROM [VFleet].[account_activity] x 
							  WHERE x.[pan] = @FLEET_PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 1) 
		     BEGIN
		        SET @STATUS = -4;
		     END

	      -- Auth record exists
		  ELSE
		     BEGIN				
			    SELECT @AUTH_HOLD_AMOUNT = x.[amount], 
					   @P97TXID = x.[p97txid],
					   @MERCHANTID = x.[merchantid],
					   @SEQUENCEID = x.[sequenceid]
			    FROM [VFleet].[account_activity] x 
				WHERE  x.[pan] = @FLEET_PAN AND x.[refnumber] = @REF_NUM AND x.[activitytype] = 1 

				-- CREATE AUTH_RELEASE  
				-- Create Activity Record
				-- Create Processing Fee Record
				BEGIN TRAN capture
				BEGIN TRY
					   INSERT INTO [VFleet].[account_activity] ([refnumber],[pan],[driverid],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype]) 
					   VALUES      (@REF_NUM, 
									@FLEET_PAN, 
									@DRIVER_ID,
									@P97TXID,
									@PERIOD, 
									@AUTH_HOLD_AMOUNT,
									@MERCHANTID, 
									@SEQUENCEID,
									2); -- AUTH_RELEASE
			                   

		  			   -- Create Capture Record  
		  			   INSERT INTO [VFleet].[account_activity] ([refnumber],[pan],[driverid],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype]) 
		  			        VALUES (@REF_NUM, 
		  						    @FLEET_PAN, 
									@DRIVER_ID,
		  						    @P97TXID,
		  						    @PERIOD, 
		  						    @AMOUNT,
		  						    @MERCHANTID, 
		  						    @SEQUENCEID,
		  						    3); -- Capture

				       -- Update the Balance   
					   SET @BALANCE_ADJUST = @AUTH_HOLD_AMOUNT - @AMOUNT; 

					   IF @BALANCE_ADJUST > 0 
					       BEGIN
						       EXEC [VFleet].[sp_update_balance] @FLEET_PAN,  @BALANCE_ADJUST, @MODIFIED_BY, 0  -- CREDIT the difference between the AUTH_HOLD and the CAPTURE amount 
							   EXEC [VFleet].[sp_update_balance] @DRIVER_PAN, @BALANCE_ADJUST, @MODIFIED_BY, 0  -- CREDIT the difference between the AUTH_HOLD and the CAPTURE amount 
						   END
			    	      

						-- Create/Update Convenience Fee Records
						INSERT INTO [VFleet].[account_activity] ([refnumber],[pan],[driverid],[p97txid],[period],[amount],[merchantid],[sequenceid],[activitytype])
						     VALUES (NEWID(),
									 @FLEET_PAN,
									 @DRIVER_ID,
									 @P97TXID,
									 @PERIOD,
									 @PROCESSING_FEE_AMOUNT,
									 @MERCHANTID, 
		  						     @SEQUENCEID,
									 5);

						UPDATE [VFleet].[account]
						   SET [TotalProcessingFees] = [TotalProcessingFees] + @PROCESSING_FEE_AMOUNT,
						       [TotalProcessingFeesModifiedOn] = GETDATE(), 
							   [TotalProcessingFeesModifiedBy] = @MODIFIED_BY
						 WHERE [pan] = @FLEET_PAN

						 UPDATE [VFleet].[account]
						   SET [TotalProcessingFees] = [TotalProcessingFees] + @PROCESSING_FEE_AMOUNT,
						       [TotalProcessingFeesModifiedOn] = GETDATE(), 
							   [TotalProcessingFeesModifiedBy] = @MODIFIED_BY
						 WHERE [pan] = @DRIVER_PAN

						COMMIT TRAN capture
						  SET @STATUS = 1; -- Successful Status = 1 
					END TRY

					BEGIN CATCH
						ROLLBACK TRANSACTION capture
						PRINT ERROR_MESSAGE()
						SET @STATUS = -1; 
					END CATCH
			END
		END
  SELECT @STATUS
END
