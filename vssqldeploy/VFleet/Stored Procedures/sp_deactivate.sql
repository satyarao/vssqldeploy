﻿
CREATE PROCEDURE [VFleet].[sp_deactivate]
 @UserID uniqueidentifier,
 @UserPaymentSourceID uniqueidentifier
 AS
 BEGIN
  DECLARE @Result INT = -1  -- assume error
  DECLARE @LS_UserPaymentSourceID INT

  SELECT @LS_UserPaymentSourceID = [LS_UserPaymentSourceID] FROM [PaymentsEx].[LKUserPaymentSource] WHERE [UserID] = @UserID AND [ID] = @UserPaymentSourceID

  BEGIN TRAN sp_vcard_deactivate
  BEGIN TRY
    UPDATE [VFleet]    .[UserVFleetPaymentSource] SET [IsActive] = 0  WHERE [UserID] = @UserID AND [UserPaymentSourceID] = @UserPaymentSourceID
    UPDATE [PaymentsEx].[LKUserPaymentSource]     SET [IsActive] = 0, [UpdatedOn] = GETDATE() WHERE [UserID] = @UserID AND [ID] = @UserPaymentSourceID
    UPDATE [Payment]   .[LKUserPaymentSource]     SET [IsActive] = 0, [UpdatedOn] = GETDATE() WHERE [UserID] = @UserID AND [UserPaymentSourceID] = @LS_UserPaymentSourceID
	COMMIT TRAN sp_vcard_deactivate
    SET @Result = 1
  END TRY
  BEGIN CATCH
   ROLLBACK TRANSACTION sp_vcard_deactivate
   SET @Result = -1
  END CATCH
  SELECT @Result
END
