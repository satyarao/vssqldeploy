﻿
CREATE PROCEDURE [VFleet].[sp_increment_service_usage_balance]
	@SERVICE_TYPE INT,
	@USER_ID uniqueidentifier,
	@FLEET_ID uniqueidentifier
AS
	BEGIN
		DECLARE @STATUS			       INT = -1
		DECLARE @SERVICE_USAGE_LIMIT   INT
		DECLARE @SERVICE_USAGE_BALANCE INT
		DECLARE @DRIVER_ID			   uniqueidentifier
		DECLARE @DRIVER_CATEGORY_ID	   INT

		SELECT @DRIVER_ID = DriverID, @DRIVER_CATEGORY_ID = DriverCategoryID
		FROM [RCI].[FleetDriver]
		WHERE UserID = @USER_ID AND TenantID = @FLEET_ID AND IsActive = 1

		IF @DRIVER_ID IS NULL
			RAISERROR('COULD NOT FIND DRIVER_ID',1,1); 
		IF @DRIVER_CATEGORY_ID IS NULL
			RAISERROR('COULD NOT FIND DRIVER_CATEGORY_ID',1,1); 

	  IF ( @SERVICE_TYPE = 0 )
		  BEGIN
			IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID AND FuelUsageLimit IS NULL )
				SET @STATUS = 1
			ELSE
				BEGIN
					SELECT @SERVICE_USAGE_LIMIT = FuelUsageLimit
					FROM [VFleet].[DriverCategoryLimit]
					WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID

					SELECT @SERVICE_USAGE_BALANCE = FuelUsageBalance
					FROM [RCi].[FleetDriver]
					WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID

					IF @SERVICE_USAGE_LIMIT IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_LIMIT',1,1); 
					IF @SERVICE_USAGE_BALANCE IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_BALANCE',1,1); 

					IF ( @SERVICE_USAGE_BALANCE < @SERVICE_USAGE_LIMIT )
						BEGIN
							UPDATE [RCi].[FleetDriver]
							  SET FuelUsageBalance = FuelUsageBalance + 1
							  WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID
							SET @STATUS = 1
						END
					ELSE
						SET @STATUS = -2
				END
		  END

	  ELSE IF ( @SERVICE_TYPE = 1 )
		  BEGIN
		  	IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID AND EvChargingUsageLimit IS NULL )
				SET @STATUS = 1
			ELSE
				BEGIN
					SELECT @SERVICE_USAGE_LIMIT = EvChargingUsageLimit
					FROM [VFleet].[DriverCategoryLimit]
					WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID

					SELECT @SERVICE_USAGE_BALANCE = EvChargingUsageBalance
					FROM [RCi].[FleetDriver]
					WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID

					IF @SERVICE_USAGE_LIMIT IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_LIMIT',1,1); 
					IF @SERVICE_USAGE_BALANCE IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_BALANCE',1,1); 

					IF ( @SERVICE_USAGE_BALANCE < @SERVICE_USAGE_LIMIT )
						BEGIN
							UPDATE [RCi].[FleetDriver]
							  SET EvChargingUsageBalance = EvChargingUsageBalance + 1
							  WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID
							SET @STATUS = 1
						END
					ELSE
						SET @STATUS = -2
				END
		  END

	  ELSE IF ( @SERVICE_TYPE = 2 )
		  BEGIN
		  	IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID AND ParkingOffUsageLimit IS NULL )
				SET @STATUS = 1
			ELSE
				BEGIN
					SELECT @SERVICE_USAGE_LIMIT = ParkingOffUsageLimit
					FROM [VFleet].[DriverCategoryLimit]
					WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID

					SELECT @SERVICE_USAGE_BALANCE = ParkingOffUsageBalance
					FROM [RCi].[FleetDriver]
					WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID

					IF @SERVICE_USAGE_LIMIT IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_LIMIT',1,1); 
					IF @SERVICE_USAGE_BALANCE IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_BALANCE',1,1); 

					IF ( @SERVICE_USAGE_BALANCE < @SERVICE_USAGE_LIMIT )
						BEGIN
							UPDATE [RCi].[FleetDriver]
							  SET ParkingOffUsageBalance = ParkingOffUsageBalance + 1
							  WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID
							SET @STATUS = 1
						END
					ELSE
						SET @STATUS = -2
				END
		  END

	  ELSE IF ( @SERVICE_TYPE = 3 )
		  BEGIN
		  	IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID AND ParkingOnUsageLimit IS NULL )
				SET @STATUS = 1
			ELSE
				BEGIN
					SELECT @SERVICE_USAGE_LIMIT = ParkingOnUsageLimit
					FROM [VFleet].[DriverCategoryLimit]
					WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID

					SELECT @SERVICE_USAGE_BALANCE = ParkingOnUsageBalance
					FROM [RCi].[FleetDriver]
					WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID

					IF @SERVICE_USAGE_LIMIT IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_LIMIT',1,1); 
					IF @SERVICE_USAGE_BALANCE IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_BALANCE',1,1); 

					IF ( @SERVICE_USAGE_BALANCE < @SERVICE_USAGE_LIMIT )
						BEGIN
							UPDATE [RCi].[FleetDriver]
							  SET ParkingOnUsageBalance = ParkingOnUsageBalance + 1
							  WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID
							SET @STATUS = 1
						END
					ELSE
						SET @STATUS = -2
				END
		  END

      ELSE IF ( @SERVICE_TYPE = 4 )
		  BEGIN
		  	IF EXISTS ( SELECT * FROM [VFleet].[DriverCategoryLimit] WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID AND CarWashUsageLimit IS NULL )
				SET @STATUS = 1
			ELSE
				BEGIN
					SELECT @SERVICE_USAGE_LIMIT = CarWashUsageLimit
					FROM [VFleet].[DriverCategoryLimit]
					WHERE DriverCategoryId = @DRIVER_CATEGORY_ID AND FleetTenantId = @FLEET_ID

					SELECT @SERVICE_USAGE_BALANCE = CarWashUsageBalance
					FROM [RCi].[FleetDriver]
					WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID

					IF @SERVICE_USAGE_LIMIT IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_LIMIT',1,1); 
					IF @SERVICE_USAGE_BALANCE IS NULL
						RAISERROR('COULD NOT FIND SERVICE_USAGE_BALANCE',1,1); 

					IF ( @SERVICE_USAGE_BALANCE < @SERVICE_USAGE_LIMIT )
						BEGIN
							UPDATE [RCi].[FleetDriver]
							  SET CarWashUsageBalance = CarWashUsageBalance + 1
							  WHERE DriverID = @DRIVER_ID AND TenantID = @FLEET_ID
							SET @STATUS = 1
						END
					ELSE
						SET @STATUS = -2
				END
		  END

		IF ( @@ROWCOUNT < 1 )
			SET @STATUS = -3

		SELECT @STATUS
	END
