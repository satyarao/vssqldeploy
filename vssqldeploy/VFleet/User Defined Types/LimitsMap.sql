﻿CREATE TYPE [VFleet].[LimitsMap] AS TABLE (
    [LimitType] INT             NULL,
    [Limit]     DECIMAL (19, 2) NULL);

