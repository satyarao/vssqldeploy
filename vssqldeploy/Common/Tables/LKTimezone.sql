﻿CREATE TABLE [Common].[LKTimezone] (
    [TimeZoneID]                 INT            NOT NULL,
    [Identifier]                 NVARCHAR (100) NULL,
    [StandardName]               NVARCHAR (100) NULL,
    [DisplayName]                NVARCHAR (100) NULL,
    [DaylightName]               NVARCHAR (100) NULL,
    [SupportsDaylightSavingTime] BIT            NULL,
    [BaseUtcOffsetSec]           INT            NULL,
    [AbbreviatedDisplayName]     NVARCHAR (100) NULL,
    CONSTRAINT [PK_Timezone] PRIMARY KEY CLUSTERED ([TimeZoneID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CHK_LKTimezone_Identifier] CHECK ([Common].[IsValidIdentifier]([Identifier])=(1))
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Timezone_Identifier]
    ON [Common].[LKTimezone]([Identifier] ASC) WITH (FILLFACTOR = 80);

