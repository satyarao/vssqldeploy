﻿CREATE TABLE [Common].[TimezoneAdjustmentRule] (
    [Id]                                     INT           NOT NULL,
    [TimezoneID]                             INT           NULL,
    [RuleNo]                                 INT           NULL,
    [DateStart]                              DATETIME2 (7) NULL,
    [DateEnd]                                DATETIME2 (7) NULL,
    [DaylightTransitionStartIsFixedDateRule] BIT           NULL,
    [DaylightTransitionStartMonth]           INT           NULL,
    [DaylightTransitionStartDay]             INT           NULL,
    [DaylightTransitionStartWeek]            INT           NULL,
    [DaylightTransitionStartDayOfWeek]       INT           NULL,
    [DaylightTransitionStartTimeOfDay]       TIME (7)      NULL,
    [DaylightTransitionEndIsFixedDateRule]   BIT           NULL,
    [DaylightTransitionEndMonth]             INT           NULL,
    [DaylightTransitionEndDay]               INT           NULL,
    [DaylightTransitionEndWeek]              INT           NULL,
    [DaylightTransitionEndDayOfWeek]         INT           NULL,
    [DaylightTransitionEndTimeOfDay]         TIME (7)      NULL,
    [DaylightDeltaSec]                       INT           NULL,
    CONSTRAINT [PK_TimezoneAdjustmentRule] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_TimezoneAdjustmentRule_Timezone] FOREIGN KEY ([TimezoneID]) REFERENCES [Common].[LKTimezone] ([TimeZoneID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TimezoneAdjustmentRule_TimezoneId_DateStart_DateEnd]
    ON [Common].[TimezoneAdjustmentRule]([TimezoneID] ASC, [DateStart] ASC, [DateEnd] ASC) WITH (FILLFACTOR = 80);

