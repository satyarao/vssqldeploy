﻿
CREATE PROCEDURE [Common].[GetRelativeTimeZoneByOffset]
	@UTC_Offset_Seconds INT,
	@NumberOfResults INT
AS
BEGIN
	SET NOCOUNT ON;
	IF @NumberOfResults <= 0
	BEGIN
		SELECT @NumberOfResults = COUNT(*)
		FROM [Common].[LKTimezone]
	END

	SELECT TOP (@NumberOfResults) 
	   [TimeZoneID]
      ,[Identifier]
      ,[StandardName]
      ,[DisplayName]
      ,[DaylightName]
      ,[BaseUtcOffsetSec]
	  ,(ABS([BaseUtcOffsetSec] + @UTC_Offset_Seconds)) AS OffsetDifference
  FROM [Common].[LKTimezone]
  ORDER BY OffsetDifference ASC
END
