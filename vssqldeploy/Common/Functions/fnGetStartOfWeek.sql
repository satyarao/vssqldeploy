﻿
/*
    SET DATEFIRST 7 -- Sunday
    SET DATEFIRST 1 -- Monday

    SELECT [Common].[fnGetStartOfWeek] (GETDATE())
    SELECT [Common].[fnGetStartOfWeek] ('2014-02-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetStartOfWeek]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL
	  , @DayOfWeek INT = @@DATEFIRST;  

	DECLARE	@FirstOfWeekday DATETIME2 = DATEADD(DAY, @DayOfWeek - 1, 0);

	SELECT	@Result = DATEADD(WEEK, DATEDIFF(WEEK, @FirstOfWeekday, @ReferenceDate), @FirstOfWeekday);

	RETURN @Result;

END
