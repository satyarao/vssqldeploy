﻿

/*
    here is an example
    SELECT [Common].[MPPAID_STRING_TO_INT] ('0032-5601')
    returns 325601
*/
CREATE   FUNCTION [Common].[MPPAID_STRING_TO_INT]
(
	@mppaid_string VARCHAR(9)
)
RETURNS int
WITH SCHEMABINDING
AS
BEGIN
	DECLARE	@mppaid_int VARCHAR(9) = 0;  
	DECLARE	@zeromppaid VARCHAR(9) = '00000000';  
	DECLARE	@mppaidNoDash VARCHAR(9) = '00000000'; 

	IF LEN(@mppaid_string) = 9
	BEGIN
		

		IF SUBSTRING(@mppaid_string,5,1) = '-'
		begin
			SELECT @mppaidNoDash = REPLACE ( @mppaid_string , '-' , '' ) 
			IF @mppaidNoDash != @zeromppaid
			BEGIN
				SELECT @mppaid_int = RIGHT(@mppaidNoDash,(LEN(@mppaidNoDash) - PATINDEX('%[^0]%',@mppaidNoDash)) + 1)
			END
		end
	END

	RETURN @mppaid_int;

END

