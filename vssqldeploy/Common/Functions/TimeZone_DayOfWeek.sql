﻿


CREATE   FUNCTION [Common].[TimeZone_DayOfWeek] (@TimeZoneId int, @isLocalTime bit = 1)
RETURNS nvarchar(50)
WITH SCHEMABINDING
AS
BEGIN

	RETURN 
	(
		SELECT
			CASE
				WHEN @isLocalTime = 0
					THEN LOWER(DATENAME(WEEKDAY, GETUTCDATE()))
				ELSE	LOWER(
							DATENAME(
								WEEKDAY, 
								GETUTCDATE() AT TIME ZONE 
									(SELECT TOP(1) [Identifier] FROM [Common].[LKTimezone] WITH(NOLOCK) WHERE [TimeZoneID] =  @TimeZoneId)
							)
						)
			END
	)

END
