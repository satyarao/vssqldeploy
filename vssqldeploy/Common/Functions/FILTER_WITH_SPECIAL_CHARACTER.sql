﻿-- =============================================
-- Author:		Igor Gaidukov
-- Create date: 11/25/2016
-- Description:	Create Where clause for filtering by keywords with special characters (&,# etc.) 
-- =============================================
CREATE FUNCTION [Common].[FILTER_WITH_SPECIAL_CHARACTER]
(
	@FilterName NVARCHAR(MAX),
	@Words NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE	@filter NVARCHAR(max) = NULL;  
	IF @Words IS NOT NULL
    begin
		SET @filter = ltrim(rtrim(@Words));
		IF @filter = ''
		BEGIN
			SET @filter = NULL
        END 
		ELSE
        BEGIN
			SET @Words = REPLACE(@Words, '''', '''''')
			SET @filter = @FilterName + ' LIKE ''%' + REPLACE(replace(replace(@Words,' ','<>'),'><',''),'<>','%''AND ' + @FilterName + ' LIKE ''%') + '%'' '
		end
	END 

	RETURN @filter;

END
