﻿
/*
    SELECT [Common].[fnGetStartOfYear] (GETDATE())
    SELECT [Common].[fnGetStartOfYear] ('2013-05-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetStartOfYear]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL;  

	SET @ReferenceDate = CONVERT(DATE, @ReferenceDate);

	SELECT	@Result = DATEADD(YEAR, DATEDIFF(YEAR, 0, @ReferenceDate), 0);

	RETURN @Result;

END
