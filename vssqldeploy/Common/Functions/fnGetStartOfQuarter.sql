﻿
/*
    SELECT [Common].[fnGetStartOfQuarter] (GETDATE())
    SELECT [Common].[fnGetStartOfQuarter] ('2014-02-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetStartOfQuarter]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL;  

	SELECT	@Result = DATEADD(QUARTER, DATEDIFF(QUARTER, 0, @ReferenceDate), 0);

	RETURN @Result;

END
