﻿

/*
    here is an example
    SELECT [Common].[MPPAID_INT_TO_STRING] (325601)
    returns '0032-5601'
*/
CREATE   FUNCTION [Common].[MPPAID_INT_TO_STRING]
(
	@mppaid_int int
)
RETURNS VARCHAR(9)
WITH SCHEMABINDING
AS
BEGIN
	DECLARE	@mppaid_string VARCHAR(9) = '0000-0000';  

	SELECT	@mppaid_string = SUBSTRING(REPLICATE('0',8-LEN(RTRIM(@mppaid_int))) + RTRIM(@mppaid_int),1,4) + '-' + SUBSTRING(REPLICATE('0',8-LEN(RTRIM(@mppaid_int))) + RTRIM(@mppaid_int),5,8);

	RETURN @mppaid_string;

END

