﻿
/*
    Supported values for parameter @DayOfWeek
        1 = Monday
        2 = Tuesday
        3 = Wednesday
        4 = Thursday
        5 = Friday
        6 = Saturday
        0 or 7 = Sunday

    SELECT [Common].[fnGetFirstWeekdayInMonth] (0, GETDATE())
    SELECT [Common].[fnGetFirstWeekdayInMonth] (1, GETDATE())
    SELECT [Common].[fnGetFirstWeekdayInMonth] (2, GETDATE())
    SELECT [Common].[fnGetFirstWeekdayInMonth] (3, GETDATE())
    SELECT [Common].[fnGetFirstWeekdayInMonth] (4, GETDATE())
    SELECT [Common].[fnGetFirstWeekdayInMonth] (5, GETDATE())
    SELECT [Common].[fnGetFirstWeekdayInMonth] (6, GETDATE())
    SELECT [Common].[fnGetFirstWeekdayInMonth] (7, GETDATE())
*/
CREATE FUNCTION [Common].[fnGetFirstWeekdayInMonth]
	(@DayOfWeek INT
   , @ReferenceDate DATETIME2)
RETURNS DATE
AS
BEGIN
	DECLARE	@Result DATE = NULL;  

	SELECT	@Result = DATEADD(DAY, 7, [Common].[fnGetLastWeekdayInMonth](@DayOfWeek, DATEADD(MONTH, -1, @ReferenceDate)));

	RETURN @Result;

END
