﻿
/*
    SELECT [Common].[fnGetEndOfYear] (GETDATE())
    SELECT [Common].[fnGetEndOfYear] ('2013-02-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetEndOfYear]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL;  

	SELECT	@Result = [Common].[fnGetEndOfDay](DATEADD(YEAR, DATEDIFF(YEAR, -1, @ReferenceDate), -1));

	RETURN @Result;

END
