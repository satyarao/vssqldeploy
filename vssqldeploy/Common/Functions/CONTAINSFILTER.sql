﻿

/*
    here is an example
    SELECT [Common].[MPPAID_INT_TO_STRING] (325601)
    returns '0032-5601'
*/
CREATE FUNCTION [Common].[CONTAINSFILTER]
(
	@Words NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE	@filter NVARCHAR(max) = NULL;  
	IF @Words IS NOT NULL
    begin
		SET @filter = ltrim(rtrim(@Words));
		IF @filter = ''
		BEGIN
			SET @filter = NULL
        END 
		ELSE
        BEGIN
			SET @filter = '''' + REPLACE(replace(replace(@Words,' ','<>'),'><',''),'<>',' AND ') + ''''
		end
	END 

	RETURN @filter;

END


