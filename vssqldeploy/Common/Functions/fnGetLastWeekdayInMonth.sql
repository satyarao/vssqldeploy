﻿

/*
    Supported values for parameter @DayOfWeek
        1 = Monday
        2 = Tuesday
        3 = Wednesday
        4 = Thursday
        5 = Friday
        6 = Saturday
        0 or 7 = Sunday

    SELECT [Common].[fnGetLastWeekdayInMonth] (0, GETDATE())
    SELECT [Common].[fnGetLastWeekdayInMonth] (1, GETDATE())
    SELECT [Common].[fnGetLastWeekdayInMonth] (2, GETDATE())
    SELECT [Common].[fnGetLastWeekdayInMonth] (3, GETDATE())
    SELECT [Common].[fnGetLastWeekdayInMonth] (4, GETDATE())
    SELECT [Common].[fnGetLastWeekdayInMonth] (5, GETDATE())
    SELECT [Common].[fnGetLastWeekdayInMonth] (6, GETDATE())
    SELECT [Common].[fnGetLastWeekdayInMonth] (7, GETDATE())
*/
CREATE FUNCTION [Common].[fnGetLastWeekdayInMonth]
	(@DayOfWeek INT
   , @ReferenceDate DATETIME2)
RETURNS DATE
AS
BEGIN
	DECLARE	@Result DATE = NULL;

    -- support of .NET values
	IF @DayOfWeek = 0
		BEGIN
			SET @DayOfWeek = 7;
		END;  

	DECLARE	@FirstOfWeekday DATETIME2 = DATEADD(DAY, @DayOfWeek - 1, 0);  

	SELECT	@Result = DATEADD(DAY, DATEDIFF(DAY, @FirstOfWeekday, DATEADD(MONTH, DATEDIFF(MONTH, 0, @ReferenceDate), DATEADD(DAY, -1, DATEADD(MONTH, 1, 0)))) / 7 * 7, @FirstOfWeekday);

	RETURN @Result;

END
