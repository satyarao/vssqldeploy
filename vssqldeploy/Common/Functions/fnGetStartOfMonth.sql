﻿
/*
    SELECT [Common].[fnGetStartOfMonth] (GETDATE())
    SELECT [Common].[fnGetStartOfMonth] ('2014-02-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetStartOfMonth]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL;  

	SET @ReferenceDate = CONVERT(DATE, @ReferenceDate);

	SELECT	@Result = DATEADD(MONTH, DATEDIFF(MONTH, 0, @ReferenceDate), 0);

	RETURN @Result;

END
