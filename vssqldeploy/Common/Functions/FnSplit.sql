﻿CREATE FUNCTION [Common].[FnSplit] (
	@ListString NVARCHAR(max)
	, @Delimiter NVARCHAR(1000)
	, @IncludeEmpty BIT = 0
	)
RETURNS @ListTable TABLE (
	ID INT
	, ListValue NVARCHAR(1000)
	)
AS
BEGIN
	DECLARE @CurrentPosition INT
		, @NextPosition INT
		, @Item NVARCHAR(max)
		, @ID INT
		, @L INT

	SELECT @ID = 1
		, @L = len(replace(@Delimiter, ' ', '^'))
		, @ListString = @ListString + @Delimiter
		, @CurrentPosition = 1

	SELECT @NextPosition = Charindex(@Delimiter, @ListString, @CurrentPosition)

	WHILE @NextPosition > 0
	BEGIN
		SET @Item = LTRIM(RTRIM(SUBSTRING(@ListString, @CurrentPosition, @NextPosition - @CurrentPosition)))

		IF @IncludeEmpty = 1
			OR LEN(@Item) > 0
		BEGIN
			INSERT INTO @ListTable (
				ID
				, ListValue
				)
			VALUES (
				@ID
				, @Item
				)

			SET @ID = @ID + 1
		END

		SET @CurrentPosition = @NextPosition + @L
		SET @NextPosition = Charindex(@Delimiter, @ListString, @CurrentPosition)
	END

	RETURN
END
