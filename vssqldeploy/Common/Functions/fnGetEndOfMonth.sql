﻿
/*
    SELECT [Common].[fnGetEndOfMonth] (GETDATE())
    SELECT [Common].[fnGetEndOfMonth] ('2014-02-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetEndOfMonth]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL;  

	SELECT	@Result = [Common].[fnGetEndOfDay](DATEADD(MONTH, DATEDIFF(MONTH, -1, @ReferenceDate), -1));

	RETURN @Result;

END
