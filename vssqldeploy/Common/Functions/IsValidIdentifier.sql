﻿
CREATE   FUNCTION [Common].[IsValidIdentifier](@Identifier nvarchar(100))
RETURNS BIT
AS
BEGIN
	RETURN 
	(
		SELECT 
		CASE
			WHEN @Identifier IN (SELECT [name] FROM sys.time_zone_info)
				THEN 1
			ELSE 0
		END
	)
END