﻿CREATE FUNCTION [Common].[BinaryToBase64] (@bin VARBINARY(MAX))
RETURNS VARCHAR(MAX)
AS
BEGIN
--SELECT Common.BinaryToBase64(CONVERT(VARBINARY(MAX), 'Converting this text to Base64...'))
	DECLARE	@Base64 VARCHAR(MAX)
	SET @Base64 = CAST(N'' AS XML).value('xs:base64Binary(xs:hexBinary(sql:variable("@bin")))', 'VARCHAR(MAX)')
	RETURN @Base64
END
