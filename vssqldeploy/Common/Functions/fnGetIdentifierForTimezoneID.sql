﻿CREATE FUNCTION [Common].[fnGetIdentifierForTimezoneID]
	(@SourceTimeZoneID INT)
RETURNS VARCHAR(100)
AS
BEGIN
	RETURN (SELECT Identifier FROM Common.LKTimezone WHERE TimeZoneID = @SourceTimeZoneID)
END
