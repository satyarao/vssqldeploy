﻿
CREATE FUNCTION [Common].[GetGeographyPoint]
(
	@Latitude decimal(10, 7),
	@Longitude decimal(10, 7)
)
RETURNS geography
AS
BEGIN
	DECLARE @Result geography
	SELECT @Result = GEOGRAPHY::STPointFromText('POINT(' + CAST(@Longitude AS VARCHAR(20)) + ' ' + CAST(@Latitude AS VARCHAR(20)) + ')', 4326)
	RETURN @Result
END
