﻿
/*
    SELECT [Common].[fnGetEndOfQuarter] (GETDATE())
    SELECT [Common].[fnGetEndOfQuarter] ('2014-02-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetEndOfQuarter]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL;  

	SELECT	@Result = [Common].[fnGetEndOfDay](DATEADD(QUARTER, DATEDIFF(QUARTER, -1, @ReferenceDate), -1));

	RETURN @Result;

END
