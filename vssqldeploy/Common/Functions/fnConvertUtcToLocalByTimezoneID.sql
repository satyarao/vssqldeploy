﻿/*
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (38, GETUTCDATE())

    -- northern hemisphere (+3600)
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (38, '2014-03-30 00:55:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (38, '2014-03-30 01:05:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (38, '2014-10-26 00:05:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (38, '2014-10-26 01:05:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (38, '2014-10-26 02:05:00')

    -- southern hemisphere (+3600)
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (43, '2014-04-05 23:55:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (43, '2014-04-06 00:55:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (43, '2014-04-06 01:55:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (43, '2014-09-07 00:55:00')
    SELECT [Common].[fnConvertUtcToLocalByTimezoneId] (43, '2014-09-07 01:05:00')

*/
CREATE FUNCTION [Common].[fnConvertUtcToLocalByTimezoneID]
	(@TargetTimezoneID INT
   , @UtcDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL

	SELECT	@Result = DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + COALESCE([ar].[DaylightDeltaSec], 0), @UtcDate)
	FROM	[Common].LKTimezone AS [tz] WITH (READUNCOMMITTED)
			LEFT JOIN [Common].[TimezoneAdjustmentRule] AS [ar] WITH (READUNCOMMITTED) ON 1 = 1
				AND [ar].[TimezoneID] = [tz].TimeZoneID
				AND CONVERT(DATE, CASE WHEN [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth] THEN DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate) -- southern hemisphere
										ELSE DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate) -- northern hemisphere
									END) BETWEEN [ar].[DateStart]
										AND	  [ar].[DateEnd]
				AND (
					1 = 0
					OR (
						1 = 1 -- southern hemisphere
						AND [ar].[DaylightTransitionStartMonth] > [ar].[DaylightTransitionEndMonth]
						AND NOT (
									1 = 1
									AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate) >= CASE WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
																															ELSE CASE	WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [Common].[fnGetLastWeekdayInMonth]([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
																																	ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [Common].[fnGetFirstWeekdayInMonth]([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
																																END
																														END
									AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate) <= CASE	WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
																								ELSE CASE WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [Common].[fnGetLastWeekdayInMonth]([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
																											ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [Common].[fnGetFirstWeekdayInMonth]([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
																										END
																							END
								)
						)
					OR (
						1 = 1                    -- northern hemisphere
						AND [ar].[DaylightTransitionStartMonth] <= [ar].[DaylightTransitionEndMonth]
						AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec], @UtcDate) >= CASE WHEN [ar].[DaylightTransitionStartIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
																						ELSE CASE WHEN [ar].[DaylightTransitionStartWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [Common].[fnGetLastWeekdayInMonth]([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
																									ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionStartWeek] - 1) * 7, [Common].[fnGetFirstWeekdayInMonth]([ar].[DaylightTransitionStartDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartDay]), 2), 121)))), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionStartTimeOfDay]), 121)
																							END
																					END
						AND DATEADD(SECOND, [tz].[BaseUtcOffsetSec] + [ar].[DaylightDeltaSec], @UtcDate) <= CASE WHEN [ar].[DaylightTransitionEndIsFixedDateRule] = 1 THEN CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
																													ELSE CASE WHEN [ar].[DaylightTransitionEndWeek] = 5 THEN CONVERT(DATETIME2, CONVERT(NVARCHAR, [Common].[fnGetLastWeekdayInMonth]([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
																															ELSE CONVERT(DATETIME2, CONVERT(NVARCHAR, CONVERT(DATE, DATEADD(DAY, ([ar].[DaylightTransitionEndWeek] - 1) * 7, [Common].[fnGetFirstWeekdayInMonth]([ar].[DaylightTransitionEndDayOfWeek], CONVERT(DATETIME2, RIGHT('0000' + CONVERT(NVARCHAR, YEAR(@UtcDate)), 4) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndMonth]), 2) + '-' + RIGHT('00' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndDay]), 2), 121)))), 121) + ' ' + CONVERT(NVARCHAR, [ar].[DaylightTransitionEndTimeOfDay]), 121)
																														END
																											END
						)
					)
	WHERE	1 = 1
			AND [tz].[TimeZoneID] = @TargetTimezoneID;

	RETURN @Result
END
