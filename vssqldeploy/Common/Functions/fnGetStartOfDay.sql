﻿
/*
    SELECT [Common].[fnGetStartOfDay] (GETDATE())
    SELECT [Common].[fnGetStartOfDay] ('2014-05-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetStartOfDay]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL;  

	SELECT	@Result = CONVERT(DATETIME2, CONVERT(DATE, @ReferenceDate));

	RETURN @Result;

END
