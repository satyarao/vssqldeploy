﻿
/*
    SET DATEFIRST 7 -- Sunday
    SET DATEFIRST 1 -- Monday

    SELECT [Common].[fnGetEndOfWeek] (GETDATE())
    SELECT [Common].[fnGetEndOfWeek] ('2014-02-03 12:45:13')
*/
CREATE FUNCTION [Common].[fnGetEndOfWeek]
	(@ReferenceDate DATETIME2)
RETURNS DATETIME2
AS
BEGIN
	DECLARE	@Result DATETIME2 = NULL
	  , @DayOfWeek INT = @@DATEFIRST;  

	DECLARE	@FirstOfWeekday DATETIME2 = DATEADD(DAY, @DayOfWeek - 1, 0);

	SELECT	@Result = [Common].[fnGetEndOfDay](DATEADD(DAY, 6, [Common].[fnGetStartOfWeek](@ReferenceDate)));

	RETURN @Result;

END
