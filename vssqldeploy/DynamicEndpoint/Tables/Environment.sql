﻿CREATE TABLE [DynamicEndpoint].[Environment] (
    [EnvironmentID]         SMALLINT      IDENTITY (1, 1) NOT NULL,
    [Name]                  VARCHAR (50)  NOT NULL,
    [Purpose]               NVARCHAR (50) NOT NULL,
    [IsProduction]          BIT           CONSTRAINT [DF_Environment_IsProduction] DEFAULT ((0)) NOT NULL,
    [OverrideEnvironmentID] SMALLINT      NULL,
    [IsActive]              BIT           CONSTRAINT [DF_Environment_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Environment] PRIMARY KEY CLUSTERED ([EnvironmentID] ASC) WITH (FILLFACTOR = 80)
);

