﻿CREATE TABLE [DynamicEndpoint].[ApiClientEndpointURI_config] (
    [EndpointID]    INT            IDENTITY (1, 1) NOT NULL,
    [EnvironmentID] SMALLINT       NOT NULL,
    [Command]       NVARCHAR (50)  NOT NULL,
    [APIVersion]    NVARCHAR (50)  NOT NULL,
    [Host]          NVARCHAR (50)  NULL,
    [URI]           NVARCHAR (250) NULL,
    [CName]         NVARCHAR (250) NULL,
    [HttpHeader]    NVARCHAR (250) NULL,
    [IsSecure]      BIT            NOT NULL,
    [CreatedOn]     DATETIME2 (7)  CONSTRAINT [DF_ApiClientEndpointURI_config_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]     DATETIME2 (7)  NULL,
    [IsActive]      BIT            CONSTRAINT [DF_ApiClientEndpointURI_config_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ApiClientEndpointURI_config] PRIMARY KEY CLUSTERED ([EndpointID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ApiClientEndpointURI_config_Environment] FOREIGN KEY ([EnvironmentID]) REFERENCES [DynamicEndpoint].[Environment] ([EnvironmentID])
);

