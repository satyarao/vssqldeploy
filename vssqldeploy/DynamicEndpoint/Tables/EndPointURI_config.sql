﻿CREATE TABLE [DynamicEndpoint].[EndPointURI_config] (
    [EndPointID]    INT              IDENTITY (1, 1) NOT NULL,
    [EnvironmentID] SMALLINT         NOT NULL,
    [TenantID]      UNIQUEIDENTIFIER NOT NULL,
    [Command]       NVARCHAR (50)    NOT NULL,
    [APIVersion]    NVARCHAR (50)    NOT NULL,
    [Host]          NVARCHAR (50)    NULL,
    [URI]           NVARCHAR (250)   NULL,
    [CName]         NVARCHAR (250)   NULL,
    [HttpHeader]    NVARCHAR (250)   NULL,
    [IsSecure]      BIT              NOT NULL,
    [CreatedOn]     DATETIME2 (7)    CONSTRAINT [DF_EndPointURI_config_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedOn]     DATETIME2 (7)    NULL,
    [IsActive]      BIT              CONSTRAINT [DF_EndPointURI_config_IsActive] DEFAULT ((1)) NOT NULL,
    [ActionCode]    CHAR (1)         CONSTRAINT [DF_EndPointURI_config_Action] DEFAULT ('A') NOT NULL,
    CONSTRAINT [PK_EndPointURI_config] PRIMARY KEY CLUSTERED ([EndPointID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_EndPointURI_config_Environment] FOREIGN KEY ([EnvironmentID]) REFERENCES [DynamicEndpoint].[Environment] ([EnvironmentID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_EndPointURI_config]
    ON [DynamicEndpoint].[EndPointURI_config]([EnvironmentID] ASC, [TenantID] ASC, [Command] ASC, [APIVersion] ASC) WITH (FILLFACTOR = 80);

