﻿CREATE TABLE [DynamicEndpoint].[ApiClientEndpointURI] (
    [EnvironmentName]    NVARCHAR (50)  NOT NULL,
    [APIVersion]         NVARCHAR (50)  NOT NULL,
    [Command]            NVARCHAR (50)  NOT NULL,
    [URI]                NVARCHAR (250) NULL,
    [HttpHeader]         NVARCHAR (250) NULL,
    [EnvironmentPurpose] NVARCHAR (50)  NOT NULL,
    [CreatedOn]          DATETIME2 (7)  CONSTRAINT [DF_ApiClientEndpointURI_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [EndPointID]         INT            IDENTITY (1, 1) NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF_ApiClientEndpointURI_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ApiClientEndpointURI] PRIMARY KEY CLUSTERED ([EnvironmentName] ASC, [APIVersion] ASC, [Command] ASC) WITH (FILLFACTOR = 80)
);

