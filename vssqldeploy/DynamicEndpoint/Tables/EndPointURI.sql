﻿CREATE TABLE [DynamicEndpoint].[EndPointURI] (
    [EnvironmentName]    VARCHAR (50)     NOT NULL,
    [TenantID]           UNIQUEIDENTIFIER NOT NULL,
    [APIVersion]         NVARCHAR (50)    NOT NULL,
    [Command]            NVARCHAR (50)    NOT NULL,
    [URI]                NVARCHAR (250)   NULL,
    [HttpHeader]         NVARCHAR (250)   NULL,
    [EnvironmentPurpose] NVARCHAR (50)    NOT NULL,
    [CreatedOn]          DATETIME2 (7)    CONSTRAINT [DF_zEndPointURI_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [EndPointID]         INT              IDENTITY (1, 1) NOT NULL,
    [IsActive]           BIT              CONSTRAINT [DF_EndPointURI_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_EndPointURI] PRIMARY KEY CLUSTERED ([EnvironmentName] ASC, [TenantID] ASC, [APIVersion] ASC, [Command] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [ix_EndPointURI_1]
    ON [DynamicEndpoint].[EndPointURI]([TenantID] ASC, [APIVersion] ASC)
    INCLUDE([Command], [EndPointID], [EnvironmentName], [EnvironmentPurpose], [HttpHeader], [URI]) WITH (FILLFACTOR = 80);

