﻿CREATE TABLE [DynamicEndpoint].[DefaultTenantAPIVersion] (
    [TenantID]      UNIQUEIDENTIFIER NOT NULL,
    [APIVersion]    VARCHAR (50)     NOT NULL,
    [EnvironmentID] SMALLINT         NOT NULL,
    CONSTRAINT [PK_DefaultTenantAPIVersion] PRIMARY KEY CLUSTERED ([TenantID] ASC, [APIVersion] ASC, [EnvironmentID] ASC) WITH (FILLFACTOR = 80)
);

