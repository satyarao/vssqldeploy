﻿
CREATE PROC [DynamicEndpoint].[RptGetDynamicEndpoint]
	@TenantID UNIQUEIDENTIFIER
  , @APIVersion NVARCHAR(50)
  , @EnvironmentName NVARCHAR(50) = NULL
  , @IsProduction BIT = NULL
AS --EXEC DynamicEndpoint.RptGetDynamicEndpoint @TenantID = '00000000-0000-0000-0000-000000000000', @APIVersion = '1.0', @EnvironmentName = NULL, @IsProduction = NULL
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET CONCAT_NULL_YIELDS_NULL OFF


CREATE TABLE [dbo].[#tmp]
	(
	  [Name] [NVARCHAR](50) NULL
	, [Purpose] [NVARCHAR](50) NOT NULL
	, [EndPointID] [INT] NOT NULL
	, [TenantID] [UNIQUEIDENTIFIER] NOT NULL
	, [Command] [NVARCHAR](50) NOT NULL
	, [APIVersion] [NVARCHAR](50) NOT NULL
	, [HttpHeader] [NVARCHAR](250) NULL
	, [EndpointURI] [NVARCHAR](509) NULL
	)

INSERT	INTO #tmp
		EXEC DynamicEndpoint.GetDynamicEndpoints @TenantID = @TenantID, @APIVersion = @APIVersion,
			@EnvironmentName = @EnvironmentName

SELECT	t.*
	  , te.Name AS TenantName
FROM	#tmp t
		JOIN dbo.Tenant te ON te.TenantID = t.TenantID

