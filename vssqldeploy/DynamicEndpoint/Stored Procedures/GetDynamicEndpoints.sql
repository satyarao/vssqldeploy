﻿CREATE PROC [DynamicEndpoint].[GetDynamicEndpoints]
	@TenantID UNIQUEIDENTIFIER
  , @APIVersion NVARCHAR(50)
  , @EnvironmentName NVARCHAR(50) = NULL
AS --EXEC [DynamicEndpoint].GetDynamicEndpoints @TenantID = '00000000-0000-0000-0000-000000000000', @APIVersion = '5'
SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

SELECT	L.EnvironmentName AS Name
	  , EnvironmentPurpose AS Purpose
	  , EndPointID
	  , L.TenantID
	  , L.Command
	  , APIVersion
	  , HttpHeader
	  , URI AS EndpointURI
FROM	[DynamicEndpoint].[EndPointURI] AS L
LEFT JOIN (
	SELECT EnvironmentName
	  , TenantID
	  , Command
	  , MAX(APIVersion) AS MaxVersion
	FROM [DynamicEndpoint].[EndPointURI]
	WHERE TenantID = @TenantID AND EnvironmentName = ISNULL(@EnvironmentName, EnvironmentName) AND APIVersion <= @APIVersion
	GROUP BY Command, TenantID, EnvironmentName
) AS R
ON L.TenantID = R.TenantID AND L.Command = R.Command AND L.EnvironmentName = R.EnvironmentName AND L.APIVersion = R.MaxVersion
WHERE	
		L.APIVersion = R.MaxVersion
		AND L.TenantID = @TenantID
		AND L.EnvironmentName = ISNULL(@EnvironmentName, L.EnvironmentName)
		AND IsActive = 1
ORDER BY Name, L.Command
