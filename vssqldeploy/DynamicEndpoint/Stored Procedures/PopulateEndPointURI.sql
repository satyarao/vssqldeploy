﻿
CREATE PROC [DynamicEndpoint].[PopulateEndPointURI]
	@EnvironmentID SMALLINT
  , @APIVersion NVARCHAR(50)
  , @TenantID UNIQUEIDENTIFIER = NULL --Required to include TenantID if it's a new one
AS
SELECT	T.TenantID
INTO	#tmp
FROM	dbo.Tenant T
WHERE	T.IsActive = 1 --TenantTypeID IN ( 0, 2, 3, 4 )
		AND ( --@TenantID IS NULL OR
			T.TenantID = @TenantID
			OR
			(@TenantID IS NULL AND EXISTS(SELECT 1 FROM [DynamicEndpoint].[EndPointURI] URI WHERE URI.TenantID = T.TenantID AND URI.IsActive = 1))
			)

DECLARE	@LTenantID UNIQUEIDENTIFIER

WHILE 1 = 1
	BEGIN
		SET @LTenantID = NULL
		SELECT	@LTenantID = TenantID
		FROM	#tmp

		IF @LTenantID IS NULL
			BREAK

		EXEC DynamicEndpoint.IUDEndPointURI @EnvironmentID = @EnvironmentID, @TenantID = @LTenantID, @APIVersion = @APIVersion

		DELETE	FROM #tmp
		WHERE	TenantID = @LTenantID
	END

