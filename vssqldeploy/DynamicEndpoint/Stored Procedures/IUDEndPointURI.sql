﻿
CREATE PROC [DynamicEndpoint].[IUDEndPointURI]
	@EnvironmentID SMALLINT
  , @TenantID UNIQUEIDENTIFIER
  , @APIVersion NVARCHAR(50)
AS /*
EXEC DynamicEndpoint.IUDEndPointURI @EnvironmentID = 2, @TenantID = '7E9FE494-AB73-40B5-A722-757D7E2D8AA7', @APIVersion = '2'
*/
SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF
DECLARE	@EnvironmentName VARCHAR(50)
DECLARE	@EnvironmentPurpose VARCHAR(50)
DECLARE	@OverrideEnvironmentID SMALLINT

SET @EnvironmentName = NULL
SET @EnvironmentPurpose = NULL
SELECT	@EnvironmentName = Name
	  , @EnvironmentPurpose = Purpose
	  , @OverrideEnvironmentID = OverrideEnvironmentID
FROM	DynamicEndpoint.Environment
WHERE	EnvironmentID = @EnvironmentID

SELECT	@EnvironmentID = ISNULL(@OverrideEnvironmentID, @EnvironmentID)
						  
IF @EnvironmentName IS NULL
	RETURN

DELETE	FROM DynamicEndpoint.EndPointURI
WHERE	EnvironmentName = @EnvironmentName
		AND TenantID = @TenantID
		AND APIVersion = @APIVersion

INSERT	INTO DynamicEndpoint.EndPointURI
		(EnvironmentName
	   , TenantID
	   , Command
	   , APIVersion
	   , URI
	   , HttpHeader
	   , EnvironmentPurpose)
SELECT	@EnvironmentName
	  , @TenantID
	  , Command
	  , APIVersion
	  , CASE WHEN IsSecure = 1 THEN 'https://'
			 ELSE 'http://'
		END + ISNULL(CName, Host) + CASE WHEN ISNULL(CName, Host) IS NOT NULL THEN '/'
									END + URI AS EndpointURI
	  , HttpHeader
	  , @EnvironmentPurpose
FROM	DynamicEndpoint.EndPointURI_config
WHERE	EnvironmentID = @EnvironmentID
		AND TenantID = @TenantID
		AND APIVersion = @APIVersion
		AND IsActive = 1
		AND ActionCode = 'A'

INSERT	INTO DynamicEndpoint.EndPointURI
		(EnvironmentName
	   , TenantID
	   , Command
	   , APIVersion
	   , URI
	   , HttpHeader
	   , EnvironmentPurpose)
SELECT	@EnvironmentName
	  , @TenantID
	  , ec.Command
	  , ec.APIVersion
	  , CASE WHEN IsSecure = 1 THEN 'https://'
			 ELSE 'http://'
		END + ISNULL(CName, Host) + CASE WHEN ISNULL(CName, Host) IS NOT NULL THEN '/'
									END + ec.URI AS EndpointURI
	  , ec.HttpHeader
	  , @EnvironmentPurpose
FROM	DynamicEndpoint.EndPointURI_config ec
		LEFT JOIN DynamicEndpoint.EndPointURI eu ON eu.EnvironmentName = @EnvironmentName
										AND eu.Command = ec.Command
										AND eu.TenantID = @TenantID
										AND eu.APIVersion = @APIVersion
WHERE	ec.EnvironmentID = @EnvironmentID
		AND ec.TenantID = '00000000-0000-0000-0000-000000000000'
		AND ec.APIVersion = @APIVersion
		AND eu.TenantID IS NULL
		AND ec.IsActive = 1
		AND ec.Command NOT IN (SELECT	Command
							   FROM		DynamicEndpoint.EndPointURI_config
							   WHERE	TenantID = @TenantID
										AND APIVersion = @APIVersion
										AND ActionCode = 'R')

