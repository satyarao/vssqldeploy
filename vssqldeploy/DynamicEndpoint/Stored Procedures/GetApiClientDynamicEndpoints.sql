﻿
-- =============================================
-- Author:		Daniel DiPaolo
-- Create date: 2015/04/21
-- Description:	Get endpoints for API Clients
-- =============================================
CREATE PROCEDURE [DynamicEndpoint].[GetApiClientDynamicEndpoints] 
	-- Add the parameters for the stored procedure here
	@APIVersion nvarchar(50), 
	@EnvironmentName nvarchar(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SELECT	EnvironmentName AS Name
	  , EnvironmentPurpose AS Purpose
	  , EndPointID
	  , Command
	  , APIVersion
	  , HttpHeader
	  , URI AS EndpointURI
FROM	DynamicEndpoint.ApiClientEndPointURI
WHERE	APIVersion = @APIVersion
		AND ( @EnvironmentName IS NULL
			  OR EnvironmentName = @EnvironmentName)
ORDER BY Name
	  , Command

END

