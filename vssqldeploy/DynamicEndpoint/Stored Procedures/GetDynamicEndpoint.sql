﻿CREATE PROC [DynamicEndpoint].[GetDynamicEndpoint]
	@TenantID UNIQUEIDENTIFIER
  , @APIVersion NVARCHAR(50)
AS --EXEC [DynamicEndpoint].GetDynamicEndpoint @TenantID = '00000000-0000-0000-0000-000000000000', @APIVersion = '5'
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE	@EnvironmentID INT
SELECT	@EnvironmentID = EnvironmentID
FROM	[DynamicEndpoint].[DefaultTenantAPIVersion] L
WHERE	L.APIVersion = @APIVersion
		AND L.TenantID = @TenantID

DECLARE	@Name VARCHAR(50)
SELECT	@Name = name
FROM	[DynamicEndpoint].[Environment]
WHERE	EnvironmentID = @EnvironmentID

SELECT	@Name AS Name
	  , EnvironmentPurpose AS Purpose
	  , EndPointID
	  , L.TenantID
	  , L.Command
	  , APIVersion
	  , HttpHeader
	  , URI AS EndpointURI
FROM	[DynamicEndpoint].[EndPointURI] AS L
LEFT JOIN (
	SELECT EnvironmentName
	  , TenantID
	  , Command
	  , MAX(APIVersion) AS MaxVersion
	FROM [DynamicEndpoint].[EndPointURI]
	WHERE TenantID = @TenantID AND EnvironmentName = @Name AND APIVersion <= @APIVersion
	GROUP BY Command, TenantID, EnvironmentName
) AS R
ON L.TenantID = R.TenantID AND L.Command = R.Command AND L.EnvironmentName = R.EnvironmentName AND L.APIVersion = R.MaxVersion
WHERE	L.APIVersion = R.MaxVersion
		AND L.TenantID = @TenantID
		AND L.EnvironmentName = @Name
		AND IsActive = 1
ORDER BY L.Command
