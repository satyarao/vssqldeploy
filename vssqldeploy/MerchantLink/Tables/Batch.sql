﻿CREATE TABLE [MerchantLink].[Batch] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [BatchNumber] INT            NOT NULL,
    [Lane]        NVARCHAR (50)  NOT NULL,
    [ErrorCode]   INT            NOT NULL,
    [ItemCount]   INT            NOT NULL,
    [Total]       DECIMAL (9, 2) NOT NULL,
    [Result]      NVARCHAR (50)  NOT NULL,
    [RefData]     NVARCHAR (MAX) NOT NULL,
    [CreatedOn]   DATETIME2 (7)  NOT NULL,
    [MppaBatchID] INT            NOT NULL,
    CONSTRAINT [PK__Batch__3214EC271D8CA242] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__Batch__MppaBatch__76B533DB] FOREIGN KEY ([MppaBatchID]) REFERENCES [dbo].[MppaBatch] ([BatchID])
);

