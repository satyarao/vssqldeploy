﻿CREATE TYPE [MerchantLink].[ListOfRecNums] AS TABLE (
    [RecNum] NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([RecNum] ASC));

