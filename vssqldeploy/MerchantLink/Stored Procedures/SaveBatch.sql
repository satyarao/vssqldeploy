﻿
CREATE PROCEDURE [MerchantLink].[SaveBatch]
	@BatchID int,
	@MerchantLinkBatchID int,
	@Lane nvarchar(50),
	@Result nvarchar(50),
	@ErrorCode int,
	@ItemCount int,
	@Total decimal(9,2),
	@RefData nvarchar(max)
AS
BEGIN	
	SET NOCOUNT ON;

	insert into MerchantLink.Batch (BatchNumber, Lane, Result, ErrorCode, CreatedOn, ItemCount, Total, RefData, MppaBatchID)
	values (@MerchantLinkBatchID, @Lane, @Result, @ErrorCode, GETUTCDATE(), @ItemCount, @Total, @RefData, @BatchID);
END
