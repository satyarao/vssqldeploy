﻿CREATE TABLE [UserInput].[LKEntryDataType] (
    [EntryDataTypeId]    INT            IDENTITY (1, 1) NOT NULL,
    [EntryName]          NVARCHAR (MAX) NULL,
    [EntryDescription]   NVARCHAR (MAX) NULL,
    [EntryDataType]      NVARCHAR (MAX) NULL,
    [DefaultRegexFormat] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_LKUserInputEntryType] PRIMARY KEY CLUSTERED ([EntryDataTypeId] ASC)
);

