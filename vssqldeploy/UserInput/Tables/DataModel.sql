﻿CREATE TABLE [UserInput].[DataModel] (
    [DataModelId]     INT              IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (MAX)   NULL,
    [RegexFormat]     NVARCHAR (MAX)   NULL,
    [Metadata]        NVARCHAR (MAX)   NULL,
    [TenantId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]       DATETIME         NULL,
    [IsActive]        BIT              NULL,
    [EntryDataTypeId] INT              NULL,
    [DataType]        NVARCHAR (50)    NULL,
    CONSTRAINT [PK_UserInputEntity] PRIMARY KEY CLUSTERED ([DataModelId] ASC)
);

