﻿CREATE TABLE [UserInput].[Fields] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [DisplayName]    NVARCHAR (MAX)   NOT NULL,
    [NameKey]        NVARCHAR (MAX)   NOT NULL,
    [Description]    NVARCHAR (MAX)   NULL,
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [InputType]      NVARCHAR (MAX)   NULL,
    [DataType]       NVARCHAR (MAX)   NULL,
    [IsActive]       BIT              NOT NULL,
    [IsRequired]     BIT              NOT NULL,
    [Weight]         INT              NOT NULL,
    [Masked]         BIT              NOT NULL,
    [DropDownValues] NVARCHAR (MAX)   NULL,
    [ParentId]       INT              NULL,
    CONSTRAINT [PK_UserInput.Fields] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserInput.Fields_UserInput.Fields_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [UserInput].[Fields] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentId]
    ON [UserInput].[Fields]([ParentId] ASC);

