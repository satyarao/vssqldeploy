﻿CREATE TABLE [UserInput].[Values] (
    [Id]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [Value]        NVARCHAR (MAX)   NULL,
    [LoweredValue] NVARCHAR (MAX)   NULL,
    [UserId]       UNIQUEIDENTIFIER NOT NULL,
    [FieldId]      INT              NOT NULL,
    CONSTRAINT [PK_UserInput.Values] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserInput.Values_UserInput.Fields_FieldId] FOREIGN KEY ([FieldId]) REFERENCES [UserInput].[Fields] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FieldId]
    ON [UserInput].[Values]([FieldId] ASC);

