﻿CREATE TABLE [UserInput].[Entry] (
    [EntryValue]  NVARCHAR (MAX)   NULL,
    [TenantId]    UNIQUEIDENTIFIER NULL,
    [UserId]      UNIQUEIDENTIFIER NULL,
    [CreatedOn]   DATETIME         NULL,
    [IsActive]    BIT              NULL,
    [EntryId]     BIGINT           IDENTITY (1, 1) NOT NULL,
    [DataModelId] INT              NULL,
    CONSTRAINT [PK_Entry] PRIMARY KEY CLUSTERED ([EntryId] ASC) WITH (FILLFACTOR = 80)
);

