﻿CREATE TABLE [GroupSet].[LKAudience] (
    [GroupId] UNIQUEIDENTIFIER NULL,
    [UserId]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [FK_LKAudience_Audience] FOREIGN KEY ([GroupId]) REFERENCES [GroupSet].[Audience] ([GroupId]),
    CONSTRAINT [FK_LKAudience_UserInfo] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserInfo] ([UserID])
);

