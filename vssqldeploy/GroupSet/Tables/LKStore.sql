﻿CREATE TABLE [GroupSet].[LKStore] (
    [GroupId] UNIQUEIDENTIFIER NULL,
    [StoreId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [FK_LKStore_Store] FOREIGN KEY ([GroupId]) REFERENCES [GroupSet].[Store] ([GroupId]),
    CONSTRAINT [FK_LKStore_Store1] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreID])
);

