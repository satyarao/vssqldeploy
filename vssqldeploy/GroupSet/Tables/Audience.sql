﻿CREATE TABLE [GroupSet].[Audience] (
    [GroupId]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    [GroupName]        NVARCHAR (150)   NOT NULL,
    [GroupDescription] NVARCHAR (250)   NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    CONSTRAINT [PK_Audience] PRIMARY KEY CLUSTERED ([GroupId] ASC)
);

