﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[CreateLKStoreValues]
(
	@GroupId uniqueidentifier,
	@StoreIds nvarchar(MAX)
)
AS
BEGIN

    SET NOCOUNT ON

insert into [GroupSet].[LKStore]
select @GroupId,value from string_split(@StoreIds,',')

END
