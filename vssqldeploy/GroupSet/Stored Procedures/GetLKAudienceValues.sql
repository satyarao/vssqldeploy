﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[GetLKAudienceValues]
(
	@GroupId uniqueidentifier
)
AS
BEGIN

    SET NOCOUNT ON

SELECT DISTINCT([UserId])
  FROM [GroupSet].[LKAudience] WHERE [GroupId] = @GroupId

END
