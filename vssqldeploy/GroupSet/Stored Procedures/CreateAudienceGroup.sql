﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[CreateAudienceGroup]
(
	@TenantId uniqueidentifier,
	@GroupName nvarchar(150),
	@GroupDescription nvarchar(250)
)
AS
BEGIN

    SET NOCOUNT ON

INSERT INTO [GroupSet].[Audience]
           ([GroupId]
           ,[TenantId]
           ,[GroupName]
           ,[GroupDescription]
           ,[CreatedOn]
           ,[IsDeleted])
		   OUTPUT INSERTED.[GroupId]
     VALUES
           (NEWID()
           ,@TenantId
           ,@GroupName
           ,@GroupDescription
           ,GETUTCDATE()
           ,0)

END
