﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[DeleteLKStoreValues]
(
	@GroupId uniqueidentifier,
	@StoreIds nvarchar(MAX)
)
AS
BEGIN

    SET NOCOUNT ON  
DECLARE @Message nvarchar(max);
BEGIN TRY
	BEGIN TRAN

		DELETE FROM [GroupSet].[LKStore] WHERE [GroupId]=@GroupId and [StoreId] in (select value from string_split(@StoreIds,','))
			
	COMMIT TRANSACTION
END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SET @Message = (SELECT ERROR_MESSAGE() AS ErrorMessage);

		THROW 50000, @Message, 1;
			
	END CATCH

END
