﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[DeleteAudienceGroup]
(
	@GroupId uniqueidentifier
)
AS
BEGIN

    SET NOCOUNT ON  
DECLARE @Message nvarchar(max);
BEGIN TRY
	BEGIN TRAN

		IF EXISTS(SELECT [GroupId] FROM [GroupSet].[Audience] WHERE [GroupId]=@GroupId)
			BEGIN
				DELETE FROM [GroupSet].[LKAudience] WHERE [GroupId]=@GroupId
				DELETE FROM [GroupSet].[Audience] WHERE [GroupId]=@GroupId
			END
		ELSE
			BEGIN
				THROW 50001, 'Audience group not found', 1;
			END

	COMMIT TRANSACTION
END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SET @Message = (SELECT ERROR_MESSAGE() AS ErrorMessage);

		THROW 50000, @Message, 1;
			
	END CATCH

END
