﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[GetLKStoreValues]
(
	@GroupId uniqueidentifier
)
AS
BEGIN

    SET NOCOUNT ON

SELECT DISTINCT([StoreId])
  FROM [GroupSet].[LKStore] WHERE [GroupId] = @GroupId

END
