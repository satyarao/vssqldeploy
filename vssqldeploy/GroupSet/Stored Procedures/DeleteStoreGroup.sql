﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[DeleteStoreGroup]
(
	@GroupId uniqueidentifier
)
AS
BEGIN

    SET NOCOUNT ON  
DECLARE @Message nvarchar(max);
BEGIN TRY
	BEGIN TRAN

		IF EXISTS(SELECT [GroupId] FROM [GroupSet].[Store] WHERE [GroupId]=@GroupId)
			BEGIN
				DELETE FROM [GroupSet].[LKStore] WHERE [GroupId]=@GroupId
				DELETE FROM [GroupSet].[Store] WHERE [GroupId]=@GroupId
			END
		ELSE
			BEGIN
				THROW 50001, 'Store group not found', 1;
			END

	COMMIT TRANSACTION
END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SET @Message = (SELECT ERROR_MESSAGE() AS ErrorMessage);

		THROW 50000, @Message, 1;
			
	END CATCH

END
