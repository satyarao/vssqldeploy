﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[GetAudienceGroup]
(
	@GroupId uniqueidentifier,
	@TenantId uniqueidentifier,
	@GroupName nvarchar(150)
)
AS
BEGIN

    SET NOCOUNT ON

SELECT [GroupId]
      ,[TenantId]
      ,[GroupName]
      ,[GroupDescription]
      ,[CreatedOn]
      ,[IsDeleted]
      ,[DeletedOn]
  FROM [GroupSet].[Audience] WHERE
  ([IsDeleted]=0 and
 [TenantId] = @TenantId) and 
 [GroupId] = COALESCE(isnull(@GroupId, null), [GroupId]) and
 [GroupName] like '%'+ COALESCE(NULLIF(@GroupName, ''), [GroupName])+'%'

END
