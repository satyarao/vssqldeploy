﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[CreateLKAudienceValues]
(
	@GroupId uniqueidentifier,
	@UserIds nvarchar(MAX)
)
AS
BEGIN

    SET NOCOUNT ON

insert into [GroupSet].[LKAudience]
select @GroupId,value from string_split(@UserIds,',')

END
