﻿
-------------------------------------------------------------------------------

CREATE   PROCEDURE [GroupSet].[DeleteLKAudienceValues]
(
	@GroupId uniqueidentifier,
	@UserIds nvarchar(MAX)
)
AS
BEGIN

    SET NOCOUNT ON  
DECLARE @Message nvarchar(max);
BEGIN TRY
	BEGIN TRAN

		DELETE FROM [GroupSet].[LKAudience] WHERE [GroupId]=@GroupId and [UserId] in (select value from string_split(@UserIds,','))
			
	COMMIT TRANSACTION
END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SET @Message = (SELECT ERROR_MESSAGE() AS ErrorMessage);

		THROW 50000, @Message, 1;
			
	END CATCH

END
