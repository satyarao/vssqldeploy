﻿CREATE TABLE [Reports].[BasketLineItem_LoyaltyDescription] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [BasketLineItemID] UNIQUEIDENTIFIER NOT NULL,
    [OfferId]          NVARCHAR (256)   NOT NULL,
    [OfferAmount]      DECIMAL (9, 2)   NULL,
    [OfferTenantId]    UNIQUEIDENTIFIER NULL,
    [OfferTitle]       NVARCHAR (MAX)   NULL,
    [OfferDiscount]    DECIMAL (9, 2)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [Reports_BasketLineItem_LoyaltyDescription_BasketLineItemID]
    ON [Reports].[BasketLineItem_LoyaltyDescription]([BasketLineItemID] ASC) WITH (FILLFACTOR = 80);

