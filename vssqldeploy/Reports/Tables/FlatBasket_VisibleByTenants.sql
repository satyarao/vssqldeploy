﻿CREATE TABLE [Reports].[FlatBasket_VisibleByTenants] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [FlatBasketID]    UNIQUEIDENTIFIER NOT NULL,
    [TransactionID]   UNIQUEIDENTIFIER NOT NULL,
    [PartnerTenantId] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatBasket_VisibleByTenants_FlatBasketID_TransactionID]
    ON [Reports].[FlatBasket_VisibleByTenants]([FlatBasketID] ASC, [TransactionID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Reports_FlatBasket_VisibleByTenants_FlatBasketID]
    ON [Reports].[FlatBasket_VisibleByTenants]([FlatBasketID] ASC) WITH (FILLFACTOR = 80);

